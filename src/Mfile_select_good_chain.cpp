#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include<vector>

std::vector<mfile0::M_Chain> SelectLongChain(std::vector<mfile0::M_Chain> &c);
mfile0::M_Chain select_best_chain(std::vector<mfile0::M_Chain> &c);
std::vector<mfile0::M_Chain> reject_low_eff_chain(std::vector<mfile0::M_Chain> &c);

void main(int argc, char **argv)
{
	if (argc != 3) {
		fprintf(stderr, "usage : prg_name [input m-file] [output m-file]\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	m.chains = SelectLongChain(m.chains);
	m.chains = reject_low_eff_chain(m.chains);

	mfile1::write_mfile_extension(file_out_mfile, m);
}

std::vector<mfile0::M_Chain> SelectLongChain(std::vector<mfile0::M_Chain> &c)
{
	int all = c.size();
	std::vector<mfile0::M_Chain> ret;

	std::multimap<int, mfile0::M_Chain*> group;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		group.insert(std::make_pair(itr->basetracks.begin()->group_id, &(*itr)));
	}

	int num_group = 0;
	for (auto itr = group.begin(); itr != group.end(); itr++) {
		num_group++;
		int cnt = group.count(itr->first);
		auto res = group.equal_range(itr->first);
		std::vector<mfile0::M_Chain> chain;
		chain.reserve(cnt);
		for (auto itr_l = res.first; itr_l != res.second; itr_l++) {
			chain.push_back(*(itr_l->second));
		}
		ret.push_back(select_best_chain(chain));

		itr = std::next(itr, cnt-1);
	}

	printf("all chain       : %d\n", all);
	printf("number of group : %d\n",num_group);
	printf("select chain    : %d\n", ret.size());

	return ret;
}

mfile0::M_Chain select_best_chain(std::vector<mfile0::M_Chain> &c) {
	std::vector<mfile0::M_Chain> sel0;
	int nseg = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		nseg = std::max(nseg, itr->nseg);
	}
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (nseg == itr->nseg) {
			sel0.push_back(*itr);
		}
	}

	double d_lat = 1000000000;
	mfile0::M_Chain ret;
	for (auto itr = sel0.begin(); itr != sel0.end(); itr++) {
		if (itr == sel0.begin()) {
			d_lat = mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(*itr), mfile0::chain_ay(*itr));
			ret = *itr;
		}
		if (d_lat > mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(*itr), mfile0::chain_ay(*itr))) {
			d_lat = mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(*itr), mfile0::chain_ay(*itr));
			ret = *itr;
		}
	}

	return ret;
}
std::vector<mfile0::M_Chain> reject_low_eff_chain(std::vector<mfile0::M_Chain> &c)
{
	std::vector<mfile0::M_Chain> ret;
	ret.reserve(c.size());
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		int npl = (itr->pos1 - itr->pos0) / 10 + 1;
		double eff = npl * 1. / itr->nseg;
		if (eff > 0.5000000001) {
			ret.push_back(*itr);
		}
	}
	printf("eff>0.5 : %d\n", ret.size());
	return ret;
}
