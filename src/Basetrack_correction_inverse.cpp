#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void trapezoid_correction(std::vector<netscan::base_track_t >&base, double slope, double intercept);
void angle_correction(std::vector<netscan::base_track_t> &base, double value);


int main(int argc, char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg input-bvxx pl zone ouput-bvxx\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_bvxx = argv[4];

	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_bvxx, base, pl, zone);

	double slope, intercept;
	slope = -1.53*pow(10, -9);
	intercept = 1.0;
	trapezoid_correction(base, slope, intercept);

	double angle_corr = 0.951;
	angle_correction(base, angle_corr);


	netscan::write_basetrack_vxx(file_out_bvxx, base, pl, zone);

}
void angle_correction(std::vector<netscan::base_track_t> &base, double value) {

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		itr->ax = itr->ax*(1./value);
		itr->ay = itr->ay*(1./value);
	}
	printf("basetrack angle trans *%5.4lf\n", 1.0/value);
	return;
}
void trapezoid_correction(std::vector<netscan::base_track_t >&base, double slope, double intercept) {
	double Area_y[2], Area_x[2], x_center;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			Area_y[0] = itr->y;
			Area_y[1] = itr->y;
		}
		if (Area_y[0] > itr->y) {
			Area_y[0] = itr->y;
		}
		if (Area_y[1] < itr->y) {
			Area_y[1] = itr->y;
		}
	}
	int count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		//yの下限から1cmのトラックのみ使用
		//xのcenterの決定
		if (Area_y[0] + 10000 < itr->y)continue;
		if (count == 0) {
			Area_x[0] = itr->x;
			Area_x[1] = itr->x;
			count++;
		}
		if (Area_x[0] > itr->x) {
			Area_x[0] = itr->x;
		}
		if (Area_x[1] < itr->x) {
			Area_x[1] = itr->x;
		}
	}
	x_center = (Area_x[1] - Area_x[0]) / 2;
	double shrink_x;
	double tmp_x;

	for (auto itr = base.begin(); itr != base.end(); itr++) {

		shrink_x = slope * itr->y + intercept;
		tmp_x = itr->x;
		//printf("y shrinkx %8.1lf %8.7lf\n",itr->y,shrink_x);
		itr->x = (tmp_x - x_center)*(shrink_x) + x_center;

	}
	printf("trapezoid correction\n");
	printf("shrink value= 1+y_position*%12.11lf\n", slope);
	printf("calc: x-->(x-x_center)*shrink+x_center\n");


}