#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <windows.h>
#include <filesystem>
#include <iomanip>
#include <chrono>
#include <omp.h>

int do_hts2beta_all(std::string file_in_data, std::string file_out_data);
int do_hts_ali_one(std::string filepath);
int do_hts_deadpixel_all(std::string filepath);
int do_hts_fvxx_all(std::string filepath, int pl);

int do_hts2beta_one(std::string file_in_data, std::string file_out_data, int arg);
int do_hts_deadpixel_one(std::string filepath, int arg);
int do_hts_fvxx_one(std::string filepath, std::string &file_out_data, int pl, int arg);
int do_f_filter_one(std::string file_in_data, std::string file_out_data, int pl, int arg);

PROCESS_INFORMATION do_prg(std::string path, std::string command, std::string output_log, HANDLE &h_out);

void console_error_out(std::string filename);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg input-folder intermediate-folder output-folder pl\n");
		exit(1);
	}
	std::string file_in_data = argv[1];
	std::string file_intermadeitae_data = argv[2];
	std::string file_out_data = argv[3];
	int pl = std::stoi(argv[4]);
	bool ret;
	ret = std::filesystem::create_directories(file_intermadeitae_data);
	ret = std::filesystem::create_directories(file_out_data);

	std::filesystem::copy(file_in_data + "\\Beta_EachImagerParam.json", file_intermadeitae_data + "\\Beta_EachImagerParam.json", std::filesystem::copy_options::overwrite_existing);
	std::filesystem::copy(file_in_data + "\\Beta_EachShotParam.json", file_intermadeitae_data + "\\Beta_EachShotParam.json", std::filesystem::copy_options::overwrite_existing);
	std::filesystem::copy(file_in_data + "\\Beta_EachViewParam.json", file_intermadeitae_data + "\\Beta_EachViewParam.json", std::filesystem::copy_options::overwrite_existing);

	auto start = std::chrono::system_clock::now(); // 計測開始時間
	bool ali_flg = false;
#pragma omp parallel sections 
	{
#pragma omp section 
		{
			int  arg = 0;

			if (do_hts2beta_one(file_in_data, file_intermadeitae_data, arg))exit(1);
			if (arg == 0) {
				if (do_hts_ali_one(file_intermadeitae_data))exit(1);
#pragma omp critical
				{
				ali_flg = true;
				}
			}
			else {
				while (!ali_flg) {
					Sleep(10*1000);
					printf("arg:%d flg:%d\n", arg, ali_flg);
				}
			}
			if (do_hts_deadpixel_one(file_intermadeitae_data, arg))exit(1);
			std::string file_out_tmp;
			if (do_hts_fvxx_one(file_intermadeitae_data, file_out_tmp, pl, arg))exit(1);
			if (do_f_filter_one(file_out_tmp, file_out_data, pl, arg))exit(1);
		}
#pragma omp section 
		{
			int  arg = 1;

			if (do_hts2beta_one(file_in_data, file_intermadeitae_data, arg))exit(1);
			if (arg == 0) {
				if (do_hts_ali_one(file_intermadeitae_data))exit(1);
				ali_flg = true;
			}
			else {
				while (!ali_flg) {
					Sleep(10 * 1000);
					printf("arg:%d flg:%d\n", arg, ali_flg);
				}
			}
			if (do_hts_deadpixel_one(file_intermadeitae_data, arg))exit(1);
			std::string file_out_tmp;
			if (do_hts_fvxx_one(file_intermadeitae_data, file_out_tmp, pl, arg))exit(1);
			if (do_f_filter_one(file_out_tmp, file_out_data, pl, arg))exit(1);
		}
#pragma omp section 
		{
			int  arg = 2;

			if (do_hts2beta_one(file_in_data, file_intermadeitae_data, arg))exit(1);
			if (arg == 0) {
				if (do_hts_ali_one(file_intermadeitae_data))exit(1);
				ali_flg = true;
			}
			else {
				while (!ali_flg) {
					Sleep(10 * 1000);
					printf("arg:%d flg:%d\n", arg, ali_flg);
				}
			}
			if (do_hts_deadpixel_one(file_intermadeitae_data, arg))exit(1);
			std::string file_out_tmp;
			if (do_hts_fvxx_one(file_intermadeitae_data, file_out_tmp, pl, arg))exit(1);
			if (do_f_filter_one(file_out_tmp, file_out_data, pl, arg))exit(1);
		}
#pragma omp section 
		{
			int  arg = 3;

			if (do_hts2beta_one(file_in_data, file_intermadeitae_data, arg))exit(1);
			if (arg == 0) {
				if (do_hts_ali_one(file_intermadeitae_data))exit(1);
				ali_flg = true;
			}
			else {
				while (!ali_flg) {
					Sleep(10 * 1000);
					printf("arg:%d flg:%d\n", arg, ali_flg);
				}
			}
			if (do_hts_deadpixel_one(file_intermadeitae_data, arg))exit(1);
			std::string file_out_tmp;
			if (do_hts_fvxx_one(file_intermadeitae_data, file_out_tmp, pl, arg))exit(1);
			if (do_f_filter_one(file_out_tmp, file_out_data, pl, arg))exit(1);
		}
	}
	auto end = std::chrono::system_clock::now(); // 計測開始時間
	double elapsed = std::chrono::duration_cast<std::chrono::seconds>(end - start).count(); //処理に要した時間をミリ秒に変換

	printf("hts2fvxx time:%.0lf[s]\n", elapsed);
	std::filesystem::copy(file_intermadeitae_data + "\\Beta_EachImagerParam.json", file_out_data + "\\Beta_EachImagerParam.json", std::filesystem::copy_options::overwrite_existing);
	std::filesystem::copy(file_intermadeitae_data + "\\Beta_EachShotParam.json", file_out_data + "\\Beta_EachShotParam.json", std::filesystem::copy_options::overwrite_existing);
	std::filesystem::copy(file_intermadeitae_data + "\\Beta_EachViewParam.json", file_out_data + "\\Beta_EachViewParam.json", std::filesystem::copy_options::overwrite_existing);
	std::filesystem::copy(file_intermadeitae_data + "\\ali.json", file_out_data + "\\ali.json", std::filesystem::copy_options::overwrite_existing);
	std::filesystem::copy(file_intermadeitae_data + "\\ali.pdf", file_out_data + "\\ali.pdf", std::filesystem::copy_options::overwrite_existing);
	std::filesystem::copy(file_intermadeitae_data + "\\deadpixel.json", file_out_data + "\\deadpixel.json", std::filesystem::copy_options::overwrite_existing);

	std::filesystem::remove_all(file_intermadeitae_data);
	std::filesystem::remove_all(file_in_data);

}

int do_hts_ali_one(std::string filepath) {
	std::stringstream command[2];

	command[0] << "hts_beta_ali_v2.exe " << filepath << "\\beta_thick_0.dat ali.json --th_angle2 0.02 --th_pos2 0.015";
	command[1] << "root_macro ali.json ali.pdf plot_hts_beta_ali";

	PROCESS_INFORMATION p;
	std::stringstream file_out_log;
	HANDLE h_out;
	//hts2beta start
	auto start = std::chrono::system_clock::now(); // 計測開始時間
	file_out_log << filepath << "\\log0.txt";
	p = do_prg(filepath, command[0].str(), file_out_log.str(), h_out);

	DWORD rc;
	WaitForSingleObject(p.hProcess, INFINITE);
	GetExitCodeProcess(p.hProcess, &rc);
	if (rc != 0) {
		TerminateProcess(p.hProcess, 0);
		CloseHandle(p.hProcess);
		CloseHandle(h_out);
		console_error_out(file_out_log.str());
		return 1;
	}

	p = do_prg(filepath, command[1].str(), file_out_log.str(), h_out);
	WaitForSingleObject(p.hProcess, INFINITE);
	GetExitCodeProcess(p.hProcess, &rc);
	if (rc != 0) {
		TerminateProcess(p.hProcess, 0);
		CloseHandle(p.hProcess);
		CloseHandle(h_out);
		console_error_out(file_out_log.str());
		return 1;
	}

	auto end = std::chrono::system_clock::now(); // 計測開始時間
	double elapsed = std::chrono::duration_cast<std::chrono::seconds>(end - start).count(); //処理に要した時間をミリ秒に変換

	printf("hts_ali time:%.0lf[s]\n", elapsed);
	return 0;
}

int do_hts2beta_one(std::string file_in_data, std::string file_out_data, int arg) {
	std::stringstream command;
	if (arg == 0)	command << "hts2beta_v2.exe " << file_out_data << "\\beta_thick_0.dat -alternate0 --outputformat 1 -removeoutofrange";
	else if (arg == 1)command << "hts2beta_v2.exe " << file_out_data << "\\beta_thick_1.dat -alternate1 --outputformat 1 -removeoutofrange";
	else if (arg == 2)	command << "hts2beta_v2.exe " << file_out_data << "\\beta_thin_0.dat -thin16_base --outputformat 1 -removeoutofrange";
	else if (arg == 3)	command << "hts2beta_v2.exe " << file_out_data << "\\beta_thin_1.dat -thin16_outer --outputformat 1 -removeoutofrange";
	else {
		fprintf(stderr, "arg error\n");
		exit(1);
	}
	PROCESS_INFORMATION p;
	std::stringstream file_out_log;
	HANDLE h_out;
	//hts2beta start
	auto hts2beta_start = std::chrono::system_clock::now(); // 計測開始時間
	file_out_log << file_out_data << "\\log" << arg << ".txt";
	p = do_prg(file_in_data, command.str(), file_out_log.str(), h_out);

	DWORD rc;
	WaitForSingleObject(p.hProcess, INFINITE);
	GetExitCodeProcess(p.hProcess, &rc);
	int flg = 0;
	if (rc != 0) {
		TerminateProcess(p.hProcess, 0);
		CloseHandle(p.hProcess);
		CloseHandle(h_out);
		console_error_out(file_out_log.str());
		return 1;

	}
	CloseHandle(p.hProcess);
	CloseHandle(h_out);

	auto hts2beta_end = std::chrono::system_clock::now(); // 計測開始時間
	double elapsed = std::chrono::duration_cast<std::chrono::seconds>(hts2beta_end - hts2beta_start).count(); //処理に要した時間をミリ秒に変換

	printf("hts2beta[%d] time:%.0lf[s]\n", arg, elapsed);
	return 0;
}
int do_hts_deadpixel_one(std::string filepath, int arg) {
	std::stringstream command;
	if (arg == 0)	command << "hts_beta_deadpixel_v2.exe " << filepath << "\\beta_thick_0.dat deadpixel.json";
	else if (arg == 1)command << "hts_beta_deadpixel_v2.exe " << filepath << "\\beta_thick_1.dat deadpixel.json";
	else if (arg == 2)	command << "hts_beta_deadpixel_v2.exe " << filepath << "\\beta_thin_0.dat deadpixel.json";
	else if (arg == 3)	command << "hts_beta_deadpixel_v2.exe " << filepath << "\\beta_thin_1.dat deadpixel.json";
	else {
		fprintf(stderr, "arg error\n");
		exit(1);
	}


	PROCESS_INFORMATION p;
	std::stringstream file_out_log;
	HANDLE h_out;
	//hts2beta start
	auto hts2beta_start = std::chrono::system_clock::now(); // 計測開始時間
	file_out_log << filepath << "\\log" << arg << ".txt";
	p = do_prg(filepath, command.str(), file_out_log.str(), h_out);

	DWORD rc;
	WaitForSingleObject(p.hProcess, INFINITE);
	GetExitCodeProcess(p.hProcess, &rc);
	int flg = 0;
	if (rc != 0) {
		TerminateProcess(p.hProcess, 0);
		CloseHandle(p.hProcess);
		CloseHandle(h_out);
		console_error_out(file_out_log.str());
		return 1;

	}
	CloseHandle(p.hProcess);
	CloseHandle(h_out);

	auto hts2beta_end = std::chrono::system_clock::now(); // 計測開始時間
	double elapsed = std::chrono::duration_cast<std::chrono::seconds>(hts2beta_end - hts2beta_start).count(); //処理に要した時間をミリ秒に変換

	printf("hts_deadpixel[%d] time:%.0lf[s]\n", arg, elapsed);
	return 0;
}
int do_hts_fvxx_one(std::string filepath, std::string &file_out_data, int pl, int arg) {
	std::stringstream out_file;
	out_file << filepath << "\\out" << arg;
	bool ret = std::filesystem::create_directories(out_file.str());
	file_out_data = out_file.str();

	std::stringstream command;
	std::string key;
	if (arg == 0)key = "thick_0";
	else if (arg == 1)key = "thick_1";
	else if (arg == 2)key = "thin_0";
	else if (arg == 3)key = "thin_1";
	else {
		fprintf(stderr, "arg error\n");
		exit(1);
	}
	command << "hts_beta_fvxx_v2.exe " << filepath << "\\beta_" << key << ".dat " << pl << " --directory " << out_file.str() << "\\ -numvola2pxpy ";

	PROCESS_INFORMATION p;
	std::stringstream file_out_log;
	HANDLE h_out;
	//hts2beta start
	auto hts2beta_start = std::chrono::system_clock::now(); // 計測開始時間
	file_out_log << filepath << "\\log" << arg << ".txt";
	p = do_prg(filepath, command.str(), file_out_log.str(), h_out);

	DWORD rc;
	WaitForSingleObject(p.hProcess, INFINITE);
	GetExitCodeProcess(p.hProcess, &rc);
	int flg = 0;
	if (rc != 0) {
		TerminateProcess(p.hProcess, 0);
		CloseHandle(p.hProcess);
		CloseHandle(h_out);
		console_error_out(file_out_log.str());
		return 1;

	}
	CloseHandle(p.hProcess);
	CloseHandle(h_out);

	auto hts2beta_end = std::chrono::system_clock::now(); // 計測開始時間
	double elapsed = std::chrono::duration_cast<std::chrono::seconds>(hts2beta_end - hts2beta_start).count(); //処理に要した時間をミリ秒に変換

	printf("hts_fvxx[%d] time:%.0lf[s]\n", arg, elapsed);
	//成功していたらbeta 削除
	std::filesystem::remove(filepath + "\\beta_" + key + ".dat ");

	return 0;
}
int do_f_filter_one(std::string file_in_data, std::string file_out_data, int pl, int arg) {
	std::stringstream command[2];
	std::string key;
	if (arg == 0)key = "_thick_0";
	else if (arg == 1)key = "_thick_1";
	else if (arg == 2)key = "_thin_0";
	else if (arg == 3)key = "_thin_1";
	else {
		fprintf(stderr, "arg error\n");
		exit(1);
	}

	command[0] << "f_filter " << pl * 10 + 1 << " "
		<< file_in_data << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << ".vxx --o "
		<< file_out_data << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << key << ".vxx"
		<< " --ghost-rl 5 5 0.01 0.01 0.05 5 --view 10000 1000";

	command[1] << "f_filter " << pl * 10 + 2 << " "
		<< file_in_data << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << ".vxx --o "
		<< file_out_data << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << key << ".vxx"
		<< " --ghost-rl 5 5 0.01 0.01 0.05 5 --view 10000 1000";

	PROCESS_INFORMATION p[2];
	std::stringstream file_out_log[2];
	HANDLE h_out[2];
	//hts2beta start
	auto start = std::chrono::system_clock::now(); // 計測開始時間
	file_out_log[0] << file_in_data << "\\log" << arg << "_0.txt";
	file_out_log[1] << file_in_data << "\\log" << arg << "_1.txt";
	p[0] = do_prg(file_in_data, command[0].str(), file_out_log[0].str(), h_out[0]);
	p[1] = do_prg(file_in_data, command[1].str(), file_out_log[1].str(), h_out[1]);

	DWORD rc;
	for (int i = 0; i < 2; i++) {
		WaitForSingleObject(p[i].hProcess, INFINITE);
		GetExitCodeProcess(p[i].hProcess, &rc);

		if (rc != 0) {
			for (int j = i; j < 2; j++) {
				TerminateProcess(p[j].hProcess, 0);
				CloseHandle(p[j].hProcess);
				CloseHandle(h_out[j]);
			}
			console_error_out(file_out_log[i].str());
			return 1;
		}
		CloseHandle(p[i].hProcess);
		CloseHandle(h_out[i]);
	}
	auto end = std::chrono::system_clock::now(); // 計測開始時間
	double elapsed = std::chrono::duration_cast<std::chrono::seconds>(end - start).count(); //処理に要した時間をミリ秒に変換

	printf("hts_f_filter[%d] time:%.0lf[s]\n", arg, elapsed);
	//成功していたらfvxx 削除
	std::stringstream del_file[2];
	del_file[0] << file_in_data << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << ".vxx";
	del_file[1] << file_in_data << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << ".vxx";
	std::filesystem::remove(del_file[0].str());
	std::filesystem::remove(del_file[1].str());

	return 0;

}
PROCESS_INFORMATION do_prg(std::string path, std::string command, std::string output_log, HANDLE &h_out) {
	SECURITY_ATTRIBUTES sec_attr;
	ZeroMemory(&sec_attr, sizeof(sec_attr));
	sec_attr.bInheritHandle = TRUE;

	//HANDLE h_out = CreateFile(TEXT("CONOUT$"), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, 0);
	//HANDLE h_err = CreateFile(TEXT("CONOUT$"), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, 0);
	h_out = CreateFile(output_log.c_str(), FILE_APPEND_DATA, FILE_SHARE_WRITE, &sec_attr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);


	PROCESS_INFORMATION p;
	STARTUPINFO s;

	ZeroMemory(&s, sizeof(s));
	s.cb = sizeof(s);
	s.dwFlags = STARTF_USESTDHANDLES;
	s.hStdOutput = h_out;
	s.hStdError = h_out;

	LPSTR str = const_cast<char *>(command.c_str());
	//printf("%s\n", command.c_str());
	int ret = CreateProcess(
		NULL, // 実行可能モジュールの名
		str, // コマンドラインの文字列
		&sec_attr, // セキュリティ記述子
		&sec_attr,// セキュリティ記述子
		TRUE, // ハンドルの継承オプション
		NORMAL_PRIORITY_CLASS, // 作成のフラグ
		NULL,// 新しい環境ブロック
		path.c_str(), // カレントディレクトリの名前
		&s, // スタートアップ情報
		&p // プロセス情報
	);
	if (!ret)
	{
		printf("miss %s\n", command.c_str());
		exit(1);
	}
	return p;

}



void console_error_out(std::string filename) {
	printf("%s\n", filename.c_str());
	std::ifstream ifs(filename);
	std::string str;

	if (ifs.fail()) {
		std::cerr << "Failed to open file." << std::endl;
	}

	std::cout << ":::::::::::::::::::Error message::::::::::::::" << std::endl;

	HANDLE hStdout;
	WORD wAttributes;
	CONSOLE_SCREEN_BUFFER_INFO csbi;//構造体です

	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(hStdout, &csbi);
	wAttributes = FOREGROUND_RED;
	SetConsoleTextAttribute(hStdout, wAttributes);

	while (getline(ifs, str)) {
		std::cout << str << std::endl;
	}
	wAttributes = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;
	SetConsoleTextAttribute(hStdout, wAttributes);
	std::cout << "::::::::::::::::::::::::::::::::::::::::::" << std::endl;

}
void do_ADAPT(std::string adapt_path, std::string in_mfile) {
	PROCESS_INFORMATION p;
	STARTUPINFO s;
	ZeroMemory(&s, sizeof(s));
	s.cb = sizeof(s);

	std::string command = adapt_path + " " + in_mfile;
	LPSTR str = const_cast<char *>(command.c_str());
	int ret = CreateProcess(
		NULL, // 実行可能モジュールの名
		str, // コマンドラインの文字列
		NULL, // セキュリティ記述子
		NULL,// セキュリティ記述子
		FALSE, // ハンドルの継承オプション
		NULL, // 作成のフラグ
		NULL,// 新しい環境ブロック
		NULL, // カレントディレクトリの名前
		&s, // スタートアップ情報
		&p // プロセス情報
	);
	if (!ret)
	{
		printf("miss %s\n", command.c_str());
	}
	else
	{
		CloseHandle(p.hThread);

		//メモ帳が終了するまで待つ
		WaitForSingleObject(p.hProcess, INFINITE);
		CloseHandle(p.hProcess);
	}

}
