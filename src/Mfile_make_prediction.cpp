#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>


std::vector<mfile0::M_Chain> Chain_Selection(std::vector<mfile0::M_Chain> chain, int PL);
std::vector<netscan::base_track_t> make_prediction(std::vector<mfile0::M_Chain> chain, std::string file_base_path, int prediction_pl, int extra_pl, corrmap0::Corrmap param);

	void Fitting(double x[4], double y[4], double &slope, double &intercept);
	std::vector<netscan::base_track_t> base_all_trans0(std::vector<netscan::base_track_t> base, std::vector<corrmap0::Corrmap>corr, double gap_nominal,int pl);
	std::vector<netscan::base_track_t> base_all_trans1(std::vector<netscan::base_track_t> base, std::vector<corrmap0::Corrmap>corr, double gap_nominal,int pl);


int main(int argc, char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-mfile file-area-path predictionPL extraPL out-bvxx-vxx\n");
		exit(1);
	}

	//input value
	std::string in_file_mfile = argv[1];
	std::string file_area_path = argv[2];
	int prediction_pl = std::stoi(argv[3]);
	int extra_pl = std::stoi(argv[4]);
	std::string out_file_base = argv[5];

	//mfile
	mfile0::Mfile m;

	//read mfile
	mfile0::read_mfile(in_file_mfile, m);

	//ChainInfの書き出し
	//std::vector<mfile0::M_Chain_inf> c_inf;
	//for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
	//	c_inf.push_back(mfile0::chain2inf(*itr));
	//}
	//mfile0::write_chaininf("chain_inf.txt", c_inf);

	//make prediction
	////chain selection
	m.chains = Chain_Selection(m.chains, prediction_pl);

	mfile0::write_mfile("m.sel.all", m);

	std::vector<corrmap0::Corrmap> corr;
	std::string file_in_corr_abs;
	{
		std::stringstream ss;
		ss << file_area_path << "\\0\\align\\corrmap-abs.lst";
		file_in_corr_abs = ss.str();
	}
	corrmap0::read_cormap(file_in_corr_abs, corr);
	corrmap0::Corrmap param;
	for (int i = 0; i < corr.size(); i++) {
		if (corr[i].pos[0] / 10 == extra_pl) param = corr[i];
	}

	//extraPLでのprediction作成
	std::vector<netscan::base_track_t> base = make_prediction(m.chains, file_area_path, prediction_pl, extra_pl, param);
	//netscan::write_basetrack_txt("b007_pred.txt", base);
	//readcorrmap
	corr.clear();
	std::string file_in_align;
	{
		std::stringstream ss;
		ss << file_area_path << "\\0\\align\\corrmap-align-" << std::setfill('0') << std::setw(3) << std::min(extra_pl, prediction_pl) << "-" << std::setfill('0') << std::setw(3) << std::max(extra_pl, prediction_pl) << ".lst";
		file_in_align = ss.str();
	}
	corrmap0::read_cormap(file_in_align, corr);

	//basetrack trans
	if (extra_pl < prediction_pl) {
		base = base_all_trans1(base, corr, 820,prediction_pl);
	}
	else {
		base = base_all_trans0(base, corr, 820,prediction_pl);
	}
	netscan::write_basetrack_vxx(out_file_base, base, prediction_pl, 0);
	//angle distribution

}


std::vector<mfile0::M_Chain> Chain_Selection(std::vector<mfile0::M_Chain> chain, int PL) {
	std::vector<mfile0::M_Chain> sel0;

	//targetPLの上下にPLがあるか確認
	int count = 0;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		count = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (abs(itr2->pos / 10 - PL )== 1)count++;
			if (abs(itr2->pos / 10 - PL) == 2)count++;
		}
		if (count==4) {
			sel0.push_back(*itr);
		}
	}
	fprintf(stderr, "targetPL 2 exist %d --> %d(%4.1lf%%)\n", chain.size(), sel0.size(), sel0.size()*100. / chain.size());

	//nsegカット
	std::vector<mfile0::M_Chain> sel1;
	for (auto itr = sel0.begin(); itr != sel0.end(); itr++) {
		if (itr->nseg>=5){
			sel1.push_back(*itr);
		}
	}
	fprintf(stderr, "nseg cut >=5 %d --> %d(%4.1lf%%)\n", sel0.size(), sel1.size(), sel1.size()*100. / sel0.size());

	//lateral角度ずれカット
	std::vector<mfile0::M_Chain> sel2;
	for (auto itr = sel1.begin(); itr != sel1.end(); itr++) {
		if (mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(*itr), mfile0::chain_ay(*itr)) < 0.005) {
			sel2.push_back(*itr);
		}
	}
	fprintf(stderr, "cut lateral deviation < 0.005 %d --> %d(%4.1lf%%)\n", sel1.size(), sel2.size(), sel2.size()*100. / sel1.size());

	//group1本化角度ずれカット
	std::vector<mfile0::M_Chain> sel3;
	std::map<int,mfile0::M_Chain> group_map;
	for (auto itr = sel2.begin(); itr != sel2.end(); itr++) {
		int gid = itr->basetracks.begin()->group_id;
		auto res = group_map.insert(std::make_pair(gid, *itr));
		if (!res.second) {
			//同一keyを持った要素が存在する場合
			if (res.first->second.nseg > itr->nseg)continue;
			else if (res.first->second.nseg < itr->nseg) res.first->second = *itr;
			else {
				double dlat[2];
				dlat[0] = mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(res.first->second), mfile0::chain_ay(res.first->second));
				dlat[1] = mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(*itr), mfile0::chain_ay(*itr));
				if (dlat[0] > dlat[1]) res.first->second = *itr;
				else continue;
			}
		}
	}
	for (auto itr = group_map.begin(); itr != group_map.end(); itr++) {
		sel3.push_back(itr->second);
	}

	fprintf(stderr, "cut group convolution %d --> %d(%4.1lf%%)\n", sel2.size(), sel3.size(), sel3.size()*100. / sel2.size());

	return sel3;

}

std::vector<netscan::base_track_t> make_prediction(std::vector<mfile0::M_Chain> chain, std::string file_base_path, int prediction_pl, int extra_pl, corrmap0::Corrmap param) {
	std::vector<netscan::base_track_t> ret;
	std::vector<netscan::base_track_t> base;

	std::string file_in_base;
	{
		std::stringstream ss;
		ss << file_base_path << "\\PL" << std::setfill('0') << std::setw(3) << extra_pl << "\\b" << std::setfill('0') << std::setw(3) << extra_pl << ".sel.cor.vxx";
		file_in_base = ss.str();
	}
	netscan::read_basetrack_extension(file_in_base, base, extra_pl, 0);
	std::map<int, netscan::base_track_t> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_map.insert(std::make_pair(itr->rawid, *itr));
	}

	double x[4], y[4], z[4];
	int count = 0;
	int rawid = 0;

	//predictionの作成
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		count = 0;
		for (int i = 0; i < 4; i++) {
			x[i] = 0;
			y[i] = 0;
			z[i] = 0;
		}
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (abs(itr2->pos / 10 - prediction_pl) == 2 || abs(itr2->pos / 10 - prediction_pl) == 1) {
				x[count] = itr2->x;
				y[count] = itr2->y;
				z[count] = itr2->z;
				count++;
			}
			if (itr2->pos / 10 == extra_pl) {
				rawid = itr2->rawid;
			}
		}

		if (count < 4)continue;

		double slope_x, intercept_x;
		Fitting(z, x, slope_x, intercept_x);
		double slope_y, intercept_y;
		Fitting(z, y, slope_y, intercept_y);

		auto res = base_map.find(rawid);
		if (res == base_map.end()) {
			fprintf(stderr, "not exist pl=%d rawid=%d\n", extra_pl, rawid);
			exit(1);
		}
		netscan::base_track_t base_tmp = res->second;
		double delta = param.angle[0] * param.angle[3] - param.angle[1] * param.angle[2];
		slope_x = slope_x - param.angle[4];
		slope_y = slope_y - param.angle[5];
		base_tmp.ax = (param.angle[3] * slope_x - param.angle[1] * slope_y) / delta;
		base_tmp.ay = (param.angle[0] * slope_y - param.angle[2] * slope_x) / delta;
		base_tmp.dmy = itr->chain_id;

		for (int i = 0; i < 2; i++) {
			base_tmp.m[i].ax = base_tmp.ax;
			base_tmp.m[i].ay = base_tmp.ay;
		}


		ret.push_back(base_tmp);
	}

	return ret;
}
void Fitting(double x[4], double y[4], double &slope, double &intercept) {

	double x1 = 0;
	double y1 = 0;
	double x2 = 0;
	double xy = 0;
	double n = 4;

	for (int i = 0; i < n; i++) {
		x1 += x[i];
		x2 += x[i] * x[i];
		y1 += y[i];
		xy += x[i] * y[i];
	}
	intercept = (x2*y1 - x1 * xy) / (n*x2 - x1 * x1);
	slope = (n*xy - x1 * y1) / (n*x2 - x1 * x1);
	return;

}

std::vector<netscan::base_track_t> base_all_trans0(std::vector<netscan::base_track_t> base, std::vector<corrmap0::Corrmap>corr, double gap_nominal,int pl) {
	//PL大のbasetrack-->PL小のbasetrackへ外挿、変換

	//corrmapでbaseのtrackを逆変換
	//位置角度で外挿

	double ini_x, ini_y, step, tmpi[2];
	std::map<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	//corrmapの設定
	int flg[3] = { 0,0,0 };
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		corr_map.insert(std::make_pair(std::make_pair(itr->notuse_i[1], itr->notuse_i[2]), *itr));
		//itr->notuse_i[1]:ix
		//itr->notuse_i[2]:iy
		if (flg[0] == 0 && itr->notuse_i[1] > 0) {
			ini_x = itr->areax[0];
			tmpi[0] = itr->notuse_i[1];
			flg[0] = 1;
		}
		if (flg[1] == 0 && itr->notuse_i[2] > 0) {
			ini_y = itr->areay[0];
			tmpi[1] = itr->notuse_i[2];
			flg[1] = 1;
		}
		if (flg[2] == 0 && itr + 1 != corr.end() && itr->notuse_i[1] > 0 && (itr + 1)->notuse_i[1] - itr->notuse_i[1] == 1) {
			step = (itr + 1)->areax[0] - itr->areax[0];
			flg[2] = 1;
		}
	}
	if (flg[0] == 0 || flg[1] == 0 || flg[2] == 0) {
		fprintf(stderr, "corrmap initialize exception\n");
		fprintf(stderr, "flg[0]=%d\n", flg[0]);
		fprintf(stderr, "flg[1]=%d\n", flg[1]);
		fprintf(stderr, "flg[2]=%d\n", flg[2]);
		exit(1);
	}
	ini_x = ini_x - step * tmpi[0];
	ini_y = ini_y - step * tmpi[1];

	//prediction作成
	std::vector<netscan::base_track_t> ret;
	int ix, iy, dist;
	bool flg_find = true;
	corrmap0::Corrmap param;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ix = (itr->x - ini_x) / step;
		iy = (itr->y - ini_y) / step;
		if (ix < 0)ix = 0;
		if (iy < 0)iy = 0;
		dist = 0;
		flg_find = true;
		//corrmap探索
		while (flg_find) {
			for (int ix_add = -1 * dist; ix_add <= dist; ix_add++) {
				if (ix + ix_add < 0)continue;
				for (int iy_add = -1 * (dist - abs(ix_add)); iy_add <= (dist - abs(ix_add)); iy_add++) {
					auto res = corr_map.find(std::make_pair(ix + ix_add, iy + iy_add));
					if (res == corr_map.end())continue;
					if (!flg_find) {
						//同じdistで2個以上候補があった時は中心距離の近いほうにする。
						double tmp_dist[2];
						tmp_dist[0] = pow(itr->x - (param.areax[0] + param.areax[1]) / 2, 2) + pow(itr->y - (param.areay[0] + param.areay[1]) / 2, 2);
						tmp_dist[1] = pow(itr->x - (res->second.areax[0] + res->second.areax[1]) / 2, 2) + pow(itr->y - (res->second.areay[0] + res->second.areay[1]) / 2, 2);
						if (tmp_dist[0] > tmp_dist[1])param = res->second;
					}
					else {
						param = res->second;
						flg_find = false;
					}

				}
			}
			dist++;
		}
		//basetrack変換
		netscan::base_track_t base_tmp = netscan::base_trans(*itr, param);
		base_tmp.x = base_tmp.x + base_tmp.ax*(gap_nominal - param.dz);
		base_tmp.y = base_tmp.y + base_tmp.ay*(gap_nominal - param.dz);
		//base_tmp.dmy = itr->dmy;
		base_tmp.pl = pl;
		base_tmp.m[0].pos = pl * 10 + 1;
		base_tmp.m[1].pos = pl * 10 + 2;
		ret.push_back(base_tmp);
	}
	return ret;
}
std::vector<netscan::base_track_t> base_all_trans1(std::vector<netscan::base_track_t> base, std::vector<corrmap0::Corrmap>corr, double gap_nominal,int pl) {
	//PL小のbasetrack-->PL大のbasetrackへ外挿、変換
	//corrmapを逆変換して使用するcorrmapを決定
	//corrmapでbaseのtrackを変換
	//位置角度で外挿

	double  step;
	//corrmapの設定
	int flg = 0;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		//itr->notuse_i[1]:ix
		//itr->notuse_i[2]:iy
		if (flg == 0 && itr + 1 != corr.end() && itr->notuse_i[1] > 0 && (itr + 1)->notuse_i[1] - itr->notuse_i[1] == 1) {
			step = (itr + 1)->areax[0] - itr->areax[0];
			flg = 1;
		}
		//逆変換
		double delta = itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2];
		double x, y;
		x = (itr->areax[0] + itr->areax[1]) / 2 - itr->position[4];
		y = (itr->areay[0] + itr->areay[1]) / 2 - itr->position[5];
		//notuse_d[0],[1] 逆変換後の中心座標(x,y)
		itr->notuse_d[0] = (itr->position[3] * x - itr->position[1] * y) / delta;
		itr->notuse_d[1] = (itr->position[0] * y - itr->position[2] * x) / delta;
	}
	if (flg == 0) {
		fprintf(stderr, "corrmap initialize exception\n");
		exit(1);
	}

	double min[2] = { corr.begin()->notuse_d[0],corr.begin()->notuse_d[1] };
	double max[2] = { corr.begin()->notuse_d[0],corr.begin()->notuse_d[1] };
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		min[0] = std::min(min[0], itr->notuse_d[0]);
		min[1] = std::min(min[1], itr->notuse_d[1]);
		max[0] = std::max(max[0], itr->notuse_d[0]);
		max[1] = std::max(max[1], itr->notuse_d[1]);
	}
	int n_x = (max[0] - min[0]) / step + 3;
	int n_y = (max[1] - min[1]) / step + 3;
	std::vector<std::vector<std::vector<corrmap0::Corrmap>>> corr_map;
	for (int i = 0; i < n_x; i++) {
		std::vector < std::vector<corrmap0::Corrmap> >corr_tmp;
		for (int j = 0; j < n_y; j++) {
			std::vector<corrmap0::Corrmap> corr_v;
			corr_tmp.push_back(corr_v);
		}
		corr_map.push_back(corr_tmp);
	}
	int ix, iy;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		ix = (itr->notuse_d[0] - min[0]) / step;
		iy = (itr->notuse_d[1] - min[1]) / step;
		//		if (ix < 0)ix = 0;
		//		if (iy < 0)iy = 0;
		for (int ix_add = -5; ix_add <= 5; ix_add++) {
			if (ix + ix_add < 0 || n_x <= ix + ix_add)continue;
			for (int iy_add = -5; iy_add <= 5; iy_add++) {
				if (iy + iy_add < 0 || n_y <= iy + iy_add)continue;
				corr_map[ix + ix_add][iy + iy_add].push_back(*itr);
			}
		}
	}

	//prediction作成
	std::vector<netscan::base_track_t> ret;
	double dist;
	corrmap0::Corrmap param;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ix = (itr->x - min[0]) / step;
		iy = (itr->y - min[1]) / step;
		if (ix < 0)ix = 0;
		if (iy < 0)iy = 0;

		if (corr_map[ix][iy].size() == 0) {
			fprintf(stderr, "exception:corrmap not found\n");
			exit(1);
		}
		dist = pow(corr_map[ix][iy].begin()->notuse_d[0] - itr->x, 2) + pow(corr_map[ix][iy].begin()->notuse_d[1] - itr->y, 2);
		param = *corr_map[ix][iy].begin();
		for (auto itr_c = corr_map[ix][iy].begin(); itr_c != corr_map[ix][iy].end(); itr_c++) {
			if (dist > pow(itr_c->notuse_d[0] - itr->x, 2) + pow(itr_c->notuse_d[1] - itr->y, 2)) {
				dist = pow(itr_c->notuse_d[0] - itr->x, 2) + pow(itr_c->notuse_d[1] - itr->y, 2);
				param = *itr_c;
			}
		}
		//distは2mm*sqrt(2)よりも小さくなるはず
		//printf("dist = %10.2lf", dist);
		//basetrack変換
		netscan::base_track_t base_tmp = *itr;
		base_tmp.x = base_tmp.x - base_tmp.ax*(gap_nominal - param.dz);
		base_tmp.y = base_tmp.y - base_tmp.ay*(gap_nominal - param.dz);

		base_tmp = netscan::base_trans_inv(base_tmp, param);
		//printf("dist = %10.2lf\n", sqrt(dist));
		//printf("%5.4lf %5.4lf %8.1lf %8.1lf\n", itr->ax, itr->ay, itr->x, itr->y);
		//printf("%5.4lf %5.4lf %8.1lf %8.1lf\n", base_tmp.ax, base_tmp.ay, base_tmp.x, base_tmp.y);
		//base_tmp.dmy = itr->dmy;
		base_tmp.pl = pl;
		base_tmp.m[0].pos = pl * 10+1;
		base_tmp.m[1].pos = pl * 10+2;
		ret.push_back(base_tmp);
	}

	return ret;
}
