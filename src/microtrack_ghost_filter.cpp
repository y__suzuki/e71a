#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>
#include <omp.h>

struct micro_ghost {
	netscan::micro_track_t *m;
	int flg;
};
void microtrack_area(std::string filename, int pos, int zone, double &x_min, double &x_max, double &y_min, double &y_max);
void Read_fvxx_area(std::string filename, int pos, int zone, std::vector<vxx::micro_track_t> &micro, double x_min, double x_max, double y_min, double y_max);
void ghost_filter(std::vector<vxx::micro_track_t>&micro, std::set<std::tuple<int, int, int>>&output);
void fvxx_area(std::string filename, int pos, int zone, double &x_min, double &x_max, double &y_min, double &y_max);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in_fvxx pos zone out_fvxx\n");
		exit(1);
	}
	std::string file_in_fvxx = argv[1];
	int pos = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_fvxx = argv[4];
	std::set<std::tuple<int,int,int>> output;

	double x_min, x_max, y_min, y_max;
	fvxx_area(file_in_fvxx, pos, zone, x_min, x_max, y_min, y_max);
	//x_min = 0;
	//y_min = 0;
	//x_max = 100000;
	//y_max = 100000;
	int divide = 5;
	int step = (x_max - x_min) / divide;
	int overrap = 500;
	double x, y;
	int count = 0;
	for (x = x_min; x <= x_max + overrap; x += step) {
		for (y = y_min; y <= y_max + overrap; y += step) {
			printf("count %d \n", count);
			//printf("x: %8.1lf %8.1lf  y: %8.1lf %8.1lf \n", x - overrap, x + step + overrap, y - overrap, y + step + overrap);
			count++;
			std::vector<vxx::micro_track_t> micro;
			Read_fvxx_area(file_in_fvxx, pos, zone, micro, x - overrap, x + step + overrap, y - overrap, y + step + overrap);
			if (micro.size() == 0)continue;
			ghost_filter(micro, output);
		}
	}

	std::vector<vxx::micro_track_t> micro;
	micro.reserve(1000 * 1000 * 1000 * 1);
	vxx::FvxxReader fr;
	int64_t cnt = 0;
	if (fr.Begin(file_in_fvxx, pos, zone))
	{
		vxx::HashEntry h;
		vxx::micro_track_t m;
		while (fr.NextHashEntry(h))
		{
			while (fr.NextMicroTrack(m))
			{
				if (cnt % 1000000 == 0) {
					fprintf(stderr, "\r fvxx read ...%d", cnt);
				}
				cnt++;
				if (output.count(std::make_tuple(m.col, m.row, m.isg)) == 1) {
					micro.push_back(m);
				}
			}
		}
		fr.End();
	}
	fprintf(stderr, "\r fvxx read ...%d\n", cnt);
	fprintf(stderr, "ghost filter %lld --> %lld\n", cnt, micro.size());

	vxx::FvxxWriter w;
	w.Write(file_out_fvxx, pos, zone, micro);

}
void fvxx_area(std::string filename, int pos, int zone, double &x_min, double &x_max, double &y_min, double &y_max) {
	int64_t count = 0;
	vxx::FvxxReader fr;
	std::vector<vxx::CutArea> area;
	if (fr.Begin(filename, pos, zone))
	{
		vxx::HashEntry h;
		vxx::micro_track_t m;
		while (fr.NextHashEntry(h))
		{
			while (fr.NextMicroTrack(m))
			{
				if (count % 1000000 == 0) {
					fprintf(stderr, "\r count %lld", count);
				}
				count++;
				if (count == 0) {
					x_min = m.x;
					x_max = m.x;
					y_min = m.y;
					y_max = m.y;
				}
				x_min = std::min(x_min, m.x);
				x_max = std::max(x_max, m.x);
				y_min = std::min(y_min, m.y);
				y_max = std::max(y_max, m.y);
			}
		}
		fr.End();
		fprintf(stderr, "\r count %lld\n", count);
	}
}
void Read_fvxx_area(std::string filename, int pos, int zone, std::vector<vxx::micro_track_t> &micro, double x_min, double x_max, double y_min, double y_max) {
	micro.clear();
	vxx::FvxxReader fr;
	std::vector<vxx::CutArea> area;
	area.push_back(vxx::CutArea(x_min, x_max, y_min, y_max));//xmin, xmax, ymin, ymax
	micro = fr.ReadAll(filename, pos, zone, vxx::opt::a = area);
}
void microtrack_area(std::string filename, int pos, int zone, double &x_min, double &x_max, double &y_min, double &y_max) {
	vxx::FvxxReader fr;
	int64_t count = 0;
	if (fr.Begin(filename, pos, zone))
	{
		vxx::HashEntry h;
		vxx::micro_track_t f;
		while (fr.NextHashEntry(h))
		{
			while (fr.NextMicroTrack(f))
			{
				if (count == 0) {
					x_min = f.x;
					x_max = f.x;
					y_min = f.y;
					y_max = f.y;
				}
				x_min = std::min(x_min, f.x);
				x_max = std::max(x_max, f.x);
				y_min = std::min(y_min, f.y);
				y_max = std::max(y_max, f.y);

				if (count % 1000000 == 0) {
					fprintf(stderr, "\r area search... %12lld", count);
				}
				count++;
			}
		}
		fr.End();
	}
	fprintf(stderr, "\r area search... %12lld fin\n", count);
}
void ghost_filter(std::vector<vxx::micro_track_t>&micro,std::set<std::tuple<int,int,int>>&output) {
	double x_min, y_min, ax_min, ay_min;
	double  hash_area = 50, hash_angle = 0.05;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		itr->px = 0;
		itr->py = 0;
		if (itr == micro.begin()) {
			x_min = itr->x;
			y_min = itr->y;
			ax_min = itr->ax;
			ay_min = itr->ay;
		}
		x_min = std::min(x_min, itr->x);
		y_min = std::min(y_min, itr->y);
		ax_min = std::min(ax_min, itr->ax);
		ay_min = std::min(ay_min, itr->ay);
	}
	std::tuple<int, int, int, int> id;
	std::multimap < std::tuple<int,int,int,int>, vxx::micro_track_t* > micro_map;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		std::get<0>(id) = (itr->x - x_min) / hash_area;
		std::get<1>(id) = (itr->y - y_min) / hash_area;
		std::get<2>(id) = (itr->ax - ax_min) / hash_angle;
		std::get<3>(id) = (itr->ay - ay_min) / hash_angle;
		micro_map.insert(std::make_pair(id, &(*itr)));
	}

	//flg=0 ���T��
	//flg=1 signal(high PH)
	//flg=2 ghost (low PH)
	int64_t count = 0;
#pragma omp parallel for private(id)
	for (int i = 0; i < micro.size(); i++) {
#pragma omp atomic
		count++;
		if (count % 100000 == 0) {
			fprintf(stderr, "\r ghost filter ...%d/%d(%4.1lf%%)", count, micro.size(), count*100. / micro.size());
		}
		if (micro[i].px >= 0.9)continue;
		micro[i].px = 1;
		int iix_range[2], iiy_range[2], iiax_range[2], iiay_range[2];
		double angle;
		angle = sqrt(micro[i].ax*micro[i].ax + micro[i].ay*micro[i].ay);
		double dr_pos = angle * 10 + 2;
		double dr_ang = angle * 0.05 + 0.1;
		double dl_pos = 1.5;
		double dl_ang = 0.07;

		iix_range[0] = ((micro[i].x - x_min) - dr_pos) / hash_area;
		iix_range[1] = ((micro[i].x - x_min) + dr_pos) / hash_area;
		iiy_range[0] = ((micro[i].y - y_min) - dr_pos) / hash_area;
		iiy_range[1] = ((micro[i].y - y_min) + dr_pos) / hash_area;
		iiax_range[0] = ((micro[i].ax - ax_min) - dr_ang) / hash_angle;
		iiax_range[1] = ((micro[i].ax - ax_min) + dr_ang) / hash_angle;
		iiay_range[0] = ((micro[i].ay - ay_min) - dr_ang) / hash_angle;
		iiay_range[1] = ((micro[i].ay - ay_min) + dr_ang) / hash_angle;
		//printf("%d %d %d %d %d %d %d %d\n", iix_range[0], iix_range[1], iiy_range[0], iiy_range[1], iiax_range[0], iiax_range[1], iiay_range[0], iiay_range[1]);
		for (int iix = iix_range[0]; iix <= iix_range[1]; iix++) {
			for (int iiy = iiy_range[0]; iiy <= iiy_range[1]; iiy++) {
				for (int iiax = iiax_range[0]; iiax <= iiax_range[1]; iiax++) {
					for (int iiay = iiay_range[0]; iiay <= iiay_range[1]; iiay++) {
						std::get<0>(id) = iix;
						std::get<1>(id) = iiy;
						std::get<2>(id) = iiax;
						std::get<3>(id) = iiay;

						if (micro_map.count(id) == 0)continue;
						else if (micro_map.count(id) == 1) {
							auto res = micro_map.find(id);
							if (fabs((res->second->ax - micro[i].ax)*micro[i].ay - (res->second->ay - micro[i].ay)*micro[i].ax) > dl_ang*angle)continue;
							if (fabs((res->second->x - micro[i].x)*micro[i].ay - (res->second->y - micro[i].y)*micro[i].ax) > dl_pos*angle)continue;
							if (fabs((res->second->ax - micro[i].ax)*micro[i].ax + (res->second->ay - micro[i].ay)*micro[i].ay) > dr_ang*angle)continue;
							if (fabs((res->second->x - micro[i].x)*micro[i].ax + (res->second->y - micro[i].y)*micro[i].ay) > dr_pos*angle)continue;
							if (res->second->rawid == micro[i].rawid)continue;
							micro[i].py++;
							if (micro[i].ph > res->second->ph) {
								res->second->px = 2;
							}
							else if(micro[i].ph < res->second->ph){
								micro[i].px = 2;
							}
							else {
								if (res->second->px >=0.9&&res->second->px<=1.1) {
									micro[i].px = 2;
								}
							}
						}
						else {
							auto range = micro_map.equal_range(id);
							for (auto res = range.first; res != range.second; res++) {
								if (fabs((res->second->ax - micro[i].ax)*micro[i].ay - (res->second->ay - micro[i].ay)*micro[i].ax) > dl_ang*angle)continue;
								if (fabs((res->second->x - micro[i].x)*micro[i].ay - (res->second->y - micro[i].y)*micro[i].ax) > dl_pos*angle)continue;
								if (fabs((res->second->ax - micro[i].ax)*micro[i].ax + (res->second->ay - micro[i].ay)*micro[i].ay) > dr_ang*angle)continue;
								if (fabs((res->second->x - micro[i].x)*micro[i].ax + (res->second->y - micro[i].y)*micro[i].ay) > dr_pos*angle)continue;
								if (res->second->rawid == micro[i].rawid)continue;
								micro[i].py++;
								if (micro[i].ph > res->second->ph) {
									res->second->px = 2;
								}
								else if (micro[i].ph < res->second->ph) {
									micro[i].px = 2;
								}
								else {
									if (res->second->px >= 0.9&&res->second->px <= 1.1) {
										micro[i].px = 2;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	fprintf(stderr, "\r ghost filter ...%d/%d(%4.1lf%%)\n", count, micro.size(), count*100. / micro.size());

	count = 0;
	for (auto itr = micro.begin(); itr != micro.end();itr++){
		//printf("%lf %lf\n", itr->px, itr->py);
		if (itr->px  <0.9) {
			fprintf(stderr, "exception not searched track\n");
			exit(1);
		}
		else if (itr->px >= 0.9&&itr->px<=1.1) {
			output.insert(std::make_tuple(itr->col, itr->row, itr->isg));
			count++;
		}
	}


	fprintf(stderr, "ghost filter %d --> %d(%4.1lf%%)\n", micro.size(), count, count*100. / micro.size());
}
