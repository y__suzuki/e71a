#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

void fvxx_merge(std::vector<vxx::micro_track_t>&micro1, std::vector<vxx::micro_track_t>&micro2);
void ShotID_trans(std::vector<vxx::micro_track_t>&micro, int NumberOfImager, int scanNUM, int scanID);
int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in_fvxx1 in_fvxx2 pos zone out_fvxx\n");
		exit(1);
	}
	std::string file_in_fvxx1 = argv[1];
	std::string file_in_fvxx2 = argv[2];
	int pos = std::stoi(argv[3]);
	int zone = std::stoi(argv[4]);
	std::string file_out_fvxx = argv[5];

	vxx::FvxxReader br;
	//ReadAll関数はbvxx中のBaseTrackを一度にすべて読みだす。
	//引数にはbvxxへのパス、pl、zoneを渡す。
	//ファイルを開けなかった場合などは空のvectorが返ってくる。
	std::vector<vxx::micro_track_t> micro1 = br.ReadAll(file_in_fvxx1, pos, zone);
	fprintf(stderr, "num of basetrack %lld\n", micro1.size());
	micro1.reserve(micro1.size() * 2);

	std::vector<vxx::micro_track_t> micro2 = br.ReadAll(file_in_fvxx2, pos, zone);
	fprintf(stderr, "num of basetrack %lld\n", micro2.size());

	//micro1 = ph_cut(micro1);
	//micro2 = ph_cut(micro2);
	if (micro1.size() + micro2.size() >= 2147483647) {
		printf("exception rawid overflow\n");
		exit(1);
	}

	ShotID_trans(micro1, 72, 2, 0);
	ShotID_trans(micro2, 72, 2, 1);
	fvxx_merge(micro1, micro2);
	
	vxx::FvxxWriter w;
	w.Write(file_out_fvxx, pos, zone, micro1);
}
void ShotID_trans(std::vector<vxx::micro_track_t>&micro, int NumberOfImager, int scanNUM, int scanID) {
	if (scanNUM <= scanID) {
		printf("scanNum(%d)<=ScanID(%d)\n", scanNUM, scanID);
		exit(1);
	}
	uint32_t ShotID;
	int ViewID = 0, ImagerID = 0;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		ShotID = ((uint32_t)(uint16_t)itr->row << 16) | ((uint32_t)(uint16_t)itr->col);
		ViewID = ShotID / NumberOfImager;
		ImagerID = ShotID % NumberOfImager;

		ShotID = ViewID * (NumberOfImager*scanNUM) + (NumberOfImager*scanID) + ImagerID;
		itr->col = (int16_t)(ShotID & 0x0000ffff);
		itr->row = (int16_t)((ShotID & 0xffff0000) >> 16);
	}
}
void fvxx_merge(std::vector<vxx::micro_track_t>&micro1, std::vector<vxx::micro_track_t>&micro2) {
	printf("fvxx merge (micro1)%d +(micro2)%d --> ", micro1.size(), micro2.size());

	std::vector<netscan::micro_track_t> ret;
	int64_t count = 0;
	for (auto itr = micro1.begin(); itr != micro1.end(); itr++) {
		itr->rawid = count;
		count++;
	}
	for (auto itr = micro2.begin(); itr != micro2.end(); itr++) {
		itr->rawid = count;
		count++;
		micro1.push_back(*itr);
	}
	micro2.clear();
	micro2.shrink_to_fit();
	printf("%lld\n", micro1.size());
}
