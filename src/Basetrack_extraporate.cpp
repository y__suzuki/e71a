#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <set>
std::vector<vxx::base_track_t> base_all_trans_1to0(std::vector<vxx::base_track_t> base, std::vector<corrmap0::Corrmap>corr, double gap_nominal, int pl);
std::vector<vxx::base_track_t> base_all_trans_0to1(std::vector<vxx::base_track_t> base, std::vector<corrmap0::Corrmap>corr, double gap_nominal, int pl);

std::set<int> read_rawid_list(std::string filename);
std::vector<vxx::base_track_t> basetrack_selection(std::vector<vxx::base_track_t> &base, std::set<int> list);
vxx::base_track_t base_trans(vxx::base_track_t base, corrmap0::Corrmap corr);
vxx::base_track_t base_trans_inv(vxx::base_track_t base, corrmap0::Corrmap corr);
void write_bvxx(std::string file_path, std::vector<vxx::base_track_t> &base, int pl);

int main(int argc, char **argv) {
	if (argc != 9) {
		fprintf(stderr, "usage:prg in-bvxx pl0 pl1 gap corrmap rawid-list out-bvxx-folder pl0-base-out\n");
		fprintf(stderr, "gap is absolute value\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl0 = std::stoi(argv[2]);
	int pl1 = std::stoi(argv[3]);
	double gap = std::stod(argv[4]);
	std::string file_in_corrmap = argv[5];
	std::string file_in_rawid = argv[6];
	std::string file_out_path = argv[7];
	bool pl0_output_flg = std::stoi(argv[8]);

	std::set<int> raw_list = read_rawid_list(file_in_rawid);

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl0, 0);
	base = basetrack_selection(base, raw_list);
	if (pl0_output_flg) {
		write_bvxx(file_out_path, base, pl0);
	}

	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corrmap, corr);

	if (pl0 < pl1) {
		base = base_all_trans_0to1(base, corr, gap, pl1);
	}
	else {
		base = base_all_trans_1to0(base, corr, gap, pl1);
	}

	write_bvxx(file_out_path, base, pl1);
}
std::set<int> read_rawid_list(std::string filename) {
	std::ifstream ifs(filename);
	std::set<int> ret;
	
	int rawid;
	while (ifs >> rawid) {
		ret.insert(rawid);
	}
	return ret;

}
std::vector<vxx::base_track_t> basetrack_selection(std::vector<vxx::base_track_t> &base, std::set<int> list) {
	std::vector<vxx::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (list.count(itr->rawid) == 1) {
			ret.push_back(*itr);
		}
	}
	return ret;

}
std::vector<vxx::base_track_t> base_all_trans_1to0(std::vector<vxx::base_track_t> base, std::vector<corrmap0::Corrmap>corr, double gap_nominal, int pl) {
	//PL大のbasetrack-->PL小のbasetrackへ外挿、変換
	//netscanで通常の処理

	//1:corrmapのarea逆変換
	//2:corrmapで変換
	//3:外挿

	double  step;
	//corrmapの設定
	int flg = 0;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		//itr->notuse_i[1]:ix
		//itr->notuse_i[2]:iy
		if (flg == 0 && itr + 1 != corr.end() && itr->notuse_i[1] > 0 && (itr + 1)->notuse_i[1] - itr->notuse_i[1] == 1) {
			step = (itr + 1)->areax[0] - itr->areax[0];
			flg = 1;
		}
		//逆変換
		double delta = itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2];
		double x, y;
		x = (itr->areax[0] + itr->areax[1]) / 2 - itr->position[4];
		y = (itr->areay[0] + itr->areay[1]) / 2 - itr->position[5];
		//notuse_d[0],[1] 逆変換後の中心座標(x,y)
		itr->notuse_d[0] = (itr->position[3] * x - itr->position[1] * y) / delta;
		itr->notuse_d[1] = (itr->position[0] * y - itr->position[2] * x) / delta;
	}
	if (flg == 0) {
		fprintf(stderr, "corrmap initialize exception\n");
		exit(1);
	}

	double min[2] = { corr.begin()->notuse_d[0],corr.begin()->notuse_d[1] };
	double max[2] = { corr.begin()->notuse_d[0],corr.begin()->notuse_d[1] };
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		min[0] = std::min(min[0], itr->notuse_d[0]);
		min[1] = std::min(min[1], itr->notuse_d[1]);
		max[0] = std::max(max[0], itr->notuse_d[0]);
		max[1] = std::max(max[1], itr->notuse_d[1]);
	}
	int n_x = (max[0] - min[0]) / step + 3;
	int n_y = (max[1] - min[1]) / step + 3;
	std::vector<std::vector<std::vector<corrmap0::Corrmap>>> corr_map;
	for (int i = 0; i < n_x; i++) {
		std::vector < std::vector<corrmap0::Corrmap> >corr_tmp;
		for (int j = 0; j < n_y; j++) {
			std::vector<corrmap0::Corrmap> corr_v;
			corr_tmp.push_back(corr_v);
		}
		corr_map.push_back(corr_tmp);
	}
	int ix, iy;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		ix = (itr->notuse_d[0] - min[0]) / step;
		iy = (itr->notuse_d[1] - min[1]) / step;
		//		if (ix < 0)ix = 0;
		//		if (iy < 0)iy = 0;
		for (int ix_add = -5; ix_add <= 5; ix_add++) {
			if (ix + ix_add < 0 || n_x <= ix + ix_add)continue;
			for (int iy_add = -5; iy_add <= 5; iy_add++) {
				if (iy + iy_add < 0 || n_y <= iy + iy_add)continue;
				corr_map[ix + ix_add][iy + iy_add].push_back(*itr);
			}
		}
	}
	//prediction作成
	std::vector<vxx::base_track_t> ret;
	double dist;
	corrmap0::Corrmap param;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ix = (itr->x - min[0]) / step;
		iy = (itr->y - min[1]) / step;
		if (ix < 0)ix = 0;
		if (iy < 0)iy = 0;

		if (corr_map[ix][iy].size() == 0) {
			fprintf(stderr, "exception:corrmap not found\n");
			exit(1);
		}
		dist = pow(corr_map[ix][iy].begin()->notuse_d[0] - itr->x, 2) + pow(corr_map[ix][iy].begin()->notuse_d[1] - itr->y, 2);
		param = *corr_map[ix][iy].begin();
		for (auto itr_c = corr_map[ix][iy].begin(); itr_c != corr_map[ix][iy].end(); itr_c++) {
			if (dist > pow(itr_c->notuse_d[0] - itr->x, 2) + pow(itr_c->notuse_d[1] - itr->y, 2)) {
				dist = pow(itr_c->notuse_d[0] - itr->x, 2) + pow(itr_c->notuse_d[1] - itr->y, 2);
				param = *itr_c;
			}
		}
		//distは2mm*sqrt(2)よりも小さくなるはず
		//printf("dist = %10.2lf", dist);
		//basetrack変換
		vxx::base_track_t base_tmp = base_trans(*itr,param);
		base_tmp.x = base_tmp.x + base_tmp.ax*(gap_nominal - param.dz);
		base_tmp.y = base_tmp.y + base_tmp.ay*(gap_nominal - param.dz);
		base_tmp.pl = pl;
		base_tmp.m[0].pos = pl * 10 + 1;
		base_tmp.m[1].pos = pl * 10 + 2;
		ret.push_back(base_tmp);
	}

	return ret;
}
std::vector<vxx::base_track_t> base_all_trans_0to1(std::vector<vxx::base_track_t> base, std::vector<corrmap0::Corrmap>corr, double gap_nominal, int pl) {
	//PL小のbasetrack-->PL大のbasetrackへ外挿、変換
	//方法
	//1:nominalなgapで仮外挿
	//2:外挿先のpositionでのgap param取得
	//3:取得したgap paramで本外挿
	//(本来はイタレーションすべき)
	//4:外挿時のparamで準変換

	std::vector<vxx::base_track_t> ret;
	double ini_x, ini_y, step, tmpi[2];
	std::map<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	//corrmapの設定
	int flg[3] = { 0,0,0 };
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		corr_map.insert(std::make_pair(std::make_pair(itr->notuse_i[1], itr->notuse_i[2]), *itr));
		//itr->notuse_i[1]:ix
		//itr->notuse_i[2]:iy
		if (flg[0] == 0 && itr->notuse_i[1] > 0) {
			ini_x = itr->areax[0];
			tmpi[0] = itr->notuse_i[1];
			flg[0] = 1;
		}
		if (flg[1] == 0 && itr->notuse_i[2] > 0) {
			ini_y = itr->areay[0];
			tmpi[1] = itr->notuse_i[2];
			flg[1] = 1;
		}
		if (flg[2] == 0 && itr + 1 != corr.end() && itr->notuse_i[1] > 0 && (itr + 1)->notuse_i[1] - itr->notuse_i[1] == 1) {
			step = (itr + 1)->areax[0] - itr->areax[0];
			flg[2] = 1;
		}
	}
	if (flg[0] == 0 || flg[1] == 0 || flg[2] == 0) {
		fprintf(stderr, "corrmap initialize exception\n");
		fprintf(stderr, "flg[0]=%d\n", flg[0]);
		fprintf(stderr, "flg[1]=%d\n", flg[1]);
		fprintf(stderr, "flg[2]=%d\n", flg[2]);
		exit(1);
	}
	ini_x = ini_x - step * tmpi[0];
	ini_y = ini_y - step * tmpi[1];

	//外挿・変換
	int ix, iy, dist;
	bool flg_find = true;
	corrmap0::Corrmap param;
	double ex_x, ex_y;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ex_x = itr->x - itr->ax*gap_nominal;
		ex_y = itr->y - itr->ay*gap_nominal;
		ix = (ex_x - ini_x) / step;
		iy = (ex_y - ini_y) / step;
		if (ix < 0)ix = 0;
		if (iy < 0)iy = 0;
		dist = 0;
		flg_find = true;
		//corrmap探索
		while (flg_find) {
			for (int ix_add = -1 * dist; ix_add <= dist; ix_add++) {
				if (ix + ix_add < 0)continue;
				for (int iy_add = -1 * (dist - abs(ix_add)); iy_add <= (dist - abs(ix_add)); iy_add++) {
					auto res = corr_map.find(std::make_pair(ix + ix_add, iy + iy_add));
					if (res == corr_map.end())continue;
					if (!flg_find) {
						//同じdistで2個以上候補があった時は中心距離の近いほうにする。
						double tmp_dist[2];
						tmp_dist[0] = pow(itr->x - (param.areax[0] + param.areax[1]) / 2, 2) + pow(itr->y - (param.areay[0] + param.areay[1]) / 2, 2);
						tmp_dist[1] = pow(itr->x - (res->second.areax[0] + res->second.areax[1]) / 2, 2) + pow(itr->y - (res->second.areay[0] + res->second.areay[1]) / 2, 2);
						if (tmp_dist[0] > tmp_dist[1])param = res->second;
					}
					else {
						param = res->second;
						flg_find = false;
					}

				}
			}
			dist++;
		}
		//basetrack変換
		vxx::base_track_t base_tmp = *itr;

		base_tmp.x = base_tmp.x - base_tmp.ax*(gap_nominal - param.dz);
		base_tmp.y = base_tmp.y - base_tmp.ay*(gap_nominal - param.dz);
		base_tmp = base_trans_inv(base_tmp, param);
		//base_tmp.dmy = itr->dmy;
		base_tmp.pl = pl;
		base_tmp.m[0].pos = pl * 10 + 1;

		base_tmp.m[1].pos = pl * 10 + 2;
		ret.push_back(base_tmp);
	}
	return ret;

}



vxx::base_track_t base_trans(vxx::base_track_t base, corrmap0::Corrmap corr) {
	double x, y;
	x = base.x;
	y = base.y;
	base.x = corr.position[0] * x + corr.position[1] * y + corr.position[4];
	base.y = corr.position[2] * x + corr.position[3] * y + corr.position[5];

	x = base.ax;
	y = base.ay;
	base.ax = corr.angle[0] * x + corr.angle[1] * y + corr.angle[4];
	base.ay = corr.angle[2] * x + corr.angle[3] * y + corr.angle[5];

	for (int i = 0; i < 2; i++) {
		x = base.m[i].ax;
		y = base.m[i].ay;
		base.m[i].ax = corr.angle[0] * x + corr.angle[1] * y + corr.angle[4];
		base.m[i].ay = corr.angle[2] * x + corr.angle[3] * y + corr.angle[5];
	}
	return base;
}
vxx::base_track_t base_trans_inv(vxx::base_track_t base, corrmap0::Corrmap corr) {
	double delta = corr.position[0] * corr.position[3] - corr.position[1] * corr.position[2];
	double x, y;
	x = base.x - corr.position[4];
	y = base.y - corr.position[5];
	base.x = (corr.position[3] * x - corr.position[1] * y) / delta;
	base.y = (corr.position[0] * y - corr.position[2] * x) / delta;

	delta = corr.angle[0] * corr.angle[3] - corr.angle[1] * corr.angle[2];
	x = base.ax - corr.angle[4];
	y = base.ay - corr.angle[5];
	base.ax = (corr.angle[3] * x - corr.angle[1] * y) / delta;
	base.ay = (corr.angle[0] * y - corr.angle[2] * x) / delta;
	for (int i = 0; i < 2; i++) {
		x = base.m[i].ax - corr.angle[4];
		y = base.m[i].ay - corr.angle[5];
		base.m[i].ax = (corr.angle[3] * x - corr.angle[1] * y) / delta;
		base.m[i].ay = (corr.angle[0] * y - corr.angle[2] * x) / delta;
	}

	return base;
}

void write_bvxx(std::string file_path, std::vector<vxx::base_track_t> &base, int pl) {
	std::stringstream filename;
	filename << file_path << "\\b" << std::setw(3) << std::setfill('0') << pl << ".vxx";

	vxx::BvxxWriter bw;
	bw.Write(filename.str(), pl, 0, base, vxx::opt::append);
}




