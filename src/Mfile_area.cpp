#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
void set_area(mfile0::Mfile &m, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z);
	void output_area(std::string filename, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg mfile output\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_output = argv[2];

	std::map<int, double> z, xmin, xmax, ymin, ymax;
	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	set_area(m, xmin, xmax, ymin, ymax,z);
	output_area(file_output, xmin, xmax, ymin, ymax, z);
}
void set_area(mfile0::Mfile &m, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z) {
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			int pl = itr2->pos / 10;
			auto res0 = xmin.insert(std::make_pair(pl, itr2->x));
			auto res1 = xmax.insert(std::make_pair(pl, itr2->x));
			auto res2 = ymin.insert(std::make_pair(pl, itr2->y));
			auto res3 = ymax.insert(std::make_pair(pl, itr2->y));
			auto res4 = z.insert(std::make_pair(pl, itr2->z));
			if (!res0.second) res0.first->second = std::min(res0.first->second, itr2->x);
			if (!res1.second) res1.first->second = std::max(res1.first->second, itr2->x);
			if (!res2.second) res2.first->second = std::min(res2.first->second, itr2->y);
			if (!res3.second) res3.first->second = std::max(res3.first->second, itr2->y);
		}
	}
}
void output_area(std::string filename, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z) {
	std::ofstream ofs(filename);


	for (auto itr = z.begin(); itr != z.end(); itr++) {
		double area[4];
		int pl = itr->first;
		auto res0 = xmin.find(pl);
		auto res1 = xmax.find(pl);
		auto res2 = ymin.find(pl);
		auto res3 = ymax.find(pl);
		if (res0 == xmin.end() || res1 == xmax.end() || res2 == ymin.end() || res3 == ymax.end()) {
			fprintf(stderr, "PL%03 exception\n",pl);
			exit(1);
		}
		ofs << std::right << std::fixed << std::setfill(' ')
			<< std::setw(3) << std::setprecision(0) << pl << " "
			<< std::setw(8) << std::setprecision(1) << res0->second << " "
			<< std::setw(8) << std::setprecision(1) << res1->second << " "
			<< std::setw(8) << std::setprecision(1) << res2->second << " "
			<< std::setw(8) << std::setprecision(1) << res3->second << " "
			<< std::setw(8) << std::setprecision(1) << itr->second << std::endl;

	}
}