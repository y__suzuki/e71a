#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"my_algorithm.lib")
#include <chrono>

#include <FILE_structure.hpp>
#include <my_algorithm.hpp>

using namespace std;

std::vector<std::pair<int, int>> multi_id_pair(std::vector<netscan::base_track_t> base);
void base_cluster_distribution(std::vector<netscan::base_track_t> base, double &angle_abs, double &d_pos_r, double &d_pos_l, double &d_ang_r, double &d_ang_l, double &d_pos_r_sig, double &d_pos_l_sig, double &d_ang_r_sig, double &d_ang_l_sig);
void micro0_cluster_distribution(std::vector<netscan::base_track_t> base, int &num,double &angle_abs, double &d_ang_r, double &d_ang_l, double &d_ang_r_sig, double &d_ang_l_sig);

int main(int argc, char** argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-bvxx pl zone output\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string output = argv[4];
	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_base, base, pl, zone);

	std::multimap<int, netscan::base_track_t> base_raw0;
	std::multimap<int, netscan::base_track_t> base_raw1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_raw0.insert(std::make_pair(itr->m[0].rawid, *itr));
		base_raw1.insert(std::make_pair(itr->m[1].rawid, *itr));
	}

	std::vector<std::pair<int, int>> pair_v = multi_id_pair(base);
	std::vector<id_pair> pair = id_clustering(pair_v);

	std::map<int, std::vector<netscan::base_track_t>> base_cluster;

	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		std::map<int, netscan::base_track_t> out_base_map;
		for (auto itr2 = itr->rawid0.begin(); itr2 != itr->rawid0.end(); itr2++) {
			if (base_raw0.count(*itr2) == 0) {
				fprintf(stderr, "exception base not found\n");
				exit(1);
			}
			else if (base_raw0.count(*itr2) == 1) {
				auto res = base_raw0.find(*itr2);
				out_base_map.insert(std::make_pair(res->second.rawid, res->second));

			}
			else {
				auto range = base_raw0.equal_range(*itr2);
				for (auto itr3 = range.first; itr3 != range.second; itr3++) {
					out_base_map.insert(std::make_pair(itr3->second.rawid, itr3->second));
				}
			}
		}
		for (auto itr2 = itr->rawid1.begin(); itr2 != itr->rawid1.end(); itr2++) {
			if (base_raw1.count(*itr2) == 0) {
				fprintf(stderr, "exception base not found\n");
				exit(1);
			}
			else if (base_raw1.count(*itr2) == 1) {
				auto res = base_raw1.find(*itr2);
				out_base_map.insert(std::make_pair(res->second.rawid, res->second));

			}
			else {
				auto range = base_raw1.equal_range(*itr2);
				for (auto itr3 = range.first; itr3 != range.second; itr3++) {
					out_base_map.insert(std::make_pair(itr3->second.rawid, itr3->second));
				}
			}
		}

		std::vector< netscan::base_track_t> out_base;
		for (auto itr2 = out_base_map.begin(); itr2 != out_base_map.end(); itr2++) {
			out_base.push_back(itr2->second);
		}
		base_cluster.insert(std::make_pair(itr->group_id, out_base));
	}

	std::ofstream ofs(output);
	double angle, d_pos_l, d_pos_r, d_ang_r, d_ang_l;
	double d_pos_r_sig, d_pos_l_sig, d_ang_r_sig, d_ang_l_sig;
	for (auto itr = base_cluster.begin(); itr != base_cluster.end(); itr++) {
		base_cluster_distribution(itr->second, angle, d_pos_r, d_pos_l, d_ang_r, d_ang_l, d_pos_r_sig, d_pos_l_sig, d_ang_r_sig, d_ang_l_sig);
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr->first << " "
			<< std::setw(5) << std::setprecision(0) << itr->second.size() << " "
			<< std::setw(7) << std::setprecision(4) << angle << " "
			<< std::setw(6) << std::setprecision(1) << d_pos_r << " "
			<< std::setw(6) << std::setprecision(1) << d_pos_l << " "
			<< std::setw(7) << std::setprecision(4) << d_ang_r << " "
			<< std::setw(7) << std::setprecision(4) << d_ang_l << " "
			<< std::setw(6) << std::setprecision(1) << d_pos_r_sig << " "
			<< std::setw(6) << std::setprecision(1) << d_pos_l_sig << " "
			<< std::setw(7) << std::setprecision(4) << d_ang_r_sig << " "
			<< std::setw(7) << std::setprecision(4) << d_ang_l_sig << std::endl;

	}
	std::ofstream ofs_m("out_micro.txt");
	int num;
	for (auto itr = base_cluster.begin(); itr != base_cluster.end(); itr++) {
		micro0_cluster_distribution(itr->second, num, angle, d_ang_r, d_ang_l, d_ang_r_sig, d_ang_l_sig);
		ofs_m << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr->first << " "
			<< std::setw(5) << std::setprecision(0) << num << " "
			<< std::setw(7) << std::setprecision(4) << angle << " "
			<< std::setw(7) << std::setprecision(4) << d_ang_r << " "
			<< std::setw(7) << std::setprecision(4) << d_ang_l << " "
			<< std::setw(7) << std::setprecision(4) << d_ang_r_sig << " "
			<< std::setw(7) << std::setprecision(4) << d_ang_l_sig << std::endl;

	}
}
std::vector<std::pair<int, int>> multi_id_pair(std::vector<netscan::base_track_t> base) {
	std::set <int> single_cand0;
	std::multimap <int, int> single_cnad1;
	std::set <std::pair<int, int>> single_pair;

	std::multimap <int, int> rawid0;
	std::multimap <int, int> rawid1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		rawid0.insert(std::make_pair(itr->m[0].rawid, itr->m[1].rawid));
		rawid1.insert(std::make_pair(itr->m[1].rawid, itr->m[0].rawid));
	}

	for (auto itr = rawid0.begin(); itr != rawid0.end(); itr++) {
		if (rawid0.count(itr->first) == 1) {
			single_cand0.insert(itr->second);
		}
	}
	for (auto itr = single_cand0.begin(); itr != single_cand0.end(); itr++) {
		if (rawid1.count(*itr) == 1) {
			auto res = rawid1.find(*itr);
			single_pair.insert(std::make_pair(res->second, res->first));
		}
	}


	std::vector<std::pair<int, int>> ret;

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (single_pair.count(std::make_pair(itr->m[0].rawid, itr->m[1].rawid)) == 0) {
			ret.push_back(std::make_pair(itr->m[0].rawid, itr->m[1].rawid));
		}
	}
	printf("all    =%d\n", base.size());
	printf("single =%d\n", single_pair.size());
	printf("multi  =%d\n", ret.size());

	printf("multi select %d --> %d (%4.1lf%%)\n", base.size(), ret.size(), ret.size()*100. / base.size());

	return ret;
}
void base_cluster_distribution(std::vector<netscan::base_track_t> base, double &angle_abs, double &d_pos_r, double &d_pos_l, double &d_ang_r, double &d_ang_l, double &d_pos_r_sig, double &d_pos_l_sig, double &d_ang_r_sig, double &d_ang_l_sig) {
	//分散も出力したい
	double ax = 0, ay = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ax += itr->ax;
		ay += itr->ay;
	}
	ax = ax / base.size();
	ay = ay / base.size();

	double pos_l[2], pos_r[2], ang_l[2], ang_r[2];
	double d_pos_l2 = 0, d_pos_r2 = 0, d_ang_l2 = 0, d_ang_r2 = 0;
	double d_pos_l1 = 0, d_pos_r1 = 0, d_ang_l1 = 0, d_ang_r1 = 0;
	double position[2], angle[2];
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		position[0] = (itr->x*ax + itr->y*ay) / sqrt(ax*ax + ay * ay);
		position[1] = (itr->x*ay - itr->y*ax) / sqrt(ax*ax + ay * ay);
		angle[0] = (itr->ax*ax + itr->ay*ay) / sqrt(ax*ax + ay * ay);
		angle[1] = (itr->ax*ay - itr->ay*ax) / sqrt(ax*ax + ay * ay);

		d_pos_r1 += position[0];
		d_pos_l1 += position[1];
		d_ang_r1 += angle[0];
		d_ang_l1 += angle[1];

		d_pos_r2 += position[0] * position[0];
		d_pos_l2 += position[1] * position[1];
		d_ang_r2 += angle[0] * angle[0];
		d_ang_l2 += angle[1] * angle[1];

		if (itr == base.begin()) {
			pos_r[0] = position[0];
			pos_r[1] = position[0];
			pos_l[0] = position[1];
			pos_l[1] = position[1];
			ang_r[0] = angle[0];
			ang_r[1] = angle[0];
			ang_l[0] = angle[1];
			ang_l[1] = angle[1];
		}
		pos_r[0] = std::min(pos_r[0], position[0]);
		pos_r[1] = std::max(pos_r[1], position[0]);
		pos_l[0] = std::min(pos_l[0], position[1]);
		pos_l[1] = std::max(pos_l[1], position[1]);
		ang_r[0] = std::min(ang_r[0], angle[0]);
		ang_r[1] = std::max(ang_r[1], angle[0]);
		ang_l[0] = std::min(ang_l[0], angle[1]);
		ang_l[1] = std::max(ang_l[1], angle[1]);
	}
	d_pos_r = pos_r[1] - pos_r[0];
	d_pos_l = pos_l[1] - pos_l[0];
	d_ang_r = ang_r[1] - ang_r[0];
	d_ang_l = ang_l[1] - ang_l[0];
	angle_abs = sqrt(ax*ax + ay * ay);

	d_pos_r_sig = sqrt(d_pos_r2 / base.size() - pow(d_pos_r1 / base.size(), 2));
	d_pos_l_sig = sqrt(d_pos_l2 / base.size() - pow(d_pos_l1 / base.size(), 2));
	d_ang_r_sig = sqrt(d_ang_r2 / base.size() - pow(d_ang_r1 / base.size(), 2));
	d_ang_l_sig = sqrt(d_ang_l2 / base.size() - pow(d_ang_l1 / base.size(), 2));
	if (isnan(d_pos_r_sig))d_pos_r_sig = 0;
	if (isnan(d_pos_l_sig))d_pos_l_sig = 0;
	if (isnan(d_ang_r_sig))d_ang_r_sig = 0;
	if (isnan(d_ang_l_sig))d_ang_l_sig = 0;

	return;
}
void micro0_cluster_distribution(std::vector<netscan::base_track_t> base, int &num,double &angle_abs, double &d_ang_r, double &d_ang_l, double &d_ang_r_sig, double &d_ang_l_sig) {
	//分散も出力したい
	std::map<int, netscan::micro_track_subset_t> micro_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		micro_map.insert(std::make_pair(itr->m[0].rawid, itr->m[0]));
	}
	double ax = 0, ay = 0;
	for (auto itr = micro_map.begin(); itr != micro_map.end(); itr++) {
		ax += itr->second.ax;
		ay += itr->second.ay;
	}
	ax = ax / micro_map.size();
	ay = ay / micro_map.size();

	double ang_l[2], ang_r[2];
	double  d_ang_l2 = 0, d_ang_r2 = 0;
	double  d_ang_l1 = 0, d_ang_r1 = 0;
	double  angle[2];
	for (auto itr = micro_map.begin(); itr != micro_map.end(); itr++) {
		angle[0] = (itr->second.ax*ax + itr->second.ay*ay) / sqrt(ax*ax + ay * ay);
		angle[1] = (itr->second.ax*ay - itr->second.ay*ax) / sqrt(ax*ax + ay * ay);

		d_ang_r1 += angle[0];
		d_ang_l1 += angle[1];

		d_ang_r2 += angle[0] * angle[0];
		d_ang_l2 += angle[1] * angle[1];

		if (itr == micro_map.begin()) {
			ang_r[0] = angle[0];
			ang_r[1] = angle[0];
			ang_l[0] = angle[1];
			ang_l[1] = angle[1];
		}
		ang_r[0] = std::min(ang_r[0], angle[0]);
		ang_r[1] = std::max(ang_r[1], angle[0]);
		ang_l[0] = std::min(ang_l[0], angle[1]);
		ang_l[1] = std::max(ang_l[1], angle[1]);
	}
	d_ang_r = ang_r[1] - ang_r[0];
	d_ang_l = ang_l[1] - ang_l[0];
	angle_abs = sqrt(ax*ax + ay * ay);

	d_ang_r_sig = sqrt(d_ang_r2 / micro_map.size() - pow(d_ang_r1 / micro_map.size(), 2));
	d_ang_l_sig = sqrt(d_ang_l2 / micro_map.size() - pow(d_ang_l1 / micro_map.size(), 2));
	if (isnan(d_ang_r_sig))d_ang_r_sig = 0;
	if (isnan(d_ang_l_sig))d_ang_l_sig = 0;
	num = micro_map.size();
	return;
}

