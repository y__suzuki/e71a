#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
# include <iostream>
# include <random>

#include <list>
class stop_track {
public:
	int64_t chainid, groupid;
	int  nseg, npl, pl0, pl1, ph, rawid;
	double ax, ay, x, y, z;
};
class track_pair {
public:
	int eventid;
	double x, y, z, md,oa;
	stop_track t[2];
};
class track_multi {
public:
	int eventid,pl;
	double x, y, z;
	std::vector< std::pair<double,stop_track>> trk;
	std::vector<track_pair>pair;
};

void read_stop_txt(std::string filename, std::map<int, std::vector<stop_track>> &stop);
void stop_shufle(std::vector<stop_track> &stop);

std::vector<track_pair> search_2trk_vtx(std::vector<stop_track> stop,int pl);
std::vector<track_multi> clustering_2trk_vtx(std::vector<track_pair> pair,int pl);
void output_vtx(std::string filename, std::vector<track_multi> vtx);
void output_chain(std::vector<track_multi> vtx, std::multimap<int, mfile0::M_Chain*> c, std::vector<mfile0::M_Chain> &chain);
void multi_vtx_count(std::vector<track_multi> vtx,int pl);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-stop out-vtx in-mfile vtx-mfile mode\n");
		fprintf(stderr, "mode=1:vtx search\n");
		fprintf(stderr, "mode=2:BG study\n");
		exit(1);
	}
	std::string file_in_stop = argv[1];
	std::string file_out_vtx = argv[2];
	std::string file_in_mfile = argv[3];
	std::string file_out_mfile = argv[4];
	int mode = std::stoi(argv[5]);
	std::map<int, std::vector<stop_track>> stop;
	read_stop_txt(file_in_stop, stop);
	//mode 0のみ使用
	mfile0::Mfile m;
	std::multimap<int, mfile0::M_Chain*> c;

	if (mode == 2) {
		for (auto itr = stop.begin(); itr != stop.end(); itr++) {
			stop_shufle(itr->second);
		}
	}
	else if (mode == 1) {
		mfile1::read_mfile_extension(file_in_mfile, m);
		for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
			c.insert(std::make_pair(itr->basetracks.begin()->group_id, &(*itr)));
		}
	}
	else {
		fprintf(stderr, "mode exception\n");
		fprintf(stderr, "mode=1:vtx search\n");
		fprintf(stderr, "mode=2:BG study\n");
		exit(1);
	}
	std::vector<mfile0::M_Chain> out_chain;
	//file消去
	std::ofstream ofs(file_out_vtx);
	ofs.close();
	for (int pl = 16; pl <= 130; pl++) {
		auto res = stop.find(pl);
		if (res == stop.end())continue;
		printf("PL%03d vtx search\n", pl);
		std::vector<track_pair> pair = search_2trk_vtx(res->second,pl);
		std::vector<track_multi> multi = clustering_2trk_vtx(pair,pl);
		multi_vtx_count(multi,pl);
		output_vtx(file_out_vtx, multi);
		if (mode == 1) {
			output_chain(multi, c, out_chain);
		}
	}
	if (mode == 1) {
		m.chains = out_chain;
		mfile0::write_mfile(file_out_mfile, m);
	}
}
void read_stop_txt(std::string filename, std::map<int, std::vector<stop_track>> &stop) {
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		stop_track stop_tmp;
		stop_tmp.chainid = stoll(str_v[0]);
		stop_tmp.nseg = stoi(str_v[1]);
		stop_tmp.pl0 = stoi(str_v[2]);
		stop_tmp.pl1 = stoi(str_v[3]);
		stop_tmp.groupid = stoll(str_v[4]);
		stop_tmp.rawid = stoi(str_v[5]);
		stop_tmp.ph = stoi(str_v[6]);
		stop_tmp.ax = stod(str_v[7]);
		stop_tmp.ay = stod(str_v[8]);
		stop_tmp.x = stod(str_v[9]);
		stop_tmp.y = stod(str_v[10]);
		stop_tmp.z = stod(str_v[11]);
		stop_tmp.npl = stop_tmp.pl1 - stop_tmp.pl0 + 1;
		cnt++;
		auto res = stop.find(stop_tmp.pl1);
		if (res == stop.end()) {
			std::vector<stop_track> tmp_s{ stop_tmp };
			stop.insert(std::make_pair(stop_tmp.pl1, tmp_s));
		}
		else {
			res->second.push_back(stop_tmp);
		}
	}

	printf("input fin %d track\n", cnt);
}
std::vector<track_pair> search_2trk_vtx(std::vector<stop_track> stop,int pl) {
	std::map<int,stop_track> track_map;
	for (auto itr = stop.begin(); itr != stop.end(); itr++) {
		//if (itr->nseg < 4)continue;
		track_map.insert(std::make_pair(itr->rawid,(*itr)));
	}
	std::vector<stop_track> track;
	for (auto itr = track_map.begin(); itr != track_map.end(); itr++) {
		track.push_back(itr->second);
	}
	std::vector< track_pair> pair;
	double zrange[2] = {0,0};
	if (pl <= 15 || (16 <= pl && pl % 2 == 0)) {
		zrange[0] = -1000;
	}
	else if (pl % 2 == 1) {
		zrange[0] = -3500;
	}
	double extra[2];
	uint64_t count = 0;
	uint64_t all = track.size() * (track.size() - 1) / 2;
	uint64_t fin = 0;
	for (auto itr0 = track.begin(); itr0 != track.end(); itr0++) {
		for (auto itr1 = std::next(itr0, 1); itr1 != track.end(); itr1++) {
			fin++;
			if (fin % 1000000 == 0) {
				fprintf(stderr, "\r calc md %lld/%lld(%4.1lf%%)", fin, all,fin*100./all);
			}
			matrix_3D::vector_3D pos0, pos1, dir0, dir1;
			pos0.x = itr0->x;
			pos0.y = itr0->y;
			pos0.z = 0;
			pos1.x = itr1->x;
			pos1.y = itr1->y;
			pos1.z = 0;
			dir0.x = itr0->ax;
			dir0.y = itr0->ay;
			dir0.z = 1;
			dir1.x = itr1->ax;
			dir1.y = itr1->ay;
			dir1.z = 1;
			double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, zrange, extra);
			if (md > 10 + 100./2500.*fabs(extra[0] + extra[1]) / 2)continue;
			track_pair pair_tmp;
			matrix_3D::vector_3D extra0 = addition(pos0, const_multiple(dir0, extra[0]));
			matrix_3D::vector_3D extra1 = addition(pos1, const_multiple(dir1, extra[1]));

			pair_tmp.x = (extra0.x + extra1.x) / 2;
			pair_tmp.y = (extra0.y + extra1.y) / 2;
			pair_tmp.z = (extra0.z + extra1.z) / 2;
			pair_tmp.eventid = count;
			pair_tmp.md = md;
			pair_tmp.oa = matrix_3D::opening_angle(dir0, dir1);
			pair_tmp.t[0] = *itr0;
			pair_tmp.t[1] = *itr1;
			pair.push_back(pair_tmp);
			count++;
		}
	}
	fprintf(stderr, "\r calc md %lld/%lld(%4.1lf%%)\n", fin, all, fin*100. / all);
	printf("2trk vtx num=%d\n", pair.size());
	return pair;

}
std::vector<track_multi> clustering_2trk_vtx(std::vector<track_pair> pair, int pl) {
	std::vector<track_multi> ret;
	int count = 0;
	std::map<int, stop_track> tracks;
	std::list<track_pair> pair_list(pair.begin(), pair.end());
	for (auto itr0 = pair_list.begin(); itr0 != pair_list.end(); itr0++) {
		//近くにあるvtxはclustering
		tracks.clear();
		tracks.insert(std::make_pair(itr0->t[0].rawid, itr0->t[0]));
		tracks.insert(std::make_pair(itr0->t[1].rawid, itr0->t[1]));
		for (auto itr1 = std::next(itr0, 1); itr1 != pair_list.end(); ) {
			//vtx位置が30um以内
			if (pow(itr0->x - itr1->x, 2) + pow(itr0->y - itr1->y, 2) + pow(itr0->z - itr1->z, 2) > pow(30, 2)) {
				itr1 = std::next(itr1, 1);
				continue;
			}
			tracks.insert(std::make_pair(itr1->t[0].rawid, itr1->t[0]));
			tracks.insert(std::make_pair(itr1->t[1].rawid, itr1->t[1]));
			itr1 = pair_list.erase(itr1);
		}
		double zrange[2] = { 0,0 };
		if (pl <= 15 || (pl >= 16 && pl % 2 == 0)) {
			zrange[0] = -1000;
		}
		else if (pl % 2 == 1) {
			zrange[0] = -3500;
		}

		double extra[2];
		track_multi multi;
		multi.eventid = count;
		multi.pl = pl;
		//全2trkのmd計算
		for (auto itr1 = tracks.begin(); itr1 != tracks.end(); itr1++) {
			for (auto itr2 = std::next(itr1, 1); itr2 != tracks.end(); itr2++) {
				matrix_3D::vector_3D pos0, pos1, dir0, dir1;
				pos0.x = itr1->second.x;
				pos0.y = itr1->second.y;
				pos0.z = 0;
				pos1.x = itr2->second.x;
				pos1.y = itr2->second.y;
				pos1.z = 0;
				dir0.x = itr1->second.ax;
				dir0.y = itr1->second.ay;
				dir0.z = 1;
				dir1.x = itr2->second.ax;
				dir1.y = itr2->second.ay;
				dir1.z = 1;
				double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, zrange, extra);
				track_pair pair_tmp;
				matrix_3D::vector_3D extra0 = addition(pos0, const_multiple(dir0, extra[0]));
				matrix_3D::vector_3D extra1 = addition(pos1, const_multiple(dir1, extra[1]));

				pair_tmp.x = (extra0.x + extra1.x) / 2;
				pair_tmp.y = (extra0.y + extra1.y) / 2;
				pair_tmp.z = (extra0.z + extra1.z) / 2;
				pair_tmp.eventid = count;
				pair_tmp.md = md;
				pair_tmp.oa = matrix_3D::opening_angle(dir0, dir1);
				pair_tmp.t[0] = itr1->second;
				pair_tmp.t[1] = itr2->second;
				multi.pair.push_back(pair_tmp);
			}
		}
		//加重平均でvtx pointの決定
		multi.x = 0;
		multi.y = 0;
		multi.z = 0;
		for (auto itr = multi.pair.begin(); itr != multi.pair.end(); itr++) {
			multi.x += itr->x;
			multi.y += itr->y;
			multi.z += itr->z;
		}
		multi.x = multi.x / multi.pair.size();
		multi.y = multi.y / multi.pair.size();
		multi.z = multi.z / multi.pair.size();
		//各trkに対してIPの計算
		for (auto itr = tracks.begin(); itr != tracks.end(); itr++) {
			matrix_3D::vector_3D pos0, pos1, dir0, dir1;
			pos0.x = itr->second.x;
			pos0.y = itr->second.y;
			pos0.z = 0;
			pos1.x = multi.x;
			pos1.y = multi.y;
			pos1.z = multi.z;
			dir0.x = itr->second.ax;
			dir0.y = itr->second.ay;
			dir0.z = 1;
			double ip = matrix_3D::inpact_parameter(pos0, dir0, pos1);
			multi.trk.push_back(std::make_pair(ip, itr->second));
		}
		ret.push_back(multi);
		count++;
	};
	printf("number of vtx %d\n", count);
	return ret;
}
void output_vtx(std::string filename, std::vector<track_multi> vtx) {
	std::ofstream ofs(filename, std::ios::app);
	for (auto itr0 = vtx.begin(); itr0 != vtx.end(); itr0++) {
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr0->eventid << " "
			<< std::setw(4) << std::setprecision(0) << itr0->pl << " "
			<< std::setw(4) << std::setprecision(0) << itr0->trk.size() << " "
			<< std::setw(8) << std::setprecision(1) << itr0->x << " "
			<< std::setw(8) << std::setprecision(1) << itr0->y << " "
			<< std::setw(8) << std::setprecision(1) << itr0->z << std::endl;
		for (auto itr1 = itr0->pair.begin(); itr1 != itr0->pair.end(); itr1++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr1->t[0].pl1 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->t[0].rawid << " "
				<< std::setw(4) << std::setprecision(0) << itr1->t[1].pl1 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->t[1].rawid << " "
				<< std::setw(8) << std::setprecision(1) << itr1->x << " "
				<< std::setw(8) << std::setprecision(1) << itr1->y << " "
				<< std::setw(8) << std::setprecision(1) << itr1->z << " "
				<< std::setw(6) << std::setprecision(4) << itr1->oa << " "
				<< std::setw(4) << std::setprecision(1) << itr1->md << std::endl;
		}
		for (auto itr1 = itr0->trk.begin(); itr1 != itr0->trk.end(); itr1++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr1->second.pl0 << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.pl1 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->second.rawid << " "
				<< std::setw(20) << std::setprecision(0) << itr1->second.chainid << " "
				<< std::setw(20) << std::setprecision(0) << itr1->second.groupid << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.nseg << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.npl << " "
				<< std::setw(6) << std::setprecision(0) << itr1->second.ph << " "
				<< std::setw(7) << std::setprecision(4) << itr1->second.ax << " "
				<< std::setw(7) << std::setprecision(4) << itr1->second.ay << " "
				<< std::setw(8) << std::setprecision(1) << itr1->second.x << " "
				<< std::setw(8) << std::setprecision(1) << itr1->second.y << " "
				<< std::setw(4) << std::setprecision(1) << itr1->first << std::endl;
		}
	}
}
void output_chain(std::vector<track_multi> vtx, std::multimap<int, mfile0::M_Chain*> c, std::vector<mfile0::M_Chain> &chain) {
	for (auto itr = vtx.begin(); itr != vtx.end(); itr++) {
		if (itr->trk.size() < 3)continue;
		for (auto itr2 = itr->trk.begin(); itr2 != itr->trk.end(); itr2++) {
			if (c.count(itr2->second.groupid) == 0)continue;
			else if (c.count(itr2->second.groupid) == 1) {
				auto res = c.find(itr2->second.groupid);
				chain.push_back(*res->second);
			}
			else {
				auto range = c.equal_range(itr2->second.groupid);
				for (auto res = range.first; res != range.second; res++) {
					chain.push_back(*res->second);
				}
			}
		}
	}
}
void stop_shufle(std::vector<stop_track> &stop) {
	double xmin, ymin, xmax, ymax;
	for (auto itr = stop.begin(); itr != stop.end(); itr++) {
		if (itr == stop.begin()) {
			xmin = itr->x;
			xmax = itr->x;
			ymin = itr->y;
			ymax = itr->y;
		}
		xmin = std::min(xmin, itr->x);
		xmax = std::max(xmax, itr->x);
		ymin = std::min(ymin, itr->y);
		ymax = std::max(ymax, itr->y);
	}

	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<double> posx(xmin, xmax);
	std::uniform_real_distribution<double> posy(ymin, ymax);
	for (auto itr = stop.begin(); itr != stop.end(); itr++) {
		itr->x = posx(mt);
		itr->y = posy(mt);
	}
}
void multi_vtx_count(std::vector<track_multi> vtx,int pl) {
	std::map<int, int> count;
	for (auto itr = vtx.begin(); itr!= vtx.end(); itr++) {
		auto res = count.insert(std::make_pair(itr->trk.size(),1));
		if (!res.second) {
			res.first->second++;
		}
	}
	printf("PL%d vtx trk num\n", pl);
	for (auto itr = count.begin(); itr != count.end(); itr++) {
		printf("%d %d\n", itr->first, itr->second);
	}
}