#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include <filesystem>

class muon_track {
public:
	int pl, rawid, t_pl, t_rawid, ievent, time;
	double ax, ay, x, y;
	double t_ax, t_ay, t_x, t_y, momentum;

};

std::multimap<int, muon_track> read_muon_list(std::string filename);
std::multimap <std::pair<int, int>, mfile0::M_Chain*> chain_pl_base_map(std::vector<mfile0::M_Chain>&c, int pl);
mfile0::M_Chain make_chain(muon_track mu, double z, std::vector<corrmap0::Corrmap> &corr);
void search_chain_output(std::vector<muon_track> mu_vec, std::multimap <std::pair<int, int>, mfile0::M_Chain*> &chain_map, mfile0::M_Header header, std::string folderpath, std::vector<corrmap0::Corrmap> &corr, bool &overwirte);


int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "prg:input-mfile input-muon input-corrmap-abs output_path\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_muon = argv[2];
	std::string file_in_corr = argv[3];
	std::string file_out_path = argv[4];

	if (!std::filesystem::create_directory(file_out_path + "\\event")) {
		printf("event folder already exsit\n");
	}
	std::multimap<int, muon_track> muon = read_muon_list(file_in_muon);
	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corr, corr);

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::multimap <std::pair<int, int>, mfile0::M_Chain*> chain_map = chain_pl_base_map(m.chains, 4);

	bool overwrite = false;
	for (auto itr = muon.begin(); itr != muon.end(); ) {
		std::vector<muon_track> mu_vec;
		int count = muon.count(itr->first);
		for (int i = 0; i < count; i++) {
			mu_vec.push_back(itr->second);
			itr = std::next(itr, 1);
		}
		search_chain_output(mu_vec, chain_map, m.header, file_out_path + "\\event", corr, overwrite);
	}
}
std::multimap<int, muon_track> read_muon_list(std::string filename) {
	std::ifstream ifs(filename);
	std::multimap<int, muon_track> ret;
	muon_track t;
	while (ifs >> t.pl >> t.rawid >> t.ax >> t.ay >> t.x >> t.y
		>> t.t_pl >> t.t_rawid >> t.t_ax >> t.t_ay >> t.t_x >> t.t_y
		>> t.ievent >> t.time >> t.momentum) {
		ret.insert(std::make_pair(t.ievent,t));
	}

	printf("all muon track %d\n", ret.size());
	return ret;

}

std::multimap <std::pair<int, int>, mfile0::M_Chain*> chain_pl_base_map(std::vector<mfile0::M_Chain>&c, int pl) {
	std::multimap <std::pair<int, int>, mfile0::M_Chain*> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->pos0 / 10 != pl)continue;
		ret.insert(std::make_pair(std::make_pair(itr->basetracks.begin()->pos / 10, itr->basetracks.begin()->rawid), &(*itr)));
	}
	return ret;
}

void search_chain_output(std::vector<muon_track> mu_vec, std::multimap <std::pair<int, int>, mfile0::M_Chain*> &chain_map,mfile0::M_Header header, std::string folderpath,std::vector<corrmap0::Corrmap> &corr,bool &overwirte) {
	int event_id = mu_vec.begin()->ievent;
	std::stringstream output_folder;
	std::string output_file;
	output_folder << folderpath << "\\" <<std::setw(10)<<std::setfill('0')<< event_id;
	if (!std::filesystem::create_directory(output_folder.str())) {
		printf("event %d folder already exsit\n",event_id);
	}

	int count = 0;

	for (auto itr = mu_vec.begin(); itr != mu_vec.end(); itr++) {
		std::stringstream output_file_ss;

		output_file_ss << output_folder.str() << "\\mu_" << std::setw(3) << std::setfill('0') << count << ".all";
		output_file = output_file_ss.str();
		count++;
		if (std::filesystem::exists(output_file)) {
			std::cout << output_file << " is already exist." << std::endl;
			bool flg = true;
			while (flg) {
				if (overwirte) {
					break;
				}
				std::cout << "overwrite file?(y/n/a)" << std::endl;
				char judge;
				std::cin >> judge;
				if (judge == 'n')return;
				else if (judge == 'y') {
					flg = false;
				}
				else if (judge == 'a') {
					flg = false;
					overwirte = true;
				}
				else {
					flg = true;
				}
			}
		}

		mfile0::Mfile m;
		m.header = header;
		double z = -3330;
		if (chain_map.count(std::pair(itr->pl, itr->rawid)) == 0) {
			//chainなし
			//basetrackをmfileに変換して書き込む
			m.chains.push_back(make_chain(*itr, z, corr));
		}
		else {
			auto range = chain_map.equal_range(std::pair(itr->pl, itr->rawid));
			for (auto res = range.first; res != range.second; res++) {
				m.chains.push_back(*(res->second));
			}
		}

		mfile0::write_mfile(output_file, m);
	}
}

mfile0::M_Chain make_chain(muon_track mu, double z, std::vector<corrmap0::Corrmap> &corr) {
	corrmap0::Corrmap param;
	bool flg = true;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (flg) {
			if (itr->pos[0] / 10 == mu.pl) {
				param = *itr;
				flg = false;
				break;
			}
		}
	}
	if (flg) {
		printf("PL%03d corrmap-abs not found\n", mu.pl);
		exit(1);
	}

	mfile0::M_Chain ret;
	mfile0::M_Base b;
	b.flg_d[0] = 0;
	b.flg_d[1] = 0;
	b.flg_i[0] = 0;
	b.flg_i[1] = 0;
	b.flg_i[2] = 0;
	b.flg_i[3] = 0;
	b.ax = mu.ax*param.angle[0] + mu.ay*param.angle[1] + param.angle[4];
	b.ay = mu.ax*param.angle[2] + mu.ay*param.angle[3] + param.angle[5];
	b.x = mu.x*param.position[0] + mu.y*param.position[1] + param.position[4];
	b.y = mu.x*param.position[2] + mu.y*param.position[3] + param.position[5];
	b.group_id = 1000000;
	b.ph = 0;
	b.rawid = mu.rawid;
	b.pos = mu.pl * 10 + 1;
	ret.basetracks.push_back(b);

	ret.chain_id = 10000000;
	ret.nseg = ret.basetracks.size();
	ret.pos0 = ret.basetracks.begin()->pos;
	ret.pos1 = ret.basetracks.rbegin()->pos;

	return ret;
}