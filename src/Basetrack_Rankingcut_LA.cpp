#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
struct CutParam {
	double angle[2];
	double intercept, slope, ymax;
};
struct BaseInformation {
	double angle, x, y;
	netscan::base_track_t *ptr;
};

std::vector<CutParam>  read_cut_param(std::string filename);
std::vector<netscan::base_track_t> RankingCut(std::vector<netscan::base_track_t> &base, CutParam param);
std::vector<netscan::base_track_t>RankingCut_lateral(std::vector<netscan::base_track_t> &base);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-bvxx pl zone out-bvxx cut-param-file\n");
		fprintf(stderr, "cut-param-file:ang_min ang_max x1 y1 x2 y2\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_base = argv[4];
	std::string file_in_cut = argv[5];

	std::vector<CutParam> cutparam = read_cut_param(file_in_cut);

	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_base, base, pl, zone);
	base = RankingCut_lateral(base);
	for (auto itr = cutparam.begin(); itr != cutparam.end(); itr++) {
		base = RankingCut(base, *itr);
	}
	netscan::write_basetrack_vxx(file_out_base, base, pl, zone);
}
std::vector<CutParam>  read_cut_param(std::string filename) {
	std::vector<CutParam> ret;
	std::ifstream ifs(filename);
	double angle[2], x[2], y[2];
	while (ifs >> angle[0] >> angle[1] >> x[0] >> y[0] >> x[1] >> y[1]) {
		CutParam par;
		par.angle[0] = angle[0];
		par.angle[1] = angle[1];
		par.slope = (y[1] - y[0]) / (x[1] - x[0]);
		par.intercept = y[0] - par.slope * x[0];
		par.ymax = y[1];
		ret.push_back(par);
		//printf("y=%lf x+%lf\n", par.slope, par.intercept);
	}
	return ret;
}

std::vector<netscan::base_track_t> RankingCut(std::vector<netscan::base_track_t> &base, CutParam param) {
	std::vector<netscan::base_track_t> ret;
	std::vector<BaseInformation> b_inf;

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		BaseInformation tmp;
		tmp.angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		tmp.x = sqrt(pow(itr->m[0].ax - itr->ax, 2) + pow(itr->m[1].ax - itr->ax, 2) + pow(itr->m[0].ay - itr->ay, 2) + pow(itr->m[1].ay - itr->ay, 2));
		tmp.y = (itr->m[0].ph + itr->m[1].ph) % 10000;
		tmp.ptr = &(*itr);
		b_inf.push_back(tmp);
	}

	int count = 0;
	for (auto itr = b_inf.begin(); itr != b_inf.end(); itr++) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r angle %3.1lf - %3.1lf %12d/%12d(%4.1lf%%)", param.angle[0], param.angle[1], count, b_inf.size(), count*100. / b_inf.size());
		}
		count++;
		if (param.angle[0] <= itr->angle&&itr->angle < param.angle[1]) {
			if (itr->y >= itr->x*param.slope + param.intercept || param.ymax <= itr->y) {
				ret.push_back(*(itr->ptr));
			}
		}
		else {
			ret.push_back(*(itr->ptr));

		}
	}

	fprintf(stderr, "\r angle %3.1lf - %3.1lf %12d/%12d(%4.1lf%%)\n", param.angle[0], param.angle[1], count, b_inf.size(), count*100. / b_inf.size());
	fprintf(stderr, "%d --> %d(%4.1lf%%)\n", base.size(), ret.size(), ret.size()*100. / base.size());

	return ret;
}
std::vector<netscan::base_track_t>RankingCut_lateral(std::vector<netscan::base_track_t> &base) {
	std::vector<netscan::base_track_t> ret;

	double d_lat;
	double angle;
	double vph;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		d_lat = sqrt(pow((itr->ax*itr->m[0].ay - itr->ay*itr->m[0].ax) / angle, 2) + pow((itr->ax*itr->m[1].ay - itr->ay*itr->m[1].ax) / angle, 2));
		vph = (itr->m[0].ph + itr->m[1].ph) % 10000;

		if (d_lat > 0.05)continue;
		if (angle > 0.0) {
			//if (vph < std::min(d_lat * 20 / 0.025 - 12, 20.))continue;
		}
		ret.push_back(*itr);
	}
	fprintf(stderr, "lateral<=0.05 %d --> %d(%4.1lf%%)\n", base.size(), ret.size(), ret.size()*100. / base.size());
	return ret;
}