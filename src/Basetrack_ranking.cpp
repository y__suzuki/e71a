#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

struct cut_param {
	//角度ずれ幅
	//cutのvph
	double angle[2];
	double dangle[2];
	int vph_cut;
};
void read_cutparam(std::string filename, std::vector<cut_param> &param);
std::vector<std::pair<double, double>> param_divide_angle(std::vector<cut_param> param, std::map< std::pair<double, double>, std::vector<cut_param>> &param_map);
std::vector<netscan::base_track_t> ranking_cut(std::vector<netscan::base_track_t>base, std::vector<cut_param> cut);
std::vector<netscan::base_track_t> angle_cut(std::vector<netscan::base_track_t>base, std::vector<netscan::base_track_t>&remain, std::pair<double, double> angle_cut);
std::vector<netscan::base_track_t> ranking_cut_lateral(std::vector<netscan::base_track_t>base, std::vector<cut_param> cut);

int main(int argc, char **argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:prg bvxx pl zone param param-lat bvxx-output\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_in_param = argv[4];
	std::string file_in_param_lat = argv[5];
	std::string file_out_bvxx = argv[6];

	std::vector<cut_param> param;
	read_cutparam(file_in_param, param);
	std::map< std::pair<double, double>, std::vector<cut_param>> param_map;
	std::vector<std::pair<double, double>> angle_range = param_divide_angle(param, param_map);

	//int count = 0;
	//for (auto itr = param_map.begin(); itr != param_map.end(); itr++) {
	//	count++;
	//	for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
	//		printf("count=%d angle %3.1lf - %3.1lf dang %4.2lf - %4.2lf cut=%d\n", count, itr2->angle[0], itr2->angle[1], itr2->dangle[0], itr2->dangle[1], itr2->vph_cut);
	//	}
	//}

	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_base, base, pl, zone);


	std::vector<netscan::base_track_t> out;
	std::vector<netscan::base_track_t> buffer0;
	std::vector<netscan::base_track_t> buffer1;
	std::vector<netscan::base_track_t> sel;
	buffer0 = base;
	for (auto itr = angle_range.begin(); itr != angle_range.end(); itr++) {
		sel.clear();
		buffer1.clear();
		sel = angle_cut(buffer0, buffer1, *itr);
		buffer0 = buffer1;
		buffer1.clear();
		buffer1 = ranking_cut(sel, param_map[*itr]);
		for (auto itr2 = buffer1.begin(); itr2 != buffer1.end(); itr2++) {
			out.push_back(*itr2);
		}
	}
	for (auto itr = buffer0.begin(); itr != buffer0.end(); itr++) {
		out.push_back(*itr);
	}
	printf("ranking cut fin %d --> %d(%4.1lf%%)\n", base.size(), out.size(), out.size()*100. / base.size());


	base.clear();
	base = out;
	printf("lateral cut\n");


	std::vector<cut_param> param_lat;
	read_cutparam(file_in_param_lat, param_lat);
	std::map< std::pair<double, double>, std::vector<cut_param>> param_lat_map;
	std::vector<std::pair<double, double>> angle_range_lat = param_divide_angle(param_lat, param_lat_map);

	out.clear();
	buffer0.clear();
	buffer1.clear();
	sel.clear();
	buffer0 = base;
	for (auto itr = angle_range_lat.begin(); itr != angle_range_lat.end(); itr++) {
		sel.clear();
		buffer1.clear();
		sel = angle_cut(buffer0, buffer1, *itr);
		buffer0 = buffer1;
		buffer1.clear();
		buffer1 = ranking_cut_lateral(sel, param_lat_map[*itr]);
		for (auto itr2 = buffer1.begin(); itr2 != buffer1.end(); itr2++) {
			out.push_back(*itr2);
		}
	}
	for (auto itr = buffer0.begin(); itr != buffer0.end(); itr++) {
		out.push_back(*itr);
	}



	printf("ranking cut lateral fin %d --> %d(%4.1lf%%)\n", base.size(), out.size(), out.size()*100. / base.size());


	netscan::write_basetrack_vxx(file_out_bvxx, out, pl, zone);

}


void read_cutparam(std::string filename, std::vector<cut_param> &param) {
	std::ifstream ifs(filename);

	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		cut_param param_tmp;
		param_tmp.angle[0] = std::stod(str_v[0]);
		param_tmp.angle[1] = std::stod(str_v[1]);
		param_tmp.dangle[0] = std::stod(str_v[2]);
		param_tmp.dangle[1] = std::stod(str_v[3]);
		param_tmp.vph_cut = std::stoi(str_v[4]);
		param.push_back(param_tmp);
	}

}
std::vector<std::pair<double, double>> param_divide_angle(std::vector<cut_param> param, std::map< std::pair<double, double>, std::vector<cut_param>> &param_map) {
	std::vector<std::pair<double, double>> ret;

	std::multimap< std::pair<double, double>, cut_param> param_multimap;

	for (auto itr = param.begin(); itr != param.end(); itr++) {
		std::pair<double, double> angle_range;
		angle_range = std::make_pair(itr->angle[0], itr->angle[1]);
		param_multimap.insert(std::make_pair(angle_range, *itr));
	}

	std::vector<cut_param> param_v;
	std::pair<double, double> angle_range;
	for (auto itr = param_multimap.begin(); itr != param_multimap.end(); itr++) {
		param_v.push_back(itr->second);
		if (std::next(itr, 1) != param_multimap.end()) {
			if (std::next(itr, 1)->first != itr->first) {
				param_map.insert(std::make_pair(itr->first, param_v));
				ret.push_back(itr->first);
				param_v.clear();
			}
		}
		else {
			param_map.insert(std::make_pair(itr->first, param_v));
			ret.push_back(itr->first);
			param_v.clear();
		}
	}
	return ret;

}
std::vector<netscan::base_track_t> ranking_cut(std::vector<netscan::base_track_t>base, std::vector<cut_param> cut) {

	std::vector<netscan::base_track_t> ret;
	double dang;
	bool flg = false;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		flg = false;
		dang = pow((itr->ax - itr->m[0].ax), 2.) + pow((itr->ax - itr->m[1].ax), 2.) + pow((itr->ay - itr->m[0].ay), 2.) + pow((itr->ay - itr->m[1].ay), 2.);
		dang = sqrt(dang);
		for (int i = 0; i < cut.size(); i++) {
			if (flg)continue;
			if (dang < cut[i].dangle[0] || cut[i].dangle[1] <= dang)continue;
			flg = true;
			if ((itr->m[0].ph + itr->m[1].ph) % 10000 > cut[i].vph_cut) {
				ret.push_back(*itr);
			}
		}
		if (!flg) {
			//cut条件に入っていない場合
			ret.push_back(*itr);
		}
	}
	fprintf(stderr, "cut %d --> %d\n", base.size(), ret.size());

	return ret;
}
std::vector<netscan::base_track_t> ranking_cut_lateral(std::vector<netscan::base_track_t>base, std::vector<cut_param> cut) {

	std::vector<netscan::base_track_t> ret;
	double dang;
	bool flg = false;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		flg = false;
		dang = pow((itr->ax*itr->m[0].ay - itr->ay*itr->m[0].ax) / sqrt(itr->ax*itr->ax + itr->ay*itr->ay),2.)+ pow((itr->ax*itr->m[1].ay - itr->ay*itr->m[1].ax) / sqrt(itr->ax*itr->ax + itr->ay*itr->ay), 2.);
		//dang = pow((itr->ax - itr->m[0].ax), 2.) + pow((itr->ax - itr->m[1].ax), 2.) + pow((itr->ay - itr->m[0].ay), 2.) + pow((itr->ay - itr->m[1].ay), 2.);
		dang = sqrt(dang);
		for (int i = 0; i < cut.size(); i++) {
			if (flg)continue;
			if (dang < cut[i].dangle[0] || cut[i].dangle[1] <= dang)continue;
			flg = true;
			if ((itr->m[0].ph + itr->m[1].ph) % 10000 > cut[i].vph_cut) {
				ret.push_back(*itr);
			}
		}
		if (!flg) {
			//cut条件に入っていない場合
			ret.push_back(*itr);
		}
	}
	fprintf(stderr, "cut %d --> %d\n", base.size(), ret.size());

	return ret;
}
std::vector<netscan::base_track_t> angle_cut(std::vector<netscan::base_track_t>base, std::vector<netscan::base_track_t>&remain, std::pair<double, double> angle_cut) {
	remain.clear();
	std::vector<netscan::base_track_t>sel;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		angle = angle * 10000;
		angle = round(angle);
		angle = angle / 10000;
		if (angle < angle_cut.first || angle_cut.second <= angle) {
			remain.push_back(*itr);
		}
		else {
			sel.push_back(*itr);
		}
	}
	return sel;

}