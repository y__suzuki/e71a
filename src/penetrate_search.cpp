#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include <functions.hpp>
#include "VxxReader.h"

#include <set>
#include <picojson.h>

class seach_chain {
public:
	int chainid, stream, forward_pl, backward_pl,nseg_threshold,npl_threshold;
	double md_threshold, oa_threshold;
	std::string output;
};
class search_mfile {
public:
	std::string file_in_mfile;
	std::vector<seach_chain> chain;
	std::string output;

};
std::string file_in_mfile_all(std::string filename);
std::vector<search_mfile> read_search_condition(std::string filename);

void penetrate_search(mfile0::Mfile &all, mfile0::Mfile m, seach_chain chain_inf, std::vector<mfile0::M_Chain> &cand);
bool search_chain(std::vector<mfile0::M_Chain> &c, mfile0::M_Chain&ret, int chainid);
std::vector<mfile0::M_Chain> penetrate_check_up(mfile0::M_Chain c, mfile0::Mfile &m_all, seach_chain chain_inf);
std::vector<mfile0::M_Chain>  penetrate_check_down(mfile0::M_Chain c, mfile0::Mfile &m_all, seach_chain chain_inf);

int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "prg param.json\n");
		exit(1);
	}
	std::string file_in_json = argv[1];
	std::string file_in_mfile = file_in_mfile_all(file_in_json);
	printf("%s\n", file_in_mfile.c_str());

	std::vector<search_mfile> param = read_search_condition(file_in_json);

	for (auto itr = param.begin(); itr != param.end(); itr++) {
		printf("%s\n", itr->file_in_mfile.c_str());
		printf("%s\n", itr->output.c_str());
		for (auto itr2 = itr->chain.begin(); itr2 != itr->chain.end(); itr2++) {
			printf("\t%d\n", itr2->chainid);
			printf("\t%d\n", itr2->stream);
			printf("\t%d\n", itr2->forward_pl);
			printf("\t%d\n", itr2->backward_pl);
			printf("\t%lf\n", itr2->md_threshold);
			printf("\t%lf\n", itr2->oa_threshold);
			printf("\t%s\n", itr2->output.c_str());
		}
	}
	
	mfile0::Mfile all;
	mfile1::read_mfile_extension(file_in_mfile, all);
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		mfile0::Mfile m;
		mfile1::read_mfile_extension(itr->file_in_mfile, m);

		mfile0::Mfile cand;
		cand.header = all.header;
		printf("%s\n", itr->file_in_mfile.c_str());
		printf("%s\n", itr->output.c_str());
		for (auto itr2 = itr->chain.begin(); itr2 != itr->chain.end(); itr2++) {
			penetrate_search(all, m, *itr2, cand.chains);
		}
		if (itr->output != "") {
			mfile0::write_mfile(itr->output, cand);
		}
	}
}
std::string file_in_mfile_all(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	picojson::object &all = v.get<picojson::object>();
	std::string file_in_mfile = all["mfile_all"].get<std::string>();

	return file_in_mfile;
}
std::vector<search_mfile> read_search_condition(std::string filename) {
	std::vector<search_mfile> ret;
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	picojson::object &all = v.get<picojson::object>();
	picojson::array &mfile_search = all["mfile_search"].get<picojson::array>();
	for (auto itr = mfile_search.begin(); itr != mfile_search.end(); itr++) {
		search_mfile m;
		picojson::object& obj = itr->get<picojson::object>();
		m.file_in_mfile = obj["mfile_search"].get<std::string>();
		m.output = obj["outputfile"].get<std::string>();
		picojson::array &chain_inf = obj["chain_inf"].get<picojson::array>();
		for (auto itr2 = chain_inf.begin(); itr2 != chain_inf.end(); itr2++) {
			seach_chain c;
			picojson::object& obj_chain = itr2->get<picojson::object>();
			c.chainid = (int)obj_chain["chain_id"].get<double>();
			c.stream = (int)obj_chain["stream"].get<double>();
			c.forward_pl = (int)obj_chain["search_pl"].get<double>();
			c.backward_pl = (int)obj_chain["inverse_pl"].get<double>();
			c.nseg_threshold = (int)obj_chain["nseg_threshold"].get<double>();
			c.npl_threshold = (int)obj_chain["npl_threshold"].get<double>();
			c.md_threshold = obj_chain["md_threshold"].get<double>();
			c.oa_threshold = obj_chain["oa_threshold"].get<double>();
			c.output = obj_chain["outputfile"].get<std::string>();
			m.chain.push_back(c);
		}
		ret.push_back(m);
	}
	return ret;
}

void penetrate_search(mfile0::Mfile &all, mfile0::Mfile m, seach_chain chain_inf, std::vector<mfile0::M_Chain> &cand) {
	mfile0::Mfile out;
	out.header = all.header;

	mfile0::M_Chain c;
	if (!search_chain(m.chains, c, chain_inf.chainid)) {
		fprintf(stderr, "chainID=%d not found\n", chain_inf.chainid);
		exit(1);
	}

	cand.push_back(c);
	out.chains.push_back(c);

	std::vector<mfile0::M_Chain> chain;
	if (chain_inf.stream == 1) chain = penetrate_check_up(c, all, chain_inf);
	else if (chain_inf.stream == -1) chain = penetrate_check_down(c, all, chain_inf);
	else{
		fprintf(stderr, "stream not -1/1\n");
	}

	for(auto itr=chain.begin();itr!=chain.end();itr++){
		cand.push_back(*itr);
		out.chains.push_back(*itr);
	}

	if (chain_inf.output != "") {
		mfile0::write_mfile(chain_inf.output, out, 0);
	}

}
bool search_chain(std::vector<mfile0::M_Chain> &c, mfile0::M_Chain&ret,int chainid) {
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->chain_id== chainid) {
			ret = *itr;
			return true;
		}
	}
	return false;
}
std::vector<mfile0::M_Chain> penetrate_check_up(mfile0::M_Chain c, mfile0::Mfile &m_all, seach_chain chain_inf) {
	std::vector<mfile0::M_Chain> ret;
	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	int pl0, pl1;
	double z0, z1;

	pl0 = c.basetracks.rbegin()->pos / 10;
	pos0.x = c.basetracks.rbegin()->x;
	pos0.y = c.basetracks.rbegin()->y;
	pos0.z = c.basetracks.rbegin()->z;
	dir0.x = mfile0::chain_ax(c);
	dir0.y = mfile0::chain_ay(c);
	dir0.z = 1;

	int pl_min = pl0 - chain_inf.backward_pl;
	int pl_max = pl0 + chain_inf.forward_pl;
	printf("srach downstream PL: PL%03d<--->PL%03d\n", pl_min, pl_max);
	for (auto itr = m_all.chains.begin(); itr != m_all.chains.end(); itr++) {
		pl1 = itr->basetracks.begin()->pos / 10;

		int npl = (itr->pos1 - itr->pos0) / 10 + 1;
		if (pl_min > pl1)continue;
		if (pl_max < pl1)continue;
		if (itr->nseg < chain_inf.nseg_threshold)continue;
		if (npl < chain_inf.npl_threshold)continue;
		if (c.chain_id == itr->chain_id)continue;
		
		pos1.x = itr->basetracks.begin()->x;
		pos1.y = itr->basetracks.begin()->y;
		pos1.z = itr->basetracks.begin()->z;
		dir1.x = mfile0::chain_ax(*itr);
		dir1.y = mfile0::chain_ay(*itr);
		dir1.z = 1;
		double oa, md;
		double extra[2], z_range[2];
		z_range[0] = pos0.z;
		z_range[1] = pos1.z;
		oa = matrix_3D::opening_angle(dir0, dir1);
		md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		if (oa > chain_inf.oa_threshold)continue;
		if (md > chain_inf.md_threshold)continue;
		ret.push_back(*itr);
	}
	return ret;
}
std::vector<mfile0::M_Chain>  penetrate_check_down(mfile0::M_Chain c, mfile0::Mfile &m_all, seach_chain chain_inf) {
	std::vector<mfile0::M_Chain> ret;
	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	int pl0, pl1;
	double z0, z1;

	pl0 = c.basetracks.begin()->pos / 10;
	pos0.x = c.basetracks.begin()->x;
	pos0.y = c.basetracks.begin()->y;
	pos0.z = c.basetracks.begin()->z;
	dir0.x = mfile0::chain_ax(c);
	dir0.y = mfile0::chain_ay(c);
	dir0.z = 1;

	int pl_min = pl0 - chain_inf.forward_pl;
	int pl_max = pl0 + chain_inf.backward_pl;
	for (auto itr = m_all.chains.begin(); itr != m_all.chains.end(); itr++) {
		pl1 = itr->basetracks.rbegin()->pos / 10;
		int npl = (itr->pos1 - itr->pos0) / 10 + 1;

		if (pl_min > pl1)continue;
		if (pl_max < pl1)continue;
		if (itr->nseg < chain_inf.nseg_threshold)continue;
		if (npl < chain_inf.npl_threshold)continue;
		if (c.chain_id == itr->chain_id)continue;
		
		pos1.x = itr->basetracks.rbegin()->x;
		pos1.y = itr->basetracks.rbegin()->y;
		pos1.z = itr->basetracks.rbegin()->z;
		dir1.x = mfile0::chain_ax(*itr);
		dir1.y = mfile0::chain_ay(*itr);
		dir1.z = 1;
		double oa, md;
		double extra[2], z_range[2];
		z_range[0] = pos0.z;
		z_range[1] = pos1.z;
		oa = matrix_3D::opening_angle(dir0, dir1);
		md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		if (oa > chain_inf.oa_threshold)continue;
		if (md > chain_inf.md_threshold)continue;
		ret.push_back(*itr);
	}
	return ret;
}
