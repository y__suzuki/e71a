#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <cassert>  // std::assert
#include <sstream>
#include <iomanip>
#include <string>
#include <numeric>

#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <VxxReader.h>
#include <picojson.h>
#include <omp.h>
#include <time.h>


//function
bool checkFileExistence(const std::string& str);
template <class T> void write_vbin(std::ofstream& ofs, const std::vector<T>& vout);
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin);
void test_calc_delete_pixels(std::vector<cv::Mat> &vmat);
void calc_delete_pixels2(std::vector<cv::Mat> &vmat, int xmin, int xmax, int ymin, int ymax, double cut_area, double cut_line);
double area_sum(cv::Mat &mat, int x, int y, int search_pixel, int picnum);
double line_sum(cv::Mat &mat, int x, int y, int search_pixel, int picnum);
void test_calc_delete_pixels2(std::vector<cv::Mat> &vmat, int xmin, int xmax, int ymin, int ymax);
int half_thread();

//main
int main(int argc, char**argv) {
	clock_t start, fin;
	start = clock();
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-image-folder shot-num out-image-folder\n");
		exit(1);
	}

	std::string file_in_image_folder = argv[1];
	int max_shot = std::stoi(argv[2]);
	std::string file_out_image_folder = argv[3];

	int image_num = 0;
#pragma omp parallel for num_threads(half_thread()) schedule(guided)
	for (int shot = 0; shot < max_shot; shot++) {
		//if (shot != 300)continue;

#pragma omp atomic
		image_num++;
#pragma omp critical
		printf("\r input image %d/%d", image_num, max_shot, image_num*100. / max_shot);

		bool file_flg = false;
		std::string file_path;
		std::string out_filename;
		{
			std::stringstream ss0, ss1;
			ss0 << "\\DATA\\04_03\\TrackHit2_0_" << std::setw(8) << std::setfill('0') << shot << "_0_000.spng";
			ss1 << file_in_image_folder << ss0.str();
			file_path = ss1.str();
			out_filename = ss0.str();
		}
		file_flg = checkFileExistence(file_path);
		if (!file_flg) {
			std::stringstream ss0, ss1;
			ss0 << "\\DATA\\04_03\\TrackHit2_0_" << std::setw(8) << std::setfill('0') << shot << "_1_000.spng";
			ss1 << file_in_image_folder << ss0.str();
			file_path = ss1.str();
			out_filename = ss0.str();
		}
		file_flg = checkFileExistence(file_path);
		if (!file_flg) {
			fprintf(stderr, "image:[%s] not found\n", file_path.c_str());
			continue;
		}
		//画像入力
		std::vector<cv::Mat> vmat;
		std::vector<std::vector<uchar>> vvin;

		read_vbin(file_path, vvin);
		for (int j = 0; j < vvin.size(); j += 1) {
			cv::Mat mat1 = cv::imdecode(vvin[j], 0);
			vmat.emplace_back(mat1);
		}
		//重ね合わせて1枚の画像を作成-->cut部分の決定-->cut
		//calc_delete_pixels(vmat, 520, 605, 0, 1087, 10);
		calc_delete_pixels2(vmat, 1890, 2048, 0, 1087, 0.1, 0.15);
		//test_calc_delete_pixels(vmat);
		//test_calc_delete_pixels2(vmat, 0, 2048, 0, 1088);

		//出力(上書き?)
		//出力ファイル名を作ってるだけ
		std::string out_file_path = file_out_image_folder + out_filename;
		std::ofstream ofs;
		ofs.exceptions(std::ios::failbit | std::ios::badbit);
		ofs.open(out_file_path, std::ios::binary);
		for (int i = 0; i < vmat.size(); i++)
		{
			std::vector<uchar> buf;
			cv::imencode(".png", vmat.at(i), buf);
			write_vbin(ofs, buf);
		}
		ofs.close();

	}
	printf("\r input image %d/%d\n", image_num, max_shot, image_num*100. / max_shot);

	fin = clock();
	printf("all time: %.0lf[s]\n", static_cast<double>(fin - start) / CLOCKS_PER_SEC);

	return 0;
}

//Implementation
bool checkFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}
template <class T> void write_vbin(std::ofstream& ofs, const std::vector<T>& vout)
{
	uint64_t j64 = vout.size();
	if (j64 == 0) { j64 = -1; }
	ofs.write((char*)&j64, sizeof(uint64_t));

	for (auto p = vout.begin(); p != vout.end(); ++p)
	{
		ofs.write((char*)&*p, sizeof(T));
	}
}
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin)
{
	std::ifstream ifs(filepath, std::ios::binary);
	if (!ifs.is_open()) { std::cout << "cannot open " << filepath << std::endl; throw std::exception(); }
	while (true)
	{
		uint64_t i64;
		ifs.read((char*)&i64, sizeof(uint64_t));
		if (ifs.eof()) { break; }

		std::vector<T> vin;
		if (i64 == -1)
		{
			vvin.push_back(vin);
			continue;
		}
		if (i64 > vin.max_size()) { throw std::exception(""); }
		vin.reserve(i64);
		for (uint64_t j = 0; j < i64; j++)
		{
			T p;
			ifs.read((char*)&p, sizeof(T));
			vin.push_back(p);
		}
		vvin.push_back(vin);
	}
	ifs.close();
}

void calc_delete_pixels(std::vector<cv::Mat> &vmat, int xmin, int xmax, int ymin, int ymax, int cut) {
	std::vector<std::pair<char, char>> ret;
	int width = vmat[0].cols;
	int height = vmat[0].rows;
	int plus = 256 / vmat.size();
	cv::Mat image = cv::Mat::zeros(height, width, CV_8U);
	for (int i = 0; i < vmat.size(); i++) {
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				if (vmat[i].at<uchar>(y, x) != 0x00) {
					image.at<uchar>(y, x)++;
				}
			}
		}
	}

	std::set<std::pair<int, int>> del_pixel;
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (xmin <= x && x <= xmax && ymin <= y && y <= ymax) {
				if ((int)image.at<uchar>(y, x) >= cut) {
					del_pixel.insert(std::make_pair(x, y));
				}
			}
		}
	}
	for (auto itr = del_pixel.begin(); itr != del_pixel.end(); itr++) {
		for (int i = 0; i < vmat.size(); i++) {
			image.at<uchar>(itr->second, itr->first) = 0x00;
		}
	}
}
void calc_delete_pixels2(std::vector<cv::Mat> &vmat, int xmin, int xmax, int ymin, int ymax, double cut_area, double cut_line) {
	std::vector<std::pair<char, char>> ret;
	int width = vmat[0].cols;
	int height = vmat[0].rows;
	int plus = 256 / vmat.size();
	cv::Mat image = cv::Mat::zeros(height, width, CV_8U);
	for (int i = 0; i < vmat.size(); i++) {
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				if (vmat[i].at<uchar>(y, x) != 0x00) {
					image.at<uchar>(y, x)++;
				}
			}
		}
	}

	ymin = std::max(0, ymin);
	ymax = std::min(height, ymax);
	xmin = std::max(0, xmin);
	xmax = std::min(width, xmax);

	std::set<std::pair<int, int>> del_pixel_line;
	std::set<std::pair<int, int>> del_pixel_area;
	std::set<std::pair<int, int>> del_pixel_tmp;
	//line上に出ているものを削除
	for (int y = ymin; y < ymax; ++y) {
		for (int x = xmin; x < xmax; ++x) {
			if (line_sum(image, x, y, 3, vmat.size()) >= cut_line) {
				del_pixel_line.insert(std::make_pair(x, y));
			}
		}

	}
	//expansion
	del_pixel_tmp = del_pixel_line;
	for (auto itr = del_pixel_tmp.begin(); itr != del_pixel_tmp.end(); itr++) {
		del_pixel_line.insert(std::make_pair(itr->first, std::min(itr->second + 1, height)));
		del_pixel_line.insert(std::make_pair(itr->first, std::max(itr->second - 1, 0)));
	}

	//面状に出ているものを削除
	for (int y = ymin; y < ymax; ++y) {
		for (int x = xmin; x < xmax; ++x) {
			if (area_sum(image, x, y, 3, vmat.size()) >= cut_area) {
				del_pixel_area.insert(std::make_pair(x, y));
			}
		}
	}
	//expansion
	del_pixel_tmp = del_pixel_area;
	for (auto itr = del_pixel_tmp.begin(); itr != del_pixel_tmp.end(); itr++) {
		for (int ix = -1; ix <= 1; ix++) {
			for (int iy = -1; iy <= 1; iy++) {
				if (itr->first + ix < 0)continue;
				if (itr->first + ix > width - 1)continue;
				if (itr->second + iy < 0)continue;
				if (itr->second + iy > height - 1)continue;
				del_pixel_area.insert(std::make_pair(itr->first + ix, itr->second + iy));
			}
		}
	}
	std::set<std::pair<int, int>> del_pixel = del_pixel_area;
	for (auto itr = del_pixel_line.begin(); itr != del_pixel_line.end(); itr++) {
		del_pixel.insert(*itr);
	}

	//pixelを黒塗り
	for (auto itr = del_pixel.begin(); itr != del_pixel.end(); itr++) {
		for (int i = 0; i < vmat.size(); i++) {
			vmat[i].at<uchar>(itr->second, itr->first) = 0x00;
		}
	}

	////境界塗りつぶし(デバッグ用)
	//for (int y = ymin; y <= ymax; ++y) {
	//	for (int i = 0; i < vmat.size(); i++) {
	//		vmat[i].at<uchar>(y, xmin) = 0x01;
	//		vmat[i].at<uchar>(y, xmax) = 0x01;
	//	}
	//}
	//printf("xmin:%d, xmax:%d, ymin:%d, ymax:%d\n", xmin, xmax, ymin, ymax);
	//printf("width:%d, hight:%d\n", width, height);

	//printf("num=%d\n", del_pixel.size());
}

inline double area_sum(cv::Mat &mat, int x, int y, int search_pixel, int picnum) {
	int max = 0, count = 0;
	int xmin, ymin, xmax, ymax;
	xmin = std::max(x - search_pixel, 0);
	ymin = std::max(y - search_pixel, 0);
	xmax = std::min(x + search_pixel, mat.cols);
	ymax = std::min(y + search_pixel, mat.rows);

	for (int ix = xmin; ix <= xmax; ix++) {
		for (int iy = ymin; iy <= ymax; iy++) {
			count += (int)mat.at<uchar>(iy, ix);
			max += picnum;
		}
	}
	return (double)count / max;
}
inline double line_sum(cv::Mat &mat, int x, int y, int search_pixel, int picnum) {
	int max = 0, count = 0;
	int xmin, ymin, xmax, ymax;
	ymin = std::max(y - search_pixel, 0);
	ymax = std::min(y + search_pixel, mat.rows);
	for (int iy = ymin; iy <= ymax; iy++) {
		count += (int)mat.at<uchar>(iy, x);
		max += picnum;
	}
	return (double)count / max;
}
void test_calc_delete_pixels(std::vector<cv::Mat> &vmat) {
	int width = vmat[0].cols;
	int height = vmat[0].rows;

	int plus = 256 / vmat.size();
	cv::Mat image = cv::Mat::zeros(height, width, CV_8U);
	for (int i = 0; i < vmat.size(); i++) {
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				if (vmat[i].at<uchar>(y, x) != 0x00) {
					image.at<uchar>(y, x)++;
				}
			}
		}
	}
	std::ofstream ofs("out.txt");

	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			//if (xmin <= x && x <= xmax && ymin <= y && y<=ymax) {
			ofs << std::setw(5) << std::setfill(' ') << x << " "
				<< std::setw(5) << std::setfill(' ') << y << " "
				<< std::setw(5) << std::setfill(' ') << (int)image.at<uchar>(y, x) << std::endl;
			//}
			if (image.at<uchar>(y, x) == vmat.size()) {
				image.at<uchar>(y, x) = 0xff;
			}
			else {
				image.at<uchar>(y, x) = image.at<uchar>(y, x)*plus;
			}
		}
	}
	ofs.close();
	std::string out_filename = "out.png";
	cv::imwrite(out_filename, image);

}

void test_calc_delete_pixels2(std::vector<cv::Mat> &vmat, int xmin, int xmax, int ymin, int ymax) {
	std::vector<std::pair<char, char>> ret;
	int width = vmat[0].cols;
	int height = vmat[0].rows;
	int plus = 256 / vmat.size();
	cv::Mat image = cv::Mat::zeros(height, width, CV_8U);
	for (int i = 0; i < vmat.size(); i++) {
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				if (vmat[i].at<uchar>(y, x) != 0x00) {
					image.at<uchar>(y, x)++;
				}
			}
		}
	}

	ymin = std::max(0, ymin);
	ymax = std::min(height, ymax);
	xmin = std::max(0, xmin);
	xmax = std::min(width, xmax);
	std::ofstream ofs("out_line.txt");

	std::set<std::pair<int, int>> del_pixel;
	//line上に出ているものを削除
	for (int y = ymin; y < ymax; ++y) {
		for (int x = xmin; x < xmax; ++x) {
			ofs << std::setw(5) << std::setfill(' ') << x << " "
				<< std::setw(5) << std::setfill(' ') << y << " "
				<< std::setw(6) << std::setprecision(5) << std::setfill(' ') << line_sum(image, x, y, 5, vmat.size()) << std::endl;
		}
	}
	ofs.close();
	ofs.open("out_area.txt");
	//面状に出ているものを削除
	for (int y = ymin; y < ymax; ++y) {
		for (int x = xmin; x < xmax; ++x) {
			ofs << std::setw(5) << std::setfill(' ') << x << " "
				<< std::setw(5) << std::setfill(' ') << y << " "
				<< std::setw(6) << std::setprecision(5) << std::setfill(' ') << area_sum(image, x, y, 5, vmat.size()) << std::endl;
		}
	}
	ofs.close();

}
int half_thread() {
	int num_all_thread = omp_get_max_threads();
	printf("max thread = %d\n", num_all_thread);
	printf("using... %d thread\n", int(num_all_thread*0.5));
	return (int)(num_all_thread * 0.5);
}
