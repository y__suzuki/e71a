#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
std::vector<mfile0::M_Chain> Chain_Selection(std::vector<mfile0::M_Chain> chain, int PL);

int main(int argc, char** argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mfile out-mfile out-txt\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];
	std::string file_out_txt = argv[3];
	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	m.chains = Chain_Selection(m.chains, 4);
	mfile0::write_mfile(file_out_mfile, m);
	std::ofstream ofs(file_out_txt);
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr->chain_id << " "
			<< std::setw(3) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(7) << std::setprecision(4) << mfile0::chain_ax(*itr) << " "
			<< std::setw(7) << std::setprecision(4) << mfile0::chain_ay(*itr) << " "
			<< std::setw(10) << std::setprecision(6) << mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(*itr), mfile0::chain_ay(*itr)) << std::endl;

	}
}
std::vector<mfile0::M_Chain> Chain_Selection(std::vector<mfile0::M_Chain> chain, int PL) {
	std::vector<mfile0::M_Chain> sel0;

	//targetPLの上下にPLがあるか確認
	int count = 0;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		count = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (abs(itr2->pos / 10 - PL) == 1)count++;
			if (abs(itr2->pos / 10 - PL) == 2)count++;
			if (abs(itr2->pos / 10 - PL) == 3)count++;
		}
		if (count == 6) {
			sel0.push_back(*itr);
		}
	}
	fprintf(stderr, "targetPL 3 exist %d --> %d(%4.1lf%%)\n", chain.size(), sel0.size(), sel0.size()*100. / chain.size());

	//nsegカット
	std::vector<mfile0::M_Chain> sel1 = sel0;
	//std::vector<mfile0::M_Chain> sel1;
	//for (auto itr = sel0.begin(); itr != sel0.end(); itr++) {
	//	if (itr->nseg>=5){
	//		sel1.push_back(*itr);
	//	}
	//}
	//fprintf(stderr, "nseg cut >=5 %d --> %d(%4.1lf%%)\n", sel0.size(), sel1.size(), sel1.size()*100. / sel0.size());

	//lateral角度ずれカット
	std::vector<mfile0::M_Chain> sel2 = sel1;
	//std::vector<mfile0::M_Chain> sel2;
	//for (auto itr = sel1.begin(); itr != sel1.end(); itr++) {
	//	if (mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(*itr), mfile0::chain_ay(*itr)) < 0.005) {
	//		sel2.push_back(*itr);
	//	}
	//}
	//fprintf(stderr, "cut lateral deviation < 0.005 %d --> %d(%4.1lf%%)\n", sel1.size(), sel2.size(), sel2.size()*100. / sel1.size());

	//group1本化角度ずれカット
	std::vector<mfile0::M_Chain> sel3;
	std::map<int, mfile0::M_Chain> group_map;
	for (auto itr = sel2.begin(); itr != sel2.end(); itr++) {
		int gid = itr->basetracks.begin()->group_id;
		auto res = group_map.insert(std::make_pair(gid, *itr));
		if (!res.second) {
			//同一keyを持った要素が存在する場合
			if (res.first->second.nseg > itr->nseg)continue;
			else if (res.first->second.nseg < itr->nseg) res.first->second = *itr;
			else {
				double dlat[2];
				dlat[0] = mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(res.first->second), mfile0::chain_ay(res.first->second));
				dlat[1] = mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(*itr), mfile0::chain_ay(*itr));
				if (dlat[0] > dlat[1]) res.first->second = *itr;
				else continue;
			}
		}
	}
	for (auto itr = group_map.begin(); itr != group_map.end(); itr++) {
		sel3.push_back(itr->second);
	}

	fprintf(stderr, "cut group convolution %d --> %d(%4.1lf%%)\n", sel2.size(), sel3.size(), sel3.size()*100. / sel2.size());

	return sel3;

}
