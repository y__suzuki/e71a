#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
void each_pl_range(std::vector<mfile0::M_Chain> &c, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax);
void set_z(std::vector<mfile0::M_Chain> &c, std::map <int, double> &z);

std::vector<mfile0::M_Chain> chain_nseg_cut(std::vector<mfile0::M_Chain>&c, int nseg);
bool connect_judge(mfile0::M_Chain &c0, mfile0::M_Chain &c1);
void SetChainStartStop(std::vector<mfile0::M_Chain>& c, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax, std::map <int, double> &z);
std::vector<mfile0::Mfile> Chain_reconnect(int pl, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map);
void connect_judge(mfile0::M_Chain &c0, mfile0::M_Chain &c1, std::ofstream &ofs);
void Chain_reconnect(int pl, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map, std::string filename);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mfile out-mfile-path out-txt\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];
	std::string file_out_txt = argv[3];
	mfile0::Mfile m, m_ori;
	mfile1::read_mfile_extension(file_in_mfile, m);
	m_ori.header = m.header;
	std::map <int, double> xmin, ymin, xmax, ymax, z;
	each_pl_range(m.chains, xmin, ymin, xmax, ymax);
	set_z(m.chains, z);

	m.chains = chain_nseg_cut(m.chains, 3);
	std::multimap <int, mfile0::M_Chain*> stop_pl;
	std::multimap <int, mfile0::M_Chain*> start_pl;
	SetChainStartStop(m.chains, stop_pl, start_pl, xmin, ymin, xmax, ymax, z);
	std::vector<mfile0::Mfile> m_vec = Chain_reconnect(30, stop_pl, start_pl );
	//Chain_reconnect(30, stop_pl, start_pl, file_out_txt);

	// mfile 出力部
	
	m.chains.clear();
	int num = 0;
	for (int i = 0; i < m_vec.size(); i++) {
		if (m_vec[i].chains.size() == 1) {
			continue;
		}
		m_vec[i].header = m.header;
		std::stringstream file_out;
		file_out << file_out_mfile << "_" << std::setw(3) << std::setfill('0') << num << ".all";
		mfile0::write_mfile(file_out.str(), m_vec[i]);
		for (auto itr = m_vec[i].chains.begin(); itr != m_vec[i].chains.end(); itr++) {
			m.chains.push_back(*itr);
			if (itr == m_vec[i].chains.begin()) {
				m_ori.chains.push_back(*itr);
			}
		}
		num++;
	}
	printf("candidate find %d / %d(%4.1lf%%)\n", num, m_vec.size(), num*100. / m_vec.size());
	std::stringstream file_out;
	file_out << file_out_mfile << ".all";
	mfile0::write_mfile(file_out.str(), m);

	std::stringstream file_out2;
	file_out2 << file_out_mfile << "_ori.all";
	mfile0::write_mfile(file_out2.str(), m_ori);
	

}
void set_z(std::vector<mfile0::M_Chain> &c, std::map <int, double> &z) {
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			z.insert(std::make_pair(itr->pos / 10, itr->z));
		}
	}

}

void each_pl_range(std::vector<mfile0::M_Chain> &c, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax) {
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			auto res1 = xmin.insert(std::make_pair(itr->pos / 10, itr->x));
			auto res2 = xmax.insert(std::make_pair(itr->pos / 10, itr->x));
			auto res3 = ymin.insert(std::make_pair(itr->pos / 10, itr->y));
			auto res4 = ymax.insert(std::make_pair(itr->pos / 10, itr->y));
			if (!res1.second)res1.first->second = std::min(res1.first->second, itr->x);
			if (!res2.second)res2.first->second = std::max(res2.first->second, itr->x);
			if (!res3.second)res3.first->second = std::min(res3.first->second, itr->y);
			if (!res4.second)res4.first->second = std::max(res4.first->second, itr->y);
		}
	}

	auto itr1 = xmin.begin();
	auto itr2 = xmax.begin();
	auto itr3 = ymin.begin();
	auto itr4 = ymax.begin();
	for (auto itr = xmin.begin(); itr != xmin.end(); itr++) {
		if (itr->first != itr1->first || itr->first != itr2->first || itr->first != itr3->first || itr->first != itr4->first) {
			fprintf(stderr, "exception different PL\n");
			printf("xmin:PL%03d xmax:PL%03d ymin:PL%03d ymax:PL%03d\n", itr1->first, itr2->first, itr3->first, itr4->first);
			exit(1);
		}
		printf("PL%03d (%8.1lf, %8.1lf) (%8.1lf, %8.1lf)\n", itr->first, itr1->second, itr2->second, itr3->second, itr4->second);
		itr1++;
		itr2++;
		itr3++;
		itr4++;
	}
	return;
}

std::vector<mfile0::M_Chain> chain_nseg_cut(std::vector<mfile0::M_Chain>&c, int nseg) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->nseg >= nseg) {
			ret.push_back(*itr);
		}
	}

	fprintf(stderr, "nseg cut(>=%d):%d --> %d(%4.1lf%%)\n", nseg, c.size(), ret.size(), ret.size()*100. / c.size());

	return ret;
}
void SetChainStartStop(std::vector<mfile0::M_Chain>& c, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax, std::map <int, double> &z) {
	//edege out cutを入れる

	int start_pl, stop_pl;
	double area_cut = 5000;
	bool flg = true;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		//
		auto start = itr->basetracks.begin();
		auto stop = itr->basetracks.rbegin();
		//start track(最下流)
		if (z.count(start->pos / 10) == 1 && z.count(start->pos / 10 - 1) == 1) {
			flg = true;
			double extra_x = start->x + start->ax*(z[start->pos / 10 - 1] - z[start->pos / 10]);
			double extra_y = start->y + start->ay*(z[start->pos / 10 - 1] - z[start->pos / 10]);
			if (xmin[start->pos / 10 - 1] + area_cut > extra_x)flg = false;
			if (xmax[start->pos / 10 - 1] - area_cut < extra_x)flg = false;
			if (ymin[start->pos / 10 - 1] + area_cut > extra_y)flg = false;
			if (ymax[start->pos / 10 - 1] - area_cut < extra_y)flg = false;
			if (flg) {
				start_pl_map.insert(std::make_pair(start->pos / 10, &(*itr)));
			}
		}
		//stop track(最上流)
		if (z.count(stop->pos / 10) == 1 && z.count(stop->pos / 10 + 1) == 1) {
			flg = true;
			double extra_x = stop->x + stop->ax*(z[stop->pos / 10 + 1] - z[stop->pos / 10]);
			double extra_y = stop->y + stop->ay*(z[stop->pos / 10 + 1] - z[stop->pos / 10]);

			for (int i = 1; i <= 3; i++) {
				if (!flg)break;
				if (xmin.count(stop->pos / 10 + i) == 0)continue;
				if (xmin[stop->pos / 10 + i] + area_cut > extra_x)flg = false;
				if (xmax[stop->pos / 10 + i] - area_cut < extra_x)flg = false;
				if (ymin[stop->pos / 10 + i] + area_cut > extra_y)flg = false;
				if (ymax[stop->pos / 10 + i] - area_cut < extra_y)flg = false;
			}
			if (flg) {
				stop_pl_map.insert(std::make_pair(stop->pos / 10, &(*itr)));
			}
		}

	}
	return;

}
//つながる候補を出力
std::vector<mfile0::Mfile> Chain_reconnect(int pl, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map) {
	std::vector<mfile0::Mfile> m_vec;
	if (stop_pl_map.count(pl) == 0) {
		fprintf(stderr, "not fount stop PL=%03d\n", pl);
		return m_vec;
	}

	auto cand = stop_pl_map.equal_range(pl);
	int count = 0;
	int num = 0;
	for (auto itr = cand.first; itr != cand.second; itr++) {
		if (itr->second->nseg < 10)continue;
		num++;
		count++;
		mfile0::Mfile m;
		m.chains.push_back(*itr->second);
		for (int search_pl = pl; search_pl <= pl + 8; search_pl++) {
			if (start_pl_map.count(search_pl) == 0)continue;
			auto res = start_pl_map.equal_range(search_pl);
			for (auto itr2 = res.first; itr2 != res.second; itr2++) {
				if (connect_judge(*itr->second, *itr2->second)) {
					m.chains.push_back(*itr2->second);
				}
			}

		}
		m_vec.push_back(m);
	}
	return m_vec;
}
//探索のみ、ずれ量をtext出力
void Chain_reconnect(int pl, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map, std::string filename) {
	std::vector<mfile0::Mfile> m_vec;
	if (stop_pl_map.count(pl) == 0) {
		fprintf(stderr, "not fount stop PL=%03d\n", pl);
		return;
	}
	std::ofstream ofs(filename);

	auto cand = stop_pl_map.equal_range(pl);
	int count = 0;
	int num = 0;

	for (auto itr = cand.first; itr != cand.second; itr++) {
		count++;
		if (itr->second->nseg < 10)continue;
		num++;
		printf("\r count %d/%d", count, stop_pl_map.count(pl));

		for (int search_pl = pl; search_pl <= pl + 8; search_pl++) {
			if (start_pl_map.count(search_pl) == 0)continue;
			auto res = start_pl_map.equal_range(search_pl);
			for (auto itr2 = res.first; itr2 != res.second; itr2++) {
				connect_judge(*itr->second, *itr2->second, ofs);
			}
		}
	}
	return;
}
bool connect_judge(mfile0::M_Chain &c0, mfile0::M_Chain &c1) {
	matrix_3D::vector_3D dir0, dir1, pos0, pos1;
	dir0.x = mfile0::chain_ax(c0);
	dir0.y = mfile0::chain_ay(c0);
	dir0.z = 1;

	dir1.x = mfile0::chain_ax(c1);
	dir1.y = mfile0::chain_ay(c1);
	dir1.z = 1;

	pos0.x = c0.basetracks.rbegin()->x;
	pos0.y = c0.basetracks.rbegin()->y;
	pos0.z = c0.basetracks.rbegin()->z;

	pos1.x = c1.basetracks.begin()->x;
	pos1.y = c1.basetracks.begin()->y;
	pos1.z = c1.basetracks.begin()->z;
	double oa, md;
	double range_z[2] = { pos1.z ,pos0.z };
	double extra[2];
	oa = matrix_3D::opening_angle(dir0, dir1);
	md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, range_z, extra);
	if (md > 100)return false;
	if (fabs(oa) > 0.03)return false;
	if (c1.pos0/10==31)return false;
	printf("oa:%lf md %lf\n", oa, md);

	return true;
}
void connect_judge(mfile0::M_Chain &c0, mfile0::M_Chain &c1, std::ofstream &ofs) {
	matrix_3D::vector_3D dir0, dir1, pos0, pos1;
	dir0.x = mfile0::chain_ax(c0);
	dir0.y = mfile0::chain_ay(c0);
	dir0.z = 1;

	dir1.x = mfile0::chain_ax(c1);
	dir1.y = mfile0::chain_ay(c1);
	dir1.z = 1;

	pos0.x = c0.basetracks.rbegin()->x;
	pos0.y = c0.basetracks.rbegin()->y;
	pos0.z = c0.basetracks.rbegin()->z;

	pos1.x = c1.basetracks.begin()->x;
	pos1.y = c1.basetracks.begin()->y;
	pos1.z = c1.basetracks.begin()->z;
	double oa, md;
	double range_z[2] = { pos1.z ,pos0.z };
	double extra[2];
	oa = matrix_3D::opening_angle(dir0, dir1);
	md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, range_z, extra);
	if (md > 1000)return;
	if (fabs(oa) > 1.0)return;
	ofs << std::right << std::fixed
		<< std::setw(12) << std::setprecision(0) << c0.chain_id << " "
		<< std::setw(3) << std::setprecision(0) << c0.pos0 / 10 << " "
		<< std::setw(3) << std::setprecision(0) << c0.pos1 / 10 << " "
		<< std::setw(3) << std::setprecision(0) << c0.nseg << " "
		<< std::setw(7) << std::setprecision(4) << dir0.x << " "
		<< std::setw(7) << std::setprecision(4) << dir0.y << " "
		<< std::setw(12) << std::setprecision(0) << c1.chain_id << " "
		<< std::setw(3) << std::setprecision(0) << c1.pos0 / 10 << " "
		<< std::setw(3) << std::setprecision(0) << c1.pos1 / 10 << " "
		<< std::setw(3) << std::setprecision(0) << c1.nseg << " "
		<< std::setw(7) << std::setprecision(4) << dir1.x << " "
		<< std::setw(7) << std::setprecision(4) << dir1.y << " "
		<< std::setw(6) << std::setprecision(4) << oa << " "
		<< std::setw(6) << std::setprecision(1) << md << std::endl;
	return;
}