#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<netscan::base_track_t>ghost_filter(std::vector<netscan::base_track_t>&base, double dang_r0, double dang_r1, double dang_l, double dpos_r0, double dpos_r1, double dpos_l);
bool sort_ph(const netscan::base_track_t &left, const netscan::base_track_t right) {
	return left.m[0].ph + left.m[1].ph > right.m[0].ph + right.m[1].ph;
}
int main(int argc, char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg input-vxx pl zone output-vxx\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_base = argv[4];

	std::vector<netscan::base_track_t>base;
	netscan::read_basetrack_extension(file_in_base, base, pl, zone);
	base = ghost_filter(base, 0.1, 0.1, 0.05, 5, 10, 5);
	netscan::write_basetrack_vxx(file_out_base, base, pl, zone);
}
std::vector<netscan::base_track_t>ghost_filter(std::vector<netscan::base_track_t>&base, double dang_r0, double dang_r1, double dang_l, double dpos_r0, double dpos_r1, double dpos_l) {

	sort(base.begin(), base.end(), sort_ph);
	double area[4];
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		itr->dmy = 0;
		if (itr == base.begin()) {
			area[0] = itr->x;
			area[1] = itr->x;
			area[2] = itr->y;
			area[3] = itr->y;
		}
		area[0] = std::min(itr->x, area[0]);
		area[1] = std::max(itr->x, area[1]);
		area[2] = std::min(itr->y, area[2]);
		area[3] = std::max(itr->y, area[3]);
	}
	double hash = 500;
	int ix_max, iy_max;
	ix_max = (area[1] - area[0]) / hash + 2;
	iy_max = (area[3] - area[2]) / hash + 2;
	std::vector < std::vector < std::vector<netscan::base_track_t *>>>base_hash;
	for (int i = 0; i < ix_max; i++) {
		std::vector < std::vector<netscan::base_track_t *>> tmp1;
		for (int j = 0; j < iy_max; j++) {
			std::vector<netscan::base_track_t *> tmp2;
			tmp1.push_back(tmp2);
		}
		base_hash.push_back(tmp1);
	}
	int ix, iy;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ix = (itr->x - area[0]) / hash;
		iy= (itr->y - area[2]) / hash;
		base_hash[ix][iy].push_back(&(*itr));
	}

	int count = 0;
	double dang[2], dpos[2],angle,ax,ay;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r calc ghost ...%d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
		}
		count++;
		if (itr->dmy != 0)continue;
		ix = (itr->x - area[0]) / hash;
		iy = (itr->y - area[2]) / hash;
		ax = itr->ax;
		ay = itr->ay;
		angle = sqrt(ax*ax + ay * ay);
		for (int iix = -1; iix <= 1; iix++) {
			if (iix + ix < 0 || ix_max <= iix + ix)continue;
			for (int iiy = -1; iiy <= 1; iiy++) {
				if (iiy + iy < 0 || iy_max <= iiy + iy)continue;
				for (auto itr2 = base_hash[iix + ix][iiy + iy].begin(); itr2 != base_hash[iix + ix][iiy + iy].end(); itr2++) {
					if (itr->rawid == (*itr2)->rawid)continue;
					dang[0] = (((*itr2)->ax - ax)*ax + ((*itr2)->ay - ay)*ay) / angle;
					dang[1] = (((*itr2)->ax - ax)*ay - ((*itr2)->ay - ay)*ax) / angle;
					dpos[0] = (((*itr2)->x - itr->x)*ax + ((*itr2)->y - itr->y)*ay) / angle;
					dpos[1] = (((*itr2)->x - itr->x)*ay - ((*itr2)->y - itr->y)*ax) / angle;
					if (fabs(dang[1]) > dang_l)continue;
					if (fabs(dpos[1]) > dpos_l)continue;
					if (fabs(dang[0]) > dang_r0 + angle * dang_r1)continue;
					if (fabs(dpos[0]) > dpos_r0 + angle * dpos_r1)continue;
					if ((*itr2)->dmy == 1) {
						itr->dmy = 2;
					}
					else {
						(*itr2)->dmy = 2;
					}
				}
			}
		}
		if (itr->dmy == 0) {
			itr->dmy = 1;
		}
	}
	printf("\r calc ghost ...%d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

	std::vector<netscan::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->dmy == 1) {
			ret.push_back(*itr);
		}
	}
	int all = base.size();
	int sel = ret.size();
	printf("ghost filter %d-->%d(%4.1lf%%)\n", all, sel, sel*100. / all);
	return ret;
}


