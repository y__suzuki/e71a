#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
void write_microtrack_bin(std::string filename, std::vector<vxx::micro_track_t> &micro);

int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-fvxx pos out-fvxx\n");
		exit(1);
	}
	std::string file_in_fvxx = argv[1];
	int pos = std::stoi(argv[2]);
	std::string file_out_fvxx = argv[3];

	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> micro = fr.ReadAll(file_in_fvxx, pos, 0);
	write_microtrack_bin(file_out_fvxx, micro);

}
void write_microtrack_bin(std::string filename, std::vector<vxx::micro_track_t> &micro) {
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open ���s
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (micro.size() == 0) {
		fprintf(stderr, "target microtrack ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int64_t count = 0;
		int64_t max = micro.size();

		for (int i = 0; i < max; i++) {
			if (count % 100000 == 0) {
				fprintf(stderr, "\r Write Microtrack ... %d/%d (%4.1lf%%)", count, int(micro.size()), count*100. / micro.size());
			}
			count++;
			ofs.write((char*)& micro[i], sizeof(vxx::micro_track_t));
		}
		fprintf(stderr, "\r Write Microtrack ... %d/%d (%4.1lf%%)\n", count, int(micro.size()), count*100. / micro.size());
	}
}
