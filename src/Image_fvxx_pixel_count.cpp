#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <cassert>  // std::assert
#include <sstream>
#include <iomanip>
#include <string>

#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <VxxReader.h>
#include <picojson.h>
#include <omp.h>
#include <time.h>

class EachImager_Param {
public:
	//CamareaID * 5 + SensorID = ImagerID
	//Width,Height 縦横のpixel数
	double Aff_coef[6], Aff_coef_offset[6], DZ;
	int CameraID, GridX, GridY, Height, ImagerID, SensorID, Width;
	std::string LastReportFilePath;
};
class EachShot_Param {
public:
	int View, Imager, StartAnalysisPicNo, GridX, GridY;
	double Z_begin, X_center, Y_center;
	std::string TrackFilePath;
	//GridX,Y intでダイジョブ?
};
class EachView_Param {
public:
	int  LayerID, NPicThickOfLayer;
	double Stage_x, Stage_y, ThickOfLayer, Z_begin, Z_end;
};
struct microtrack_layer {
	double px_center, py_center, pax, pay;
	int ViewID, CameraID, SensorID, Layernum, pixelnum, hitnum;
	vxx::micro_track_t *m;

};
bool sort_id_layer(const  microtrack_layer &left, const  microtrack_layer &right) {
	if (left.CameraID != right.CameraID) {
		return left.CameraID < right.CameraID;
	}
	else if (left.SensorID != right.SensorID) {
		return left.SensorID < right.SensorID;
	}
	else if (left.ViewID != right.ViewID) {
		return left.ViewID < right.ViewID;
	}
	else {
		return left.Layernum < right.Layernum;
	}
}

//function
std::map<int, EachImager_Param> read_EachImager(std::string filename);
std::vector<EachShot_Param> read_EachShot(std::string filename);
std::map<int, EachView_Param> read_EachView(std::string filename);
std::vector<EachShot_Param> EachShot_center(std::vector<EachShot_Param>shot, std::map<int, EachImager_Param>&imager, std::map<int, EachView_Param>&view, int LayerID);
std::multimap<std::pair<int, int>, EachShot_Param>EachShot_hash(std::vector<EachShot_Param>&shot, double x_width, double y_width, double &x_min, double &y_min);

void microtrack_pixel_convert(vxx::micro_track_t &m, std::vector<microtrack_layer> &m_layer, std::multimap<std::pair<int, int>, EachShot_Param> &shot, std::map<int, EachImager_Param>&imager, std::map<int, EachView_Param>&view, int LayerID, double x_width, double y_width, double x_min, double y_min);
std::pair<bool, microtrack_layer> microtrack_transformation(EachImager_Param imager, EachView_Param view, vxx::micro_track_t &m, int Layer, double dz);
template <class T> void write_vbin(std::ofstream& ofs, const std::vector<T>& vout);
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin);
std::vector<std::pair<int, int>> count_penetrate_pixel(double px_center, double  py_center, double pax, double pay);
std::vector<std::pair<int, int>> expansion(std::vector<std::pair<int, int>> pixel, int expansion);
void pixel_count(cv::Mat &mat, std::multimap<std::pair<int, int>, microtrack_layer*> &m_layer_map);
void Print_track_image(std::vector<cv::Mat> vmat, std::vector<std::vector<microtrack_layer >> track_pixel, std::string filename);

//main
int main(int argc, char**argv) {
	clock_t start, fin_read_file, fin_fvxx_trans, fin_fvxx_clustering, fin_count_pixel,fin;
	start = clock();
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-fvxx pos zone beta_path out-fvxx\n");
		exit(1);
	}
	bool debug_output = true;

	std::string file_in_fvxx = argv[1];
	int pos = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_path_beta = argv[4];
	std::string file_out_fvxx = argv[5];

	int LayerID;
	if (pos % 10 == 1) {
		LayerID = 1;
	}
	else if (pos % 10 == 2) {
		LayerID = 0;
	}
	else {
		fprintf(stderr, "Layer ID exception\n");
		fprintf(stderr, "pos:xx2 -->LayerID = 0\n");
		fprintf(stderr, "pos:xx1 -->LayerID = 1\n");
		exit(1);
	}
	//EachImager:1視野内での各sensorの値 センサー数(72)個
	//EachShot:全視野での各sensorの値 視野数*センサー数(72)個
	//EachView:各視野の値 視野数
	std::string file_in_Beta_EachImagerParam = file_path_beta + "\\Beta_EachImagerParam.json";
	std::string file_in_Beta_EachShotParam = file_path_beta + "\\Beta_EachShotParam.json";
	std::string file_in_Beta_EachViewParam = file_path_beta + "\\Beta_EachViewParam.json";

	std::map<int, EachImager_Param> imager_map = read_EachImager(file_in_Beta_EachImagerParam);
	std::vector<EachShot_Param> shot_vec = read_EachShot(file_in_Beta_EachShotParam);
	std::map<int, EachView_Param> view_map = read_EachView(file_in_Beta_EachViewParam);

	shot_vec = EachShot_center(shot_vec, imager_map, view_map, LayerID);
	double x_min, y_min;
	//sonsorの大きさ,mm
	//大きくすると計算量が増える
	//小さくすると、探索しないAreaが増える
	//領域間の間隔より大きい量
	double x_width = 2048 * 0.45 / 1000;
	double y_width = 1088 * 0.45 / 1000;
	std::multimap<std::pair<int, int>, EachShot_Param>shot_hash = EachShot_hash(shot_vec, x_width, y_width, x_min, y_min);
	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> micro = fr.ReadAll(file_in_fvxx, pos, zone);
	fin_read_file = clock();


	//pxの初期化-->カウントしたpixel数を入れる
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		itr->px = 0;
		itr->py = 0;
	}

	//microtrackの座標変換 pixel 座標へ
	std::vector<microtrack_layer> m_layer;
	m_layer.reserve(micro.size() * 32);
	for (int i = 0; i < micro.size(); i++) {
		if (i % 100000 == 0) {
			printf("\r now calc %d/%d(%4.1lf%%)", i, micro.size(), i*100. / micro.size());
		}
		//if (i > 1000)continue;
		microtrack_pixel_convert(micro[i], m_layer, shot_hash, imager_map, view_map, LayerID, x_width, y_width, x_min, y_min);
	}
	printf("\r now calc %d/%d(%4.1lf%%)\n", micro.size(), micro.size(), micro.size()*100. / micro.size());
	fin_fvxx_trans = clock();

	printf("sort begin :");
	sort(m_layer.begin(), m_layer.end(), sort_id_layer);
	printf(": sort end\n");

	std::map<std::tuple<int, int, int>,std::vector<std::vector<microtrack_layer>>> m_layer_map;
	std::vector<std::tuple<int, int, int>> input_image_list;
	std::tuple<int, int, int>imageID = std::make_tuple(-1, -1, -1);
	int Layer_num = -1;

	int64_t all = m_layer.size();
	std::vector<std::vector<microtrack_layer>> m_map_tmp;

	for (int64_t i = 0; i < all;) {
		if (imageID != std::make_tuple(m_layer[i].CameraID, m_layer[i].SensorID, m_layer[i].ViewID)) {
			if (m_map_tmp.size() > 0) {
				m_layer_map.insert(std::make_pair(imageID, m_map_tmp));
			}
			for (int j = 0; j < m_map_tmp.size(); j++) {
				m_map_tmp[j].clear();
			}
			m_map_tmp.clear();
			std::get<0>(imageID) = m_layer[i].CameraID;
			std::get<1>(imageID) = m_layer[i].SensorID;
			std::get<2>(imageID) = m_layer[i].ViewID;
			input_image_list.push_back(imageID);
		}
		Layer_num = m_layer[i].Layernum;
		std::vector<microtrack_layer >m_layer_v;
		while (i < all&&Layer_num == m_layer[i].Layernum) {
			m_layer_v.push_back(m_layer[i]);
			if (i % 1000000 == 0) {
				fprintf(stderr, "\r microtrack layer clustering %d/%d(%4.1lf%%)", i, all, i*100. / all);
			}
			i++;
		}
		m_map_tmp.push_back( m_layer_v);
	}
	if (m_map_tmp.size() > 0) {
		m_layer_map.insert(std::make_pair(imageID, m_map_tmp));
	}
	for (int j = 0; j < m_map_tmp.size(); j++) {
		m_map_tmp[j].clear();
	}
	m_map_tmp.clear();
	fprintf(stderr, "\r microtrack layer clustering %d/%d(%4.1lf%%)\n", all, all, all*100. / all);
	fin_fvxx_clustering = clock();

	//確認用コード
	if(debug_output){
		std::get<0>(imageID) = 0;
		std::get<1>(imageID) = 0;
		std::get<2>(imageID) = 374;

		std::vector<cv::Mat> vmat1;
		std::string input_image;
		std::stringstream ss;
		ss << file_path_beta << "\\IMAGE\\" << std::setw(2) << std::setfill('0') << std::get<0>(imageID)
			<< "_" << std::setw(2) << std::setfill('0') << std::get<1>(imageID)
			<< "\\ParallelizedBinaryImageFilterPlus_GPU_2_" << std::setw(8) << std::setfill('0') << std::get<2>(imageID)
			<< "_" << std::setw(1) << LayerID << "_044.spng";
		input_image = ss.str();
		std::vector<std::vector<uchar>> vvin;
		read_vbin(input_image, vvin);
		for (int j = 0; j < vvin.size(); j++) {
			cv::Mat mat1 = cv::imdecode(vvin[j], 0);
			vmat1.emplace_back(mat1);
		}
		//layerでloop
		auto m_map = m_layer_map[imageID];
		Print_track_image(vmat1, m_map, "out1");
		return 1;
	}

	 
	int64_t trk_cnt = 0;
	int image_num = 0;
#pragma omp parallel for
	for (int i = 0; i < input_image_list.size(); i++){
#pragma omp atomic
		image_num++;
#pragma omp critical
		printf("\r now calc %d/%d(%4.1lf%%) [input image %d/%d]", trk_cnt, all, trk_cnt*100. / all, image_num, input_image_list.size());
		
		//if (std::get<0>(input_image_list[i]) != 0 || std::get<1>(input_image_list[i]) != 0 || std::get<2>(input_image_list[i]) != 26)continue;
		
		//画像入力
		std::vector<cv::Mat> vmat1;
		std::string input_image;
		std::stringstream ss;
		ss << file_path_beta << "\\IMAGE\\" << std::setw(2) << std::setfill('0') << std::get<0>(input_image_list[i])
			<< "_" << std::setw(2) << std::setfill('0') << std::get<1>(input_image_list[i])
			<< "\\ParallelizedBinaryImageFilterPlus_GPU_2_" << std::setw(8) << std::setfill('0') << std::get<2>(input_image_list[i])
			<< "_" << std::setw(1) << LayerID << "_044.spng";
		input_image = ss.str();
		std::vector<std::vector<uchar>> vvin;
		read_vbin(input_image, vvin);
		for (int j = 0; j < vvin.size(); j++) {
			cv::Mat mat1 = cv::imdecode(vvin[j], 0);
			vmat1.emplace_back(mat1);
		}
		//layerでloop
		std::vector<std::vector<microtrack_layer>> m_map = m_layer_map[input_image_list[i]];
		for (int j = 0; j < m_map.size(); j++) {
			//printf("layer size = %d/%d trk num =%d\n", j, m_map.size(), m_map[j].size());
			int Layer_num = m_map[j].begin()->Layernum;
			std::multimap<std::pair<int, int>, microtrack_layer*> m_count_pixel;
			//microtrack layerでloop
			for (auto itr = m_map[j].begin(); itr != m_map[j].end(); itr++) {
#pragma omp atomic
				trk_cnt++;
				std::vector<std::pair<int, int>> search_pixel = count_penetrate_pixel(itr->px_center, itr->py_center, itr->pax, itr->pay);
				//search_pixel = expansion(search_pixel, 1);
				//printf("search_pixel size=%d ", search_pixel.size());
				//カウントしたいpixel情報を入力
				for (auto itr2 = search_pixel.begin(); itr2 != search_pixel.end(); itr2++) {
					m_count_pixel.insert(std::make_pair(*itr2, &(*itr)));
				}
				//printf("m_count_pixel size = %d\n", m_count_pixel.size());
			}
			//pixel count
			pixel_count(vmat1[Layer_num], m_count_pixel);
			for (auto itr = m_map[j].begin(); itr != m_map[j].end(); itr++) {
				itr->m->px += (float)itr->hitnum;
				itr->m->py += (float)itr->pixelnum;
				//std::cout << std::right << std::fixed
				//	<< std::setw(3) << std::setprecision(0) << itr->CameraID << " "
				//	<< std::setw(3) << std::setprecision(0) << itr->SensorID << " "
				//	<< std::setw(5) << std::setprecision(0) << itr->ViewID << " "
				//	<< std::setw(3) << std::setprecision(0) << itr->Layernum << " "
				//	<< std::setw(6) << std::setprecision(1) << itr->px_center << " "
				//	<< std::setw(6) << std::setprecision(1) << itr->py_center << " "
				//	<< std::setw(7) << std::setprecision(4) << itr->pax << " "
				//	<< std::setw(7) << std::setprecision(4) << itr->pay << " "
				//	<< std::setw(4) << std::setprecision(0) << itr->pixelnum << " "
				//	<< std::setw(4) << std::setprecision(0) << itr->hitnum << " "
				//	<< std::setw(6) << std::setprecision(0) << itr->m->ph << std::endl;
			}
		}
	}
	printf("\r now calc %d/%d(%4.1lf%%) [input image %d/%d]\n", trk_cnt, all, trk_cnt*100. / all, image_num, input_image_list.size());
	fin_count_pixel = clock();

	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		//printf("%12d %8.0lf %8.0lf %7d ", itr->rawid, itr->px, itr->py, itr->ph);
		itr->ph = (itr->ph / 10000) * 10000 + std::min((int)itr->px, 9999);
		//printf("%7d\n", itr->ph);
	}
	vxx::FvxxWriter fw;
	fw.Write(file_out_fvxx, pos, zone, micro);
	fin = clock();
	printf("time read file       %.0lf[s]\n", static_cast<double>(fin_read_file - start) / CLOCKS_PER_SEC);
	printf("time fvxx trans      %.0lf[s]\n", static_cast<double>(fin_fvxx_trans - fin_read_file) / CLOCKS_PER_SEC);
	printf("time fvxx clustering %.0lf[s]\n", static_cast<double>(fin_fvxx_clustering - fin_fvxx_trans) / CLOCKS_PER_SEC);
	printf("time count pixel     %.0lf[s]\n", static_cast<double>(fin_count_pixel - fin_fvxx_clustering) / CLOCKS_PER_SEC);
	printf("all time             %.0lf[s]\n", static_cast<double>(fin - start) / CLOCKS_PER_SEC);
}

//Implementation
std::map<int, EachImager_Param> read_EachImager(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	std::map<int, EachImager_Param> ret;
	//mapのkey=imagerID
	int ImagerID = 0;
	picojson::array &all = v.get<picojson::array>();
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachImager_Param param;
		picojson::array &Aff_coef = obj["Aff_coef"].get<picojson::array>();
		picojson::array &Aff_coef_offset = obj["Aff_coef_offset"].get<picojson::array>();
		int i = 0;
		for (int i = 0; i < 6; i++) {
			param.Aff_coef[i] = Aff_coef[i].get<double>();
			param.Aff_coef_offset[i] = Aff_coef_offset[i].get<double>();
		}
		param.DZ = obj["DZ"].get<double>();
		param.CameraID = (int)obj["CameraID"].get<double>();
		param.GridX = (int)obj["GridX"].get<double>();
		param.GridY = (int)obj["GridY"].get<double>();
		param.Height = (int)obj["Height"].get<double>();
		param.ImagerID = (int)obj["ImagerID"].get<double>();
		param.SensorID = (int)obj["SensorID"].get<double>();
		param.Width = (int)obj["Width"].get<double>();
		param.LastReportFilePath = obj["LastReportFilePath"].get<std::string>();
		ret.insert(std::make_pair(ImagerID, param));
		ImagerID++;
	}
	printf("number of imager = %d\n", ImagerID);
	return ret;
}
std::vector<EachShot_Param> read_EachShot(std::string filename) {
	//JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}

	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	//位置-->shot paramに行けるようにしたい-->全beta.json読み込んだ後でやる
	std::vector<EachShot_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachShot_Param param;
		param.Z_begin = obj["Z_begin"].get<double>();
		param.View = (int)obj["View"].get<double>();
		param.Imager = (int)obj["Imager"].get<double>();
		param.StartAnalysisPicNo = (int)obj["StartAnalysisPicNo"].get<double>();
		param.GridX = (int)obj["GridX"].get<double>();
		param.GridY = (int)obj["GridY"].get<double>();
		param.TrackFilePath = obj["TrackFilePath"].get<std::string>();
		param.X_center = -1;
		param.Y_center = -1;
		ret.push_back(param);
	}
	printf("number of shot = %zd\n", ret.size());

	return ret;
}
std::map<int, EachView_Param> read_EachView(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}
	std::map<int, EachView_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	int ViewID = 0;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachView_Param param;
		param.Stage_x = obj["Stage_x"].get<double>();
		param.Stage_y = obj["Stage_y"].get<double>();
		param.ThickOfLayer = obj["ThickOfLayer"].get<double>();
		param.Z_begin = obj["Z_begin"].get<double>();
		param.Z_end = obj["Z_end"].get<double>();
		param.LayerID = (int)obj["LayerID"].get<double>();
		param.NPicThickOfLayer = (int)obj["NPicThickOfLayer"].get<double>();
		ret.insert(std::make_pair(ViewID, param));
		ViewID++;
	}
	printf("number of view = %d\n", ViewID);
	return ret;
}
std::vector<EachShot_Param> EachShot_center(std::vector<EachShot_Param>shot, std::map<int, EachImager_Param>&imager, std::map<int, EachView_Param>&view, int LayerID) {
	std::vector<EachShot_Param> ret;
	for (auto itr = shot.begin(); itr != shot.end(); itr++) {
		if (view[itr->View].LayerID != LayerID)continue;
		//範囲外アクセス例外処理したほうが良いかも
		//Aff_coef_offsetも?
		itr->X_center = view[itr->View].Stage_x + imager[itr->Imager].Aff_coef[4] + imager[itr->Imager].Aff_coef_offset[4];
		itr->Y_center = view[itr->View].Stage_y + imager[itr->Imager].Aff_coef[5] + imager[itr->Imager].Aff_coef_offset[5];
		ret.push_back(*itr);
	}
	return ret;
}
std::multimap<std::pair<int, int>, EachShot_Param>EachShot_hash(std::vector<EachShot_Param>&shot, double x_width, double y_width, double &x_min, double &y_min) {
	std::multimap<std::pair<int, int>, EachShot_Param>ret;
	for (auto itr = shot.begin(); itr != shot.end(); itr++) {
		if (itr == shot.begin()) {
			x_min = itr->X_center;
			y_min = itr->Y_center;
		}
		x_min = std::min(itr->X_center, x_min);
		y_min = std::min(itr->Y_center, y_min);
	}
	std::pair<int, int> id;
	int ix, iy;
	for (auto itr = shot.begin(); itr != shot.end(); itr++) {
		ix = int((itr->X_center - x_min) / x_width + 0.5);
		iy = int((itr->Y_center - y_min) / y_width + 0.5);
		//printf("%d %d %lf %lf\n", ix, iy, itr->X_center, itr->Y_center);
		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				id.first = ix + iix;
				id.second = iy + iiy;
				ret.insert(std::make_pair(id, *itr));
			}
		}
	}
	return ret;
}

void microtrack_pixel_convert(vxx::micro_track_t &m, std::vector<microtrack_layer> &m_layer,std::multimap<std::pair<int, int>, EachShot_Param> &shot, std::map<int, EachImager_Param>&imager, std::map<int, EachView_Param>&view, int LayerID, double x_width, double y_width, double x_min, double y_min) {
	//microtrack-->各層でのpixel座標に
	double min_dis = 10000;
	double min_shot = -1;
	double dis;
	double x, y;
	std::pair<int, int> id;
	EachShot_Param shot_param;
	//um-->mmへ変換
	x = m.x / 1000;
	y = m.y / 1000;
	id.first = (x - x_min) / x_width;
	id.second = (y - y_min) / y_width;
	//各視野をloop中心の最も近い視野を探す
    //ここのloopはうまくhashとかすれば短縮できそう-->適当にhashした
	if (shot.count(id) == 0) {
		fprintf(stderr, "Not seach view");
		exit(1);
	}
	else if (shot.count(id) == 1) {
		auto res = shot.find(id);
		dis = sqrt((res->second.X_center - x)*(res->second.X_center - x) + (res->second.Y_center - y)*(res->second.Y_center - y));
		if (min_dis > dis) {
			shot_param = res->second;
			min_dis = dis;
		}
	}
	else {
		auto range = shot.equal_range(id);
		for (auto res = range.first; res != range.second; res++) {
			dis = sqrt((res->second.X_center - x)*(res->second.X_center - x) + (res->second.Y_center - y)*(res->second.Y_center - y));
			if (min_dis > dis) {
				shot_param = res->second;
				min_dis = dis;
			}
		}
	}

	if (min_dis > 1) {
		fprintf(stderr, "Not seach view");
		printf("distance = %lf\n", min_dis);
		exit(1);
	}
	//各IDの取得
	int CameraID, SensorID, ViewID;
	CameraID = imager[shot_param.Imager].CameraID;
	SensorID = imager[shot_param.Imager].SensorID;
	ViewID = shot_param.View;
	if (view[shot_param.View].LayerID != LayerID) {
		fprintf(stderr, "Layer ID mismatch\n");
		exit(1);
	}
	//microtrack の変換
	//基準面-->
	double dz = 0;
	for (int Layer = 14; Layer < 32; Layer++) {
		auto res = microtrack_transformation(imager[CameraID * 12 + SensorID], view[ViewID], m, Layer,dz);
		if (res.first) {
			res.second.CameraID = CameraID;
			res.second.SensorID = SensorID;
			res.second.ViewID = ViewID;
			res.second.Layernum += shot_param.StartAnalysisPicNo;
			m_layer.push_back(res.second);
		}
		else {
			//正しいCameraID,SensorID,ViewIDの再探索
			//dzの測定、dzはViewID = shot_param.Viewからどれだけずれているか
			//shot_param-->CameraID,SensorID,ViewIDから再探索-->StartAnalysisPicNoの決定
			
		}
	}
	CameraID = imager[shot_param.Imager].CameraID;
	SensorID = imager[shot_param.Imager].SensorID;
	ViewID = shot_param.View;
	for (int Layer = 13; Layer >= 0; Layer--){
		auto res = microtrack_transformation(imager[CameraID * 12 + SensorID], view[ViewID], m, Layer, dz);
		if (res.first) {
			res.second.CameraID = CameraID;
			res.second.SensorID = SensorID;
			res.second.ViewID = ViewID;
			res.second.Layernum += shot_param.StartAnalysisPicNo;
			m_layer.push_back(res.second);
		}
		else {
			//正しいCameraID,SensorID,ViewIDの再探索
			//dzの測定、dzはViewID = shot_param.Viewからどれだけずれているか
		}
	}
}
std::pair<bool, microtrack_layer> microtrack_transformation(EachImager_Param imager, EachView_Param view, vxx::micro_track_t &m, int Layer, double dz) {
	//stage-->pixel 座標へ
	//角度は回転のみ
	double px, py, pax, pay, x_tmp, y_tmp;
	//1層の厚み*層数*角度/(pixel length)+dz補正
	x_tmp = m.x - (view.ThickOfLayer / view.NPicThickOfLayer * (Layer - 14) + dz * 1000)*m.ax;
	y_tmp = m.y - (view.ThickOfLayer / view.NPicThickOfLayer * (Layer - 14) + dz * 1000)*m.ay;

	//um --> mm & shift成分の計算
	x_tmp = x_tmp / 1000 - imager.Aff_coef[4] - imager.Aff_coef_offset[4] - view.Stage_x;
	y_tmp = y_tmp / 1000 - imager.Aff_coef[5] - imager.Aff_coef_offset[5] - view.Stage_y;
	double factor = 1.0 / (imager.Aff_coef[0] * imager.Aff_coef[3] - imager.Aff_coef[1] * imager.Aff_coef[2]);
	px = factor * (imager.Aff_coef[3] * x_tmp - imager.Aff_coef[1] * y_tmp);
	py = factor * (imager.Aff_coef[0] * y_tmp - imager.Aff_coef[2] * x_tmp);
	px = px + 2048 / 2;
	py = py + 1088 / 2;
	//角度の変換 回転のみ
	//fabsとっていい?
	factor = sqrt(fabs(factor));
	pax = factor * (imager.Aff_coef[3] * m.ax - imager.Aff_coef[1] * m.ay);
	pay = factor * (imager.Aff_coef[0] * m.ay - imager.Aff_coef[2] * m.ax);

	microtrack_layer ret;
	ret.CameraID = 0;
	ret.Layernum = Layer;
	ret.m = &m;
	ret.pax = pax;
	ret.pay = pay;
	ret.px_center = px;
	ret.py_center = py;
	ret.SensorID = 0;
	ret.ViewID = 0;
	ret.pixelnum = 0;
	ret.hitnum = 0;
	double px_min, px_max, py_min, py_max;
	double cut = 50;
	px_min = 0 + cut;
	px_max = 2048 - cut;
	py_min = 0 + cut;
	py_max = 1088 - cut;

	if (px_min < px&&px < px_max&&py_min < py&&py < py_max) {
		return std::make_pair(true, ret);
	}
	return std::make_pair(false, ret);
}
template <class T> void write_vbin(std::ofstream& ofs, const std::vector<T>& vout)
{
	uint64_t j64 = vout.size();
	if (j64 == 0) { j64 = -1; }
	ofs.write((char*)&j64, sizeof(uint64_t));

	for (auto p = vout.begin(); p != vout.end(); ++p)
	{
		ofs.write((char*)&*p, sizeof(T));
	}
}
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin)
{
	std::ifstream ifs(filepath, std::ios::binary);
	if (!ifs.is_open()) { std::cout << "cannot open " << filepath << std::endl; throw std::exception(); }
	while (true)
	{
		uint64_t i64;
		ifs.read((char*)&i64, sizeof(uint64_t));
		if (ifs.eof()) { break; }

		std::vector<T> vin;
		if (i64 == -1)
		{
			vvin.push_back(vin);
			continue;
		}
		if (i64 > vin.max_size()) { throw std::exception(""); }
		vin.reserve(i64);
		for (uint64_t j = 0; j < i64; j++)
		{
			T p;
			ifs.read((char*)&p, sizeof(T));
			vin.push_back(p);
		}
		vvin.push_back(vin);
	}
	ifs.close();
}
inline std::vector<std::pair<int, int>> count_penetrate_pixel(double px_center, double  py_center, double pax, double pay) {

	//始点と終点を結んだ直線
	//pixel+0.5でpixelの中心?
	//被写界深度4um,radial方向の探索範囲に相当
	std::vector<std::pair<int, int>> ret;
	double	Depth_of_field = 4;
	//始点pixelと終点pixelの決定
	int px_i, py_i, px_e, py_e;
	if (pax > 0) {
		px_i = int(floor(px_center - Depth_of_field * pax));
		px_e = int(ceil(px_center + Depth_of_field * pax));
	}
	else {
		px_i = int(floor(px_center + Depth_of_field * pax));
		px_e = int(ceil(px_center - Depth_of_field * pax));
	}
	if (pay > 0) {
		py_i = int(floor(py_center - Depth_of_field * pay));
		py_e = int(ceil(py_center + Depth_of_field * pay));
	}
	else {
		py_i = int(floor(py_center + Depth_of_field * pay));
		py_e = int(ceil(py_center - Depth_of_field * pay));
	}
	//slope=pay/paxとするとpax<<1の時大変
	//この方法は (px_e-px_i)>=1 が保証
	double slope, intercept;
	if (pax*pay > 0) {
		slope = (double)(py_e - py_i) / (px_e - px_i);
		intercept = py_i - slope * px_i;
	}
	else {
		slope = -1 * (double)(py_e - py_i) / (px_e - px_i);
		intercept = py_i - slope * px_e;
	}

	for (int px = px_i; px <= px_e; px++) {
		if (slope > 0) {
			py_i = int(floor(slope*px + intercept));
			py_e = int(ceil(slope*(px + 1) + intercept));
		}
		else {
			py_i = int(floor(slope*(px + 1) + intercept));
			py_e = int(ceil(slope*px + intercept));
		}
		for (int py = py_i; py <= py_e; py++) {
			ret.push_back(std::make_pair(px, py));
		}
	}
	//printf("%.1lf %.1lf %5.4lf %5.4lf\n", px_center, py_center, pax, pay);
	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	printf("%d %d\n", itr->first, itr->second);
	//}
	return ret;
}
inline std::vector<std::pair<int, int>> expansion(std::vector<std::pair<int, int>> pixel, int expansion) {
	std::set<std::pair<int, int>> after_expansion;
	for (auto itr = pixel.begin(); itr != pixel.end(); itr++) {
		for (int px = -1 * expansion + itr->first; px <= expansion + itr->first; px++) {
			for (int py = -1 * expansion + itr->second; py <= expansion + itr->second; py++) {
				after_expansion.insert(std::make_pair(px, py));
			}
		}
	}
	std::vector<std::pair<int, int>> ret;
	for (auto itr = after_expansion.begin(); itr != after_expansion.end(); itr++) {
		ret.push_back(*itr);
	}
	return ret;
}
void pixel_count(cv::Mat &mat, std::multimap<std::pair<int, int>, microtrack_layer*> &m_layer_map) {
	int rows = mat.rows;
	int cols = mat.cols;
	for (auto itr = m_layer_map.begin(); itr != m_layer_map.end(); itr++) {
		if (itr->first.first >= cols)continue;
		if (itr->first.second >= rows)continue;
		if (itr->first.first < 0)continue;
		if (itr->first.second < 0)continue;
		itr->second->pixelnum++;
		//白(0x01がHit)
		if (mat.at<uchar>(itr->first.second, itr->first.first) == 0x01) {
			itr->second->hitnum++;
		}
		//printf("(%5d,%5d) pixel = %d pixelnum= %d hitnum=%d\n", itr->first.first, itr->first.second, (int)mat.at<uchar>(itr->first.second, itr->first.first), itr->second->pixelnum, itr->second->hitnum);
	}
}
//確認用,実行時には使用しない
void Print_track_image(std::vector<cv::Mat> vmat, std::vector<std::vector<microtrack_layer >> track_pixel, std::string filename) {

	std::vector<cv::Mat> vmat_color;
	int rows, cols;
	for (int i = 0; i < vmat.size(); i++) {
		rows = vmat[i].rows;
		cols = vmat[i].cols;
		cv::Mat image = cv::Mat::zeros(rows, cols, CV_8UC3);
		for (int y = 0; y < vmat[i].rows; ++y) {
			for (int x = 0; x < vmat[i].cols; ++x) {
				if (vmat[i].at<uchar>(y, x) == 0x00) {
					image.at<cv::Vec3b>(y, x)[0] = 0x00; //青
					image.at<cv::Vec3b>(y, x)[1] = 0x00; //緑
					image.at<cv::Vec3b>(y, x)[2] = 0x00; //赤
				}
				else {
					image.at<cv::Vec3b>(y, x)[0] = 0xff; //青
					image.at<cv::Vec3b>(y, x)[1] = 0xff; //緑
					image.at<cv::Vec3b>(y, x)[2] = 0xff; //赤
				}
			}
		}
		vmat_color.push_back(image);
	}

	std::set<std::pair<int, int>>trace_pixel;
	std::set<std::pair<int, int>>frame_pixel;
	for (int j = 0; j < track_pixel.size(); j++) {
		//printf("layer size = %d/%d trk num =%d\n", j, m_map.size(), m_map[j].size());
		int Layer_num = track_pixel[j].begin()->Layernum;
		std::multimap<std::pair<int, int>, microtrack_layer*> m_count_pixel;
		//microtrack layerでloop
		for (auto itr = track_pixel[j].begin(); itr != track_pixel[j].end(); itr++) {
			std::vector<std::pair<int, int>> search_pixel = count_penetrate_pixel(itr->px_center, itr->py_center, itr->pax, itr->pay);
			//search_pixel = expansion(search_pixel, 1);
			for (auto itr2 = search_pixel.begin(); itr2 != search_pixel.end(); itr2++) {
				m_count_pixel.insert(std::make_pair(*itr2, &(*itr)));
			}

			trace_pixel.clear();
			frame_pixel.clear();
			//trace_pixelにデータを入れる
			for (auto itr2 = search_pixel.begin(); itr2 != search_pixel.end(); itr2++) {
				trace_pixel.insert(*itr2);
			}
			//frame_pixelにデータを入れる
			for (auto itr2 = search_pixel.begin(); itr2 != search_pixel.end(); itr2++) {
				for (int ix = -1; ix <= 1; ix++) {
					for (int iy = -1; iy <= 1; iy++) {
						if (trace_pixel.count(std::make_pair(itr2->first + ix, itr2->second + iy)) == 0) {
							if (itr2->first + ix < 0)continue;
							if (itr2->second + iy < 0)continue;
							if (itr2->first + ix > cols - 1)continue;
							if (itr2->second + iy > rows - 1)continue;
							frame_pixel.insert(std::make_pair(itr2->first + ix, itr2->second + iy));
						}
					}
				}
			}
			//vmat_color[Layer_num]のframepixelを赤にする
			for (auto itr2 = frame_pixel.begin(); itr2 != frame_pixel.end(); itr2++) {
				vmat_color[Layer_num].at<cv::Vec3b>(itr2->second, itr2->first)[0] = 0x00; //青
				vmat_color[Layer_num].at<cv::Vec3b>(itr2->second, itr2->first)[1] = 0x00; //緑
				vmat_color[Layer_num].at<cv::Vec3b>(itr2->second, itr2->first)[2] = 0xff; //赤
			}
		}
		pixel_count(vmat[Layer_num], m_count_pixel);

	}

	//png出力
	std::string out_filename;
	for (int k = 0; k < vmat_color.size(); k++) {
		{
			std::stringstream ss;
			ss << filename << "_" << std::setfill('0') << std::setw(3) << k << ".png";
			out_filename = ss.str();
		}
		cv::imwrite(out_filename, vmat_color[k]);
	}

	//microtrack dataの出力
	std::stringstream ss;
	ss << filename << ".txt";
	out_filename = ss.str();
	std::ofstream ofs(out_filename);
	for (int j = 0; j < track_pixel.size(); j++) {
		for (auto itr = track_pixel[j].begin(); itr != track_pixel[j].end(); itr++) {
			ofs << std::right << std::fixed
				<< std::setw(3) << std::setprecision(0) << itr->CameraID << " "
				<< std::setw(3) << std::setprecision(0) << itr->SensorID << " "
				<< std::setw(5) << std::setprecision(0) << itr->ViewID << " "
				<< std::setw(3) << std::setprecision(0) << itr->Layernum << " "
				<< std::setw(6) << std::setprecision(1) << itr->px_center << " "
				<< std::setw(6) << std::setprecision(1) << itr->py_center << " "
				<< std::setw(7) << std::setprecision(4) << itr->pax << " "
				<< std::setw(7) << std::setprecision(4) << itr->pay << " "
				<< std::setw(4) << std::setprecision(0) << itr->pixelnum << " "
				<< std::setw(4) << std::setprecision(0) << itr->hitnum << " "
				<< std::setw(6) << std::setprecision(0) << itr->m->ph << std::endl;
		}
	}
}