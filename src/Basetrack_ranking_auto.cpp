#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class Base_rank{
public :
	netscan::base_track_t *b;
	int  vph,flg,ph;
	double angle,d_lat,d_xy;
	//コンストラクタ
	Base_rank(netscan::base_track_t base) {
		angle = sqrt(base.ax*base.ax + base.ay*base.ay);
		vph = int((base.m[0].ph + base.m[1].ph) % 10000);
		ph = int((base.m[0].ph + base.m[1].ph) / 10000);
		d_xy = sqrt(pow(base.ax - base.m[0].ax, 2) + pow(base.ay - base.m[0].ay, 2) + pow(base.ax - base.m[1].ax, 2) + pow(base.ay - base.m[1].ay, 2));
		d_lat = sqrt(pow((base.ax*base.m[0].ay - base.ay*base.m[0].ax) / angle, 2) + pow((base.ax*base.m[1].ay - base.ay*base.m[1].ax) / angle, 2));
		flg = 1;
	}

};

bool sort_base_angle(const Base_rank &left, const Base_rank &right) {
	return left.angle < right.angle;
}
bool sort_base_d_xy(const Base_rank *left, const Base_rank *right) {
	return left->d_xy < right->d_xy;
}
bool sort_base_vph(const Base_rank *left, const Base_rank *right) {
	return left->vph < right->vph;
}
bool sort_base_ph(const Base_rank *left, const Base_rank *right) {
	return left->ph < right->ph;
}
std::vector<Base_rank> Set_base_rank(std::vector<netscan::base_track_t> &base);
std::vector<Base_rank> VPHoffset(std::vector<Base_rank> &base, int offset0, int offset1, int offset2, int offset3, int offset4, int offset5, int offset6,int offset7);
void VPHoffsetReverse(std::vector<Base_rank> &base, int offset0, int offset1, int offset2, int offset3, int offset4, int offset5, int offset6, int offset7);
std::vector<std::vector<Base_rank*>> Base_hash_angle(std::vector<Base_rank> &base);

std::vector<Base_rank> TrackRankingSelection_lateral_all(std::vector<Base_rank> &base);
std::vector<std::vector<Base_rank*> > Base_hash_VPH(std::vector<Base_rank*> &base, int vph_step, int &vph);

void TrackRankingSelection_xy(std::vector<std::vector<Base_rank*> > base);
void TrackRankingSelection_lateral(std::vector<Base_rank*>  base);
std::vector<netscan::base_track_t> Base_Selection(std::vector<Base_rank> base_cut);

double GetMean_da(std::vector<Base_rank*> base);
double GetRMS_da(std::vector<Base_rank*> base, double da_mean);
void GetRange_vph(std::vector<Base_rank*> base, int *vph_rth1, int *vph_rth2, int k);
double GetMean_vph(std::vector<Base_rank*> base);
double GetRMS_vph(std::vector<Base_rank*> base, double vph_mean);
int GetVPHth(std::vector<Base_rank*> base, double vph_mean, double vph_rms, int vph_min_noise, double *vphth);
int Signal_PH(std::vector<Base_rank*>  base);

int main(int argc, char *argv[])
{
	if (argc != 13) {
		printf("引数が違います\n");
		printf("prg [input-bvxx] [output-bvxx] [PL] [zone] [VPH_offset (0.0-0.1)] [VPH_offset (0.1-0.2)] [VPH_offset (0.2-0.3)] [VPH_offset (0.3-0.4)] [VPH_offset (0.4-0.6)] [VPH_offset (0.6-1.1)] [VPH_offset (1.1-1.5)] [VPH_offset (1.5-)]\n");
		printf("prg b044.vxx b044.sel.vxx 30 25 20 15 10 10 10\n");
		printf("\n");
		exit(1);
	}
	std::string in_file_base = argv[1];
	std::string out_file_base = argv[2];
	int pl = atoi(argv[3]);
	int zone = atoi(argv[4]);
	int offset0 = atoi(argv[5]);
	int offset1 = atoi(argv[6]);
	int offset2 = atoi(argv[7]);
	int offset3 = atoi(argv[8]);
	int offset4 = atoi(argv[9]);
	int offset5 = atoi(argv[10]);
	int offset6 = atoi(argv[11]);
	int offset7 = atoi(argv[11]);
	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(in_file_base, base, pl, zone);

	std::vector<Base_rank> base_rank = Set_base_rank(base);



	std::vector<Base_rank> base_cut = VPHoffset(base_rank, offset0, offset1, offset2, offset3, offset4, offset5, offset6, offset7);
	base_cut = TrackRankingSelection_lateral_all(base_cut);
	VPHoffsetReverse(base_cut, offset0, offset1, offset2, offset3, offset4, offset5, offset6, offset7);

	std::vector<std::vector<Base_rank*>> base_hash = Base_hash_angle(base_cut);

	//ranking cut
	TrackRankingSelection_xy(base_hash);


	std::vector<netscan::base_track_t> base_out= Base_Selection(base_cut);
	printf("base %d --> %d\n", base.size(), base_out.size());
	netscan::write_basetrack_vxx(out_file_base, base_out, pl, zone);
	//netscan::write_basetrack_txt(out_file_base, base_out);
}

std::vector<Base_rank> Set_base_rank(std::vector<netscan::base_track_t> &base) {
	std::vector<Base_rank> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		Base_rank base_tmp(*itr);
		base_tmp.b = &(*itr);
		ret.push_back(base_tmp);
	}
	return ret;
}
std::vector<Base_rank> VPHoffset(std::vector<Base_rank> &base, int offset0, int offset1, int offset2, int offset3, int offset4, int offset5, int offset6, int offset7)
{
	std::vector<Base_rank> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (int(itr->angle * 10) == 0) {
			if (itr->vph % 10000 < offset0)continue;
			else {
				itr->vph -= offset0;
			}
		}
		else if (int(itr->angle * 10) == 1) {
			if (itr->vph % 10000 < offset1)continue;
			else {
				itr->vph -= offset1;
			}
		}
		else if (int(itr->angle * 10) == 2) {
			if (itr->vph % 10000 < offset2)continue;
			else {
				itr->vph -= offset2;
			}
		}
		else if (int(itr->angle * 10) == 3) {
			if (itr->vph % 10000 < offset3)continue;
			else {
				itr->vph -= offset3;
			}
		}
		else if (4 <= int(itr->angle * 10) && int(itr->angle * 10) < 6) {
			if (itr->vph % 10000 < offset4)continue;
			else {
				itr->vph -= offset4;
			}
		}
		else if (6 <= int(itr->angle * 10) && int(itr->angle * 10) < 11) {
			if (itr->vph % 10000 < offset5)continue;
			else {
				itr->vph -= offset5;
			}
		}
		else if (11 <= int(itr->angle * 10) && int(itr->angle * 10) < 15) {
			if (itr->vph % 10000 < offset6)continue;
			else {
				itr->vph -= offset6;
			}
		}
		else if (15 <= int(itr->angle * 10) ) {
			if (itr->vph % 10000 < offset7)continue;
			else {
				itr->vph -= offset7;
			}
		}
		ret.push_back(*itr);
	}
	return ret;
}
void VPHoffsetReverse(std::vector<Base_rank> &base, int offset0, int offset1, int offset2, int offset3, int offset4, int offset5, int offset6, int offset7)
{
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (int(itr->angle * 10) == 0) itr->vph += offset0;
		else if (int(itr->angle * 10) == 1) itr->vph += offset1;
		else if (int(itr->angle * 10) == 2) itr->vph += offset2;
		else if (int(itr->angle * 10) == 3) itr->vph += offset3;
		else if (4 <= int(itr->angle * 10) && int(itr->angle * 10) < 6) itr->vph += offset4;
		else if (6 <= int(itr->angle * 10) && int(itr->angle * 10) < 11) itr->vph += offset5;
		else if (11 <= int(itr->angle * 10) && int(itr->angle * 10) < 15) itr->vph += offset6;
		else if (15 <= int(itr->angle * 10) ) itr->vph += offset7;
	}
}
std::vector<std::vector<Base_rank*>> Base_hash_angle(std::vector<Base_rank> &base){
	sort(base.begin(), base.end(), sort_base_angle);
	std::vector<std::vector<Base_rank*>> ret;

	int angle,now_angle = -1;
	std::vector<Base_rank*> base_hash;

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (now_angle != int(itr->angle * 10) && int(itr->angle * 10) < 16) {
			if (now_angle != -1)ret.push_back(base_hash);
			base_hash.clear();
			now_angle = int(itr->angle * 10);
		}
		base_hash.push_back(&(*itr));
	}
	ret.push_back(base_hash);
	return ret;

}
//noiseの有無を判定
//phで輪切り
//くぼんだ所をfit
void TrackRankingSelection_xy(std::vector<std::vector<Base_rank*> > base) {
	for (int k = 0; k < std::min(int(base.size() - 1),15); k++) {
		int vph_min;
		int vph_step = 20;

		std::vector<std::vector<Base_rank*> > base_hash_vph = Base_hash_VPH(base[k], vph_step, vph_min);
		std::vector<std::pair<double, double>> thrshold;
		for (int j = 0; j < base_hash_vph.size(); j++) {

			//hashのbin幅を決める
			std::vector<Base_rank*> b;
			std::vector<std::vector<Base_rank*>>hash_da_2;
			double signal_da_max = (k / 10. * 0.08 + 0.02) * 1.5;
			double bin = signal_da_max / 10;

			bool noise_flg = true;
			bool flg = false;
			int signal_peak_i, noise_peak_i;
			int PH_min = 32;
			for (auto itr = base_hash_vph[j].begin(); itr != base_hash_vph[j].end(); itr++) {
				PH_min = std::min((*itr)->ph, PH_min);
			}
			PH_min -= 1;
			while (noise_flg) {
				PH_min++;
				b.clear();
				for (auto itr = base_hash_vph[j].begin(); itr != base_hash_vph[j].end(); itr++) {
					if ((*itr)->vph < PH_min)continue;
					//vph高いところは無視(noiseが少ない)
					if (k == 0 && (*itr)->vph > 200) continue;
					if (k == 1 && (*itr)->vph > 180) continue;
					if (k == 2 && (*itr)->vph > 160) continue;
					if (k == 3 && (*itr)->vph > 140) continue;
					if (k == 4 && (*itr)->vph > 120) continue;
					if (k >= 5 && (*itr)->vph > 100) continue;
					b.push_back(*itr);
				}
				if (b.size() < 100) {
					flg = true;
					break;
				}
				noise_flg = false;

				double damax = (*b.begin())->d_xy;
				for (auto itr = b.begin(); itr != b.end(); itr++) {
					damax = std::max(damax, (*itr)->d_xy);
				}
				int damax_i = damax / bin + 1;
				//signal領域にしかtrackが無い
				if (damax_i <= 15) {
					flg = true;
					break;
				}

				std::vector<std::vector<Base_rank*> >hash_da;
				for (int i = 0; i < damax_i; i++) {
					std::vector<Base_rank*> hash_tmp;
					hash_da.push_back(hash_tmp);
				}
				for (auto itr = b.begin(); itr != b.end(); itr++) {
					hash_da[int((*itr)->d_xy / bin)].push_back(*itr);
				}

				signal_peak_i = 0;
				int signal_peak = hash_da[signal_peak_i].size();
				for (int i = 0; i < 10; i++) {
					if (signal_peak < hash_da[i].size()) {
						signal_peak = hash_da[i].size();
						//i==9のとき
						//noise only
						//phcutを上げてやり直し
						signal_peak_i = i;
						if (i == 9) {
							noise_flg == true;
						}
					}

				}

				noise_peak_i = hash_da.size() - 1;
				int noise_peak = hash_da[noise_peak_i].size();
				for (int i = noise_peak_i; i >= 15; i--) {
					if (noise_peak < hash_da[i].size()) {
						noise_peak = hash_da[i].size();
						noise_peak_i = i;
						//i==10のとき
						//signal only
						//終了
						if (i == 15) {
							flg = true;
							break;
						}
					}
				}
				if (flg)break;
				if (!noise_flg) {
					hash_da_2 = hash_da;
				}
			}
			if (flg)continue;

			int thrshold_da_i = signal_peak_i;
			int thrshold_da = hash_da_2[thrshold_da_i].size();
			for (int i = signal_peak_i; i <= noise_peak_i; i++) {
				//printf("%d %d\n", i, hash_da_2[i].size());
				if (thrshold_da > hash_da_2[i].size()) {
					thrshold_da = hash_da_2[i].size();
					thrshold_da_i = i;
				}
			}

			double vph_thr = vph_min + (j + 0.5) * vph_step;
			double da_thr = (thrshold_da_i + 0.5) * bin;
			thrshold.push_back(std::make_pair(da_thr, vph_thr));
		}
		if (thrshold.size() <= 2)continue;
		//thrsholdを最小二乗法でfit
		double slope, intercept;
		double x = 0, y = 0, xx = 0, xy = 0, n = 0;
		for (auto itr = thrshold.begin(); itr != thrshold.end(); itr++) {
			x += itr->first;
			xx += itr->first*itr->first;
			xy += itr->first*itr->second;
			y += itr->second;
			n++;
		}

		slope = (n*xy + x * y) / (n*xx + x * x);
		intercept = (xx*y - xy * x) / (n*xx + x * x);

		double vph_mean;
		double vph_rms;
		std::vector<Base_rank*> b;
		for (auto itr = base[k].begin(); itr != base[k].end(); itr++) {
			if ((*itr)->vph > slope*(*itr)->d_xy + intercept) {
				b.push_back(*itr);
			}
		}
		vph_mean = GetMean_vph(b);
		vph_rms = GetRMS_vph(b, vph_mean);
		for (auto itr = base[k].begin(); itr != base[k].end(); itr++) {
			if ((*itr)->vph < slope*(*itr)->d_xy + intercept && (*itr)->vph < vph_mean+vph_rms) (*itr)->flg = 0;
		}
		printf("cut param: %2.1lf<angle<%2.1lf\n", k*0.1, (k + 1)*0.1);
		printf("vph > d_angle*%5.4lf + %5.1lf\n", slope, intercept);
		printf("vph > %5.1lf\n", vph_mean + vph_rms);

	}
}
std::vector<std::vector<Base_rank*> > Base_hash_VPH(std::vector<Base_rank*> &base, int vph_step, int &vph) {
	std::vector<std::vector<Base_rank*> >ret;
	int vphmin = (*base.begin())->vph;
	int vphmax = (*base.begin())->vph;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		vphmin = std::min(vphmin, (*itr)->vph);
		vphmax = std::max(vphmax, (*itr)->vph);
	}
	int hash = vph_step;
	vphmin = vphmin / hash;
	vphmax = vphmax / hash + 1 - vphmin;
	vph = vphmin * hash;
	for (int i = 0; i < vphmax; i++) {
		std::vector<Base_rank*> hash_tmp;
		ret.push_back(hash_tmp);
	}
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ret[(*itr)->vph / hash - vphmin].push_back(*itr);
	}
	return ret;
}
int Signal_PH(std::vector<Base_rank*>  base) {

	std::map<int, int> ph_distribution;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		auto res = ph_distribution.insert(std::make_pair((*itr)->ph, 1));
		if (!res.second) {
			res.first->second++;
		}
	}
	int peak1 = 0, peak2 = 0;
	for (auto itr = ph_distribution.rbegin(); itr != ph_distribution.rend(); itr++) {
		if (std::next(itr, 1) == ph_distribution.rend())continue;
		if (itr == ph_distribution.rbegin())continue;
		if (peak1 == 0 && itr->first > std::next(itr, 1)->first)peak1 = itr->first;
		if (peak1 != 0 && itr->first < std::next(itr, 1)->first) {
			peak2 = itr->first;
			break;
		}
	}
	if (peak1 != 0 && peak2 != 0)return peak1 - 1;
	else {
		return (ph_distribution.rbegin()->first + ph_distribution.begin()->first) / 2 + 1;
	}
}





void TrackRankingSelection_lateral(std::vector<Base_rank*>  base) {

	std::vector<Base_rank*>  b;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (((*itr)->b->m[0].ph + (*itr)->b->m[1].ph) / 10000 >= 23)b.push_back(*itr);
	}
	double vph_mean = GetMean_vph(b);
	double vph_rms = GetRMS_vph(b, vph_mean);
	b.clear();
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (fabs(vph_mean - (*itr)->vph) < vph_rms)b.push_back(*itr);
	}
	vph_mean = GetMean_vph(b);

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if ((*itr)->d_lat > 0.05) {
			(*itr)->flg = 0;
		}
		if ((*itr)->d_lat > 0.03 && (*itr)->vph < vph_mean) {
			(*itr)->flg = 0;
		}
		if ((*itr)->d_lat <= 0.03 && (*itr)->vph < (*itr)->d_lat * 25 / 0.03 ) {
			(*itr)->flg = 0;
		}
	}
}
std::vector<Base_rank> TrackRankingSelection_lateral_all(std::vector<Base_rank> &base) {
	std::vector<Base_rank>  ret;

	std::vector<std::vector<Base_rank*>> base_hash = Base_hash_angle(base);
	for (int i = 0; i < base_hash.size(); i++) {

		std::vector<Base_rank *> b = base_hash[i];
		std::vector<Base_rank *> bt;
		int ph_th = 0;
		if (i < 5)ph_th = Signal_PH(b);
		ph_th = std::max(ph_th, 23);

		for (auto itr = b.begin(); itr != b.end(); itr++) {
			if ((*itr)->ph >= ph_th)bt.push_back((*itr));
		}
		double vph_mean = GetMean_vph(bt);
		double vph_rms = GetRMS_vph(bt, vph_mean);
		bt.clear();
		for (auto itr = b.begin(); itr != b.end(); itr++) {
			if (fabs(vph_mean - (*itr)->vph) < vph_rms)bt.push_back(*itr);
		}
		vph_mean = GetMean_vph(bt);
		bt.clear();
		for (auto itr = b.begin(); itr != b.end(); itr++) {
			if (i < 5) {
				if ((*itr)->d_lat > 0.05) (*itr)->flg = 0;
				if ((*itr)->d_lat > 0.04 && (*itr)->vph < vph_mean)(*itr)->flg = 0;
				if ((*itr)->d_lat <= 0.04 && (*itr)->vph < (*itr)->d_lat * 25 / 0.03*0.6) (*itr)->flg = 0;
			}
			else {
				if ((*itr)->d_lat > 0.05) (*itr)->flg = 0;
				if ((*itr)->d_lat > 0.03 && (*itr)->vph < vph_mean)(*itr)->flg = 0;
				if ((*itr)->d_lat <= 0.03 && (*itr)->vph < (*itr)->d_lat * 25 / 0.03) (*itr)->flg = 0;
			}
		}
		b.clear();
	}
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->flg == 1) {
			ret.push_back(*itr);
		}
	}
	return ret;
}


std::vector<netscan::base_track_t> Base_Selection(std::vector<Base_rank> base_cut) {
	std::vector<netscan::base_track_t> ret;
	for (auto itr = base_cut.begin(); itr != base_cut.end(); itr++) {
		//printf("%5.4lf %5.4lf %8.1lf %8.1lf\n", itr->b->ax, itr->b->ay, itr->b->x, itr->b->y);
		if (itr->flg == 1)ret.push_back(*(itr->b));
	}
	return ret;
}



double GetMean_da(std::vector<Base_rank*> base) {
	double da_sum = 0.0;
	double da_mean;

	for (int i = 0; i < base.size(); i++) {
		da_sum += base[i]->d_xy;
	}
	da_mean = da_sum / (double)base.size();

	return da_mean;
}
double GetRMS_da(std::vector<Base_rank*> base, double da_mean) {
	double dda, da_rms;
	double dda_sum = 0;

	for (int i = 0; i < base.size(); i++) {

		dda = base[i]->d_xy - da_mean;

		dda_sum += dda * dda;
	}
	da_rms = sqrt(dda_sum / (double)base.size());

	return da_rms;
}
void GetRange_vph(std::vector<Base_rank*> base, int *vph_rth1, int *vph_rth2, int k)
{
	int i, j;
	std::vector<int> vph;
	for (j = 0; j < 40; j++) vph.push_back(0);

	double dath = 0.01 + 0.003*(double)k;
	int vphth = (int)(10.0 - (double)k / 3);

	for (i = 0; i < base.size(); i++) {
		for (j = 0; j < 40; j++) {
			if (base[i]->d_xy < dath && base[i]->vph > vphth) {
				if ((int)((double)base[i]->vph / 4) == j) {
					vph[j] += 1;
				}
			}
		}
	}

	int vphmax;
	int vphmaxnum = -1;
	for (j = 0; j < 40; j++) {
		if (vph[j] > vphmaxnum) {
			vphmaxnum = vph[j];
			vphmax = j;
		}
	}
	int vphrangenum = (int)((double)vphmaxnum / 4);
	int vphrange1, vphrange2;

	for (j = vphmax; j < 40; j++) {
		if (vph[j] < vphrangenum) {
			vphrange1 = j;
			break;
		}
	}
	for (j = vphmax; j >= 0; j--) {
		if (vph[j] < vphrangenum) {
			vphrange2 = j;
			break;
		}
	}

	(*vph_rth1) = vphrange1 * 4 + 2;
	(*vph_rth2) = vphrange2 * 4 + 2;
}
double GetMean_vph(std::vector<Base_rank*> base)
{
	double vph_sum = 0;
	double vph_mean;

	for (int i = 0; i < base.size(); i++) {

		vph_sum += base[i]->vph;
	}
	vph_mean = vph_sum / (double)base.size();

	return vph_mean;
}

double GetRMS_vph(std::vector<Base_rank*> base, double vph_mean)
{
	double dvph, vph_rms;
	double dvph_sum = 0;

	for (int i = 0; i < base.size(); i++) {

		dvph = (double)base[i]->vph - vph_mean;

		dvph_sum += dvph * dvph;
	}
	vph_rms = sqrt(dvph_sum / (double)base.size());

	return vph_rms;
}
int GetVPHth(std::vector<Base_rank*> base, double vph_mean, double vph_rms, int vph_min_noise, double *vphth)
{
	int flg = 0;
	int i, j;
	int k = 0;
	for (i = 0; i < base.size(); i++) {

		if (base[i]->vph > vph_mean - vph_rms && base[i]->vph <= vph_mean) k++;
	}
	double vph_signal = (double)k / vph_rms;

	k = 0;
	int vph_min = 100000;
	if (vph_min_noise == 0) {
		for (i = 0; i < base.size(); i++) {
			if (vph_min > base[i]->vph) vph_min = base[i]->vph;
		}
		for (i = 0; i < base.size(); i++) {
			if (base[i]->vph == vph_min || base[i]->vph == vph_min + 1) k++;
		}
	}
	if (vph_min_noise != 0) {
		for (i = 0; i < base.size(); i++) {
			if (base[i]->vph <= vph_min_noise) k++;
		}
		vph_min = vph_min_noise;
	}
	double vph_noise = (double)k / 2;

	if (vph_noise >= vph_signal * 2.5) flg = 1;

	int vph_fit = (int)(vph_mean - vph_rms * 2);
	int n = vph_fit - vph_min + 1;
	if (n <= 1) flg = 0;

	if (flg == 1) {

		std::vector<int> num;
		std::vector<int> vph;

		for (j = 0; j < n; j++) num.push_back(0);
		for (j = 0; j < n; j++) vph.push_back(vph_min + j);

		for (i = 0; i < base.size(); i++) {
			for (j = 0; j < n; j++) {
				if (base[i]->vph < vph_min) num[0] += 1;
				if (base[i]->vph == vph[j]) num[j] += 1;
			}
		}

		//		for(j=0;j<n;j++) printf("%d %d\n",vph[j],num[j]);

		double a, b;
		int xx, xy, yy, x, y;
		xx = 0; xy = 0; yy = 0; x = 0; y = 0;
		for (j = 0; j < n; j++) {
			xx += vph[j] * vph[j];
			xy += vph[j] * num[j];
			yy += num[j] * num[j];
			x += vph[j];
			y += num[j];
		}

		a = ((double)n*xy - (double)x*y) / ((double)n*xx - (double)x*x);
		b = ((double)xx*y - (double)x*xy) / ((double)n*xx - (double)x*x);
		(*vphth) = (-1)*b / a;
	}

	return flg;
}




