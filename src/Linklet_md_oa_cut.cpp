#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>


std::vector<netscan::linklet_t> oa_cut(std::vector<netscan::linklet_t> link, double thr);
std::vector<netscan::linklet_t> md_cut(std::vector<netscan::linklet_t> link, double thr);

int main(int argc, char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-linklet(bin) out-linklet(bin) oa-thr md-thr");
		exit(1);
	}
	std::string file_in_link = argv[1];
	std::string file_out_link = argv[2];
	double thr_oa = std::stod(argv[3]);
	double thr_md = std::stod(argv[4]);

	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_bin(file_in_link, link);

	link = oa_cut(link, thr_oa);
	link = md_cut(link, thr_md);

	netscan::write_linklet_bin(file_out_link, link);

}
std::vector<netscan::linklet_t> oa_cut(std::vector<netscan::linklet_t> link, double thr) {
	using namespace matrix_3D;

	std::vector<netscan::linklet_t>ret;
	double d_lat;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		vector_3D pos0, pos1, dir0, dir1;
		pos0.x = itr->b[0].x;
		pos0.y = itr->b[0].y;
		pos0.z = itr->b[0].z;
		dir0.x = itr->b[0].ax;
		dir0.y = itr->b[0].ay;
		dir0.z = 1;
		pos1.x = itr->b[1].x;
		pos1.y = itr->b[1].y;
		pos1.z = itr->b[1].z;
		dir1.x = itr->b[1].ax;
		dir1.y = itr->b[1].ay;
		dir1.z = 1;
		if (opening_angle(dir0, dir1) > thr)continue;
		ret.push_back(*itr);
	}
	printf("opening angle cut <= %5.4lf %d --> %d(%4.1lf%%)\n", thr, link.size(), ret.size(), ret.size()*100. / link.size());
	return ret;
}
std::vector<netscan::linklet_t> md_cut(std::vector<netscan::linklet_t> link, double thr) {
	using namespace matrix_3D;

	std::vector<netscan::linklet_t>ret;
	double d_lat;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		vector_3D pos0, pos1, dir0, dir1;
		pos0.x = itr->b[0].x;
		pos0.y = itr->b[0].y;
		pos0.z = itr->b[0].z;
		dir0.x = itr->b[0].ax;
		dir0.y = itr->b[0].ay;
		dir0.z = 1;
		pos1.x = itr->b[1].x;
		pos1.y = itr->b[1].y;
		pos1.z = itr->b[1].z;
		dir1.x = itr->b[1].ax;
		dir1.y = itr->b[1].ay;
		dir1.z = 1;
		double z_range[2], extra[2];
		z_range[0] = pos0.z;
		z_range[1] = pos1.z;
		if (minimum_distance(pos0, pos1, dir0, dir1, z_range, extra) > thr)continue;
		ret.push_back(*itr);
	}
	printf("minimum distance cut <= %5.4lf %d --> %d(%4.1lf%%)\n", thr, link.size(), ret.size(), ret.size()*100. / link.size());
	return ret;
}
