#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>

mfile0::Mfile merge_mfile(std::vector<mfile0::Mfile> &m_v);

int main(int argc, char **argv) {
	if (argc < 3) {
		fprintf(stderr, "prg out-mfile num mfile-1 mfile2 ...\n");
		exit(1);
	}
	std::string file_out_mfile = argv[1];
	int num = std::stoi(argv[2]);
	if (argc !=num+3) {
		fprintf(stderr, "prg out-mfile num mfile-1 mfile2 ...\n");
		exit(1);
	}
	std::vector<std::string> file_in_mfile;
	for (int i = 3; i < 3 + num; i++) {
		file_in_mfile.push_back(argv[i]);
	}

	std::vector<mfile0::Mfile>m_v;
	for (int i = 0; i < file_in_mfile.size(); i++) {
		mfile0::Mfile m;
		mfile1::read_mfile_extension(file_in_mfile[i], m);
		m_v.push_back(m);
	}

	mfile0::Mfile m=merge_mfile(m_v);

	mfile1::write_mfile_extension(file_out_mfile, m);

}
mfile0::Mfile merge_mfile(std::vector<mfile0::Mfile> &m_v) {
	mfile0::Mfile m = *(m_v.begin());

	std::set<int> pos;
	for (auto itr = m_v.begin(); itr != m_v.end(); itr++) {
		for (auto itr2 = itr->header.all_pos.begin(); itr2 != itr->header.all_pos.end(); itr2++) {
			pos.insert(*itr2);
		}
	}

	m.header.all_pos.clear();
	for (auto itr = pos.begin(); itr != pos.end(); itr++) {
		m.header.all_pos.push_back(*itr);
	}
	m.header.num_all_plate = m.header.all_pos.size();

	m.chains.clear();
	int64_t chain_id = 0, gid = 0;

	for (auto itr = m_v.begin(); itr != m_v.end(); itr++) {
		for (auto itr2 = itr->chains.begin(); itr2 != itr->chains.end(); itr2++) {
			itr2->chain_id = chain_id;
			chain_id++;

			for (auto itr3 = itr2->basetracks.begin(); itr3 != itr2->basetracks.end(); itr3++) {
				itr3->group_id = gid;
			}
			if ((itr2 + 1) != itr->chains.end() && itr2->basetracks.begin()->group_id != (itr2 + 1)->basetracks.begin()->group_id) {
				gid++;
			}
			else if ((itr2 + 1) == itr->chains.end()) {
				gid++;
			}

			m.chains.push_back(*itr2);
		}

	}

	return m;
}
