#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>
std::vector<mfile0::M_Chain> chain_nseg_selection(std::vector<mfile0::M_Chain> &chain, int nseg);
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold);
std::vector<mfile0::M_Chain> reject_Fe_ECC(std::vector<mfile0::M_Chain> &chain);

std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>  &chain);
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold);
std::vector<mfile0::M_Chain> chain_angle_selection(std::vector<mfile0::M_Chain>  &chain, double thr_ax, double thr_ay);
void CalcEfficiency(std::ofstream &ofs_eff, std::ofstream &ofs_pred, std::ofstream &ofs_id, std::vector<mfile0::M_Chain>  chain, int PL, std::map<int, double> zmap);
void CalcEfficiency2(std::ofstream &ofs_eff, std::ofstream &ofs_pred, std::ofstream &ofs_id, std::vector<mfile0::M_Chain>  chain, int PL, std::map<int, double> zmap);
int angle_divide(double angle);

struct Prediction{
	int PL, flg,rawid;
	double ax, ay, x, y;
};
int main(int argc, char **argv) {
	if (argc !=6) {
		fprintf(stderr, "usage:prg in-mfile out-file(eff) out-file(prediction) out_exist_base_id_list PLmax\n");
		exit(1);
	}

	//input value
	std::string file_in_mfile = argv[1];
	std::string file_out_eff = argv[2];
	std::string file_out_prediction = argv[3];
	std::string file_out_id_list = argv[4];
	int PLmax = std::stoi(argv[5]);
	//mfile
	mfile0::Mfile m;

	//read mfile
	mfile1::read_mfile_extension(file_in_mfile, m);
	//zmap
	std::map<int, double> zmap;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			zmap.insert(std::make_pair(itr2->pos / 10, itr2->z));
		}
	}
	m.chains = chain_nseg_selection(m.chains, 15);
	m.chains = reject_Fe_ECC(m.chains);
	m.chains = chain_angle_selection(m.chains, 4.0, 4.0);
	m.chains = chain_dlat_selection(m.chains, 0.005);
	m.chains = group_clustering(m.chains);
	std::ofstream ofs_eff(file_out_eff);
	std::ofstream ofs_pred(file_out_prediction);
	std::ofstream ofs_id(file_out_id_list);
	for (int PL = 4; PL <= PLmax; PL++) {
		CalcEfficiency(ofs_eff, ofs_pred, ofs_id, m.chains, PL, zmap);
	}
}
std::vector<mfile0::M_Chain> chain_nseg_selection(std::vector<mfile0::M_Chain> &chain, int nseg) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr->nseg < nseg)continue;
		ret.push_back(*itr);
	}
	printf("chain nseg >= %d: %d --> %d (%4.1lf%%)\n", nseg, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::M_Chain> reject_Fe_ECC(std::vector<mfile0::M_Chain> &chain) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr->pos0 / 10 > 15) {
			ret.push_back(*itr);
		}
		else {
			int flg = 0;
			for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
				if (itr2->pos / 10 == 16)flg++;
				if (itr2->pos / 10 == 17)flg++;
				if (itr2->pos / 10 == 18)flg++;
				if (itr2->pos / 10 == 19)flg++;
				if (itr2->pos / 10 == 20)flg++;
				if (itr2->pos / 10 == 21)flg++;
				if (itr2->pos / 10 == 22)flg++;
				if (itr2->pos / 10 == 23)flg++;
			}
			if(flg>=6|| itr->pos1 / 10 > 23){
				ret.push_back(*itr);
			}
		}
	}
	printf("reject_Fe_ECC(PL1 >= 24): %d --> %d (%4.1lf%%)\n", chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>  &chain) {

	std::vector<mfile0::M_Chain> ret;
	std::set<int>gid;
	std::multimap<int, mfile0::M_Chain*>group;
	int nseg_max;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		gid.insert(itr->basetracks[0].group_id);
		group.insert(std::make_pair(itr->basetracks[0].group_id, &(*itr)));
	}
	for (auto itr = gid.begin(); itr != gid.end(); itr++) {
		if (group.count(*itr) == 1) {
			ret.push_back(*(group.find(*itr)->second));
		}
		else {
			auto range = group.equal_range(*itr);
			nseg_max = 1;
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				nseg_max = std::max(itr2->second->nseg, nseg_max);
			}
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (itr2->second->nseg == nseg_max) {
					ret.push_back(*(itr2->second));
					break;
				}
			}
		}
	}
	printf("chain group clustering: %d --> %d (%4.1lf%%)\n", chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;

}
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (mfile0::angle_diff_dev_lat(*itr, ax, ay) > threshold)continue;
		ret.push_back(*itr);
	}
	printf("chain lateral selection <= %5.4lf : %d --> %d (%4.1lf%%)\n", threshold, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;

}
std::vector<mfile0::M_Chain> chain_angle_selection(std::vector<mfile0::M_Chain>  &chain, double thr_ax, double thr_ay) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (thr_ax - fabs(ax) < 0)continue;
		if (thr_ay - fabs(ay) < 0)continue;
		ret.push_back(*itr);
	}
	printf("chain average angle selection |ax|<=%3.1lf |ay|<=%3.1lf : %d --> %d (%4.1lf%%)\n", thr_ax, thr_ay, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
void CalcEfficiency(std::ofstream &ofs_eff, std::ofstream &ofs_pred, std::ofstream &ofs_id, std::vector<mfile0::M_Chain>  chain, int PL, std::map<int, double> zmap) {

	std::vector<mfile0::M_Chain> chain_sel;
	int flg = 0;
	double dz;
	int PL_min = 4;
	int PL_max = 60;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		flg = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (PL_min == PL) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 6) {
					flg++;
				}
			}
			else if (PL_min+1 == PL) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 5) {
					flg++;
				}
			}
			else if (PL_min + 2 == PL) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 4) {
					flg++;
				}
			}
			else if (PL_min + 3<=PL&&PL<=PL_max-3) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 3) {
					flg++;
				}
			}
			else if (PL_max-2 == PL) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 4) {
					flg++;
				}
			}
			else if (PL_max-1 == PL) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 5) {
					flg++;
				}
			}
			else if(PL_max==PL){
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 6) {
					flg++;
				}
			}
		}
		if (flg == 6) {
			chain_sel.push_back(*itr);
		}
	}

	printf("PL=%d prediction selection: %d --> %d (%4.1lf%%)\n", PL, chain.size(), chain_sel.size(), chain_sel.size()*100. / chain.size());
	if (chain_sel.size() == 0) {
		printf("PL%03d no prediction track\n", PL);
		return;
	}
	std::vector<Prediction> pred;
	for (auto itr = chain_sel.begin(); itr != chain_sel.end(); itr++) {
		Prediction pred_tmp;
		pred_tmp.PL = PL;
		pred_tmp.ax = mfile0::chain_ax(*itr);
		pred_tmp.ay = mfile0::chain_ay(*itr);
		pred_tmp.flg = 0;
		pred_tmp.rawid = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if ( (PL >= 5 && PL <= 15) || (PL >= 16 && PL % 2 == 1)) {
				if (itr2->pos / 10 == PL - 1) {
					pred_tmp.x = itr2->x + pred_tmp.ax*(zmap[PL] - zmap[PL - 1]);
					pred_tmp.y = itr2->y + pred_tmp.ay*(zmap[PL] - zmap[PL - 1]);
				}
			}
			else if (PL == 4  || (PL >= 16 && PL % 2 == 0)) {
				if (itr2->pos / 10 == PL + 1) {
					pred_tmp.x = itr2->x + pred_tmp.ax*(zmap[PL] - zmap[PL + 1]);
					pred_tmp.y = itr2->y + pred_tmp.ay*(zmap[PL] - zmap[PL + 1]);
				}
			}
			else {
				fprintf(stderr, "PL exception PL%03d\n", PL);
			}

			if (itr2->pos / 10 == PL) {
				pred_tmp.flg = 1;
				pred_tmp.rawid = itr2->rawid;
			}
		}
		pred.push_back(pred_tmp);
	}


	double xmin, xmax, ymin, ymax;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		if (itr == pred.begin()) {
			xmin = itr->x;
			xmax = itr->x;
			ymin = itr->y;
			ymax = itr->y;
		}
		xmin = std::min(itr->x, xmin);
		xmax = std::max(itr->x, xmax);
		ymin = std::min(itr->y, ymin);
		ymax = std::max(itr->y, ymax);
	}
	xmin = xmin + 5000;
	xmax = xmax - 5000;
	ymin = xmin + 3000;
	ymax = ymax - 3000;

	std::vector<Prediction> pred_sel;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		if (itr->x < xmin)continue;
		if (itr->x > xmax)continue;
		if (itr->y < ymin)continue;
		if (itr->y > ymax)continue;
		pred_sel.push_back(*itr);
	}
	if (pred_sel.size() == 0) {
		printf("PL%03d no prediction track\n", PL);
		return;
	}


	const int NUM = 16;
	int all[NUM] = {}, count[NUM] = {};
	double eff[NUM], eff_err[NUM];
	double angle;
	for (auto itr = pred_sel.begin(); itr != pred_sel.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		all[angle_divide(angle)]++;
		if (itr->flg == 1) {
			count[angle_divide(angle)]++;
		}
		ofs_pred << std::right << std::fixed << std::setfill(' ')
			<< std::setw(3) << std::setprecision(0) << itr->PL << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->x << " "
			<< std::setw(8) << std::setprecision(1) << itr->y << " "
			<< std::setw(2) << std::setprecision(0) << itr->flg << std::endl;
		if (itr->flg == 1) {
			ofs_id << std::right << std::fixed << std::setfill(' ')
				<< std::setw(3) << std::setprecision(0) << itr->PL << " "
				<< std::setw(12) << std::setprecision(0) << itr->rawid << " "
				<< std::setw(7) << std::setprecision(4) << itr->ax << " "
				<< std::setw(7) << std::setprecision(4) << itr->ay << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << std::endl;

		}
	}

	for (int i = 0; i < 13; i++) {
		eff[i] = double(count[i]) / all[i];
		eff_err[i] = sqrt(count[i] * eff[i] * (1 - eff[i])) / all[i];
		ofs_eff << std::right << std::fixed << std::setfill(' ')
			<< std::setw(4) << std::setprecision(0) << PL << " ";

		if (i == 0)ofs_eff << "0.0 0.1 ";
		if (i == 1)ofs_eff << "0.1 0.3 ";
		if (i == 2)ofs_eff << "0.3 0.5 ";
		if (i == 3)ofs_eff << "0.5 0.7 ";
		if (i == 4)ofs_eff << "0.7 0.9 ";
		if (i == 5)ofs_eff << "0.9 1.1 ";
		if (i == 6)ofs_eff << "1.1 1.3 ";
		if (i == 7)ofs_eff << "1.3 1.5 ";
		if (i == 8)ofs_eff << "1.5 2.0 ";
		if (i == 9)ofs_eff << "2.0 2.5 ";
		if (i == 10)ofs_eff << "2.5 3.0 ";
		if (i == 11)ofs_eff << "3.0 3.5 ";
		if (i == 12)ofs_eff << "3.5 4.0 ";
		if (i == 13)ofs_eff << "4.0 4.5 ";
		if (i == 14)ofs_eff << "4.5 5.0 ";
		if (i == 15)ofs_eff << "5.0 10.0 ";

		ofs_eff << std::setw(8) << std::setprecision(0) << all[i] << " "
			<< std::setw(8) << std::setprecision(0) << count[i] << " "
			<< std::setw(5) << std::setprecision(4) << eff[i] << " "
			<< std::setw(5) << std::setprecision(4) << eff_err[i] << std::endl;

	}

}
void CalcEfficiency2(std::ofstream &ofs_eff, std::ofstream &ofs_pred, std::ofstream &ofs_id, std::vector<mfile0::M_Chain>  chain, int PL, std::map<int, double> zmap) {

	std::vector<mfile0::M_Chain> chain_sel;
	int flg = 0;
	double dz;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		flg = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (PL == 38) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 6) {
					flg++;
				}
			}
			if (PL == 39) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 6) {
					flg++;
				}
			}
		}
		if (flg == 5) {
			chain_sel.push_back(*itr);
		}
	}

	printf("PL=%d prediction selection: %d --> %d (%4.1lf%%)\n", PL, chain.size(), chain_sel.size(), chain_sel.size()*100. / chain.size());
	if (chain_sel.size() == 0) {
		printf("PL%03d no prediction track\n", PL);
		return;
	}
	std::vector<Prediction> pred;
	for (auto itr = chain_sel.begin(); itr != chain_sel.end(); itr++) {
		Prediction pred_tmp;
		pred_tmp.PL = PL;
		pred_tmp.ax = mfile0::chain_ax(*itr);
		pred_tmp.ay = mfile0::chain_ay(*itr);
		pred_tmp.flg = 0;
		pred_tmp.rawid = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (PL == 2 || (PL >= 4 && PL <= 15) || (PL >= 16 && PL % 2 == 1)) {
				if (itr2->pos / 10 == PL - 1) {
					pred_tmp.x = itr2->x + pred_tmp.ax*(zmap[PL] - zmap[PL - 1]);
					pred_tmp.y = itr2->y + pred_tmp.ay*(zmap[PL] - zmap[PL - 1]);
				}
			}
			else if (PL == 1 || PL == 3 || (PL >= 16 && PL % 2 == 0)) {
				if (itr2->pos / 10 == PL + 1) {
					pred_tmp.x = itr2->x + pred_tmp.ax*(zmap[PL] - zmap[PL + 1]);
					pred_tmp.y = itr2->y + pred_tmp.ay*(zmap[PL] - zmap[PL + 1]);
				}
			}
			else {
				fprintf(stderr, "PL exception PL%03d\n", PL);
			}

			if (itr2->pos / 10 == PL) {
				pred_tmp.flg = 1;
				pred_tmp.rawid = itr2->rawid;
			}
		}
		pred.push_back(pred_tmp);
	}


	double xmin, xmax, ymin, ymax;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		if (itr == pred.begin()) {
			xmin = itr->x;
			xmax = itr->x;
			ymin = itr->y;
			ymax = itr->y;
		}
		xmin = std::min(itr->x, xmin);
		xmax = std::max(itr->x, xmax);
		ymin = std::min(itr->y, ymin);
		ymax = std::max(itr->y, ymax);
	}
	xmin = xmin + 3000;
	xmax = xmax - 3000;
	ymin = xmin + 3000;
	ymax = ymax - 3000;

	std::vector<Prediction> pred_sel;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		if (itr->x < xmin)continue;
		if (itr->x > xmax)continue;
		if (itr->y < ymin)continue;
		if (itr->y > ymax)continue;
		pred_sel.push_back(*itr);
	}
	if (pred_sel.size() == 0) {
		printf("PL%03d no prediction track\n", PL);
		return;
	}


	const int NUM = 16;
	int all[NUM] = {}, count[NUM] = {};
	double eff[NUM], eff_err[NUM];
	double angle;
	for (auto itr = pred_sel.begin(); itr != pred_sel.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		all[angle_divide(angle)]++;
		if (itr->flg == 1) {
			count[angle_divide(angle)]++;
		}
		ofs_pred << std::right << std::fixed << std::setfill(' ')
			<< std::setw(3) << std::setprecision(0) << itr->PL << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->x << " "
			<< std::setw(8) << std::setprecision(1) << itr->y << " "
			<< std::setw(2) << std::setprecision(0) << itr->flg << std::endl;
		if (itr->flg == 1) {
			ofs_id << std::right << std::fixed << std::setfill(' ')
				<< std::setw(3) << std::setprecision(0) << itr->PL << " "
				<< std::setw(12) << std::setprecision(0) << itr->rawid << " "
				<< std::setw(7) << std::setprecision(4) << itr->ax << " "
				<< std::setw(7) << std::setprecision(4) << itr->ay << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << std::endl;

		}
	}

	for (int i = 0; i < 13; i++) {
		eff[i] = double(count[i]) / all[i];
		eff_err[i] = sqrt(count[i] * eff[i] * (1 - eff[i])) / all[i];
		ofs_eff << std::right << std::fixed << std::setfill(' ')
			<< std::setw(4) << std::setprecision(0) << PL << " ";

		if (i == 0)ofs_eff << "0.0 0.1 ";
		if (i == 1)ofs_eff << "0.1 0.3 ";
		if (i == 2)ofs_eff << "0.3 0.5 ";
		if (i == 3)ofs_eff << "0.5 0.7 ";
		if (i == 4)ofs_eff << "0.7 0.9 ";
		if (i == 5)ofs_eff << "0.9 1.1 ";
		if (i == 6)ofs_eff << "1.1 1.3 ";
		if (i == 7)ofs_eff << "1.3 1.5 ";
		if (i == 8)ofs_eff << "1.5 2.0 ";
		if (i == 9)ofs_eff << "2.0 2.5 ";
		if (i == 10)ofs_eff << "2.5 3.0 ";
		if (i == 11)ofs_eff << "3.0 3.5 ";
		if (i == 12)ofs_eff << "3.5 4.0 ";
		if (i == 13)ofs_eff << "4.0 4.5 ";
		if (i == 14)ofs_eff << "4.5 5.0 ";
		if (i == 15)ofs_eff << "5.0 10.0 ";

		ofs_eff << std::setw(8) << std::setprecision(0) << all[i] << " "
			<< std::setw(8) << std::setprecision(0) << count[i] << " "
			<< std::setw(5) << std::setprecision(4) << eff[i] << " "
			<< std::setw(5) << std::setprecision(4) << eff_err[i] << std::endl;

	}

}
int angle_divide(double angle) {
	if (angle < 0.1)return 0;
	if (0.1 <= angle && angle < 0.3)return 1;
	if (0.3 <= angle && angle < 0.5)return 2;
	if (0.5 <= angle && angle < 0.7)return 3;
	if (0.7 <= angle && angle < 0.9)return 4;
	if (0.9 <= angle && angle < 1.1)return 5;
	if (1.1 <= angle && angle < 1.3)return 6;
	if (1.3 <= angle && angle < 1.5)return 7;
	if (1.5 <= angle && angle < 2.0)return 8;
	if (2.0 <= angle && angle < 2.5)return 9;
	if (2.5 <= angle && angle < 3.0)return 10;
	if (3.0 <= angle && angle < 3.5)return 11;
	if (3.5 <= angle && angle < 4.0)return 12;
	if (4.0 <= angle && angle < 4.5)return 13;
	if (4.5 <= angle && angle < 5.0)return 14;
	if (5.0 <= angle)return 15;
	return -1;
}
