#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")

#include <FILE_structure.hpp>

int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "prg input-linklet-txt output-linklet-bll 0\n");
		fprintf(stderr, "prg input-linklet-bll output-linklet-txt 1\n");
		exit(1);
	}
	std::string file_in_link = argv[1];
	std::string file_out_link = argv[2];
	int mode = std::stoi(argv[3]);
	if (mode != 0 && mode != 1) {
		exit(1);
	}
	std::vector<netscan::linklet_t> link;
	if (mode == 0) {
		netscan::read_linklet_txt( file_in_link, link);
		netscan::write_linklet_bin(file_out_link, link);
	}
	else if (mode == 1) {
		netscan::read_linklet_bin(file_in_link, link);
		netscan::write_linklet_txt(file_out_link, link);
	}
}