#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>

#include <set>
#include <unordered_map>
struct My_Group{
	int groupid, pos0, pos1, npl, nseg, nbase;
	std::vector<mfile0::M_Base> b;
	std::map<int, int> pl_num;
};
void multi_group_chain_selection(std::vector<mfile0::M_Chain> &chain, std::vector<std::vector<mfile0::M_Chain *>> &multi_chain, std::vector<mfile0::M_Chain *>&single_chain);
void multi_group_classification(std::vector<std::vector<mfile0::M_Chain *>> &multi_chain, std::vector<My_Group> &group);
std::vector <mfile0::M_Chain> write_chain(std::vector < mfile0::M_Chain> &chain, std::vector<int>groupID);
void Group_Chain_selection(My_Group group);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:input-mfile output\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	//signle chain group
	//multi chain group
	// |
	//  ->unique base
	//  ->multi  base
	std::vector<std::vector<mfile0::M_Chain *>> multi_chain;
	std::vector<mfile0::M_Chain *>single_chain;
	multi_group_chain_selection(m.chains, multi_chain, single_chain);
	std::vector<My_Group> group;
	multi_group_classification(multi_chain, group);

	//以下デバッグ用出力
	std::vector<int> groupID;
	bool flg = true;
	int count = 0;
	for (auto itr = group.begin(); itr != group.end(); itr++) {
		flg = false;
		for (auto itr2 = itr->pl_num.begin(); itr2 != itr->pl_num.end(); itr2++) {
			if (itr2->second > 1) {
				flg = true;
			}
		}
		if (flg==false&& itr->nseg > 50) {
			groupID.push_back(itr->groupid);
		}
		if (itr->nseg < 50)flg = false;
		if (flg == false) continue;
		//ささくれ
		//if (1.0 <= itr->nbase *1.0 / itr->nseg&&itr->nbase *1.0 / itr->nseg < 1.2) 
		//中間-->2trk
		//if (1.2 <= itr->nbase *1.0 / itr->nseg&&itr->nbase *1.0 / itr->nseg < 1.7) 
		//2trk
		//if (1.7 <= itr->nbase *1.0 / itr->nseg&&itr->nbase *1.0 / itr->nseg < 2.2) 
		//中間
		//if (2.2 <= itr->nbase *1.0 / itr->nseg&&itr->nbase *1.0 / itr->nseg < 2.5) 
		//3trk
		//if (2.5 <= itr->nbase *1.0 / itr->nseg&&itr->nbase *1.0 / itr->nseg < 3.1) 
		//over
		if (3.1 <= itr->nbase *1.0 / itr->nseg) 
		{
			if (count == 0) {
				//Group_Chain_selection(*itr);
				count++;
			}
			//groupID.push_back(itr->groupid);
		}
	}

	//return 0;

	std::vector <mfile0::M_Chain> output_chain = write_chain(m.chains, groupID);

	//ouputfile open&write header
	int output = 1;
	int min = 0, max = 100;
	count = 0;
	if (output == 1) {
		std::ofstream ofs(file_out_mfile);
		if (!ofs) {
			//file open s
			fprintf(stderr, "File[%s] is not exist!!\n", file_out_mfile.c_str());
			throw std::exception();
		}
		//write_mfile_header(ofs, m.header, 0);
		for (auto itr = output_chain.begin(); itr != output_chain.end(); itr++) {
			if (min <= count && count < max) {
				//mfile0::write_mfile_chain(ofs, *itr);
				mfile0::write_mfile_chain_IVE(ofs, *itr);
			}
			count++;
		}
	}

}
void multi_group_chain_selection(std::vector<mfile0::M_Chain> &chain, std::vector<std::vector<mfile0::M_Chain *>> &multi_chain, std::vector<mfile0::M_Chain *>&single_chain) {
	std::multimap<int, mfile0::M_Chain *> group;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		group.insert(std::make_pair(itr->basetracks.begin()->group_id, &(*itr)));
	}

	int count;
	for (auto itr = group.begin(); itr != group.end();) {
		count = group.count(itr->first);
		if (count == 1) {
			single_chain.push_back(itr->second);
			itr++;
		}
		else {
			std::vector<mfile0::M_Chain *> group_tmp;
			for (int i = 0; i < count; i++) {
				group_tmp.push_back(itr->second);
				itr++;
			}
			multi_chain.push_back(group_tmp);
		}
	}
	fprintf(stderr, "signle chain group = %d\n", int(single_chain.size()));
	fprintf(stderr, "multi  chain group = %d\n", int(multi_chain.size()));
}
void multi_group_classification(std::vector<std::vector<mfile0::M_Chain *>> &multi_chain, std::vector<My_Group> &group_vec) {
	int count = 0;
	for (auto g = multi_chain.begin(); g != multi_chain.end(); g++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r group classification... %d/%d(%4.1lf%%)", count, multi_chain.size(), count*100. / multi_chain.size());
		}
		count++;

		My_Group group;
		group.groupid = (*(g->begin()))->basetracks.begin()->group_id;
		std::map<std::pair<int, int>, mfile0::M_Base> base;
		for (auto c = g->begin(); c != g->end(); c++) {
			for (auto b = (*c)->basetracks.begin(); b != (*c)->basetracks.end(); b++) {
				base.insert(std::make_pair(std::make_pair(b->pos / 10, b->rawid), *b));
			}
		}
		group.nbase = base.size();
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			group.b.push_back(itr->second);
			auto res = group.pl_num.insert(std::make_pair(itr->first.first, 1));
			if (!res.second) {
				res.first->second += 1;
			}
		}
		group.pos0 = (group.pl_num.begin()->first) * 10;
		group.pos1 = (group.pl_num.rbegin()->first) * 10;
		group.npl = (group.pos1 - group.pos0) / 10 + 1;
		group.nseg = group.pl_num.size();

		group_vec.push_back(group);
	}
	fprintf(stderr, "\r group classification... %d/%d(%4.1lf%%)\n", count, multi_chain.size(), count*100. / multi_chain.size());

	int output = 0;
	if (output == 1) {
		std::ofstream ofs_unique("group_inf_unique.txt");
		std::ofstream ofs_multi("group_inf_multi.txt");
		bool flg = true;

		for (auto itr = group_vec.begin(); itr != group_vec.end(); itr++) {
			flg = true;
			for (auto itr2 = itr->pl_num.begin(); itr2 != itr->pl_num.end(); itr2++) {
				if (itr2->second > 1) {
					flg = false;
				}
			}
			if (flg == true) {
				ofs_unique << std::right << std::fixed
					<< std::setw(12) << std::setprecision(0) << itr->groupid << " "
					<< std::setw(4) << std::setprecision(0) << itr->pos0 << " "
					<< std::setw(4) << std::setprecision(0) << itr->pos1 << " "
					<< std::setw(4) << std::setprecision(0) << itr->npl << " "
					<< std::setw(4) << std::setprecision(0) << itr->nseg << " "
					<< std::setw(5) << std::setprecision(0) << itr->nbase << std::endl;
				for (auto itr2 = itr->b.begin(); itr2 != itr->b.end(); itr2++) {
					ofs_unique << std::right << std::fixed
						<< std::setw(4) << std::setprecision(0) << itr2->pos << " "
						<< std::setw(12) << std::setprecision(0) << itr2->rawid << " "
						<< std::setw(8) << std::setprecision(0) << itr2->ph << " "
						<< std::setw(7) << std::setprecision(4) << itr2->ax << " "
						<< std::setw(7) << std::setprecision(4) << itr2->ay << " "
						<< std::setw(8) << std::setprecision(1) << itr2->x << " "
						<< std::setw(8) << std::setprecision(1) << itr2->y << " "
						<< std::setw(8) << std::setprecision(1) << itr2->z << std::endl;
				}
			}
			else {
				ofs_multi << std::right << std::fixed
					<< std::setw(12) << std::setprecision(0) << itr->groupid << " "
					<< std::setw(4) << std::setprecision(0) << itr->pos0 << " "
					<< std::setw(4) << std::setprecision(0) << itr->pos1 << " "
					<< std::setw(4) << std::setprecision(0) << itr->npl << " "
					<< std::setw(4) << std::setprecision(0) << itr->nseg << " "
					<< std::setw(5) << std::setprecision(0) << itr->nbase << std::endl;
				for (auto itr2 = itr->b.begin(); itr2 != itr->b.end(); itr2++) {
					ofs_multi << std::right << std::fixed
						<< std::setw(4) << std::setprecision(0) << itr2->pos << " "
						<< std::setw(12) << std::setprecision(0) << itr2->rawid << " "
						<< std::setw(8) << std::setprecision(0) << itr2->ph << " "
						<< std::setw(7) << std::setprecision(4) << itr2->ax << " "
						<< std::setw(7) << std::setprecision(4) << itr2->ay << " "
						<< std::setw(8) << std::setprecision(1) << itr2->x << " "
						<< std::setw(8) << std::setprecision(1) << itr2->y << " "
						<< std::setw(8) << std::setprecision(1) << itr2->z << std::endl;
				}
			}
		}
	}
}
std::vector <mfile0::M_Chain> write_chain(std::vector < mfile0::M_Chain> &chain,std::vector<int>groupID) {
	std::unordered_multimap<int, mfile0::M_Chain *> group_map;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		group_map.insert(std::make_pair(itr->basetracks.begin()->group_id, &(*itr)));
	}

	std::vector < mfile0::M_Chain> ret;
	for (auto itr = groupID.begin(); itr != groupID.end(); itr++) {
		if (group_map.count(*itr) <= 1)continue;
		auto range = group_map.equal_range(*itr);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			ret.push_back(*itr2->second);
		}
	}
	return ret;
}
std::vector <mfile0::M_Chain> Group_one_track_selection(std::vector<My_Group> group_vec) {
	//group-->1trackのchainの生成




}
void Group_Chain_selection(My_Group group) {

	std::vector<double> par1;
	std::vector<double> par2;
	std::vector<double> par3;
	double distance, slope[2];
	matrix_3D::vector_3D pos, origin,dir;
	for (auto itr = group.b.begin(); itr != group.b.end(); itr++) {
		pos.x = itr->x;
		pos.y = itr->y;
		pos.z = itr->z;
		dir.x = itr->ax;
		dir.y = itr->ay;
		dir.z = 1;
		origin.x = 0;
		origin.y = 0;
		origin.z = 0;

		distance = matrix_3D::inpact_parameter(pos, dir, origin);
		slope[0] = 1.0 / sqrt(dir.x*dir.x + dir.y*dir.y + 1);
		slope[1] = dir.x / sqrt(dir.x*dir.x + dir.y*dir.y);
		par1.push_back(distance);
		par2.push_back(slope[0]);
		par3.push_back(slope[1]);
	}

	printf("%d %d\n", group.groupid, group.b.size());
	for (int i = 0; i < par1.size();i++) {
		printf("%lf %lf %lf\n", par1[i], par2[i], par3[i]);
	}

}
