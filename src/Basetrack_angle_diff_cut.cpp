#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<netscan::base_track_t> cut_angle_diff(std::vector<netscan::base_track_t> base);

int main(int argc,char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg input-vxx pl zone output-vxx\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_bvxx = argv[4];

	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_bvxx, base, pl, zone);

	std::vector<netscan::base_track_t> base_out = cut_angle_diff(base);

	netscan::write_basetrack_vxx(file_out_bvxx, base_out, pl, zone);

}
std::vector<netscan::base_track_t> cut_angle_diff(std::vector<netscan::base_track_t> base) {
	std::vector<netscan::base_track_t> ret;
	double angle, d_angle;
	for (auto itr = base.begin(); itr!=base.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (0.0 <= angle && angle < 1.0) {
			d_angle = sqrt(pow(itr->ax - itr->m[0].ax, 2) + pow(itr->ax - itr->m[1].ax, 2) + pow(itr->ay - itr->m[0].ay, 2) + pow(itr->ay - itr->m[1].ay, 2));
			if (d_angle > 0.2*angle+0.1)continue;
		}
		ret.push_back(*itr);
	}
	printf("base-micro angle difference cut\n");
	printf("d_ang>0.2*angle+0.1\n");
	printf("%d --> %d(%4.1lf%%)\n", base.size(), ret.size(), ret.size()*100. / base.size());
	return ret;
}

