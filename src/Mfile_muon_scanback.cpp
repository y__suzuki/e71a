#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>

class muon_track {
public:
	int pl, rawid, t_pl, t_rawid, ievent,time;
	double ax, ay, x, y;
	double t_ax, t_ay, t_x, t_y,momentum;

};

std::vector<muon_track> read_muon_list(std::string filename);
void write_mfile_vector(std::string file_path, std::vector<mfile0::Mfile>&m_v);
std::vector<mfile0::Mfile> search_muon_track(std::vector<muon_track >&mu, mfile0::Mfile &m);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg:input-mfile input-muon putput-mfile-path\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_muon = argv[2];
	std::string file_out_path = argv[3];

	std::vector<muon_track> muon = read_muon_list(file_in_muon);
	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::vector<mfile0::Mfile> m_v = search_muon_track(muon, m);
	write_mfile_vector(file_out_path, m_v);
}
std::vector<muon_track> read_muon_list(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<muon_track> ret;
	muon_track t;
	while (ifs >> t.pl >> t.rawid >> t.ax >> t.ay >> t.x >> t.y
		>> t.t_pl >> t.t_rawid >> t.t_ax >> t.t_ay >> t.t_x >> t.t_y
		>> t.ievent >> t.time >> t.momentum) {
		ret.push_back(t);
	}

	printf("all muon track %d\n", ret.size());
	return ret;

}

std::vector<mfile0::Mfile> search_muon_track(std::vector<muon_track >&mu, mfile0::Mfile &m) {
	std::multimap <std::pair<int,int>, mfile0::M_Chain*> pl4_id;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (itr->pos0 / 10 != 4)continue;
		pl4_id.insert(std::make_pair(std::make_pair(itr->basetracks.begin()->pos / 10, itr->basetracks.begin()->rawid), &(*itr)));
	}


	std::vector<mfile0::Mfile> ret;
	for (auto itr = mu.begin(); itr != mu.end(); itr++) {
		if (pl4_id.count(std::make_pair(itr->pl, itr->rawid)) == 0)continue;
		mfile0::Mfile m_tmp;
		m_tmp.header = m.header;
		auto res = pl4_id.equal_range(std::make_pair(itr->pl, itr->rawid));
		for (auto range = res.first; range != res.second; range++) {
			m_tmp.chains.push_back(*(range->second));
		}
		ret.push_back(m_tmp);
	}

	printf("all muon track %d\n", mu.size());
	printf("detect chains %d\n", ret.size());

	return ret;
}

void write_mfile_vector(std::string file_path, std::vector<mfile0::Mfile>&m_v) {
	for (int i = 0; i < m_v.size(); i++) {
		std::stringstream filename;
		filename << file_path << "\\muon_" << std::setw(8) << std::setfill('0') << m_v[i].chains.begin()->basetracks.begin()->rawid << ".all";
		mfile1::write_mfile_extension(filename.str(), m_v[i]);
	}
}