#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <filesystem>

void Get_dz_Mean_RMS(std::vector<corrmap0::Corrmap> &corr, double &mean, double&rms);
std::map<int, std::pair<double, double>> read_film_thick(std::string filename);
std::map<int, std::pair<double, double>> read_SUS_thick(std::string filename);
void output(std::string filename, std::map<std::pair<int, int>, std::vector<corrmap0::Corrmap>>&corr, std::map<int, std::pair<double, double>>&film, std::map<int, std::pair<double, double>> &sus);
int Get_ironID(int PL);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg input-align-folder in_file_thick in_SUS_thick output-txt\n");
		exit(1);
	}
	std::string path_in_align = argv[1];
	std::string file_in_film = argv[2];
	std::string file_in_SUS = argv[3];
	std::string file_out_txt = argv[4];

	std::map<int, std::pair<double, double>>film_thick = read_film_thick(file_in_film);
	std::map<int, std::pair<double, double>>SUS_thick = read_SUS_thick(file_in_SUS);

	std::map<std::pair<int, int>, std::vector<corrmap0::Corrmap>> corr_all;
	for (int PL = 1; PL <= 90; PL++) {
		std::stringstream file_in_corr;
		file_in_corr << path_in_align << "\\corrmap-align-" << std::setw(3) << std::setfill('0') << PL << "-" << std::setw(3) << std::setfill('0') << PL + 1 << ".lst";
		//fileがない場合はcontinue
		if (!std::filesystem::exists(file_in_corr.str()))continue;
		std::vector<corrmap0::Corrmap> corr;
		corrmap0::read_cormap(file_in_corr.str(), corr);
		corr_all.insert(std::make_pair(std::make_pair(PL, PL + 1), corr));
	}
	output(file_out_txt, corr_all, film_thick, SUS_thick);

}
void Get_dz_Mean_RMS(std::vector<corrmap0::Corrmap> &corr, double &mean, double&rms) {
	double sum = 0, sum2 = 0, count = 0;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		count++;
		sum += itr->dz;
		sum2 += itr->dz*itr->dz;
	}
	mean = sum / count;
	rms = sqrt(sum2 / count - mean * mean);
}

std::map<int, std::pair<double, double>> read_film_thick(std::string filename) {
	std::map<int, std::pair<double, double>> ret;
	std::ifstream ifs(filename);
	double thick[4], mean, rms;
	int id;
	double slope = 1.0013;
	double intercept = 4.6557;
	while (ifs >> id >> thick[0] >> thick[1] >> thick[2] >> thick[3]) {
		//printf("%d %.1lf %.1lf %.1lf %.1lf\n", id, thick[0], thick[1], thick[2], thick[3]);
		//thickness ゲージの補正
		for (int i = 0; i < 4; i++) {
			thick[i] = (thick[i] - intercept) / slope;
		}

		mean = 0;
		rms = 0;
		for (int i = 0; i < 4; i++) {
			mean += thick[i];
		}
		mean = mean / 4;
		for (int i = 0; i < 4; i++) {
			rms += (thick[i] - mean)*(thick[i] - mean);
			rms = sqrt(rms / 4);
		}
		ret.insert(std::make_pair(id, std::make_pair(mean, rms)));
	}
	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	printf("%3d %5.1lf %5.1lf\n", itr->first, itr->second.first, itr->second.second);
	//}
	return ret;

}
std::map<int, std::pair<double, double>> read_SUS_thick(std::string filename) {
	std::map<int, std::pair<double, double>> ret;
	std::ifstream ifs(filename);
	double thick[8], mean, rms;
	int id;
	while (ifs >> id >> thick[0] >> thick[1] >> thick[2] >> thick[3]>> thick[4] >> thick[5] >> thick[6] >> thick[7]){
		//printf("%d %.1lf %.1lf %.1lf %.1lf %.1lf %.1lf %.1lf %.1lf\n", id, thick[0], thick[1], thick[2], thick[3], thick[4], thick[5], thick[6], thick[7]);
		mean = 0;
		rms = 0;
		for (int i = 0; i < 8; i++) {
			mean += thick[i];
		}
		mean = mean / 8;
		for (int i = 0; i < 8; i++) {
			rms += (thick[i] - mean)*(thick[i] - mean);
			rms = sqrt(rms / 8);
		}
		ret.insert(std::make_pair(id, std::make_pair(mean, rms)));
	}
	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	printf("%3d %5.1lf %5.1lf\n", itr->first, itr->second.first, itr->second.second);
	//}
	return ret;
}

void output(std::string filename, std::map<std::pair<int, int>, std::vector<corrmap0::Corrmap>>&corr,std::map<int, std::pair<double, double>>&film, std::map<int, std::pair<double, double>> &sus) {
	
	std::ofstream ofs(filename);
	for (int PL = 1; PL <= 133; PL++) {
		if (corr.count(std::make_pair(PL, PL + 1)) == 0)continue;
		//(PL) - (PL+1) が鉄以外はcontinue
		if (PL < 4)continue;
		if (PL == 15)continue;
		if (PL > 15 && PL % 2 == 1)continue;
		auto res = corr.find(std::make_pair(PL, PL + 1));

		double mean, rms;
		Get_dz_Mean_RMS(res->second, mean, rms);
		mean = 350 + 500 - mean;

		int iron_id;
		iron_id = Get_ironID(PL);

		if (sus.count(iron_id) == 0) {
			fprintf(stderr, "PL%03d SUS ID %d not found\n", PL, iron_id);
			exit(1);
		}
		auto sus_thick = sus.find(iron_id);

		if (film.count(PL) == 0) {
			fprintf(stderr, "PL%03d film not found\n", PL);
			exit(1);
		}
		if (film.count(PL+1) == 0) {
			fprintf(stderr, "PL%03d film not found\n", PL+1);
			exit(1);
		}
		auto film_0 = film.find(PL);
		auto film_1 = film.find(PL+1);

		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << PL << " "
			<< std::setw(3) << std::setprecision(0) << PL + 1 << " "
			<< std::setw(6) << std::setprecision(1) << mean << " "
			<< std::setw(5) << std::setprecision(1) << rms << " "
			<< std::setw(6) << std::setprecision(1) << film_0->second.first << " "
			<< std::setw(5) << std::setprecision(1) << film_0->second.second << " "
			<< std::setw(6) << std::setprecision(1) << film_1->second.first << " "
			<< std::setw(5) << std::setprecision(1) << film_1->second.second << " "
			<< std::setw(6) << std::setprecision(1) << sus_thick->second.first << " "
			<< std::setw(5) << std::setprecision(1) << sus_thick->second.second << std::endl;

	}

}
int Get_ironID(int PL) {
	if (PL == 4)return 1;
	else if (PL == 5)return 2;
	else if (PL == 6)return 3;
	else if (PL == 7)return 4;
	else if (PL == 8)return 5;
	else if (PL == 9)return 6;
	else if (PL == 10)return 7;
	else if (PL == 11)return 8;
	else if (PL == 12)return 9;
	else if (PL == 13)return 10;
	else if (PL == 14)return 11;
	else if (PL >= 16 && PL % 2 == 0) {
		return (PL - 14) / 2 + 11;
	}
	else {
		return -1;
	}
}