#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"


#include <set>
std::vector<mfile0::M_Chain> chain_nseg_selection(std::vector<mfile0::M_Chain> &chain, int nseg);
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold);

std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>  &chain);
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold);
std::vector<mfile0::M_Chain> chain_agnle_selection(std::vector<mfile0::M_Chain>  &chain, double thr_ax, double thr_ay);
void CalcEfficiency(std::ofstream &ofs_eff, std::ofstream &ofs_pred, std::ofstream &ofs_id, std::vector<mfile0::M_Chain>  chain, int PL, std::map<int, double> zmap);
void CalcEfficiency2(std::ofstream &ofs_eff, std::ofstream &ofs_pred, std::ofstream &ofs_id, std::vector<mfile0::M_Chain>  chain, int PL, std::map<int, double> zmap);
int angle_divide(double angle);
bool checkFileExistence(const std::string& str);

std::vector<vxx::base_track_t> basetrack_selection(std::vector<vxx::base_track_t>&base, std::vector<mfile0::M_Chain>  &chain, int pl);

struct Prediction {
	int PL, flg, rawid;
	double ax, ay, x, y;
};
int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mfile in-base-floder out-file(bvxx)\n");
		exit(1);
	}

	//input value
	std::string file_in_mfile = argv[1];
	std::string file_in_bvxx_path = argv[2];
	std::string file_out_bvxx_path = argv[3];

	//mfile
	mfile0::Mfile m;

	//read mfile
	mfile1::read_mfile_extension(file_in_mfile, m);
	m.chains = chain_nseg_selection(m.chains, 5);
	m.chains = chain_agnle_selection(m.chains, 4.0, 4.0);
	m.chains = chain_dlat_selection(m.chains, 0.005);

	int PL_min = *m.header.all_pos.begin() / 10;
	int PL_max = *m.header.all_pos.rbegin() / 10;
	for (int PL = PL_min; PL <= PL_max; PL++) {
		std::stringstream bvxx_file;
		bvxx_file << file_in_bvxx_path << "\\PL" << std::setw(3) << std::setfill('0') << PL
			<< "\\b" << std::setw(3) << std::setfill('0') << PL << ".sel.vxx";
		if (!checkFileExistence(bvxx_file.str())) {
			fprintf(stderr, "file[%s] not found\n", bvxx_file.str().c_str());
			continue;
		}
		std::vector<vxx::base_track_t> base;
		vxx::BvxxReader br;
		base = br.ReadAll(bvxx_file.str(), PL, 0);
		base = basetrack_selection(base, m.chains, PL);

		std::stringstream bvxx_file_write;
		bvxx_file_write << file_out_bvxx_path << "\\b" << std::setw(3) << std::setfill('0') << PL << ".sel.vxx";

		vxx::BvxxWriter bw;
		bw.Write(bvxx_file_write.str(), PL, 0, base);
	}
}
std::vector<mfile0::M_Chain> chain_nseg_selection(std::vector<mfile0::M_Chain> &chain, int nseg) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr->nseg < nseg)continue;
		ret.push_back(*itr);
	}
	printf("chain nseg >= %d: %d --> %d (%4.1lf%%)\n", nseg, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>  &chain) {

	std::vector<mfile0::M_Chain> ret;
	std::set<int>gid;
	std::multimap<int, mfile0::M_Chain*>group;
	int nseg_max;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		gid.insert(itr->basetracks[0].group_id);
		group.insert(std::make_pair(itr->basetracks[0].group_id, &(*itr)));
	}
	for (auto itr = gid.begin(); itr != gid.end(); itr++) {
		if (group.count(*itr) == 1) {
			ret.push_back(*(group.find(*itr)->second));
		}
		else {
			auto range = group.equal_range(*itr);
			nseg_max = 1;
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				nseg_max = std::max(itr2->second->nseg, nseg_max);
			}
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (itr2->second->nseg == nseg_max) {
					ret.push_back(*(itr2->second));
					break;
				}
			}
		}
	}
	printf("chain group clustering: %d --> %d (%4.1lf%%)\n", chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;

}
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (mfile0::angle_diff_dev_lat(*itr, ax, ay) > threshold)continue;
		ret.push_back(*itr);
	}
	printf("chain lateral selection <= %5.4lf : %d --> %d (%4.1lf%%)\n", threshold, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;

}
std::vector<mfile0::M_Chain> chain_agnle_selection(std::vector<mfile0::M_Chain>  &chain, double thr_ax, double thr_ay) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (thr_ax - fabs(ax) < 0)continue;
		if (thr_ay - fabs(ay) < 0)continue;
		ret.push_back(*itr);
	}
	printf("chain average angle selection |ax|<=%3.1lf |ay|<=%3.1lf : %d --> %d (%4.1lf%%)\n", thr_ax, thr_ay, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
bool checkFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}
std::vector<vxx::base_track_t> basetrack_selection(std::vector<vxx::base_track_t>&base, std::vector<mfile0::M_Chain>  &chain, int pl) {

	std::set<int> basetrack_rawid;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (pl != itr2->pos / 10)continue;
			basetrack_rawid.insert(itr2->rawid);
		}
	}
	std::vector<vxx::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (basetrack_rawid.count(itr->rawid) == 1) {
			ret.push_back(*itr);
		}
	}

	fprintf(stderr, "connected selection %d --> %d(%4.1lf%%)\n", base.size(), ret.size(), ret.size()*100. / base.size());

	return ret;
}