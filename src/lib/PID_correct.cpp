#include "PID_correct.h"
#include "FILE_structure.hpp"
#pragma comment(lib,"FILE_structure.lib")

//コンストラクタ
PID_track::PID_track() {


}
//デストラクタ
PID_track::~PID_track() {

}
void PID_track::wrtie_pid_track(std::string filename, std::vector<PID_track> &t) {
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (t.size() == 0) {
		fprintf(stderr, "target track ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = t.size();
	for (int i = 0; i < t.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& t[i], sizeof(PID_track));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}
void PID_track::wrtie_pid_track_txt(std::string filename, std::vector<PID_track> &t) {
	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (t.size() == 0) {
		fprintf(stderr, "target track ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = t.size();
	for (int i = 0; i < t.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs << std::right << std::fixed
			<< std::setw(20) << std::setprecision(0) << t[i].chainid << " "
			<< std::setw(12) << std::setprecision(0) << t[i].rawid << " "
			<< std::setw(4) << std::setprecision(2) << t[i].pid << " "
			<< std::setw(8) << std::setprecision(1) << t[i].pb << " "
			<< std::setw(7) << std::setprecision(4) << t[i].angle << " "
			<< std::setw(4) << std::setprecision(0) << t[i].pos << " "
			<< std::setw(3) << std::setprecision(0) << t[i].trackingid << " "
			<< std::setw(6) << std::setprecision(1) << t[i].vph << " "
			<< std::setw(6) << std::setprecision(1) << t[i].pixelnum << " "
			<< std::setw(3) << std::setprecision(0) << t[i].sensorid << std::endl;


	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}
void PID_track::read_pid_track(std::string filename, std::vector<PID_track> &t) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	PID_track t_tmp;
	while (ifs.read((char*)& t_tmp, sizeof(PID_track))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		t.emplace_back(t_tmp);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no track!\n", filename.c_str());
		exit(1);
	}

}
