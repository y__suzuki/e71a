#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <fstream>
#include <iostream>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <set>
#include <math.h>

#include "FILE_structure.hpp"
#include "VxxReader.h"
#pragma comment(lib, "VxxReader.lib")
#include <functions.hpp>
#pragma comment(lib,"functions.lib")


namespace mfile0 {
	void mfile0::read_mfile(std::string file_path, Mfile &mfile) {
		std::ifstream ifs(file_path);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
		}

		std::string str;

		M_Header head_tmp;
		for (int i = 0; i < 3; i++) {
			std::getline(ifs, str);
			head_tmp.head[i] = str;
		}
		std::getline(ifs, str);
		head_tmp.num_all_plate = stoi(str);

		{
			std::getline(ifs, str);
			auto str_v = StringSplit(str);
			for (int i = 0; i < str_v.size(); i++) {
				head_tmp.all_pos.push_back(stoi(str_v[i]));
			}
		}

		mfile.header = head_tmp;
		int cnt = 0;

		while (std::getline(ifs, str)) {
			if (cnt % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			cnt++;
			M_Chain chains;

			auto strs = StringSplit(str);
			if (strs.size() == 4) {
				chains.chain_id = stoi(strs[0]);
				chains.nseg = stoi(strs[1]);
				chains.pos0 = stoi(strs[2]);
				chains.pos1 = stoi(strs[3]);

				for (int i = 0; i < chains.nseg; i++) {

					M_Base bt;

					std::getline(ifs, str);
					strs = StringSplit(str);
					if (strs.size() == 15) {
						bt.pos = stoi(strs[0]);
						bt.group_id = stoll(strs[1]);
						bt.rawid = stoi(strs[2]);
						bt.ph = stoi(strs[3]);
						bt.ax = stof(strs[4]);
						bt.ay = stof(strs[5]);
						bt.x = stod(strs[6]);
						bt.y = stod(strs[7]);
						bt.z = stod(strs[8]);

						bt.flg_i[0] = stoi(strs[9]);
						bt.flg_i[1] = stoi(strs[10]);
						bt.flg_i[2] = stoi(strs[11]);
						bt.flg_i[3] = stoi(strs[12]);
						bt.flg_d[0] = stod(strs[13]);
						bt.flg_d[1] = stod(strs[14]);
					}
					else {
						fprintf(stderr, "mfile format err! cannot read file\n");
						fprintf(stderr, "base track information 15 -->%d\n", int(strs.size()));
						for (int i = 0; i < strs.size(); i++) {
							printf("%s ", strs[i].c_str());
						}
						printf("\n");
						throw std::exception();
					}
					chains.basetracks.push_back(bt);
				}
			}
			else {
				fprintf(stderr, "mfile format err! cannot read file\n");
				fprintf(stderr, "chain information 4 -->%d\n", int(strs.size()));
				for (int i = 0; i < strs.size(); i++) {
					printf("%s ", strs[i].c_str());
				}
				printf("\n");

				throw std::exception();
			}
			mfile.chains.push_back(chains);
		}
		auto size1 = eofpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
		if (cnt == 0) {
			fprintf(stderr, "%s no chain!\n", file_path.c_str());
			exit(1);
		}
	}
	void mfile0::read_mfile(std::string file_path, Mfile &mfile,int nseg_thr) {
		std::ifstream ifs(file_path);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
		}

		std::string str;

		M_Header head_tmp;
		for (int i = 0; i < 3; i++) {
			std::getline(ifs, str);
			head_tmp.head[i] = str;
		}
		std::getline(ifs, str);
		head_tmp.num_all_plate = stoi(str);

		{
			std::getline(ifs, str);
			auto str_v = StringSplit(str);
			for (int i = 0; i < str_v.size(); i++) {
				head_tmp.all_pos.push_back(stoi(str_v[i]));
			}
		}

		mfile.header = head_tmp;
		int cnt = 0;

		while (std::getline(ifs, str)) {
			if (cnt % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			cnt++;
			M_Chain chains;

			auto strs = StringSplit(str);
			if (strs.size() == 4) {
				chains.chain_id = stoi(strs[0]);
				chains.nseg = stoi(strs[1]);
				chains.pos0 = stoi(strs[2]);
				chains.pos1 = stoi(strs[3]);
				if (chains.nseg < nseg_thr) {
					for (int i = 0; i < chains.nseg; i++) {
						std::getline(ifs, str);
						continue;
					}
				}
				else{
					for (int i = 0; i < chains.nseg; i++) {

						M_Base bt;

						std::getline(ifs, str);
						strs = StringSplit(str);
						if (strs.size() == 15) {
							bt.pos = stoi(strs[0]);
							bt.group_id = stoi(strs[1]);
							bt.rawid = stoi(strs[2]);
							bt.ph = stoi(strs[3]);
							bt.ax = stof(strs[4]);
							bt.ay = stof(strs[5]);
							bt.x = stod(strs[6]);
							bt.y = stod(strs[7]);
							bt.z = stod(strs[8]);

							bt.flg_i[0] = stoi(strs[9]);
							bt.flg_i[1] = stoi(strs[10]);
							bt.flg_i[2] = stoi(strs[11]);
							bt.flg_i[3] = stoi(strs[12]);
							bt.flg_d[0] = stod(strs[13]);
							bt.flg_d[1] = stod(strs[14]);
						}
						else {
							fprintf(stderr, "mfile format err! cannot read file\n");
							fprintf(stderr, "base track information 15 -->%d\n", int(strs.size()));
							for (int i = 0; i < strs.size(); i++) {
								printf("%s ", strs[i].c_str());
							}
							printf("\n");
							throw std::exception();
						}
						chains.basetracks.push_back(bt);
					}
				}
			}
			else {
				fprintf(stderr, "mfile format err! cannot read file\n");
				fprintf(stderr, "chain information 4 -->%d\n", int(strs.size()));
				for (int i = 0; i < strs.size(); i++) {
					printf("%s ", strs[i].c_str());
				}
				printf("\n");

				throw std::exception();
			}
			mfile.chains.push_back(chains);
		}
		auto size1 = eofpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
		if (cnt == 0) {
			fprintf(stderr, "%s no chain!\n", file_path.c_str());
			exit(1);
		}
	}

	void mfile0::write_mfile(std::string filename, const Mfile &mfile, int output) {
		std::ofstream ofs(filename);
		if (!ofs) {
			//file open s
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			throw std::exception();
		}
		write_mfile_header(ofs, mfile.header, output);
		int count = 0;
		for (auto itr = mfile.chains.begin(); itr != mfile.chains.end(); itr++) {
			if (output == 1 && count % 10000 == 0) {
				fprintf(stderr, "\r Write Mfile chain ... %d/%d (%4.1lf%%)", count, int(mfile.chains.size()), count*100. / mfile.chains.size());
			}
			count++;
			write_mfile_chain(ofs, *itr);
		}
		if (output == 1) {
			fprintf(stderr, "\r Write Mfile chain ... %d/%d (%4.1lf%%)\n", count, int(mfile.chains.size()), count*100. / mfile.chains.size());
		}
	}
	void mfile0::write_mfile_header(std::ofstream &ofs, const M_Header &header, int output) {
		std::cout << std::right << std::fixed;
		ofs << header.head[0] << std::endl;
		ofs << header.head[1] << std::endl;
		ofs << header.head[2] << std::endl;
		ofs << std::setw(6) << header.num_all_plate << std::endl;
		for (int i = 0; i < header.all_pos.size(); i++) {
			ofs << " " << std::setw(4) << header.all_pos[i];
		}
		ofs << std::endl;
		if (output == 1) {
			fprintf(stderr, "mfile header write fin\n");
		}
	}
	void mfile0::write_mfile_chain(std::ofstream &ofs, const M_Chain &chains) {
		ofs << std::right << std::fixed
			<< std::setw(10) << chains.chain_id << " "
			<< std::setw(5) << chains.nseg << " "
			<< std::setw(6) << chains.pos0 << " "
			<< std::setw(6) << chains.pos1 << std::endl;
		for (auto& p : chains.basetracks) {
			ofs << std::right << std::fixed
				<< std::setw(6) << p.pos << " "
				<< std::setw(20) << p.group_id << " "
				<< std::setw(9) << p.rawid << " "
				<< std::setw(9) << p.ph << " "
				<< std::setw(8) << std::setprecision(4) << p.ax << " "
				<< std::setw(8) << std::setprecision(4) << p.ay << " "
				<< std::setw(10) << std::setprecision(1) << p.x << " "
				<< std::setw(10) << std::setprecision(1) << p.y << " "
				<< std::setw(10) << std::setprecision(0) << p.z << " "
				<< std::setw(2) << p.flg_i[0] << " "
				<< std::setw(2) << p.flg_i[1] << " "
				<< std::setw(2) << p.flg_i[2] << " "
				<< std::setw(2) << p.flg_i[3] << " "
				<< std::setw(5) << std::setprecision(4) << p.flg_d[0] << " "
				<< std::setw(5) << std::setprecision(4) << p.flg_d[1] << " "
				<< std::endl;
		}
	}
	double mfile0::chain_ax(mfile0::M_Chain chain) {
		double tmp = 0;
		int count = 0;
		for (auto itr = chain.basetracks.begin(); itr != chain.basetracks.end(); itr++) {
			tmp += itr->ax;
			count++;
		}
		return tmp / count;
	}
	double mfile0::chain_ay(mfile0::M_Chain chain) {
		double tmp = 0;
		int count = 0;
		for (auto itr = chain.basetracks.begin(); itr != chain.basetracks.end(); itr++) {
			tmp += itr->ay;
			count++;
		}
		return tmp / count;
	}
	double mfile0::chain_vph(mfile0::M_Chain chain) {
		double tmp = 0;
		int count = 0;
		for (auto itr = chain.basetracks.begin(); itr != chain.basetracks.end(); itr++) {
			tmp += itr->ph % 10000;
			count++;
		}
		return tmp / count;

	}
	double mfile0::angle_diff_dev_rad(M_Chain chain, double ax, double ay) {
		double sum = 0;
		double sum2 = 0;
		double diff = 0;
		double angle = sqrt(ax*ax + ay * ay);
		int count = 0;
		for (auto itr = chain.basetracks.begin(); itr != chain.basetracks.end(); itr++) {
			diff = (itr->ax*ax + itr->ay*ay) / angle - angle;
			sum += diff;
			sum2 += diff * diff;
			count++;
		}
		return sqrt(sum2 / count - pow(sum / count, 2.));
	}
	double mfile0::angle_diff_dev_lat(M_Chain chain, double ax, double ay) {
		double sum = 0;
		double sum2 = 0;
		double diff = 0;
		double angle = sqrt(ax * ax + ay * ay);
		int count = 0;
		for (auto itr = chain.basetracks.begin(); itr != chain.basetracks.end(); itr++) {
			diff = (itr->ay * ax - itr->ax * ay) / angle;
			sum += diff;
			sum2 += diff * diff;
			count++;
		}
		return sqrt(sum2 / count - pow(sum / count, 2.));
	}
	
	void mfile0::angle_diff_dev_theta(M_Chain &chain, double &ax, double &ay, double &div_rad, double &div_lat) {
		double sum[2] = { 0,0 };
		double sum2[2] = { 0,0 };
		double diff[2] = { 0,0 };
		int count = 0;
		double denominator;
		if (sqrt(ax*ax + ay * ay) > 0.01) {
			denominator = 1 / sqrt(ax*ax + ay * ay + 1);
			double t[3] = {
				ax *denominator,
			ay * denominator,
			1 * denominator
			};
			denominator = 1 / sqrt(ax*ax + ay * ay + pow(ax*ax + ay * ay, 2));
			double r[3] = {
			-1 * ax * denominator,
			-1 * ay * denominator,
				(ax*ax + ay * ay)*denominator
			};
			denominator = 1 / sqrt(ax*ax + ay * ay);
			double l[3] = {
				-1 * ay*denominator,
				ax*denominator,
				0
			};
			for (auto itr = chain.basetracks.begin(); itr != chain.basetracks.end(); itr++) {
				denominator = 1 / (itr->ax*t[0] + itr->ay*t[1] + t[2]);

				diff[0] = (itr->ax*r[0] + itr->ay*r[1] + r[2])*denominator;
				diff[1] = (itr->ax*l[0] + itr->ay*l[1] + l[2])*denominator;
				sum[0] += diff[0];
				sum[1] += diff[1];
				sum2[0] += diff[0] * diff[0];
				sum2[1] += diff[1] * diff[1];
				count++;
			}
		}
		else {
			for (auto itr = chain.basetracks.begin(); itr != chain.basetracks.end(); itr++) {
				diff[0] = itr->ax - ax;
				diff[1] = itr->ay - ay;
				sum[0] += diff[0];
				sum[1] += diff[1];
				sum2[0] += diff[0] * diff[0];
				sum2[1] += diff[1] * diff[1];
				count++;
			}
		}
		div_rad = sqrt(sum2[0] / count - pow(sum[0] / count, 2));
		div_lat = sqrt(sum2[1] / count - pow(sum[1] / count, 2));

	}

	
	
	
	void apply_vph_correction(mfile0::Mfile &m, std::string filename_corr) {
		struct Vol_Correction {
			std::map<int, double> mean, sigma;
		};
		Vol_Correction ang_00_02, ang_02_03, ang_03_06, ang_06_08, ang_08_10, ang_10_12, ang_12_;

		std::ifstream ifs(filename_corr);
		std::string str;

		while (std::getline(ifs, str)) {
			auto str_v = StringSplit(str);
			if (str_v.size() == 15) {
				int pl = stoi(str_v[0]);
				ang_00_02.mean.insert(std::make_pair(pl, stod(str_v[1])));
				ang_02_03.mean.insert(std::make_pair(pl, stod(str_v[3])));
				ang_03_06.mean.insert(std::make_pair(pl, stod(str_v[5])));
				ang_06_08.mean.insert(std::make_pair(pl, stod(str_v[7])));
				ang_08_10.mean.insert(std::make_pair(pl, stod(str_v[9])));
				ang_10_12.mean.insert(std::make_pair(pl, stod(str_v[11])));
				ang_12_.mean.insert(std::make_pair(pl, stod(str_v[13])));

				ang_00_02.sigma.insert(std::make_pair(pl, stod(str_v[2])));
				ang_02_03.sigma.insert(std::make_pair(pl, stod(str_v[4])));
				ang_03_06.sigma.insert(std::make_pair(pl, stod(str_v[6])));
				ang_06_08.sigma.insert(std::make_pair(pl, stod(str_v[8])));
				ang_08_10.sigma.insert(std::make_pair(pl, stod(str_v[10])));
				ang_10_12.sigma.insert(std::make_pair(pl, stod(str_v[12])));
				ang_12_.sigma.insert(std::make_pair(pl, stod(str_v[14])));
			}
			else {
				fprintf(stderr, "incorect file format [%s]\n", filename_corr.c_str());
				exit(1);
			}

		}

		int pl_tmp;
		double angle_tmp;
		int count = 0;
		for (int i = 0; i < m.chains.size(); i++) {
			if (count % 100000 == 0) {
				fprintf(stderr, "\r vph correction apply %d/%d(%4.1lf%%)", count, int(m.chains.size()), count*100. / m.chains.size());
			}
			count++;

			for (auto itr = m.chains[i].basetracks.begin(); itr != m.chains[i].basetracks.end(); itr++) {

				pl_tmp = itr->pos / 10;
				angle_tmp = sqrt(pow(itr->ax, 2.) + pow(itr->ay, 2.));
				if (angle_tmp < 0.2) itr->flg_d[0] = (itr->ph % 10000) / ang_00_02.mean[pl_tmp];
				else if (0.2 <= angle_tmp && angle_tmp < 0.3) itr->flg_d[0] = (itr->ph % 10000) / ang_02_03.mean[pl_tmp];
				else if (0.3 <= angle_tmp && angle_tmp < 0.6) itr->flg_d[0] = (itr->ph % 10000) / ang_03_06.mean[pl_tmp];
				else if (0.6 <= angle_tmp && angle_tmp < 0.8) itr->flg_d[0] = (itr->ph % 10000) / ang_06_08.mean[pl_tmp];
				else if (0.8 <= angle_tmp && angle_tmp < 1.0) itr->flg_d[0] = (itr->ph % 10000) / ang_08_10.mean[pl_tmp];
				else if (1.0 <= angle_tmp && angle_tmp < 1.2) itr->flg_d[0] = (itr->ph % 10000) / ang_10_12.mean[pl_tmp];
				else itr->flg_d[0] = (itr->ph % 10000) / ang_12_.mean[pl_tmp];
			}
		}

		fprintf(stderr, "\r vph correction apply %d/%d(%4.1lf%%)\n", count, int(m.chains.size()), count*100. / m.chains.size());

	}
	void write_chaininf(std::string filename, std::vector<M_Chain_inf> chain_inf) {
		std::ofstream ofs(filename);

		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (chain_inf.size() == 0) {
			fprintf(stderr, "target Chain Information ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		else {
			int count = 0;
			for (auto itr = chain_inf.begin(); itr != chain_inf.end(); itr++) {
				if (count % 10000 == 0) {
					fprintf(stderr, "\r Write chain information dump ... %d/%d (%4.1lf%%)", count, int(chain_inf.size()), count*100. / chain_inf.size());
				}
				count++;
				ofs << std::right << std::fixed
					<< std::setw(10) << std::setprecision(0) << itr->chainID << " "
					<< std::setw(10) << std::setprecision(0) << itr->groupID << " "
					<< std::setw(4) << std::setprecision(0) << itr->nseg << " "
					<< std::setw(4) << std::setprecision(0) << itr->pos0 << " "
					<< std::setw(4) << std::setprecision(0) << itr->pos1 << " "
					<< std::setw(7) << std::setprecision(4) << itr->ax << " "
					<< std::setw(7) << std::setprecision(4) << itr->ay << " "

					<< std::setw(10) << std::setprecision(1) << itr->x_down.second << " "
					<< std::setw(10) << std::setprecision(1) << itr->y_down.second << " "
					<< std::setw(10) << std::setprecision(1) << itr->x_down.first << " "
					<< std::setw(2) << std::setprecision(0) << itr->outflg_down << " "

					<< std::setw(10) << std::setprecision(1) << itr->x_up.second << " "
					<< std::setw(10) << std::setprecision(1) << itr->y_up.second << " "
					<< std::setw(10) << std::setprecision(1) << itr->x_up.first << " "
					<< std::setw(2) << std::setprecision(0) << itr->outflg_up << " "

					<< std::setw(7) << std::setprecision(4) << itr->vph_ratio << " "
					<< std::setw(7) << std::setprecision(4) << itr->vph_slope << " "
					<< std::setw(7) << std::setprecision(4) << itr->vph_slope_acc << " "

					<< std::setw(6) << std::setprecision(4) << itr->radial_deviation << " "
					<< std::setw(6) << std::setprecision(4) << itr->lateral_deviation << " "
					<< std::setw(6) << std::setprecision(4) << itr->d_radial_deviation << " "
					<< std::setw(6) << std::setprecision(4) << itr->d_lateral_deviation << std::endl;
			}
			fprintf(stderr, "\r Write chain information dump ... %d/%d (%4.1lf%%)\n", count, int(chain_inf.size()), count*100. / chain_inf.size());

		}

		ofs.close();
	}
	M_Chain_inf chain2inf(mfile0::M_Chain c) {
		M_Chain_inf ret;
		ret.ax = mfile0::chain_ax(c);
		ret.ay = mfile0::chain_ay(c);
		ret.chainID = c.chain_id;
		ret.groupID = c.basetracks[0].group_id;
		ret.nseg = c.nseg;
		ret.pos0 = c.pos0;
		ret.pos1 = c.pos1;
		ret.x_down = std::make_pair(c.basetracks[0].z, c.basetracks[0].x);
		ret.y_down = std::make_pair(c.basetracks[0].z, c.basetracks[0].y);
		ret.x_up = std::make_pair(c.basetracks[c.nseg - 1].z, c.basetracks[c.nseg - 1].x);
		ret.y_up = std::make_pair(c.basetracks[c.nseg - 1].z, c.basetracks[c.nseg - 1].y);
		ret.outflg_down = false;
		ret.outflg_up = false;

		double  buf[4];
		std::pair<int, double> sum[4], sum2[4];
		for (int i = 0; i < 4; i++) {
			sum[i].first = 0;
			sum[i].second = 0;
			sum2[i].first = 0;
			sum2[i].second = 0;
		}
		buf[0] = ret.ax;
		buf[1] = ret.ay;
		double diff[2];
		//角度小さいときはx-y
		if (sqrt(pow(ret.ax, 2.) + pow(ret.ay, 2.)) < 0.01) {
			for (int i = 0; i < c.basetracks.size(); i++) {
				diff[0] = buf[0] - c.basetracks[i].ax;
				diff[1] = buf[1] - c.basetracks[i].ay;
				sum[0].first++;
				sum2[0].first++;
				sum[0].second += diff[0];
				sum2[0].second += pow(diff[0], 2.);
				sum[1].first++;
				sum2[1].first++;
				sum[1].second += diff[1];
				sum2[1].second += pow(diff[1], 2.);
				if (i != 0) {
					diff[0] = buf[2] - c.basetracks[i].ax;
					diff[1] = buf[3] - c.basetracks[i].ay;

					sum[2].first++;
					sum2[2].first++;
					sum[2].second += diff[0];
					sum2[2].second += pow(diff[0], 2.);
					sum[3].first++;
					sum2[3].first++;
					sum[3].second += diff[1];
					sum2[3].second += pow(diff[1], 2.);
				}
				buf[2] = c.basetracks[i].ax;
				buf[3] = c.basetracks[i].ay;
			}

		}
		//大きいときはrad-lat
		else {
			for (int i = 0; i < c.basetracks.size(); i++) {
				{
					double ax_axis = buf[0];
					double ay_axis = buf[1];
					double ax = c.basetracks[i].ax;
					double ay = c.basetracks[i].ay;

					double r = sqrt(pow(ax_axis, 2) + pow(ay_axis, 2));

					diff[0] = r - (ax_axis*ax + ay_axis * ay) / r;
					diff[1] = (ax_axis*ay - ay_axis * ax) / r;

				}
				//radial 角度差
				sum[0].first++;
				sum2[0].first++;
				sum[0].second += diff[0];
				sum2[0].second += pow(diff[0], 2.);
				//lateral 角度差
				sum[1].first++;
				sum2[1].first++;
				sum[1].second += diff[1];
				sum2[1].second += pow(diff[1], 2.);
				if (i != 0) {
					{
						double ax_axis = buf[2];
						double ay_axis = buf[3];
						double ax = c.basetracks[i].ax;
						double ay = c.basetracks[i].ay;

						double r = sqrt(pow(ax_axis, 2) + pow(ay_axis, 2));

						diff[0] = r - (ax_axis*ax + ay_axis * ay) / r;
						diff[1] = (ax_axis*ay - ay_axis * ax) / r;

					}
					//radial 角度差
					sum[2].first++;
					sum2[2].first++;
					sum[2].second += diff[0];
					sum2[2].second += pow(diff[0], 2.);
					//lateral 角度差
					sum[3].first++;
					sum2[3].first++;
					sum[3].second += diff[1];
					sum2[3].second += pow(diff[1], 2.);
				}
				buf[2] = c.basetracks[i].ax;
				buf[3] = c.basetracks[i].ay;
			}
		}
		double ave, ave_2;
		for (int i = 0; i < 4; i++) {
			if (sum[i].first != 0) {
				ave = sum[i].second / sum[i].first;
				ave_2 = sum2[i].second / sum2[i].first;
				if (ave_2 - pow(ave, 2) >= 0) {
					switch (i)
					{
					case 0:
						ret.radial_deviation = sqrt(ave_2 - pow(ave, 2));
						break;
					case 1:
						ret.lateral_deviation = sqrt(ave_2 - pow(ave, 2));
						break;
					case 2:
						ret.d_radial_deviation = sqrt(ave_2 - pow(ave, 2));
						break;
					case 3:
						ret.d_lateral_deviation = sqrt(ave_2 - pow(ave, 2));
						break;
					default:
						break;
					}
				}
				//丸め誤差でマイナスに
				else if (ave_2 - pow(ave, 2) > -1 * pow(0.001, 2)) {
					switch (i)
					{
					case 0:
						ret.radial_deviation = 0;
						break;
					case 1:
						ret.lateral_deviation = 0;
						break;
					case 2:
						ret.d_radial_deviation = 0;
						break;
					case 3:
						ret.d_lateral_deviation = 0;
						break;
					default:
						break;
					}
				}
				else {
					fprintf(stderr, "Chain ID %lld  cannot calc(deviation)\n", ret.chainID);
					fprintf(stderr, "i=%d in root [%lf]\n", i, ave_2 - pow(ave, 2));
					exit(1);
				}
			}
			else {
				switch (i)
				{
				case 0:
					ret.radial_deviation = -1;
					break;
				case 1:
					ret.lateral_deviation = -1;
					break;
				case 2:
					ret.d_radial_deviation = -1;
					break;
				case 3:
					ret.d_lateral_deviation = -1;
					break;
				default:
					break;
				}

			}

		}

		//vphの計算
		//最小二乗法
		double x, xx, xy, y, num;
		double slope, slope_err, intercept, intercept_err, mean;
		x = 0;
		xx = 0;
		xy = 0;
		y = 0;
		num = 0;
		for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
			x += double(itr->pos / 10);
			xx += pow(double(itr->pos / 10), 2.);
			xy += double(itr->pos / 10)*itr->flg_d[0];
			y += itr->flg_d[0];
			num++;
		}
		if (num <= 1) {
			intercept = 0;
			intercept_err = 0;
			slope = 0;
			slope_err = 0;
			mean = 0;
		}
		else {
			double delta = num * xx - pow(x, 2);
			intercept = (xx*y - x * xy) / delta;
			slope = (num*xy - x * y) / delta;
			mean = y / num;
			//誤差の見積もり
			//統計2以下では直線は一意に定まるため計算不可
			if (num <= 2) {
				intercept_err = 0;
				slope_err = 0;
			}
			else {
				double err_y = 0;
				for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
					err_y += pow(itr->flg_d[0] - (intercept + slope * double(itr->pos / 10)), 2);
				}
				err_y = sqrt(err_y / (num - 2));
				intercept_err = err_y * sqrt(xx / delta);
				slope_err = err_y * sqrt(num / delta);
			}
		}
		ret.vph_slope = slope;
		ret.vph_slope_acc = slope_err;
		ret.vph_ratio = mean;

		return ret;
	}
	void chain_inf_flg(M_Chain_inf &inf, mfile0::M_Chain c, int pl0, int pl1, double range[4], std::map<int, double> z) {
		if (c.pos0 / 10 <= pl0) {
			inf.outflg_down = true;
		}
		else {
			double position[2];
			int pl = c.basetracks[0].pos / 10;
			position[0] = c.basetracks[0].x + c.basetracks[0].ax*(z[pl - 1] - z[pl]);
			position[1] = c.basetracks[0].y + c.basetracks[0].ay*(z[pl - 1] - z[pl]);
			if (position[0] < range[0] || range[1] < position[0] || position[1] < range[2] || range[3] < position[1]) {
				inf.outflg_down = true;
			}
		}
		if (c.pos1 / 10 >= pl1) {
			inf.outflg_up = true;
		}
		else {
			double position[2];
			int pl = c.basetracks[0].pos / 10;
			position[0] = c.basetracks[c.nseg - 1].x + c.basetracks[c.nseg - 1].ax*(z[pl + 1] - z[pl]);
			position[1] = c.basetracks[c.nseg - 1].y + c.basetracks[c.nseg - 1].ay*(z[pl + 1] - z[pl]);
			if (position[0] < range[0] || range[1] < position[0] || position[1] < range[2] || range[3] < position[1]) {
				inf.outflg_up = true;
			}
		}

	}
	void set_header(int pl0, int pl1, mfile0::Mfile &m) {
		mfile0::M_Header head;
		for (int pl = pl0; pl <= pl1; pl++) {
			head.all_pos.push_back(pl * 10 + 1);
		}
		head.num_all_plate = int(head.all_pos.size());
		head.head[0] = "% Created by mkmf";
		head.head[1] = "% on 2007 / 12 / 25 23:28 : 09 + 09:00 (JST)";
		head.head[2] = "0       0   3   0      0.0   0.0000";
		m.header = head;
	}
	void mfile0::write_mfile_chain_IVE(std::ofstream &ofs, const M_Chain &chains) {
		//IVE描画用出力
		double base_extra = 180;
		for (int i = 0; i < chains.basetracks.size(); i++) {
			ofs << std::right << std::fixed
				<< std::setw(12) << chains.chain_id << " "
				<< std::setw(10) << chains.basetracks.begin()->group_id << " "
				<< std::setw(4) << chains.basetracks[i].pos / 10 << " "
				<< std::setw(12) << chains.basetracks[i].rawid << " "
				<< std::setw(2) << chains.basetracks[i].ph / 10000 << " "
				<< std::setw(4) << chains.basetracks[i].ph % 10000 << " "
				//basetrack描画param
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[i].x - chains.basetracks[i].ax*base_extra << " "
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[i].y - chains.basetracks[i].ay*base_extra << " "
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[i].z - base_extra << " "
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[i].x + chains.basetracks[i].ax*base_extra << " "
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[i].y + chains.basetracks[i].ay*base_extra << " "
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[i].z + base_extra << " "
				//chain描画param
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[i].x << " "
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[i].y << " "
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[i].z << " "
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[std::max(0, i - 1)].x << " "
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[std::max(0, i - 1)].y << " "
				<< std::setw(8) << std::setprecision(1) << chains.basetracks[std::max(0, i - 1)].z << std::endl;
		}
	}
	double chain_fit_dist(M_Chain chain,double slope[3],double intercept[3],double &dist) {
		if (chain.nseg < 3) {
			fprintf(stderr, "segment < 3 cannot fit\n");
			exit(1);
		}
		std::vector<double> x_v, y_v, z_v;
		for (auto itr = chain.basetracks.begin(); itr != chain.basetracks.end(); itr++) {
			x_v.push_back(itr->x);
			y_v.push_back(itr->y);
			z_v.push_back(itr->z);
		}
		double slope_error[2] = {};
		double position_error[2] = {};
		//z-xでの最小二乗
		double x, y, xx, xy, n;
		x = 0;
		y = 0;
		xx = 0;
		xy = 0;
		n = 0;
		for (int i = 0; i < z_v.size(); i++){
			x += z_v[i];
			y += x_v[i];
			xx += z_v[i] * z_v[i];
			xy += z_v[i] * x_v[i];
			n++;
		}
		slope[0] = (n*xy - x * y) / (n*xx - x * x);
		intercept[0]=(xx*y-xy*x)/ (n*xx - x * x);
		//fitからの差
		for (int i = 0; i < z_v.size(); i++) {
			slope_error[0] += pow((slope[0] * z_v[i] + intercept[0]) - x_v[i], 2);
		}
		position_error[0] = sqrt(slope_error[0] / (n - 2));
		slope_error[0] = position_error[0] * sqrt(n / (n*xx - x * x));

		//z-yでの最小二乗
		x = 0;
		y = 0;
		xx = 0;
		xy = 0;
		n = 0;
		for (int i = 0; i < z_v.size(); i++) {
			x += z_v[i];
			y += y_v[i];
			xx += z_v[i] * z_v[i];
			xy += z_v[i] * y_v[i];
			n++;
		}
		slope[1] = (n*xy - x * y) / (n*xx - x * x);
		intercept[1] = (xx*y - xy * x) / (n*xx - x * x);
		//fitからの差
		for (int i = 0; i < z_v.size(); i++) {
			slope_error[1] += pow((slope[1] * z_v[i] + intercept[1]) - y_v[i], 2);
		}
		position_error[1] = sqrt(slope_error[1] / (n - 2));
		slope_error[1] = position_error[1] * sqrt(n / (n*xx - x * x));

		slope[2] = 1;
		intercept[2] = 0;

		double sigma = 3;
		//位置のばらつき
		dist = sqrt(pow(position_error[0] * sigma, 2) + pow(position_error[1] * sigma, 2));
		//傾きに対する誤差が小さいものを選ぶ
		return sqrt(pow(slope_error[0], 2) + pow(slope_error[1], 2));
	}
}

namespace mfile1 {
	//mfile txt-->binary
	void mfile1::converter(const mfile0::Mfile& old, MFile& mfile) {
		int count = 0;
		for (auto& c : old.chains) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r mfile convert txt-->binay (%4.1lf%%)", count*100. / old.chains.size());
			}
			count++;

			MFileChain1 chain;
			chain.chain_id = c.chain_id;
			chain.nseg = c.nseg;
			chain.pos0 = c.pos0;
			chain.pos1 = c.pos1;
			mfile.chains.emplace_back(chain);
			std::vector< MFileBase1> basetracks;
			for (auto& b : c.basetracks) {
				MFileBase1 base;

				base.pos = b.pos;
				base.group_id = b.group_id;
				base.rawid = b.rawid;
				base.ph = b.ph;
				base.ax = b.ax;
				base.ay = b.ay;
				base.x = b.x;
				base.y = b.y;
				base.z = b.z;
				basetracks.emplace_back(base);
			}
			mfile.all_basetracks.emplace_back(basetracks);
		}
		fprintf(stderr, "\r mfile convert txt-->binay (%4.1lf%%)\n", count*100. / old.chains.size());
	}
	//mfile binaty-->txt
	void mfile1::converter(const MFile& old, mfile0::Mfile& mfile) {
		mfile.header.head[0] = "% Created by mkmf";
		mfile.header.head[1] = "% on 2007 / 12 / 25 23:28 : 09 + 09:00 (JST)";
		mfile.header.head[2] = "	0       0   3   0      0.0   0.0000";
		std::set<int> pos;

		int count = 0;
		for (int i = 0; i < old.chains.size(); i++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r mfile convert binay-->txt (%4.1lf%%)", count*100. / old.chains.size());
			}
			count++;

			mfile0::M_Chain chain;
			chain.chain_id = old.chains[i].chain_id;
			chain.nseg = old.chains[i].nseg;
			chain.pos0 = old.chains[i].pos0;
			chain.pos1 = old.chains[i].pos1;
			for (int j = 0; j < old.all_basetracks[i].size(); j++) {
				mfile0::M_Base base;
				base.pos = old.all_basetracks[i][j].pos;
				base.group_id = old.all_basetracks[i][j].group_id;
				base.rawid = old.all_basetracks[i][j].rawid;
				base.ph = old.all_basetracks[i][j].ph;
				base.ax = old.all_basetracks[i][j].ax;
				base.ay = old.all_basetracks[i][j].ay;
				base.x = old.all_basetracks[i][j].x;
				base.y = old.all_basetracks[i][j].y;
				base.z = old.all_basetracks[i][j].z;
				base.flg_d[0] = 0;
				base.flg_d[1] = 0;
				base.flg_i[0] = 0;
				base.flg_i[1] = 0;
				base.flg_i[2] = 0;
				base.flg_i[3] = 0;

				pos.insert(base.pos);
				chain.basetracks.push_back(base);
			}
			mfile.chains.push_back(chain);
		}
		fprintf(stderr, "\r mfile convert binay-->txt (%4.1lf%%)\n", count*100. / old.chains.size());

		mfile.header.num_all_plate = int(pos.size());

		for (auto itr = pos.begin(); itr != pos.end(); itr++) {
			mfile.header.all_pos.push_back(*itr);
		}
	}
	//mfile binaty-->txt
	void mfile1::converter(const MFile_minimum& old, mfile0::Mfile& mfile) {
		mfile.header.head[0] = "% Created by mkmf";
		mfile.header.head[1] = "% on 2007 / 12 / 25 23:28 : 09 + 09:00 (JST)";
		mfile.header.head[2] = "	0       0   3   0      0.0   0.0000";
		std::set<int> pos;

		int count = 0;
		for (int i = 0; i < old.chains.size(); i++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r mfile convert binay-->txt (%4.1lf%%)", count*100. / old.chains.size());
			}
			count++;

			mfile0::M_Chain chain;
			chain.chain_id = old.chains[i].chain_id;
			chain.nseg = old.chains[i].nseg;
			chain.pos0 = old.chains[i].pos0;
			chain.pos1 = old.chains[i].pos1;
			for (int j = 0; j < old.all_basetracks[i].size(); j++) {
				mfile0::M_Base base;
				base.pos = old.all_basetracks[i][j].pos;
				base.group_id = old.all_basetracks[i][j].group_id;
				base.rawid = old.all_basetracks[i][j].rawid;
				base.ph = old.all_basetracks[i][j].ph;
				base.ax = old.all_basetracks[i][j].ax;
				base.ay = old.all_basetracks[i][j].ay;
				base.x = old.all_basetracks[i][j].x;
				base.y = old.all_basetracks[i][j].y;
				base.z = old.all_basetracks[i][j].z;
				base.flg_d[0] = 0;
				base.flg_d[1] = 0;
				base.flg_i[0] = 0;
				base.flg_i[1] = 0;
				base.flg_i[2] = 0;
				base.flg_i[3] = 0;

				pos.insert(base.pos);
				chain.basetracks.push_back(base);
			}
			mfile.chains.push_back(chain);
		}
		fprintf(stderr, "\r mfile convert binay-->txt (%4.1lf%%)\n", count*100. / old.chains.size());

		mfile.header.num_all_plate = int(pos.size());

		for (auto itr = pos.begin(); itr != pos.end(); itr++) {
			mfile.header.all_pos.push_back(*itr);
		}
	}

	void mfile1::read_mfile(std::string filepath, MFile& mfile) {
		std::ifstream ifs(filepath, std::ios::binary);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
		}
		//Mfile headerの読み込み
		ifs.read((char*)& mfile.header, sizeof(MFileHeader));
		if (ifs.eof()) { throw std::exception(); }
		std::string  filetype = "mfile-a0";
		memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
		if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

		//mfile info headerの読み込み
		ifs.read((char*)& mfile.info_header, sizeof(MFileInfoHeader));
		if (ifs.eof()) { throw std::exception(); }

		if (sizeof(MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
		if (sizeof(MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }

		std::vector< MFileChain1> chains;
		chains.reserve(mfile.info_header.Nchain);

		std::vector< std::vector< MFileBase1>> all_basetracks;
		uint64_t count = 0;
		for (uint64_t c = 0; c < mfile.info_header.Nchain; c++) {
			if (count % 100000 == 0) {
				auto nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			count++;
			MFileChain1 chain;
			ifs.read((char*)& chain, sizeof(MFileChain));
			if (ifs.eof()) { throw std::exception(); }
			chains.emplace_back(chain);

			std::vector< MFileBase1> basetracks;
			basetracks.reserve(chain.nseg);

			for (int b = 0; b < chain.nseg; b++) {
				MFileBase1 base;
				ifs.read((char*)& base, sizeof(MFileBase));
				if (ifs.eof()) { throw std::exception(); }
				basetracks.emplace_back(base);
			}
			all_basetracks.emplace_back(basetracks);
		}
		auto nowpos = ifs.tellg();
		auto size1 = nowpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

		size_t Nbasetrack = 0;
		for (auto& p : all_basetracks) {
			Nbasetrack += p.size();
		}

		if (Nbasetrack != mfile.info_header.Nbasetrack) { throw std::exception("Nbasetrack is wrong."); }

		mfile.chains.swap(chains);
		mfile.all_basetracks.swap(all_basetracks);
	}
	void mfile1::read_mfile(std::string filepath, MFile_minimum& mfile) {
		std::ifstream ifs(filepath, std::ios::binary);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
		}
		//Mfile headerの読み込み
		ifs.read((char*)& mfile.header, sizeof(MFileHeader));
		if (ifs.eof()) { throw std::exception(); }
		std::string  filetype = "mfile-a0";
		memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
		if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

		//mfile info headerの読み込み
		ifs.read((char*)& mfile.info_header, sizeof(MFileInfoHeader));
		if (ifs.eof()) { throw std::exception(); }

		if (sizeof(MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
		if (sizeof(MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }

		std::vector< MFileChain> chains;
		chains.reserve(mfile.info_header.Nchain);

		std::vector< std::vector< MFileBase>> all_basetracks;
		int count = 0;
		for (uint32_t c = 0; c < mfile.info_header.Nchain; c++) {
			if (count % 100000 == 0) {
				auto nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			count++;
			MFileChain chain;
			ifs.read((char*)& chain, sizeof(MFileChain));
			if (ifs.eof()) { throw std::exception(); }
			chains.emplace_back(chain);

			std::vector< MFileBase> basetracks;
			basetracks.reserve(chain.nseg);

			for (int b = 0; b < chain.nseg; b++) {
				MFileBase base;
				ifs.read((char*)& base, sizeof(MFileBase));
				if (ifs.eof()) { throw std::exception(); }
				basetracks.emplace_back(base);
			}
			all_basetracks.emplace_back(basetracks);
		}
		auto nowpos = ifs.tellg();
		auto size1 = nowpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

		size_t Nbasetrack = 0;
		for (auto& p : all_basetracks) {
			Nbasetrack += p.size();
		}

		if (Nbasetrack != mfile.info_header.Nbasetrack) { throw std::exception("Nbasetrack is wrong."); }

		mfile.chains.swap(chains);
		mfile.all_basetracks.swap(all_basetracks);

	}
	void mfile1::read_mfile(std::string filepath, MFile_minimum& mfile, int nseg_threshold) {
		std::ifstream ifs(filepath, std::ios::binary);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
		}
		//Mfile headerの読み込み
		ifs.read((char*)& mfile.header, sizeof(MFileHeader));
		if (ifs.eof()) { throw std::exception(); }
		std::string  filetype = "mfile-a0";
		memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
		if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

		//mfile info headerの読み込み
		ifs.read((char*)& mfile.info_header, sizeof(MFileInfoHeader));
		if (ifs.eof()) { throw std::exception(); }

		if (sizeof(MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
		if (sizeof(MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }

		std::vector< MFileChain> chains;
		chains.reserve(mfile.info_header.Nchain);

		std::vector< std::vector< MFileBase>> all_basetracks;
		int count = 0;
		for (uint32_t c = 0; c < mfile.info_header.Nchain; c++) {
			if (count % 100000 == 0) {
				auto nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			count++;
			MFileChain chain;
			ifs.read((char*)& chain, sizeof(MFileChain));
			if (ifs.eof()) { throw std::exception(); }

			std::vector< MFileBase> basetracks;
			basetracks.reserve(chain.nseg);

			for (int b = 0; b < chain.nseg; b++) {
				MFileBase base;
				ifs.read((char*)& base, sizeof(MFileBase));
				if (ifs.eof()) { throw std::exception(); }
				basetracks.emplace_back(base);
			}
			if (basetracks.size() < nseg_threshold)continue;
			chains.emplace_back(chain);
			all_basetracks.emplace_back(basetracks);
		}
		auto nowpos = ifs.tellg();
		auto size1 = nowpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

		size_t Nbasetrack = 0, Nchain = 0;
		for (auto& p : all_basetracks) {
			Nbasetrack += p.size();
			Nchain++;
		}
		mfile.info_header.Nbasetrack = Nbasetrack;
		mfile.info_header.Nchain = Nchain;
		//if (Nbasetrack != mfile.info_header.Nbasetrack) { throw std::exception("Nbasetrack is wrong."); }

		mfile.chains.swap(chains);
		mfile.all_basetracks.swap(all_basetracks);

	}
	void mfile1::read_mfile_txt(std::string filepath, MFile& mfile) {

		std::ifstream ifs(filepath);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
		}

		std::string str;

		mfile0::M_Header head_tmp;
		for (int i = 0; i < 3; i++) {
			std::getline(ifs, str);
			head_tmp.head[i] = str;
		}
		std::getline(ifs, str);
		head_tmp.num_all_plate = stoi(str);

		{
			std::getline(ifs, str);
			auto str_v = StringSplit(str);
			for (int i = 0; i < str_v.size(); i++) {
				head_tmp.all_pos.push_back(stoi(str_v[i]));
			}
		}

		int cnt = 0;
		std::vector< MFileChain1> chains;
		chains.reserve(mfile.info_header.Nchain);

		std::vector< std::vector< MFileBase1>> all_basetracks;

		while (std::getline(ifs, str)) {
			if (cnt % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			cnt++;
			MFileChain1 chain;

			auto strs = StringSplit(str);
			if (strs.size() == 4) {
				chain.chain_id = stoi(strs[0]);
				chain.nseg = stoi(strs[1]);
				chain.pos0 = stoi(strs[2]);
				chain.pos1 = stoi(strs[3]);
				std::vector< MFileBase1> basetracks;

				for (int i = 0; i < chain.nseg; i++) {
					MFileBase1 base;
					//int pos, group_id;
					//uint64_t rawid;
					//int ph;
					//float ax, ay;
					//double x, y, z;

					std::getline(ifs, str);
					strs = StringSplit(str);
					if (strs.size() == 15) {
						base.pos = stoi(strs[0]);
						base.group_id = stoi(strs[1]);
						base.rawid = stoi(strs[2]);
						base.ph = stoi(strs[3]);
						base.ax = stof(strs[4]);
						base.ay = stof(strs[5]);
						base.x = stod(strs[6]);
						base.y = stod(strs[7]);
						base.z = stod(strs[8]);
						base.tmp[0] = 0;
						base.tmp[1] = 0;
						base.tmp[2] = 0;
						base.tmp[3] = 0;
						basetracks.emplace_back(base);
					}
					else {
						fprintf(stderr, "mfile format err! cannot read file\n");
						fprintf(stderr, "base track information 15 -->%d\n", int(strs.size()));
						for (int i = 0; i < strs.size(); i++) {
							printf("%s ", strs[i].c_str());
						}
						printf("\n");
						throw std::exception();
					}
				}
				all_basetracks.emplace_back(basetracks);
				chains.emplace_back(chain);
			}
			else {
				fprintf(stderr, "mfile format err! cannot read file\n");
				fprintf(stderr, "chain information 4 -->%d\n", int(strs.size()));
				for (int i = 0; i < strs.size(); i++) {
					printf("%s ", strs[i].c_str());
				}
				printf("\n");

				throw std::exception();
			}
		}
		auto size1 = eofpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
		if (cnt == 0) {
			fprintf(stderr, "%s no chain!\n", filepath.c_str());
			exit(1);
		}


		mfile.chains.swap(chains);
		mfile.all_basetracks.swap(all_basetracks);
	}

	void mfile1::write_mfile(std::string filepath, MFile& mfile) {
		std::ofstream ofs(filepath, std::ios::binary);

		std::string  filetype = "mfile-a0";
		mfile.header.filetype = 0;
		memcpy((char*)& mfile.header.filetype, filetype.data(), filetype.size());

		mfile.info_header.classsize1 = sizeof(MFileChain);
		mfile.info_header.classsize2 = sizeof(MFileBase);

		mfile.info_header.Nchain = mfile.chains.size();
		assert(mfile.chains.size() == mfile.all_basetracks.size());

		size_t Nbasetrack = 0;
		for (auto& p : mfile.all_basetracks) {
			Nbasetrack += p.size();
		}
		mfile.info_header.Nbasetrack = Nbasetrack;

		ofs.write((char*)& mfile.header, sizeof(MFileHeader));
		ofs.write((char*)& mfile.info_header, sizeof(MFileInfoHeader));

		int count = 0;
		int64_t max = mfile.info_header.Nchain;

		for (int c = 0; c < mfile.info_header.Nchain; c++) {
			if (count % 10000 == 0) {
				std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
			}
			count++;
			ofs.write((char*)& mfile.chains[c], sizeof(MFileChain));
			assert(mfile.chains[c].nseg == mfile.all_basetracks[c].size());
			for (int b = 0; b < mfile.chains[c].nseg; b++) {
				ofs.write((char*)& mfile.all_basetracks[c][b], sizeof(MFileBase));
			}
		}
		std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;

	}
	void mfile1::write_mfile(std::string filepath, MFile_minimum& mfile) {
		std::ofstream ofs(filepath, std::ios::binary);

		std::string  filetype = "mfile-a0";
		mfile.header.filetype = 0;
		memcpy((char*)& mfile.header.filetype, filetype.data(), filetype.size());

		mfile.info_header.classsize1 = sizeof(MFileChain);
		mfile.info_header.classsize2 = sizeof(MFileBase);

		mfile.info_header.Nchain = mfile.chains.size();
		assert(mfile.chains.size() == mfile.all_basetracks.size());

		size_t Nbasetrack = 0;
		for (auto& p : mfile.all_basetracks) {
			Nbasetrack += p.size();
		}
		mfile.info_header.Nbasetrack = Nbasetrack;

		ofs.write((char*)& mfile.header, sizeof(MFileHeader));
		ofs.write((char*)& mfile.info_header, sizeof(MFileInfoHeader));

		int count = 0;
		int64_t max = mfile.info_header.Nchain;

		for (int c = 0; c < mfile.info_header.Nchain; c++) {
			if (count % 10000 == 0) {
				std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
			}
			count++;
			ofs.write((char*)& mfile.chains[c], sizeof(MFileChain));
			assert(mfile.chains[c].nseg == mfile.all_basetracks[c].size());
			for (int b = 0; b < mfile.chains[c].nseg; b++) {
				ofs.write((char*)& mfile.all_basetracks[c][b], sizeof(MFileBase));
			}
		}
		std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;

	}
	void mfile1::read_mfile_extension(std::string filename, mfile0::Mfile &m) {
		m.chains.reserve(100000000);
		std::string extension;
		extension = filename.substr(filename.size() - 3, 3);
		if (extension == "bmf") {
			mfile1::MFile m_tmp;
			mfile1::read_mfile(filename, m_tmp);
			mfile1::converter(m_tmp, m);
		}
		else {
			mfile0::read_mfile(filename, m);
		}
		m.chains.shrink_to_fit();
	}
	void mfile1::write_mfile_extension(std::string filename, mfile0::Mfile &m) {
		std::string extension;
		extension = filename.substr(filename.size() - 3, 3);
		if (extension == "bmf") {
			mfile1::MFile m_tmp;
			mfile1::converter(m, m_tmp);
			mfile1::write_mfile(filename, m_tmp);
		}
		else {
			mfile0::write_mfile(filename, m);
		}
	}



	double mfile1::MFile::chain_ax(int i) {
		double tmp = 0;
		int count = 0;
		for (auto itr = all_basetracks[i].begin(); itr != all_basetracks[i].end(); itr++) {
			tmp += itr->ax;
			count++;
		}
		return tmp / count;
	}
	double mfile1::MFile::chain_ay(int i) {
		double tmp = 0;
		int count = 0;
		for (auto itr = all_basetracks[i].begin(); itr != all_basetracks[i].end(); itr++) {
			tmp += itr->ay;
			count++;
		}
		return tmp / count;
	}
	double mfile1::MFile::angle_diff_dev_rad(int i, double ax, double ay) {
		double sum = 0;
		double sum2 = 0;
		double diff = 0;
		double angle = sqrt(ax*ax + ay * ay);
		int count = 0;
		for (auto itr = all_basetracks[i].begin(); itr != all_basetracks[i].end(); itr++) {
			diff = (itr->ax*ax + itr->ay*ay) / angle - angle;
			sum += diff;
			sum2 += diff * diff;
			count++;
		}
		return sqrt(sum2 / count - pow(sum / count, 2.));
	}
	double mfile1::MFile::angle_diff_dev_lat(int i, double ax, double ay) {
		double sum = 0;
		double sum2 = 0;
		double diff = 0;
		double angle = sqrt(ax*ax + ay * ay);
		int count = 0;
		for (auto itr = all_basetracks[i].begin(); itr != all_basetracks[i].end(); itr++) {
			diff = (itr->ay*ax - itr->ay*ax) / angle;
			sum += diff;
			sum2 += diff * diff;
			count++;
		}
		return sqrt(sum2 / count - pow(sum / count, 2.));
	}
	double chain_ax(std::vector<MFileBase> &b) {
		double tmp = 0;
		int count = 0;
		for (auto itr = b.begin(); itr != b.end(); itr++) {
			tmp += itr->ax;
			count++;
		}
		return tmp / count;
	}
	double chain_ay(std::vector<MFileBase> &b) {
		double tmp = 0;
		int count = 0;
		for (auto itr = b.begin(); itr != b.end(); itr++) {
			tmp += itr->ay;
			count++;
		}
		return tmp / count;

	}
	double angle_diff_dev_rad(std::vector<MFileBase> &b) {
		double sum = 0;
		double sum2 = 0;
		double diff = 0;
		double ax = chain_ax(b);
		double ay = chain_ay(b);
		double angle = sqrt(ax*ax + ay * ay);
		int count = 0;
		for (auto itr = b.begin(); itr != b.end(); itr++) {
			diff = (itr->ax*ax + itr->ay*ay) / angle - angle;
			sum += diff;
			sum2 += diff * diff;
			count++;
		}
		return sqrt(sum2 / count - pow(sum / count, 2.));

	}
	double angle_diff_dev_lat(std::vector<MFileBase> &b) {
		double sum = 0;
		double sum2 = 0;
		double diff = 0;
		double ax = chain_ax(b);
		double ay = chain_ay(b);
		double angle = sqrt(ax*ax + ay * ay);
		int count = 0;
		for (auto itr = b.begin(); itr != b.end(); itr++) {
			diff = (itr->ay*ax - itr->ax*ay) / angle;
			sum += diff;
			sum2 += diff * diff;
			count++;
		}
		return sqrt(sum2 / count - pow(sum / count, 2.));

	}
	void angle_diff_dev_theta(std::vector<MFileBase> &b, double &ax, double &ay, double &div_rad, double &div_lat) {
		double sum[2] = { 0,0 };
		double sum2[2] = { 0,0 };
		double diff[2] = { 0,0 };
		int count = 0;
		double denominator;
		if (sqrt(ax*ax + ay * ay) > 0.01) {
			denominator = 1 / sqrt(ax*ax + ay * ay + 1);
			double t[3] = {
				ax *denominator,
			ay * denominator,
			1 * denominator
			};
			denominator = 1 / sqrt(ax*ax + ay * ay + pow(ax*ax + ay * ay, 2));
			double r[3] = {
			-1 * ax * denominator,
			-1 * ay * denominator,
				(ax*ax + ay * ay)*denominator
			};
			denominator = 1 / sqrt(ax*ax + ay * ay);
			double l[3] = {
				-1 * ay*denominator,
				ax*denominator,
				0
			};
			for (auto itr = b.begin(); itr != b.end(); itr++) {
				denominator = 1 / (itr->ax*t[0] + itr->ay*t[1] + t[2]);

				diff[0] = (itr->ax*r[0] + itr->ay*r[1] + r[2])*denominator;
				diff[1] = (itr->ax*l[0] + itr->ay*l[1] + l[2])*denominator;
				sum[0] += diff[0];
				sum[1] += diff[1];
				sum2[0] += diff[0] * diff[0];
				sum2[1] += diff[1] * diff[1];
				count++;
			}
		}
		else {
			for (auto itr = b.begin(); itr != b.end(); itr++) {
				diff[0] = itr->ax - ax;
				diff[1] = itr->ay - ay;
				sum[0] += diff[0];
				sum[1] += diff[1];
				sum2[0] += diff[0] * diff[0];
				sum2[1] += diff[1] * diff[1];
				count++;
			}
		}
		div_rad = sqrt(sum2[0] / count - pow(sum[0] / count, 2));
		div_lat = sqrt(sum2[1] / count - pow(sum[1] / count, 2));

	}
	double chain_vph(std::vector<MFileBase> &b) {
		double tmp = 0;
		int count = 0;
		for (auto itr = b.begin(); itr != b.end(); itr++) {
			tmp += itr->ph % 10000;
			count++;
		}
		return tmp / count;
	}
	double chain_ph(std::vector<MFileBase> &b) {
		double tmp = 0;
		int count = 0;
		for (auto itr = b.begin(); itr != b.end(); itr++) {
			tmp += (int)(itr->ph / 10000);
			count++;
		}
		return tmp / count;
	}
	double angle_diff_mom_iron(std::vector<MFileBase> &b,int num_thr) {

		double sum = 0;
		double sum2 = 0;
		double diff = 0;
		double ax, ay, angle;
		ax = 0;
		ay = 0;
		int pl, count = 0;
		for (auto itr = b.begin(); itr != b.end(); itr++) {
			if (itr + 1 == b.end())continue;
			if ((itr + 1)->pos / 10 - itr->pos / 10 != 1)continue;
			pl = itr->pos / 10;
			if (pl == 1 || pl == 2 || pl == 3||pl==15)continue;
			if (pl > 15 && pl % 2 == 1)continue;
			angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
			diff = ((itr + 1)->ay*itr->ax - (itr + 1)->ax*itr->ay) / angle;
			sum += diff;
			sum2 += diff * diff;
			count++;
			ax += ((itr + 1)->ax + itr->ax) / 2;
			ay += ((itr + 1)->ay + itr->ay) / 2;
		}
		if (count < num_thr)return -1;
		ax = ax / count;
		ay = ay / count;
		double rms= sqrt(sum2 / count - pow(sum / count, 2.));
		double iron_thick = 500 * sqrt(1 + ax * ax + ay * ay)*count;
		double iron_rad = 17.57 * 1000;
		double bpc = 13.6 / rms * sqrt(iron_thick / iron_rad)*(1 + 0.038*std::log(iron_thick / iron_rad));
		return bpc;

	}

}

namespace netscan {

	bool read_linklet_txt(std::string filename, std::vector<linklet_t> &link, int output) {

		std::ifstream ifs(filename);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (output != 0) {
			if (GB > 0) {
				std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
			}
			else {
				std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
			}
		}

		std::string str;
		std::vector<std::string> str_v;
		std::string buffer;
		int cnt = 0;
		while (std::getline(ifs, str))
		{
			str_v.clear();
			str_v = StringSplit(str);
			std::stringstream ss(str);

			linklet_t linklet;

			linklet.pos[0] = stoi(str_v[0]);
			linklet.pos[1] = stoi(str_v[2]);

			linklet.b[0].pl = stoi(str_v[0]) / 10;
			linklet.b[0].rawid = stoi(str_v[1]);
			linklet.b[1].pl = stoi(str_v[2]) / 10;
			linklet.b[1].rawid = stoi(str_v[3]);

			linklet.b[0].ax = stod(str_v[5]);
			linklet.b[0].ay = stod(str_v[6]);
			linklet.b[0].x = stod(str_v[7]);
			linklet.b[0].y = stod(str_v[8]);

			linklet.b[1].ax = stod(str_v[10]);
			linklet.b[1].ay = stod(str_v[11]);
			linklet.b[1].x = stod(str_v[12]);
			linklet.b[1].y = stod(str_v[13]);

			linklet.b[0].z = stod(str_v[14]);
			linklet.b[1].z = stod(str_v[15]);

			linklet.zproj = stod(str_v[16]);
			linklet.xc = stod(str_v[17]);
			linklet.yc = stod(str_v[18]);

			linklet.b[0].m[0].pos = stoi(str_v[19]);
			linklet.b[0].m[0].col = stoi(str_v[20]);
			linklet.b[0].m[0].row = stoi(str_v[21]);
			linklet.b[0].m[0].zone = stoi(str_v[22]);
			linklet.b[0].m[0].isg = stoi(str_v[23]);

			linklet.b[0].m[1].pos = stoi(str_v[24]);
			linklet.b[0].m[1].col = stoi(str_v[25]);
			linklet.b[0].m[1].row = stoi(str_v[26]);
			linklet.b[0].m[1].zone = stoi(str_v[27]);
			linklet.b[0].m[1].isg = stoi(str_v[28]);

			linklet.b[1].m[0].pos = stoi(str_v[29]);
			linklet.b[1].m[0].col = stoi(str_v[30]);
			linklet.b[1].m[0].row = stoi(str_v[31]);
			linklet.b[1].m[0].zone = stoi(str_v[32]);
			linklet.b[1].m[0].isg = stoi(str_v[33]);

			linklet.b[1].m[1].pos = stoi(str_v[34]);
			linklet.b[1].m[1].col = stoi(str_v[35]);
			linklet.b[1].m[1].row = stoi(str_v[36]);
			linklet.b[1].m[1].zone = stoi(str_v[37]);
			linklet.b[1].m[1].isg = stoi(str_v[38]);


			linklet.b[0].m[0].rawid = stoi(str_v[41]);
			linklet.b[0].m[0].ph = stoi(str_v[42]);
			linklet.b[0].m[0].ax = stod(str_v[43]);
			linklet.b[0].m[0].ay = stod(str_v[44]);

			linklet.b[0].m[1].rawid = stoi(str_v[45]);
			linklet.b[0].m[1].ph = stoi(str_v[46]);
			linklet.b[0].m[1].ax = stod(str_v[47]);
			linklet.b[0].m[1].ay = stod(str_v[48]);

			linklet.b[1].m[0].rawid = stoi(str_v[49]);
			linklet.b[1].m[0].ph = stoi(str_v[50]);
			linklet.b[1].m[0].ax = stod(str_v[51]);
			linklet.b[1].m[0].ay = stod(str_v[52]);

			linklet.b[1].m[1].rawid = stoi(str_v[53]);
			linklet.b[1].m[1].ph = stoi(str_v[54]);
			linklet.b[1].m[1].ax = stod(str_v[55]);
			linklet.b[1].m[1].ay = stod(str_v[56]);

			linklet.dx = stod(str_v[57]);
			linklet.dy = stod(str_v[58]);


			link.push_back(linklet);
			if (output != 0) {
				if (cnt % 10000 == 0) {
					nowpos = ifs.tellg();
					auto size1 = nowpos - begpos;
					std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
				}
			}
			cnt++;
		}

		if (output != 0) {
			auto size1 = eofpos - begpos;
			std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
		}
		if (cnt == 0) {
			fprintf(stderr, "%s no linklet!\n", filename.c_str());
			exit(1);
		}
		return true;

	}
	bool read_linklet_bin(std::string filename, std::vector<linklet_t> &link) {

		std::ifstream ifs(filename, std::ios::binary);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
		int64_t count = 0;
		linklet_t l;
		while (ifs.read((char*)& l, sizeof(linklet_t))) {
			if (count % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			count++;

			link.emplace_back(l);
		}
		auto size1 = eofpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
		if (count == 0) {
			fprintf(stderr, "%s no linklet!\n", filename.c_str());
			exit(1);
		}
		return true;

	}
	bool read_basetrack_txt(std::string filename, std::vector<base_track_t> &base, int output) {

		std::ifstream ifs(filename);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (output == 1) {
			if (GB > 0) {
				std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
			}
			else {
				std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
			}
		}

		std::string str;
		std::vector<std::string> str_v;
		std::string buffer;
		int cnt = 0;
		while (std::getline(ifs, str))
		{
			str_v.clear();
			str_v = StringSplit(str);
			std::stringstream ss(str);
			base_track_t basetrack;

			basetrack.rawid = stoi(str_v[0]);
			basetrack.pl = stoi(str_v[1]);
			basetrack.isg = stoi(str_v[2]);
			basetrack.ax = stod(str_v[3]);
			basetrack.ay = stod(str_v[4]);
			basetrack.x = stod(str_v[5]);
			basetrack.y = stod(str_v[6]);

			basetrack.m[0].ph = stoi(str_v[7]);
			basetrack.m[0].ax = stod(str_v[8]);
			basetrack.m[0].ay = stod(str_v[9]);
			basetrack.m[0].z = stod(str_v[12]);

			basetrack.m[0].pos = stoi(str_v[13]);
			basetrack.m[0].col = stoi(str_v[14]);
			basetrack.m[0].row = stoi(str_v[15]);
			basetrack.m[0].zone = stoi(str_v[16]);
			basetrack.m[0].isg = stoi(str_v[17]);
			basetrack.m[0].rawid = stoi(str_v[18]);

			basetrack.m[1].ph = stoi(str_v[19]);
			basetrack.m[1].ax = stod(str_v[20]);
			basetrack.m[1].ay = stod(str_v[21]);
			basetrack.m[1].z = stod(str_v[24]);

			basetrack.m[1].pos = stoi(str_v[25]);
			basetrack.m[1].col = stoi(str_v[26]);
			basetrack.m[1].row = stoi(str_v[27]);
			basetrack.m[1].zone = stoi(str_v[28]);
			basetrack.m[1].isg = stoi(str_v[29]);
			basetrack.m[1].rawid = stoi(str_v[30]);

			basetrack.dmy = 0;
			basetrack.zone = basetrack.m[0].zone;

			base.push_back(basetrack);
			if (cnt % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				if (output == 1) {
					std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
				}
			}
			cnt++;

		}
		auto size1 = eofpos - begpos;
		if (output == 1) {
			std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;
		}
		if (cnt == 0) {
			fprintf(stderr, "%s no linklet!\n", filename.c_str());
			exit(1);
		}
		return true;

	}
	void read_basetrack_extension(std::string filename, std::vector<base_track_t> &base, int PL, int zone) {
		base.reserve(10000000);
		std::string extension;
		extension = filename.substr(filename.size() - 3, 3);
		if (extension == "vxx") {
			vxx::BvxxReader br;
			//ReadAll関数はbvxx中のBaseTrackを一度にすべて読みだす。
			//引数にはbvxxへのパス、pl、zoneを渡す。
			//ファイルを開けなかった場合などは空のvectorが返ってくる。
			std::vector<vxx::base_track_t> res = br.ReadAll(filename, PL, zone);
			fprintf(stderr, "num of basetrack %lld\n", res.size());
			for (auto itr = res.begin(); itr != res.end(); itr++) {
				base_track_t tmp;
				tmp.ax = itr->ax;
				tmp.ay = itr->ay;
				tmp.x = itr->x;
				tmp.y = itr->y;
				tmp.z = itr->z;
				tmp.zone = itr->zone;
				tmp.rawid = itr->rawid;
				tmp.dmy = itr->dmy;
				tmp.isg = itr->isg;
				tmp.pl = itr->pl;
				for (int i = 0; i < 2; i++) {
					tmp.m[i].ax = itr->m[i].ax;
					tmp.m[i].ay = itr->m[i].ay;
					tmp.m[i].z = itr->m[i].z;
					tmp.m[i].col = itr->m[i].col;
					tmp.m[i].isg = itr->m[i].isg;
					tmp.m[i].row = itr->m[i].row;
					tmp.m[i].ph = itr->m[i].ph;
					tmp.m[i].pos = itr->m[i].pos;
					tmp.m[i].rawid = itr->m[i].rawid;
					tmp.m[i].zone = itr->m[i].zone;
				}
				base.push_back(tmp);
			}
		}
		else {
			read_basetrack_txt(filename, base);
		}
	}
	bool read_microtrack_txt(std::string filename, std::vector<micro_track_t> &micro, int output) {

		std::ifstream ifs(filename);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (output == 1) {
			if (GB > 0) {
				std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
			}
			else {
				std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
			}
		}

		std::string str;
		std::vector<std::string> str_v;
		std::string buffer;
		int cnt = 0;
		while (std::getline(ifs, str))
		{
			str_v.clear();
			str_v = StringSplit(str);
			std::stringstream ss(str);
			micro_track_t microtrack;
			microtrack.pos = stoi(str_v[0]);
			microtrack.zone = stoi(str_v[1]);
			microtrack.rawid = stoi(str_v[2]);
			microtrack.isg = stoi(str_v[3]);
			microtrack.ph = stoi(str_v[4]);
			microtrack.ax = stod(str_v[5]);
			microtrack.ay = stod(str_v[6]);
			microtrack.x = stod(str_v[7]);
			microtrack.y = stod(str_v[8]);
			microtrack.z = stod(str_v[9]);
			microtrack.z1 = stod(str_v[10]);
			microtrack.z2 = stod(str_v[11]);
			microtrack.px = stof(str_v[12]);
			microtrack.py = stof(str_v[13]);
			microtrack.col = stoi(str_v[14]);
			microtrack.row = stoi(str_v[15]);


			micro.push_back(microtrack);
			if (cnt % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				if (output == 1) {
					std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
				}
			}
			cnt++;

		}
		auto size1 = eofpos - begpos;
		if (output == 1) {
			std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;
		}
		if (cnt == 0) {
			fprintf(stderr, "%s no linklet!\n", filename.c_str());
			exit(1);
		}
		return true;

	}
	void read_microtrack_extension(std::string filename, std::vector<micro_track_t> &micro, int pos, int zone) {
		micro.reserve(10000000);
		std::string extension;
		extension = filename.substr(filename.size() - 3, 3);
		if (extension == "vxx") {
			vxx::FvxxReader br;
			//ReadAll関数はbvxx中のBaseTrackを一度にすべて読みだす。
			//引数にはbvxxへのパス、pl、zoneを渡す。
			//ファイルを開けなかった場合などは空のvectorが返ってくる。
			std::vector<vxx::micro_track_t> res = br.ReadAll(filename, pos, zone);
			fprintf(stderr, "num of basetrack %lld\n", res.size());
			for (auto itr = res.begin(); itr != res.end(); itr++) {
				//pos zone rawid isg ph ax ay x y z z1 z2 px py col row f
				micro_track_t tmp;
				tmp.pos = itr->pos;
				tmp.zone = itr->zone;
				tmp.rawid = itr->rawid;
				tmp.isg = itr->isg;
				tmp.ph = itr->ph;
				tmp.ax = itr->ax;
				tmp.ay = itr->ay;
				tmp.x = itr->x;
				tmp.y = itr->y;
				tmp.z = itr->z;
				tmp.z1 = itr->z1;
				tmp.z2 = itr->z2;
				tmp.px = itr->px;
				tmp.py = itr->py;
				tmp.col = itr->col;
				tmp.row = itr->row;
				micro.push_back(tmp);
			}
		}
		else {
			read_microtrack_txt(filename, micro);
		}
	}

	base_track_t base_trans(base_track_t base, corrmap0::Corrmap corr) {
		double x, y;
		x = base.x;
		y = base.y;
		base.x = corr.position[0] * x + corr.position[1] * y + corr.position[4];
		base.y = corr.position[2] * x + corr.position[3] * y + corr.position[5];

		x = base.ax;
		y = base.ay;
		base.ax = corr.angle[0] * x + corr.angle[1] * y + corr.angle[4];
		base.ay = corr.angle[2] * x + corr.angle[3] * y + corr.angle[5];

		for (int i = 0; i < 2; i++) {
			x = base.m[i].ax;
			y = base.m[i].ay;
			base.m[i].ax = corr.angle[0] * x + corr.angle[1] * y + corr.angle[4];
			base.m[i].ay = corr.angle[2] * x + corr.angle[3] * y + corr.angle[5];
		}
		return base;
	}
	base_track_t base_trans_inv(base_track_t base, corrmap0::Corrmap corr) {
		double delta = corr.position[0] * corr.position[3] - corr.position[1] * corr.position[2];
		double x, y;
		x = base.x - corr.position[4];
		y = base.y - corr.position[5];
		base.x = (corr.position[3] * x - corr.position[1] * y) / delta;
		base.y = (corr.position[0] * y - corr.position[2] * x) / delta;

		delta = corr.angle[0] * corr.angle[3] - corr.angle[1] * corr.angle[2];
		x = base.ax - corr.angle[4];
		y = base.ay - corr.angle[5];
		base.ax = (corr.angle[3] * x - corr.angle[1] * y) / delta;
		base.ay = (corr.angle[0] * y - corr.angle[2] * x) / delta;
		for (int i = 0; i < 2; i++) {
			x = base.m[i].ax - corr.angle[4];
			y = base.m[i].ay - corr.angle[5];
			base.m[i].ax = (corr.angle[3] * x - corr.angle[1] * y) / delta;
			base.m[i].ay = (corr.angle[0] * y - corr.angle[2] * x) / delta;
		}

		return base;
	}
	void write_linklet_txt(std::string filename, std::vector<linklet_t> l) {
		std::ofstream ofs(filename);
		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (l.size() == 0) {
			fprintf(stderr, "target linklet ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		else {
			int count = 0;
			std::cout << std::right << std::fixed;
			for (auto itr = l.begin(); itr != l.end(); itr++) {
				if (count % 10000 == 0) {
					fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)", count, int(l.size()), count*100. / l.size());
				}
				count++;
				ofs << std::right << std::fixed
					<< std::setw(6) << itr->pos[0] << " "
					<< std::setw(15) << itr->b[0].rawid << " "
					<< std::setw(6) << itr->pos[1] << " "
					<< std::setw(15) << itr->b[1].rawid
					<< std::setw(8) << itr->b[0].m[0].ph + itr->b[0].m[1].ph << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[0].ax << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[0].ay << " "
					<< std::setw(10) << std::setprecision(1) << itr->b[0].x << " "
					<< std::setw(10) << std::setprecision(1) << itr->b[0].y << " "
					<< std::setw(8) << itr->b[1].m[0].ph + itr->b[1].m[1].ph << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[1].ax << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[1].ay << " "
					<< std::setw(10) << std::setprecision(1) << itr->b[1].x << " "
					<< std::setw(10) << std::setprecision(1) << itr->b[1].y << " "
					<< std::setw(11) << std::setprecision(1) << itr->b[0].z << " "
					<< std::setw(11) << std::setprecision(1) << itr->b[1].z << " "
					<< std::setw(11) << std::setprecision(1) << itr->zproj << " "
					<< std::setw(10) << std::setprecision(1) << itr->xc << " "
					<< std::setw(10) << std::setprecision(1) << itr->yc << " "
					<< std::setw(6) << itr->b[0].m[0].pos << " "
					<< std::setw(8) << itr->b[0].m[0].col << " "
					<< std::setw(10) << itr->b[0].m[0].row << " "
					<< std::setw(3) << itr->b[0].m[0].zone << " "
					<< std::setw(7) << itr->b[0].m[0].isg << " "
					<< std::setw(6) << itr->b[0].m[1].pos << " "
					<< std::setw(8) << itr->b[0].m[1].col << " "
					<< std::setw(10) << itr->b[0].m[1].row << " "
					<< std::setw(3) << itr->b[0].m[1].zone << " "
					<< std::setw(7) << itr->b[0].m[1].isg << " "
					<< std::setw(6) << itr->b[1].m[0].pos << " "
					<< std::setw(8) << itr->b[1].m[0].col << " "
					<< std::setw(10) << itr->b[1].m[0].row << " "
					<< std::setw(3) << itr->b[1].m[0].zone << " "
					<< std::setw(7) << itr->b[1].m[0].isg << " "
					<< std::setw(6) << itr->b[1].m[1].pos << " "
					<< std::setw(8) << itr->b[1].m[1].col << " "
					<< std::setw(10) << itr->b[1].m[1].row << " "
					<< std::setw(3) << itr->b[1].m[1].zone << " "
					<< std::setw(7) << itr->b[1].m[1].isg << " "
					<< "0" << " " << "0.0" << " "
					<< std::setw(15) << itr->b[0].m[0].rawid << " "
					<< std::setw(8) << itr->b[0].m[0].ph << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[0].m[0].ax << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[0].m[0].ay << " "
					<< std::setw(15) << itr->b[0].m[1].rawid << " "
					<< std::setw(8) << itr->b[0].m[1].ph << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[0].m[1].ax << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[0].m[1].ay << " "
					<< std::setw(15) << itr->b[1].m[0].rawid << " "
					<< std::setw(8) << itr->b[1].m[0].ph << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[1].m[0].ax << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[1].m[0].ay << " "
					<< std::setw(15) << itr->b[1].m[1].rawid << " "
					<< std::setw(8) << itr->b[1].m[1].ph << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[1].m[1].ax << " "
					<< std::setw(8) << std::setprecision(4) << itr->b[1].m[1].ay << " "
					<< std::setw(10) << std::setprecision(1) << itr->dx << " "
					<< std::setw(10) << std::setprecision(1) << itr->dy << std::endl;
			}
			fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)\n", count, int(l.size()), count*100. / l.size());
		}
		ofs.close();

	}
	void write_linklet_bin(std::string filename, std::vector<linklet_t> l) {
		std::ofstream ofs(filename, std::ios::binary);
		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (l.size() == 0) {
			fprintf(stderr, "target linklet ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		int64_t count = 0;
		int64_t max = l.size();
		for (int i = 0; i < l.size(); i++) {
			if (count % 10000 == 0) {
				std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
			}
			count++;
			ofs.write((char*)& l[i], sizeof(linklet_t));
		}
		std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
		ofs.close();
	}
	void write_basetrack_txt(std::string filename, std::vector<base_track_t> &base) {
		std::ofstream ofs(filename);
		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (base.size() == 0) {
			fprintf(stderr, "target linklet ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		else {
			int count = 0;
			std::cout << std::right << std::fixed;
			for (auto itr = base.begin(); itr != base.end(); itr++) {
				if (count % 10000 == 0) {
					fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)", count, int(base.size()), count*100. / base.size());
				}
				count++;
				ofs << std::right << std::fixed
					<< std::setw(10) << std::setprecision(0) << itr->rawid << " "
					<< std::setw(4) << std::setprecision(0) << itr->pl << " "
					<< std::setw(4) << std::setprecision(0) << itr->isg << " "
					<< std::setw(7) << std::setprecision(4) << itr->ax << " "
					<< std::setw(7) << std::setprecision(4) << itr->ay << " "
					<< std::setw(8) << std::setprecision(1) << itr->x << " "
					<< std::setw(8) << std::setprecision(1) << itr->y << " "

					<< std::setw(7) << std::setprecision(0) << itr->m[0].ph << " "
					<< std::setw(7) << std::setprecision(4) << itr->m[0].ax << " "
					<< std::setw(7) << std::setprecision(4) << itr->m[0].ay << " "
					<< std::setw(8) << std::setprecision(1) << itr->x << " "
					<< std::setw(8) << std::setprecision(1) << itr->y << " "
					<< std::setw(6) << std::setprecision(1) << itr->m[0].z << " "

					<< std::setw(4) << std::setprecision(0) << itr->m[0].pos << " "
					<< std::setw(3) << std::setprecision(0) << itr->m[0].col << " "
					<< std::setw(3) << std::setprecision(0) << itr->m[0].row << " "
					<< std::setw(3) << std::setprecision(0) << itr->m[0].zone << " "
					<< std::setw(4) << std::setprecision(0) << itr->m[0].isg << " "
					<< std::setw(12) << std::setprecision(0) << itr->m[0].rawid << " "

					<< std::setw(7) << std::setprecision(0) << itr->m[1].ph << " "
					<< std::setw(7) << std::setprecision(4) << itr->m[1].ax << " "
					<< std::setw(7) << std::setprecision(4) << itr->m[1].ay << " "
					<< std::setw(8) << std::setprecision(1) << itr->x + (itr->m[1].z - itr->m[0].z)*itr->ax << " "
					<< std::setw(8) << std::setprecision(1) << itr->y + (itr->m[1].z - itr->m[0].z)*itr->ay << " "
					<< std::setw(6) << std::setprecision(1) << itr->m[1].z << " "

					<< std::setw(4) << std::setprecision(0) << itr->m[1].pos << " "
					<< std::setw(3) << std::setprecision(0) << itr->m[1].col << " "
					<< std::setw(3) << std::setprecision(0) << itr->m[1].row << " "
					<< std::setw(3) << std::setprecision(0) << itr->m[1].zone << " "
					<< std::setw(4) << std::setprecision(0) << itr->m[1].isg << " "
					<< std::setw(12) << std::setprecision(0) << itr->m[1].rawid << std::endl;
			}
			fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)\n", count, int(base.size()), count*100. / base.size());
		}
	}
	void write_basetrack_vxx(std::string filename, std::vector<base_track_t> &base, int PL, int zone) {
		std::vector<vxx::base_track_t> t;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			vxx::base_track_t tmp;
			tmp.ax = itr->ax;
			tmp.ay = itr->ay;
			tmp.x = itr->x;
			tmp.y = itr->y;
			tmp.z = itr->z;
			tmp.zone = itr->zone;
			tmp.rawid = itr->rawid;
			tmp.dmy = itr->dmy;
			tmp.isg = itr->isg;
			tmp.pl = itr->pl;
			for (int i = 0; i < 2; i++) {
				tmp.m[i].ax = itr->m[i].ax;
				tmp.m[i].ay = itr->m[i].ay;
				tmp.m[i].z = itr->m[i].z;
				tmp.m[i].col = itr->m[i].col;
				tmp.m[i].isg = itr->m[i].isg;
				tmp.m[i].row = itr->m[i].row;
				tmp.m[i].ph = itr->m[i].ph;
				tmp.m[i].pos = itr->m[i].pos;
				tmp.m[i].rawid = itr->m[i].rawid;
				tmp.m[i].zone = itr->m[i].zone;
			}
			t.push_back(tmp);
		}

		vxx::BvxxWriter w;
		w.Write(filename, PL, zone, t);

	}
	void write_basetrack_bin(std::string filename, std::vector<base_track_t> &base) {
		std::ofstream ofs(filename, std::ios::binary);
		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (base.size() == 0) {
			fprintf(stderr, "target linklet ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		else {
			int64_t count = 0;
			int64_t max = base.size();

			for (int i = 0; i < max; i++) {
				if (count % 10000 == 0) {
					fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)", count, int(base.size()), count*100. / base.size());
				}
				count++;
				ofs.write((char*)& base[i], sizeof(base_track_t));
			}
			fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)\n", count, int(base.size()), count*100. / base.size());
		}
	}
	void write_microtrack_txt(std::string filename, std::vector<micro_track_t> &micro) {
		std::ofstream ofs(filename);
		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (micro.size() == 0) {
			fprintf(stderr, "target linklet ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		else {
			int count = 0;
			std::cout << std::right << std::fixed;
			for (auto itr = micro.begin(); itr != micro.end(); itr++) {
				if (count % 10000 == 0) {
					fprintf(stderr, "\r Write Microtrack ... %d/%d (%4.1lf%%)", count, int(micro.size()), count*100. / micro.size());
				}
				count++;
				//pos zone rawid isg ph ax ay x y z z1 z2 px py col row f

				ofs << std::right << std::fixed
					<< std::setw(5) << std::setprecision(0) << itr->pos << " "
					<< std::setw(3) << std::setprecision(0) << itr->zone << " "
					<< std::setw(12) << std::setprecision(0) << itr->rawid << " "
					<< std::setw(12) << std::setprecision(0) << itr->isg << " "
					<< std::setw(8) << std::setprecision(0) << itr->ph << " "
					<< std::setw(7) << std::setprecision(4) << itr->ax << " "
					<< std::setw(7) << std::setprecision(4) << itr->ay << " "
					<< std::setw(8) << std::setprecision(1) << itr->x << " "
					<< std::setw(8) << std::setprecision(1) << itr->y << " "
					<< std::setw(8) << std::setprecision(1) << itr->z << " "
					<< std::setw(8) << std::setprecision(1) << itr->z1 << " "
					<< std::setw(8) << std::setprecision(1) << itr->z2 << " "
					<< std::setw(8) << std::setprecision(4) << itr->px << " "
					<< std::setw(8) << std::setprecision(4) << itr->py << " "
					<< std::setw(8) << std::setprecision(0) << itr->col << " "
					<< std::setw(8) << std::setprecision(0) << itr->row << std::endl;
			}
			fprintf(stderr, "\r Write Microtrack ... %d/%d (%4.1lf%%)\n", count, int(micro.size()), count*100. / micro.size());
		}
	}
	void write_microtrack_vxx(std::string filename, std::vector<micro_track_t> &micro, int pos, int zone) {
		std::vector<vxx::micro_track_t> t;
		for (auto itr = micro.begin(); itr != micro.end(); itr++) {
			vxx::micro_track_t tmp;
			tmp.ax = itr->ax;
			tmp.ay = itr->ay;
			tmp.x = itr->x;
			tmp.y = itr->y;
			tmp.z = itr->z;
			tmp.zone = itr->zone;
			tmp.rawid = itr->rawid;
			tmp.col = itr->col;
			tmp.isg = itr->isg;
			tmp.ph = itr->ph;
			tmp.pos = itr->pos;
			tmp.px = itr->px;
			tmp.py = itr->py;
			tmp.z1 = itr->z1;
			tmp.z2 = itr->z2;
			t.push_back(tmp);
		}

		vxx::FvxxWriter w;
		w.Write(filename, pos, zone, t);

	}
	void write_microtrack_bin(std::string filename, std::vector<micro_track_t> &micro) {
		std::ofstream ofs(filename, std::ios::binary);
		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (micro.size() == 0) {
			fprintf(stderr, "target microtrack ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		else {
			int64_t count = 0;
			int64_t max = micro.size();

			for (int i = 0; i < max; i++) {
				if (count % 10000 == 0) {
					fprintf(stderr, "\r Write Microtrack ... %d/%d (%4.1lf%%)", count, int(micro.size()), count*100. / micro.size());
				}
				count++;
				ofs.write((char*)& micro[i], sizeof(micro_track_t));
			}
			fprintf(stderr, "\r Write Microtrack ... %d/%d (%4.1lf%%)\n", count, int(micro.size()), count*100. / micro.size());
		}
	}



}

namespace vtx {
	void vtx::read_vtxfile(std::string filepath, std::vector<VTX> &vtx) {
		std::ifstream ifs(filepath);

		std::string str;
		std::vector<std::string> str_v;

		int count = 0, flg = 0;
		VTX tmp;

		while (std::getline(ifs, str)) {
			if (count % 100 == 0) {
				fprintf(stderr, "\r vtx file reading... %d", count);
			}
			str_v = StringSplit(str);
			//vtx point informatino
			if (str_v.size() == 6) {
				if (flg != 0) {
					vtx.push_back(tmp);
				}
				tmp.base.clear();
				tmp.mindis.clear();

				tmp.eventid = stoi(str_v[0]);
				tmp.pl = stoi(str_v[1]);
				tmp.nbase = stoi(str_v[2]);
				tmp.x = stod(str_v[3]);
				tmp.y = stod(str_v[4]);
				tmp.z = stod(str_v[5]);
				flg = 1;
				count++;

			}
			//vtx minimum distance information
			else if (str_v.size() == 8) {
				VTX_mindis mindis_tmp;
				mindis_tmp.rawid[0] = stoi(str_v[0]);
				mindis_tmp.rawid[1] = stoi(str_v[1]);
				mindis_tmp.openang = stod(str_v[2]);
				mindis_tmp.mindis = stod(str_v[3]);
				mindis_tmp.x = stod(str_v[4]);
				mindis_tmp.y = stod(str_v[5]);
				mindis_tmp.z = stod(str_v[6]);
				mindis_tmp.flg = stoi(str_v[7]);

				tmp.mindis.push_back(mindis_tmp);
			}
			//vtx basetrack information
			else if (str_v.size() == 11) {
				VTX_base base_tmp;
				base_tmp.pl = stoi(str_v[0]);
				base_tmp.rawid = stoi(str_v[1]);
				base_tmp.ph = stoi(str_v[2]);
				base_tmp.ax = stod(str_v[3]);
				base_tmp.ay = stod(str_v[4]);
				base_tmp.x = stod(str_v[5]);
				base_tmp.y = stod(str_v[6]);
				base_tmp.chainid = stoi(str_v[7]);
				base_tmp.startPL = stoi(str_v[8]);
				base_tmp.nseg = stoi(str_v[9]);
				base_tmp.ip = stod(str_v[10]);

				tmp.base.push_back(base_tmp);
			}
			else {
				fprintf(stderr, "vtx file format is incorrect\n");
				fprintf(stderr, "str isze = %d\n", int(str_v.size()));

			}
		}
		if (flg != 0) {
			vtx.push_back(tmp);
		}
		fprintf(stderr, "\r vtx file reading... %d fin\n", count);
	}
	void vtx::write_vtxfile(std::string filepath, std::vector<VTX> vtx) {

		std::ofstream ofs(filepath);
		if (!ofs) {
			//file open s
			fprintf(stderr, "File[%s] is not exist!!\n", filepath.c_str());
			throw std::exception();
		}
		for (auto itr_1 = vtx.begin(); itr_1 != vtx.end(); itr_1++) {
			ofs << std::left << std::fixed
				<< std::setw(8) << std::setprecision(4) << itr_1->eventid << " "
				<< std::setw(8) << std::setprecision(4) << itr_1->pl << " "
				<< std::setw(8) << std::setprecision(4) << itr_1->nbase << " "
				<< std::setw(8) << std::setprecision(1) << itr_1->x << " "
				<< std::setw(8) << std::setprecision(1) << itr_1->y << " "
				<< std::setw(8) << std::setprecision(1) << itr_1->z << std::endl;
			for (auto itr_2 = itr_1->mindis.begin(); itr_2 != itr_1->mindis.end(); itr_2++) {
				ofs << std::right << std::fixed
					<< std::setw(10) << std::setprecision(0) << itr_2->rawid[0] << " "
					<< std::setw(10) << std::setprecision(0) << itr_2->rawid[1] << " "
					<< std::setw(7) << std::setprecision(4) << itr_2->openang << " "
					<< std::setw(5) << std::setprecision(1) << itr_2->mindis << " "
					<< std::setw(8) << std::setprecision(1) << itr_2->x << " "
					<< std::setw(8) << std::setprecision(1) << itr_2->y << " "
					<< std::setw(8) << std::setprecision(1) << itr_2->z << " "
					<< std::setw(3) << std::setprecision(0) << itr_2->flg << std::endl;
			}
			for (auto itr_2 = itr_1->base.begin(); itr_2 != itr_1->base.end(); itr_2++) {
				ofs << std::right << std::fixed
					<< std::setw(4) << std::setprecision(0) << itr_2->pl << " "
					<< std::setw(10) << std::setprecision(0) << itr_2->rawid << " "
					<< std::setw(8) << std::setprecision(0) << itr_2->ph << " "
					<< std::setw(7) << std::setprecision(4) << itr_2->ax << " "
					<< std::setw(7) << std::setprecision(4) << itr_2->ay << " "
					<< std::setw(8) << std::setprecision(1) << itr_2->x << " "
					<< std::setw(8) << std::setprecision(1) << itr_2->y << " "
					<< std::setw(10) << std::setprecision(0) << itr_2->chainid << " "
					<< std::setw(4) << std::setprecision(0) << itr_2->startPL << " "
					<< std::setw(4) << std::setprecision(0) << itr_2->nseg << " "
					<< std::setw(5) << std::setprecision(1) << itr_2->ip << std::endl;
			}
		}
	}
}

namespace chamber0 {
	chamber0::chamber_structure::chamber_structure() {

	}
	chamber0::chamber_structure::chamber_structure(std::string name) {
		if (name == "run8b") {
			chamber_one_layer one;
			one.iron = false;
			one.water = false;
			one.z = -280;
			chamber.insert(std::make_pair(1, one));
			one.z = -630;
			chamber.insert(std::make_pair(2, one));
			one.z = -2980;
			chamber.insert(std::make_pair(3, one));
			one.z = -3330;
			chamber.insert(std::make_pair(4, one));
			one.z = -3912;
			one.iron = true;
			chamber.insert(std::make_pair(5, one));
			for (int pl = 6; pl <= 10; pl++) {
				one.z -= 850;
				if (pl == 10) {
					one.iron = false;
				}
				chamber.insert(std::make_pair(pl, one));
			}
			one.z = -8984;
			one.iron = true;
			chamber.insert(std::make_pair(11, one));
			for (int pl = 12; pl <= 126; pl++) {
				if (pl % 2 == 0) {
					one.z -= 850;
					one.water = true;
					one.iron = false;
				}
				else {
					one.z -= 2582;
					one.water = false;
					one.iron = true;
				}
				chamber.insert(std::make_pair(pl, one));
			}
		}
		else if (name == "run8a") {

			chamber_one_layer one;
			one.iron = false;
			one.water = false;
			one.z = -250;
			chamber.insert(std::make_pair(1, one));
			one.z = -570;
			chamber.insert(std::make_pair(2, one));
			one.z = -2890;
			chamber.insert(std::make_pair(3, one));
			one.z = -3210;
			chamber.insert(std::make_pair(4, one));
			one.z = -3762;
			one.iron = true;
			chamber.insert(std::make_pair(5, one));
			for (int pl = 6; pl <= 10; pl++) {
				one.z -= 820;
				if (pl == 10) {
					one.iron = false;
				}
				chamber.insert(std::make_pair(pl, one));
			}
			one.z = -8654;
			one.iron = true;
			chamber.insert(std::make_pair(11, one));
			for (int pl = 12; pl <= 128; pl++) {
				if (pl % 2 == 0) {
					one.z -= 820;
					one.water = true;
					one.iron = false;
				}
				else {
					one.z -= 2552;
					one.water = false;
					one.iron = true;
				}
				chamber.insert(std::make_pair(pl, one));
			}
		}
		else if (name == "PRA") {

			chamber_one_layer one;
			one.iron = false;
			one.water = false;
			one.z = -250;
			chamber.insert(std::make_pair(1, one));
			one.z = -570;
			chamber.insert(std::make_pair(2, one));
			one.z = -2890;
			chamber.insert(std::make_pair(3, one));
			one.z = -3210;
			chamber.insert(std::make_pair(4, one));
			for (int pl = 5; pl <= 15; pl++) {
				one.z -= 820;
				if (pl == 10) {
					one.iron = false;
				}
				chamber.insert(std::make_pair(pl, one));
			}
			one.z = -8654;
			one.iron = true;
			chamber.insert(std::make_pair(11, one));
			for (int pl = 12; pl <= 128; pl++) {
				if (pl % 2 == 0) {
					one.z -= 820;
					one.water = true;
					one.iron = false;
				}
				else {
					one.z -= 2552;
					one.water = false;
					one.iron = true;
				}
				chamber.insert(std::make_pair(pl, one));
			}
		}
		else {

		}
	}
	void chamber0::chamber_structure::disp_z_val() {
		for (auto itr = chamber.begin(); itr != chamber.end(); itr++) {
			printf("%d %10.1lf iron:%d water:%d\n", itr->first, itr->second.z, itr->second.iron, itr->second.water);
		}
	}
	double chamber0::chamber_structure::dz(int pl1, int pl2) {
		return chamber[pl1].z - chamber[pl2].z;
	}
	int chamber0::chamber_structure::number_of_iron(int pl1, int pl2) {
		int pl_min = std::min(pl1, pl2);
		int pl_max = std::max(pl1, pl2);
		int num = 0;
		for (int pl = pl_min; pl < pl_max; pl++) {
			if (chamber[pl].iron)num++;
		}
		return num;
	}
	int chamber0::chamber_structure::number_of_water(int pl1, int pl2) {
		int pl_min = std::min(pl1, pl2);
		int pl_max = std::max(pl1, pl2);
		int num = 0;
		for (int pl = pl_min; pl < pl_max; pl++) {
			if (chamber[pl].water)num++;
		}
		return num;
	}
	double chamber0::chamber_structure::z_nominal(int pl) {
		return chamber[pl].z;
	}

}
namespace chamber1 {
	void chamber1::read_structure(std::string filename, chamber1::Chamber &chamber) {
		std::ifstream ifs(filename);
		std::string str;

		std::vector < std::vector<std::string>> all_data;
		while (std::getline(ifs, str)) {
			auto str_v = StringSplit(str);
			all_data.push_back(str_v);
		}

		int pos;
		double z;
		for (int i = 0; i < all_data.size();i++){

			if (all_data[i].size() == 2) {

				pos = std::stoi(all_data[i][0]);
				z = std::stod(all_data[i][1]);
				if (pos % 10 == 1) {
					z += std::stod(all_data[i + 1][0]);
				}
				else if (pos % 10 == 2) {
					z -= std::stod(all_data[i - 1][0]);
				}
				else {
					fprintf(stderr, "structure file format error\n");
					exit(1);
				}
				chamber.z.insert(std::make_pair(pos, z));
			}
			else if (all_data[i].size() == 4) {
				One_layer one;
				one.material = all_data[i][3];
				one.radiation_length = std::stod(all_data[i][1]);
				one.thick = std::stod(all_data[i][0]);
				chamber.layer.push_back(one);
			}
			else if (all_data[i].size() == 5) {
				One_layer one;
				one.material = all_data[i][4];
				one.radiation_length = std::stod(all_data[i][1]);
				one.thick = std::stod(all_data[i][0]);
				chamber.layer.push_back(one);
			}
			else {
				fprintf(stderr, "structure file format error\n");
				for (int j = 0; j < all_data[i].size(); j++) {
					fprintf(stderr, "%s \n", all_data[i][j].c_str());
				}
				fprintf(stderr, "\n");

				exit(1);

			}
		}
	}
	std::map<int, double> base_z_convert(chamber1::Chamber &chamber) {
		std::map<int, double> ret;
		int pl;
		for (auto itr = chamber.z.begin(); itr != chamber.z.end(); itr++) {
			if (itr->first % 10 == 1) {
				pl = itr->first / 10;
				ret.insert(std::make_pair(pl, itr->second));
			}
		}
		return ret;
	}
}

namespace corrmap0 {
	bool sort_corrid(const Corrmap& left, const Corrmap& right) {
		return left.id < right.id;
	}
	bool sort_corrx(const Corrmap& left, const Corrmap& right) {
		return left.areax[0] == right.areax[0] ? left.areay[0] < right.areay[0] : left.areax[0] < right.areax[0];
	}
	void read_cormap(std::string file, std::vector<struct Corrmap> &cor,int output) {
		std::ifstream ifs(file);
		struct Corrmap buffer;

		while (ifs >> buffer.id >> buffer.pos[0] >> buffer.pos[1] >> buffer.areax[0] >> buffer.areax[1] >> buffer.areay[0] >> buffer.areay[1] >>
			buffer.position[0] >> buffer.position[1] >> buffer.position[2] >> buffer.position[3] >> buffer.position[4] >> buffer.position[5] >>
			buffer.angle[0] >> buffer.angle[1] >> buffer.angle[2] >> buffer.angle[3] >> buffer.angle[4] >> buffer.angle[5] >>
			buffer.dz >> buffer.signal >> buffer.background >> buffer.SN >> buffer.rms_pos[0] >> buffer.rms_pos[1] >>
			buffer.notuse_d[0] >> buffer.notuse_d[1] >> buffer.rms_angle[0] >> buffer.rms_angle[1] >>buffer.notuse_i[0] >> 
			//これがix,iy
			buffer.notuse_i[1] >> buffer.notuse_i[2] >> 
			buffer.notuse_i[3] >> buffer.notuse_i[4] >> buffer.notuse_i[5] >>buffer.notuse_i[6] >> buffer.notuse_i[7] >> buffer.notuse_i[8] >>
			buffer.notuse_d[2] >> buffer.notuse_d[3] >> buffer.notuse_d[4]) {
			struct Corrmap *cormap = new Corrmap();
			cormap = &buffer;
			cor.push_back(*cormap);
		}
		if (output == 1) {
			fprintf(stderr, "%s input finish\n", file.c_str());
		}
		if (cor.size() == 0) {
			fprintf(stderr, "%s alignment miss!\n", file.c_str());
			exit(1);
		}


	}
	void read_cormap(std::string file, Corrmap &cor) {
		std::ifstream ifs(file);
		struct Corrmap buffer;
		int flg = 0;
		while (ifs >> buffer.id >> buffer.pos[0] >> buffer.pos[1] >> buffer.areax[0] >> buffer.areax[1] >> buffer.areay[0] >> buffer.areay[1] >>
			buffer.position[0] >> buffer.position[1] >> buffer.position[2] >> buffer.position[3] >> buffer.position[4] >> buffer.position[5] >>
			buffer.angle[0] >> buffer.angle[1] >> buffer.angle[2] >> buffer.angle[3] >> buffer.angle[4] >> buffer.angle[5] >>
			buffer.dz >> buffer.signal >> buffer.background >> buffer.SN >> buffer.rms_pos[0] >> buffer.rms_pos[1] >>
			buffer.notuse_d[0] >> buffer.notuse_d[1] >> buffer.rms_angle[0] >> buffer.rms_angle[1] >>
			buffer.notuse_i[0] >> buffer.notuse_i[1] >> buffer.notuse_i[2] >> buffer.notuse_i[3] >> buffer.notuse_i[4] >> buffer.notuse_i[5] >>
			buffer.notuse_i[6] >> buffer.notuse_i[7] >> buffer.notuse_i[8] >>
			buffer.notuse_d[2] >> buffer.notuse_d[3] >> buffer.notuse_d[4]) {
			struct Corrmap *cormap = new Corrmap();
			cormap = &buffer;
			cor = *cormap;
			flg++;
		}
		if (flg == 0) {
			fprintf(stderr, "%s alignment miss!\n", file.c_str());
			exit(1);
		}


	}
	void write_corrmap(std::string filename, std::vector<struct Corrmap> cor) {
		std::ofstream ofs(filename);
		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (cor.size() == 0) {
			fprintf(stderr, "target corrmap ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		else {
			int count = 0;
			std::cout << std::right << std::fixed;
			for (auto itr = cor.begin(); itr != cor.end(); itr++) {
				if (count % 10000 == 0) {
					fprintf(stderr, "\r Write Corrmap ... %d/%d (%4.1lf%%)", count, int(cor.size()), count*100. / cor.size());
				}
				count++;
				ofs << std::right << std::fixed
					<< std::setw(8) << std::setprecision(0) << itr->id

					<< std::setw(5) << std::setprecision(0) << itr->pos[0]
					<< std::setw(5) << std::setprecision(0) << itr->pos[1]

					<< std::setw(12) << std::setprecision(1) << itr->areax[0] << " "
					<< std::setw(12) << std::setprecision(1) << itr->areax[1] << " "
					<< std::setw(12) << std::setprecision(1) << itr->areay[0] << " "
					<< std::setw(12) << std::setprecision(1) << itr->areay[1] << " "
					<< std::setw(10) << std::setprecision(6) << itr->position[0] << " "
					<< std::setw(10) << std::setprecision(6) << itr->position[1] << " "
					<< std::setw(10) << std::setprecision(6) << itr->position[2] << " "
					<< std::setw(10) << std::setprecision(6) << itr->position[3] << " "
					<< std::setw(10) << std::setprecision(1) << itr->position[4] << " "
					<< std::setw(10) << std::setprecision(1) << itr->position[5] << " "
					<< std::setw(10) << std::setprecision(6) << itr->angle[0] << " "
					<< std::setw(10) << std::setprecision(6) << itr->angle[1] << " "
					<< std::setw(10) << std::setprecision(6) << itr->angle[2] << " "
					<< std::setw(10) << std::setprecision(6) << itr->angle[3] << " "
					<< std::setw(10) << std::setprecision(6) << itr->angle[4] << " "
					<< std::setw(10) << std::setprecision(6) << itr->angle[5] << " "

					<< std::setw(8) << std::setprecision(1) << itr->dz << " "
					<< std::setw(8) << std::setprecision(1) << itr->signal << " "
					<< std::setw(6) << std::setprecision(1) << itr->background << " "
					<< std::setw(6) << std::setprecision(1) << itr->SN << " "
					<< std::setw(4) << std::setprecision(1) << itr->rms_pos[0] << " "
					<< std::setw(4) << std::setprecision(1) << itr->rms_pos[1] << " "
					<< std::setw(5) << std::setprecision(4) << itr->notuse_d[0] << " "
					<< std::setw(5) << std::setprecision(4) << itr->notuse_d[1] << " "
					<< std::setw(5) << std::setprecision(4) << itr->rms_angle[0] << " "
					<< std::setw(5) << std::setprecision(4) << itr->rms_angle[1] << " "
					<< std::setw(4) << std::setprecision(0) << itr->notuse_i[0] << " "
					<< std::setw(6) << std::setprecision(0) << itr->notuse_i[1] << " "
					<< std::setw(6) << std::setprecision(0) << itr->notuse_i[2] << " "
					<< std::setw(4) << std::setprecision(0) << itr->notuse_i[3] << " "
					<< std::setw(4) << std::setprecision(0) << itr->notuse_i[4] << " "
					<< std::setw(4) << std::setprecision(0) << itr->notuse_i[5] << " "
					<< std::setw(4) << std::setprecision(0) << itr->notuse_i[6] << " "
					<< std::setw(4) << std::setprecision(0) << itr->notuse_i[7] << " "
					<< std::setw(2) << std::setprecision(0) << itr->notuse_i[8] << " "
					<< std::setw(6) << std::setprecision(5) << itr->notuse_d[2] << " "
					<< std::setw(6) << std::setprecision(5) << itr->notuse_d[3] << " "
					<< std::setw(6) << std::setprecision(5) << itr->notuse_d[4] << " "
					<< std::endl;
			}
			fprintf(stderr, "\r Write Corrmap ... %d/%d (%4.1lf%%)\n", count, int(cor.size()), count*100. / cor.size());
		}
		ofs.close();
	}
	void write_corrmap(std::string filename, struct Corrmap cor) {
		std::ofstream ofs(filename);
		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		else {
			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(0) << cor.id

				<< std::setw(5) << std::setprecision(0) << cor.pos[0]
				<< std::setw(5) << std::setprecision(0) << cor.pos[1]

				<< std::setw(12) << std::setprecision(1) << cor.areax[0] << " "
				<< std::setw(12) << std::setprecision(1) << cor.areax[1] << " "
				<< std::setw(12) << std::setprecision(1) << cor.areay[0] << " "
				<< std::setw(12) << std::setprecision(1) << cor.areay[1] << " "
				<< std::setw(10) << std::setprecision(6) << cor.position[0] << " "
				<< std::setw(10) << std::setprecision(6) << cor.position[1] << " "
				<< std::setw(10) << std::setprecision(6) << cor.position[2] << " "
				<< std::setw(10) << std::setprecision(6) << cor.position[3] << " "
				<< std::setw(10) << std::setprecision(1) << cor.position[4] << " "
				<< std::setw(10) << std::setprecision(1) << cor.position[5] << " "
				<< std::setw(10) << std::setprecision(6) << cor.angle[0] << " "
				<< std::setw(10) << std::setprecision(6) << cor.angle[1] << " "
				<< std::setw(10) << std::setprecision(6) << cor.angle[2] << " "
				<< std::setw(10) << std::setprecision(6) << cor.angle[3] << " "
				<< std::setw(10) << std::setprecision(6) << cor.angle[4] << " "
				<< std::setw(10) << std::setprecision(6) << cor.angle[5] << " "

				<< std::setw(8) << std::setprecision(1) << cor.dz << " "
				<< std::setw(8) << std::setprecision(1) << cor.signal << " "
				<< std::setw(6) << std::setprecision(1) << cor.background << " "
				<< std::setw(6) << std::setprecision(1) << cor.SN << " "
				<< std::setw(4) << std::setprecision(1) << cor.rms_pos[0] << " "
				<< std::setw(4) << std::setprecision(1) << cor.rms_pos[1] << " "
				<< std::setw(5) << std::setprecision(4) << cor.notuse_d[0] << " "
				<< std::setw(5) << std::setprecision(4) << cor.notuse_d[1] << " "
				<< std::setw(5) << std::setprecision(4) << cor.rms_angle[0] << " "
				<< std::setw(5) << std::setprecision(4) << cor.rms_angle[1] << " "
				<< std::setw(4) << std::setprecision(0) << cor.notuse_i[0] << " "
				<< std::setw(6) << std::setprecision(0) << cor.notuse_i[1] << " "
				<< std::setw(6) << std::setprecision(0) << cor.notuse_i[2] << " "
				<< std::setw(4) << std::setprecision(0) << cor.notuse_i[3] << " "
				<< std::setw(4) << std::setprecision(0) << cor.notuse_i[4] << " "
				<< std::setw(4) << std::setprecision(0) << cor.notuse_i[5] << " "
				<< std::setw(4) << std::setprecision(0) << cor.notuse_i[6] << " "
				<< std::setw(4) << std::setprecision(0) << cor.notuse_i[7] << " "
				<< std::setw(2) << std::setprecision(0) << cor.notuse_i[8] << " "
				<< std::setw(6) << std::setprecision(5) << cor.notuse_d[2] << " "
				<< std::setw(6) << std::setprecision(5) << cor.notuse_d[3] << " "
				<< std::setw(6) << std::setprecision(5) << cor.notuse_d[4] << " "
				<< std::endl;
		}
		ofs.close();
	}
	void read_cormap_dc(std::string file, std::vector<struct CorrmapDC> &cor, int output) {
		std::ifstream ifs(file);
		struct CorrmapDC buffer;

		while (ifs >> buffer.id >> buffer.pos[0] >> buffer.pos[1] >> buffer.areax[0] >> buffer.areax[1] >> buffer.areay[0] >> buffer.areay[1] >>
			buffer.notuse_d[0] >> buffer.notuse_d[1] >> buffer.notuse_d[2] >> buffer.notuse_d[3] >> buffer.notuse_d[4] >> buffer.notuse_d[5] >>
			buffer.shr >> buffer.ddz >> buffer.notuse_d[6] >> buffer.shr >> buffer.dax >> buffer.day >>
			buffer.dz >> buffer.signal >> buffer.background >> buffer.SN >> buffer.rms_pos[0] >> buffer.rms_pos[1] >>
			buffer.notuse_d[7] >> buffer.notuse_d[8] >> buffer.rms_angle[0] >> buffer.rms_angle[1] >>
			buffer.notuse_i[0] >> buffer.ix >> buffer.iy >> buffer.notuse_i[1] >> buffer.notuse_i[2] >> buffer.notuse_i[3] >>
			buffer.notuse_i[4] >> buffer.notuse_i[5] >> buffer.notuse_i[6] >>
			buffer.notuse_d[9] >> buffer.notuse_d[10] >> buffer.notuse_d[11]) {
			struct CorrmapDC *cormap = new CorrmapDC();
			cormap = &buffer;
			cor.push_back(*cormap);
		}
		if (output == 1) {
			fprintf(stderr, "%s input finish\n", file.c_str());
		}
		if (cor.size() == 0) {
			fprintf(stderr, "%s alignment miss!\n", file.c_str());
			exit(1);
		}





	}
}

namespace corrmap_3d {

	std::vector<align_param> read_ali_param(std::string filename, bool output) {

		std::vector<align_param> ret;
		align_param param_tmp;
		std::ifstream ifs(filename);

		while (ifs >> param_tmp.id >> param_tmp.ix >> param_tmp.iy >> param_tmp.signal
			>> param_tmp.x >> param_tmp.y >> param_tmp.z
			>> param_tmp.x_rot >> param_tmp.y_rot >> param_tmp.z_rot
			>> param_tmp.x_shrink >> param_tmp.y_shrink >> param_tmp.z_shrink
			>> param_tmp.yx_shear >> param_tmp.zx_shear >> param_tmp.zy_shear
			>> param_tmp.dx >> param_tmp.dy >> param_tmp.dz) {
			ret.push_back(param_tmp);
			//printf("ix %d iy%d\n", param_tmp.ix, param_tmp.iy);

		}
		if (output == 1) {
			fprintf(stderr, "%s input finish\n", filename.c_str());
		}
		if (ret.size() == 0) {
			fprintf(stderr, "%s alignment miss!\n", filename.c_str());
			exit(1);
		}
		return ret;

	}
	std::map<int,std::vector<align_param>> read_ali_param_abs(std::string filename, bool output) {

		std::map<int, std::vector<align_param>>ret;
		std::multimap<int, align_param>corrmap_multi;
		align_param param_tmp;
		std::ifstream ifs(filename);
		int pl;
		while (ifs >>pl>> param_tmp.id >> param_tmp.ix >> param_tmp.iy >> param_tmp.signal
			>> param_tmp.x >> param_tmp.y >> param_tmp.z
			>> param_tmp.x_rot >> param_tmp.y_rot >> param_tmp.z_rot
			>> param_tmp.x_shrink >> param_tmp.y_shrink >> param_tmp.z_shrink
			>> param_tmp.yx_shear >> param_tmp.zx_shear >> param_tmp.zy_shear
			>> param_tmp.dx >> param_tmp.dy >> param_tmp.dz) {
			corrmap_multi.insert(std::make_pair(pl, param_tmp));
			//printf("ix %d iy%d\n", param_tmp.ix, param_tmp.iy);

		}
		if (output == 1) {
			fprintf(stderr, "%s input finish\n", filename.c_str());
		}
		if (corrmap_multi.size() == 0) {
			fprintf(stderr, "%s alignment miss!\n", filename.c_str());
			exit(1);
		}
		for (auto itr = corrmap_multi.begin(); itr != corrmap_multi.end(); itr++) {
			int count = corrmap_multi.count(itr->first);
			std::vector<align_param> corr_tmp;
			corr_tmp.reserve(count);
			auto range = corrmap_multi.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				corr_tmp.push_back(res->second);
			}
			ret.insert(std::make_pair(itr->first, corr_tmp));

			itr = std::next(itr, count - 1);
		}
		return ret;
	}
	std::vector <align_param2 >DelaunayDivide(std::vector <align_param >&corr) {

		//delaunay分割
		std::vector<double> x, y;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			x.push_back(itr->x);
			y.push_back(itr->y);
		}

		delaunay::DelaunayTriangulation DT(x, y); // (std::vector<double> x, std::vector<double> y, uint32_t seed_)
		DT.execute(); // (double min_delta = 1e-6, double max_delta = 1e-5, int max_miss_count = 30)
		std::vector<delaunay::Edge> edge = DT.get_edges();

		std::multimap<int, int> edge_map;

		for (auto itr = edge.begin(); itr != edge.end(); itr++) {
			edge_map.insert(std::make_pair(std::min(itr->first, itr->second), std::max(itr->first, itr->second)));

		}
		std::set<std::tuple<int, int, int>>triangle;
		std::set<int> vertex;
		for (auto itr = edge_map.begin(); itr != edge_map.end(); itr++) {
			//itr->firstの点=aを通る三角形の探索
			vertex.clear();
			auto range = edge_map.equal_range(itr->first);
			//aを通りitr->secondの点=bに行く。bのsetを作成
			for (auto res = range.first; res != range.second; res++) {
				vertex.insert(res->second);
			}
			//bを通る線分の探索
			for (auto itr2 = vertex.begin(); itr2 != vertex.end(); itr2++) {
				if (edge_map.count(*itr2) == 0)continue;
				auto range2 = edge_map.equal_range(*itr2);
				//bを通る線分の中からaから始まる線分を探す
				for (auto res = range2.first; res != range2.second; res++) {
					if (vertex.count(res->second) == 1) {
						triangle.insert(std::make_tuple(itr->first, *itr2, res->second));
					}
				}

			}
		}

		std::vector <align_param2 > ret;
		for (auto itr = triangle.begin(); itr != triangle.end(); itr++) {
			//printf("delaunay triangle %d %d %d\n", std::get<0>(*itr), std::get<1>(*itr), std::get<2>(*itr));
			align_param2 param;
			param.corr_p[0] = &(corr[std::get<0>(*itr)]);
			param.corr_p[1] = &(corr[std::get<1>(*itr)]);
			param.corr_p[2] = &(corr[std::get<2>(*itr)]);
			param.x = 0;
			param.y = 0;
			param.z = 0;
			param.z_shrink = 0;
			param.zx_shear = 0;
			param.zy_shear = 0;
			for (int i = 0; i < 3; i++) {
				param.x += param.corr_p[i]->x;
				param.y += param.corr_p[i]->y;
				param.z += param.corr_p[i]->z;
				param.z_shrink += param.corr_p[i]->z_shrink;
				param.zx_shear += param.corr_p[i]->zx_shear;
				param.zy_shear += param.corr_p[i]->zy_shear;
			}
			param.x = param.x / 3;
			param.y = param.y / 3;
			param.z = param.z / 3;
			param.z_shrink = param.z_shrink/3;
			param.zx_shear = param.zx_shear/3;
			param.zy_shear = param.zx_shear/3;

			param.Calc_9param();
			//内角の最小が20度より小さい場合は使わない
			if (!triangle_internal_angle_cut(param, 20))continue;
			ret.push_back(param);
		}

		return ret;

	}
	std::map<int, std::vector<align_param2>> DelaunayDivide_map(std::map<int, std::vector<align_param>>&corr) {
		int all = corr.size(), count = 0;
		std::map<int, std::vector<align_param2>> ret;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			printf("\r DelaunayDivide %d/%d", count, all);
			count++;
			std::vector <align_param2 > dd = DelaunayDivide(itr->second);
			ret.insert(std::make_pair(itr->first, dd));
		}
		printf("\r DelaunayDivide %d/%d\n", count, all);

		return ret;
	}

	void align_param2::Calc_9param() {


		double bp[3][3], ap[3][3], cos_z, sin_z;
		for (int i = 0; i < 3; i++) {
			bp[i][0] = corr_p[i]->x;
			bp[i][1] = corr_p[i]->y;
			bp[i][2] = corr_p[i]->z;



			//ap[i][0] = corr_p[i]->x + corr_p[i]->dx;
			//ap[i][1] = corr_p[i]->y + corr_p[i]->dy;
			//ap[i][2] = corr_p[i]->z + corr_p[i]->dz;
			cos_z = cos(corr_p[i]->z_rot);
			sin_z = sin(corr_p[i]->z_rot);
			ap[i][0] = corr_p[i]->x_shrink*cos_z*(corr_p[i]->x) - corr_p[i]->y_shrink*sin_z*(corr_p[i]->y) + corr_p[i]->dx;
			ap[i][1] = corr_p[i]->x_shrink*sin_z*(corr_p[i]->x) + corr_p[i]->y_shrink*cos_z*(corr_p[i]->y) + corr_p[i]->dy;
			ap[i][2] = corr_p[i]->z + corr_p[i]->dz;
			//printf("bp%d %8.1lf %8.1lf %8.1lf\n",i, bp[i][0], bp[i][1], bp[i][2]);
			//printf("ap%d %8.1lf %8.1lf %8.1lf\n", i,ap[i][0], ap[i][1], ap[i][2]);
		}
		//apの位置ずれvectorを定義
		double dp[2][3];
		for (int i = 0; i < 3; i++) {
			dp[0][i] = ap[1][i] - ap[0][i];
			dp[1][i] = ap[2][i] - ap[0][i];
		}
		//printf("0-->1 x,y,z : %.1lf %.1lf %.1lf\n", dp[0][0], dp[0][1], dp[0][2]);
		//printf("0-->2 x,y,z : %.1lf %.1lf %.1lf\n", dp[1][0], dp[1][1], dp[1][2]);
		//法線vector
		double n_v[3];
		n_v[0] = (dp[0][1] * dp[1][2] - dp[0][2] * dp[1][1]);
		n_v[1] = (dp[0][2] * dp[1][0] - dp[0][0] * dp[1][2]);
		n_v[2] = (dp[0][0] * dp[1][1] - dp[0][1] * dp[1][0]);

		//std::cout << "normal vector" << std::endl;
		//for (int i = 0; i < 3; i++) {
		//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
		//}

		x_rot = atan(n_v[1] / n_v[2]);
		n_v[1] = cos(x_rot)*n_v[1] - sin(x_rot)*n_v[2];
		n_v[2] = sin(x_rot)*n_v[1] + cos(x_rot)*n_v[2];
		//std::cout << "normal vector" << std::endl;
		//for (int i = 0; i < 3; i++) {
		//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
		//}
		y_rot = atan(-1 * n_v[0] / n_v[2]);
		n_v[0] = cos(y_rot)*n_v[0] + sin(y_rot)*n_v[2];
		n_v[2] = -1 * sin(y_rot)*n_v[0] + cos(y_rot)*n_v[2];

		//std::cout << "normal vector" << std::endl;
		//for (int i = 0; i < 3; i++) {
		//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
		//}

		//printf("x rot:%.6lf\n", x_rot);
		//printf("y rot:%.6lf\n", y_rot);


		matrix_3D::matrix_33 x_rot_mat(0, x_rot), y_rot_mat(1, y_rot);
		matrix_3D::vector_3D ap_v[3];
		for (int i = 0; i < 3; i++) {
			ap_v[i].x = ap[i][0];
			ap_v[i].y = ap[i][1];
			ap_v[i].z = ap[i][2];
		}
		for (int i = 0; i < 3; i++) {
			ap_v[i].matrix_multiplication(x_rot_mat);
			ap_v[i].matrix_multiplication(y_rot_mat);
		}
		//for (int i = 0; i < 3; i++) {
		//	printf("point %d\n", i);
		//	printf("\t %.2lf %.2lf %.2lf\n", bp[i][0], bp[i][1], bp[i][2]);
		//	printf("\t %.2lf %.2lf %.2lf\n", ap_v[i].x, ap_v[i].y, ap_v[i].z);
		//}
		dz = (ap_v[0].z - bp[0][2] + ap_v[1].z - bp[1][2] + ap_v[2].z - bp[2][2]) / 3;
		//printf("dz=%.2lf\n", dz);
		//3元方程式を解く
		double a[2][3][3] = { { {bp[0][0],bp[0][1],1},{bp[1][0],bp[1][1],1},{bp[2][0],bp[2][1],1} },  { {bp[0][0],bp[0][1],1},{bp[1][0],bp[1][1],1},{bp[2][0],bp[2][1],1} } };
		double b[2][3] = { { ap_v[0].x,ap_v[1].x,ap_v[2].x },{ ap_v[0].y,ap_v[1].y,ap_v[2].y } };
		double c[2][3] = { {1, 1, 1},{1,1,1} };
		//gauss(a[0], b[0], c[0]);
		//gauss(a[1], b[1], c[1]);
		GaussJorden(a[0], b[0], c[0]);
		GaussJorden(a[1], b[1], c[1]);
		z_rot = atan(c[1][0] / c[0][0]);
		x_shrink = c[0][0] / cos(z_rot);
		y_shrink = (c[0][0] * c[1][1] - c[0][1] * c[1][0]) / (c[0][0] * cos(z_rot) + c[1][0] * sin(z_rot));
		yx_shear = (c[0][1] * cos(z_rot) + c[1][1] * sin(z_rot)) / (c[0][0] * cos(z_rot) + c[1][0] * sin(z_rot));

		dx = c[0][2];
		dy = c[1][2];
		matrix_3D::vector_3D dr;
		dr.x = c[0][2];
		dr.y = c[1][2];
		dr.z = dz;

		x_rot = x_rot * -1;
		y_rot = y_rot * -1;
		matrix_3D::matrix_33 x_rot_mat_inv(0, x_rot), y_rot_mat_inv(1, y_rot);


		dr.matrix_multiplication(y_rot_mat_inv);
		dr.matrix_multiplication(x_rot_mat_inv);

		dx = dr.x;
		dy = dr.y;
		dz = dr.z;

		//printf("x rot: %.6lf\n",x_rot);
		//printf("y rot: %.6lf\n",y_rot);
		//printf("z rot: %.6lf\n",z_rot);
		//printf("x shrink: %.6lf\n", x_shrink);
		//printf("y shrink: %.6lf\n", y_shrink);
		//printf("z shrink: %.6lf\n", z_shrink);
		//printf("x shift: %.5lf\n", dx);
		//printf("y shift: %.5lf\n", dy);
		//printf("z shift: %.5lf\n", dz);
		//printf("yx shear: %.6lf\n", yx_shear);
		//printf("zx shear: %.6lf\n", zx_shear);
		//printf("zy shear: %.6lf\n", zy_shear);

		//std::vector< matrix_3D::vector_3D >point,point_after;
		//for (int i = 0; i < 3; i++) {
		//	matrix_3D::vector_3D p;
		//	p.x = corr_p[i]->x;
		//	p.y = corr_p[i]->y;
		//	p.z = corr_p[i]->z;
		//	point.push_back(p);
		//	p.x = corr_p[i]->x + corr_p[i]->dx;
		//	p.y = corr_p[i]->y + corr_p[i]->dy;
		//	p.z = corr_p[i]->z + corr_p[i]->dz;
		//	point_after.push_back(p);
		//}
		//trans_9para(point, *this);
		//for (auto p : point_after) {
		//	printf("x:%10.1lf y:%10.1lf z:%10.1lf\n", p.x, p.y, p.z);
		//}
	}

	void align_param2::Calc_9param_inv() {


		double bp[3][3], ap[3][3], cos_z, sin_z;
		for (int i = 0; i < 3; i++) {
			ap[i][0] = corr_p[i]->x;
			ap[i][1] = corr_p[i]->y;
			ap[i][2] = corr_p[i]->z;

			cos_z = cos(corr_p[i]->z_rot);
			sin_z = sin(corr_p[i]->z_rot);
			bp[i][0] = corr_p[i]->x_shrink*cos_z*(corr_p[i]->x) - corr_p[i]->y_shrink*sin_z*(corr_p[i]->y) + corr_p[i]->dx;
			bp[i][1] = corr_p[i]->x_shrink*sin_z*(corr_p[i]->x) + corr_p[i]->y_shrink*cos_z*(corr_p[i]->y) + corr_p[i]->dy;
			bp[i][2] = corr_p[i]->z + corr_p[i]->dz;
		}
		//apの位置ずれvectorを定義
		double dp[2][3];
		for (int i = 0; i < 3; i++) {
			dp[0][i] = ap[1][i] - ap[0][i];
			dp[1][i] = ap[2][i] - ap[0][i];
		}
		//printf("0-->1 x,y,z : %.1lf %.1lf %.1lf\n", dp[0][0], dp[0][1], dp[0][2]);
		//printf("0-->2 x,y,z : %.1lf %.1lf %.1lf\n", dp[1][0], dp[1][1], dp[1][2]);
		//法線vector
		double n_v[3];
		n_v[0] = (dp[0][1] * dp[1][2] - dp[0][2] * dp[1][1]);
		n_v[1] = (dp[0][2] * dp[1][0] - dp[0][0] * dp[1][2]);
		n_v[2] = (dp[0][0] * dp[1][1] - dp[0][1] * dp[1][0]);

		//std::cout << "normal vector" << std::endl;
		//for (int i = 0; i < 3; i++) {
		//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
		//}

		x_rot = atan(n_v[1] / n_v[2]);
		n_v[1] = cos(x_rot)*n_v[1] - sin(x_rot)*n_v[2];
		n_v[2] = sin(x_rot)*n_v[1] + cos(x_rot)*n_v[2];
		//std::cout << "normal vector" << std::endl;
		//for (int i = 0; i < 3; i++) {
		//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
		//}
		y_rot = atan(-1 * n_v[0] / n_v[2]);
		n_v[0] = cos(y_rot)*n_v[0] + sin(y_rot)*n_v[2];
		n_v[2] = -1 * sin(y_rot)*n_v[0] + cos(y_rot)*n_v[2];

		//std::cout << "normal vector" << std::endl;
		//for (int i = 0; i < 3; i++) {
		//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
		//}

		//printf("x rot:%.6lf\n", x_rot);
		//printf("y rot:%.6lf\n", y_rot);


		matrix_3D::matrix_33 x_rot_mat(0, x_rot), y_rot_mat(1, y_rot);
		matrix_3D::vector_3D ap_v[3];
		for (int i = 0; i < 3; i++) {
			ap_v[i].x = ap[i][0];
			ap_v[i].y = ap[i][1];
			ap_v[i].z = ap[i][2];
		}
		for (int i = 0; i < 3; i++) {
			ap_v[i].matrix_multiplication(x_rot_mat);
			ap_v[i].matrix_multiplication(y_rot_mat);
		}
		//for (int i = 0; i < 3; i++) {
		//	printf("point %d\n", i);
		//	printf("\t %.2lf %.2lf %.2lf\n", bp[i][0], bp[i][1], bp[i][2]);
		//	printf("\t %.2lf %.2lf %.2lf\n", ap_v[i].x, ap_v[i].y, ap_v[i].z);
		//}
		dz = (ap_v[0].z - bp[0][2] + ap_v[1].z - bp[1][2] + ap_v[2].z - bp[2][2]) / 3;
		//printf("dz=%.2lf\n", dz);
		//3元方程式を解く
		double a[2][3][3] = { { {bp[0][0],bp[0][1],1},{bp[1][0],bp[1][1],1},{bp[2][0],bp[2][1],1} },  { {bp[0][0],bp[0][1],1},{bp[1][0],bp[1][1],1},{bp[2][0],bp[2][1],1} } };
		double b[2][3] = { { ap_v[0].x,ap_v[1].x,ap_v[2].x },{ ap_v[0].y,ap_v[1].y,ap_v[2].y } };
		double c[2][3] = { {1, 1, 1},{1,1,1} };
		//gauss(a[0], b[0], c[0]);
		//gauss(a[1], b[1], c[1]);
		GaussJorden(a[0], b[0], c[0]);
		GaussJorden(a[1], b[1], c[1]);
		z_rot = atan(c[1][0] / c[0][0]);
		x_shrink = c[0][0] / cos(z_rot);
		y_shrink = (c[0][0] * c[1][1] - c[0][1] * c[1][0]) / (c[0][0] * cos(z_rot) + c[1][0] * sin(z_rot));
		yx_shear = (c[0][1] * cos(z_rot) + c[1][1] * sin(z_rot)) / (c[0][0] * cos(z_rot) + c[1][0] * sin(z_rot));

		dx = c[0][2];
		dy = c[1][2];
		matrix_3D::vector_3D dr;
		dr.x = c[0][2];
		dr.y = c[1][2];
		dr.z = dz;

		x_rot = x_rot * -1;
		y_rot = y_rot * -1;
		matrix_3D::matrix_33 x_rot_mat_inv(0, x_rot), y_rot_mat_inv(1, y_rot);


		dr.matrix_multiplication(y_rot_mat_inv);
		dr.matrix_multiplication(x_rot_mat_inv);

		dx = dr.x;
		dy = dr.y;
		dz = dr.z;

		//printf("x rot: %.6lf\n",x_rot);
		//printf("y rot: %.6lf\n",y_rot);
		//printf("z rot: %.6lf\n",z_rot);
		//printf("x shrink: %.6lf\n", x_shrink);
		//printf("y shrink: %.6lf\n", y_shrink);
		//printf("z shrink: %.6lf\n", z_shrink);
		//printf("x shift: %.5lf\n", dx);
		//printf("y shift: %.5lf\n", dy);
		//printf("z shift: %.5lf\n", dz);
		//printf("yx shear: %.6lf\n", yx_shear);
		//printf("zx shear: %.6lf\n", zx_shear);
		//printf("zy shear: %.6lf\n", zy_shear);

		//std::vector< matrix_3D::vector_3D >point,point_after;
		//for (int i = 0; i < 3; i++) {
		//	matrix_3D::vector_3D p;
		//	p.x = corr_p[i]->x;
		//	p.y = corr_p[i]->y;
		//	p.z = corr_p[i]->z;
		//	point.push_back(p);
		//	p.x = corr_p[i]->x + corr_p[i]->dx;
		//	p.y = corr_p[i]->y + corr_p[i]->dy;
		//	p.z = corr_p[i]->z + corr_p[i]->dz;
		//	point_after.push_back(p);
		//}
		//trans_9para(point, *this);
		//for (auto p : point_after) {
		//	printf("x:%10.1lf y:%10.1lf z:%10.1lf\n", p.x, p.y, p.z);
		//}
	}

	void align_param_3D::Calc_9param() {
		double bp[3][3], ap[3][3];
		for (int i = 0; i < 3; i++) {
			bp[i][0] = x[0][i];
			bp[i][1] = y[0][i];
			bp[i][2] = z[0][i];

			ap[i][0] = x[1][i];
			ap[i][1] = y[1][i];
			ap[i][2] = z[1][i];
		}
		//apの位置ずれvectorを定義
		double dp[2][3];
		for (int i = 0; i < 3; i++) {
			dp[0][i] = ap[1][i] - ap[0][i];
			dp[1][i] = ap[2][i] - ap[0][i];
		}
		//printf("0-->1 x,y,z : %.1lf %.1lf %.1lf\n", dp[0][0], dp[0][1], dp[0][2]);
		//printf("0-->2 x,y,z : %.1lf %.1lf %.1lf\n", dp[1][0], dp[1][1], dp[1][2]);
		//法線vector
		double n_v[3];
		n_v[0] = (dp[0][1] * dp[1][2] - dp[0][2] * dp[1][1]);
		n_v[1] = (dp[0][2] * dp[1][0] - dp[0][0] * dp[1][2]);
		n_v[2] = (dp[0][0] * dp[1][1] - dp[0][1] * dp[1][0]);

		//std::cout << "normal vector" << std::endl;
		//for (int i = 0; i < 3; i++) {
		//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
		//}

		x_rot = atan(n_v[1] / n_v[2]);
		n_v[1] = cos(x_rot)*n_v[1] - sin(x_rot)*n_v[2];
		n_v[2] = sin(x_rot)*n_v[1] + cos(x_rot)*n_v[2];
		//std::cout << "normal vector" << std::endl;
		//for (int i = 0; i < 3; i++) {
		//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
		//}
		y_rot = atan(-1 * n_v[0] / n_v[2]);
		n_v[0] = cos(y_rot)*n_v[0] + sin(y_rot)*n_v[2];
		n_v[2] = -1 * sin(y_rot)*n_v[0] + cos(y_rot)*n_v[2];

		//std::cout << "normal vector" << std::endl;
		//for (int i = 0; i < 3; i++) {
		//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
		//}

		//printf("x rot:%.6lf\n", x_rot);
		//printf("y rot:%.6lf\n", y_rot);


		matrix_3D::matrix_33 x_rot_mat(0, x_rot), y_rot_mat(1, y_rot);
		matrix_3D::vector_3D ap_v[3];
		for (int i = 0; i < 3; i++) {
			ap_v[i].x = ap[i][0];
			ap_v[i].y = ap[i][1];
			ap_v[i].z = ap[i][2];
		}
		for (int i = 0; i < 3; i++) {
			ap_v[i].matrix_multiplication(x_rot_mat);
			ap_v[i].matrix_multiplication(y_rot_mat);
		}
		//for (int i = 0; i < 3; i++) {
		//	printf("point %d\n", i);
		//	printf("\t %.2lf %.2lf %.2lf\n", bp[i][0], bp[i][1], bp[i][2]);
		//	printf("\t %.2lf %.2lf %.2lf\n", ap_v[i].x, ap_v[i].y, ap_v[i].z);
		//}
		dz = (ap_v[0].z - bp[0][2] + ap_v[1].z - bp[1][2] + ap_v[2].z - bp[2][2]) / 3;
		//printf("dz=%.2lf\n", dz);
		//3元方程式を解く
		double a[2][3][3] = { { {bp[0][0],bp[0][1],1},{bp[1][0],bp[1][1],1},{bp[2][0],bp[2][1],1} },  { {bp[0][0],bp[0][1],1},{bp[1][0],bp[1][1],1},{bp[2][0],bp[2][1],1} } };
		double b[2][3] = { { ap_v[0].x,ap_v[1].x,ap_v[2].x },{ ap_v[0].y,ap_v[1].y,ap_v[2].y } };
		double c[2][3] = { {1, 1, 1},{1,1,1} };
		//gauss(a[0], b[0], c[0]);
		//gauss(a[1], b[1], c[1]);
		GaussJorden(a[0], b[0], c[0]);
		GaussJorden(a[1], b[1], c[1]);
		z_rot = atan(c[1][0] / c[0][0]);
		x_shrink = c[0][0] / cos(z_rot);
		y_shrink = (c[0][0] * c[1][1] - c[0][1] * c[1][0]) / (c[0][0] * cos(z_rot) + c[1][0] * sin(z_rot));
		yx_shear = (c[0][1] * cos(z_rot) + c[1][1] * sin(z_rot)) / (c[0][0] * cos(z_rot) + c[1][0] * sin(z_rot));

		dx = c[0][2];
		dy = c[1][2];
		matrix_3D::vector_3D dr;
		dr.x = c[0][2];
		dr.y = c[1][2];
		dr.z = dz;

		x_rot = x_rot * -1;
		y_rot = y_rot * -1;
		matrix_3D::matrix_33 x_rot_mat_inv(0, x_rot), y_rot_mat_inv(1, y_rot);


		dr.matrix_multiplication(y_rot_mat_inv);
		dr.matrix_multiplication(x_rot_mat_inv);

		dx = dr.x;
		dy = dr.y;
		dz = dr.z;
	}
	void align_param_3D::Param_inv() {
		double buff[3];
		for (int i = 0; i < 3; i++) {
			buff[0] = x[0][i];
			buff[1] = y[0][i];
			buff[2] = z[0][i];
			x[0][i] = x[1][i];
			y[0][i] = y[1][i];
			z[0][i] = z[1][i];
			x[1][i] = buff[0];
			y[1][i] = buff[1];
			z[1][i] = buff[2];
		}
		z_shrink = 1 / z_shrink;
		zx_shear = -1 * zx_shear;
		zy_shear = -1 * zy_shear;
	}
	bool triangle_internal_angle_cut(align_param2 &param,double threshold_degree) {
		double l[3];
		l[0] = pow(param.corr_p[0]->x - param.corr_p[1]->x, 2) + pow(param.corr_p[0]->y - param.corr_p[1]->y, 2);
		l[1] = pow(param.corr_p[1]->x - param.corr_p[2]->x, 2) + pow(param.corr_p[1]->y - param.corr_p[2]->y, 2);
		l[2] = pow(param.corr_p[2]->x - param.corr_p[0]->x, 2) + pow(param.corr_p[2]->y - param.corr_p[0]->y, 2);
		int index;
		if (l[0] <= l[1] && l[0] <= l[2]) {
			//0-2-1の角度が最小
			index = 0;
		}
		else if (l[1] <= l[0] && l[1] <= l[2]) {
			index = 1;
		}
		else {
			index = 2;
		}
		double a, b, c;
		a = l[index];
		b= l[(index+1)%3];
		c = l[(index+2)%3];
		double theta = acos((b + c - a) / (2 * sqrt(b*c)));
		return theta >= threshold_degree*3.1415926535 / 180;
	}
	//vxx::base_track_t
	//basetrack-alignment mapの対応
	std::vector <std::pair<vxx::base_track_t*, align_param2*>>track_affineparam_correspondence(std::vector<vxx::base_track_t>&base, std::vector <align_param2> &param) {

		//local alignの視野中心を取り出して、位置でhash
		//local alignの視野中心の作るdelaunay三角形をmapで対応

		std::map<int, align_param*> view_center;
		std::multimap<int, align_param2*>triangles;
		double xmin = 999999, ymin = 999999, hash = 2000;
		for (auto itr = param.begin(); itr != param.end(); itr++) {
			for (int i = 0; i < 3; i++) {
				view_center.insert(std::make_pair(itr->corr_p[i]->id, (itr->corr_p[i])));
				triangles.insert(std::make_pair(itr->corr_p[i]->id, &(*itr)));
				xmin = std::min(itr->corr_p[i]->x, xmin);
				ymin = std::min(itr->corr_p[i]->y, ymin);
			}
		}
		std::multimap<std::pair<int, int>, align_param*> view_center_hash;
		std::pair<int, int>id;
		for (auto itr = view_center.begin(); itr != view_center.end(); itr++) {
			id.first = int((itr->second->x - xmin) / hash);
			id.second = int((itr->second->y - ymin) / hash);
			view_center_hash.insert(std::make_pair(id, itr->second));
		}

		std::vector < std::pair<vxx::base_track_t*, align_param2*>> ret;
		std::vector<align_param*> param_cand;
		int loop = 0, ix, iy, count = 0;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			if (count % 100000 == 0) {
				printf("\r search correspond triangles %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
			}
			count++;
			ix = (itr->x - xmin) / hash;
			iy = (itr->y - ymin) / hash;
			loop = 1;
			while (true) {
				param_cand.clear();
				for (int iix = ix - loop; iix <= ix + loop; iix++) {
					for (int iiy = iy - loop; iiy <= iy + loop; iiy++) {
						id.first = iix;
						id.second = iiy;
						if (view_center_hash.count(id) != 0) {
							auto range = view_center_hash.equal_range(id);
							for (auto res = range.first; res != range.second; res++) {
								param_cand.push_back(res->second);
							}
						}
					}
				}
				if (param_cand.size() > 2)break;
				loop++;
			}
			align_param2* param2 = search_param(param_cand, *itr, triangles);
			ret.push_back(std::make_pair(&(*itr), param2));
		}
		printf("\r search correspond triangles %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

		return ret;
	}
	std::vector <std::pair<vxx::base_track_t*, align_param2*>>track_affineparam_correspondence(std::vector<vxx::base_track_t*>&base, std::vector <align_param2> &param) {

		//local alignの視野中心を取り出して、位置でhash
		//local alignの視野中心の作るdelaunay三角形をmapで対応

		std::map<int, align_param*> view_center;
		std::multimap<int, align_param2*>triangles;
		double xmin = 999999, ymin = 999999, hash = 2000;
		for (auto itr = param.begin(); itr != param.end(); itr++) {
			for (int i = 0; i < 3; i++) {
				view_center.insert(std::make_pair(itr->corr_p[i]->id, (itr->corr_p[i])));
				triangles.insert(std::make_pair(itr->corr_p[i]->id, &(*itr)));
				xmin = std::min(itr->corr_p[i]->x, xmin);
				ymin = std::min(itr->corr_p[i]->y, ymin);
			}
		}
		std::multimap<std::pair<int, int>, align_param*> view_center_hash;
		std::pair<int, int>id;
		for (auto itr = view_center.begin(); itr != view_center.end(); itr++) {
			id.first = int((itr->second->x - xmin) / hash);
			id.second = int((itr->second->y - ymin) / hash);
			view_center_hash.insert(std::make_pair(id, itr->second));
		}

		std::vector < std::pair<vxx::base_track_t*, align_param2*>> ret;
		std::vector<align_param*> param_cand;
		int loop = 0, ix, iy, count = 0;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			if (count % 100000 == 0) {
				printf("\r search correspond triangles %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
			}
			count++;
			ix = ((*itr)->x - xmin) / hash;
			iy = ((*itr)->y - ymin) / hash;
			loop = 1;
			while (true) {
				param_cand.clear();
				for (int iix = ix - loop; iix <= ix + loop; iix++) {
					for (int iiy = iy - loop; iiy <= iy + loop; iiy++) {
						id.first = iix;
						id.second = iiy;
						if (view_center_hash.count(id) != 0) {
							auto range = view_center_hash.equal_range(id);
							for (auto res = range.first; res != range.second; res++) {
								param_cand.push_back(res->second);
							}
						}
					}
				}
				if (param_cand.size() > 2)break;
				loop++;
			}
			align_param2* param2 = search_param(param_cand, *(*itr), triangles);
			ret.push_back(std::make_pair((*itr), param2));
		}
		printf("\r search correspond triangles %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

		return ret;
	}
	align_param2* search_param(std::vector<align_param*> &param, vxx::base_track_t&base, std::multimap<int, align_param2*>&triangles) {
		//三角形内部
		//最近接三角形
		double dist = 0;
		std::map<double, align_param* > dist_map;
		//align_paramを近い順にsort
		for (auto itr = param.begin(); itr != param.end(); itr++) {
			dist = ((*itr)->x - base.x)*((*itr)->x - base.x) + ((*itr)->y - base.y)*((*itr)->y - base.y);
			dist_map.insert(std::make_pair(dist, (*itr)));
		}

		double sign[3];
		bool flg = false;
		int id;

		align_param2* ret = triangles.begin()->second;
		for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
			if (itr != dist_map.begin())continue;


			//corrmapのID
			id = itr->second->id;
			if (triangles.count(id) == 0) {
				fprintf(stderr, "alignment triangle ID=%d not found\n", id);
				exit(1);
			}
			//idの属する三角形を探索
			auto range = triangles.equal_range(id);
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				sign[0] = (itr2->second->corr_p[1]->x - itr2->second->corr_p[0]->x)*(base.y - itr2->second->corr_p[1]->y) - (itr2->second->corr_p[1]->y - itr2->second->corr_p[0]->y)*(base.x - itr2->second->corr_p[1]->x);
				sign[1] = (itr2->second->corr_p[2]->x - itr2->second->corr_p[1]->x)*(base.y - itr2->second->corr_p[2]->y) - (itr2->second->corr_p[2]->y - itr2->second->corr_p[1]->y)*(base.x - itr2->second->corr_p[2]->x);
				sign[2] = (itr2->second->corr_p[0]->x - itr2->second->corr_p[2]->x)*(base.y - itr2->second->corr_p[0]->y) - (itr2->second->corr_p[0]->y - itr2->second->corr_p[2]->y)*(base.x - itr2->second->corr_p[0]->x);
				//printf("point %.lf,%.1lf\n", base.x, base.y);
				//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[0]->x, itr2->second->corr_p[0]->y);
				//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[1]->x, itr2->second->corr_p[1]->y);
				//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[2]->x, itr2->second->corr_p[2]->y);
				//printf("sign %.1lf %1.lf %.1lf\n", sign[0], sign[1], sign[2]);
				//printf("  signbit %d %d %d\n", std::signbit(sign[0]), std::signbit(sign[1]), std::signbit(sign[2]));
				//printf("n signbit %d %d %d\n", !std::signbit(sign[0]), !std::signbit(sign[1]), !std::signbit(sign[2]));
				//printf("judge %d\n", (std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2])));
				//printf("\n");

				//符号が3つとも一致でtrue
				if ((std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2]))) {
					ret = itr2->second;
					flg = true;
					break;
				}
			}
			if (flg)break;
		}
		if (flg) {
			//printf("point in trianlge\n");
			return ret;
		}

		//distが最小になるcorrmapをとってくる
		dist = -1;
		for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
			//corrmapのID
			id = itr->second->id;
			if (triangles.count(id) == 0) {
				fprintf(stderr, "alignment triangle ID=%d not found\n", id);
				exit(1);
			}
			//idの属する三角形を探索
			auto range = triangles.equal_range(id);
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (dist<0 || dist>select_triangle_vale(itr2->second, base)) {
					dist = select_triangle_vale(itr2->second, base);
					ret = itr2->second;
				}
			}
		}
		//printf("point not in trianlge\n");
		return ret;
	}
	double select_triangle_vale(align_param2* param, vxx::base_track_t&base) {
		double x, y;
		double dist = 0;
		x = (param->corr_p[0]->x + param->corr_p[1]->x + param->corr_p[2]->x) / 3;
		y = (param->corr_p[0]->y + param->corr_p[1]->y + param->corr_p[2]->y) / 3;
		dist = (base.x - x)*(base.x - x) + (base.y - y)*(base.y - y);
		return dist;
	}
	//変換 zshrink補正-->9para変換
	void trans_base_all(std::vector < std::pair<vxx::base_track_t*, align_param2*>>&track_pair) {
		std::map<std::tuple<int, int, int>, align_param2*> param_map;
		std::multimap<std::tuple<int, int, int>, vxx::base_track_t*>base_map;
		std::tuple<int, int, int>id;
		//三角形ごとにbasetrackをまとめる
		for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
			std::get<0>(id) = itr->second->corr_p[0]->id;
			std::get<1>(id) = itr->second->corr_p[1]->id;
			std::get<2>(id) = itr->second->corr_p[2]->id;
			param_map.insert(std::make_pair(id, itr->second));
			base_map.insert(std::make_pair(id, itr->first));
		}


		//ここで三角形ごとに変換
		int count = 0;
		std::vector<vxx::base_track_t*> t_base;
		for (auto itr = param_map.begin(); itr != param_map.end(); itr++) {
			if (count % 1000 == 0) {
				printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)", count, param_map.size(), count*100. / param_map.size());
			}
			count++;

			t_base.clear();

			if (base_map.count(itr->first) == 0)continue;
			auto range = base_map.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				t_base.push_back(res->second);
			}
			trans_base(t_base, itr->second);

		}
		printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)\n", count, param_map.size(), count*100. / param_map.size());

	}
	void trans_base(std::vector<vxx::base_track_t*>&base, align_param2 *param) {

		matrix_3D::matrix_33 x_rot_mat(0, param->x_rot), y_rot_mat(1, param->y_rot), z_rot_mat(2, param->z_rot), all_trans(0, 0), shear_mat(0, 0), shrink_mat(0, 0);

		shrink_mat.val[0][0] *= param->x_shrink;
		shrink_mat.val[1][1] *= param->y_shrink;
		//shrink_mat.val[2][2] *= param->z_shrink;
		shear_mat.val[0][1] = param->yx_shear;
		//shear_mat.val[0][2] = param->zx_shear;
		//shear_mat.val[1][2] = param->zy_shear;

		matrix_3D::vector_3D shift, center;
		center.x = param->x;
		center.y = param->y;
		center.z = param->z;
		shift.x = param->dx;
		shift.y = param->dy;
		shift.z = param->dz;

		all_trans.matrix_multiplication(shear_mat);
		all_trans.matrix_multiplication(shrink_mat);
		all_trans.matrix_multiplication(z_rot_mat);
		all_trans.matrix_multiplication(y_rot_mat);
		all_trans.matrix_multiplication(x_rot_mat);

		//all_trans.Print();
		matrix_3D::vector_3D base_p0, base_p1;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_p0.x = (*itr)->x;
			base_p0.y = (*itr)->y;
			base_p0.z = param->z;

			base_p1.x = (*itr)->x + (*itr)->ax*((*itr)->m[1].z - (*itr)->m[0].z);
			base_p1.y = (*itr)->y + (*itr)->ay*((*itr)->m[1].z - (*itr)->m[0].z);
			//角度shrink分はここでかける
			base_p1.z = param->z + ((*itr)->m[1].z - (*itr)->m[0].z) / param->z_shrink;

			//視野中心を原点に移動
			//base_p0 = matrix_3D::addition(base_p0, matrix_3D::const_multiple(center, -1));
			//base_p1 = matrix_3D::addition(base_p1, matrix_3D::const_multiple(center, -1));

			//変換の実行
			base_p0.matrix_multiplication(all_trans);
			base_p0 = matrix_3D::addition(base_p0, shift);
			base_p1.matrix_multiplication(all_trans);
			base_p1 = matrix_3D::addition(base_p1, shift);

			//原点をもとに戻す
			//base_p0 = matrix_3D::addition(base_p0, center);
			//base_p1 = matrix_3D::addition(base_p1, center);

			(*itr)->x = base_p0.x;
			(*itr)->y = base_p0.y;
			(*itr)->z = base_p0.z;

			//printf("ax:%.4lf --> %.4lf\n", (*itr)->ax, (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z));
			//printf("ay:%.4lf --> %.4lf\n", (*itr)->ay, (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z));

			(*itr)->ax = (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z) + param->zx_shear;
			(*itr)->ay = (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z) + param->zy_shear;

		}
	}
	
	//mfile0::M_Base
	//basetrack-alignment mapの対応
	double select_triangle_vale(align_param2* param, mfile0::M_Base*base) {
		double x, y;
		double dist = 0;
		x = (param->corr_p[0]->x + param->corr_p[1]->x + param->corr_p[2]->x) / 3;
		y = (param->corr_p[0]->y + param->corr_p[1]->y + param->corr_p[2]->y) / 3;
		dist = (base->x - x)*(base->x - x) + (base->y - y)*(base->y - y);
		return dist;
	}
	align_param2* search_param(std::vector<align_param*> &param, mfile0::M_Base*base, std::multimap<int, align_param2*>&triangles) {
		//三角形内部
		//最近接三角形
		double dist = 0;
		std::map<double, align_param* > dist_map;
		//align_paramを近い順にsort
		for (auto itr = param.begin(); itr != param.end(); itr++) {
			dist = ((*itr)->x - base->x)*((*itr)->x - base->x) + ((*itr)->y - base->y)*((*itr)->y - base->y);
			dist_map.insert(std::make_pair(dist, (*itr)));
		}

		double sign[3];
		bool flg = false;
		int id;

		align_param2* ret = triangles.begin()->second;
		for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
			if (itr != dist_map.begin())continue;


			//corrmapのID
			id = itr->second->id;
			if (triangles.count(id) == 0) {
				fprintf(stderr, "alignment triangle ID=%d not found\n", id);
				exit(1);
			}
			//idの属する三角形を探索
			auto range = triangles.equal_range(id);
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				sign[0] = (itr2->second->corr_p[1]->x - itr2->second->corr_p[0]->x)*(base->y - itr2->second->corr_p[1]->y) - (itr2->second->corr_p[1]->y - itr2->second->corr_p[0]->y)*(base->x - itr2->second->corr_p[1]->x);
				sign[1] = (itr2->second->corr_p[2]->x - itr2->second->corr_p[1]->x)*(base->y - itr2->second->corr_p[2]->y) - (itr2->second->corr_p[2]->y - itr2->second->corr_p[1]->y)*(base->x - itr2->second->corr_p[2]->x);
				sign[2] = (itr2->second->corr_p[0]->x - itr2->second->corr_p[2]->x)*(base->y - itr2->second->corr_p[0]->y) - (itr2->second->corr_p[0]->y - itr2->second->corr_p[2]->y)*(base->x - itr2->second->corr_p[0]->x);
				//printf("point %.lf,%.1lf\n", base.x, base.y);
				//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[0]->x, itr2->second->corr_p[0]->y);
				//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[1]->x, itr2->second->corr_p[1]->y);
				//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[2]->x, itr2->second->corr_p[2]->y);
				//printf("sign %.1lf %1.lf %.1lf\n", sign[0], sign[1], sign[2]);
				//printf("  signbit %d %d %d\n", std::signbit(sign[0]), std::signbit(sign[1]), std::signbit(sign[2]));
				//printf("n signbit %d %d %d\n", !std::signbit(sign[0]), !std::signbit(sign[1]), !std::signbit(sign[2]));
				//printf("judge %d\n", (std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2])));
				//printf("\n");

				//符号が3つとも一致でtrue
				if ((std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2]))) {
					ret = itr2->second;
					flg = true;
					break;
				}
			}
			if (flg)break;
		}
		if (flg) {
			//printf("point in trianlge\n");
			return ret;
		}

		//distが最小になるcorrmapをとってくる
		dist = -1;
		for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
			//corrmapのID
			id = itr->second->id;
			if (triangles.count(id) == 0) {
				fprintf(stderr, "alignment triangle ID=%d not found\n", id);
				exit(1);
			}
			//idの属する三角形を探索
			auto range = triangles.equal_range(id);
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (dist<0 || dist>select_triangle_vale(itr2->second, base)) {
					dist = select_triangle_vale(itr2->second, base);
					ret = itr2->second;
				}
			}
		}
		//printf("point not in trianlge\n");
		return ret;
	}
	std::vector <std::pair<mfile0::M_Base*, align_param2*>>track_affineparam_correspondence(std::vector<mfile0::M_Base*>&base, std::vector <align_param2> &param) {

		//local alignの視野中心を取り出して、位置でhash
		//local alignの視野中心の作るdelaunay三角形をmapで対応

		std::map<int, align_param*> view_center;
		std::multimap<int, align_param2*>triangles;
		double xmin = 999999, ymin = 999999, hash = 2000;
		for (auto itr = param.begin(); itr != param.end(); itr++) {
			for (int i = 0; i < 3; i++) {
				view_center.insert(std::make_pair(itr->corr_p[i]->id, (itr->corr_p[i])));
				triangles.insert(std::make_pair(itr->corr_p[i]->id, &(*itr)));
				xmin = std::min(itr->corr_p[i]->x, xmin);
				ymin = std::min(itr->corr_p[i]->y, ymin);
			}
		}
		std::multimap<std::pair<int, int>, align_param*> view_center_hash;
		std::pair<int, int>id;
		for (auto itr = view_center.begin(); itr != view_center.end(); itr++) {
			id.first = int((itr->second->x - xmin) / hash);
			id.second = int((itr->second->y - ymin) / hash);
			view_center_hash.insert(std::make_pair(id, itr->second));
		}

		std::vector < std::pair<mfile0::M_Base*, align_param2*>> ret;
		std::vector<align_param*> param_cand;
		int loop = 0, ix, iy, count = 0;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			//if (count % 100000 == 0) {
			//	printf("\r search correspond triangles %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
			//}
			count++;
			ix = ((*itr)->x - xmin) / hash;
			iy = ((*itr)->y - ymin) / hash;
			loop = 1;
			while (true) {
				param_cand.clear();
				for (int iix = ix - loop; iix <= ix + loop; iix++) {
					for (int iiy = iy - loop; iiy <= iy + loop; iiy++) {
						id.first = iix;
						id.second = iiy;
						if (view_center_hash.count(id) != 0) {
							auto range = view_center_hash.equal_range(id);
							for (auto res = range.first; res != range.second; res++) {
								param_cand.push_back(res->second);
							}
						}
					}
				}
				if (param_cand.size() > 2)break;
				loop++;
			}
			align_param2* param2 = search_param(param_cand, *itr, triangles);
			ret.push_back(std::make_pair((*itr), param2));
		}
		//printf("\r search correspond triangles %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

		return ret;
	}
	//変換 zshrink補正-->9para変換
	void trans_base(std::vector<mfile0::M_Base*>&base, align_param2 *param) {

		matrix_3D::matrix_33 x_rot_mat(0, param->x_rot), y_rot_mat(1, param->y_rot), z_rot_mat(2, param->z_rot), all_trans(0, 0), shear_mat(0, 0), shrink_mat(0, 0);

		shrink_mat.val[0][0] *= param->x_shrink;
		shrink_mat.val[1][1] *= param->y_shrink;
		//shrink_mat.val[2][2] *= param->z_shrink;
		shear_mat.val[0][1] = param->yx_shear;
		//shear_mat.val[0][2] = param->zx_shear;
		//shear_mat.val[1][2] = param->zy_shear;

		matrix_3D::vector_3D shift, center;
		center.x = param->x;
		center.y = param->y;
		center.z = param->z;
		shift.x = param->dx;
		shift.y = param->dy;
		shift.z = param->dz;

		all_trans.matrix_multiplication(shear_mat);
		all_trans.matrix_multiplication(shrink_mat);
		all_trans.matrix_multiplication(z_rot_mat);
		all_trans.matrix_multiplication(y_rot_mat);
		all_trans.matrix_multiplication(x_rot_mat);

		//all_trans.Print();
		matrix_3D::vector_3D base_p0, base_p1;
		double base_thick = 210;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_p0.x = (*itr)->x;
			base_p0.y = (*itr)->y;
			base_p0.z = param->z;

			base_p1.x = (*itr)->x + (*itr)->ax*base_thick;
			base_p1.y = (*itr)->y + (*itr)->ay*base_thick;
			//角度shrink分はここでかける
			base_p1.z = param->z + base_thick / param->z_shrink;

			//視野中心を原点に移動
			//base_p0 = matrix_3D::addition(base_p0, matrix_3D::const_multiple(center, -1));
			//base_p1 = matrix_3D::addition(base_p1, matrix_3D::const_multiple(center, -1));

			//変換の実行
			base_p0.matrix_multiplication(all_trans);
			base_p0 = matrix_3D::addition(base_p0, shift);
			base_p1.matrix_multiplication(all_trans);
			base_p1 = matrix_3D::addition(base_p1, shift);

			//原点をもとに戻す
			//base_p0 = matrix_3D::addition(base_p0, center);
			//base_p1 = matrix_3D::addition(base_p1, center);

			(*itr)->x = base_p0.x;
			(*itr)->y = base_p0.y;
			(*itr)->z = base_p0.z;

			//printf("ax:%.4lf --> %.4lf\n", (*itr)->ax, (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z));
			//printf("ay:%.4lf --> %.4lf\n", (*itr)->ay, (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z));

			(*itr)->ax = (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z) + param->zx_shear;
			(*itr)->ay = (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z) + param->zy_shear;

		}
	}
	void trans_base_all(std::vector < std::pair<mfile0::M_Base*, align_param2*>>&track_pair) {
		std::map<std::tuple<int, int, int>, align_param2*> param_map;
		std::multimap<std::tuple<int, int, int>, mfile0::M_Base*>base_map;
		std::tuple<int, int, int>id;
		//三角形ごとにbasetrackをまとめる
		for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
			std::get<0>(id) = itr->second->corr_p[0]->id;
			std::get<1>(id) = itr->second->corr_p[1]->id;
			std::get<2>(id) = itr->second->corr_p[2]->id;
			param_map.insert(std::make_pair(id, itr->second));
			base_map.insert(std::make_pair(id, itr->first));
		}


		//ここで三角形ごとに変換
		int count = 0;
		std::vector<mfile0::M_Base*> t_base;
		for (auto itr = param_map.begin(); itr != param_map.end(); itr++) {
			//if (count % 1000 == 0) {
			//	printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)", count, param_map.size(), count*100. / param_map.size());
			//}
			count++;

			t_base.clear();

			if (base_map.count(itr->first) == 0)continue;
			auto range = base_map.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				t_base.push_back(res->second);
			}
			trans_base(t_base, itr->second);

		}
		//printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)\n", count, param_map.size(), count*100. / param_map.size());

	}


	void trans_base(std::vector<vxx::base_track_t>&base, align_param2 *param) {

		matrix_3D::matrix_33 x_rot_mat(0, param->x_rot), y_rot_mat(1, param->y_rot), z_rot_mat(2, param->z_rot), all_trans(0, 0), shear_mat(0, 0), shrink_mat(0, 0);

		shrink_mat.val[0][0] *= param->x_shrink;
		shrink_mat.val[1][1] *= param->y_shrink;
		//shrink_mat.val[2][2] *= param->z_shrink;
		shear_mat.val[0][1] = param->yx_shear;
		//shear_mat.val[0][2] = param->zx_shear;
		//shear_mat.val[1][2] = param->zy_shear;

		matrix_3D::vector_3D shift, center;
		center.x = param->x;
		center.y = param->y;
		center.z = param->z;
		shift.x = param->dx;
		shift.y = param->dy;
		shift.z = param->dz;

		all_trans.matrix_multiplication(shear_mat);
		all_trans.matrix_multiplication(shrink_mat);
		all_trans.matrix_multiplication(z_rot_mat);
		all_trans.matrix_multiplication(y_rot_mat);
		all_trans.matrix_multiplication(x_rot_mat);

		//all_trans.Print();
		matrix_3D::vector_3D base_p0, base_p1;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_p0.x = (itr)->x;
			base_p0.y = (itr)->y;
			base_p0.z = param->z;

			base_p1.x = (itr)->x + (itr)->ax*((itr)->m[1].z - (itr)->m[0].z);
			base_p1.y = (itr)->y + (itr)->ay*((itr)->m[1].z - (itr)->m[0].z);
			//角度shrink分はここでかける
			base_p1.z = param->z + ((itr)->m[1].z - (itr)->m[0].z) / param->z_shrink;

			//視野中心を原点に移動
			//base_p0 = matrix_3D::addition(base_p0, matrix_3D::const_multiple(center, -1));
			//base_p1 = matrix_3D::addition(base_p1, matrix_3D::const_multiple(center, -1));

			//変換の実行
			base_p0.matrix_multiplication(all_trans);
			base_p0 = matrix_3D::addition(base_p0, shift);
			base_p1.matrix_multiplication(all_trans);
			base_p1 = matrix_3D::addition(base_p1, shift);

			//原点をもとに戻す
			//base_p0 = matrix_3D::addition(base_p0, center);
			//base_p1 = matrix_3D::addition(base_p1, center);

			(itr)->x = base_p0.x;
			(itr)->y = base_p0.y;
			(itr)->z = base_p0.z;

			//printf("ax:%.4lf --> %.4lf\n", (*itr)->ax, (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z));
			//printf("ay:%.4lf --> %.4lf\n", (*itr)->ay, (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z));

			(itr)->ax = (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z) + param->zx_shear;
			(itr)->ay = (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z) + param->zy_shear;

		}
	}
	void trans_basetrack(std::vector < vxx::base_track_t>&base, std::vector<align_param>&corr) {
		//delaunay3角形分割
		std::vector <align_param2 >corr2 = DelaunayDivide(corr);

		//trackとdelaunay3角形の対応
		std::vector < std::pair<vxx::base_track_t*, align_param2*>> track_param = track_affineparam_correspondence(base, corr2);
		//basetrackを変換
		trans_base_all(track_param);
	}
	
	//外挿
	void base_extra_PL0_to_PL1(std::vector<vxx::base_track_t>&base, std::vector<align_param2 >&param, bool coordinate_inverse) {
		for (auto &b : base) {
			align_param2 param_decide;
			double dist;
			//対応するパラメータの探索
			for (auto itr = param.begin(); itr != param.end(); itr++) {
				double ap[3][3], cos_z, sin_z;
				for (int i = 0; i < 3; i++) {
					cos_z = cos(itr->corr_p[i]->z_rot);
					sin_z = sin(itr->corr_p[i]->z_rot);
					ap[i][0] = itr->corr_p[i]->x_shrink*cos_z*(itr->corr_p[i]->x) - itr->corr_p[i]->y_shrink*sin_z*(itr->corr_p[i]->y) + itr->corr_p[i]->dx;
					ap[i][1] = itr->corr_p[i]->x_shrink*sin_z*(itr->corr_p[i]->x) + itr->corr_p[i]->y_shrink*cos_z*(itr->corr_p[i]->y) + itr->corr_p[i]->dy;
					ap[i][2] = itr->corr_p[i]->z + itr->corr_p[i]->dz;
				}

				matrix_3D::vector_3D dir0, dir1;
				dir0.x = ap[0][0] - ap[1][0];
				dir0.y = ap[0][1] - ap[1][1];
				dir0.z = ap[0][2] - ap[1][2];
				dir1.x = ap[0][0] - ap[2][0];
				dir1.y = ap[0][1] - ap[2][1];
				dir1.z = ap[0][2] - ap[2][2];


				matrix_3D::vector_3D cros;
				cros.x = dir0.y*dir1.z - dir0.z*dir1.y;
				cros.y = dir0.z*dir1.x - dir0.x*dir1.z;
				cros.z = dir0.x*dir1.y - dir0.y*dir1.x;

				double ex_t = ((ap[0][0] - b.x)*cros.x +
					(ap[0][1] - b.y)*cros.y +
					(ap[0][2] - 0)*cros.z)
					/
					(b.ax*cros.x + b.ay*cros.y + 1 * cros.z);
				//外挿時の座標
				matrix_3D::vector_3D pos_ex;
				pos_ex.x = b.x + b.ax*ex_t;
				pos_ex.y = b.y + b.ay*ex_t;
				pos_ex.z = ex_t;
				double sign[3];
				sign[0] = (ap[1][0] - ap[0][0])*(pos_ex.y - ap[1][1]) - (ap[1][1] - ap[0][1])*(pos_ex.x - ap[1][0]);
				sign[1] = (ap[2][0] - ap[1][0])*(pos_ex.y - ap[2][1]) - (ap[2][1] - ap[1][1])*(pos_ex.x - ap[2][0]);
				sign[2] = (ap[0][0] - ap[2][0])*(pos_ex.y - ap[0][1]) - (ap[0][1] - ap[2][1])*(pos_ex.x - ap[0][0]);

				//符号が3つとも一致でtrue
				if ((std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2]))) {
					param_decide = (*itr);
					break;
				}
				double dist_center;
				dist_center =
					pow(pos_ex.x - (ap[0][0] + ap[1][0] + ap[2][0]) / 3, 2) +
					pow(pos_ex.y - (ap[0][1] + ap[1][1] + ap[2][1]) / 3, 2) +
					pow(pos_ex.z - (ap[0][2] + ap[1][2] + ap[2][2]) / 3, 2);
				if (itr == param.begin() || dist > dist_center) {
					param_decide = (*itr);

					dist = dist_center;
				}
			}
			//外挿
			{
				double ap[3][3], cos_z, sin_z;
				for (int i = 0; i < 3; i++) {
					cos_z = cos(param_decide.corr_p[i]->z_rot);
					sin_z = sin(param_decide.corr_p[i]->z_rot);
					ap[i][0] = param_decide.corr_p[i]->x_shrink*cos_z*(param_decide.corr_p[i]->x) - param_decide.corr_p[i]->y_shrink*sin_z*(param_decide.corr_p[i]->y) + param_decide.corr_p[i]->dx;
					ap[i][1] = param_decide.corr_p[i]->x_shrink*sin_z*(param_decide.corr_p[i]->x) + param_decide.corr_p[i]->y_shrink*cos_z*(param_decide.corr_p[i]->y) + param_decide.corr_p[i]->dy;
					ap[i][2] = param_decide.corr_p[i]->z + param_decide.corr_p[i]->dz;
				}

				matrix_3D::vector_3D dir0, dir1;
				dir0.x = ap[0][0] - ap[1][0];
				dir0.y = ap[0][1] - ap[1][1];
				dir0.z = ap[0][2] - ap[1][2];
				dir1.x = ap[0][0] - ap[2][0];
				dir1.y = ap[0][1] - ap[2][1];
				dir1.z = ap[0][2] - ap[2][2];


				matrix_3D::vector_3D cros;
				cros.x = dir0.y*dir1.z - dir0.z*dir1.y;
				cros.y = dir0.z*dir1.x - dir0.x*dir1.z;
				cros.z = dir0.x*dir1.y - dir0.y*dir1.x;

				double ex_t = ((ap[0][0] - b.x)*cros.x +
					(ap[0][1] - b.y)*cros.y +
					(ap[0][2] - 0)*cros.z)
					/
					(b.ax*cros.x + b.ay*cros.y + 1 * cros.z);
				//外挿時の座標
				matrix_3D::vector_3D pos_ex;
				pos_ex.x = b.x + b.ax*ex_t;
				pos_ex.y = b.y + b.ay*ex_t;
				pos_ex.z = ex_t;

				b.x = pos_ex.x;
				b.y = pos_ex.y;
				b.z = pos_ex.z;
			}
			//PL1の座標系に逆変換
			if (coordinate_inverse) {
				align_param2 param_inv;
				param_inv.corr_p[0] = param_decide.corr_p[0];
				param_inv.corr_p[1] = param_decide.corr_p[1];
				param_inv.corr_p[2] = param_decide.corr_p[2];
				param_inv.x = 0;
				param_inv.y = 0;
				param_inv.z = 0;
				param_inv.z_shrink = 0;
				param_inv.zx_shear = 0;
				param_inv.zy_shear = 0;

				double cos_z, sin_z;
				for (int i = 0; i < 3; i++) {
					cos_z = cos(param_decide.corr_p[i]->z_rot);
					sin_z = sin(param_decide.corr_p[i]->z_rot);
					param_inv.x += param_decide.corr_p[i]->x_shrink*cos_z*(param_decide.corr_p[i]->x) - param_decide.corr_p[i]->y_shrink*sin_z*(param_decide.corr_p[i]->y) + param_decide.corr_p[i]->dx;
					param_inv.y += param_decide.corr_p[i]->x_shrink*sin_z*(param_decide.corr_p[i]->x) + param_decide.corr_p[i]->y_shrink*cos_z*(param_decide.corr_p[i]->y) + param_decide.corr_p[i]->dy;
					param_inv.z += param_decide.corr_p[i]->z + param_decide.corr_p[i]->dz;
					param_inv.z_shrink += 1 / (param_decide.corr_p[i]->z_shrink);
					param_inv.zx_shear += -1 * (param_decide.corr_p[i]->zx_shear);
					param_inv.zy_shear += -1 * (param_decide.corr_p[i]->zy_shear);
				}
				param_inv.x = param_inv.x / 3;
				param_inv.y = param_inv.y / 3;
				param_inv.z = param_inv.z / 3;
				param_inv.z_shrink = param_inv.z_shrink / 3;
				param_inv.zx_shear = param_inv.zx_shear / 3;
				param_inv.zy_shear = param_inv.zx_shear / 3;

				param_inv.Calc_9param_inv();
				std::vector<vxx::base_track_t> base_inv;
				base_inv.push_back(b);
				trans_base(base_inv, &param_inv);
				b = *base_inv.begin();
			}
		}
	}

	std::vector <align_param_3D >Make_3D_Param(std::vector <align_param >&corr, double inner_angle_thr) {

		//delaunay分割
		std::vector<double> x, y;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			x.push_back(itr->x);
			y.push_back(itr->y);
		}

		delaunay::DelaunayTriangulation DT(x, y); // (std::vector<double> x, std::vector<double> y, uint32_t seed_)
		DT.execute(); // (double min_delta = 1e-6, double max_delta = 1e-5, int max_miss_count = 30)
		std::vector<delaunay::Edge> edge = DT.get_edges();

		std::multimap<int, int> edge_map;

		for (auto itr = edge.begin(); itr != edge.end(); itr++) {
			edge_map.insert(std::make_pair(std::min(itr->first, itr->second), std::max(itr->first, itr->second)));

		}
		std::set<std::tuple<int, int, int>>triangle;
		std::set<int> vertex;
		for (auto itr = edge_map.begin(); itr != edge_map.end(); itr++) {
			//itr->firstの点=aを通る三角形の探索
			vertex.clear();
			auto range = edge_map.equal_range(itr->first);
			//aを通りitr->secondの点=bに行く。bのsetを作成
			for (auto res = range.first; res != range.second; res++) {
				vertex.insert(res->second);
			}
			//bを通る線分の探索
			for (auto itr2 = vertex.begin(); itr2 != vertex.end(); itr2++) {
				if (edge_map.count(*itr2) == 0)continue;
				auto range2 = edge_map.equal_range(*itr2);
				//bを通る線分の中からaから始まる線分を探す
				for (auto res = range2.first; res != range2.second; res++) {
					if (vertex.count(res->second) == 1) {
						triangle.insert(std::make_tuple(itr->first, *itr2, res->second));
					}
				}

			}
		}

		std::vector <align_param_3D> ret;
		align_param p_tmp[3];
		int count = 0;
		double cos_z, sin_z;
		for (auto itr = triangle.begin(); itr != triangle.end(); itr++) {
			//printf("delaunay triangle %d %d %d\n", std::get<0>(*itr), std::get<1>(*itr), std::get<2>(*itr));
			align_param_3D param;
			p_tmp[0] = corr[std::get<0>(*itr)];
			p_tmp[1] = corr[std::get<1>(*itr)];
			p_tmp[2] = corr[std::get<2>(*itr)];
			param.id[0] = p_tmp[0].id;
			param.id[1] = p_tmp[1].id;
			param.id[2] = p_tmp[2].id;
			//param.corr_p[0] = &(corr[std::get<0>(*itr)]);
			//param.corr_p[1] = &(corr[std::get<1>(*itr)]);
			//param.corr_p[2] = &(corr[std::get<2>(*itr)]);
			param.z_shrink = 0;
			param.zx_shear = 0;
			param.zy_shear = 0;
			for (int i = 0; i < 3; i++) {
				param.x[0][i] = p_tmp[i].x;
				param.y[0][i] = p_tmp[i].y;
				param.z[0][i] = p_tmp[i].z;
				cos_z = cos(p_tmp[i].z_rot);
				sin_z = sin(p_tmp[i].z_rot);

				param.x[1][i] = p_tmp[i].x_shrink*cos_z*p_tmp[i].x - p_tmp[i].y_shrink*sin_z*p_tmp[i].y + p_tmp[i].dx;
				param.y[1][i] = p_tmp[i].x_shrink*sin_z*p_tmp[i].x + p_tmp[i].y_shrink*cos_z*p_tmp[i].y + p_tmp[i].dy;
				param.z[1][i] = p_tmp[i].z + p_tmp[i].dz;

				param.z_shrink += p_tmp[i].z_shrink;
				//param.zx_shear += p_tmp[i].zx_shear;
				//param.zy_shear += p_tmp[i].zy_shear;
			}
			param.z_shrink = param.z_shrink / 3;
			param.zx_shear = param.zx_shear / 3;
			param.zy_shear = param.zy_shear / 3;

			//内角の最小値> inner_angle_thr
			double vec[3][2];
			vec[0][0] = param.x[0][0] - param.x[0][1];
			vec[1][0] = param.x[0][1] - param.x[0][2];
			vec[2][0] = param.x[0][2] - param.x[0][0];
			vec[0][1] = param.y[0][0] - param.y[0][1];
			vec[1][1] = param.y[0][1] - param.y[0][2];
			vec[2][1] = param.y[0][2] - param.y[0][0];
			double inner_angle[3];
			inner_angle[0] = (vec[0][0] * vec[1][0] + vec[0][1] * vec[1][1])
				/ (sqrt(pow(vec[0][0], 2) + pow(vec[0][1], 2))*sqrt(pow(vec[1][0], 2) + pow(vec[1][1], 2)));
			inner_angle[1] = (vec[1][0] * vec[2][0] + vec[1][1] * vec[2][1])
				/ (sqrt(pow(vec[1][0], 2) + pow(vec[1][1], 2))*sqrt(pow(vec[2][0], 2) + pow(vec[2][1], 2)));
			inner_angle[2] = (vec[2][0] * vec[0][0] + vec[2][1] * vec[0][1])
				/ (sqrt(pow(vec[2][0], 2) + pow(vec[2][1], 2))*sqrt(pow(vec[0][0], 2) + pow(vec[0][1], 2)));
			if (acos(inner_angle[0]) * 180 / M_PI < inner_angle_thr)continue;
			if (acos(inner_angle[1]) * 180 / M_PI < inner_angle_thr)continue;
			if (acos(inner_angle[2]) * 180 / M_PI < inner_angle_thr)continue;

			param.Calc_9param();

			ret.push_back(param);
		}

		return ret;

	}

	//basetrack-alignment mapの対応
	std::vector <std::pair<vxx::base_track_t*, align_param_3D*>>track_affineparam_correspondence(std::vector<vxx::base_track_t>&base, std::vector <align_param_3D> &param) {

		//local alignの視野中心を取り出して、位置でhash
		//local alignの視野中心の作るdelaunay三角形をmapで対応
		std::map<int, Position> view_center;
		std::multimap<int, align_param_3D*>triangles;
		double xmin = 999999, ymin = 999999, hash = 2000;
		Position pos;
		for (auto itr = param.begin(); itr != param.end(); itr++) {
			for (int i = 0; i < 3; i++) {
				pos.id = itr->id[i];
				pos.x = itr->x[0][i];
				pos.y = itr->y[0][i];
				pos.z = itr->z[0][i];
				view_center.insert(std::make_pair(itr->id[i], pos));
				triangles.insert(std::make_pair(itr->id[i], &(*itr)));
				xmin = std::min(itr->x[0][i], xmin);
				ymin = std::min(itr->y[0][i], ymin);
			}
		}
		std::multimap<std::pair<int, int>, Position> view_center_hash;
		std::pair<int, int>id;
		for (auto itr = view_center.begin(); itr != view_center.end(); itr++) {
			id.first = int((itr->second.x - xmin) / hash);
			id.second = int((itr->second.y - ymin) / hash);
			view_center_hash.insert(std::make_pair(id, itr->second));
		}

		std::vector < std::pair<vxx::base_track_t*, align_param_3D*>> ret;
		std::vector< Position> param_cand;
		//std::vector<align_param*> param_cand;
		int loop = 0, ix, iy, count = 0;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			if (count % 100000 == 0) {
				printf("\r search correspond triangles %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
			}
			count++;
			ix = (itr->x - xmin) / hash;
			iy = (itr->y - ymin) / hash;
			loop = 1;
			while (true) {
				param_cand.clear();
				for (int iix = ix - loop; iix <= ix + loop; iix++) {
					for (int iiy = iy - loop; iiy <= iy + loop; iiy++) {
						id.first = iix;
						id.second = iiy;
						if (view_center_hash.count(id) != 0) {
							auto range = view_center_hash.equal_range(id);
							for (auto res = range.first; res != range.second; res++) {
								param_cand.push_back(res->second);
							}
						}
					}
				}
				if (param_cand.size() > 2)break;
				loop++;
			}
			align_param_3D* param2 = search_param(param_cand, *itr, triangles);
			ret.push_back(std::make_pair(&(*itr), param2));
		}
		printf("\r search correspond triangles %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

		return ret;
	}
	align_param_3D* search_param(std::vector<Position> &param, vxx::base_track_t&base, std::multimap<int, align_param_3D*>&triangles) {
		//三角形内部
		//最近接三角形
		double dist = 0;
		std::map<double, Position > dist_map;
		//align_paramを近い順にsort
		for (auto itr = param.begin(); itr != param.end(); itr++) {
			dist = (itr->x - base.x)*(itr->x - base.x) + (itr->y - base.y)*(itr->y - base.y);
			dist_map.insert(std::make_pair(dist, (*itr)));
		}

		double sign[3];
		bool flg = false;
		int id;

		align_param_3D* ret = triangles.begin()->second;
		for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
			if (itr != dist_map.begin())continue;
			//corrmapのID
			id = itr->second.id;
			if (triangles.count(id) == 0) {
				fprintf(stderr, "alignment triangle ID=%d not found\n", id);
				exit(1);
			}
			//idの属する三角形を探索
			auto range = triangles.equal_range(id);
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				sign[0] = (itr2->second->x[0][1] - itr2->second->x[0][0])*(base.y - itr2->second->y[0][1]) - (itr2->second->y[0][1] - itr2->second->y[0][0])*(base.x - itr2->second->x[0][1]);
				sign[1] = (itr2->second->x[0][2] - itr2->second->x[0][1])*(base.y - itr2->second->y[0][2]) - (itr2->second->y[0][2] - itr2->second->y[0][1])*(base.x - itr2->second->x[0][2]);
				sign[2] = (itr2->second->x[0][0] - itr2->second->x[0][2])*(base.y - itr2->second->y[0][0]) - (itr2->second->y[0][0] - itr2->second->y[0][2])*(base.x - itr2->second->x[0][0]);
				//符号が3つとも一致でtrue
				if ((std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2]))) {
					ret = itr2->second;
					flg = true;
					break;
				}
			}
			if (flg)break;
		}
		if (flg) {
			return ret;
		}

		/////////////////////////////
		////求める点が三角形内部にない場合
		//dist(三角形重心-飛跡)が最小になるcorrmapをとってくる
		dist = -1;
		for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
			//corrmapのID
			id = itr->second.id;
			if (triangles.count(id) == 0) {
				fprintf(stderr, "alignment triangle ID=%d not found\n", id);
				exit(1);
			}
			//idの属する三角形を探索
			auto range = triangles.equal_range(id);
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (dist<0 || dist>select_triangle_vale(itr2->second, base)) {
					dist = select_triangle_vale(itr2->second, base);
					ret = itr2->second;
				}
			}
		}
		//printf("point not in trianlge\n");
		return ret;
	}
	double select_triangle_vale(align_param_3D* param, vxx::base_track_t&base) {
		double x, y;
		double dist = 0;
		x = 0; y = 0;
		for (int i = 0; i < 3; i++) {
			x += param->x[0][i];
			y += param->y[0][i];
		}
		x /= 3;
		y /= 3;
		dist = (base.x - x)*(base.x - x) + (base.y - y)*(base.y - y);
		return dist;
	}
	//変換 zshrink補正-->9para変換
	void trans_base_all(std::vector < std::pair<vxx::base_track_t*, align_param_3D*>>&track_pair) {
		std::map<std::tuple<int, int, int>, align_param_3D*> param_map;
		std::multimap<std::tuple<int, int, int>, vxx::base_track_t*>base_map;
		std::tuple<int, int, int>id;
		//三角形ごとにbasetrackをまとめる
		for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
			std::get<0>(id) = itr->second->id[0];
			std::get<1>(id) = itr->second->id[1];
			std::get<2>(id) = itr->second->id[2];
			param_map.insert(std::make_pair(id, itr->second));
			base_map.insert(std::make_pair(id, itr->first));
		}


		//ここで三角形ごとに変換
		int count = 0;
		std::vector<vxx::base_track_t*> t_base;
		for (auto itr = param_map.begin(); itr != param_map.end(); itr++) {
			if (count % 1000 == 0) {
				printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)", count, param_map.size(), count*100. / param_map.size());
			}
			count++;

			t_base.clear();

			if (base_map.count(itr->first) == 0)continue;
			auto range = base_map.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				t_base.push_back(res->second);
			}
			trans_base(t_base, itr->second);

		}
		printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)\n", count, param_map.size(), count*100. / param_map.size());

	}
	void trans_base(std::vector<vxx::base_track_t*>&base, align_param_3D *param) {

		matrix_3D::matrix_33 x_rot_mat(0, param->x_rot), y_rot_mat(1, param->y_rot), z_rot_mat(2, param->z_rot), all_trans(0, 0), shear_mat(0, 0), shrink_mat(0, 0);

		shrink_mat.val[0][0] *= param->x_shrink;
		shrink_mat.val[1][1] *= param->y_shrink;
		//shrink_mat.val[2][2] *= param->z_shrink;
		shear_mat.val[0][1] = param->yx_shear;
		//shear_mat.val[0][2] = param->zx_shear;
		//shear_mat.val[1][2] = param->zy_shear;

		matrix_3D::vector_3D shift, center;
		shift.x = param->dx;
		shift.y = param->dy;
		shift.z = param->dz;

		all_trans.matrix_multiplication(shear_mat);
		all_trans.matrix_multiplication(shrink_mat);
		all_trans.matrix_multiplication(z_rot_mat);
		all_trans.matrix_multiplication(y_rot_mat);
		all_trans.matrix_multiplication(x_rot_mat);

		//all_trans.Print();
		matrix_3D::vector_3D base_p0, base_p1;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_p0.x = (*itr)->x;
			base_p0.y = (*itr)->y;
			base_p0.z = (param->z[0][0] + param->z[0][1] + param->z[0][2]) / 3;

			base_p1.x = (*itr)->x + ((*itr)->ax)*((*itr)->m[1].z - (*itr)->m[0].z) ;
			base_p1.y = (*itr)->y + ((*itr)->ay)*((*itr)->m[1].z - (*itr)->m[0].z) ;
			//角度shrink分はここでかける
			base_p1.z = base_p0.z + ((*itr)->m[1].z - (*itr)->m[0].z) / param->z_shrink;

			//視野中心を原点に移動
			//base_p0 = matrix_3D::addition(base_p0, matrix_3D::const_multiple(center, -1));
			//base_p1 = matrix_3D::addition(base_p1, matrix_3D::const_multiple(center, -1));

			//変換の実行
			base_p0.matrix_multiplication(all_trans);
			base_p0 = matrix_3D::addition(base_p0, shift);
			base_p1.matrix_multiplication(all_trans);
			base_p1 = matrix_3D::addition(base_p1, shift);

			//原点をもとに戻す
			//base_p0 = matrix_3D::addition(base_p0, center);
			//base_p1 = matrix_3D::addition(base_p1, center);

			(*itr)->x = base_p0.x;
			(*itr)->y = base_p0.y;
			(*itr)->z = base_p0.z;

			//printf("ax:%.4lf --> %.4lf\n", (*itr)->ax, (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z));
			//printf("ay:%.4lf --> %.4lf\n", (*itr)->ay, (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z));

			(*itr)->ax = (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z)+param->zx_shear;
			(*itr)->ay = (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z)+param->zy_shear;

		}
	}



}
namespace chain {
	void read_chain(std::string file_path, Chain_file &chain) {
		std::ifstream ifs(file_path);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
		}

		std::string str;
		//header読み込み
		for (int i = 0; i < 10; i++) {
			std::getline(ifs, str);
			if (i == 0) {
				auto str_v = StringSplit_with_tab(str);
				chain.header.n_pos = (stoi(str_v[3]));
				//printf("%d\n", chain.header.n_pos);
			}
			else if (i == 1) {
				auto str_v = StringSplit_with_tab(str);
				for (int j = 0; j < str_v.size(); j++) {
					if (j > 2) {
						chain.header.use_pl.push_back(stoi(str_v[j]));
						//printf("%d ", stoi(str_v[j]));

					}
				}
				//printf("\n");
			}
			else if (i == 2) {
				auto str_v = StringSplit_with_tab(str);
				chain.header.PmPeke = (stoi(str_v[3]));
				//printf("%d\n", chain.header.PmPeke);
			}
		}

		int cnt = 0, nseg_cnt = 0, raw;
		//int debug_cnt = 137566;
		while (std::getline(ifs, str)) {

			if (cnt % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			cnt++;
			//if (cnt >= debug_cnt) printf("\n%s\n", str.c_str());

			auto strs = StringSplit_with_tab(str);
			if (strs[0] == "#") {
				//l2c footer
				if (strs[1] == "N_linklet")chain.footer.n_linklet = std::stoi(strs[3]);
				else if (strs[1] == "N_folded-linklet")chain.footer.n_linklet = std::stoll(strs[3]);
				else if (strs[1] == "N_chain")chain.footer.n_chain = std::stoll(strs[3]);
				else if (strs[1] == "N_gcomm")chain.footer.n_gcomm = std::stoll(strs[3]);
				else if (strs[1] == "N_groot")chain.footer.n_groot = std::stoll(strs[3]);
				else if (strs[1] == "N_gends")chain.footer.n_gends = std::stoll(strs[3]);
				//l2c-x footer
				else if (strs[1] == "N_chain_out")chain.footer.n_chain = std::stoll(strs[3]);
				else if (strs[1] == "N_chain")chain.footer.n_chain = std::stoll(strs[3]);
				else if (strs[1] == "N_group")chain.footer.n_gcomm = std::stoll(strs[3]);
				else if (strs[1] == "N_linklet")chain.footer.n_linklet = std::stoll(strs[3]);
				else {
					fprintf(stderr, "chain footer cannot interpretation\n");
					for (int i = 0; i < strs.size(); i++) {
						printf("%s ", strs[i].c_str());
					}
					printf("\n");
				}
				continue;
			}


			if (strs.size() == 9) {
				Group_header group;
				std::vector<Chain> g_chain;
				group.group_id = stoll(strs[0]);
				//groupid=137571
				group.nchain = stoll(strs[1]);
				for (int i = 0; i < group.nchain; i++) {

					Chain c;
					std::getline(ifs, str);
					//if (cnt >= debug_cnt) printf("%s\n", str.c_str());

					strs = StringSplit_with_tab(str);
					if (strs.size() >= 6) {
						c.chain_id = stoll(strs[0]);
						c.start_pl = stoi(strs[3]);
						c.end_pl = stoi(strs[4]);
						c.nseg = stoi(strs[5]);
					}
					else {
						fprintf(stderr, "chain format err! cannot read file\n");
						fprintf(stderr, "chain header information >= 6 -->%d\n", int(strs.size()));
						for (int j = 0; j < strs.size(); j++) {
							printf("%s ", strs[j].c_str());
						}
						printf("\n");
						throw std::exception();
					}
					std::getline(ifs, str);
					//if (cnt >= debug_cnt) printf("%s\n", str.c_str());

					strs = StringSplit_with_tab(str);
					nseg_cnt = 0;
					for (int j = 0; j < strs.size(); j++) {
						raw = stoi(strs[j]);
						if (raw >= 0) {
							nseg_cnt++;
							c.rawid.push_back(std::make_pair(chain.header.use_pl.at(c.start_pl - 1 + j), raw));
						}
					}
					if (nseg_cnt != c.nseg) {
						fprintf(stderr, "chain format err! cannot read file\n");
						fprintf(stderr, "chain segment information ==%d -->%d\n", c.nseg, nseg_cnt);
						for (int j = 0; j < strs.size(); j++) {
							printf("%s ", strs[j].c_str());
						}
						printf("\n");
						throw std::exception();
					}
					g_chain.push_back(c);
				}
				chain.groups.push_back(group);
				chain.chains.push_back(g_chain);
			}
			else {
				fprintf(stderr, "chain format err! cannot read file\n");
				fprintf(stderr, "group header information == 9 -->%d\n", int(strs.size()));
				for (int i = 0; i < strs.size(); i++) {
					printf("%s ", strs[i].c_str());
				}
				printf("\n");
				throw std::exception();
			}
		}
		auto size1 = eofpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
		if (cnt == 0) {
			fprintf(stderr, "%s no chain!\n", file_path.c_str());
			exit(1);
		}

	}

}

namespace delaunay {

	double dot(Point a, Point b) { return a.x * b.x + a.y * b.y; }
	double cross(Point a, Point b) { return a.x * b.y - a.y * b.x; }

	/*** Edge connecting two points ***/
	Edge make_edge(size_t a, size_t b) { return Edge(std::min(a, b), std::max(a, b)); }


}

void GaussJorden(double in[3][3], double b[3], double c[3]) {


	double a[3][4];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			if (j < 3) {
				a[i][j] = in[i][j];
			}
			else {
				a[i][j] = b[i];
			}
		}
	}
	int N = 3;
	double p, d;         // ピボット係数、ピボット行ｘ係数
	double max, dummy;   // 最大絶対値、入れ替え時ダミー
	int s;

	//元の連立方程式をコンソール出力
   //for (int i = 0; i < N; i++) {
   //	for (int j = 0; j < N; j++)
   //		printf("%+fx%d ", a[i][j], j + 1);
   //	printf("= %+f\n", a[i][N]);
   //}

	for (int k = 0; k < N; k++) {
		// 行入れ替え
		max = 0; s = k;
		for (int j = k; j < N; j++) {
			if (fabs(a[j][k]) > max) {
				max = fabs(a[j][k]);
				s = j;
			}
		}
		if (max == 0) {
			printf("解けない！");
			exit(1);
		}
		for (int j = 0; j <= N; j++) {
			dummy = a[k][j];
			a[k][j] = a[s][j];
			a[s][j] = dummy;
		}

		// ピボット係数
		p = a[k][k];

		// ピボット行を p で除算
		for (int j = k; j < N + 1; j++)
			a[k][j] /= p;

		// ピボット列の掃き出し
		for (int i = 0; i < N; i++) {
			if (i != k) {
				d = a[i][k];
				for (int j = k; j < N + 1; j++)
					a[i][j] -= d * a[k][j];
			}
		}
	}

	// 結果出力
	for (int k = 0; k < N; k++) {
		c[k] = a[k][N];
		//printf("x%d = %f\n", k + 1, a[k][N]);
	}
}

namespace Sharing_file {
	std::vector<Sharing_file> Read_sharing_file_bin(std::string filename) {
		std::vector<Sharing_file> ret;
		std::ifstream ifs(filename, std::ios::binary);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cerr << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cerr << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
		int64_t count = 0;
		Sharing_file t;
		while (ifs.read((char*)& t, sizeof(Sharing_file))) {
			if (count % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			count++;

			ret.emplace_back(t);
		}
		auto size1 = eofpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
		if (count == 0) {
			fprintf(stderr, "%s no data!\n", filename.c_str());
			exit(1);
		}
		return ret;
	}
	std::vector<Sharing_file> Read_sharing_file_txt(std::string filename) {
		std::vector<Sharing_file> ret;
		std::ifstream ifs(filename);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
		int64_t count = 0;
		Sharing_file t;
		//while (ifs.read((char*)& t, sizeof(Sharing_file))) {
		while (ifs >> t.pl >> t.ecc_id >> t.oss_id >> t.fixedwall_id >> t.trackerwall_id >> t.spotid >> t.zone[0] >> t.rawid[0] >> t.zone[1] >> t.rawid[1] >> t.unix_time >> t.tracker_track_id >> t.babymind_bunch >> t.entry_in_daily_file >> t.babymind_nplane >> t.charge>> t.babymind_momentum >> t.chi2_shifter[0] >> t.chi2_shifter[1] >> t.chi2_shifter[2] >> t.chi2_shifter[3] >> t.eventid >> t.track_type >> t.ecc_track_type) {
			if (count % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			count++;

			ret.emplace_back(t);
		}
		auto size1 = eofpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
		if (count == 0) {
			fprintf(stderr, "%s no data!\n", filename.c_str());
			exit(1);
		}
		return ret;
	}
	void Write_sharing_file_bin(std::string filename, std::vector<Sharing_file>&sharing_file_v) {
		std::ofstream ofs(filename, std::ios::binary);
		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (sharing_file_v.size() == 0) {
			fprintf(stderr, "target data ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		else {
			int64_t count = 0;
			int64_t max = sharing_file_v.size();

			for (int i = 0; i < max; i++) {
				if (count % 10000 == 0) {
					std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / sharing_file_v.size() << "%%";
				}
				count++;
				ofs.write((char*)& sharing_file_v[i], sizeof(Sharing_file));
			}
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / sharing_file_v.size() << "%%" << std::endl;
		}
	}
	void Write_sharing_file_txt(std::string filename, std::vector<Sharing_file>& sharing_file_v)
	{
		std::ofstream ofs(filename);
		if (!ofs) {
			//file open s
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (sharing_file_v.size() == 0) {
			fprintf(stderr, "target data ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		else {
			int64_t count = 0;
			int64_t max = sharing_file_v.size();

			for (int i = 0; i < max; i++) {
				if (count % 10000 == 0) {
					std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / sharing_file_v.size() << "%%";
				}
				count++;
				ofs << std::right << std::fixed
					<< std::setw(3) << std::setprecision(0) << sharing_file_v[i].pl << " "
					<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].ecc_id << " "
					<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].oss_id << " "
					<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].fixedwall_id << " "
					<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].trackerwall_id << " "
					<< std::setw(4) << std::setprecision(0) << sharing_file_v[i].spotid << " "
					<< std::setw(2) << std::setprecision(0) << sharing_file_v[i].zone[0] << " "
					<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].rawid[0] << " "
					<< std::setw(2) << std::setprecision(0) << sharing_file_v[i].zone[1] << " "
					<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].rawid[1] << " "
					<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].unix_time << " "
					<< std::setw(3) << std::setprecision(0) << sharing_file_v[i].tracker_track_id << " "
					<< std::setw(3) << std::setprecision(0) << sharing_file_v[i].babymind_bunch << " "
					<< std::setw(6) << std::setprecision(0) << sharing_file_v[i].entry_in_daily_file << " "
					<< std::setw(3) << std::setprecision(0) << sharing_file_v[i].babymind_nplane << " "
					<< std::setw(3) << std::setprecision(0) << sharing_file_v[i].charge << " "
					<< std::setw(7) << std::setprecision(1) << sharing_file_v[i].babymind_momentum << " "
					<< std::setw(7) << std::setprecision(3) << sharing_file_v[i].chi2_shifter[0] << " "
					<< std::setw(7) << std::setprecision(3) << sharing_file_v[i].chi2_shifter[1] << " "
					<< std::setw(7) << std::setprecision(3) << sharing_file_v[i].chi2_shifter[2] << " "
					<< std::setw(7) << std::setprecision(3) << sharing_file_v[i].chi2_shifter[3] << " "
					<< std::setw(7) << std::setprecision(0) << sharing_file_v[i].eventid << " "
					<< std::setw(2) << std::setprecision(0) << sharing_file_v[i].track_type << " "
					<< std::setw(2) << std::setprecision(0) << sharing_file_v[i].ecc_track_type
					<< std::endl;

			}
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / sharing_file_v.size() << "%%" << std::endl;
		}
	}
	std::string Sharing_file::Print_content() {
		std::stringstream ret;

		ret << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << pl << " "
			<< std::setw(10) << std::setprecision(0) << ecc_id << " "
			<< std::setw(10) << std::setprecision(0) << oss_id << " "
			<< std::setw(10) << std::setprecision(0) << fixedwall_id << " "
			<< std::setw(10) << std::setprecision(0) << trackerwall_id << " "
			<< std::setw(4) << std::setprecision(0) << spotid << " "
			<< std::setw(2) << std::setprecision(0) << zone[0] << " "
			<< std::setw(10) << std::setprecision(0) << rawid[0] << " "
			<< std::setw(2) << std::setprecision(0) << zone[1] << " "
			<< std::setw(10) << std::setprecision(0) << rawid[1] << " "
			<< std::setw(10) << std::setprecision(0) << unix_time << " "
			<< std::setw(3) << std::setprecision(0) << tracker_track_id << " "
			<< std::setw(3) << std::setprecision(0) << babymind_bunch << " "
			<< std::setw(6) << std::setprecision(0) << entry_in_daily_file << " "
			<< std::setw(3) << std::setprecision(0) << babymind_nplane << " "
			<< std::setw(3) << std::setprecision(0) << charge << " " 
			<< std::setw(7) << std::setprecision(1) << babymind_momentum << " "
			<< std::setw(7) << std::setprecision(3) << chi2_shifter[0] << " "
			<< std::setw(7) << std::setprecision(3) << chi2_shifter[1] << " "
			<< std::setw(7) << std::setprecision(3) << chi2_shifter[2] << " "
			<< std::setw(7) << std::setprecision(3) << chi2_shifter[3] << " "
			<< std::setw(7) << std::setprecision(0) << eventid << " "
			<< std::setw(2) << std::setprecision(0) << track_type << " "
			<< std::setw(2) << std::setprecision(0) << ecc_track_type;
		return ret.str();
	}
	std::vector<Sharing_file> Read_sharing_file_extension(std::string filename) {
		std::vector<Sharing_file> ret;
		std::string extension;
		extension = filename.substr(filename.size() - 7, 7);
		if (extension == "ninjasf") {
			ret = Read_sharing_file_bin(filename);
		}
		else {
			ret = Read_sharing_file_txt(filename);
		}
		return ret;
	}

}
namespace Momentum_recon {
	void Write_Event_information_header(std::ofstream &ofs, Event_information&ev) {
		int chian_num = ev.chains.size();
		int true_chian_num = ev.true_chains.size();

		ofs.write((char*)& ev.groupid, sizeof(int));
		ofs.write((char*)& ev.unix_time, sizeof(int));
		ofs.write((char*)& ev.tracker_track_id, sizeof(int));
		ofs.write((char*)& ev.entry_in_daily_file, sizeof(int));
		ofs.write((char*)& ev.vertex_pl, sizeof(int));
		ofs.write((char*)& ev.ECC_id, sizeof(int));
		ofs.write((char*)& ev.vertex_material, sizeof(int));
		ofs.write((char*)& ev.vertex_position, sizeof(double) * 3);
		ofs.write((char*)& ev.true_vertex_position, sizeof(double) * 3);
		ofs.write((char*)& ev.weight, sizeof(double));
		ofs.write((char*)& ev.nu_energy, sizeof(double));
		ofs.write((char*)& ev.nu_ax, sizeof(double));
		ofs.write((char*)& ev.nu_ay, sizeof(double));
		ofs.write((char*)& chian_num, sizeof(int));
		ofs.write((char*)& true_chian_num, sizeof(int));

	}
	bool Read_Event_information_header(std::ifstream &ifs, Event_information&ev, int &chian_num, int &true_chian_num) {

		if (!ifs.read((char*)& ev.groupid, sizeof(int)))return false;
		if (!ifs.read((char*)& ev.unix_time, sizeof(int)))return false;
		if (!ifs.read((char*)& ev.tracker_track_id, sizeof(int)))return false;
		if (!ifs.read((char*)& ev.entry_in_daily_file, sizeof(int)))return false;
		if (!ifs.read((char*)& ev.vertex_pl, sizeof(int)))return false;
		if (!ifs.read((char*)& ev.ECC_id, sizeof(int)))return false;
		if (!ifs.read((char*)& ev.vertex_material, sizeof(int)))return false;
		if (!ifs.read((char*)& ev.vertex_position, sizeof(double) * 3))return false;
		if (!ifs.read((char*)& ev.true_vertex_position, sizeof(double) * 3))return false;
		if (!ifs.read((char*)& ev.weight, sizeof(double)))return false;
		if (!ifs.read((char*)& ev.nu_energy, sizeof(double)))return false;
		if (!ifs.read((char*)& ev.nu_ax, sizeof(double)))return false;
		if (!ifs.read((char*)& ev.nu_ay, sizeof(double)))return false;
		if (!ifs.read((char*)& chian_num, sizeof(int)))return false;
		if (!ifs.read((char*)& true_chian_num, sizeof(int)))return false;

		return true;
	}

	void Write_mom_chain_header(std::ofstream &ofs, Mom_chain&mom_chain) {
		int base_num = mom_chain.base.size();
		int base_pair_num = mom_chain.base_pair.size();

		ofs.write((char*)& mom_chain.chainid, sizeof(int));
		ofs.write((char*)& mom_chain.stop_flg, sizeof(int));
		ofs.write((char*)& mom_chain.particle_flg, sizeof(int));
		ofs.write((char*)& mom_chain.direction, sizeof(int));
		ofs.write((char*)& mom_chain.charge_sign, sizeof(int));
		ofs.write((char*)& mom_chain.ecc_range_mom, sizeof(double) * 2);
		ofs.write((char*)& mom_chain.ecc_mcs_mom, sizeof(double) * 2);
		ofs.write((char*)& mom_chain.bm_range_mom, sizeof(double));
		ofs.write((char*)& mom_chain.bm_curvature_mom, sizeof(double));
		ofs.write((char*)& mom_chain.ecc_range_mom_error, sizeof(double) * 2 * 2);
		ofs.write((char*)& mom_chain.ecc_mcs_mom_error, sizeof(double) * 2 * 2);
		ofs.write((char*)& mom_chain.bm_range_mom_error, sizeof(double) * 2);
		ofs.write((char*)& mom_chain.bm_curvature_mom_error, sizeof(double) * 2);
		ofs.write((char*)& mom_chain.muon_likelihood, sizeof(double));
		ofs.write((char*)& mom_chain.proton_likelihood, sizeof(double));
		ofs.write((char*)& base_num, sizeof(int));
		ofs.write((char*)& base_pair_num, sizeof(int));
	}
	bool Read_mom_chain_header(std::ifstream &ifs, Mom_chain&mom_chain, int &base_num, int &base_pair_num) {
		if (!ifs.read((char*)& mom_chain.chainid, sizeof(int)))return false;
		if (!ifs.read((char*)& mom_chain.stop_flg, sizeof(int)))return false;
		if (!ifs.read((char*)& mom_chain.particle_flg, sizeof(int)))return false;
		if (!ifs.read((char*)& mom_chain.direction, sizeof(int)))return false;
		if (!ifs.read((char*)& mom_chain.charge_sign, sizeof(int)))return false;
		if (!ifs.read((char*)& mom_chain.ecc_range_mom, sizeof(double) * 2))return false;
		if (!ifs.read((char*)& mom_chain.ecc_mcs_mom, sizeof(double) * 2))return false;
		if (!ifs.read((char*)& mom_chain.bm_range_mom, sizeof(double)))return false;
		if (!ifs.read((char*)& mom_chain.bm_curvature_mom, sizeof(double)))return false;
		if (!ifs.read((char*)& mom_chain.ecc_range_mom_error, sizeof(double) * 2 * 2))return false;
		if (!ifs.read((char*)& mom_chain.ecc_mcs_mom_error, sizeof(double) * 2 * 2))return false;
		if (!ifs.read((char*)& mom_chain.bm_range_mom_error, sizeof(double) * 2))return false;
		if (!ifs.read((char*)& mom_chain.bm_curvature_mom_error, sizeof(double) * 2))return false;
		if (!ifs.read((char*)& mom_chain.muon_likelihood, sizeof(double)))return false;
		if (!ifs.read((char*)& mom_chain.proton_likelihood, sizeof(double)))return false;
		if (!ifs.read((char*)& base_num, sizeof(int)))return false;
		if (!ifs.read((char*)& base_pair_num, sizeof(int)))return false;
		mom_chain.base.reserve(base_num);
		mom_chain.base_pair.reserve(base_pair_num);
		return true;
	}

	void Write_Event_information_bin(std::string filename, std::vector<Event_information>&ev_v) {
		std::ofstream ofs(filename, std::ios::binary);
		if (!ofs) {
			//file open 失敗
			fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
			exit(1);
		}
		if (ev_v.size() == 0) {
			fprintf(stderr, "target data ... null\n");
			fprintf(stderr, "File[%s] has no text\n", filename.c_str());
		}
		else {
			int64_t count = 0;
			int64_t max = ev_v.size();
			int base_num, base_pair_num;
			int chain_num, true_chain_num;

			for (int i = 0; i < max; i++) {
				if (count % 100 == 0) {
					std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%%";
				}
				count++;
				Write_Event_information_header(ofs, ev_v[i]);
				chain_num = ev_v[i].chains.size();
				true_chain_num = ev_v[i].true_chains.size();
				for (int j = 0; j < chain_num; j++) {
					base_num = ev_v[i].chains[j].base.size();
					base_pair_num = ev_v[i].chains[j].base_pair.size();
					Write_mom_chain_header(ofs, ev_v[i].chains[j]);

					for (int k = 0; k < base_num; k++) {
						ofs.write((char*)& ev_v[i].chains[j].base[k], sizeof(Mom_basetrack));
					}
					for (int k = 0; k < base_pair_num; k++) {
						ofs.write((char*)& ev_v[i].chains[j].base_pair[k].first, sizeof(Mom_basetrack));
						ofs.write((char*)& ev_v[i].chains[j].base_pair[k].second, sizeof(Mom_basetrack));
					}
				}
				for (int j = 0; j < true_chain_num; j++) {
					base_num = ev_v[i].true_chains[j].base.size();
					base_pair_num = ev_v[i].true_chains[j].base_pair.size();
					Write_mom_chain_header(ofs, ev_v[i].true_chains[j]);

					for (int k = 0; k < base_num; k++) {
						ofs.write((char*)& ev_v[i].true_chains[j].base[k], sizeof(Mom_basetrack));
					}
					for (int k = 0; k < base_pair_num; k++) {
						ofs.write((char*)& ev_v[i].true_chains[j].base_pair[k].first, sizeof(Mom_basetrack));
						ofs.write((char*)& ev_v[i].true_chains[j].base_pair[k].second, sizeof(Mom_basetrack));
					}
				}
			}
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%%" << std::endl;
		}
	}
	std::vector<Event_information> Read_Event_information_bin(std::string filename) {
		std::vector<Event_information> ret;
		std::ifstream ifs(filename, std::ios::binary);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
		int64_t count = 0;
		Mom_chain t;
		Mom_basetrack m;
		std::pair<Mom_basetrack, Mom_basetrack> p;
		int base_num, base_pair_num;
		int chain_num, true_chain_num;
		int header[5];
		double mom_recon;
		Event_information ev;
		while (Read_Event_information_header(ifs, ev, chain_num, true_chain_num)) {

			if (count % 100 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			count++;
			for (int i_ch = 0; i_ch < chain_num; i_ch++) {
				if (Read_mom_chain_header(ifs, t, base_num, base_pair_num)) {

					t.base.clear();
					t.base_pair.clear();
					t.base.reserve(base_num);
					t.base_pair.reserve(base_pair_num);
					for (int j = 0; j < base_num; j++) {
						ifs.read((char*)& m, sizeof(Mom_basetrack));
						t.base.push_back(m);
					}
					for (int j = 0; j < base_pair_num; j++) {
						ifs.read((char*)& p.first, sizeof(Mom_basetrack));
						ifs.read((char*)& p.second, sizeof(Mom_basetrack));
						t.base_pair.push_back(p);
					}
					ev.chains.push_back(t);
				}
			}
			for (int i_ch = 0; i_ch < true_chain_num; i_ch++) {
				if (Read_mom_chain_header(ifs, t, base_num, base_pair_num)) {
					t.base.clear();
					t.base_pair.clear();
					t.base.reserve(base_num);
					t.base_pair.reserve(base_pair_num);
					for (int j = 0; j < base_num; j++) {
						ifs.read((char*)& m, sizeof(Mom_basetrack));
						t.base.push_back(m);
					}
					for (int j = 0; j < base_pair_num; j++) {
						ifs.read((char*)& p.first, sizeof(Mom_basetrack));
						ifs.read((char*)& p.second, sizeof(Mom_basetrack));
						t.base_pair.push_back(p);
					}
					ev.true_chains.push_back(t);
				}
			}
			ret.push_back(ev);
			ev.chains.clear();
			ev.true_chains.clear();
		}

		auto size1 = eofpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
		if (count == 0) {
			fprintf(stderr, "%s no data!\n", filename.c_str());
			exit(1);
		}
		return ret;

	}

	void Write_Event_information_txt(std::string filename, std::vector<Event_information>&ev_v) {

		std::ofstream ofs(filename);
		int count = 0, all = ev_v.size();
		for (auto &ev : ev_v) {
			if (count % 100 == 0) {
				fprintf(stderr, "\r write group %d/%d(%4.1lf%%)", count, all, count*100. / all);
			}
			count++;
			//event inforamtion header 書き出し
			ofs << std::right << std::fixed
				<< std::setw(10) << std::setprecision(0) << ev.groupid << " "
				<< std::setw(10) << std::setprecision(0) << ev.unix_time << " "
				<< std::setw(3) << std::setprecision(0) << ev.tracker_track_id << " "
				<< std::setw(5) << std::setprecision(0) << ev.entry_in_daily_file << " "
				<< std::setw(3) << std::setprecision(0) << ev.vertex_pl << " "
				<< std::setw(2) << std::setprecision(0) << ev.ECC_id << " "
				<< std::setw(2) << std::setprecision(0) << ev.vertex_material << " "
				<< std::setw(8) << std::setprecision(1) << ev.vertex_position[0] << " "
				<< std::setw(8) << std::setprecision(1) << ev.vertex_position[1] << " "
				<< std::setw(8) << std::setprecision(1) << ev.vertex_position[2] << " "
				<< std::setw(8) << std::setprecision(1) << ev.true_vertex_position[0] << " "
				<< std::setw(8) << std::setprecision(1) << ev.true_vertex_position[1] << " "
				<< std::setw(8) << std::setprecision(1) << ev.true_vertex_position[2] << " "
				<< std::setw(7) << std::setprecision(1) << ev.nu_energy << " "
				<< std::setw(7) << std::setprecision(4) << ev.nu_ax << " "
				<< std::setw(7) << std::setprecision(4) << ev.nu_ay << " "
				<< std::setw(5) << std::setprecision(0) << ev.chains.size() << " "
				<< std::setw(5) << std::setprecision(0) << ev.true_chains.size() << std::endl;
			for (int i = 0; i < ev.chains.size(); i++) {
				ofs << std::right << std::fixed
					<< std::setw(10) << std::setprecision(0) << ev.chains[i].chainid << " "
					<< std::setw(5) << std::setprecision(0) << ev.chains[i].stop_flg << " "
					<< std::setw(5) << std::setprecision(0) << ev.chains[i].particle_flg << " "
					<< std::setw(3) << std::setprecision(0) << ev.chains[i].direction << " "
					<< std::setw(2) << std::setprecision(0) << ev.chains[i].charge_sign << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_range_mom[0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_range_mom[1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_mcs_mom[0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_mcs_mom[1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].bm_range_mom << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].bm_curvature_mom << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_range_mom_error[0][0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_range_mom_error[0][1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_range_mom_error[1][0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_range_mom_error[1][1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_mcs_mom_error[0][0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_mcs_mom_error[0][1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_mcs_mom_error[1][0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].ecc_mcs_mom_error[1][1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].bm_range_mom_error[0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].bm_range_mom_error[1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].bm_curvature_mom_error[0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.chains[i].bm_curvature_mom_error[1] << " "
					<< std::setw(7) << std::setprecision(4) << ev.chains[i].muon_likelihood << " "
					<< std::setw(7) << std::setprecision(4) << ev.chains[i].proton_likelihood << " "
					<< std::setw(3) << std::setprecision(0) << ev.chains[i].base.size() << " "
					<< std::setw(3) << std::setprecision(0) << ev.chains[i].base_pair.size() << std::endl;

				for (auto &b : ev.chains[i].base) {
					ofs << std::right << std::fixed
						<< std::setw(4) << std::setprecision(0) << b.pl << " "
						<< std::setw(10) << std::setprecision(0) << b.rawid << " "
						<< std::setw(7) << std::setprecision(4) << b.ax << " "
						<< std::setw(7) << std::setprecision(4) << b.ay << " "
						<< std::setw(8) << std::setprecision(1) << b.x << " "
						<< std::setw(8) << std::setprecision(1) << b.y << " "
						<< std::setw(8) << std::setprecision(1) << b.z << " "
						<< std::setw(3) << std::setprecision(0) << b.m[0].zone << " "
						<< std::setw(4) << std::setprecision(0) << b.m[0].view << " "
						<< std::setw(3) << std::setprecision(0) << b.m[0].imager << " "
						<< std::setw(7) << std::setprecision(0) << b.m[0].ph << " "
						<< std::setw(5) << std::setprecision(0) << b.m[0].pixelnum << " "
						<< std::setw(5) << std::setprecision(0) << b.m[0].hitnum << " "
						<< std::setw(3) << std::setprecision(0) << b.m[1].zone << " "
						<< std::setw(4) << std::setprecision(0) << b.m[1].view << " "
						<< std::setw(3) << std::setprecision(0) << b.m[1].imager << " "
						<< std::setw(7) << std::setprecision(0) << b.m[1].ph << " "
						<< std::setw(5) << std::setprecision(0) << b.m[1].pixelnum << " "
						<< std::setw(5) << std::setprecision(0) << b.m[1].hitnum << std::endl;
				}
				for (auto &p : ev.chains[i].base_pair) {
					ofs << std::right << std::fixed
						<< std::setw(4) << std::setprecision(0) << p.first.pl << " "
						<< std::setw(10) << std::setprecision(0) << p.first.rawid << " "
						<< std::setw(4) << std::setprecision(0) << p.second.pl << " "
						<< std::setw(10) << std::setprecision(0) << p.second.rawid << " "
						<< std::setw(7) << std::setprecision(4) << p.first.ax << " "
						<< std::setw(7) << std::setprecision(4) << p.first.ay << " "
						<< std::setw(8) << std::setprecision(1) << p.first.x << " "
						<< std::setw(8) << std::setprecision(1) << p.first.y << " "
						<< std::setw(8) << std::setprecision(1) << p.first.z << " "
						<< std::setw(7) << std::setprecision(4) << p.second.ax << " "
						<< std::setw(7) << std::setprecision(4) << p.second.ay << " "
						<< std::setw(8) << std::setprecision(1) << p.second.x << " "
						<< std::setw(8) << std::setprecision(1) << p.second.y << " "
						<< std::setw(8) << std::setprecision(1) << p.second.z << std::endl;
				}

			}

			for (int i = 0; i < ev.true_chains.size(); i++) {
				ofs << std::right << std::fixed
					<< std::setw(10) << std::setprecision(0) << ev.true_chains[i].chainid << " "
					<< std::setw(5) << std::setprecision(0) << ev.true_chains[i].stop_flg << " "
					<< std::setw(5) << std::setprecision(0) << ev.true_chains[i].particle_flg << " "
					<< std::setw(3) << std::setprecision(0) << ev.true_chains[i].direction << " "
					<< std::setw(2) << std::setprecision(0) << ev.true_chains[i].charge_sign << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_range_mom[0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_range_mom[1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_mcs_mom[0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_mcs_mom[1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].bm_range_mom << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].bm_curvature_mom << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_range_mom_error[0][0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_range_mom_error[0][1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_range_mom_error[1][0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_range_mom_error[1][1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_mcs_mom_error[0][0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_mcs_mom_error[0][1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_mcs_mom_error[1][0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].ecc_mcs_mom_error[1][1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].bm_range_mom_error[0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].bm_range_mom_error[1] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].bm_curvature_mom_error[0] << " "
					<< std::setw(7) << std::setprecision(1) << ev.true_chains[i].bm_curvature_mom_error[1] << " "
					<< std::setw(7) << std::setprecision(4) << ev.true_chains[i].muon_likelihood << " "
<< std::setw(7) << std::setprecision(4) << ev.true_chains[i].proton_likelihood << " "
<< std::setw(3) << std::setprecision(0) << ev.true_chains[i].base.size() << " "
<< std::setw(3) << std::setprecision(0) << ev.true_chains[i].base_pair.size() << std::endl;

for (auto &b : ev.true_chains[i].base) {
	ofs << std::right << std::fixed
		<< std::setw(4) << std::setprecision(0) << b.pl << " "
		<< std::setw(10) << std::setprecision(0) << b.rawid << " "
		<< std::setw(7) << std::setprecision(4) << b.ax << " "
		<< std::setw(7) << std::setprecision(4) << b.ay << " "
		<< std::setw(8) << std::setprecision(1) << b.x << " "
		<< std::setw(8) << std::setprecision(1) << b.y << " "
		<< std::setw(8) << std::setprecision(1) << b.z << " "
		<< std::setw(3) << std::setprecision(0) << b.m[0].zone << " "
		<< std::setw(4) << std::setprecision(0) << b.m[0].view << " "
		<< std::setw(3) << std::setprecision(0) << b.m[0].imager << " "
		<< std::setw(7) << std::setprecision(0) << b.m[0].ph << " "
		<< std::setw(5) << std::setprecision(0) << b.m[0].pixelnum << " "
		<< std::setw(5) << std::setprecision(0) << b.m[0].hitnum << " "
		<< std::setw(3) << std::setprecision(0) << b.m[1].zone << " "
		<< std::setw(4) << std::setprecision(0) << b.m[1].view << " "
		<< std::setw(3) << std::setprecision(0) << b.m[1].imager << " "
		<< std::setw(7) << std::setprecision(0) << b.m[1].ph << " "
		<< std::setw(5) << std::setprecision(0) << b.m[1].pixelnum << " "
		<< std::setw(5) << std::setprecision(0) << b.m[1].hitnum << std::endl;
}
for (auto &p : ev.true_chains[i].base_pair) {
	ofs << std::right << std::fixed
		<< std::setw(4) << std::setprecision(0) << p.first.pl << " "
		<< std::setw(10) << std::setprecision(0) << p.first.rawid << " "
		<< std::setw(4) << std::setprecision(0) << p.second.pl << " "
		<< std::setw(10) << std::setprecision(0) << p.second.rawid << " "
		<< std::setw(7) << std::setprecision(4) << p.first.ax << " "
		<< std::setw(7) << std::setprecision(4) << p.first.ay << " "
		<< std::setw(8) << std::setprecision(1) << p.first.x << " "
		<< std::setw(8) << std::setprecision(1) << p.first.y << " "
		<< std::setw(8) << std::setprecision(1) << p.first.z << " "
		<< std::setw(7) << std::setprecision(4) << p.second.ax << " "
		<< std::setw(7) << std::setprecision(4) << p.second.ay << " "
		<< std::setw(8) << std::setprecision(1) << p.second.x << " "
		<< std::setw(8) << std::setprecision(1) << p.second.y << " "
		<< std::setw(8) << std::setprecision(1) << p.second.z << std::endl;
}

			}

		}
		fprintf(stderr, "\r write group %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

	}
	std::vector<Event_information> Read_Event_information_txt(std::string filename) {
		std::vector<Event_information> ret;
		std::ifstream ifs(filename);
		//filesize取得
		ifs.seekg(0, std::ios::end);
		int64_t eofpos = ifs.tellg();
		ifs.clear();
		ifs.seekg(0, std::ios::beg);
		int64_t begpos = ifs.tellg();
		int64_t nowpos = ifs.tellg();
		int64_t size2 = eofpos - begpos;
		int64_t GB = size2 / (1000 * 1000 * 1000);
		int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
		int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
		int64_t count = 0;
		Mom_chain t;
		Mom_basetrack m;
		Event_information ev;
		std::pair<Mom_basetrack, Mom_basetrack> p;
		int base_num, base_pair_num;
		int chain_num, true_chain_num;
		while (ifs >> ev.groupid >> ev.unix_time >> ev.tracker_track_id >> ev.entry_in_daily_file >> ev.vertex_pl >> ev.ECC_id >> ev.vertex_material
			>> ev.vertex_position[0] >> ev.vertex_position[1] >> ev.vertex_position[2]
			>> ev.true_vertex_position[0] >> ev.true_vertex_position[1] >> ev.true_vertex_position[2]
			>> ev.nu_energy >> ev.nu_ax >> ev.nu_ay >> chain_num >> true_chain_num) {
			if (count % 100 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
			count++;
			for (int i = 0; i < chain_num; i++) {
				t.base.clear();
				t.base_pair.clear();
				t.base.reserve(base_num);
				t.base_pair.reserve(base_pair_num);
				ifs >> t.chainid >> t.stop_flg >> t.particle_flg >> t.direction >> t.charge_sign
					>> t.ecc_range_mom[0] >> t.ecc_range_mom[1] >> t.ecc_mcs_mom[0] >> t.ecc_mcs_mom[1]
					>> t.bm_range_mom >> t.bm_curvature_mom
					>> t.ecc_range_mom_error[0][0] >> t.ecc_range_mom_error[0][1] >> t.ecc_range_mom_error[1][0] >> t.ecc_range_mom_error[1][1]
					>> t.ecc_mcs_mom_error[0][0] >> t.ecc_mcs_mom_error[0][1] >> t.ecc_mcs_mom_error[1][0] >> t.ecc_mcs_mom_error[1][1]
					>> t.bm_range_mom_error[0] >> t.bm_range_mom_error[1]
					>> t.bm_curvature_mom_error[0] >> t.bm_curvature_mom_error[1]
					>> t.muon_likelihood >> t.proton_likelihood
					>> base_num >> base_pair_num;
				for (int i = 0; i < base_num; i++) {
					ifs >> m.pl >> m.rawid >> m.ax >> m.ay >> m.x >> m.y >> m.z
						>> m.m[0].zone >> m.m[0].view >> m.m[0].imager >> m.m[0].ph >> m.m[0].pixelnum >> m.m[0].hitnum
						>> m.m[1].zone >> m.m[1].view >> m.m[1].imager >> m.m[1].ph >> m.m[1].pixelnum >> m.m[1].hitnum;
					t.base.push_back(m);
				}
				for (int i = 0; i < base_pair_num; i++) {
					ifs >> p.first.pl >> p.first.rawid >> p.second.pl >> p.second.rawid
						>> p.first.ax >> p.first.ay >> p.first.x >> p.first.y >> p.first.z
						>> p.second.ax >> p.second.ay >> p.second.x >> p.second.y >> p.second.z;
					t.base_pair.push_back(p);
				}
				input_basetrack_information(t.base, t.base_pair);
				ev.chains.push_back(t);
			}
			for (int i = 0; i < true_chain_num; i++) {
				t.base.clear();
				t.base_pair.clear();
				t.base.reserve(base_num);
				t.base_pair.reserve(base_pair_num);
				ifs >> t.chainid >> t.stop_flg >> t.particle_flg >> t.direction >> t.charge_sign
					>> t.ecc_range_mom[0] >> t.ecc_range_mom[1] >> t.ecc_mcs_mom[0] >> t.ecc_mcs_mom[1]
					>> t.bm_range_mom >> t.bm_curvature_mom
					>> t.ecc_range_mom_error[0][0] >> t.ecc_range_mom_error[0][1] >> t.ecc_range_mom_error[1][0] >> t.ecc_range_mom_error[1][1]
					>> t.ecc_mcs_mom_error[0][0] >> t.ecc_mcs_mom_error[0][1] >> t.ecc_mcs_mom_error[1][0] >> t.ecc_mcs_mom_error[1][1]
					>> t.bm_range_mom_error[0] >> t.bm_range_mom_error[1]
					>> t.bm_curvature_mom_error[0] >> t.bm_curvature_mom_error[1]
					>> t.muon_likelihood >> t.proton_likelihood
					>> base_num >> base_pair_num;
				for (int i = 0; i < base_num; i++) {
					ifs >> m.pl >> m.rawid >> m.ax >> m.ay >> m.x >> m.y >> m.z
						>> m.m[0].zone >> m.m[0].view >> m.m[0].imager >> m.m[0].ph >> m.m[0].pixelnum >> m.m[0].hitnum
						>> m.m[1].zone >> m.m[1].view >> m.m[1].imager >> m.m[1].ph >> m.m[1].pixelnum >> m.m[1].hitnum;
					t.base.push_back(m);
				}
				for (int i = 0; i < base_pair_num; i++) {
					ifs >> p.first.pl >> p.first.rawid >> p.second.pl >> p.second.rawid
						>> p.first.ax >> p.first.ay >> p.first.x >> p.first.y >> p.first.z
						>> p.second.ax >> p.second.ay >> p.second.x >> p.second.y >> p.second.z;
					t.base_pair.push_back(p);
				}
				input_basetrack_information(t.base, t.base_pair);
				ev.true_chains.push_back(t);
			}
			ret.push_back(ev);

			ev.chains.clear();
			ev.true_chains.clear();
		}
		auto size1 = eofpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
		if (count == 0) {
			fprintf(stderr, "%s no data!\n", filename.c_str());
			exit(1);
		}
		return ret;
	}

	std::vector<Event_information> Read_Event_information_bin_block(std::ifstream &ifs,int max_event_num) {
		std::vector<Event_information> ret;
		int64_t count = 0;
		Mom_chain t;
		Mom_basetrack m;
		std::pair<Mom_basetrack, Mom_basetrack> p;
		int base_num, base_pair_num;
		int chain_num, true_chain_num;
		int header[5];
		double mom_recon;
		Event_information ev;
		while (Read_Event_information_header(ifs, ev, chain_num, true_chain_num)) {
			for (int i_ch = 0; i_ch < chain_num; i_ch++) {
				if (Read_mom_chain_header(ifs, t, base_num, base_pair_num)) {
					t.base.clear();
					t.base_pair.clear();
					t.base.reserve(base_num);
					t.base_pair.reserve(base_pair_num);
					for (int j = 0; j < base_num; j++) {
						ifs.read((char*)& m, sizeof(Mom_basetrack));
						t.base.push_back(m);
					}
					for (int j = 0; j < base_pair_num; j++) {
						ifs.read((char*)& p.first, sizeof(Mom_basetrack));
						ifs.read((char*)& p.second, sizeof(Mom_basetrack));
						t.base_pair.push_back(p);
					}
					ev.chains.push_back(t);
				}
			}
			for (int i_ch = 0; i_ch < true_chain_num; i_ch++) {
				if (Read_mom_chain_header(ifs, t, base_num, base_pair_num)) {
					t.base.clear();
					t.base_pair.clear();
					t.base.reserve(base_num);
					t.base_pair.reserve(base_pair_num);
					for (int j = 0; j < base_num; j++) {
						ifs.read((char*)& m, sizeof(Mom_basetrack));
						t.base.push_back(m);
					}
					for (int j = 0; j < base_pair_num; j++) {
						ifs.read((char*)& p.first, sizeof(Mom_basetrack));
						ifs.read((char*)& p.second, sizeof(Mom_basetrack));
						t.base_pair.push_back(p);
					}
					ev.true_chains.push_back(t);
				}
			}
			ret.push_back(ev);
			ev.chains.clear();
			ev.true_chains.clear();
			count++;
			if (max_event_num == count) {
				break;
			}
		}

		std::cerr << "\r read Event_information " << ret.size() << std::endl;
		return ret;

	}
	void Write_Event_information_bin_block(std::ofstream &ofs, std::vector<Event_information>&ev_v) {
		if (ev_v.size() == 0) {
			fprintf(stderr, "target data ... null\n");
		}
		else {
			int64_t count = 0;
			int64_t max = ev_v.size();
			int base_num, base_pair_num;
			int chain_num, true_chain_num;

			for (int i = 0; i < max; i++) {
				Write_Event_information_header(ofs, ev_v[i]);
				chain_num = ev_v[i].chains.size();
				true_chain_num = ev_v[i].true_chains.size();
				for (int j = 0; j < chain_num; j++) {
					base_num = ev_v[i].chains[j].base.size();
					base_pair_num = ev_v[i].chains[j].base_pair.size();
					Write_mom_chain_header(ofs, ev_v[i].chains[j]);

					for (int k = 0; k < base_num; k++) {
						ofs.write((char*)& ev_v[i].chains[j].base[k], sizeof(Mom_basetrack));
					}
					for (int k = 0; k < base_pair_num; k++) {
						ofs.write((char*)& ev_v[i].chains[j].base_pair[k].first, sizeof(Mom_basetrack));
						ofs.write((char*)& ev_v[i].chains[j].base_pair[k].second, sizeof(Mom_basetrack));
					}
				}
				for (int j = 0; j < true_chain_num; j++) {
					base_num = ev_v[i].true_chains[j].base.size();
					base_pair_num = ev_v[i].true_chains[j].base_pair.size();
					Write_mom_chain_header(ofs, ev_v[i].true_chains[j]);

					for (int k = 0; k < base_num; k++) {
						ofs.write((char*)& ev_v[i].true_chains[j].base[k], sizeof(Mom_basetrack));
					}
					for (int k = 0; k < base_pair_num; k++) {
						ofs.write((char*)& ev_v[i].true_chains[j].base_pair[k].first, sizeof(Mom_basetrack));
						ofs.write((char*)& ev_v[i].true_chains[j].base_pair[k].second, sizeof(Mom_basetrack));
					}
				}
				count++;
			}

			std::cerr << "\r write Event_information " << count << std::endl;
		}
	}

	void input_basetrack_information(std::vector<Mom_basetrack> &base,std::vector<std::pair<Mom_basetrack, Mom_basetrack>> &base_pair) {

		std::multimap<std::pair<int,int>, Mom_basetrack*> base_pair_p;
		for (auto itr = base_pair.begin(); itr != base_pair.end(); itr++) {
			base_pair_p.insert(std::make_pair(std::make_pair(itr->first.pl, itr->first.rawid), &(itr->first)));
			base_pair_p.insert(std::make_pair(std::make_pair(itr->second.pl, itr->second.rawid), &(itr->second)));
		}
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			std::pair<int, int> id = std::make_pair(itr->pl, itr->rawid);
			if (base_pair_p.count(id) == 0)continue;
			auto range = base_pair_p.equal_range(id);
			for (auto res = range.first; res != range.second; res++) {
				res->second->m[0] = itr->m[0];
				res->second->m[1] = itr->m[1];
			}
		}


	}

	std::vector<Event_information> Read_Event_information_extension(std::string filename) {
		std::vector<Event_information> ret;
		std::string extension;
		extension = filename.substr(filename.size() - 5, 5);
		if (extension == "momch") {
			ret = Read_Event_information_bin(filename);
		}
		else {
			ret = Read_Event_information_txt(filename);
		}
		return ret;
	}
	void Write_Event_information_extension(std::string filename, std::vector<Event_information>&ev_v) {
		std::string extension;
		extension = filename.substr(filename.size() - 5, 5);
		if (extension == "momch") {
			Write_Event_information_bin(filename, ev_v);
		}
		else {
			Write_Event_information_txt(filename, ev_v);
		}
	}


	microtrack_minimum::microtrack_minimum() {
		ph = -1;
		zone = -1;
		imager = -1;
		pixelnum = -1;
		hitnum = -1;
	}
	Mom_basetrack::Mom_basetrack() {
		pl = -1;
		rawid = -1;
		ax = NAN;
		ay = NAN;
		x = NAN;
		y = NAN;
		z = NAN;
	}
	Mom_chain::Mom_chain() {
		chainid = -1;
		stop_flg = -1;
		particle_flg = -1;
		direction = 0;
		charge_sign = 0;
		ecc_range_mom[0] = -1;
		ecc_range_mom[1] = -1;
		ecc_mcs_mom[0] = -1;
		ecc_mcs_mom[1] = -1;
		bm_range_mom = -1;
		charge_sign = -1;
		ecc_range_mom_error[0][0] = -1;
		ecc_range_mom_error[0][1] = -1;
		ecc_range_mom_error[1][0] = -1;
		ecc_range_mom_error[1][1] = -1;
		ecc_mcs_mom_error[0][0] = -1;
		ecc_mcs_mom_error[0][1] = -1;
		ecc_mcs_mom_error[1][0] = -1;
		ecc_mcs_mom_error[1][1] = -1;
		bm_range_mom_error[0] = -1;
		bm_range_mom_error[1] = -1;
		bm_curvature_mom_error[0] = -1;
		bm_curvature_mom_error[1] = -1;
		muon_likelihood = NAN;
		proton_likelihood = NAN;
	}
	Event_information::Event_information() {
		groupid = -1;
		unix_time = -1;
		tracker_track_id = -1;
		entry_in_daily_file = -1;
		vertex_pl = -1;
		ECC_id = -1;
		vertex_material = -1;
		vertex_position[0] = NAN;
		vertex_position[1] = NAN;
		vertex_position[2] = NAN;
		true_vertex_position[0] = NAN;
		true_vertex_position[1] = NAN;
		true_vertex_position[2] = NAN;
		weight = -1;
		nu_energy = -1;
		nu_ax = NAN;
		nu_ay = NAN;
	}

	double Mom_chain::Get_muon_mcs_pb() {
		if (!isfinite(ecc_mcs_mom[0]))return -1;
		if (ecc_mcs_mom[0] < 0)return -1;
		else if (ecc_mcs_mom[0] < 0.0000001)return 0;

		double mass = 105.65836668;
		double beta = 1 / sqrt(1 + pow(mass / ecc_mcs_mom[0], 2));
		return ecc_mcs_mom[0] * beta;
	}
	void Mom_chain::Get_muon_pb_mcs_error(double *pb_error) {
		double mass = 105.65836668;
		double pb_edge,beta;

		if (!isfinite(ecc_mcs_mom[0])||ecc_mcs_mom[0] < 0) {
			pb_error[0] = -1;
			pb_error[1] = -1;
			return;
		}
		//下限の計算
		if (ecc_mcs_mom_error[0][0] < 0)pb_error[0] = -1;
		else {
			pb_edge = ecc_mcs_mom[0] - ecc_mcs_mom_error[0][0];
			if (pb_edge < 0)pb_error[0] = -1;
			else if (pb_edge < 0.0000001) {
				pb_error[0] = Get_muon_mcs_pb();
			}
			else {
				beta = 1 / sqrt(1 + pow(mass / pb_edge, 2));
				pb_error[0] = Get_muon_mcs_pb() - pb_edge * beta;
			}
		}
		//上限の計算
		if (ecc_mcs_mom_error[0][1] < 0)pb_error[1] = -1;
		else {
			pb_edge = ecc_mcs_mom[0] + ecc_mcs_mom_error[0][1];
			if (pb_edge < 0)pb_error[1] = -1;
			else if (pb_edge < 0.0000001) {
				pb_error[1] = Get_muon_mcs_pb();
			}
			else {
				beta = 1 / sqrt(1 + pow(mass / pb_edge, 2));
				pb_error[1] = pb_edge * beta- Get_muon_mcs_pb();
			}
		}
	}
	double Mom_chain::Get_proton_mcs_pb() {
		double mass = 938.2720813;
		if (!isfinite(ecc_mcs_mom[1]))return -1;
		if (ecc_mcs_mom[1] < 0)return -1;
		else if (ecc_mcs_mom[1] < 0.0000001)return 0;

		double beta = 1 / sqrt(1 + pow(mass / ecc_mcs_mom[1], 2));
		return ecc_mcs_mom[1] * beta;

	}

	void Mom_chain::Get_proton_pb_mcs_error(double *pb_error) {
		double mass = 938.2720813;
		double pb_edge, beta;

		if (!isfinite(ecc_mcs_mom[1])||ecc_mcs_mom[1] < 0) {
			pb_error[0] = -1;
			pb_error[1] = -1;
			return;
		}
		//下限の計算
		if (ecc_mcs_mom_error[1][0] < 0)pb_error[0] = -1;
		else {
			pb_edge = ecc_mcs_mom[1] - ecc_mcs_mom_error[1][0];
			if (pb_edge < 0)pb_error[0] = -1;
			else if (pb_edge < 0.0000001) {
				pb_error[0] = Get_proton_mcs_pb();
			}
			else {
				beta = 1 / sqrt(1 + pow(mass / pb_edge, 2));
				pb_error[0] = Get_proton_mcs_pb() - pb_edge * beta;
			}
		}
		//上限の計算
		if (ecc_mcs_mom_error[1][1] < 0)pb_error[1] = -1;
		else {
			pb_edge = ecc_mcs_mom[1] + ecc_mcs_mom_error[1][1];
			if (pb_edge < 0)pb_error[1] = -1;
			else if (pb_edge < 0.0000001) {
				pb_error[1] = Get_proton_mcs_pb();
			}
			else {
				beta = 1 / sqrt(1 + pow(mass / pb_edge, 2));
				pb_error[1] = pb_edge * beta - Get_proton_mcs_pb();
			}
		}
	}

}
namespace Fiducial_Area {

	std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename) {

		std::ifstream ifs(filename);
		std::multimap<int, Fiducial_Area> fa_multi;
		std::map<int, std::vector<Fiducial_Area>> ret;
		Fiducial_Area fa;
		while (ifs >> fa.pl >> fa.p[0].x >> fa.p[0].y >> fa.p[0].z >> fa.p[1].x >> fa.p[1].y >> fa.p[1].z) {
			fa_multi.insert(std::make_pair(fa.pl, fa));
		}

		int count = 0;
		for (auto itr = fa_multi.begin(); itr != fa_multi.end(); itr++) {
			count = fa_multi.count(itr->first);
			auto range = fa_multi.equal_range(itr->first);
			std::vector<Fiducial_Area> fa_vec;
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				fa_vec.push_back(itr2->second);
			}
			ret.insert(std::make_pair(itr->first, fa_vec));
		}

		return ret;

	}
	void trans_mfile_cordinate(std::vector<corrmap_3d::align_param2> &param, std::vector<Fiducial_Area>&area) {

		std::vector< Point*> p_trans;
		for (auto itr = area.begin(); itr != area.end(); itr++) {
			p_trans.push_back(&(itr->p[0]));
			p_trans.push_back(&(itr->p[1]));
		}
		std::vector <std::pair<Point*, corrmap_3d::align_param2*>> p_trans_map = track_affineparam_correspondence(p_trans, param);
		trans_base_all(p_trans_map);
	}

	//mfile0::M_Base
	//basetrack-alignment mapの対応
	double select_triangle_vale(corrmap_3d::align_param2* param, Point*p) {
		double x, y;
		double dist = 0;
		x = (param->corr_p[0]->x + param->corr_p[1]->x + param->corr_p[2]->x) / 3;
		y = (param->corr_p[0]->y + param->corr_p[1]->y + param->corr_p[2]->y) / 3;
		dist = (p->x - x)*(p->x - x) + (p->y - y)*(p->y - y);
		return dist;
	}
	corrmap_3d::align_param2* search_param(std::vector<corrmap_3d::align_param*> &param, Point*p, std::multimap<int, corrmap_3d::align_param2*>&triangles) {
		//三角形内部
		//最近接三角形
		double dist = 0;
		std::map<double, corrmap_3d::align_param* > dist_map;
		//align_paramを近い順にsort
		for (auto itr = param.begin(); itr != param.end(); itr++) {
			dist = ((*itr)->x - p->x)*((*itr)->x - p->x) + ((*itr)->y - p->y)*((*itr)->y - p->y);
			dist_map.insert(std::make_pair(dist, (*itr)));
		}

		double sign[3];
		bool flg = false;
		int id;

		corrmap_3d::align_param2* ret = triangles.begin()->second;
		for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
			if (itr != dist_map.begin())continue;


			//corrmapのID
			id = itr->second->id;
			if (triangles.count(id) == 0) {
				fprintf(stderr, "alignment triangle ID=%d not found\n", id);
				exit(1);
			}
			//idの属する三角形を探索
			auto range = triangles.equal_range(id);
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				sign[0] = (itr2->second->corr_p[1]->x - itr2->second->corr_p[0]->x)*(p->y - itr2->second->corr_p[1]->y) - (itr2->second->corr_p[1]->y - itr2->second->corr_p[0]->y)*(p->x - itr2->second->corr_p[1]->x);
				sign[1] = (itr2->second->corr_p[2]->x - itr2->second->corr_p[1]->x)*(p->y - itr2->second->corr_p[2]->y) - (itr2->second->corr_p[2]->y - itr2->second->corr_p[1]->y)*(p->x - itr2->second->corr_p[2]->x);
				sign[2] = (itr2->second->corr_p[0]->x - itr2->second->corr_p[2]->x)*(p->y - itr2->second->corr_p[0]->y) - (itr2->second->corr_p[0]->y - itr2->second->corr_p[2]->y)*(p->x - itr2->second->corr_p[0]->x);
				//printf("point %.lf,%.1lf\n", base.x, base.y);
				//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[0]->x, itr2->second->corr_p[0]->y);
				//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[1]->x, itr2->second->corr_p[1]->y);
				//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[2]->x, itr2->second->corr_p[2]->y);
				//printf("sign %.1lf %1.lf %.1lf\n", sign[0], sign[1], sign[2]);
				//printf("  signbit %d %d %d\n", std::signbit(sign[0]), std::signbit(sign[1]), std::signbit(sign[2]));
				//printf("n signbit %d %d %d\n", !std::signbit(sign[0]), !std::signbit(sign[1]), !std::signbit(sign[2]));
				//printf("judge %d\n", (std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2])));
				//printf("\n");

				//符号が3つとも一致でtrue
				if ((std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2]))) {
					ret = itr2->second;
					flg = true;
					break;
				}
			}
			if (flg)break;
		}
		if (flg) {
			//printf("point in trianlge\n");
			return ret;
		}

		//distが最小になるcorrmapをとってくる
		dist = -1;
		for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
			//corrmapのID
			id = itr->second->id;
			if (triangles.count(id) == 0) {
				fprintf(stderr, "alignment triangle ID=%d not found\n", id);
				exit(1);
			}
			//idの属する三角形を探索
			auto range = triangles.equal_range(id);
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (dist<0 || dist>select_triangle_vale(itr2->second, p)) {
					dist = select_triangle_vale(itr2->second, p);
					ret = itr2->second;
				}
			}
		}
		//printf("point not in trianlge\n");
		return ret;
	}
	std::vector <std::pair<Point*, corrmap_3d::align_param2*>>track_affineparam_correspondence(std::vector<Point*>&p, std::vector <corrmap_3d::align_param2> &param) {

		//local alignの視野中心を取り出して、位置でhash
		//local alignの視野中心の作るdelaunay三角形をmapで対応

		std::map<int, corrmap_3d::align_param*> view_center;
		std::multimap<int, corrmap_3d::align_param2*>triangles;
		double xmin = 999999, ymin = 999999, hash = 2000;
		for (auto itr = param.begin(); itr != param.end(); itr++) {
			for (int i = 0; i < 3; i++) {
				view_center.insert(std::make_pair(itr->corr_p[i]->id, (itr->corr_p[i])));
				triangles.insert(std::make_pair(itr->corr_p[i]->id, &(*itr)));
				xmin = std::min(itr->corr_p[i]->x, xmin);
				ymin = std::min(itr->corr_p[i]->y, ymin);
			}
		}
		std::multimap<std::pair<int, int>, corrmap_3d::align_param*> view_center_hash;
		std::pair<int, int>id;
		for (auto itr = view_center.begin(); itr != view_center.end(); itr++) {
			id.first = int((itr->second->x - xmin) / hash);
			id.second = int((itr->second->y - ymin) / hash);
			view_center_hash.insert(std::make_pair(id, itr->second));
		}

		std::vector < std::pair<Point*, corrmap_3d::align_param2*>> ret;
		std::vector<corrmap_3d::align_param*> param_cand;
		int loop = 0, ix, iy, count = 0;
		for (auto itr = p.begin(); itr != p.end(); itr++) {
			if (count % 100000 == 0) {
				printf("\r search correspond triangles %d/%d(%4.1lf%%)", count, p.size(), count*100. / p.size());
			}
			count++;
			ix = ((*itr)->x - xmin) / hash;
			iy = ((*itr)->y - ymin) / hash;
			loop = 1;
			while (true) {
				param_cand.clear();
				for (int iix = ix - loop; iix <= ix + loop; iix++) {
					for (int iiy = iy - loop; iiy <= iy + loop; iiy++) {
						id.first = iix;
						id.second = iiy;
						if (view_center_hash.count(id) != 0) {
							auto range = view_center_hash.equal_range(id);
							for (auto res = range.first; res != range.second; res++) {
								param_cand.push_back(res->second);
							}
						}
					}
				}
				if (param_cand.size() > 2)break;
				loop++;
			}
			corrmap_3d::align_param2* param2 = search_param(param_cand, *itr, triangles);
			ret.push_back(std::make_pair((*itr), param2));
		}
		printf("\r search correspond triangles %d/%d(%4.1lf%%)\n", count, p.size(), count*100. / p.size());

		return ret;
	}
	//変換 zshrink補正-->9para変換
	void trans_base(std::vector<Point*>&p, corrmap_3d::align_param2 *param) {

		matrix_3D::matrix_33 x_rot_mat(0, param->x_rot), y_rot_mat(1, param->y_rot), z_rot_mat(2, param->z_rot), all_trans(0, 0), shear_mat(0, 0), shrink_mat(0, 0);

		shrink_mat.val[0][0] *= param->x_shrink;
		shrink_mat.val[1][1] *= param->y_shrink;
		//shrink_mat.val[2][2] *= param->z_shrink;
		shear_mat.val[0][1] = param->yx_shear;
		//shear_mat.val[0][2] = param->zx_shear;
		//shear_mat.val[1][2] = param->zy_shear;

		matrix_3D::vector_3D shift, center;
		center.x = param->x;
		center.y = param->y;
		center.z = param->z;
		shift.x = param->dx;
		shift.y = param->dy;
		shift.z = param->dz;

		all_trans.matrix_multiplication(shear_mat);
		all_trans.matrix_multiplication(shrink_mat);
		all_trans.matrix_multiplication(z_rot_mat);
		all_trans.matrix_multiplication(y_rot_mat);
		all_trans.matrix_multiplication(x_rot_mat);

		//all_trans.Print();
		matrix_3D::vector_3D base_p0;
		double base_thick = 210;
		for (auto itr = p.begin(); itr != p.end(); itr++) {
			base_p0.x = (*itr)->x;
			base_p0.y = (*itr)->y;
			base_p0.z = param->z;

			//base_p1.x = (*itr)->x + (*itr)->ax*base_thick;
			//base_p1.y = (*itr)->y + (*itr)->ay*base_thick;
			////角度shrink分はここでかける
			//base_p1.z = param->z + base_thick / param->z_shrink;

			//視野中心を原点に移動
			//base_p0 = matrix_3D::addition(base_p0, matrix_3D::const_multiple(center, -1));
			//base_p1 = matrix_3D::addition(base_p1, matrix_3D::const_multiple(center, -1));

			//変換の実行
			base_p0.matrix_multiplication(all_trans);
			base_p0 = matrix_3D::addition(base_p0, shift);
			//base_p1.matrix_multiplication(all_trans);
			//base_p1 = matrix_3D::addition(base_p1, shift);

			//原点をもとに戻す
			//base_p0 = matrix_3D::addition(base_p0, center);
			//base_p1 = matrix_3D::addition(base_p1, center);

			(*itr)->x = base_p0.x;
			(*itr)->y = base_p0.y;
			(*itr)->z = base_p0.z;

			//printf("ax:%.4lf --> %.4lf\n", (*itr)->ax, (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z));
			//printf("ay:%.4lf --> %.4lf\n", (*itr)->ay, (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z));

			//(*itr)->ax = (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z) + param->zx_shear;
			//(*itr)->ay = (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z) + param->zy_shear;

		}
	}
	void trans_base_all(std::vector < std::pair<Point*, corrmap_3d::align_param2*>>&track_pair) {
		std::map<std::tuple<int, int, int>, corrmap_3d::align_param2*> param_map;
		std::multimap<std::tuple<int, int, int>, Point*>base_map;
		std::tuple<int, int, int>id;
		//三角形ごとにbasetrackをまとめる
		for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
			std::get<0>(id) = itr->second->corr_p[0]->id;
			std::get<1>(id) = itr->second->corr_p[1]->id;
			std::get<2>(id) = itr->second->corr_p[2]->id;
			param_map.insert(std::make_pair(id, itr->second));
			base_map.insert(std::make_pair(id, itr->first));
		}


		//ここで三角形ごとに変換
		int count = 0;
		std::vector<Point*> t_base;
		for (auto itr = param_map.begin(); itr != param_map.end(); itr++) {
			if (count % 1000 == 0) {
				printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)", count, param_map.size(), count*100. / param_map.size());
			}
			count++;

			t_base.clear();

			if (base_map.count(itr->first) == 0)continue;
			auto range = base_map.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				t_base.push_back(res->second);
			}
			trans_base(t_base, itr->second);

		}
		printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)\n", count, param_map.size(), count*100. / param_map.size());

	}

	bool judge_fiducial_area(std::vector<Fiducial_Area>&area, mfile0::M_Base&b) {

		std::map<double, Point> point_map;
		double ex_x, ex_y, dist;
		for (auto itr = area.begin(); itr != area.end(); itr++) {
			ex_x = b.x + b.ax*(itr->p[0].z - b.z);
			ex_y = b.y + b.ay*(itr->p[0].z - b.z);
			dist = pow(ex_x - itr->p[0].x, 2) + pow(ex_y - itr->p[0].y, 2);
			point_map.insert(std::make_pair(dist, itr->p[0]));
		}
		//外挿先から距離の一番近い点のz座標を使用
		double z = point_map.begin()->second.z;
		double x = b.x + b.ax*(z - b.z);
		double y = b.y + b.ay*(z - b.z);


		//true でArea内　falseでarea外

		//点(x,y)からx軸性の方向に直線を引き、その直線と多角形の辺が何回交わるか。
		//下から上に交わったときwn+1
		//上から下に交わったときwn-1
		int wn = 0;
		double vt;
		for (auto itr = area.begin(); itr != area.end(); itr++) {
			// 上向きの辺、下向きの辺によって処理が分かれる。
		// 上向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、終点は含まない。(ルール1)
			if (itr->p[0].y <= y && itr->p[1].y > y) {
				// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
				// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
				vt = (y - itr->p[0].y) / (itr->p[1].y - itr->p[0].y);
				if (x < itr->p[0].x + vt * (itr->p[1].x - itr->p[0].x)) {
					++wn;  //ここが重要。上向きの辺と交差した場合は+1
				}
			}
			// 下向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、始点は含まない。(ルール2)
			else if (itr->p[0].y > y && itr->p[1].y <= y) {
				// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
				// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
				vt = (y - itr->p[0].y) / (itr->p[1].y - itr->p[0].y);
				if (x < itr->p[0].x + vt * (itr->p[1].x - itr->p[0].x)) {
					--wn;  //ここが重要。下向きの辺と交差した場合は-1
				}
			}
		}
		if (wn >= 1)return true;
		return false;
	}

	bool judge_inner_track(std::vector<Fiducial_Area>&area, mfile0::M_Base&b,double threshold) {
		std::map<double, Point> point_map;
		double ex_x, ex_y, dist;
		for (auto itr = area.begin(); itr != area.end(); itr++) {
			ex_x = b.x + b.ax*(itr->p[0].z - b.z);
			ex_y = b.y + b.ay*(itr->p[0].z - b.z);
			dist = sqrt(pow(ex_x - itr->p[0].x, 2) + pow(ex_y - itr->p[0].y, 2));
			point_map.insert(std::make_pair(dist, itr->p[0]));
			//printf("(%.1lf,%.1lf,%.1lf) %.1lf\n", itr->p[0].x, itr->p[0].y, itr->p[0].z - b.z,dist);
			//printf("dz=%.1lf\n", itr->p[0].z - b.z);
		}
		//外挿先から距離の一番近い点のz座標を使用
		double z = point_map.begin()->second.z;
		double x = b.x + b.ax*(z - b.z);
		double y = b.y + b.ay*(z - b.z);
		//printf("dist=%.1lf\n", point_map.begin()->first);
		return point_map.begin()->first > threshold;
	}

}