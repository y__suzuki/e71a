#define _CRT_SECURE_NO_WARNINGS

#include "my_algorithm.hpp"

#include <iostream>
#include <unordered_set>
#include <iterator>
#include <algorithm>
#include <string>
#include <chrono>
bool sort_pair_1(const std::pair<int, int> &left, const std::pair<int, int> &right) {
	return left.first < right.first;
}
bool sort_pair_2(const std::pair<int, int> &left, const std::pair<int, int> &right) {
	return left.second < right.second;
}
//std::vector<id_pair> id_clustering(std::vector<std::pair<int, int >> pair_v,bool output) {
//	int gid = 0;
//	//uniqueなrawidとgroupIDのpairを作成
//	std::vector<std::pair<int, int>> raw0;
//	std::vector<std::pair<int, int>> raw1;
//	{
//		std::set <int> rawid0;
//		std::set <int> rawid1;
//		for (auto itr = pair_v.begin(); itr != pair_v.end(); itr++) {
//			rawid0.insert(itr->first);
//			rawid1.insert(itr->second);
//		}
//		for (auto itr = rawid0.begin(); itr != rawid0.end(); itr++) {
//			raw0.push_back(std::make_pair(*itr, gid));
//			gid++;
//		}
//		for (auto itr = rawid1.begin(); itr != rawid1.end(); itr++) {
//			raw1.push_back(std::make_pair(*itr, gid));
//			gid++;
//		}
//	}
//
//	//rawid multiの対応表を作成
//	//std::multimap<int, int> id_pair_01;
//	//std::multimap<int, int> id_pair_10;
//	//for (auto itr = pair_v.begin(); itr != pair_v.end(); itr++) {
//	//	id_pair_01.insert(std::make_pair(itr->first, itr->second));
//	//	id_pair_10.insert(std::make_pair(itr->second, itr->first));
//	//}
//
//	std::multimap <int, int> gid_rawid0;
//	std::multimap <int, int> gid_rawid1;
//	std::map <int, int*> rawid0_gid;
//	std::map <int, int*> rawid1_gid;
//
//	for (auto itr = raw0.begin(); itr != raw0.end(); itr++) {
//		rawid0_gid.insert(std::make_pair(itr->first, &(itr->second)));
//		gid_rawid0.insert(std::make_pair(itr->second, itr->first));
//	}
//	for (auto itr = raw1.begin(); itr != raw1.end(); itr++) {
//		rawid1_gid.insert(std::make_pair(itr->first, &(itr->second)));
//		gid_rawid1.insert(std::make_pair(itr->second, itr->first));
//	}
//
//	int num = 0;
//	int* after_gid_ptr;
//	int  count0, count1, before_gid;
//	for (auto itr = pair_v.begin(); itr != pair_v.end(); itr++) {
//		if (output&&num % 10000 == 0) {
//			printf("\r %d/%d (%4.1lf%%)", num, pair_v.size(), num*100. / pair_v.size());
//		}
//		num++;
//
//		auto gid0 = rawid0_gid.find(itr->first);
//		auto gid1 = rawid1_gid.find(itr->second);
//
//		if (gid0 == rawid0_gid.end() || gid1 == rawid1_gid.end()) {
//			fprintf(stderr, "exception id not found\n");
//			exit(1);
//		}
//		
//		if (*(gid0->second) == *(gid1->second)) {
//			//変換不要
//			//最終的にはすべてがここに入る
//			continue;
//		}
//		//変換前後のgid
//		else if (*(gid0->second) < *(gid1->second)) {
//			before_gid = *(gid1->second);
//			after_gid_ptr = gid0->second;
//		}
//		else {
//			before_gid = *(gid0->second);
//			after_gid_ptr = gid1->second;
//		}
//
//		count0 = gid_rawid0.count(before_gid);
//		count1 = gid_rawid1.count(before_gid);
//		//変換部分
//		if (count0 == 0) {
//
//		}
//		else if (count0 == 1) {
//			auto change_rawid0 = gid_rawid0.find(before_gid);
//			auto change_gid0 = rawid0_gid.find(change_rawid0->second);
//			//gidの変更
//			int raw_tmp, gid;
//			change_gid0->second = after_gid_ptr;
//			raw_tmp = change_gid0->first;
//			gid = *(after_gid_ptr);
//			gid_rawid0.erase(change_rawid0);
//			gid_rawid0.insert(std::make_pair(gid, raw_tmp));
//		}
//		else {
//			auto change_rawid0 = gid_rawid0.equal_range(before_gid);
//			std::vector<std::pair<int, int>> raw_buf;
//			for (auto itr_change = change_rawid0.first; itr_change != change_rawid0.second; itr_change++) {
//				auto change_gid0 = rawid0_gid.find(itr_change->second);
//				change_gid0->second = after_gid_ptr;
//				int raw_tmp, gid;
//				raw_tmp = change_gid0->first;
//				gid = *(after_gid_ptr);
//				raw_buf.push_back(std::make_pair(gid, raw_tmp));
//			}
//			gid_rawid0.erase(change_rawid0.first, change_rawid0.second);
//			for (auto itr_p = raw_buf.begin(); itr_p != raw_buf.end(); itr_p++) {
//				gid_rawid0.insert(*itr_p);
//			}
//		}
//		if (count1 == 0) {
//		}
//		else if (count1 == 1) {
//			auto change_rawid1 = gid_rawid1.find(before_gid);
//			auto change_gid1 = rawid1_gid.find(change_rawid1->second);
//			//gidの変更
//			int raw_tmp, gid;
//			change_gid1->second = after_gid_ptr;
//			raw_tmp = change_gid1->first;
//			gid = *(after_gid_ptr);
//			gid_rawid1.erase(change_rawid1);
//			gid_rawid1.insert(std::make_pair(gid, raw_tmp));
//		}
//		else {
//			auto change_rawid1 = gid_rawid1.equal_range(before_gid);
//			std::vector<std::pair<int, int>> raw_buf;
//			for (auto itr_change = change_rawid1.first; itr_change != change_rawid1.second; itr_change++) {
//				auto change_gid1 = rawid1_gid.find(itr_change->second);
//				change_gid1->second = after_gid_ptr;
//				int raw_tmp, gid;
//				raw_tmp = change_gid1->first;
//				gid = *(after_gid_ptr);
//				raw_buf.push_back(std::make_pair(gid, raw_tmp));
//			}
//			gid_rawid1.erase(change_rawid1.first, change_rawid1.second);
//			for (auto itr_p = raw_buf.begin(); itr_p != raw_buf.end(); itr_p++) {
//				gid_rawid1.insert(*itr_p);
//			}
//		}
//	}
//	if (output) {
//		printf("\r %d/%d (%4.1lf%%)\n", num, pair_v.size(), num*100. / pair_v.size());
//	}
//
//	//error確認用
//	for (auto itr = pair_v.begin(); itr != pair_v.end(); itr++) {
//		auto gid0 = rawid0_gid.find(itr->first);
//		auto gid1 = rawid1_gid.find(itr->second);
//		//printf("raw0 %d raw1 %d gid0 %d gid1 %d\n", gid0->first, gid1->first, *gid0->second, *gid1->second);
//		if (*(gid0->second) != *(gid1->second)) {
//			fprintf(stderr, "exception trans miss\n");
//			fprintf(stderr, "in [my_algorithm.cpp]\n");
//			exit(1);
//		}
//	}
//
//	//戻り値の型に変換
//	std::vector<id_pair> ret;
//	for (int i = 0; i < gid; i++) {
//		int count0 = gid_rawid0.count(i);
//		int count1 = gid_rawid1.count(i);
//		if (count0 == 0 && count1 == 0)continue;
//		else if (count0 == 0 || count1 == 0) {
//			fprintf(stderr, "exception gid not found\n");
//			printf("count0=%d count1=%d\n", count0, count1);
//			exit(1);
//		}
//		id_pair tmp_pair;
//		tmp_pair.group_id = i;
//		if (count0 == 1) {
//			tmp_pair.rawid0.insert(gid_rawid0.find(i)->second);
//		}
//		else {
//			auto range = gid_rawid0.equal_range(i);
//			for (auto itr = range.first; itr != range.second; itr++) {
//				tmp_pair.rawid0.insert(itr->second);
//			}
//		}
//		if (count1 == 1) {
//			tmp_pair.rawid1.insert(gid_rawid1.find(i)->second);
//		}
//		else {
//			auto range = gid_rawid1.equal_range(i);
//			for (auto itr = range.first; itr != range.second; itr++) {
//				tmp_pair.rawid1.insert(itr->second);
//			}
//		}
//		ret.push_back(tmp_pair);
//	}
//
//	return ret;
//}
std::vector<id_pair> id_clustering(std::vector<std::pair<int, int >> pair_v, bool output) {
	if (output) {
		printf("input all pair %d\n", pair_v.size());
	}
	auto start = std::chrono::system_clock::now();      // 計測スタート時刻を保存

	std::multimap<int, std::pair<bool, int>>raw1_map;
	std::multimap<int, std::pair<bool, int>>raw2_map;
	for (auto itr = pair_v.begin(); itr != pair_v.end(); itr++) {
		raw1_map.insert(std::make_pair(itr->first, std::make_pair(false, itr->second)));
		raw2_map.insert(std::make_pair(itr->second, std::make_pair(false, itr->first)));
	}


	//mapはkeyの値でsortされている
	std::map<int, std::set<std::pair<int, int>>> all_group;
	std::set<std::pair<int, int>> searched;
	std::set<int> search_list;

	int gid = 0;
	int count = 0;
	bool flg;
	for (auto itr = raw1_map.begin(); itr != raw1_map.end(); itr++) {
		if (itr->second.first)continue;
		//groupの探索(最初)
		std::set<std::pair<int, int>> group;
		searched.clear();
		search_list.clear();

		for (int i = 0; i < raw1_map.count(itr->first); i++) {
			group.insert(std::make_pair(1, itr->first));
			group.insert(std::make_pair(2, itr->second.second));
			itr->second.first = true;
			searched.insert(std::make_pair(1, itr->first));
			if (i + 1 < raw1_map.count(itr->first)) {
				itr=std::next(itr, 1);
			}
			count++;
		}

		if (searched.size() > 1) {
			printf("exception\n");
		}
		//探索リストの作成
		flg = true;
		int roop = 0;
		while (flg) {
			roop++;
			count = 0;
			search_list.clear();
			//2の探索
			for (auto itr2 = group.begin(); itr2 != group.end(); itr2++) {
				if (searched.count(*itr2))continue;
				if (itr2->first == 1) {
					fprintf(stderr, "exception group search 1\n");
					exit(1);
				}
				search_list.insert(itr2->second);
			}

			//groupの探索
			for (auto itr2 = search_list.begin(); itr2 != search_list.end(); itr2++) {
				if (raw2_map.count(*itr2)==0) {
					fprintf(stderr, "exception group search 2\n");
					exit(1);
				}
				else if (raw2_map.count(*itr2) == 1) {
					auto itr3 = raw2_map.find(*itr2);
					auto res = group.insert(std::make_pair(1, itr3->second.second));
					itr3->second.first = true;
				}
				else {
					auto range = raw2_map.equal_range(*itr2);
					for (auto itr3 = range.first; itr3 != range.second; itr3++) {
						auto res = group.insert(std::make_pair(1, itr3->second.second));
						itr3->second.first = true;
					}
				}
				searched.insert(std::make_pair(2, *itr2));
			}
			//1の探索
			search_list.clear();
			for (auto itr2 = group.begin(); itr2 != group.end(); itr2++) {
				if (searched.count(*itr2))continue;
				if (itr2->first == 2) {
					fprintf(stderr, "exception group search 3\n");
					exit(1);
				}
				search_list.insert(itr2->second);
			}

			//groupの探索
			for (auto itr2 = search_list.begin(); itr2 != search_list.end(); itr2++) {

				if (raw1_map.count(*itr2) == 0) {
					fprintf(stderr, "exception group search 2\n");
					exit(1);
				}
				else if (raw1_map.count(*itr2) == 1) {
					auto itr3 = raw1_map.find(*itr2);
					auto res = group.insert(std::make_pair(2, itr3->second.second));
					itr3->second.first = true;
				}
				else {
					auto range = raw1_map.equal_range(*itr2);
					for (auto itr3 = range.first; itr3 != range.second; itr3++) {
						auto res = group.insert(std::make_pair(2, itr3->second.second));
						itr3->second.first = true;
					}
				}
				searched.insert(std::make_pair(1, *itr2));
			}
			//if (roop > 30) {
			if (group.size() == searched.size()) {
				flg = false;
			}

		}
		all_group.insert(std::make_pair(gid, group));
		gid++;
	}
	for (auto itr = raw1_map.begin(); itr != raw1_map.end(); itr++) {
		if (!itr->second.first) {
			printf("exception rawid %d,%d\n", itr->first, itr->second);

		}
	}
	for (auto itr = raw2_map.begin(); itr != raw2_map.end(); itr++) {
		if (!itr->second.first) {
			printf("exception rawid %d,%d\n", itr->first, itr->second);

		}
	}

	std::vector<id_pair> ret;
	for (auto itr = all_group.begin(); itr != all_group.end(); itr++) {
		id_pair tmp;
		tmp.group_id = itr->first;
		count = 0;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (itr2->first == 1) {
				tmp.rawid0.insert(itr2->second);
				count++;
			}
			else if (itr2->first == 2) {
				tmp.rawid1.insert(itr2->second);
				count++;
			}
			else {
				fprintf(stderr, "exception\n");
				exit(1);
			}
		}
		if (count == 2) {
			printf("exception one track\n");
		}
		if (count == 1) {
			printf("exception zero tack\n");
		}
		ret.push_back(tmp);
	}

	auto end = std::chrono::system_clock::now();       // 計測終了時刻を保存
	auto dur = end - start;        // 要した時間を計算
	auto msec = std::chrono::duration_cast<std::chrono::milliseconds>(dur).count();
	if (output) {
		printf("number of group = %d\n", all_group.size());
		printf("time %10.3lf[sec]\n", msec / 1000.);
	}
	//exit(1);
	return ret;

}

