#include <netscan_ali.hpp>

//corrmap - pos1のほうの変換map探索
std::multimap <int,vxx::base_track_t> search_corrmap(std::vector<vxx::base_track_t> &base, std::vector<corrmap0::Corrmap> corr) {
	if (base.begin()->pl != corr.begin()->pos[1] / 10) {
		fprintf(stderr, "netescan_ali exception\n");
		exit(1);
	}

	//corrmapを逆変換
	double x_min, y_min, hash = 3000;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		double factor;
		factor = 1 / (itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);

		std::pair<double, double> corner[4];
		corner[0].first = itr->areax[0];
		corner[1].first = itr->areax[0];
		corner[2].first = itr->areax[1];
		corner[3].first = itr->areax[1];
		corner[0].second = itr->areay[0];
		corner[1].second = itr->areay[1];
		corner[2].second = itr->areay[0];
		corner[3].second = itr->areay[1];
		for (int i = 0; i < 4; i++) {
			double tmp_x, tmp_y;
			tmp_x = corner[i].first - itr->position[4];
			tmp_y = corner[i].second - itr->position[5];
			corner[i].first = factor * (tmp_x*itr->position[3] - tmp_y * itr->position[1]);
			corner[i].second = factor * (tmp_y*itr->position[0] - tmp_x * itr->position[2]);
		}
		itr->notuse_d[0] = (corner[0].first + corner[1].first + corner[2].first + corner[3].first)*0.25;
		itr->notuse_d[1] = (corner[0].second + corner[1].second + corner[2].second + corner[3].second)*0.25;
		x_min = std::min(itr->notuse_d[0], x_min);
		y_min = std::min(itr->notuse_d[1], y_min);
	}
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	std::pair<int, int> id;
	for (auto itr = corr.begin(); itr!=corr.end(); itr++) {
		id.first = int((itr->notuse_d[0] - x_min) / hash);
		id.second = int((itr->notuse_d[1] - y_min) / hash);
		for (int ix = -1; ix <= 1; ix++) {
			for (int iy = -1; iy <= 1; iy++) {
				corr_map.insert(std::make_pair(std::make_pair(id.first + ix, id.second + iy), *itr));
			}
		}
	}
	//補正マップセンターを決める
	//最近接のtrackを変換
	std::multimap <int, vxx::base_track_t> ret;
	bool flg;
	int ix, iy, corr_id = 0;
	;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		id.first = (itr->x - x_min) / hash;
		id.second = (itr->y - y_min) / hash;
		ix = 0;
		iy = 0;
		flg = true;
		while (flg) {
			std::vector<corrmap0::Corrmap> corr_cand;
			for (int i = -1 * ix; i <= ix; i++) {
				for (int j = -1 * iy; i <= iy; j++) {
					if (corr_map.count(std::make_pair(id.first + ix, id.second + iy)) == 0)continue;
					auto range = corr_map.equal_range(std::make_pair(id.first + ix, id.second + iy));
					for (auto res = range.first; res != range.second; res++) {
						corr_cand.push_back(res->second);
					}
				}
			}
			double dist = 0;

			if (corr_cand.size() > 0) {
				flg = false;
			}
			for (auto itr2 = corr_cand.begin(); itr2 != corr_cand.end(); itr2++) {
				if (itr2 == corr_cand.begin()) {
					dist = pow(itr->x - itr2->notuse_d[0], 2) + pow(itr->y - itr2->notuse_d[1], 2);
					corr_id = itr2->id;
				}
				if (dist > pow(itr->x - itr2->notuse_d[0], 2) + pow(itr->y - itr2->notuse_d[1], 2)) {
					dist = pow(itr->x - itr2->notuse_d[0], 2) + pow(itr->y - itr2->notuse_d[1], 2);
					corr_id = itr2->id;
				}
			}
			ix++;
			iy++;
		}
		ret.insert(std::make_pair(corr_id, *itr));

	}
	return ret;
}

