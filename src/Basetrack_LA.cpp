#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int main(int argc, char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-bvxx PL zone out-bvxx\n");
		exit(1);
	}

	//input value
	std::string in_file_base = argv[1];
	std::string out_file_base = argv[4];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);

	//basetrack data
	std::vector<netscan::base_track_t> base;

	//read basetrack
	netscan::read_basetrack_extension(in_file_base, base, pl, zone);

	//write basetrack
	netscan::write_basetrack_vxx(out_file_base, base, pl, zone);


	
}
