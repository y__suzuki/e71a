#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

struct EfficiencyPositionMap {
	int PL, all, exist;
	double xmin, xmax, ymin, ymax, eff, err;
};
struct EfficiencyAngleMap {
	int PL, all, exist;
	double axmin, axmax, aymin, aymax, eff, err;
};
struct EfficiencyPLCorrelation {
	int PL, all, exist;
	double xmin, xmax, ymin, ymax, angmin, angmax, eff, err;
};
struct Prediction {
	int PL, ChainID;
	double ax, ay, x, y, z;
	bool Hit;
};

bool sort_pred_ChainID(const Prediction &left, const Prediction &right) {
	return left.ChainID < right.ChainID;
}

std::vector<mfile0::M_Chain> SelectGroupChain(std::vector<mfile0::M_Chain> c);
std::vector<mfile0::M_Chain> SelectLongChain(std::vector<mfile0::M_Chain> c, int npl_thr);
std::vector<mfile0::M_Chain> SelectMomentum(std::vector<mfile0::M_Chain> c, double threshold, int option);

void FillGap(mfile0::Mfile m, std::vector<std::pair<int, double>>&gap_list);
void MakePrediction(mfile0::Mfile m, std::vector<Prediction> &pre_v, int PL, std::vector<std::pair<int, double>>gap_list);

void ClacEfficiencyPosition(std::vector<Prediction> pre, std::vector<std::vector<EfficiencyPositionMap>> &eff_map, double area_size);
void ClacEfficiencyAngle(std::vector<Prediction> pre, std::vector<std::vector<EfficiencyAngleMap>> &eff_map, double area_size);
void ClacEfficiencyPL(std::vector<Prediction> pre, std::vector<EfficiencyPLCorrelation> &eff_map, double edge_cut);

void WriteEfficiencyPosition(std::string filename, std::vector<std::vector<std::vector<EfficiencyPositionMap>>> eff_map);
void WriteEfficiencyAngle(std::string filename, std::vector<std::vector<std::vector<EfficiencyAngleMap>>> eff_map);
void WriteEfficiencyPL(std::string filename, std::vector<std::vector<EfficiencyPLCorrelation>> eff_map);
void WritePrediction(std::string filename, std::vector<std::vector<Prediction>> pred);

std::vector<Prediction> Prediction_Angle_cut(std::vector<Prediction> pred, double x_min, double x_max, double y_min, double y_max);
std::vector<Prediction> Prediction_Area_cut(std::vector<Prediction> pred, double x_min, double x_max, double y_min, double y_max);

void main(int argc, char* argv[]) {
	if (argc != 4) {
	usage:
		fprintf(stderr, "usage:prg input-mfile RMSthreshold thr-opt\n");
		fprintf(stderr, "Ex)  prg.exe m.all 0.015 -1\n");
		fprintf(stderr, "thr-opt: [-1 --> trackRMS < RMSthreshold] High momemtum \n");
		fprintf(stderr, "thr-opt: [ 1 --> trackRMS > RMSthreshold] Low  momemtum \n");
		exit(1);
	}
	std::string filename = argv[1];
	double threshold = std::stod(argv[2]);
	int option = std::stoi(argv[3]);
	if (abs(option) != 1) {
		goto usage;
	}

	mfile0::Mfile m;
	//mfileの読み込み
	mfile1::read_mfile_extension(filename, m);
	//必要な定数の抽出
	int PLmin = int(m.header.all_pos[0] / 10);
	int PLmax = int(m.header.all_pos[m.header.all_pos.size() - 1] / 10);
	//printf("PLmin %03d --- PLmax %03d\n", PLmin, PLmax);
	std::vector<std::pair<int, double>>gap_list;
	FillGap(m, gap_list);
	//mfileのselection;
	m.chains = SelectLongChain(m.chains, 10);
	m.chains = SelectMomentum(m.chains, threshold,option);
	m.chains = SelectGroupChain(m.chains);

	//PLごとにpredicitonの作成
	std::vector<std::vector<Prediction>> pred_v;
	for (int pl = PLmin; pl <= PLmax; pl++) {
		fprintf(stderr, "\r make prediction PL%03d", pl);
		std::vector<Prediction> pred;
		MakePrediction(m, pred, pl, gap_list);

		pred = Prediction_Area_cut(pred, 20000, 90000, 10000, 90000);
		//pred = Prediction_Angle_cut(pred, -2.5, 2.5, -2.5, 2.5);

		pred_v.push_back(pred);
	}
	fprintf(stderr, "\n");

	//2500um角でefficiencyの計算
	std::vector<std::vector<std::vector<EfficiencyPositionMap>>> eff_map_position;
	for (int i = 0; i < pred_v.size(); i++) {
		if (pred_v[i].size() < 1)continue;
		fprintf(stderr, "\r calc efficiency (position)\t\tPL%03d", pred_v[i][0].PL);
		std::vector<std::vector<EfficiencyPositionMap>> eff_map;
		ClacEfficiencyPosition(pred_v[i], eff_map, 2500);
		eff_map_position.push_back(eff_map);
	}
	fprintf(stderr, "\n");

	//0.1rad角でefficiencyの計算
	std::vector<std::vector<std::vector<EfficiencyAngleMap>>> eff_map_angle;
	for (int i = 0; i < pred_v.size(); i++) {
		if (pred_v[i].size() < 1)continue;
		fprintf(stderr, "\r calc efficiency (angle)\t\tPL%03d", pred_v[i][0].PL);
		std::vector<std::vector<EfficiencyAngleMap>> eff_map;
		ClacEfficiencyAngle(pred_v[i], eff_map, 0.1);
		eff_map_angle.push_back(eff_map);
	}
	fprintf(stderr, "\n");

	//PL-efficiency相関用
	std::vector<std::vector<EfficiencyPLCorrelation>> eff_map_pl;
	for (int i = 0; i < pred_v.size(); i++) {
		if (pred_v[i].size() < 1)continue;
		fprintf(stderr, "\r calc efficiency (PL correlation)\tPL%03d", pred_v[i][0].PL);
		std::vector<EfficiencyPLCorrelation> eff_map;
		//端から1cmカット
		ClacEfficiencyPL(pred_v[i], eff_map, 10000);
		eff_map_pl.push_back(eff_map);
	}
	fprintf(stderr, "\n");

	WriteEfficiencyPosition("eff_position.txt", eff_map_position);
	WriteEfficiencyAngle("eff_angle.txt", eff_map_angle);
	WriteEfficiencyPL("eff_pl.txt", eff_map_pl);
	WritePrediction("prediction.txt", pred_v);
}

std::vector<mfile0::M_Chain> SelectGroupChain(std::vector<mfile0::M_Chain> c ) {
	std::vector<mfile0::M_Chain> ret;

	std::multimap<int, mfile0::M_Chain*>chain_map;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		chain_map.insert(std::make_pair(itr->basetracks.begin()->group_id, &(*itr)));
	}

	for (auto itr = chain_map.begin(); itr != chain_map.end();) {
		if (chain_map.count(itr->first) == 1) {
			ret.push_back(*itr->second);
			itr++;
		}
		else {
			auto range = chain_map.equal_range(itr->first);
			int seg_max = 0;
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				seg_max = std::max(itr2->second->nseg, seg_max);
				itr++;
			}
			std::vector<mfile0::M_Chain *>multi;
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (seg_max == itr2->second->nseg) {
					multi.push_back(itr2->second);
				}
			}
			auto cand = multi.begin();
			double d_lat = mfile0::angle_diff_dev_lat(**cand, mfile0::chain_ax(**cand), mfile0::chain_ay(**cand));
			for (auto itr2 = multi.begin(); itr2 != multi.end(); itr2++) {
				if (d_lat > mfile0::angle_diff_dev_lat(**itr2, mfile0::chain_ax(**itr2), mfile0::chain_ay(**itr2))) {
					cand = itr2;
					d_lat = mfile0::angle_diff_dev_lat(**cand, mfile0::chain_ax(**cand), mfile0::chain_ay(**cand));
				}
			}
			ret.push_back(**cand);
		}
	}

	printf("SelectGroupChain %10d --> %10d(%4.1lf%%)\n", (int)c.size(), (int)ret.size(), ret.size()*100. / c.size());
	return ret;
}
std::vector<mfile0::M_Chain> SelectLongChain(std::vector<mfile0::M_Chain> c,int npl_thr) {
	std::vector<mfile0::M_Chain> ret;
	int nseg, npl;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		nseg = (itr)->nseg;
		npl = (itr->pos1 - itr->pos0) / 10 + 1;
		if (npl < npl_thr)continue;
		if (nseg*1.0 / npl < 0.5)continue;
		ret.push_back(*itr);
	}
	printf("SelectLongChain npl>=%d %10d --> %10d(%4.1lf%%)\n", npl_thr, (int)c.size(), (int)ret.size(), ret.size()*100. / c.size());
	return ret;
}
std::vector<mfile0::M_Chain> SelectMomentum(std::vector<mfile0::M_Chain> c, double threshold, int option) {
	//option=1で以上
	//option=-1で以下
	if (int(abs(option)) != 1) {
		fprintf(stderr, "Function:AngleRMSCut Option Error\n");
		exit(1);
	}
	std::vector<mfile0::M_Chain> ret;
	double d_lat;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		d_lat = mfile0::angle_diff_dev_lat(*itr, mfile0::chain_ax(*itr), mfile0::chain_ay(*itr));

		if (threshold*option < d_lat*option) {
			ret.push_back(*itr);
		}
	}
	if (option == -1) {
		printf("select angle rms(lateral) rms < %5.4lf %d --> %d(%4.1lf%%)\n", threshold, int(c.size()), int(ret.size()), ret.size()*100. / c.size());
	}
	else if (option == 1) {
		printf("select angle rms(lateral) rms > %5.4lf %d --> %d(%4.1lf%%)\n", threshold, int(c.size()), int(ret.size()), ret.size()*100. / c.size());
	}
	return ret;
}

void FillGap(mfile0::Mfile m, std::vector<std::pair<int, double>>&gap_list) {
	auto itr = m.chains.begin();
	int flg = 0;
	int count = 0;
	int chain_count = 0;
	int PL;
	while (itr != m.chains.end() && count < m.header.num_all_plate) {
		if (chain_count % 100000 == 0) {
			fprintf(stderr, "\r chain %d / %d ,z Fill %d", chain_count, int(m.chains.size()), count);
		}
		chain_count++;

		for (auto itr_b = itr->basetracks.begin(); itr_b != itr->basetracks.end(); itr_b++) {
			flg = 0;
			PL = int(itr_b->pos / 10);
			if (gap_list.size() > 0) {
				for (int i = 0; i < gap_list.size(); i++) {
					if (gap_list[i].first == PL) {
						flg = 1;
						break;
					}
				}
			}
			if (flg == 0) {
				gap_list.push_back(std::make_pair(PL, itr_b->z));
				count++;
			}
		}
		itr++;
	}
	fprintf(stderr, "\r chain %d / %d ,z Fill %d\n", chain_count, int(m.chains.size()), count);

	sort(gap_list.begin(), gap_list.end());
	/*
	for (auto itr = gap_list.begin(); itr != gap_list.end(); itr++) {
		printf("PL%03d z %8.1lf\n", itr->first, itr->second);
	}
	*/
}
void MakePrediction(mfile0::Mfile m, std::vector<Prediction> &pre_v, int PL, std::vector<std::pair<int, double>>gap_list) {
	int z_flg = 0;
	//double の宣言のみ
	double Pred_z;
	//predictionを作るzを求める
	for (auto itr = gap_list.begin(); itr != gap_list.end(); itr++) {
		if (PL == itr->first) {
			Pred_z = itr->second;
			z_flg = 1;
			break;
		}
	}
	if (z_flg == 0) {
		fprintf(stderr, "Function err [MakePrediction] no Prediction z\n");
		exit(1);
	}

	int flg = 0;
	int PL_tmp;
	int h_flg = 0;
	int Ex_PL_id = 0;
	//PLがECCの端の場合
	if (PL == int(m.header.all_pos[0] / 10) || PL == int(m.header.all_pos[m.header.all_pos.size() - 1] / 10)) {
		for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
			flg = 0;
			h_flg = 0;
			for (auto itr_b = itr->basetracks.begin(); itr_b != itr->basetracks.end(); itr_b++) {
				PL_tmp = int(itr_b->pos / 10);
				if (abs(PL - PL_tmp) == 1 || abs(PL - PL_tmp) == 2) {
					flg++;
				}
				if (abs(PL - PL_tmp) == 0) {
					h_flg = 1;
				}

			}
			if (flg == 2) {
				Prediction * pre = new Prediction();
				pre->PL = PL;
				pre->ChainID = itr->chain_id;
				pre->ax = mfile0::chain_ax(*itr);
				pre->ay = mfile0::chain_ay(*itr);
				pre->z = Pred_z;
				if (h_flg == 1) {
					pre->Hit = 1;
				}
				else {
					pre->Hit = 0;
				}

				//最下流
				if (PL == (m.header.all_pos[0] / 10)) {
					Ex_PL_id = h_flg;
				}
				//最上流
				else {
					Ex_PL_id = itr->nseg - 1 - h_flg;
				}

				pre->x = pre->ax*(Pred_z - itr->basetracks[Ex_PL_id].z) + itr->basetracks[Ex_PL_id].x;
				pre->y = pre->ay*(Pred_z - itr->basetracks[Ex_PL_id].z) + itr->basetracks[Ex_PL_id].y;

				pre_v.push_back(*pre);

			}

		}
	}
	//それ以外
	else {
		for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
			flg = 0;
			h_flg = 0;
			for (int i = 0; i < itr->basetracks.size(); i++) {
				PL_tmp = int(itr->basetracks[i].pos / 10);
				if (abs(PL - PL_tmp) == 1) {
					flg++;
					if (PL - PL_tmp == 1) {
						Ex_PL_id = i;
					}
				}
				if (abs(PL - PL_tmp) == 0) {
					h_flg = 1;
				}

			}
			if (flg == 2) {
				Prediction * pre = new Prediction();
				pre->PL = PL;
				pre->ChainID = itr->chain_id;
				pre->ax = mfile0::chain_ax(*itr);
				pre->ay = mfile0::chain_ay(*itr);
				pre->z = Pred_z;
				if (h_flg == 1) {
					pre->Hit = 1;
				}
				else {
					pre->Hit = 0;
				}

				//近い方のPLから外挿
				if (fabs(pre->z - itr->basetracks[Ex_PL_id].z) > fabs(pre->z - itr->basetracks[Ex_PL_id + 1 + h_flg].z)) {
					Ex_PL_id = Ex_PL_id + 1 + h_flg;
				}

				pre->x = pre->ax*(Pred_z - itr->basetracks[Ex_PL_id].z) + itr->basetracks[Ex_PL_id].x;
				pre->y = pre->ay*(Pred_z - itr->basetracks[Ex_PL_id].z) + itr->basetracks[Ex_PL_id].y;

				pre_v.push_back(*pre);

			}
		}
	}
}

void ClacEfficiencyPosition(std::vector<Prediction> pre, std::vector<std::vector<EfficiencyPositionMap>> &eff_map, double area_size) {
	double area[4];
	int PL;
	for (auto itr = pre.begin(); itr != pre.end(); itr++) {
		if (itr == pre.begin()) {
			area[0] = itr->x;
			area[1] = itr->x;
			area[2] = itr->y;
			area[3] = itr->y;
			PL = itr->PL;
		}
		if (area[0] > itr->x) {
			area[0] = itr->x;
		}
		if (area[1] < itr->x) {
			area[1] = itr->x;
		}
		if (area[2] > itr->y) {
			area[2] = itr->y;
		}
		if (area[3] < itr->y) {
			area[3] = itr->y;
		}
	}

	int ix_size = int((area[1] - area[0]) / area_size) + 1;
	int iy_size = int((area[3] - area[2]) / area_size) + 1;
	// ix*area_size <= x-area[0] < (ix+1)*area_size

	// ix*area_size+area[0] < x < (ix+1)*area_size+area[0]
	// ix < (x-area[0])/area_size < ix+1
	//printf("PL%03d\n", PL);
	//printf("data x:(%8.1lf , %8.1lf) y:(%8.1lf , %8.1lf)\n", area[0], area[1], area[2], area[3]);
	//printf("hash x:(%8.1lf , %8.1lf) y:(%8.1lf , %8.1lf)\n", area[0], (ix_size )*area_size + area[0], area[2], (iy_size )*area_size + area[2]);


	std::vector<EfficiencyPositionMap> eff_tmp_v;
	for (int ix = 0; ix < ix_size; ix++) {
		eff_map.push_back(eff_tmp_v);
		for (int iy = 0; iy < iy_size; iy++) {
			EfficiencyPositionMap *eff_tmp = new EfficiencyPositionMap();
			eff_tmp->xmin = ix * area_size + area[0];
			eff_tmp->xmax = (ix + 1)*area_size + area[0];
			eff_tmp->ymin = iy * area_size + area[2];
			eff_tmp->ymax = (iy + 1)*area_size + area[2];
			eff_tmp->PL = PL;
			eff_tmp->all = 0;
			eff_tmp->exist = 0;
			eff_map[ix].push_back(*eff_tmp);
		}
	}
	//Hit or No Hit
	int ix, iy;
	double angle_cut = 4.0;
	for (auto itr = pre.begin(); itr != pre.end(); itr++) {
		//角度カット
		if (fabs(itr->ax) > angle_cut)continue;
		if (fabs(itr->ay) > angle_cut)continue;

		// ix <= (x-area[0])/area_size < ix+1
		ix = int((itr->x - area[0]) / area_size);
		iy = int((itr->y - area[2]) / area_size);
		eff_map[ix][iy].all++;
		if (itr->Hit == 1) {
			eff_map[ix][iy].exist++;
		}
	}

	//Efficiencyの計算
	for (int ix = 0; ix < ix_size; ix++) {
		for (int iy = 0; iy < iy_size; iy++) {
			if (eff_map[ix][iy].exist < 1) {
				eff_map[ix][iy].eff = 0;
				eff_map[ix][iy].err = 0;
			}
			else {
				eff_map[ix][iy].eff = eff_map[ix][iy].exist*1.0 / eff_map[ix][iy].all;
				eff_map[ix][iy].err = 1.0 / eff_map[ix][iy].all*sqrt(eff_map[ix][iy].exist*(1.0 - eff_map[ix][iy].eff));
			}
		}
	}

}
void ClacEfficiencyAngle(std::vector<Prediction> pre, std::vector<std::vector<EfficiencyAngleMap>> &eff_map, double area_size) {
	double area[4];
	int PL = pre.begin()->PL;
	area[0] = -5.0;
	area[1] = 5.0;
	area[2] = -5.0;
	area[3] = 5.0;

	int ix_size = int((area[1] - area[0]) / area_size) + 1;
	int iy_size = int((area[3] - area[2]) / area_size) + 1;
	// ix*area_size <= x-area[0] < (ix+1)*area_size

	// ix*area_size+area[0] < x < (ix+1)*area_size+area[0]
	// ix < (x-area[0])/area_size < ix+1
	//printf("PL%03d\n", PL);
	//printf("data x:(%8.1lf , %8.1lf) y:(%8.1lf , %8.1lf)\n", area[0], area[1], area[2], area[3]);
	//printf("hash x:(%8.1lf , %8.1lf) y:(%8.1lf , %8.1lf)\n", area[0], (ix_size )*area_size + area[0], area[2], (iy_size )*area_size + area[2]);

	for (int ix = 0; ix < ix_size; ix++) {
		std::vector<EfficiencyAngleMap> eff_tmp_v;
		for (int iy = 0; iy < iy_size; iy++) {
			EfficiencyAngleMap *eff_tmp = new EfficiencyAngleMap();
			eff_tmp->axmin = ix * area_size + area[0];
			eff_tmp->axmax = (ix + 1)*area_size + area[0];
			eff_tmp->aymin = iy * area_size + area[2];
			eff_tmp->aymax = (iy + 1)*area_size + area[2];
			eff_tmp->PL = PL;
			eff_tmp->all = 0;
			eff_tmp->exist = 0;
			eff_tmp_v.push_back(*eff_tmp);
		}
		eff_map.push_back(eff_tmp_v);
	}

	//Hit or No Hit
	int ix, iy;
	for (auto itr = pre.begin(); itr != pre.end(); itr++) {
		// ix <= (x-area[0])/area_size < ix+1
		ix = int((itr->ax - area[0]) / area_size);
		iy = int((itr->ay - area[2]) / area_size);
		if (0 > ix || ix >= ix_size)continue;
		if (0 > iy || iy >= iy_size)continue;
		eff_map[ix][iy].all++;
		if (itr->Hit == 1) {
			eff_map[ix][iy].exist++;
		}
	}

	//Efficiencyの計算
	for (int ix = 0; ix < ix_size; ix++) {
		for (int iy = 0; iy < iy_size; iy++) {
			if (eff_map[ix][iy].exist < 1) {
				eff_map[ix][iy].eff = 0;
				eff_map[ix][iy].err = 0;
			}
			else {
				eff_map[ix][iy].eff = eff_map[ix][iy].exist*1.0 / eff_map[ix][iy].all;
				eff_map[ix][iy].err = 1.0 / eff_map[ix][iy].all*sqrt(eff_map[ix][iy].exist*(1.0 - eff_map[ix][iy].eff));
			}
		}
	}

}
void ClacEfficiencyPL(std::vector<Prediction> pre, std::vector<EfficiencyPLCorrelation> &eff_map, double edge_cut) {
	double area[4];
	int PL;
	for (auto itr = pre.begin(); itr != pre.end(); itr++) {
		if (itr == pre.begin()) {
			area[0] = itr->x;
			area[1] = itr->x;
			area[2] = itr->y;
			area[3] = itr->y;
			PL = itr->PL;
		}
		if (area[0] > itr->x) {
			area[0] = itr->x;
		}
		if (area[1] < itr->x) {
			area[1] = itr->x;
		}
		if (area[2] > itr->y) {
			area[2] = itr->y;
		}
		if (area[3] < itr->y) {
			area[3] = itr->y;
		}
	}
	area[0] = area[0] + edge_cut;
	area[1] = area[1] - edge_cut;
	area[2] = area[2] + edge_cut;
	area[3] = area[3] - edge_cut;
	for (int i = 0; i < 9; i++) {
		EfficiencyPLCorrelation *eff_tmp = new EfficiencyPLCorrelation();
		eff_tmp->xmin = area[0];
		eff_tmp->xmax = area[1];
		eff_tmp->ymin = area[2];
		eff_tmp->ymax = area[3];
		eff_tmp->PL = PL;
		eff_tmp->exist = 0;
		eff_tmp->all = 0;
		switch (i) {
		case 0:
			eff_tmp->angmin = 0.0;
			eff_tmp->angmax = 0.3;
			break;
		case 1:
			eff_tmp->angmin = 0.4;
			eff_tmp->angmax = 0.6;
			break;
		case 2:
			eff_tmp->angmin = 0.9;
			eff_tmp->angmax = 1.1;
			break;
		case 3:
			eff_tmp->angmin = 1.4;
			eff_tmp->angmax = 1.6;
			break;
		case 4:
			eff_tmp->angmin = 1.9;
			eff_tmp->angmax = 2.1;
			break;
		case 5:
			eff_tmp->angmin = 2.4;
			eff_tmp->angmax = 2.6;
			break;
		case 6:
			eff_tmp->angmin = 2.9;
			eff_tmp->angmax = 3.1;
			break;
		case 7:
			eff_tmp->angmin = 3.4;
			eff_tmp->angmax = 3.6;
			break;
		case 8:
			eff_tmp->angmin = 3.9;
			eff_tmp->angmax = 4.1;
			break;
		default:
			eff_tmp->angmin = 0.0;
			eff_tmp->angmax = 0.0;
		}
		eff_map.push_back(*eff_tmp);
	}

	//Hit or No Hit
	int i_ang;
	double angle;
	for (auto itr = pre.begin(); itr != pre.end(); itr++) {
		angle = sqrt(pow(itr->ax, 2.) + pow(itr->ay, 2.));
		i_ang = -1;
		for (int i = 0; i < 9; i++) {
			if (eff_map[i].angmin <= angle && angle <= eff_map[i].angmax) {
				i_ang = i;
				break;
			}
		}
		if (i_ang < 0)continue;
		if (itr->x < area[0])continue;
		if (itr->x > area[1])continue;
		if (itr->y < area[2])continue;
		if (itr->y > area[3])continue;
		eff_map[i_ang].all++;
		if (itr->Hit == 1) {
			eff_map[i_ang].exist++;
		}
	}

	//Efficiencyの計算
	for (int i = 0; i < 9; i++) {

		if (eff_map[i].exist < 1) {
			eff_map[i].eff = 0;
			eff_map[i].err = 0;
		}
		else {
			eff_map[i].eff = eff_map[i].exist*1.0 / eff_map[i].all;
			eff_map[i].err = 1.0 / eff_map[i].all*sqrt(eff_map[i].exist*(1.0 - eff_map[i].eff));
		}
	}

}

void WriteEfficiencyPosition(std::string filename, std::vector<std::vector<std::vector<EfficiencyPositionMap>>> eff_map) {
	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	int count = 0;
	int max = 0;
	for (int i = 0; i < eff_map.size(); i++) {
		for (int ix = 0; ix < eff_map[i].size(); ix++) {
			for (int iy = 0; iy < eff_map[i][ix].size(); iy++) {
				max++;
			}
		}
	}
	for (int i = 0; i < eff_map.size(); i++) {
		for (int ix = 0; ix < eff_map[i].size(); ix++) {
			for (int iy = 0; iy < eff_map[i][ix].size(); iy++) {
				if (count % 1000 == 0) {
					fprintf(stderr, "\r Write Efficiency Position %d /%d (%4.1lf%%)", count, max, count*100. / max);
				}
				count++;
				ofs << std::right << std::fixed
					<< std::setw(4) << eff_map[i][ix][iy].PL << " "
					<< std::setw(10) << std::setprecision(1) << eff_map[i][ix][iy].xmin << " "
					<< std::setw(10) << std::setprecision(1) << eff_map[i][ix][iy].xmax << " "
					<< std::setw(10) << std::setprecision(1) << eff_map[i][ix][iy].ymin << " "
					<< std::setw(10) << std::setprecision(1) << eff_map[i][ix][iy].ymax << " "
					<< std::setw(10) << eff_map[i][ix][iy].exist << " "
					<< std::setw(10) << eff_map[i][ix][iy].all << " "
					<< std::setw(5) << std::setprecision(4) << eff_map[i][ix][iy].eff << " "
					<< std::setw(5) << std::setprecision(4) << eff_map[i][ix][iy].err << std::endl;

			}
		}
	}
	fprintf(stderr, "\r Write Efficiency Position %d /%d (%4.1lf%%)\n", count, max, count*100. / max);

	ofs.close();

}
void WriteEfficiencyAngle(std::string filename, std::vector<std::vector<std::vector<EfficiencyAngleMap>>> eff_map) {
	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	int count = 0;
	int max = 0;
	for (int i = 0; i < eff_map.size(); i++) {
		for (int ix = 0; ix < eff_map[i].size(); ix++) {
			for (int iy = 0; iy < eff_map[i][ix].size(); iy++) {
				max++;
			}
		}
	}

	for (int i = 0; i < eff_map.size(); i++) {
		for (int ix = 0; ix < eff_map[i].size(); ix++) {
			for (int iy = 0; iy < eff_map[i][ix].size(); iy++) {
				if (count % 1000 == 0) {
					fprintf(stderr, "\r Write Efficiency Angle %d /%d (%4.1lf%%)", count, max, count*100. / max);
				}
				count++;

				ofs << std::right << std::fixed
					<< std::setw(4) << eff_map[i][ix][iy].PL << " "
					<< std::setw(6) << std::setprecision(2) << eff_map[i][ix][iy].axmin << " "
					<< std::setw(6) << std::setprecision(2) << eff_map[i][ix][iy].axmax << " "
					<< std::setw(6) << std::setprecision(2) << eff_map[i][ix][iy].aymin << " "
					<< std::setw(6) << std::setprecision(2) << eff_map[i][ix][iy].aymax << " "
					<< std::setw(10) << eff_map[i][ix][iy].exist << " "
					<< std::setw(10) << eff_map[i][ix][iy].all << " "
					<< std::setw(5) << std::setprecision(4) << eff_map[i][ix][iy].eff << " "
					<< std::setw(5) << std::setprecision(4) << eff_map[i][ix][iy].err << std::endl;

			}
		}
	}
	fprintf(stderr, "\r Write Efficiency Angle %d /%d (%4.1lf%%)\n", count, max, count*100. / max);

	ofs.close();

}
void WriteEfficiencyPL(std::string filename, std::vector<std::vector<EfficiencyPLCorrelation>> eff_map) {
	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	int count = 0;
	int max = 0;
	for (int i = 0; i < eff_map.size(); i++) {
		for (int j = 0; j < eff_map[i].size(); j++) {
			max++;
		}
	}

	for (int i = 0; i < eff_map.size(); i++) {
		for (int j = 0; j < eff_map[i].size(); j++) {
			if (count % 1000 == 0) {
				fprintf(stderr, "\r Write Efficiency PL %d /%d (%4.1lf%%)", count, max, count*100. / max);
			}
			count++;

			ofs << std::right << std::fixed
				<< std::setw(4) << eff_map[i][j].PL << " "
				<< std::setw(10) << std::setprecision(1) << eff_map[i][j].xmin << " "
				<< std::setw(10) << std::setprecision(1) << eff_map[i][j].xmax << " "
				<< std::setw(10) << std::setprecision(1) << eff_map[i][j].ymin << " "
				<< std::setw(10) << std::setprecision(1) << eff_map[i][j].ymax << " "
				<< std::setw(6) << std::setprecision(2) << eff_map[i][j].angmin << " "
				<< std::setw(6) << std::setprecision(2) << eff_map[i][j].angmax << " "
				<< std::setw(10) << eff_map[i][j].exist << " "
				<< std::setw(10) << eff_map[i][j].all << " "
				<< std::setw(5) << std::setprecision(4) << eff_map[i][j].eff << " "
				<< std::setw(5) << std::setprecision(4) << eff_map[i][j].err << std::endl;

		}
	}
	fprintf(stderr, "\r Write Efficiency PL %d /%d (%4.1lf%%)\n", count, max, count*100. / max);

	ofs.close();

}
void WritePrediction(std::string filename, std::vector<std::vector<Prediction>> pred) {
	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	int count = 0;
	int max = 0;
	for (int i = 0; i < pred.size(); i++) {
		sort(pred[i].begin(), pred[i].end(), sort_pred_ChainID);
		for (int j = 0; j < pred[i].size(); j++) {
			max++;
		}
	}

	for (int i = 0; i < pred.size(); i++) {
		for (int j = 0; j < pred[i].size(); j++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write Prediction %d /%d (%4.1lf%%)", count, max, count*100. / max);
			}
			count++;

			ofs << std::right << std::fixed
				<< std::setw(4) << pred[i][j].PL << " "
				<< std::setw(10) << pred[i][j].ChainID << " "
				<< std::setw(7) << std::setprecision(4) << pred[i][j].ax << " "
				<< std::setw(7) << std::setprecision(4) << pred[i][j].ay << " "
				<< std::setw(10) << std::setprecision(1) << pred[i][j].x << " "
				<< std::setw(10) << std::setprecision(1) << pred[i][j].y << " "
				<< std::setw(10) << std::setprecision(1) << pred[i][j].z << " "
				<< std::setw(1) << pred[i][j].Hit << std::endl;
		}

	}
	fprintf(stderr, "\r Write Prediction %d /%d (%4.1lf%%)\n", count, max, count*100. / max);
	ofs.close();

}

std::vector<Prediction> Prediction_Area_cut(std::vector<Prediction> pred, double x_min, double x_max, double y_min, double y_max) {
	std::vector<Prediction> ret;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		if (itr->x < x_min)continue;
		if (itr->x > x_max)continue;
		if (itr->y < y_min)continue;
		if (itr->y > y_max)continue;
		ret.push_back(*itr);
	}
	return ret;
}
std::vector<Prediction> Prediction_Angle_cut(std::vector<Prediction> pred, double x_min, double x_max, double y_min, double y_max) {
	std::vector<Prediction> ret;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		if (itr->ax < x_min)continue;
		if (itr->ax > x_max)continue;
		if (itr->ay < y_min)continue;
		if (itr->ay > y_max)continue;
		ret.push_back(*itr);
	}
	return ret;
}
