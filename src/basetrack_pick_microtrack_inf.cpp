#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>

class microtrack_inf {
public:
	int zone, pos, col, row, isg, ph, pixelnum, hitnum;
};

std::multimap<std::pair<int, int>, std::tuple<int, int, int>>read_base_id(std::string filename, int pl);
std::vector<vxx::micro_track_t> read_micro(std::string file_path, std::string filename, int num, int pos, int zone0, int zone1, std::multimap<std::pair<int, int>, std::tuple<int, int, int>>&base_id);
void fvxx_merge(std::vector<vxx::micro_track_t> &all, std::vector<vxx::micro_track_t> &m);
void write_fvxx_inf(std::string filename, std::vector<vxx::micro_track_t>&m);

int main(int argc, char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-bvxx pl area fvxx-folder out-file\n");
		fprintf(stderr, "*************[[[ caution ]]]***********\n");
		fprintf(stderr, "this program depend on zone structure\n");
		exit(1);
	}

	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	int area = std::stoi(argv[3]);
	std::string path_in_fvxx = argv[4];
	std::string file_out_file = argv[5];

	//microtrackはbasetrack中のpos,zone / col,row,isgでuniqueになる。
	std::multimap<std::pair<int, int>, std::tuple<int, int, int>>base_id = read_base_id(file_in_bvxx, pl);

	//zone構造に依存する部分
	//fvxxのzoneは小さいほうを割り当てる
	std::vector<vxx::micro_track_t> f1_thick_0 = read_micro(path_in_fvxx, "thick", 0, pl * 10 + 1, area + 6 * 0, area + 6 * 1, base_id);
	std::vector<vxx::micro_track_t> f2_thick_0 = read_micro(path_in_fvxx, "thick", 0, pl * 10 + 2, area + 6 * 0, area + 6 * 2, base_id);
	std::vector<vxx::micro_track_t> f1_thick_1 = read_micro(path_in_fvxx, "thick", 1, pl * 10 + 1, area + 6 * 2, area + 6 * 3, base_id);
	std::vector<vxx::micro_track_t> f2_thick_1 = read_micro(path_in_fvxx, "thick", 1, pl * 10 + 2, area + 6 * 1, area + 6 * 3, base_id);

	std::vector<vxx::micro_track_t> f1_thin_0 = read_micro(path_in_fvxx, "thin", 0, pl * 10 + 1, area + 6 * 4, area + 6 * 5, base_id);
	std::vector<vxx::micro_track_t> f2_thin_0 = read_micro(path_in_fvxx, "thin", 0, pl * 10 + 2, area + 6 * 4, area + 6 * 6, base_id);
	std::vector<vxx::micro_track_t> f1_thin_1 = read_micro(path_in_fvxx, "thin", 1, pl * 10 + 1, area + 6 * 6, area + 6 * 7, base_id);
	std::vector<vxx::micro_track_t> f2_thin_1 = read_micro(path_in_fvxx, "thin", 1, pl * 10 + 2, area + 6 * 5, area + 6 * 7, base_id);


	std::vector<vxx::micro_track_t> fvxx;
	if (f1_thick_0.size() != 0)fvxx_merge(fvxx, f1_thick_0);
	if (f2_thick_0.size() != 0)fvxx_merge(fvxx, f2_thick_0);
	if (f1_thick_1.size() != 0)fvxx_merge(fvxx, f1_thick_1);
	if (f2_thick_1.size() != 0)fvxx_merge(fvxx, f2_thick_1);
	if (f1_thin_0.size() != 0)fvxx_merge(fvxx, f1_thin_0);
	if (f2_thin_0.size() != 0)fvxx_merge(fvxx, f2_thin_0);
	if (f1_thin_1.size() != 0)fvxx_merge(fvxx, f1_thin_1);
	if (f2_thin_1.size() != 0)fvxx_merge(fvxx, f2_thin_1);

	printf("all fvxx size=%d\n", fvxx.size());
	write_fvxx_inf(file_out_file, fvxx);

}

std::multimap<std::pair<int, int>, std::tuple<int, int, int>>read_base_id(std::string filename, int pl) {
	std::multimap<std::pair<int, int>, std::tuple<int, int, int>> ret;

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(filename, pl, 0);
	std::map<int, vxx::micro_track_subset_t> pos0, pos1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		pos0.insert(std::make_pair(itr->m[0].rawid, itr->m[0]));
		pos1.insert(std::make_pair(itr->m[1].rawid, itr->m[1]));
	}
	for (auto itr = pos0.begin(); itr != pos0.end(); itr++) {
		ret.insert(std::make_pair(std::make_pair(itr->second.pos, itr->second.zone), std::make_tuple(itr->second.col, itr->second.row, itr->second.isg)));
	}
	for (auto itr = pos1.begin(); itr != pos1.end(); itr++) {
		ret.insert(std::make_pair(std::make_pair(itr->second.pos, itr->second.zone), std::make_tuple(itr->second.col, itr->second.row, itr->second.isg)));
	}

	return ret;
}
std::vector<vxx::micro_track_t> read_micro(std::string file_path,std::string filename,int num, int pos, int zone0, int zone1, std::multimap<std::pair<int, int>, std::tuple<int, int, int>>&base_id) {
	//fxxx1_thick_0.vxx zone0=area,zone1=area+6

	std::set<std::tuple<int, int, int>> micro_id;
	if (base_id.count(std::make_pair(pos, zone0)) != 0) {
		auto range = base_id.equal_range(std::make_pair(pos, zone0));
		for (auto itr = range.first; itr != range.second; itr++) {
			micro_id.insert(itr->second);
		}
	}
	if (base_id.count(std::make_pair(pos, zone1)) != 0) {
		auto range = base_id.equal_range(std::make_pair(pos, zone1));
		for (auto itr = range.first; itr != range.second; itr++) {
			micro_id.insert(itr->second);
		}
	}

	std::vector<vxx::micro_track_t> ret;
	if (micro_id.size() == 0)return ret;

	std::stringstream fvxxname;
	fvxxname << file_path << "\\f" << std::setw(4) << std::setfill('0') << pos << "_" << filename << "_" << std::setw(1) << num << ".vxx";

	int read_track_num = 100 * 1000 * 1000;
	int microtrack_num = -1;
	int id_range = 0;

	std::tuple<int, int, int> id;

	while (microtrack_num != 0) {
		microtrack_num = 0;
		vxx::FvxxReader fr;

		std::array<int, 2> index = { id_range*read_track_num, (id_range + 1)*read_track_num };//1234<=rawid<=5678であるようなものだけを読む。
		//std::array<int, 2> index = { 0,1000000};//1234<=rawid<=5678であるようなものだけを読む。

		std::vector<vxx::micro_track_t> micro;
		micro.reserve(microtrack_num);
		micro = fr.ReadAll(fvxxname.str(), pos, 0, vxx::opt::index = index);
		microtrack_num = micro.size();
		id_range++;
		for (auto itr = micro.begin(); itr != micro.end(); itr++) {
			std::get<0>(id) = itr->col;
			std::get<1>(id) = itr->row;
			std::get<2>(id) = itr->isg;
			if (micro_id.count(id)==1) {
				itr->zone = std::min(zone0, zone1);
				ret.push_back(*itr);
			}
		}
		micro.clear();
	}


	printf("read fvxx %d --> %d\n", micro_id.size(), ret.size());
	return ret;
}

void rewrite_vph(std::vector<vxx::base_track_t> &base, std::map<std::tuple<int, int, int>, vxx::micro_track_t *>  &micro_map) {
	int pos_id;
	if (base.begin()->m[0].pos == micro_map.begin()->second->pos) {
		pos_id = 0;
	}
	else if (base.begin()->m[1].pos == micro_map.begin()->second->pos) {
		pos_id = 1;
	}
	else {
		fprintf(stderr, "pos not match\n");
		exit(1);
	}

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		auto res = micro_map.find(std::make_tuple(itr->m[pos_id].col, itr->m[pos_id].row, itr->m[pos_id].isg));
		if (res == micro_map.end()) {
			fprintf(stderr, "id =%d not found\n", itr->rawid);
		}
		else {
			itr->m[pos_id].ph = (itr->m[pos_id].ph / 10000) * 10000 + (int)res->second->px;
		}
	}
}
void fvxx_merge(std::vector<vxx::micro_track_t> &all, std::vector<vxx::micro_track_t> &m) {
	int all_size = all.size();
	all.reserve(all.size() + m.size());
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		all.push_back(*itr);
	}
	printf("fvxx merge %d + %d = %d\n", all_size, m.size(), all.size());
 }

void write_fvxx_inf(std::string filename,std::vector<vxx::micro_track_t>&m) {
	//suzuki code<--保存されていない
	//ret.px = m.ph2 + m.pixelnum * 100;
	//ret.py = m.hitnum;


	//yoshimoto code
	//int pc_ph()const { return num & 0x1f; }
	//int pc_vol()const { return vola; }
	//int pc_area()const { return num >> 5; }
	std::vector<microtrack_inf> m_inf;
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		int num = itr->px;
		int vola = itr->py;

		microtrack_inf m_tmp;
		m_tmp.zone = itr->zone;
		m_tmp.pos = itr->pos;
		m_tmp.col = itr->col;
		m_tmp.row = itr->row;
		m_tmp.isg = itr->isg;
		m_tmp.pixelnum= num >> 5;
		m_tmp.ph = num & 0x1f;
		m_tmp.hitnum = vola;
		m_inf.push_back(m_tmp);
	}

	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (m_inf.size() == 0) {
		fprintf(stderr, "target file ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = m_inf.size();
	for (int i = 0; i < m_inf.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& m_inf[i], sizeof(microtrack_inf));
		//printf("%d %d %d %d %d %d\n", m_inf[i].col, m_inf[i].row, m_inf[i].isg, m_inf[i].ph, m_inf[i].pixelnum, m_inf[i].hitnum);
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();
}


