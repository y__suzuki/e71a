#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>


std::vector<std::pair<netscan::base_track_t , netscan::base_track_t >> base_matching(std::vector<netscan::base_track_t> &base1, std::vector<netscan::base_track_t> &base2);
int base_match_allowance(netscan::base_track_t base1, netscan::base_track_t base2, double d_pos_r0, double d_pos_r1, double d_pos_l, double d_ang_r0, double d_ang_r1, double d_ang_l);

int main(int argc, char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-bvxx1 in-bvxx2 PL zone\n");
		exit(1);
	}

	//input value
	std::string in_file_base1 = argv[1];
	std::string in_file_base2 = argv[2];
	int pl = std::stoi(argv[3]);
	int zone = std::stoi(argv[4]);

	//basetrack data
	std::vector<netscan::base_track_t> base1;
	netscan::read_basetrack_extension(in_file_base1, base1, pl, zone);

	std::vector<netscan::base_track_t> base2;
	netscan::read_basetrack_extension(in_file_base2, base2, pl, zone);

	
	std::vector<std::pair<netscan::base_track_t, netscan::base_track_t >> pair = base_matching(base1, base2);


	


}
std::vector<std::pair<netscan::base_track_t , netscan::base_track_t >> base_matching(std::vector<netscan::base_track_t> &base1, std::vector<netscan::base_track_t> &base2) {

	std::multimap<std::pair<int, int>, netscan::base_track_t*> base_hash;
	double x_min, y_min;
	double hash = 1000;
	for (auto itr = base2.begin(); itr != base2.end(); itr++) {
		if (itr == base2.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(x_min, itr->x);
		y_min = std::min(y_min, itr->y);

	}
	int ix, iy;
	std::pair<int, int> index;
	for (auto itr = base2.begin(); itr != base2.end(); itr++) {
		ix = (itr->x - x_min) / hash;
		iy = (itr->y - y_min) / hash;
		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				base_hash.insert(std::make_pair(std::make_pair(ix + iix, iy + iiy), &(*itr)));
			}
		}
	}

	std::vector<std::pair<netscan::base_track_t , netscan::base_track_t >> ret;
	for (auto itr = base1.begin(); itr!=base1.end(); itr++) {
		ix = (itr->x - x_min) / hash;
		iy = (itr->y - y_min) / hash; 
		index.first = ix;
		index.second = iy;
		if (base_hash.count(index) == 0)continue;
		else if (base_hash.count(index) == 1) {
			auto res = base_hash.find(index);
			if (base_match_allowance(*itr, *(res->second), 10, 10, 10, 0.1, 0.1, 0.1)) {
				ret.push_back(std::make_pair(*itr, *(res->second)));
			}
		}
		else {
			auto range= base_hash.equal_range(index);
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (base_match_allowance(*itr, *(itr2->second), 10, 10, 10, 0.1, 0.1, 0.1)) {
					ret.push_back(std::make_pair(*itr, *(itr2->second)));
					//�d���h�~
					break;
				}
			}
		}
	}
	return ret;
}
int base_match_allowance(netscan::base_track_t base1, netscan::base_track_t base2, double d_pos_r0, double d_pos_r1, double d_pos_l, double d_ang_r0, double d_ang_r1, double d_ang_l) {
	double angle, dpos[2], dang[2],dpos_rl[2], dang_rl[2];
	angle = sqrt(base1.ax*base1.ax + base1.ay*base1.ay);
	dpos[0] = base1.x - base2.x;
	dpos[1] = base1.y - base2.y;
	dang[0] = base1.ax - base2.ax;
	dang[1] = base1.ay - base2.ay;

	dpos_rl[0] = (dpos[0] * base1.ax + dpos[1] * base1.ay) / angle;
	dpos_rl[1] = (dpos[0] * base1.ay - dpos[1] * base1.ax) / angle;
	dang_rl[0] = (dang[0] * base1.ax + dang[1] * base1.ay) / angle;
	dang_rl[1] = (dang[0] * base1.ay - dang[1] * base1.ax) / angle;

	if (fabs(dpos_rl[0]) > d_pos_r0 + d_pos_r1 * angle)return 0;
	if (fabs(dpos_rl[1]) > d_pos_l)return 0;
	if (fabs(dang_rl[0]) > d_ang_r0 + d_ang_r1 * angle)return 0;
	if (fabs(dang_rl[1]) > d_ang_l)return 0;

	printf("%10d %10d %5.4lf %5.4lf %5.2lf %5.2lf %5.4lf %5.4lf\n", base1.rawid, base2.rawid, base1.ax, base1.ay, dpos_rl[0], dpos_rl[1], dang_rl[0], dang_rl[1]);
	return 1;
}