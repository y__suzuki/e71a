//#define _CRT_SECURE_NO_WARNINGS　
#include <fstream>
#include <iostream>
#include <ios>
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <map>
#include <windows.h>
#include <atlstr.h>

std::vector<std::string> read_view_list(std::string filename);
CString Set_m2b_arg(std::string ED, std::string VIEW, std::string RC, std::string DC, int area, int pl);
CString Set_ranking_arg(std::string in_bvxx, std::string out_bvxx, int pl, int zone, std::string param_xy, std::string param_lat);
CString Set_b_join_arg(std::vector<std::string> in_bvxx, std::string out_bvxx, int pl);

bool checkFileExistence(const std::string& str);
int main(int argc, char**argv) {
	if (argc != 11) {
		fprintf(stderr, "usage;prg ED RC DC view_list out_base_ED out_bvxx pl zone param param_lat\n");
		exit(1);
	}
	std::string file_in_EventDescriptor = argv[1];
	std::string file_in_m2b_rc = argv[2];
	std::string file_in_dc = argv[3];
	std::string file_in_view_list = argv[4];

	std::string file_out_base_ED = argv[5];
	std::string file_out_bvxx = argv[6];
	int pl = std::stoi(argv[7]);
	int area = std::stoi(argv[8]);
	std::string file_in_param_xy = argv[9];
	std::string file_in_param_lat = argv[10];
	std::vector<std::string> bvxx_intermediate_output;
	std::vector<std::string> view_list = read_view_list(file_in_view_list);

	PROCESS_INFORMATION p_m2b, p_ranking,p_b_join;
	STARTUPINFO s_m2b, s_ranking, s_b_join;
	ZeroMemory(&s_m2b, sizeof(s_m2b));
	ZeroMemory(&s_ranking, sizeof(s_ranking));
	ZeroMemory(&s_b_join, sizeof(s_b_join));
	s_m2b.cb = sizeof(s_m2b);
	s_ranking.cb = sizeof(s_ranking);
	s_b_join.cb = sizeof(s_b_join);
	int DIVIDE = 10;
	const int number_of_area = view_list.size() / DIVIDE;
	int area_id = 0;
	for (int i = 0; i <= DIVIDE; i++) {
		printf("proccess %d/%d\n", i, DIVIDE);
		std::string file_in_area_buf = "tmp_area.txt";
		std::ofstream ofs(file_in_area_buf);
		//number_of_area = 10視野分書き出す
		if (area_id >= view_list.size())continue;
		for (int j = 0; j < number_of_area; j++) {
			if (area_id >= view_list.size())continue;
			ofs << view_list[area_id] << std::endl;
			area_id++;
		}
		ofs.close();
		CString m2b_argument = Set_m2b_arg(file_in_EventDescriptor, file_in_area_buf, file_in_m2b_rc, file_in_dc, area, pl);
		LPTSTR  m2b_arg = new TCHAR[m2b_argument.GetLength() + 1];
		_tcscpy(m2b_arg, m2b_argument);

		//m2bを実行
		int ret = CreateProcess(
			"F:\\NINJA\\E71a\\ECC5\\Layer32\\work\\Basetrack\\m2b_new\\m2b.exe", // 実行可能モジュールの名
			m2b_arg, // コマンドラインの文字列
			NULL, // セキュリティ記述子
			NULL,// セキュリティ記述子
			FALSE, // ハンドルの継承オプション
			NULL, // 作成のフラグ
			NULL,// 新しい環境ブロック
			NULL, // カレントディレクトリの名前
			&s_m2b, // スタートアップ情報
			&p_m2b // プロセス情報
		);
		CloseHandle(p_m2b.hThread);
		//終了するまで待つ
		WaitForSingleObject(p_m2b.hProcess, INFINITE);
		CloseHandle(p_m2b.hProcess);
		//fprintf(stderr, "m2b finished\n");

		//ranking cut
		char file_bvxx[256];
		sprintf(file_bvxx, "b%03d_%d.vxx", pl, i);
		bvxx_intermediate_output.push_back(file_bvxx);
		CString ranking_argument = Set_ranking_arg(file_out_base_ED, file_bvxx, pl,0, file_in_param_xy, file_in_param_lat);
		LPTSTR  ranking_arg = new TCHAR[ranking_argument.GetLength() + 1];
		_tcscpy(ranking_arg, ranking_argument);
		ret = CreateProcess(
			"C:\\Users\\suzuki\\source\\repos\\e71a\\bin\\Basetrack_Rankingcut.exe", // 実行可能モジュールの名
			ranking_arg, // コマンドラインの文字列
			NULL, // セキュリティ記述子
			NULL,// セキュリティ記述子
			FALSE, // ハンドルの継承オプション
			NULL, // 作成のフラグ
			NULL,// 新しい環境ブロック
			NULL, // カレントディレクトリの名前
			&s_ranking, // スタートアップ情報
			&p_ranking // プロセス情報
		);
		CloseHandle(p_ranking.hThread);
		//メモ帳が終了するまで待つ
		WaitForSingleObject(p_ranking.hProcess, INFINITE);
		CloseHandle(p_ranking.hProcess);
		//fprintf(stderr, "b_filter finished\n");
	}
	char del_command[256];
	sprintf(del_command, "del %s", file_out_base_ED.c_str());
	system(del_command);
	system("del tmp_area.txt");

	//b_join
	CString b_join_argument = Set_b_join_arg(bvxx_intermediate_output, file_out_bvxx, pl);
	LPTSTR  b_join_arg = new TCHAR[b_join_argument.GetLength() + 1];
	_tcscpy(b_join_arg, b_join_argument);
	int ret = CreateProcess(
		"M:\\data\\NINJA\\NETSCAN\\win-msvc-x64-15_200129\\bin\\b_join.exe", // 実行可能モジュールの名
		b_join_arg, // コマンドラインの文字列
		NULL, // セキュリティ記述子
		NULL,// セキュリティ記述子
		FALSE, // ハンドルの継承オプション
		NULL, // 作成のフラグ
		NULL,// 新しい環境ブロック
		NULL, // カレントディレクトリの名前
		&s_b_join, // スタートアップ情報
		&p_b_join // プロセス情報
	);
	CloseHandle(p_b_join.hThread);
	//メモ帳が終了するまで待つ
	WaitForSingleObject(p_b_join.hProcess, INFINITE);
	CloseHandle(p_b_join.hProcess);
	//fprintf(stderr, "b_filter finished\n");
	for (int i = 0; i < bvxx_intermediate_output.size();i++){
		sprintf(del_command, "del %s", bvxx_intermediate_output[i].c_str());
		system(del_command);
	}
}

std::vector<std::string> read_view_list(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<std::string> ret;
	std::string str;
	while (std::getline(ifs, str)) {
		ret.push_back(str);
	}
	return ret;
}
CString Set_m2b_arg(std::string ED, std::string VIEW, std::string RC, std::string DC, int area, int pl) {
	std::stringstream ss;
	ss << std::setfill(' ') << " " << pl << "-" << area
		<< " --view " << VIEW
		<< " --rc " << RC
		<< " --descriptor " << ED
		<< " --c " << DC;
	//printf("arg[%s]\n", ss.str().c_str());
	CString ret = ss.str().c_str();
	return ret;
	//%NETSCAN%\m2b.exe % 1 - % 2 --view 2000 1300 --rc .. / .. / rc / m2b_thick.rc --descriptor .. / .. / EventDescriptor - 0.ini --c dc - % 1_thick.lst

}
CString Set_ranking_arg(std::string in_bvxx, std::string out_bvxx, int pl, int zone, std::string param_xy, std::string param_lat) {
	std::stringstream ss;
	ss << std::setfill(' ')
		<< " " << in_bvxx
		<< " " << pl
		<< " " << zone
		<< " " << out_bvxx
		<< " " << param_xy
		<< " " << param_lat;
	//printf("arg[%s]\n", ss.str().c_str());
	CString ret = ss.str().c_str();
	return ret;
	//C:\Users\suzuki\source\repos\e71a\bin\Basetrack_Rankingcut.exe b%pl%_thick_%num%.vxx %pl % 0 tmp.vxx param.txt param_lat.txt
}
CString Set_b_join_arg(std::vector<std::string> in_bvxx, std::string out_bvxx, int pl) {
	std::stringstream ss;
	ss << std::setfill(' ')
		<< " " << pl
		<< " " << out_bvxx;
	for (auto itr = in_bvxx.begin(); itr != in_bvxx.end(); itr++) {
		ss << " " << *itr;
	}
	//printf("arg[%s]\n", ss.str().c_str());
	CString ret = ss.str().c_str();
	return ret;
	//usage : b_join pl fname_out fname_in1 fname_in2 ...
}
bool checkFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}

