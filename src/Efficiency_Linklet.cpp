#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>
bool CheckFileExistence(const std::string& str);
std::set<std::pair<int, int>> Connect_Linklet_Selection(std::vector<netscan::linklet_t>link, std::set<std::pair<int, int>> id_list);
std::set<std::pair<int, int>> Connect_Linklet_Selection_Re(std::vector<netscan::linklet_t>link, std::set<std::pair<int, int>> id_list);

std::set<std::pair<int, int>> Set_IDlist(std::vector<netscan::linklet_t>link);
double GapMean(std::string file_path, int pl0, int pl1);
std::vector<netscan::base_track_t> rawid_search(std::vector<netscan::base_track_t> base, std::set<std::pair<int, int>> id_list);

int main(int argc, char** argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg Areax-path evaluated-pl output_bvxx_path\n");
		exit(1);
	}
	std::string file_area_path = argv[1];
	int evaluated_pl = std::stoi(argv[2]);
	std::string file_out_bvxx_path = argv[3];

	std::stringstream file_in_link_0, file_in_link_1, file_in_link_2;
	file_in_link_0 << file_area_path << "\\0\\linklet\\l-" << std::setfill('0') << std::setw(3) << evaluated_pl - 2 << "-" << std::setw(3) << evaluated_pl - 1 << ".thick.sel.dump";
	file_in_link_1 << file_area_path << "\\0\\linklet\\l-" << std::setfill('0') << std::setw(3) << evaluated_pl - 1 << "-" << std::setw(3) << evaluated_pl + 1 << ".thick.sel.dump";
	file_in_link_2 << file_area_path << "\\0\\linklet\\l-" << std::setfill('0') << std::setw(3) << evaluated_pl + 1 << "-" << std::setw(3) << evaluated_pl + 2 << ".thick.sel.dump";
	std::cout << file_in_link_0.str() << std::endl;;
	std::cout << file_in_link_1.str() << std::endl;;
	std::cout << file_in_link_2.str() << std::endl;;
	if (!CheckFileExistence(file_in_link_0.str())) {
		fprintf(stderr, "file:%s not found\n", file_in_link_0.str());
		exit(1);
	}
	if (!CheckFileExistence(file_in_link_1.str())) {
		fprintf(stderr, "file:%s not found\n", file_in_link_1.str());
		exit(1);
	}
	if (!CheckFileExistence(file_in_link_2.str())) {
		fprintf(stderr, "file:%s not found\n", file_in_link_2.str());
		exit(1);
	}

	std::vector<netscan::linklet_t> link0, link1, link2;
	netscan::read_linklet_txt(file_in_link_0.str(), link0);
	netscan::read_linklet_txt(file_in_link_1.str(), link1);
	netscan::read_linklet_txt(file_in_link_2.str(), link2);

	std::set<std::pair<int, int>> id_list;
	id_list = Set_IDlist(link0);
	id_list = Connect_Linklet_Selection(link1,id_list);
	id_list = Connect_Linklet_Selection(link2,id_list);

	//外挿元のPL決定
	int ex_pl;
	if (GapMean(file_area_path, evaluated_pl - 1, evaluated_pl) < GapMean(file_area_path, evaluated_pl, evaluated_pl + 1)) {
		ex_pl = evaluated_pl - 1;
	}
	else {
		ex_pl = evaluated_pl + 1;
	}
	printf("ec_pl=%d ex_pl=%d\n",evaluated_pl, ex_pl);
	id_list=Connect_Linklet_Selection_Re(link2, id_list);
	if (id_list.begin()->first != ex_pl) {
		id_list=Connect_Linklet_Selection_Re(link1, id_list);
	}
	printf("id_lisit %d ex_pl =%d\n", id_list.begin()->first, ex_pl);

	std::stringstream file_in_base1, file_in_base2;
	file_in_base1 << file_area_path << "\\PL" << std::setfill('0') << std::setw(3) << ex_pl << "\\b" << std::setw(3) << ex_pl << "_thick.sel.cor.vxx";
	file_in_base2 << file_area_path << "\\PL" << std::setfill('0') << std::setw(3) << ex_pl << "\\b" << std::setw(3) << ex_pl << "_thick.sel.vxx";
	if (!CheckFileExistence(file_in_base1.str())) {
		fprintf(stderr, "file:%s not found\n", file_in_link_2.str());
		exit(1);
	}
	if (!CheckFileExistence(file_in_base2.str())) {
		fprintf(stderr, "file:%s not found\n", file_in_link_2.str());
		exit(1);
	}

	std::vector<netscan::base_track_t> base1,base2;
	netscan::read_basetrack_extension(file_in_base1.str(), base1, ex_pl, 0);
	netscan::read_basetrack_extension(file_in_base2.str(), base2, ex_pl, 0);
	base1 = rawid_search(base1, id_list);
	base2 = rawid_search(base2, id_list);
	std::stringstream file_out_bvxx1, file_out_bvxx2;
	file_out_bvxx1 << file_out_bvxx_path << "\\b" << std::setfill('0') << std::setw(3) << ex_pl << "_thick.sel.cor.vxx";
	file_out_bvxx2 << file_out_bvxx_path << "\\b" << std::setfill('0') << std::setw(3) << ex_pl << "_thick.sel.vxx";

	netscan::write_basetrack_vxx(file_out_bvxx1.str(), base1, ex_pl, 0);
	netscan::write_basetrack_vxx(file_out_bvxx2.str(), base2, ex_pl, 0);

}
bool CheckFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}
std::set<std::pair<int, int>> Set_IDlist(std::vector<netscan::linklet_t>link) {
	std::set<std::pair<int, int>> ret;
	std::set<std::pair<int, int>> id_list;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		id_list.insert(std::make_pair(itr->b[0].pl, itr->b[0].rawid));
	}
	std::multimap<int, netscan::linklet_t> link_map;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		link_map.insert(std::make_pair(itr->b[0].rawid, *itr));
	}

	int count;
	int multi_count = 0;
	for (auto itr = id_list.begin(); itr != id_list.end(); itr++) {
		count = link_map.count(itr->second);
		if (count == 0)continue;
		else if (count == 1) {
			auto res = link_map.find(itr->second);
			ret.insert(std::make_pair(res->second.b[1].pl, res->second.b[1].rawid));
		}
		else {
			multi_count++;
			//最適なlinkletを選択
			auto range = link_map.equal_range(itr->second);
			double Value;
			netscan::linklet_t sel;
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (itr2 == range.first) {
					Value = (itr2->second.dx*itr2->second.b[0].ay - itr2->second.dy* itr2->second.b[0].ax) / sqrt(pow(itr2->second.b[0].ax, 2) + pow(itr2->second.b[0].ay, 2));
					sel = itr2->second;
				}
				if (Value > (itr2->second.dx*itr2->second.b[0].ay - itr2->second.dy* itr2->second.b[0].ax) / sqrt(pow(itr2->second.b[0].ax, 2) + pow(itr2->second.b[0].ay, 2))) {
					Value = (itr2->second.dx*itr2->second.b[0].ay - itr2->second.dy* itr2->second.b[0].ax) / sqrt(pow(itr2->second.b[0].ax, 2) + pow(itr2->second.b[0].ay, 2));
					sel = itr2->second;
				}
			}
			ret.insert(std::make_pair(sel.b[1].pl, sel.b[1].rawid));

		}

	}
	printf("Connect PL%03d-PL%03d :linklet %d --> %d\n", link.begin()->pos[0]/10, link.begin()->pos[1]/10, id_list.size(), ret.size());
	printf("multi linklet %d\n", multi_count);

	return ret;
}
std::set<std::pair<int, int>> Connect_Linklet_Selection(std::vector<netscan::linklet_t>link, std::set<std::pair<int, int>> id_list) {
	std::set<std::pair<int, int>> ret;

	std::multimap<int, netscan::linklet_t> link_map;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		link_map.insert(std::make_pair(itr->b[0].rawid, *itr));
	}
	int count;
	int multi_count = 0;
	for (auto itr = id_list.begin(); itr != id_list.end(); itr++) {
		count = link_map.count(itr->second);
		if (count == 0)continue;
		else if (count == 1) {
			auto res = link_map.find(itr->second);
			ret.insert(std::make_pair(res->second.b[1].pl, res->second.b[1].rawid));
		}
		else {
			multi_count++;
			//最適なlinkletを選択
			auto range = link_map.equal_range(itr->second);
			double Value;
			netscan::linklet_t sel;
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (itr2 == range.first) {
					Value = (itr2->second.dx*itr2->second.b[0].ay - itr2->second.dy* itr2->second.b[0].ax) / sqrt(pow(itr2->second.b[0].ax, 2) + pow(itr2->second.b[0].ay, 2));
					sel = itr2->second;
				}
				if (Value > (itr2->second.dx*itr2->second.b[0].ay - itr2->second.dy* itr2->second.b[0].ax) / sqrt(pow(itr2->second.b[0].ax, 2) + pow(itr2->second.b[0].ay, 2))) {
					Value = (itr2->second.dx*itr2->second.b[0].ay - itr2->second.dy* itr2->second.b[0].ax) / sqrt(pow(itr2->second.b[0].ax, 2) + pow(itr2->second.b[0].ay, 2));
					sel = itr2->second;
				}
			}
			ret.insert(std::make_pair(sel.b[1].pl, sel.b[1].rawid));

		}

	}
	printf("Connect PL%03d-PL%03d :linklet %d --> %d\n", link.begin()->b[0].pl, link.begin()->b[1].pl, id_list.size(), ret.size());
	printf("multi linklet %d\n", multi_count);
	return ret;
}
std::set<std::pair<int, int>> Connect_Linklet_Selection_Re(std::vector<netscan::linklet_t>link, std::set<std::pair<int, int>> id_list) {
	std::set<std::pair<int, int>> ret;

	std::multimap<int, netscan::linklet_t> link_map;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		link_map.insert(std::make_pair(itr->b[1].rawid, *itr));
	}
	int count;
	int multi_count = 0;
	for (auto itr = id_list.begin(); itr != id_list.end(); itr++) {
		count = link_map.count(itr->second);
		if (count == 0)continue;
		else if (count == 1) {
			auto res = link_map.find(itr->second);
			ret.insert(std::make_pair(res->second.b[0].pl, res->second.b[0].rawid));
		}
		else {
			multi_count++;
			//最適なlinkletを選択
			auto range = link_map.equal_range(itr->second);
			double Value;
			netscan::linklet_t sel;
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (itr2 == range.first) {
					Value = (itr2->second.dx*itr2->second.b[0].ay - itr2->second.dy* itr2->second.b[0].ax) / sqrt(pow(itr2->second.b[0].ax, 2) + pow(itr2->second.b[0].ay, 2));
					sel = itr2->second;
				}
				if (Value > (itr2->second.dx*itr2->second.b[0].ay - itr2->second.dy* itr2->second.b[0].ax) / sqrt(pow(itr2->second.b[0].ax, 2) + pow(itr2->second.b[0].ay, 2))) {
					Value = (itr2->second.dx*itr2->second.b[0].ay - itr2->second.dy* itr2->second.b[0].ax) / sqrt(pow(itr2->second.b[0].ax, 2) + pow(itr2->second.b[0].ay, 2));
					sel = itr2->second;
				}
			}
			ret.insert(std::make_pair(sel.b[0].pl, sel.b[0].rawid));

		}

	}
	printf("Connect PL%03d-PL%03d :linklet %d --> %d\n", link.begin()->b[1].pl, link.begin()->b[0].pl, id_list.size(), ret.size());
	printf("multi linklet %d\n", multi_count);
	return ret;
}

double GapMean(std::string file_path, int pl0, int pl1) {
	std::stringstream file_in_link;
	file_in_link << file_path << "\\0\\linklet\\l-" << std::setfill('0') << std::setw(3) << pl0 << "-" << std::setw(3) << pl1 << ".thick.sel.dump";
	if (!CheckFileExistence(file_in_link.str())) {
		fprintf(stderr, "file:%s not found\n", file_in_link.str());
		exit(1);
	}

	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_txt(file_in_link.str(), link, 0);

	double gap = 0;
	int count = 0;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		gap += fabs(itr->b[0].z - itr->b[1].z);
		count++;
	}
	return gap / count;
}

std::vector<netscan::base_track_t> rawid_search(std::vector<netscan::base_track_t> base, std::set<std::pair<int, int>> id_list) {
	std::vector<netscan::base_track_t> ret;
	std::map<int, netscan::base_track_t> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_map.insert(std::make_pair(itr->rawid, *itr));
	}
	for (auto itr = id_list.begin(); itr != id_list.end(); itr++) {
		auto res = base_map.find(itr->second);
		if (res == base_map.end()) {
			fprintf(stderr, "basetrack rawid=%d not found\n", itr->second);
			exit(1);
		}
		ret.push_back(res->second);
	}
	return ret;
}