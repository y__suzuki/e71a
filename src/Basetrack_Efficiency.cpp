#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int main(int argc, char** argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg prediction-vxx input-vxx PL\n");
		exit(1);
	}
	std::string in_file_bvxx1 = argv[1];
	std::string in_file_bvxx2 = argv[2];
	int pl = std::stoi(argv[3]);

	std::vector<netscan::base_track_t> base;
	std::vector<netscan::base_track_t> pred;

	netscan::read_basetrack_extension(in_file_bvxx1, base, pl, 0);
	netscan::read_basetrack_extension(in_file_bvxx2, pred, pl, 0);






}
