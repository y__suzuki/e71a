#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <chrono>

#include <FILE_structure.hpp>
bool checkFileExistence(const std::string& str);
void input_output_header(FILE* fp_in, FILE* fp_out, int64_t link_num);
int64_t file_size(std::string filename);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "prg input-linklet(bin) output-header(txt)\n");
		exit(1);
	}
	std::string file_in_linklet = argv[1];
	std::string file_out_txt = argv[2];
	if (checkFileExistence(file_in_linklet) == false) {
		fprintf(stderr, "file [%s] not exist\n", file_in_linklet.c_str());
		return 0;
	}
	
	int64_t link_num = file_size(file_in_linklet) / sizeof(netscan::linklet_t);
	FILE*fp_in, *fp_out;
	if ((fp_out = fopen(file_out_txt.c_str(), "a")) == NULL) {
		printf("file open error!!\n");
		exit(EXIT_FAILURE);	/* (3)エラーの場合は通常、異常終了する */
	}
	if ((fp_in = fopen(file_in_linklet.c_str(), "rb")) == NULL) {
		printf("%s file not open!\n", file_in_linklet.c_str());
		exit(EXIT_FAILURE);
	}
	auto start = std::chrono::system_clock::now(); // 計測開始時間
	input_output_header(fp_in, fp_out, link_num);
	auto end = std::chrono::system_clock::now();  // 計測終了時間
	double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count(); //処理に要した時間をミリ秒に変換
	printf("elapsed %.1lf [s]\n",  elapsed / 1000);
	fclose(fp_in);
	fclose(fp_out);

	return 0;

}
bool checkFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}

int64_t file_size(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2;
}

void input_output_header(FILE* fp_in, FILE* fp_out,int64_t link_num) {
	netscan::linklet_t link[100];
	int64_t  now = 0;
	int read_num;
	bool flg = true;
	while (flg) {
		if (link_num - now == 100) {
			read_num = link_num - now;
			flg = false;
		}
		else if (link_num - now < 100) {
			read_num = link_num - now;
			flg = false;
		}
		else {
			read_num = 100;
		}
		fread(&link, sizeof(netscan::linklet_t), read_num, fp_in);
		for (int i = 0; i < read_num; i++) {
			fprintf(fp_out, "%4d %10d %4d %10d\n", link[i].pos[0], link[i].b[0].rawid, link[i].pos[1], link[i].b[1].rawid);
		}
		now += read_num;
	}
	printf("write fin %lld\n", now);
}
