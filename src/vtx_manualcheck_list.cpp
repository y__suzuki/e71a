#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
# include <iostream>
# include <random>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <list>
class stop_track {
public:
	int chainid, nseg, npl, pl0, pl1, ph, groupid, rawid;
	double ax, ay, x, y, z;
};
class track_pair {
public:
	int pl0, pl1, raw0, raw1;
	double x, y, z, md;
};
class track_multi {
public:
	int eventid;
	double x, y, z;
	std::vector< std::pair<double, stop_track>> trk;
	std::vector<track_pair>pair;
};

void read_vtx_file(std::string filename, std::vector<track_multi>&multi);
std::vector<track_multi> vtx_selection(std::vector<track_multi>multi);
void set_z(std::vector<mfile0::M_Chain> &c, std::map <int, double> &z);
void base_inverted_trance(vxx::base_track_t &base, corrmap0::Corrmap param);
void out_manualcheck_list(std::vector<track_multi>multi, std::map <int, double>z, std::string file_out_bvxx_path, std::vector<corrmap0::Corrmap> corr);
corrmap0::Corrmap search_corrmap(int pl, std::vector<corrmap0::Corrmap> corr);


int main(int argc,char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-vtx in-mfile in-corrmap-abs out-bvxx-path\n");
		exit(1);
	}
	std::string file_in_vtx = argv[1];
	std::string file_in_mfile = argv[2];
	std::string file_in_corrmap_abs = argv[3];
	std::string file_out_bvxx_path = argv[4];
	std::vector<track_multi> multi;
	read_vtx_file(file_in_vtx, multi);
	multi=vtx_selection(multi);

	printf("size = %d\n", multi.size());
	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::map <int, double> z;
	set_z(m.chains, z);

	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corrmap_abs, corr);




	out_manualcheck_list(multi, z, file_out_bvxx_path, corr);
}
void read_vtx_file(std::string filename, std::vector<track_multi>&multi) {
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0,trk_num;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);

		track_multi m;
		m.eventid = std::stoi(str_v[0]);
		m.x = std::stod(str_v[2]);
		m.y = std::stod(str_v[3]);
		m.z = std::stod(str_v[4]);
		trk_num = std::stoi(str_v[1]);

		for (int i = 0; i < trk_num*(trk_num - 1) / 2; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			track_pair p;
			p.pl0 = std::stoi(str_v[0]);
			p.raw0 = std::stoi(str_v[1]);
			p.pl1 = std::stoi(str_v[2]);
			p.raw0 = std::stoi(str_v[3]);
			p.x = std::stod(str_v[4]);
			p.y = std::stod(str_v[5]);
			p.z = std::stod(str_v[6]);
			p.md = std::stod(str_v[7]);
			m.pair.push_back(p);
		}
		for (int i = 0; i < trk_num; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			stop_track s;
			s.pl0 = std::stoi(str_v[0]);
			s.pl1 = std::stoi(str_v[1]);
			s.rawid = std::stoi(str_v[2]);
			s.chainid = std::stoi(str_v[3]);
			s.groupid = std::stoi(str_v[4]);
			s.nseg = std::stoi(str_v[5]);
			s.npl = std::stoi(str_v[6]);
			s.ph = std::stoi(str_v[7]);
			s.ax = std::stod(str_v[8]);
			s.ay = std::stod(str_v[9]);
			s.x = std::stod(str_v[10]);
			s.y = std::stod(str_v[11]);
			s.z = 0;
			double ip = std::stod(str_v[12]);
			m.trk.push_back(std::make_pair(ip, s));
		}
		multi.push_back(m);
	}

}
std::vector<track_multi> vtx_selection(std::vector<track_multi>multi) {
	std::vector<track_multi> ret;
	for (auto itr = multi.begin(); itr != multi.end(); itr++) {
		if (itr->trk.size() < 3)continue;
		printf("%d %d %.1lf\n", itr->trk.begin()->second.pl1,itr->eventid, itr->z);
		if (itr->eventid != 397)continue;
		ret.push_back(*itr);
	}
	return ret;
}
void set_z(std::vector<mfile0::M_Chain> &c, std::map <int, double> &z) {
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			z.insert(std::make_pair(itr->pos / 10, itr->z));
		}
	}

}
void out_manualcheck_list(std::vector<track_multi>multi, std::map <int, double>z, std::string file_out_bvxx_path, std::vector<corrmap0::Corrmap> corr) {
	for (auto eve = multi.begin(); eve != multi.end(); eve++) {
		std::vector<vxx::base_track_t> base_up;
		for (auto itr = eve->trk.begin(); itr != eve->trk.end(); itr++) {
			vxx::base_track_t b_tmp;
			b_tmp.ax = itr->second.ax;
			b_tmp.ay = itr->second.ay;
			b_tmp.x = itr->second.x;
			b_tmp.y = itr->second.y;
			b_tmp.z = 0;
			b_tmp.rawid = itr->second.rawid;
			b_tmp.isg = itr->second.rawid;
			b_tmp.pl = itr->second.pl1;
			b_tmp.zone = 0;
			
			b_tmp.m[0].ax = b_tmp.ax;
			b_tmp.m[0].ay = b_tmp.ay;
			b_tmp.m[0].ph = (itr->second.ph / 10000) / 2 * 10000 + (itr->second.ph % 10000) / 2;
			b_tmp.m[0].pos = b_tmp.pl * 10 + 1;
			b_tmp.m[0].col = 0;
			b_tmp.m[0].row = 0;
			b_tmp.m[0].zone = 0;
			b_tmp.m[0].isg = b_tmp.rawid;
			b_tmp.m[0].z = 0;
			b_tmp.m[1].ax = b_tmp.ax;
			b_tmp.m[1].ay = b_tmp.ay;
			b_tmp.m[1].ph = (itr->second.ph / 10000) / 2 * 10000 + (itr->second.ph % 10000) / 2;
			b_tmp.m[1].pos = b_tmp.pl * 10 + 2;
			b_tmp.m[1].col = 0;
			b_tmp.m[1].row = 0;
			b_tmp.m[1].zone = 0;
			b_tmp.m[1].isg = b_tmp.rawid;
			b_tmp.m[1].z = 210;
			base_up.push_back(b_tmp);
		}
		std::vector<vxx::base_track_t> base_extra;
		for (auto itr = base_up.begin(); itr != base_up.end(); itr++) {
			vxx::base_track_t b_tmp;
			b_tmp.ax = itr->ax;
			b_tmp.ay = itr->ay;
			if (z.count(itr->pl) + z.count(itr->pl + 1) != 2) {
				fprintf(stderr, "z range exception\n");
				exit(1);
			}
			b_tmp.x = itr->x + itr->ax*(z[itr->pl + 1] - z[itr->pl]);
			b_tmp.y = itr->x + itr->ax*(z[itr->pl + 1] - z[itr->pl]);
			b_tmp.z = 0;
			b_tmp.rawid = itr->rawid;
			b_tmp.isg = itr->rawid;
			b_tmp.pl = itr->pl + 1;

			b_tmp.m[0].ax = b_tmp.ax;
			b_tmp.m[0].ay = b_tmp.ay;
			b_tmp.m[0].ph = itr->m[0].ph;
			b_tmp.m[0].pos = b_tmp.pl * 10 + 1;
			b_tmp.m[0].col = 0;
			b_tmp.m[0].row = 0;
			b_tmp.m[0].zone = 0;
			b_tmp.m[0].isg = b_tmp.rawid;
			b_tmp.m[0].z = 0;
			b_tmp.m[1].ax = b_tmp.ax;
			b_tmp.m[1].ay = b_tmp.ay;
			b_tmp.m[1].ph = itr->m[1].ph;
			b_tmp.m[1].pos = b_tmp.pl * 10 + 2;
			b_tmp.m[1].col = 0;
			b_tmp.m[1].row = 0;
			b_tmp.m[1].zone = 0;
			b_tmp.m[1].isg = b_tmp.rawid;
			b_tmp.m[1].z = 210;

			base_extra.push_back(b_tmp);
		}

		for (auto itr = base_up.begin(); itr != base_up.end(); itr++) {
			int pl = itr->pl;
			corrmap0::Corrmap param = search_corrmap(pl, corr);
			base_inverted_trance(*itr, param);
		}
		for (auto itr = base_extra.begin(); itr != base_extra.end(); itr++) {
			int pl = itr->pl;
			corrmap0::Corrmap param = search_corrmap(pl, corr);
			base_inverted_trance(*itr, param);
		}

		std::stringstream file_out_up;
		std::stringstream file_out_extra;
		vxx::BvxxWriter bw;

		int out_pl = base_up.begin()->pl;
		file_out_up << file_out_bvxx_path << "\\b" << std::setw(3) << std::setfill('0') << out_pl << "_" << eve->eventid << "_up.vxx";
		bw.Write(file_out_up.str(), out_pl, 0, base_up);

		out_pl = base_extra.begin()->pl;
		file_out_extra << file_out_bvxx_path << "\\b" << std::setw(3) << std::setfill('0') << out_pl << "_" << eve->eventid << "_extra.vxx";
		bw.Write(file_out_extra.str(), out_pl, 0, base_extra);
	}
}
corrmap0::Corrmap search_corrmap(int pl, std::vector<corrmap0::Corrmap> corr) {
	int val = -1;
	for (int i = 0; i < corr.size(); i++) {
		if (corr[i].pos[0] / 10 == pl) {
			val = i;
			break;
		}
	}
	if (val < 0) {
		fprintf(stderr, "corrmap pl%d not found\n", pl);
		exit(1);
	}
	return corr[val];
}
void base_inverted_trance(vxx::base_track_t &base,corrmap0::Corrmap param) {
	double tmpx = base.x - param.position[4];
	double tmpy = base.y - param.position[5];
	double factor = 1 / (param.position[0] * param.position[3] - param.position[1] * param.position[2]);
	base.x = factor * (tmpx*param.position[3] - tmpy * param.position[1]);
	base.y = factor * (tmpy*param.position[0] - tmpx * param.position[2]);

	tmpx = base.ax - param.angle[4];
	tmpy = base.ay - param.angle[5];
	factor = 1 / (param.angle[0] * param.angle[3] - param.angle[1] * param.angle[2]);
	base.ax = factor * (tmpx*param.angle[3] - tmpy * param.angle[1]);
	base.ay = factor * (tmpy*param.angle[0] - tmpx * param.angle[2]);
	return;
}