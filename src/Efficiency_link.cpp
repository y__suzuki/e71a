#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <unordered_set>
std::vector<netscan::linklet_t> areacut_link(double area[4], std::vector<netscan::linklet_t>link);
std::vector<netscan::base_track_t> areacut_base(double area[4], std::vector<netscan::base_track_t>base);
double calc_effiency(std::vector<netscan::base_track_t> base, std::vector<netscan::linklet_t> link, double angle_min, double angle_max);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:in-pred in-link pl out\n");
		exit(1);
	}
	std::string in_file_bvxx = argv[1];
	std::string in_file_link = argv[2];
	int pl = std::stoi(argv[3]);
	std::string out_file = argv[4];

	std::vector<netscan::linklet_t> link;
	std::vector<netscan::base_track_t> base;

	netscan::read_basetrack_extension(in_file_bvxx, base, pl, 0);
	netscan::read_linklet_txt(in_file_link, link);

	double area[4] = { 40000,95000,5000,90000 };
	//double area[4] = { -20000,10000,5000,90000 };

	//std::vector<netscan::linklet_t> link_sel=areacut_link(area,link);
	std::vector<netscan::base_track_t> base_sel = areacut_base(area, base);
	double angle[2];
	for (int i = 0; i < 15; i++) {
		angle[0] = i * 0.1;
		angle[1] = (i + 1) * 0.1;
		calc_effiency(base_sel, link, angle[0], angle[1]);
	}
	for (int i = 3; i < 12; i++) {
		angle[0] = i * 0.5;
		angle[1] = (i + 1) * 0.5;
		calc_effiency(base_sel, link, angle[0], angle[1]);
	}

}
std::vector<netscan::linklet_t> areacut_link(double area[4], std::vector<netscan::linklet_t>link) {
	std::vector<netscan::linklet_t> ret;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (itr->b[0].x < area[0])continue;
		if (itr->b[0].x >= area[1])continue;
		if (itr->b[0].y < area[2])continue;
		if (itr->b[0].y >= area[3])continue;
		ret.push_back(*itr);
	}
	return ret;
}
std::vector<netscan::base_track_t> areacut_base(double area[4], std::vector<netscan::base_track_t>base) {
	std::vector<netscan::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->x < area[0])continue;
		if (itr->x >= area[1])continue;
		if (itr->y < area[2])continue;
		if (itr->y >= area[3])continue;
		ret.push_back(*itr);
	}
	return ret;
}
double calc_effiency(std::vector<netscan::base_track_t> base, std::vector<netscan::linklet_t> link, double angle_min,double angle_max) {
	std::unordered_set<int> set_id;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		set_id.insert(itr->b[0].rawid);
	}

	int all = 0, sel = 0;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		angle = sqrt(itr->ax * itr->ax + itr->ay*itr->ay);
		if (angle < angle_min)continue;
		if (angle >= angle_max)continue;
		all++;
		if (set_id.count(itr->rawid)) {
			sel++;
		}
	}
	printf("%4.1lf - %4.1lf %5.2lf%% %d %d\n", angle_min, angle_max, sel*100. / all, sel, all);

	return sel * 1.0 / all;
}