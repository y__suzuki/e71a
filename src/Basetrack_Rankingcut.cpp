#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#include <fstream>
#include <list>
struct CutParam {
	double angle[2];
	double intercept, slope, ymax;
};
struct BaseInformation {
	double angle,x,y;
	vxx::base_track_t *ptr;
};

std::vector<CutParam>  read_cut_param(std::string filename);
void RankingCut(std::list<vxx::base_track_t> &base, CutParam param);
void RankingCut_lateral(std::list<vxx::base_track_t> &base, CutParam param);
std::list<vxx::base_track_t> vector_list_convert(std::vector<vxx::base_track_t> &base);
std::vector<vxx::base_track_t> list_vector_convert(std::list<vxx::base_track_t> &base);

int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:prg in-bvxx pl zone out-bvxx cut-param-file cut-param-file-lat\n");
		fprintf(stderr, "cut-param-file:ang_min ang_max x1 y1 x2 y2\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_base = argv[4];
	std::string file_in_cut_normal = argv[5];
	std::string file_in_cut_lat = argv[6];

	std::vector<CutParam> cutparam_norm = read_cut_param(file_in_cut_normal);
	std::vector<CutParam> cutparam_lat = read_cut_param(file_in_cut_lat);
	std::list<vxx::base_track_t> base_list;

	int64_t count = 0;
	vxx::BvxxReader br;
	if (br.Begin(file_in_base, pl,zone))
	{
		vxx::HashEntry h;
		vxx::base_track_t b;
		while (br.NextHashEntry(h))
		{
			while (br.NextBaseTrack(b))
			{
				if (count % 100000 == 0) {
					fprintf(stderr, "\r read bvxx ... %d", count);
				}
				count++;
				base_list.push_back(b);
			}
		}
		br.End();
		fprintf(stderr, "\r read bvxx ... %d fin\n", count);
	}

	for (auto itr = cutparam_lat.begin(); itr != cutparam_lat.end(); itr++) {
		 RankingCut_lateral (base_list, *itr);
	}
	for (auto itr = cutparam_norm.begin(); itr != cutparam_norm.end(); itr++) {
		 RankingCut(base_list, *itr);
	}
	std::vector<vxx::base_track_t> base = list_vector_convert(base_list);
	vxx::BvxxWriter bw;
	bw.Write(file_out_base, pl, zone, base);
}
std::vector<CutParam>  read_cut_param(std::string filename) {
	std::vector<CutParam> ret;
	std::ifstream ifs(filename);
	double angle[2], x[2], y[2];
	while (ifs >> angle[0] >> angle[1] >> x[0] >> y[0] >> x[1] >> y[1]) {
		CutParam par;
		par.angle[0] = angle[0];
		par.angle[1] = angle[1];
		par.slope = (y[1] - y[0] )/ (x[1] - x[0]);
		par.intercept = y[0] - par.slope * x[0];
		par.ymax = y[1];
		ret.push_back(par);
		//printf("y=%lf x+%lf\n", par.slope, par.intercept);
	}
	return ret;
}
std::list<vxx::base_track_t> vector_list_convert(std::vector<vxx::base_track_t> &base) {
	std::list<vxx::base_track_t> ret;

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ret.push_back(*itr);
	}
	base.clear();
	base.shrink_to_fit();
	return ret;
}
std::vector<vxx::base_track_t> list_vector_convert(std::list<vxx::base_track_t> &base) {
	std::vector<vxx::base_track_t> ret;

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ret.push_back(*itr);
	}
	base.clear();
	return ret;
}

void RankingCut(std::list<vxx::base_track_t> &base, CutParam param) {
	int all_base = base.size();
	int count = 0;
	int all = 0, lost = 0;
	double angle;
	double x, y;
	for (auto itr = base.begin(); itr != base.end(); ) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r angle %3.1lf - %3.1lf %12d/%12d(%4.1lf%%) (x-y)", param.angle[0], param.angle[1], count, all_base, count*100. / all_base);
		}
		count++;
		angle= sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (param.angle[0] <= angle&&angle<param.angle[1]) {
			all++;
			x= sqrt(pow(itr->m[0].ax - itr->ax, 2) + pow(itr->m[1].ax - itr->ax, 2) + pow(itr->m[0].ay - itr->ay, 2) + pow(itr->m[1].ay - itr->ay, 2));
			y = (itr->m[0].ph + itr->m[1].ph) % 10000;
			if (y >= x*param.slope + param.intercept || param.ymax <= y) {
				itr++;
			}
			else {
				itr = base.erase(itr);
				lost++;
			}
		}
		else{
			itr++;
		}
	}

	fprintf(stderr, "\r angle %3.1lf - %3.1lf %12d/%12d(%4.1lf%%) (x-y)\n", param.angle[0], param.angle[1], count, all_base, count*100. / all_base);
	fprintf(stderr, "%d --> %d(%4.1lf%%)\n", all_base,base.size(), base.size()*100. / all_base);
	fprintf(stderr, "all %d ,lost %d\n",all,lost);
}
void RankingCut_lateral(std::list<vxx::base_track_t> &base, CutParam param) {

	int all_base = base.size();
	int count = 0;
	int all = 0, lost = 0;
	double angle;
	double x, y;
	for (auto itr = base.begin(); itr != base.end(); ) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r angle %3.1lf - %3.1lf %12d/%12d(%4.1lf%%) (lateral)", param.angle[0], param.angle[1], count, all_base, count*100. / all_base);
		}
		count++;
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (param.angle[0] <= angle && angle < param.angle[1]) {
			all++;
			x = sqrt(pow((itr->ax*itr->m[0].ay - itr->ay*itr->m[0].ax) / angle, 2) + pow((itr->ax*itr->m[1].ay - itr->ay*itr->m[1].ax) / angle, 2));
			y = (itr->m[0].ph + itr->m[1].ph) % 10000;
			if (y >= x*param.slope + param.intercept || param.ymax <= y) {
				itr++;
			}
			else {
				itr = base.erase(itr);
				lost++;
			}
		}
		else {
			itr++;
		}
	}

	fprintf(stderr, "\r angle %3.1lf - %3.1lf %12d/%12d(%4.1lf%%) (lateral)\n", param.angle[0], param.angle[1], count, all_base, count*100. / all_base);
	fprintf(stderr, "%d --> %d(%4.1lf%%)\n", all_base, base.size(), base.size()*100. / all_base);
	fprintf(stderr, "all %d ,lost %d\n", all, lost);
}
