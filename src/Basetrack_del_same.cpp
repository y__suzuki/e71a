//同一Area内のtrackを想定
//micro trackはIDで一意に決まることを仮定
//ID+zoneで一意になるように拡張はしていない

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <windows.h>
#include <set>
struct IDpair {
	int id0, id1, gnum;
};
//IDpairのsort
bool sort_pair0(const IDpair&left, const IDpair &right) {
	return left.id0 == right.id0 ? left.id1 < right.id1 : left.id0 < right.id0;
}
bool sort_pair1(const IDpair&left, const IDpair &right) {
	return left.id1 == right.id1 ? left.id0 < right.id0 : left.id1 < right.id1;
}
bool sort_gnum(const IDpair&left, const IDpair &right) {
	return left.gnum == right.gnum ? left.id0 == right.id0 ? left.id1 < right.id1 : left.id0 < right.id0 : left.gnum < right.gnum;
}

bool sort_id1(const netscan::base_track_t &left, const netscan::base_track_t &right) {
	if (left.m[0].rawid < right.m[0].rawid)return true;
	else if (left.m[0].rawid > right.m[0].rawid)return false;
	else {
		if (left.m[1].rawid <= right.m[1].rawid)return true;
		else {
			return false;
		}
	}
}
bool sort_id2(const netscan::base_track_t &left, const netscan::base_track_t &right) {
	if (left.m[1].rawid < right.m[1].rawid)return true;
	else if (left.m[1].rawid > right.m[1].rawid)return false;
	else {
		if (left.m[0].rawid <= right.m[0].rawid)return true;
		else {
			return false;
		}
	}
}

std::vector<IDpair> SlctMultiBase(std::vector<netscan::base_track_t> &base);
std::multimap<int, netscan::base_track_t> basetrack_clastering(std::vector<netscan::base_track_t> base);
int count_key(std::multimap<int, netscan::base_track_t *> map);

int main(int argc, char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-bvxx pl zone out-bvxx\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_base = argv[4];

	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_base, base, pl, zone);


	std::vector<IDpair> pair = SlctMultiBase(base);
	GIDdicide(pair);
	//std::multimap<int,netscan::base_track_t> clustered_base;

	//sort(pair_sel.begin(), pair_sel.end(), sort_gnum);
	//OutPutGID("GID_.txt", pair_sel);

	//pair_selにmulti linkletのid0,id1,GroupIDが入っている。
	//LinkletをmultiのgroupIDごとにvectorにつめる。
	int gnum_count;
	//gnum_count = ReIDOrder(pair_sel);



	netscan::write_basetrack_vxx(file_out_base, base, pl, zone);
}



std::vector<IDpair> SlctMultiBase(std::vector<netscan::base_track_t> &base){
	//microtrackのかぶっているbasetrackのidのみを取り出す。
	std::vector<IDpair> pair_v;
	sort(base.begin(), base.end(), sort_id1);
	for (auto itr = base.begin(); itr != base.end();) {
		//idが次のlinkletと一致するか判定
		if ((itr + 1) != base.end() && itr->m[0].rawid == (itr + 1)->m[0].rawid) {
			//一致したらそのlinkletのIDを記録
			IDpair *tmp_p = new IDpair();
			tmp_p->id0 = itr->m[0].rawid;
			tmp_p->id1 = itr->m[1].rawid;
			tmp_p->gnum = 0;
			pair_v.push_back(*tmp_p);
			//次のつぎのlinkletも一致しているので記録
			do {
				IDpair *tmp_pp = new IDpair();
				tmp_pp->id0 = (itr + 1)->m[0].rawid;
				tmp_pp->id1 = (itr + 1)->m[1].rawid;
				tmp_pp->gnum = 0;
				pair_v.push_back(*tmp_pp);
				//itratorを1つ進める
				itr++;
				//進めた次のlinkletのidの一致判定
			} while ((itr + 1) != base.end() && itr->m[0].rawid == (itr + 1)->m[0].rawid);
		}
		else {
			//一致していなければitrをすすめるだけ。
			itr++;
		}
	}
	sort(base.begin(), base.end(), sort_id1);
	for (auto itr = base.begin(); itr != base.end();) {
		//idが次のlinkletと一致するか判定
		if ((itr + 1) != base.end() && itr->m[1].rawid == (itr + 1)->m[1].rawid) {
			//一致したらそのlinkletのIDを記録
			IDpair *tmp_p = new IDpair();
			tmp_p->id0 = itr->m[0].rawid;
			tmp_p->id1 = itr->m[1].rawid;
			tmp_p->gnum = 0;
			pair_v.push_back(*tmp_p);
			//次のつぎのlinkletも一致しているので記録
			do {
				IDpair *tmp_pp = new IDpair();
				tmp_pp->id0 = (itr + 1)->m[0].rawid;
				tmp_pp->id1 = (itr + 1)->m[1].rawid;
				tmp_pp->gnum = 0;
				pair_v.push_back(*tmp_pp);
				//itratorを1つ進める
				itr++;
				//進めた次のlinkletのidの一致判定
			} while ((itr + 1) != base.end() && itr->m[1].rawid == (itr + 1)->m[1].rawid);
		}
		else {
			//一致していなければitrをすすめるだけ。
			itr++;
		}
	}

	//WriteLinklet("test_out.dat", link);

	std::vector<IDpair> rt_pair;
	//同一pairの削除
	sort(pair_v.begin(), pair_v.end(), sort_pair0);
	for (auto itr = pair_v.begin(); itr != pair_v.end(); itr++) {
		while ((itr + 1) != pair_v.end() && itr->id0 == (itr + 1)->id0&&itr->id1 == (itr + 1)->id1) {
			//次が一致すればpush_backせずにitrを進める
			itr++;
		}
		rt_pair.push_back(*itr);
	}
	return rt_pair;
}

void GIDdicide(std::vector<IDpair> &pair) {
	//最初にgnumをつける
	std::multimap<int, IDpair*> gid_pair;

	sort(pair.begin(), pair.end(), sort_pair0);
	int count = 0;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		count++;
		if ((itr + 1) != pair.end() && itr->id0 == (itr + 1)->id0) {
			itr->gnum = count;
			gid_pair.insert(std::make_pair(itr->gnum, &(*itr)));
			while ((itr + 1) != pair.end() && itr->id0 == (itr + 1)->id0) {
				itr++;
				itr->gnum = count;
				gid_pair.insert(std::make_pair(itr->gnum, &(*itr)));
			}
		}
		else {
			itr->gnum = count;
			gid_pair.insert(std::make_pair(itr->gnum, &(*itr)));
		}
	}

	//sortするとgid_pairのポインタが使えなくなるのでsort用のIDpairを用意
	//pairはsort厳禁
	std::vector<IDpair> p = pair;
	std::map<int, std::vector<int>> id0_gid;
	std::map<int, std::vector<int>> id1_gid;

	{
		//id0基準
		int gnum = -1, start, end;
		p = pair;
		sort(p.begin(), p.end(), sort_pair0);
		std::map<int, int>convert;
		std::map<int, std::map<int, int>* >re_convert;
		for (int i = 0; i < p.size(); i++) {
			if (i + 1 != p.size() && p[i].id0 == p[i + 1].id0) {
				//id0が同じtrack集団を集める
				std::set<int> sameID;
				sameID.insert(p[i].gnum);
				while (i + 1 != pair.size() && pair[i].id0 == pair[i + 1].id0) {
					i++;
					sameID.insert(p[i].gnum);
				}
				//id0が同じtrack集団のgnum
				//IDの変更先
				int to_id = *(sameID.begin());
				for (auto itr = sameID.begin(); itr != sameID.end();) {
					if (to_id == *itr)continue;
					//gidの変換表作成
					auto res = convert.insert(std::make_pair(*itr, to_id));
					if (!res.second) {
						//挿入失敗
						if (res.first->second < *(sameID.begin())) {
							//gidの変換表作成し直し
							auto ptr = re_convert.find(res.first->second);

							to_id = res.first->second;
							itr = sameID.begin();
						}
						else {
							//convert内で行先が	res.first->secondのものをto_id に変更
							auto ptr = re_convert.find(res.first->second);
							for (auto itr2 = ptr->second->begin(); itr2 != ptr->second->end(); itr2++) {
								auto res2 = convert.insert(std::make_pair(itr2->first, to_id));
							}
							re_convert.erase(res.first->second);
						}
					}
					else {
						//作成中
						auto res_r = re_convert.insert(std::make_pair(to_id, &(*res.first)));
					}
				}
			}
		}
	}
}

				
				
				
				int IDGID(std::vector<IDpair> &pair, int mode) {
	int conv_num = 0;
	std::vector<int> gnum_v;
	int gnum = -1, start, end;
	if (mode == 0) {
		sort(pair.begin(), pair.end(), sort_pair0);
		for (int i = 0; i < pair.size(); ) {
			if (i + 1 != pair.size() && pair[i].id0 == pair[i + 1].id0) {
				//id0が同じtrack集団を集める
				start = i;
				gnum_v.clear();
				gnum_v.push_back(pair[i].gnum);
				while (i + 1 != pair.size() && pair[i].id0 == pair[i + 1].id0) {
					i++;
					gnum_v.push_back(pair[i].gnum);
					end = i;
				}
				//gnumの再セット(最小値)
				gnum = *std::min_element(gnum_v.begin(), gnum_v.end());
				for (i = start; i <= end; i++) {
					if (pair[i].gnum != gnum) {
						pair[i].gnum = gnum;
						conv_num++;
					}
				}
			}
			else {
				i++;
			}
		}
	}
	else if (mode == 1) {
		sort(pair.begin(), pair.end(), sort_pair1);
		for (int i = 0; i < pair.size();) {
			if (i + 1 != pair.size() && pair[i].id1 == pair[i + 1].id1) {
				start = i;
				gnum_v.clear();
				gnum_v.push_back(pair[i].gnum);
				while (i + 1 != pair.size() && pair[i].id1 == pair[i + 1].id1) {
					i++;
					gnum_v.push_back(pair[i].gnum);
					end = i;
				}
				//gnumのセット(最小値)
				gnum = *std::min_element(gnum_v.begin(), gnum_v.end());
				for (i = start; i <= end; i++) {
					if (pair[i].gnum != gnum) {
						pair[i].gnum = gnum;
						conv_num++;
					}
				}
			}
			else {
				i++;
			}
		}
	}
	return conv_num;
}
//GIDの付け直し
int ReIDOrder(std::vector<IDpair> &pair) {
	int tmp_GID = -1, count = -1;
	sort(pair.begin(), pair.end(), sort_gnum);
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		if (itr->gnum != tmp_GID) {
			count++;
			tmp_GID = itr->gnum;
			itr->gnum = count;
		}
		else {
			itr->gnum = count;
		}
	}
	//Group数-1 を返す(ID:0,1,2,3,4...なので)
	return count;
}

std::multimap<int, netscan::base_track_t> basetrack_clastering(std::vector<netscan::base_track_t> base) {

	std::multimap<int, netscan::base_track_t *> before;
	std::multimap<int, netscan::base_track_t *> after;
	int count = 0;
	sort(base.begin(), base.end(), sort_id1);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		//face1のmicro trackでclustering
		//if (count < 100) printf("count %d %d %d\n", count, itr->m[0].rawid, itr->m[1].rawid);
			
		after.insert(std::make_pair(count, &(*itr)));
		if (itr + 1 != base.end() && itr->m[0].rawid != (itr + 1)->m[0].rawid) {
			count++;
		}
	}
	int before_num, after_num;
	before_num = base.size();
	after_num = count_key(after);
	printf("%d --> %d\n", before_num, after_num);
	while (before_num != after_num) {
	//group化iteration
	//移動先の決定


	//}
	//count = 0;
	//count++;
	//for (auto itr = face1.begin(); itr != face1.end(); itr++) {
	//	after.insert(std::make_pair(count, *itr));
	//	if(std::next(itr,1)!=face1.end()&&itr->first!=)
	//}

}

int count_key(std::multimap<int, netscan::base_track_t *> map) {

	// QueryPerformanceCounter関数の1秒当たりのカウント数を取得する
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);

	LARGE_INTEGER start, end;

	QueryPerformanceCounter(&start);

	// 何かの処理
	int count = 1;
	for (auto itr = map.begin(); itr != map.end(); itr++) {
		if (std::next(itr, 1) == map.end())continue;
		if (itr->first != std::next(itr, 1)->first) {
			count++;
		}
	}


	QueryPerformanceCounter(&end);

	double time = static_cast<double>(end.QuadPart - start.QuadPart) * 1000.0 / freq.QuadPart;
	printf("time %lf[ms]\n", time);

	return count;
}
