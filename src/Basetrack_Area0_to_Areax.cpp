#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <set>
#include <list>
#include <filesystem>

std::vector<vxx::base_track_t> select_matched_base(std::vector<vxx::base_track_t>&base_all, std::vector<vxx::base_track_t>&base_sel);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg input_bvxx_all pl input_bvxx_path output_bvxx_path\n");
		exit(1);
	}

	std::string file_in_bvxx_all = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_bvxx_path = argv[3];
	std::string file_out_bvxx_path = argv[4];

	std::vector<vxx::base_track_t> base_all, base[6];
	vxx::BvxxReader br;
	vxx::BvxxWriter bw;

	base_all = br.ReadAll(file_in_bvxx_all, pl, 0);
	for (int area = 1; area <= 6; area++) {
		std::stringstream file_in_bvxx;
		file_in_bvxx << file_in_bvxx_path << "\\Area" << std::setw(1) << area << "\\PL" << std::setw(3) << std::setfill('0') << pl
			<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.vxx";
		if (!std::filesystem::exists(file_in_bvxx.str())) {
			printf("file[%s]not exist\n", file_in_bvxx.str().c_str());
			continue;
		}
		base[area - 1] = br.ReadAll(file_in_bvxx.str(), pl, 0);
		base[area-1] = select_matched_base(base_all, base[area-1]);

		std::stringstream file_out_bvxx;
		file_out_bvxx << file_out_bvxx_path << "\\b" << std::setw(3) << std::setfill('0') << pl << "_area" << std::setw(1) << area << ".sel.vxx";
		bw.Write(file_out_bvxx.str(), pl, 0, base[area - 1]);

	}
}
std::vector<vxx::base_track_t> select_matched_base(std::vector<vxx::base_track_t>&base_all, std::vector<vxx::base_track_t>&base_sel) {
	std::vector<vxx::base_track_t> ret;
	std::set<std::tuple<int, int, int>> id;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		id.insert(std::make_tuple(itr->m[0].zone, itr->m[0].rawid, itr->m[1].rawid));
	}
	for (auto itr = base_sel.begin(); itr != base_sel.end(); itr++) {
		if (id.count(std::make_tuple(itr->m[0].zone, itr->m[0].rawid, itr->m[1].rawid)) == 1) {
			ret.push_back(*itr);
		}
	}
	return ret;
}