#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>

void rewrite_vph(std::vector<vxx::base_track_t> &base, std::map<std::tuple<int, int, int>, vxx::micro_track_t *>  &micro_map);

int main(int argc, char **argv) {
	if (argc != 10) {
		fprintf(stderr, "usage:prg in-bvxx pl in-fvxx1 pos zone infvxx2 pos zone out-bvxx\n");
		exit(1);
	}

	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_fvxx1 = argv[3];
	int pos1 = std::stoi(argv[4]);
	int zone1 = std::stoi(argv[5]);
	std::string file_in_fvxx2 = argv[6];
	int pos2 = std::stoi(argv[7]);
	int zone2 = std::stoi(argv[8]);
	std::string file_out_bvxx = argv[9];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);


	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> micro = fr.ReadAll(file_in_fvxx1, pos1, zone1);
	{
		std::map<std::tuple<int,int,int>, vxx::micro_track_t *> micro_map;
		for (auto itr = micro.begin(); itr != micro.end(); itr++) {
			micro_map.insert(std::make_pair(std::make_tuple(itr->col, itr->row, itr->isg), &(*itr)));
		}
		rewrite_vph(base, micro_map);
	}

	micro.clear();
	micro = fr.ReadAll(file_in_fvxx2, pos2, zone2);
	{
		std::map<std::tuple<int, int, int>, vxx::micro_track_t *> micro_map;
		for (auto itr = micro.begin(); itr != micro.end(); itr++) {
			micro_map.insert(std::make_pair(std::make_tuple(itr->col, itr->row, itr->isg), &(*itr)));
		}
		rewrite_vph(base, micro_map);
	}

	vxx::BvxxWriter bw;
	bw.Write(file_out_bvxx, pl, 0, base);
}
void rewrite_vph(std::vector<vxx::base_track_t> &base, std::map<std::tuple<int, int, int>, vxx::micro_track_t *>  &micro_map) {
	int pos_id;
	if (base.begin()->m[0].pos == micro_map.begin()->second->pos) {
		pos_id = 0;
	}
	else if (base.begin()->m[1].pos == micro_map.begin()->second->pos) {
		pos_id = 1;
	}
	else {
		fprintf(stderr, "pos not match\n");
		exit(1);
	}

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		auto res = micro_map.find(std::make_tuple(itr->m[pos_id].col, itr->m[pos_id].row, itr->m[pos_id].isg));
		if (res == micro_map.end()) {
			fprintf(stderr, "id =%d not found\n", itr->rawid);
		}
		else {
			itr->m[pos_id].ph = (itr->m[pos_id].ph / 10000) * 10000 + (int)res->second->px;
		}
	}
}
