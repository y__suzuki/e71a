#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <filesystem>
int base_count(std::vector<vxx::base_track_t>&base, double cut_angle);

int main(int argc, char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-bvxx-folder start-pl end-pl output\n");
		exit(1);
	}

	std::string file_in_bvxx_path = argv[1];
	int pl0 = std::stoi(argv[2]);
	int pl1 = std::stoi(argv[3]);
	std::string file_out_txt = argv[4];

	std::ofstream ofs(file_out_txt);
	for (int pl = pl0; pl <= pl1; pl++) {
		std::vector<vxx::base_track_t> base;
		vxx::BvxxReader br;
		std::stringstream file_in_bvxx;
		file_in_bvxx << file_in_bvxx_path << "\\PL" << std::setw(3) << std::setfill('0') << pl << "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
		//file_in_bvxx << file_in_bvxx_path << "\\PL" << std::setw(3) << std::setfill('0') << pl  << "\\Area0\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
		if (!std::filesystem::exists(file_in_bvxx.str()))continue;
		base = br.ReadAll(file_in_bvxx.str(), pl, 0);
		int count[7], all;
		all = base_count(base, 4);
		count[0] = base_count(base, 0.1);
		count[1] = base_count(base, 0.3);
		count[2] = base_count(base, 0.5);
		count[3] = base_count(base, 0.7);
		count[4] = base_count(base, 1.0);
		count[5] = base_count(base, 2.0);
		count[6] = base_count(base, 3.0);
		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(0) << pl << " "
			<< std::setw(8) << std::setprecision(0) << all << " "
			<< std::setw(8) << std::setprecision(0) << count[0] << " ";

		for (int i = 1; i < 7; i++) {
			ofs << std::right << std::fixed << std::setw(8) << std::setprecision(0) << count[i] - count[i - 1] << " ";
		}
		ofs << std::endl;

	}
}
int base_count(std::vector<vxx::base_track_t>&base,double cut_angle) {
	int count = 0;
	std::vector<vxx::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->ax*itr->ax + itr->ay*itr->ay > cut_angle*cut_angle)continue;
		count++;
	}
	return count;

}