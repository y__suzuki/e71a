#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include<vector>
#include<algorithm>
using namespace std;
struct base_track_t_rank {
	vxx::base_track_t *b;
	int vph;
	double da;
	double da_lateral;
	double ang;
	int flg;
};

struct SelectionPRM {
	double vphth1, vphth2;
};

std::vector< base_track_t_rank> basetrack_rank_format(std::vector<vxx::base_track_t> &base);
std::vector<vxx::base_track_t> basetrack_rank_format(std::vector< base_track_t_rank> &base);
void VPHoffset(std::vector< base_track_t_rank> &base, std::vector< base_track_t_rank> &ret, int offset[7]);
void Hash(vector<base_track_t_rank>b_rank, vector<vector<base_track_t_rank> > &b_rank_hash);
void basetrack_flg_apply(vector<base_track_t_rank>&bt);

std::vector<vxx::base_track_t> TrackRankingSelection(vector<vector<base_track_t_rank> > &base);
void RankingSelection(vector<base_track_t_rank>&bt, double da_mean, double da_rms, double vphth, int k, int vph_min_noise);
void RankingSelection_Lateral(vector<base_track_t_rank>&bt, int vph_min_noise, int vph_max_th);

vector<base_track_t_rank> noise_track(vector<base_track_t_rank>bt, double dath, int &vph_min_noise);
vector<base_track_t_rank> noise_track_lat(vector<base_track_t_rank>bt, double dath, int &vph_min_noise);

void GetRange_vph(vector<base_track_t_rank> bt, int *vph_rth1, int *vph_rth2, int k);
void GetRange_vph_Lateral(vector<base_track_t_rank> bt, int *vph_rth1, int *vph_rth2);
double GetMean_da(vector<base_track_t_rank> bt);
double GetRMS_da(vector<base_track_t_rank> bt, double vph_mean);
double GetMean_vph(vector<base_track_t_rank> bt);
double GetRMS_vph(vector<base_track_t_rank> bt, double vph_mean);
int GetVPHth(vector<base_track_t_rank> b, double vph_mean, double vph_rms, double *vphth);
int GetVPHth(vector<base_track_t_rank> b, double vph_mean, double vph_rms, int vph_min_noise, double *vphth);

int main(int argc, char *argv[])
{
	if (argc<=3||12<=argc ) {
		printf("引数が違います\n");
		printf("prg [in-bvxx] [out-bvxx] [pl] [VPH_offset (0.0-0.1)] [VPH_offset (0.1-0.2)] [VPH_offset (0.2-0.3)] [VPH_offset (0.3-0.4)] [VPH_offset (0.4-0.6)] [VPH_offset (0.6-1.1)] [VPH_offset (1.1-)]\n");
		printf("prg b044.vxx b044.sel.vxx 30 25 20 15 10 10 10\n");
		printf("\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	std::string file_out_bvxx = argv[2];
	int pl = std::stoi(argv[3]);
	int offset[7] = {};
	for (int i = 0; i < 7; i++) {
		if (i + 4 < argc) {
			offset[i] = std::stoi(argv[i + 4]);
		}
		else {
			offset[i] = 0;
		}
	}

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);
	std::vector< base_track_t_rank> b_rank = basetrack_rank_format(base);
	std::vector< base_track_t_rank> b_rank_cut;
	VPHoffset(b_rank, b_rank_cut, offset);

	vector<vector<base_track_t_rank> > b_rank_hash;
	Hash(b_rank_cut, b_rank_hash);

	std::vector<vxx::base_track_t> base_sel = TrackRankingSelection(b_rank_hash);
	printf("basetrack ranking %d --> %d (%4.1lf%%)\n", base.size(), base_sel.size(), base_sel.size()*100. / base.size());
	vxx::BvxxWriter bw;
	bw.Write(file_out_bvxx, pl, 0, base_sel);

	return 0;
}

std::vector< base_track_t_rank> basetrack_rank_format(std::vector<vxx::base_track_t> &base) {
	std::vector< base_track_t_rank> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_track_t_rank b_rank;
		b_rank.b = &(*itr);
		b_rank.vph = itr->m[0].ph % 10000 + itr->m[1].ph % 10000;
		double ang = itr->ax*itr->ax + itr->ay*itr->ay;
		b_rank.ang = sqrt(ang);
		b_rank.da = sqrt(pow(itr->ax - itr->m[0].ax, 2) + pow(itr->ax - itr->m[1].ax, 2) + pow(itr->ay - itr->m[0].ay, 2) + pow(itr->ay - itr->m[1].ay, 2));
		if (ang > 0.0) b_rank.da_lateral = sqrt(pow((itr->ax*itr->m[0].ay - itr->ay*itr->m[0].ax) / sqrt(ang), 2) + pow((itr->ax*itr->m[1].ay - itr->ay*itr->m[1].ax) / sqrt(ang), 2));
		else {
			double da1 = pow(itr->ax - itr->m[0].ax, 2) + pow(itr->ax - itr->m[1].ax, 2);
			double da2 = pow(itr->ay - itr->m[0].ay, 2) + pow(itr->ay - itr->m[1].ay, 2);
			if (da1 >= da2) b_rank.da_lateral = sqrt(da2);
			else {
				b_rank.da_lateral = sqrt(da1);
			}
		}
		b_rank.flg = 1;
		ret.push_back(b_rank);
	}
	return ret;
}
std::vector<vxx::base_track_t> basetrack_rank_format(std::vector< base_track_t_rank> &base) {
	std::vector< vxx::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ret.push_back(*(itr->b));
	}
	return ret;
}
void VPHoffset(std::vector< base_track_t_rank> &base, std::vector< base_track_t_rank> &ret, int offset[7]){

	int angle_id, flg;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		flg = 1;
		angle_id = (int)(itr->ang * 10);
		if (angle_id == 0 && itr->vph < offset[0]) flg = 0;
		else if (angle_id == 1 && itr->vph < offset[1]) flg = 0;
		else if (angle_id == 2 && itr->vph < offset[2]) flg = 0;
		else if (angle_id == 3 && itr->vph < offset[3]) flg = 0;
		else if (angle_id == 4 && itr->vph < offset[4]) flg = 0;
		else if (angle_id == 5 && itr->vph < offset[4]) flg = 0;
		else if (6 <= angle_id && angle_id < 11 && itr->vph < offset[5]) flg = 0;
		else if (11 <= angle_id  && itr->vph < offset[6]) flg = 0;

		if (angle_id == 0 && flg == 1) itr->vph -= offset[0];
		else if (angle_id == 1 && flg == 1) itr->vph -= offset[1];
		else if (angle_id == 2 && flg == 1) itr->vph -= offset[2];
		else if (angle_id == 3 && flg == 1) itr->vph -= offset[3];
		else if (angle_id == 4 && flg == 1) itr->vph -= offset[4];
		else if (angle_id == 5 && flg == 1) itr->vph -= offset[4];
		else if (6 <= angle_id && angle_id < 11 && flg == 1) itr->vph -= offset[5];
		else if (11 <= angle_id && flg == 1) itr->vph -= offset[6];

		if (flg == 1) ret.push_back(*itr);

	}
}
void Hash(vector<base_track_t_rank>b_rank, vector<vector<base_track_t_rank> > &b_rank_hash)
{
	for (int i = 0; i < 22; i++) {
		vector<base_track_t_rank> tmp_vec;
		b_rank_hash.push_back(tmp_vec);
	}

	int angle_id;
	for (auto itr = b_rank.begin(); itr != b_rank.end(); itr++) {
		angle_id = (int)(itr->ang * 10);
		if (angle_id <= 14) {
			b_rank_hash[angle_id].push_back(*itr);
		}
		else {
			angle_id = int((angle_id - 15) / 5) + 15;
			angle_id = std::min(angle_id, 21);
			b_rank_hash[angle_id].push_back(*itr);
		}
	}
}

std::vector<vxx::base_track_t> TrackRankingSelection(vector<vector<base_track_t_rank> > &base)
{
	int i, j, k;
	double da_mean, da_rms;
	double vph_mean, vph_rms;
	double vphth;
	int flg;
	double dath;
	int vph_min_noise;
	std::vector<vxx::base_track_t> ret;

	FILE *fout;
	fout = fopen("TRankSel.log", "w");

	for (k = 0; k < 22; k++) {
		vector<base_track_t_rank> bt = base[k];
		vector<base_track_t_rank> b;

		//vector<BaseTrk*> bt = btk[k];
		//vector<BaseTrk*> b;

		fprintf(stderr, "%.1lf <= sqrt(ax*ax+ay*ay) < %.1lf\n", (double)k / 10, (double)(k + 1) / 10);
		fprintf(fout, "%.1lf <= sqrt(ax*ax+ay*ay) < %.1lf\n", (double)k / 10, (double)(k + 1) / 10);
		dath = 0.02 + 0.04*(double)k / 10;
		/****  Noise da mean, rms ****/
//		for(i=0;i<bt.size();i++){
//			
//			if(bt[i]->da > dath && bt[i]->vph < 7) b.push_back(bt[i]);
//		}
		b = noise_track(bt, dath, vph_min_noise);
		//b = noise_track_lat(bt, 0.015, vph_min_noise);

		int vph_rth1, vph_rth2;
		/****  Lateral selection ****/
		GetRange_vph_Lateral(bt, &vph_rth1, &vph_rth2);
		if (vph_min_noise < 7) vph_min_noise = 7;
		RankingSelection_Lateral(bt, vph_min_noise, vph_rth1);
		fprintf(stderr, " Lateral Process: 1 (da_lateral<=0.05 & vph<=%d, da_lateral<=0.03/%d*vph)\n\n", vph_rth1, vph_min_noise);
		fprintf(fout, " Lateral Process: 1 (da_lateral<=0.05 & vph<=%d, da_lateral<=0.03/%d*vph)\n\n", vph_rth1, vph_min_noise);
		/***********************/

		basetrack_flg_apply(bt);
		b.clear();

		b = noise_track(bt, dath, vph_min_noise);
		//b = noise_track_lat(bt, 0.015, vph_min_noise);

		da_mean = GetMean_da(b);
		da_rms = GetRMS_da(b, da_mean);
		b.clear();
		fprintf(stderr, " Noise da = %.4lf+-%.4lf (vph<=%d, da>=%7.4lf)\n", da_mean, da_rms, vph_min_noise, dath);
		fprintf(fout, " Noise da = %.4lf+-%.4lf (vph<=%d, da>=%7.4lf)\n", da_mean, da_rms, vph_min_noise, dath);
		/*****************************/

		/****  Signal vph mean, rms ****/
		GetRange_vph(bt, &vph_rth1, &vph_rth2, k);
		//		fprintf(stderr," %d %d\n",vph_rth1,vph_rth2);
		dath = 0.01 + 0.003*(double)k;

		for (i = 0; i < bt.size(); i++) {

			if (bt[i].da < dath && bt[i].vph < vph_rth1 && bt[i].vph >= vph_rth2) b.push_back(bt[i]);
			//			if(k<=1         && bt[i]->da < 0.02 && bt[i]->vph < 120 && bt[i]->vph >= 40) b.push_back(bt[i]);
			//			if(k>=2 && k<=3 && bt[i]->da < 0.02 && bt[i]->vph < 100 && bt[i]->vph >= 20) b.push_back(bt[i]);
			//			if(k>=4         && bt[i]->da < 0.02 && bt[i]->vph <  80 && bt[i]->vph >= 10) b.push_back(bt[i]);
		}

		vph_mean = GetMean_vph(b);
		vph_rms = GetRMS_vph(b, vph_mean);
		b.clear();
		fprintf(stderr, " Signal vph = %.1lf+-%.1lf (vph: %d - %d, da<=%7.4lf)\n", vph_mean, vph_rms, vph_rth2, vph_rth1, dath);
		fprintf(fout, " Signal vph = %.1lf+-%.1lf (vph: %d - %d, da<=%7.4lf)\n", vph_mean, vph_rms, vph_rth2, vph_rth1, dath);
		/******************************/

		/****  Noise vph max ****/
		for (i = 0; i < bt.size(); i++) {

			if (bt[i].da > da_mean - da_rms && bt[i].da < da_mean + da_rms) b.push_back(bt[i]);
		}

		//		flg = GetVPHth(b,vph_mean, vph_rms, &vphth);
		flg = GetVPHth(b, vph_mean, vph_rms, vph_min_noise, &vphth);
		b.clear();
		fprintf(stderr, " Noise vph_max = %.1lf\n", vphth);
		fprintf(stderr, " Nomal Process:   %d\n", flg);
		fprintf(fout, " Noise vph_max = %.1lf\n", vphth);
		fprintf(fout, " Nomal Process:   %d\n", flg);
		/***********************/

//		if(flg==1) RankingSelection(bt,da_mean,da_rms,vphth);
		if (flg == 1) RankingSelection(bt, da_mean, da_rms, vphth, k, vph_min_noise);

		basetrack_flg_apply(bt);
		for (auto itr = bt.begin(); itr != bt.end(); itr++) {
			ret.push_back(*(itr->b));
		}
		bt.clear();
	}

	fclose(fout);
	return ret;
}
void RankingSelection(vector<base_track_t_rank>&bt, double da_mean, double da_rms, double vphth, int k, int vph_min_noise)
{
	double w;
	if (k == 0 || k == 3) w = 1.1;
	if (k == 1 || k == 2) w = 1.3;
	if (k >= 4) w = 1.0;

	double a = (vphth - (double)vph_min_noise) / (da_mean - da_rms) * w;
	double vph_cut;
	double vph_cut_max = da_mean * a + (double)vph_min_noise;

	for (int i = 0; i < bt.size(); i++) {

		vph_cut = bt[i].da*a + (double)vph_min_noise;
		if (bt[i].da > da_mean - da_rms * 4 && bt[i].da <= da_mean && bt[i].vph < vph_cut) bt[i].flg = 0;
		if (bt[i].da > da_mean && bt[i].vph < vph_cut_max) bt[i].flg = 0;
	}
}
void RankingSelection_Lateral(vector<base_track_t_rank>&bt, int vph_min_noise, int vph_max_th)
{
	double da_cut;
	double a = 0.030 / (double)vph_min_noise;

	for (int i = 0; i < bt.size(); i++) {

		da_cut = bt[i].vph*a;
		if (bt[i].da_lateral > da_cut) bt[i].flg = 0;
		if (bt[i].da_lateral > 0.050 && bt[i].vph <= vph_max_th) bt[i].flg = 0;
	}
}

vector<base_track_t_rank> noise_track(vector<base_track_t_rank>bt, double dath, int &vph_min_noise) {
	vector<base_track_t_rank> b;
	int i;
	vph_min_noise = 0;
	for (i = 0; i < bt.size(); i++) if (bt[i].da > dath && bt[i].vph < 7) b.push_back(bt[i]);
	if (b.size() < 100) {
		for (i = 0; i < bt.size(); i++) if (bt[i].da > dath && bt[i].vph >= 7 && bt[i].vph < 14) b.push_back(bt[i]);
		vph_min_noise = 14;
	}
	if (b.size() < 100) {
		for (i = 0; i < bt.size(); i++) if (bt[i].da > dath && bt[i].vph >= 14 && bt[i].vph < 21) b.push_back(bt[i]);
		vph_min_noise = 21;
	}
	if (b.size() < 100) {
		for (i = 0; i < bt.size(); i++) if (bt[i].da > dath && bt[i].vph >= 21 && bt[i].vph < 28) b.push_back(bt[i]);
		vph_min_noise = 28;
	}
	return b;

}
vector<base_track_t_rank> noise_track_lat(vector<base_track_t_rank>bt, double dath, int &vph_min_noise) {
	vector<base_track_t_rank> b;
	int i;
	vph_min_noise = 0;
	for (i = 0; i < bt.size(); i++) if (bt[i].da_lateral > dath && bt[i].vph < 7) b.push_back(bt[i]);
	if (b.size() < 100) {
		for (i = 0; i < bt.size(); i++) if (bt[i].da_lateral > dath && bt[i].vph >= 7 && bt[i].vph < 14) b.push_back(bt[i]);
		vph_min_noise = 14;
	}
	if (b.size() < 100) {
		for (i = 0; i < bt.size(); i++) if (bt[i].da_lateral > dath && bt[i].vph >= 14 && bt[i].vph < 21) b.push_back(bt[i]);
		vph_min_noise = 21;
	}
	if (b.size() < 100) {
		for (i = 0; i < bt.size(); i++) if (bt[i].da_lateral > dath && bt[i].vph >= 21 && bt[i].vph < 28) b.push_back(bt[i]);
		vph_min_noise = 28;
	}
	return b;
}

int GetVPHth(vector<base_track_t_rank> bt, double vph_mean, double vph_rms, int vph_min_noise, double *vphth)
{
	int flg = 0;
	int i, j;
	int k = 0;
	for (i = 0; i < bt.size(); i++) {

		if (bt[i].vph > vph_mean - vph_rms && bt[i].vph <= vph_mean) k++;
	}
	double vph_signal = (double)k / vph_rms;

	k = 0;
	int vph_min = 100000;
	if (vph_min_noise == 0) {
		for (i = 0; i < bt.size(); i++) {
			if (vph_min > bt[i].vph) vph_min = bt[i].vph;
		}
		for (i = 0; i < bt.size(); i++) {
			if (bt[i].vph == vph_min || bt[i].vph == vph_min + 1) k++;
		}
	}
	if (vph_min_noise != 0) {
		for (i = 0; i < bt.size(); i++) {
			if (bt[i].vph <= vph_min_noise) k++;
		}
		vph_min = vph_min_noise;
	}
	double vph_noise = (double)k / 2;

	if (vph_noise >= vph_signal * 2.5) flg = 1;

	int vph_fit = (int)(vph_mean - vph_rms * 2);
	int n = vph_fit - vph_min + 1;
	if (n <= 1) flg = 0;

	if (flg == 1) {

		vector<int> num;
		vector<int> vph;

		for (j = 0; j < n; j++) num.push_back(0);
		for (j = 0; j < n; j++) vph.push_back(vph_min + j);

		for (i = 0; i < bt.size(); i++) {
			for (j = 0; j < n; j++) {
				if (bt[i].vph < vph_min) num[0] += 1;
				if (bt[i].vph == vph[j]) num[j] += 1;
			}
		}

		//		for(j=0;j<n;j++) printf("%d %d\n",vph[j],num[j]);

		double a, b;
		int xx, xy, yy, x, y;
		xx = 0; xy = 0; yy = 0; x = 0; y = 0;
		for (j = 0; j < n; j++) xx += vph[j] * vph[j];
		for (j = 0; j < n; j++) xy += vph[j] * num[j];
		for (j = 0; j < n; j++) yy += num[j] * num[j];
		for (j = 0; j < n; j++) x += vph[j];
		for (j = 0; j < n; j++) y += num[j];

		a = ((double)n*xy - (double)x*y) / ((double)n*xx - (double)x*x);
		b = ((double)xx*y - (double)x*xy) / ((double)n*xx - (double)x*x);
		(*vphth) = (-1)*b / a;
	}

	return flg;
}
int GetVPHth(vector<base_track_t_rank> bt, double vph_mean, double vph_rms, double *vphth)
{
	int flg = 0;
	int i, j;
	int k = 0;
	for (i = 0; i < bt.size(); i++) {

		if (bt[i].vph > vph_mean - vph_rms && bt[i].vph <= vph_mean) k++;
	}
	double vph_signal = (double)k / vph_rms;

	int vph_min = 100000;
	for (i = 0; i < bt.size(); i++) {
		if (vph_min > bt[i].vph) vph_min = bt[i].vph;
	}
	k = 0;
	for (i = 0; i < bt.size(); i++) {
		if (bt[i].vph == vph_min || bt[i].vph == vph_min + 1) k++;
	}
	double vph_noise = (double)k / 2;

	if (vph_noise >= vph_signal * 2.5) flg = 1;

	int vph_fit = (int)(vph_mean - vph_rms * 2);
	int n = vph_fit - vph_min + 1;
	if (n <= 1) flg = 0;

	if (flg == 1) {

		vector<int> num;
		vector<int> vph;

		for (j = 0; j < n; j++) num.push_back(0);
		for (j = 0; j < n; j++) vph.push_back(vph_min + j);

		for (i = 0; i < bt.size(); i++) {
			for (j = 0; j < n; j++) {
				if (bt[i].vph == vph[j]) num[j] += 1;
			}
		}

		//		for(j=0;j<n;j++) printf("%d %d\n",vph[j],num[j]);

		double a, b;
		int xx, xy, yy, x, y;
		xx = 0; xy = 0; yy = 0; x = 0; y = 0;
		for (j = 0; j < n; j++) xx += vph[j] * vph[j];
		for (j = 0; j < n; j++) xy += vph[j] * num[j];
		for (j = 0; j < n; j++) yy += num[j] * num[j];
		for (j = 0; j < n; j++) x += vph[j];
		for (j = 0; j < n; j++) y += num[j];

		a = ((double)n*xy - (double)x*y) / ((double)n*xx - (double)x*x);
		b = ((double)xx*y - (double)x*xy) / ((double)n*xx - (double)x*x);
		(*vphth) = (-1)*b / a;
	}

	return flg;
}
double GetMean_da(vector<base_track_t_rank> b)
{
	double da_sum = 0.0;
	double da_mean;

	for (int i = 0; i < b.size(); i++) {

		da_sum += b[i].da;
	}
	da_mean = da_sum / (double)b.size();

	return da_mean;
}
double GetRMS_da(vector<base_track_t_rank> bt, double da_mean)
{
	double dda, da_rms;
	double dda_sum = 0;

	for (int i = 0; i < bt.size(); i++) {

		dda = bt[i].da - da_mean;

		dda_sum += dda * dda;
	}
	da_rms = sqrt(dda_sum / (double)bt.size());

	return da_rms;
}
double GetMean_vph(vector<base_track_t_rank> bt)
{
	double vph_sum = 0;
	double vph_mean;

	for (int i = 0; i < bt.size(); i++) {

		vph_sum += bt[i].vph;
	}
	vph_mean = vph_sum / (double)bt.size();

	return vph_mean;
}
double GetRMS_vph(vector<base_track_t_rank> bt, double vph_mean)
{
	double dvph, vph_rms;
	double dvph_sum = 0;

	for (int i = 0; i < bt.size(); i++) {

		dvph = (double)bt[i].vph - vph_mean;

		dvph_sum += dvph * dvph;
	}
	vph_rms = sqrt(dvph_sum / (double)bt.size());

	return vph_rms;
}
void GetRange_vph(vector<base_track_t_rank> bt, int *vph_rth1, int *vph_rth2, int k)
{
	int i, j;
	vector<int> vph;
	for (j = 0; j < 40; j++) vph.push_back(0);

	double dath = 0.01 + 0.003*(double)k;
	int vphth = (int)(10.0 - (double)k / 3);

	for (i = 0; i < bt.size(); i++) {
		for (j = 0; j < 40; j++) {
			if (bt[i].da < dath && bt[i].vph > vphth) {
				if ((int)((double)bt[i].vph / 4) == j) {
					vph[j] += 1;
				}
			}
		}
	}

	int vphmax;
	int vphmaxnum = -1;
	for (j = 0; j < 40; j++) {
		if (vph[j] > vphmaxnum) {
			vphmaxnum = vph[j];
			vphmax = j;
		}
	}
	int vphrangenum = (int)((double)vphmaxnum / 4);
	int vphrange1, vphrange2;

	for (j = vphmax; j < 40; j++) {
		if (vph[j] < vphrangenum) {
			vphrange1 = j;
			break;
		}
	}
	for (j = vphmax; j >= 0; j--) {
		if (vph[j] < vphrangenum) {
			vphrange2 = j;
			break;
		}
	}

	(*vph_rth1) = vphrange1 * 4 + 2;
	(*vph_rth2) = vphrange2 * 4 + 2;
}
void GetRange_vph_Lateral(vector<base_track_t_rank>bt, int *vph_rth1, int *vph_rth2)
{
	int i, j;
	vector<int> vph;
	for (j = 0; j < 40; j++) vph.push_back(0);

	double dath = 0.015;
	int vphth = 7;

	for (i = 0; i < bt.size(); i++) {
		for (j = 0; j < 40; j++) {
			if (bt[i].da_lateral < dath && bt[i].vph > vphth) {
				if ((int)((double)bt[i].vph / 4) == j) {
					vph[j] += 1;
				}
			}
		}
	}

	int vphmax;
	int vphmaxnum = -1;
	for (j = 4; j < 40; j++) {
		if (vph[j] > vphmaxnum) {
			vphmaxnum = vph[j];
			vphmax = j;
		}
	}
	int vphrangenum = (int)((double)vphmaxnum / 4);
	int vphrange1, vphrange2;

	for (j = vphmax; j < 40; j++) {
		if (vph[j] < vphrangenum) {
			vphrange1 = j;
			break;
		}
	}
	vphrange2 = 0;
	for (j = vphmax; j >= 0; j--) {
		if (vph[j] < vphrangenum) {
			vphrange2 = j;
			break;
		}
	}

	(*vph_rth1) = vphrange1 * 4 + 2;
	(*vph_rth2) = vphrange2 * 4 + 2;
}
void basetrack_flg_apply(vector<base_track_t_rank>&bt) {
	vector<base_track_t_rank> base;
	for (auto itr = bt.begin(); itr != bt.end(); itr++) {
		if (itr->flg == 1) {
			base.push_back(*itr);
		}
	}
	bt = base;
}
