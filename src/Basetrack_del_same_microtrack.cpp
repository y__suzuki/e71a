#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"my_algorithm.lib")
#pragma comment(lib,"functions.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include <my_algorithm.hpp>
#include <functions.hpp>
#include <set>
#include "VxxReader.h"

class Basetrack_val {
public:
	double val, dr, dl;
	int flg, vph, ph;
	std::vector<vxx::base_track_t> base;
};
bool sort_id0(const vxx::base_track_t& left, const vxx::base_track_t& right) {
	return left.m[0].rawid == right.m[0].rawid ? left.m[1].rawid < right.m[1].rawid : left.m[0].rawid < right.m[0].rawid;
}
bool sort_id1(const vxx::base_track_t& left, const vxx::base_track_t& right) {
	return left.m[1].rawid == right.m[1].rawid ? left.m[0].rawid < right.m[0].rawid : left.m[1].rawid < right.m[1].rawid;
}

std::vector<std::pair<int, int>> multi_id_pair(std::vector<vxx::base_track_t> &base, std::vector<std::pair<int, int>>&single);
std::vector<Basetrack_val> BasetrackConvolution_val(std::vector<vxx::base_track_t> base);
std::vector<std::vector<vxx::base_track_t>> BasetrackConbination(std::vector<vxx::base_track_t> base, int Basetracknum);
std::vector<std::vector<std::pair<int, int>>> IDConbination(std::vector<int> id0, std::vector<int> id1);
std::vector<std::vector<std::pair<int, int>>> RemoveException(std::vector<std::vector<std::pair<int, int>>> all_pair, std::vector<std::pair<int, int>> exception_pair);
std::vector<Basetrack_val> SelectBasetrack_val(std::vector<std::vector<vxx::base_track_t>> base);
std::vector<Basetrack_val> SelectBestBasetrack_val(std::vector<vxx::base_track_t> base, int Basetracknum);
std::vector<Basetrack_val> SelectHighPHBasetrack_val(std::vector<vxx::base_track_t> base, int Basetracknum);

void Calc_ConnectionValue(std::vector<vxx::base_track_t> base, Basetrack_val &ret);
double Calc_ConnectionValue(vxx::base_track_t base);
int Permutation(int n, int r);
void Permutation(int n, int r, int &step, std::vector<std::vector<int>> &per);

int main(int argc, char *argv[])
{
	if (argc != 5) {
		fprintf(stderr, "usage:prg input-bvxx pl zone ouput-bvxx\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_bvxx = argv[4];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, zone);
	std::vector<vxx::base_track_t> base_out;


	std::multimap<int, vxx::base_track_t *> m0_raw;
	std::multimap<int, vxx::base_track_t *> m1_raw;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		m0_raw.insert(std::make_pair(itr->m[0].rawid, &(*itr)));
		m1_raw.insert(std::make_pair(itr->m[1].rawid, &(*itr)));
	}


	std::vector<std::pair<int, int>> multi, single;

	multi = multi_id_pair(base, single);
	for (auto itr = single.begin(); itr != single.end(); itr++) {
		base_out.push_back(*m0_raw.find(itr->first)->second);
	}
	std::vector<id_pair> pair = id_clustering(multi);

	std::vector<vxx::base_track_t> multi_base;
	std::vector<vxx::base_track_t> sel_base;

	int count = 0;
	std::map<std::pair<int, int>, int> multi_count;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		std::map<std::pair<int, int>, vxx::base_track_t> out_base_map;
		for (auto itr2 = itr->rawid0.begin(); itr2 != itr->rawid0.end(); itr2++) {
			if (m0_raw.count(*itr2) == 0) {
				fprintf(stderr, "exception base not found\n");
				exit(1);
			}
			else if (m0_raw.count(*itr2) == 1) {
				auto res = m0_raw.find(*itr2);
				out_base_map.insert(std::make_pair(std::make_pair(res->second->m[0].rawid, res->second->m[1].rawid), *(res->second)));

			}
			else {
				auto range = m0_raw.equal_range(*itr2);
				for (auto itr3 = range.first; itr3 != range.second; itr3++) {
					out_base_map.insert(std::make_pair(std::make_pair(itr3->second->m[0].rawid, itr3->second->m[1].rawid), *itr3->second));
				}
			}
		}
		for (auto itr2 = itr->rawid1.begin(); itr2 != itr->rawid1.end(); itr2++) {
			if (m1_raw.count(*itr2) == 0) {
				fprintf(stderr, "exception base not found\n");
				exit(1);
			}
			else if (m1_raw.count(*itr2) == 1) {
				auto res = m1_raw.find(*itr2);
				out_base_map.insert(std::make_pair(std::make_pair(res->second->m[0].rawid, res->second->m[1].rawid), *res->second));

			}
			else {
				auto range = m1_raw.equal_range(*itr2);
				for (auto itr3 = range.first; itr3 != range.second; itr3++) {
					out_base_map.insert(std::make_pair(std::make_pair(itr3->second->m[0].rawid, itr3->second->m[1].rawid), *itr3->second));
				}
			}
		}

		std::vector<vxx::base_track_t> out_base;
		for (auto itr2 = out_base_map.begin(); itr2 != out_base_map.end(); itr2++) {
			out_base.push_back(itr2->second);
		}


		std::vector<Basetrack_val> base_val = BasetrackConvolution_val(out_base);
		for (auto itr2 = base_val.begin(); itr2 != base_val.end(); itr2++) {
			if (itr2->flg) {
				for (auto itr3 = itr2->base.begin(); itr3 != itr2->base.end(); itr3++) {
					base_out.push_back(*itr3);
				}
			}
		}
	}

	printf("multi del %d --> %d\n", base.size(), base_out.size());

	vxx::BvxxWriter w;
	w.Write(file_out_bvxx, pl, zone, base_out);

}
std::vector<std::pair<int, int>> multi_id_pair(std::vector<vxx::base_track_t> &base, std::vector<std::pair<int, int>>&single) {
	std::set <int> single_cand0;
	std::multimap <int, int> single_cnad1;
	std::set <std::pair<int, int>> single_pair;

	std::multimap <int, int> rawid0;
	std::multimap <int, int> rawid1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		rawid0.insert(std::make_pair(itr->m[0].rawid, itr->m[1].rawid));
		rawid1.insert(std::make_pair(itr->m[1].rawid, itr->m[0].rawid));
	}

	for (auto itr = rawid0.begin(); itr != rawid0.end(); itr++) {
		if (rawid0.count(itr->first) == 1) {
			single_cand0.insert(itr->second);
		}
	}
	for (auto itr = single_cand0.begin(); itr != single_cand0.end(); itr++) {
		if (rawid1.count(*itr) == 1) {
			auto res = rawid1.find(*itr);
			single_pair.insert(std::make_pair(res->second, res->first));
		}
	}


	std::vector<std::pair<int, int>> ret;

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (single_pair.count(std::make_pair(itr->m[0].rawid, itr->m[1].rawid)) == 0) {
			ret.push_back(std::make_pair(itr->m[0].rawid, itr->m[1].rawid));
		}
		else {
			single.push_back(std::make_pair(itr->m[0].rawid, itr->m[1].rawid));
		}
	}
	printf("all    =%d\n", base.size());
	printf("single =%d\n", single.size());
	printf("multi  =%d\n", ret.size());

	printf("multi select %d --> %d (%4.1lf%%)\n", base.size(), ret.size(), ret.size()*100. /base.size());

	return ret;
}

std::vector<Basetrack_val> BasetrackConvolution_val(std::vector<vxx::base_track_t> base) {
	std::vector<Basetrack_val> ret;

	//最終的に残すbasetrackの本数
	int Basetracknum, MicroNum0 = 0, MicroNum1 = 0, id_tmp = -1;
	sort(base.begin(), base.end(), sort_id0);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (id_tmp != itr->m[0].rawid) {
			MicroNum0++;
			id_tmp = itr->m[0].rawid;
		}
	}
	id_tmp = -1;
	sort(base.begin(), base.end(), sort_id1);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (id_tmp != itr->m[1].rawid) {
			MicroNum1++;
			id_tmp = itr->m[1].rawid;
		}
	}
	Basetracknum = std::min(MicroNum0, MicroNum1);
	ret = SelectHighPHBasetrack_val(base, Basetracknum);
	return ret;
}

//LinkletConbination(IDConbination,Permutation,RemoveException)+SelectLinklet でlinkletの選択
//unique　linkletの組み合わせを返す
std::vector<std::vector<vxx::base_track_t>> BasetrackConbination(std::vector<vxx::base_track_t> base, int Basetracknum) {
	std::vector<std::vector<vxx::base_track_t>> ret_base;

	//id0のlist
	std::vector<int> id0;
	//id1のlist
	std::vector<int> id1;
	//basetrackで繋がれないid pairのlist
	std::vector<std::pair<int, int>> exception_pair;

	//id0のlist作成
	int id_tmp = -1;
	sort(base.begin(), base.end(), sort_id0);
	for (auto itr =base.begin(); itr != base.end(); itr++) {
		if (id_tmp != itr->m[0].rawid) {
			id0.push_back(itr->m[0].rawid);
			id_tmp = itr->m[0].rawid;
		}
	}
	//id1のlist作成
	id_tmp = -1;
	sort(base.begin(), base.end(), sort_id1);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (id_tmp != itr->m[1].rawid) {
			id1.push_back(itr->m[1].rawid);
			id_tmp = itr->m[1].rawid;
		}
	}
	//exception pairのlist作成
	int flg = 0;
	sort(base.begin(), base.end(), sort_id0);
	for (int i = 0; i < id0.size(); i++) {
		for (int j = 0; j < id1.size(); j++) {
			flg = 0;
			for (auto itr = base.begin(); itr != base.end() && flg == 0; itr++) {
				if (id0[i] == itr->m[0].rawid&&id1[j] == itr->m[1].rawid) {
					flg = 1;
				}
			}
			if (flg == 0) {
				exception_pair.push_back(std::make_pair(id0[i], id1[j]));
			}
		}
	}

	//printf("id0 = %d id1 = %d exception = %d\n", int(id0.size()), int(id1.size()), int(exception_pair.size()));
	std::vector<std::vector<std::pair<int, int>>> all_pair, slct_pair;
	//IDpairでできるすべての組み合わせを列挙
	all_pair = IDConbination(id0, id1);

	//Exceprion pairを除く
	slct_pair = RemoveException(all_pair, exception_pair);
	//printf("all_pair = %d remove_pair = %d\n", int(all_pair.size()), int(slct_pair.size()));

	//
	//ここまでで、linkletの組み合わせの抽出完了。
	//
	//以下id でlinkletの抽出
	//
	//
	sort(base.begin(), base.end(), sort_id0);
	for (int i = 0; i < slct_pair.size(); i++) {
		std::vector<vxx::base_track_t> base_tmp;
		auto itr = base.begin();
		sort(slct_pair[i].begin(), slct_pair[i].end());
		for (int j = 0; j < slct_pair[i].size(); j++) {
			for (; itr != base.end(); itr++) {
				if (itr->m[0].rawid == slct_pair[i][j].first&&itr->m[1].rawid == slct_pair[i][j].second) {
					base_tmp.push_back(*itr);
					break;
				}
			}
		}
		ret_base.push_back(base_tmp);
	}
	return ret_base;
}
//nPr通りのlinkletが作成可能
//少ない方はidでソートして入れる
//nこの数字を使ってr個の配列を作成、その数字のIDを持ってくる
std::vector<std::vector<std::pair<int, int>>> IDConbination(std::vector<int> id0, std::vector<int> id1) {
	int id0_num, id1_num, link_num, conb_num;
	id0_num = id0.size();
	id1_num = id1.size();
	link_num = std::min(id0_num, id1_num);
	conb_num = Permutation(std::max(id0_num, id1_num), link_num);

	std::vector<std::vector<std::pair<int, int>>> ret;

	if (id0_num == link_num) {

		//id0<id1またはid0=id1
		sort(id0.begin(), id0.end());
		sort(id1.begin(), id1.end());

		int step = 0;
		std::vector<std::vector<int>> per;
		Permutation(id1_num, id0_num, step, per);
		for (int i = 0; i < per.size(); i++) {
			std::vector<std::pair<int, int>> pair_tmp;
			for (int j = 0; j < per[i].size(); j++) {
				pair_tmp.push_back(std::make_pair(id0[j], id1[per[i][j]]));
			}
			ret.push_back(pair_tmp);
		}
	}
	else {
		//id0>id1
		sort(id0.begin(), id0.end());
		sort(id1.begin(), id1.end());

		int step = 0;
		std::vector<std::vector<int>> per;
		Permutation(id0_num, id1_num, step, per);
		for (int i = 0; i < per.size(); i++) {
			std::vector<std::pair<int, int>> pair_tmp;
			for (int j = 0; j < per[i].size(); j++) {
				pair_tmp.push_back(std::make_pair(id0[per[i][j]], id1[j]));
			}
			ret.push_back(pair_tmp);
		}
	}

	return ret;
}
//IDConbinationで作った組み合わせに対して、ExceptionListにあるものを除く
std::vector<std::vector<std::pair<int, int>>> RemoveException(std::vector<std::vector<std::pair<int, int>>> all_pair, std::vector<std::pair<int, int>> exception_pair) {
	std::vector<std::vector<std::pair<int, int>>> ret;
	std::vector<std::vector<std::pair<int, int>>> ret_tmp;
	//exception pairの要素を削除
	int flg = 0;
	for (int i = 0; i < all_pair.size(); i++) {
		std::vector<std::pair<int, int>> ret_tmptmp;
		for (int j = 0; j < all_pair[i].size(); j++) {
			flg = 0;
			for (auto itr = exception_pair.begin(); itr != exception_pair.end(); itr++) {
				if (all_pair[i][j].first == itr->first&&all_pair[i][j].second == itr->second) {
					flg = 1;
					break;
				}
			}
			if (flg == 0) {
				ret_tmptmp.push_back(all_pair[i][j]);
			}
		}
		ret_tmp.push_back(ret_tmptmp);
	}
	int maxLink = 0;
	for (int i = 0; i < ret_tmp.size(); i++) {
		if (maxLink < ret_tmp[i].size()) {
			maxLink = ret_tmp[i].size();
		}
	}
	for (int i = 0; i < ret_tmp.size(); i++) {
		if (maxLink == ret_tmp[i].size()) {
			ret.push_back(ret_tmp[i]);
		}
	}
	return ret;
}
//Linkletの組み合わせに対して一番良いlinkletを返す
std::vector<Basetrack_val> SelectBasetrack_val(std::vector<std::vector<vxx::base_track_t>> base) {
	std::vector<Basetrack_val> ret;
	int ret_i = -1;
	double angle;
	int num = 0;

	for (int i = 0; i < base.size(); i++) {
		Basetrack_val base_tmp;
		Calc_ConnectionValue(base[i],base_tmp);
		ret.push_back(base_tmp);
	}

	auto sel = ret.begin();
	double CV = ret.begin()->val;
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		if (CV > itr->val) {
			CV = itr->val;
			sel = itr;
		}
	}
	sel->flg = 1;
	return ret;
}
std::vector<Basetrack_val> SelectBestBasetrack_val(std::vector<vxx::base_track_t> base, int Basetracknum) {
	std::vector<Basetrack_val> ret;
	int flg = 0, count = 0;
	double dr, dl;
	Basetrack_val base_val;
	std::vector<vxx::base_track_t> base_tmp;
	while (flg == 0) {
		double cval_now, Cval = -1;
		flg = 0;
		std::vector<vxx::base_track_t>::iterator Citr;

		for (auto itr = base.begin(); itr != base.end(); itr++) {
			//linkletの評価に使う変数cval(connection value)

			cval_now = Calc_ConnectionValue(*itr);
			if (itr == base.begin() || cval_now < Cval) {
				Cval = cval_now;
				Citr = itr;
			}
		}
		base_tmp.push_back(*Citr);
		int Bid0 = Citr->m[0].rawid;
		int Bid1 = Citr->m[1].rawid;
		count++;

		//選ばれたlinkletで使用しているbasetrackの削除
		auto itr = base.begin();
		while (itr != base.end()) {
			if (itr->m[0].rawid == Bid0 || itr->m[1].rawid == Bid1) {
				itr = base.erase(itr);
			}
			else
			{
				itr++;
			}

		}
		if (count >= Basetracknum) {
			flg = 1;
		}
		else if (base.size() == 0) {
			//この場合最大限のlinkletを残せるようなselectionにしたい
			//最大linklet数になる組み合わせを抽出-->複数候補の場合はCvalで選択
			flg = 2;
		}
	}

	double  angle = 0;
	double gap = 0;
	int num = 0;
	Calc_ConnectionValue(base_tmp, base_val);
	base_val.flg = 1;
	ret.push_back(base_val);
	return ret;
}
std::vector<Basetrack_val> SelectHighPHBasetrack_val(std::vector<vxx::base_track_t> base, int Basetracknum) {
	std::vector<Basetrack_val> ret;
	int flg = 0, count = 0;
	double dr, dl;
	Basetrack_val base_val;
	std::vector<vxx::base_track_t> base_tmp;
	while (flg == 0) {
		double cval_now, Cval = -1;
		flg = 0;
		std::vector<vxx::base_track_t>::iterator Citr;

		for (auto itr = base.begin(); itr != base.end(); itr++) {
			//linkletの評価に使う変数cval(connection value)

			cval_now = itr->m[0].ph + itr->m[1].ph;
			if (itr == base.begin() || cval_now > Cval) {
				Cval = cval_now;
				Citr = itr;
			}
		}
		base_tmp.push_back(*Citr);
		int Bid0 = Citr->m[0].rawid;
		int Bid1 = Citr->m[1].rawid;
		count++;

		//選ばれたlinkletで使用しているbasetrackの削除
		auto itr = base.begin();
		while (itr != base.end()) {
			if (itr->m[0].rawid == Bid0 || itr->m[1].rawid == Bid1) {
				itr = base.erase(itr);
			}
			else
			{
				itr++;
			}

		}
		if (base.size() == 0) {
			//basetrackがすべて削除済み-->選択終了
			flg = 1;
		}
	}

	Calc_ConnectionValue(base_tmp, base_val);
	base_val.flg = 1;
	ret.push_back(base_val);
	return ret;
}
//connectionValueの計算
void Calc_ConnectionValue(std::vector<vxx::base_track_t> base, Basetrack_val &ret) {
	ret.dl = 0;
	ret.dr = 0;
	ret.val = 0;
	double dr_sigma, dl_sigma, angle, dr[2], dl[2];
	dl_sigma = 0.015;
	double vph_val[2];
	double vph_sig = 10;
	double vph_mean = 20;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ret.base.push_back(*itr);
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		dr_sigma = 0.1*angle + 0.015;
		dr[0] = ((itr->m[0].ax - itr->ax)*itr->ax + (itr->m[0].ay - itr->ay)*itr->ay) / angle;
		dr[1] = ((itr->m[1].ax - itr->ax)*itr->ax + (itr->m[1].ay - itr->ay)*itr->ay) / angle;
		dl[0] = ((itr->m[0].ax - itr->ax)*itr->ay - (itr->m[0].ay - itr->ay)*itr->ax) / angle;
		dl[1] = ((itr->m[1].ax - itr->ax)*itr->ay - (itr->m[1].ay - itr->ay)*itr->ax) / angle;
		vph_val[0] = -1*(itr->m[0].ph % 10000 - vph_mean) / (vph_sig*sqrt(1 + pow((itr->m[0].ph % 10000 - vph_mean) / vph_sig, 2))) + 1;
		vph_val[1] = -1*(itr->m[1].ph % 10000 - vph_mean) / (vph_sig*sqrt(1 + pow((itr->m[0].ph % 10000 - vph_mean) / vph_sig, 2))) + 1;
		ret.dr += sqrt(pow(dr[0], 2) + pow(dr[1], 2));
		ret.dl += sqrt(pow(dl[0], 2) + pow(dl[1], 2));
		ret.val += pow(dr[0] / dr_sigma, 2) + pow(dr[1] / dr_sigma, 2) + pow(dl[0] / dl_sigma, 2) + pow(dl[1] / dl_sigma, 2) + pow(vph_val[0]*2, 2) + pow(vph_val[1]*2, 2);
	}
	ret.val = sqrt(ret.val);
	ret.flg = 0;
}
double Calc_ConnectionValue(vxx::base_track_t base) {
	double angle, dr[2], dl[2];
	angle = sqrt(base.ax*base.ax + base.ay*base.ay);
	double vph_sig = 10;
	double vph_mean = 20;
	double vph_val[2];
	double dr_sigma, dl_sigma;
	dl_sigma = 0.015;
	dr_sigma = 0.1*angle + 0.015;
	dr[0] = ((base.m[0].ax - base.ax)*base.ax + (base.m[0].ay - base.ay)*base.ay) / angle;
	dr[1] = ((base.m[1].ax - base.ax)*base.ax + (base.m[1].ay - base.ay)*base.ay) / angle;
	dl[0] = ((base.m[0].ax - base.ax)*base.ay - (base.m[0].ay - base.ay)*base.ax) / angle;
	dl[1] = ((base.m[1].ax - base.ax)*base.ay - (base.m[1].ay - base.ay)*base.ax) / angle;
	vph_val[0] = -1 * (base.m[0].ph % 10000 - vph_mean) / (vph_sig*sqrt(1 + pow((base.m[0].ph % 10000 - vph_mean) / vph_sig, 2))) + 1;
	vph_val[1] = -1 * (base.m[1].ph % 10000 - vph_mean) / (vph_sig*sqrt(1 + pow((base.m[0].ph % 10000 - vph_mean) / vph_sig, 2))) + 1;

	return pow(dr[0] / dr_sigma, 2) + pow(dr[1] / dr_sigma, 2) + pow(dl[0] / dl_sigma, 2) + pow(dl[1] / dl_sigma, 2) + pow(vph_val[0]*2, 2) + pow(vph_val[1]*2, 2);

}
int Permutation(int n, int r) {
	int calc = 1;
	for (int i = 0; i < r; i++) {
		calc = calc * (n - i);
	}
	return calc;
}
void Permutation(int n, int r, int &step, std::vector<std::vector<int>> &per) {
	//printf("step=%d n=%d r=%d\n", step, n, r);
	if (step == 0) {

		per.clear();
		for (int i = 0; i < n; i++) {
			std::vector<int> tmp{ i };
			per.push_back(tmp);
		}
		step++;
		Permutation(n, r, step, per);

	}
	else if (step < r) {
		//step=sの段階でnPs個の配列が生成されている
		int flg = 0;
		std::vector<std::vector<int>>tmp_v;
		std::vector<std::vector<int>>_per(per);

		for (int i = 0; i < _per.size(); i++) {
			for (int j = 0; j < n; j++) {
				flg = 0;
				for (int k = 0; k < _per[i].size(); k++) {
					if (j == _per[i][k]) {
						flg = 1;
						break;
					}
				}
				if (flg == 0) {
					std::vector<int> add_v(_per[i]);
					add_v.push_back(j);
					tmp_v.push_back(add_v);
				}
			}
		}
		per = tmp_v;
		step++;
		Permutation(n, r, step, per);
	}
	else {
		return;
	}

}
