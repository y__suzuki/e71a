#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"
#include <list>

bool sort_m_raw0(const vxx::base_track_t &left, const vxx::base_track_t &right) {
	if (left.m[0].zone == right.m[0].zone) {
		return left.m[0].rawid < right.m[0].rawid;
	}
	else {
		return left.m[0].zone < right.m[0].zone;
	}
}
bool sort_m_raw1(const vxx::base_track_t &left, const vxx::base_track_t &right) {
	if (left.m[1].zone == right.m[1].zone) {
		return left.m[1].rawid < right.m[1].rawid;
	}
	else {
		return left.m[1].zone < right.m[1].zone;
	}
}


void DeleteSameMicroTrk0(std::list<vxx::base_track_t>&base);
void DeleteSameMicroTrk1(std::list<vxx::base_track_t>&base);

int main(int argc, char *argv[])
{

	if (argc != 4) {
		fprintf(stderr, "usage:prg input-bvxx pl output-bvxx\n");
		exit(1);
	}

	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_out_bvxx = argv[3];

	std::vector<vxx::base_track_t> base;
	vxx::BvxxReader br;
	base = br.ReadAll(file_in_bvxx, pl, 0);
	std::list<vxx::base_track_t> base_list(base.begin(), base.end());
	base.clear();

	DeleteSameMicroTrk0(base_list);
	DeleteSameMicroTrk1(base_list);

	std::vector<vxx::base_track_t> out(base_list.begin(), base_list.end());

	vxx::BvxxWriter bw;
	bw.Write(file_out_bvxx, pl, 0, out);
}

void DeleteSameMicroTrk0(std::list<vxx::base_track_t>&base) {
	int all = base.size();
	base.sort(sort_m_raw0);
	double da0, da1;
	for (auto itr = base.begin(); itr != base.end(); ) {
		if (std::next(itr, 1) == base.end())break;
		auto itr_next = std::next(itr, 1);
		if (itr->m[0].zone == itr_next->m[0].zone&&itr->m[0].rawid == itr_next->m[0].rawid) {
			if (itr->m[1].ph < itr_next->m[1].ph) {
				itr = base.erase(itr);
			}
			else if (itr->m[1].ph > itr_next->m[1].ph) {
				base.erase(itr_next);
			}
			else {
				da0 = (itr->ax - itr->m[1].ax)*(itr->ax - itr->m[1].ax) + (itr->ay - itr->m[1].ay)*(itr->ay - itr->m[1].ay);
				da1 = (itr_next->ax - itr_next->m[1].ax)*(itr_next->ax - itr_next->m[1].ax) + (itr_next->ay - itr_next->m[1].ay)*(itr_next->ay - itr_next->m[1].ay);
				if (da0 < da1) {
					itr = base.erase(itr);
				}
				else {
					base.erase(itr_next);
				}
			}
		}
		else {
			itr++;
		}
	}
	int sel = base.size();
	fprintf(stderr, "Delete Same MicroTrk (isg2) %d/%d\n", sel,all);
}
void DeleteSameMicroTrk1(std::list<vxx::base_track_t>&base) {
	int all = base.size();
	base.sort(sort_m_raw1);
	double da0, da1;
	for (auto itr = base.begin(); itr != base.end(); ) {
		if (std::next(itr, 1) == base.end())break;
		auto itr_next = std::next(itr, 1);
		if (itr->m[1].zone == itr_next->m[1].zone&&itr->m[1].rawid == itr_next->m[1].rawid) {
			if (itr->m[0].ph < itr_next->m[0].ph) {
				itr = base.erase(itr);
			}
			else if (itr->m[0].ph > itr_next->m[0].ph) {
				base.erase(itr_next);
			}
			else {
				da0 = (itr->ax - itr->m[0].ax)*(itr->ax - itr->m[0].ax) + (itr->ay - itr->m[0].ay)*(itr->ay - itr->m[0].ay);
				da1 = (itr_next->ax - itr_next->m[0].ax)*(itr_next->ax - itr_next->m[0].ax) + (itr_next->ay - itr_next->m[0].ay)*(itr_next->ay - itr_next->m[0].ay);
				if (da0 < da1) {
					itr = base.erase(itr);
				}
				else {
					base.erase(itr_next);
				}
			}
		}
		else {
			itr++;
		}
	}
	int sel = base.size();
	fprintf(stderr, "Delete Same MicroTrk (isg1) %d/%d\n", sel,all);
}
