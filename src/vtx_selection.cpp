#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
# include <iostream>
# include <random>
#include <set>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <list>
class stop_track {
public:
	uint64_t chainid, groupid;
	int  nseg, npl, pl0, pl1, ph, rawid;
	double ax, ay, x, y, z;
};
class track_pair {
public:
	int pl0, pl1, raw0, raw1;
	double x, y, z, md, oa;
};
class track_multi {
public:
	int eventid;
	double x, y, z;
	std::vector< std::pair<double, stop_track>> trk;
	std::vector<track_pair>pair;
};

void read_vtx_file(std::string filename, std::vector<track_multi>&multi);
std::vector<track_multi> vtx_selection(std::vector<track_multi>multi);
void set_z(std::vector<mfile0::M_Chain> &c, std::map <int, double> &z);
void base_inverted_trance(vxx::base_track_t &base, corrmap0::Corrmap param);
void out_manualcheck_list(std::vector<track_multi>multi, std::map <int, double>z, std::string file_out_bvxx_path, std::vector<corrmap0::Corrmap> corr);
corrmap0::Corrmap search_corrmap(int pl, std::vector<corrmap0::Corrmap> corr);
void output_vtx_inf(track_multi multi, std::string filename, mfile0::Mfile &m);
void vtx_divide_int(std::vector<track_multi>&in, std::vector<track_multi>&water, std::vector<track_multi>&fe);
void output_vtx_all(std::vector < track_multi> multi, std::string filename, mfile0::Mfile &m);
void output_vtx(std::string filename, std::vector<track_multi> vtx);
std::vector<track_multi> vtx_selection_multi(std::vector<track_multi>multi);

std::vector<track_multi> vtx_selection_reach_downstream(std::vector<track_multi>multi);
void show_multiplicity(std::vector<track_multi> multi);
double black_threshold(double angle);
std::vector<track_multi> vtx_selection_thin_black(std::vector<track_multi>multi, int n_black, int n_thin);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg in-vtx out_vtx_path\n");
		exit(1);
	}
	std::string file_in_vtx = argv[1];
	std::string file_out_vtx_path = argv[2];
	std::stringstream file_out_water;
	file_out_water << file_out_vtx_path << "\\vtx_water.sel.txt";
	std::stringstream file_out_fe;
	file_out_fe << file_out_vtx_path << "\\vtx_fe.sel.txt";

	std::vector<track_multi> multi, fe, water;
	read_vtx_file(file_in_vtx, multi);

	vtx_divide_int(multi, water, fe);


	//water
	printf("water event cand\n");
	show_multiplicity(water);
	water = vtx_selection_reach_downstream(water);
	show_multiplicity(water);
	water = vtx_selection_multi(water);
	output_vtx(file_out_water.str(), water);

	//fe
	printf("\n");
	printf("---------------------------\n");
	printf("fe event cand\n");
	show_multiplicity(fe);
	fe = vtx_selection_reach_downstream(fe);
	show_multiplicity(fe);
	std::vector<track_multi> vtx_2p2h = vtx_selection_thin_black(fe, 1, 1);

	fe = vtx_selection_multi(fe);
	output_vtx(file_out_fe.str(), fe);

	std::stringstream file_out_fe_2p2h;
	file_out_fe_2p2h << file_out_vtx_path << "\\vtx_fe_1m1b.sel.txt";

	output_vtx(file_out_fe_2p2h.str(), vtx_2p2h);

}
void read_vtx_file(std::string filename, std::vector<track_multi>&multi) {
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0, trk_num;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);

		track_multi m;
		m.eventid = std::stoi(str_v[0]);
		m.x = std::stod(str_v[2]);
		m.y = std::stod(str_v[3]);
		m.z = std::stod(str_v[4]);
		trk_num = std::stoi(str_v[1]);

		for (int i = 0; i < trk_num*(trk_num - 1) / 2; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			track_pair p;
			p.pl0 = std::stoi(str_v[0]);
			p.raw0 = std::stoi(str_v[1]);
			p.pl1 = std::stoi(str_v[2]);
			p.raw1 = std::stoi(str_v[3]);
			p.x = std::stod(str_v[4]);
			p.y = std::stod(str_v[5]);
			p.z = std::stod(str_v[6]);
			p.oa = std::stod(str_v[7]);
			p.md = std::stod(str_v[8]);
			m.pair.push_back(p);
		}
		for (int i = 0; i < trk_num; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			stop_track s;
			s.pl0 = std::stoi(str_v[0]);
			s.pl1 = std::stoi(str_v[1]);
			s.rawid = std::stoi(str_v[2]);
			s.chainid = std::stoll(str_v[3]);
			s.groupid = std::stoll(str_v[4]);
			s.nseg = std::stoi(str_v[5]);
			s.npl = std::stoi(str_v[6]);
			s.ph = std::stoi(str_v[7]);
			s.ax = std::stod(str_v[8]);
			s.ay = std::stod(str_v[9]);
			s.x = std::stod(str_v[10]);
			s.y = std::stod(str_v[11]);
			s.z = 0;
			double ip = std::stod(str_v[12]);
			m.trk.push_back(std::make_pair(ip, s));
		}
		multi.push_back(m);
	}

}
std::vector<track_multi> vtx_selection_reach_downstream(std::vector<track_multi>multi) {
	std::vector<track_multi> ret;
	std::vector<track_multi> remain;
	bool flg = false;
	for (auto itr = multi.begin(); itr != multi.end(); itr++) {
		flg = false;
		for (auto itr2 = itr->trk.begin(); itr2 != itr->trk.end(); itr2++) {
			if (itr2->second.pl0 == 4)flg = true;
		}
		if (flg) {
			ret.push_back(*itr);
		}
		else {
			remain.push_back(*itr);
		}
	}
	printf("reach downstream %d --> %d\n", multi.size(), ret.size());
	return ret;
}
std::vector<track_multi> vtx_selection_multi(std::vector<track_multi>multi) {
	//3trk�ȏ�
	//2trk�ȏ�/1black(ave>=200)
	std::map<int, std::vector<track_multi>> multi_map;
	for (auto itr = multi.begin(); itr != multi.end(); itr++) {
		auto res = multi_map.find(itr->trk.size());
		if (res == multi_map.end()) {
			std::vector<track_multi> multi_tmp;
			multi_tmp.push_back(*itr);
			multi_map.insert(std::make_pair(itr->trk.size(), multi_tmp));
		}
		else {
			res->second.push_back(*itr);
		}
	}
	std::vector<track_multi> ret;
	std::multimap<std::tuple<int, int>, track_multi> black_selection;
	double angle, vph_ave;
	for (auto itr = multi_map.begin(); itr != multi_map.end(); itr++) {
		printf("%d %d\n", itr->first, itr->second.size());
		std::map<std::tuple<int, int>, int> blacknum;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			std::tuple<int, int> num;
			std::get<0>(num) = 0;
			std::get<1>(num) = 0;

			for (auto itr3 = itr2->trk.begin(); itr3 != itr2->trk.end(); itr3++) {
				if (itr3->second.ph % 10000 < black_threshold(sqrt(pow(itr3->second.ax, 2) + pow(itr3->second.ay, 2)))) {
					std::get<0>(num)++;
				}
				else {
					std::get<1>(num)++;
				}
			}
			//selction ����
			if (itr2->trk.size() >= 3 || std::get<1>(num) >= 1) {
				black_selection.insert(std::make_pair(num, *itr2));
			}

			auto res = blacknum.find(num);
			if (res == blacknum.end()) {
				blacknum.insert(std::make_pair(num, 1));
			}
			else {
				res->second++;
			}
		}
		for (auto itr2 = blacknum.begin(); itr2 != blacknum.end(); itr2++) {
			printf("\tmip:%d, black:%d  %d\n", std::get<0>(itr2->first), std::get<1>(itr2->first), itr2->second);
		}
	}

	printf("selected %d --> %d\n\n", multi.size(), black_selection.size());
	for (auto itr = black_selection.begin(); itr != black_selection.end(); itr++) {
		ret.push_back(itr->second);
	}
	return ret;
}
std::vector<track_multi> vtx_selection_thin_black(std::vector<track_multi>multi,int n_black,int n_thin) {
	//3trk�ȏ�
	//2trk�ȏ�/1black(ave>=200)
	std::map<int, std::vector<track_multi>> multi_map;
	for (auto itr = multi.begin(); itr != multi.end(); itr++) {
		auto res = multi_map.find(itr->trk.size());
		if (res == multi_map.end()) {
			std::vector<track_multi> multi_tmp;
			multi_tmp.push_back(*itr);
			multi_map.insert(std::make_pair(itr->trk.size(), multi_tmp));
		}
		else {
			res->second.push_back(*itr);
		}
	}
	std::vector<track_multi> ret;
	std::multimap<std::tuple<int, int>, track_multi> black_selection;
	double angle, vph_ave;
	for (auto itr = multi_map.begin(); itr != multi_map.end(); itr++) {
		printf("%d %d\n", itr->first, itr->second.size());
		std::map<std::tuple<int, int>, int> blacknum;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			std::tuple<int, int> num;
			std::get<0>(num) = 0;
			std::get<1>(num) = 0;

			for (auto itr3 = itr2->trk.begin(); itr3 != itr2->trk.end(); itr3++) {
				if (itr3->second.ph % 10000 < black_threshold(sqrt(pow(itr3->second.ax, 2) + pow(itr3->second.ay, 2)))) {
					std::get<0>(num)++;
				}
				else {
					std::get<1>(num)++;
				}
			}
			//selction ����
			if (std::get<0>(num) == n_thin && std::get<1>(num) == n_black) {
				black_selection.insert(std::make_pair(num, *itr2));
			}

			auto res = blacknum.find(num);
			if (res == blacknum.end()) {
				blacknum.insert(std::make_pair(num, 1));
			}
			else {
				res->second++;
			}
		}
		for (auto itr2 = blacknum.begin(); itr2 != blacknum.end(); itr2++) {
			printf("\tmip:%d, black:%d  %d\n", std::get<0>(itr2->first), std::get<1>(itr2->first), itr2->second);
		}
	}

	printf("selected %d --> %d\n\n", multi.size(), black_selection.size());
	for (auto itr = black_selection.begin(); itr != black_selection.end(); itr++) {
		ret.push_back(itr->second);
	}
	return ret;
}
void vtx_divide_int(std::vector<track_multi>&in, std::vector<track_multi>&water, std::vector<track_multi>&fe) {
	water.clear();
	fe.clear();
	for (auto itr = in.begin(); itr != in.end(); itr++) {
		if ((16 <= itr->trk.begin()->second.pl1) && (itr->trk.begin()->second.pl1 % 2 == 1)) {
			water.push_back(*itr);
		}
		else if ((16 <= itr->trk.begin()->second.pl1) && (itr->trk.begin()->second.pl1 % 2 == 0)) {
			fe.push_back(*itr);
		}
		else {

		}
	}
	printf("all %d --> water %d, fe %d\n", in.size(), water.size(), fe.size());
}
void output_vtx(std::string filename, std::vector<track_multi> vtx) {
	std::ofstream ofs(filename);
	for (auto itr0 = vtx.begin(); itr0 != vtx.end(); itr0++) {
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr0->eventid << " "
			<< std::setw(4) << std::setprecision(0) << itr0->trk.size() << " "
			<< std::setw(8) << std::setprecision(1) << itr0->x << " "
			<< std::setw(8) << std::setprecision(1) << itr0->y << " "
			<< std::setw(8) << std::setprecision(1) << itr0->z << std::endl;
		for (auto itr1 = itr0->pair.begin(); itr1 != itr0->pair.end(); itr1++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr1->pl0 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->raw0 << " "
				<< std::setw(4) << std::setprecision(0) << itr1->pl1 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->raw1 << " "
				<< std::setw(8) << std::setprecision(1) << itr1->x << " "
				<< std::setw(8) << std::setprecision(1) << itr1->y << " "
				<< std::setw(8) << std::setprecision(1) << itr1->z << " "
				<< std::setw(6) << std::setprecision(4) << itr1->oa << " "
				<< std::setw(4) << std::setprecision(1) << itr1->md << std::endl;
		}
		for (auto itr1 = itr0->trk.begin(); itr1 != itr0->trk.end(); itr1++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr1->second.pl0 << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.pl1 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->second.rawid << " "
				<< std::setw(12) << std::setprecision(0) << itr1->second.chainid << " "
				<< std::setw(12) << std::setprecision(0) << itr1->second.groupid << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.nseg << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.npl << " "
				<< std::setw(6) << std::setprecision(0) << itr1->second.ph << " "
				<< std::setw(7) << std::setprecision(4) << itr1->second.ax << " "
				<< std::setw(7) << std::setprecision(4) << itr1->second.ay << " "
				<< std::setw(8) << std::setprecision(1) << itr1->second.x << " "
				<< std::setw(8) << std::setprecision(1) << itr1->second.y << " "
				<< std::setw(4) << std::setprecision(1) << itr1->first << std::endl;
		}
	}
}
void show_multiplicity(std::vector<track_multi> multi) {
	std::map<int, int> multiplicity;
	for (auto itr = multi.begin(); itr != multi.end(); itr++) {
		auto res = multiplicity.insert(std::make_pair(itr->trk.size(), 1));
		if (!res.second) {
			res.first->second++;
		}
	}
	for (auto itr = multiplicity.begin(); itr != multiplicity.end(); itr++) {
		printf("%d track : %d\n", itr->first, itr->second);
	}
}
double black_threshold(double angle) {
	if (angle < 1.0) {
		return angle * -70 + 190;
	}
	else if (1.0 <= angle && angle < 2.0) {
		return angle * -30 + 150;
	}
	else if (2.0 <= angle && angle < 3.0) {
		return angle * -10 + 110;
	}
	else {
		return angle * -5 + 95;
	}
	return 200;
}