#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>

#include <set>
mfile0::Mfile penetrate_check_down(mfile0::Mfile &m, mfile0::Mfile &m_all);
mfile0::Mfile penetrate_check_up(mfile0::Mfile &m, mfile0::Mfile &m_all);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-mfile filr-in-mfile-all out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile_sel = argv[1];
	std::string file_in_mfile_all = argv[2];
	std::string file_out_mfile = argv[3];
	
	mfile0::Mfile m,m_all;
	mfile1::read_mfile_extension(file_in_mfile_sel, m);
	mfile1::read_mfile_extension(file_in_mfile_all, m_all);

	mfile0::Mfile pene = penetrate_check_down(m, m_all);
	mfile0::write_mfile(file_out_mfile, pene);
}
mfile0::Mfile penetrate_check_up(mfile0::Mfile &m, mfile0::Mfile &m_all) {
	mfile0::Mfile ret;
	ret.header = m.header;

	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	int pl0, pl1;
	double z0, z1;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {

		pl0 = itr->basetracks.rbegin()->pos / 10;
		pos0.x = itr->basetracks.rbegin()->x;
		pos0.y = itr->basetracks.rbegin()->y;
		pos0.z = itr->basetracks.rbegin()->z;
		dir0.x = mfile0::chain_ax(*itr);
		dir0.y = mfile0::chain_ay(*itr);
		dir0.z = 1;
		for (auto itr2 = m_all.chains.begin(); itr2 != m_all.chains.end(); itr2++) {
			pl1 = itr2->basetracks.begin()->pos / 10;

			if (pl0 > pl1)continue;
			if (pl1 - pl0 > 10)continue;
			pos1.x = itr2->basetracks.begin()->x;
			pos1.y = itr2->basetracks.begin()->y;
			pos1.z = itr2->basetracks.begin()->z;
			dir1.x = mfile0::chain_ax(*itr2);
			dir1.y = mfile0::chain_ay(*itr2);
			dir1.z = 1;
			double oa, md;
			double extra[2], z_range[2];
			z_range[0] = pos0.z;
			z_range[1] = pos1.z;
			oa = matrix_3D::opening_angle(dir0, dir1);
			md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
			//if (oa > 0.4)continue;
			if (md > 100)continue;
			ret.chains.push_back(*itr2);
		}
	}
	return ret;
}
mfile0::Mfile penetrate_check_down(mfile0::Mfile &m, mfile0::Mfile &m_all) {
	mfile0::Mfile ret;
	ret.header = m.header;

	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	int pl0, pl1;
	double z0, z1;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {

		pl0 = itr->basetracks.begin()->pos / 10;
		pos0.x = itr->basetracks.begin()->x;
		pos0.y = itr->basetracks.begin()->y;
		pos0.z = itr->basetracks.begin()->z;
		dir0.x = mfile0::chain_ax(*itr);
		dir0.y = mfile0::chain_ay(*itr);
		dir0.z = 1;
		for (auto itr2 = m_all.chains.begin(); itr2 != m_all.chains.end(); itr2++) {
			pl1 = itr2->basetracks.rbegin()->pos / 10;

			if (pl0 < pl1)continue;
			if (pl0 - pl1 > 5)continue;
			pos1.x = itr2->basetracks.rbegin()->x;
			pos1.y = itr2->basetracks.rbegin()->y;
			pos1.z = itr2->basetracks.rbegin()->z;
			dir1.x = mfile0::chain_ax(*itr2);
			dir1.y = mfile0::chain_ay(*itr2);
			dir1.z = 1;
			double oa, md;
			double extra[2], z_range[2];
			z_range[0] = pos0.z;
			z_range[1] = pos1.z;
			oa = matrix_3D::opening_angle(dir0, dir1);
			md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
			if (oa > 0.4)continue;
			if (md > 100)continue;
			ret.chains.push_back(*itr2);
		}
	}
	return ret;
}
