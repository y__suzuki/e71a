#pragma once
#include <fstream>
#include <iostream>
#include <ios>
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <map>

namespace runcard{
	class file_path {
		std::string Area0;
		std::string basetrack;
		std::string linklet;
		std::string alignment;
		std::string mfile;

	};
}