#include <map>

#include <fstream>
#include <iostream>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <set>
#include <unordered_set>
#include <iterator>

struct id_pair {
	int group_id;
	std::unordered_set<int> rawid0;
	std::unordered_set<int> rawid1;
};
std::vector<id_pair> id_clustering(std::vector<std::pair<int, int >> pair_v,bool output=true);
