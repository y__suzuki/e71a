#pragma once
#include "FILE_structure.hpp"
#include <bitset>

class Chain_base {
public:
	uint64_t chainid,rawid;
	int pl;
};

class PID_track {
public:
	uint64_t chainid;
	short pos, trackingid, sensorid;
	int rawid,col,row,isg;
	double pb, angle, vph, pixelnum,pid;
	
	PID_track();
	~PID_track();
	static void wrtie_pid_track(std::string filename, std::vector<PID_track> &t);
	static void wrtie_pid_track_txt(std::string filename, std::vector<PID_track> &t);
	static void read_pid_track(std::string filename, std::vector<PID_track> &t);

};

template<class T>
void print_4byte(T v) {
	union { T v; int i; } a;
	a.v = v;
	/* ビットの列を表示します */
	for (int i = sizeof(v) * CHAR_BIT - 1; i >= 0; i--) {
		printf("%d", (a.i >> i) & 1);
		//if (i % 4 == 0)printf(" ");
		if (i == 31)printf(" ");
		if (i == 23)printf(" ");
	}
	printf("\n");
}

template<class T>
void print_8byte(T v) {
	union { T v; uint64_t i; } a;
	a.v = v;
	/* ビットの列を表示します */
	for (int i = sizeof(v) * CHAR_BIT - 1; i >= 0; i--) {
		printf("%d", (a.i >> i) & 1);
		if (i % 4 == 0)printf(" ");
	}
	printf("\n");
}
template<class T>
void print_bin(T v) {
	auto type_size = sizeof(v);
	std::cout << "size " << type_size << "byte ";
	if (type_size == 4)print_4byte(v);
	else if(type_size == 8)print_8byte(v);
	else {
		printf("this size not support\n");
	}
}
