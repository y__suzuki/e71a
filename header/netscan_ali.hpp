#pragma once

#include "FILE_structure.hpp"
#include "VxxReader.h"

std::vector<netscan::linklet_t> connect_basetrack(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1, std::vector<corrmap0::Corrmap> &corr);
