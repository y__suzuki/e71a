#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void output(std::string filename, std::vector<Momentum_recon::Mom_chain> &momch);
void output_oa_md(std::string filename, std::vector<Momentum_recon::Mom_chain> &momch);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-momch file-out file-out-md_oa\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_out = argv[2];
	std::string file_out_mdoa = argv[3];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);

	output(file_out, momch);
	output_oa_md(file_out_mdoa, momch);

}
void output(std::string filename, std::vector<Momentum_recon::Mom_chain> &momch) {
	std::ofstream ofs(filename);

	double dz, ex_x, ex_y, angle, dr, dl, dar, dal;
	double  tmp_x, tmp_y;
	for (auto &ch : momch) {
		for (auto itr = ch.base_pair.begin(); itr != ch.base_pair.end(); itr++) {
			ofs << std::fixed << std::right
				<< std::setw(6) << std::setprecision(0) << ch.groupid << " "
				<< std::setw(6) << std::setprecision(0) << ch.chainid << " ";
			dz = itr->second.z - itr->first.z;
			ex_x = itr->first.x + itr->first.ax*dz;
			ex_y = itr->first.y + itr->first.ay*dz;
			angle = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);

			dl = itr->second.x - ex_x;
			dr = itr->second.y - ex_y;
			dal = itr->second.ax - itr->first.ax;
			dar = itr->second.ay - itr->first.ay;

			if (angle > 0.01) {
				tmp_x = dl;
				tmp_y = dr;
				dl = (tmp_x * itr->first.ay - tmp_y * itr->first.ax) / angle;
				dr = (tmp_x * itr->first.ax + tmp_y * itr->first.ay) / angle;
				tmp_x = dal;
				tmp_y = dar;
				dal = (tmp_x * itr->first.ay - tmp_y * itr->first.ax) / angle;
				dar = (tmp_x * itr->first.ax + tmp_y * itr->first.ay) / angle;
			}


			ofs << std::fixed << std::right
				<< std::setw(7) << std::setprecision(1) << dz << " "
				<< std::setw(8) << std::setprecision(5) << dar << " "
				<< std::setw(8) << std::setprecision(5) << dal << " "
				<< std::setw(6) << std::setprecision(1) << dr << " "
				<< std::setw(6) << std::setprecision(1) << dl << std::endl;


		}
	}
}
void output_oa_md(std::string filename, std::vector<Momentum_recon::Mom_chain> &momch) {
	std::ofstream ofs(filename);

	double dz, ex_x, ex_y, angle, dr, dl, dar, dal;
	double  tmp_x, tmp_y;
	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	double oa, md,extra[2],z_range[2];
	for (auto &ch : momch) {
		for (auto itr = ch.base_pair.begin(); itr != ch.base_pair.end(); itr++) {
			ofs << std::fixed << std::right
				<< std::setw(6) << std::setprecision(0) << ch.groupid << " "
				<< std::setw(6) << std::setprecision(0) << ch.chainid << " ";

			pos0.x = itr->first.x;
			pos0.y = itr->first.y;
			pos0.z = itr->first.z;
			dir0.x = itr->first.ax;
			dir0.y = itr->first.ay;
			dir0.z = 1;

			pos1.x = itr->second.x;
			pos1.y = itr->second.y;
			pos1.z = itr->second.z;
			dir1.x = itr->second.ax;
			dir1.y = itr->second.ay;
			dir1.z = 1;

			z_range[0] = pos0.z;
			z_range[1] = pos1.z;

			md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
			oa = matrix_3D::opening_angle(dir0, dir1);



			ofs << std::fixed << std::right
				<< std::setw(4) << std::setprecision(0) << itr->first.pl << " "
				<< std::setw(4) << std::setprecision(0) << itr->second.pl << " "
				<< std::setw(7) << std::setprecision(1) << pos0.z - pos1.z << " "
				<< std::setw(7) << std::setprecision(4) << oa << " "
				<< std::setw(8) << std::setprecision(1) << md << std::endl;


		}
	}
}
