#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "functions.lib")
#include <functions.hpp>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <chrono>
#include <filesystem>
#include <set>
#include <omp.h>
#include <thread>

#define CPU_RATIO 0.1

class output_format_base {
public:
	int pl, rawid, ph0, ph1;
	double ax, ay, x, y, z;
};
class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};
class align_param {
public:
	int id, signal, ix, iy;
	//視野中心
	double x, y, z;
	//parameter(9);
	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;

};
class cut_param {
public:
	//member変数
	std::map<int, double > cut_dx, cut_dax, cut_dy, cut_day, cut_dar, cut_dal, cut_dl, cut_dr;
	int id_max;
	//<slope,intercept>
	std::pair<double, double>ex_dx, ex_dy, ex_dax, ex_day, ex_dar, ex_dr, ex_dal, ex_dl;
	//関数
	int ang_id(double angle) {
		return int(fabs(angle) / 0.1);
	}
	double id_ang(int id) {
		if (id > id_max) {
			return -1;
		}
		return id * 0.1;
	}

	bool judge_connect(vxx::base_track_t &b0, vxx::base_track_t &b1);

	void calc_ex_dx();
	void calc_ex_dax();
	void calc_ex_dy();
	void calc_ex_day();
	void calc_ex_dr();
	void calc_ex_dar();
	void calc_ex_dl();
	void calc_ex_dal();

	double Get_sigma_ax(const double &ax);
	double Get_sigma_ay(const double &ay);
	double Get_sigma_px(const double &ax);
	double Get_sigma_py(const double &ay);
	double Get_sigma_ar(const double &angle);
	double Get_sigma_al(const double &angle);
	double Get_sigma_pr(const double &angle);
	double Get_sigma_pl(const double &angle);

};

class track_difference {
public:
	double ax0, ax1, ay0, ay1, x0, x1, y0, y1, z0, z1;
	double sigma_ax, sigma_ay, sigma_px, sigma_py;
	



};
double nominal_gap(std::string file_in_ECC, int pl[2]);
std::string Set_file_read_bvxx_path(std::string file_ECC_path, int pl, int area);
std::string Set_file_read_ali_path(std::string file_ECC_path, int pl[2], int area);


void Calc_align_param(std::vector<corrmap0::Corrmap> &corr);
std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>> Make_base_corrmap_pair(std::vector<vxx::base_track_t>&base, std::vector<corrmap0::Corrmap> &corr);
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> &corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap*> &corr_map, double hash_size);
void basetrack_trans_affine(std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>>&base_pairs, double nominal_z);

std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> connect_track_all(std::vector<vxx::base_track_t>&base0, std::vector<vxx::base_track_t>&base1);
std::vector<vxx::base_track_t*> connect_track(vxx::base_track_t &t, std::vector<vxx::base_track_t*> &connect_cand);
bool judge_connect_xy(vxx::base_track_t &t1, vxx::base_track_t &t2);
bool judge_connect_rl(vxx::base_track_t &t1, vxx::base_track_t &t2);
void Calc_position_difference(vxx::base_track_t &t1, vxx::base_track_t &t2, double &dr, double &dl);

output_format_link output_format(vxx::base_track_t &t1, vxx::base_track_t &t2);
void output_pair(std::string filename, std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&track_pair);
void output_pair_bin(std::string filename, std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&track_pair);
std::vector<vxx::base_track_t> pickup_base(double center_x, double center_y, double width, double angle_max, std::vector<vxx::base_track_t> &base);
std::pair<double, double> corrmap_center(corrmap0::Corrmap corr);
void basetrack_trans_affine(std::vector<vxx::base_track_t> &base, corrmap0::Corrmap &param, double &gap);
void output_corrmap(std::string filename, std::vector<align_param> &corr);
int get_signal_num(std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&pair);
std::vector<align_param> align_change_format(std::vector<corrmap0::Corrmap> &corr, double nominal_gap);
std::vector< std::pair<corrmap0::Corrmap, std::vector<vxx::base_track_t>>> Make_base_corrmap_pair(std::vector<vxx::base_track_t>&base, std::vector<corrmap0::Corrmap> &corr, double view, double anlge_max);
std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t>base_hash(std::vector < vxx::base_track_t>&base, double hash_pos_size, double hash_ang_size, std::tuple<double, double, double, double> &min);
std::vector<vxx::base_track_t> connect_track(vxx::base_track_t &t, std::vector<vxx::base_track_t> &connect_cand);
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_all(std::vector<vxx::base_track_t>&base1, std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t>&base0, double &hash_pos, double  &hash_ang, std::tuple<double, double, double, double> &min);
int get_signal_num(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&pair);
void output_pair_bin(std::string filename, std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&track_pair);
output_format_link output_format(vxx::base_track_t &t1, vxx::base_track_t &t2);
int use_thread(double ratio, bool output);
void signal_region(std::vector< std::pair<corrmap0::Corrmap, std::vector<vxx::base_track_t>>>&corr, double &center, double &rms);
void align_fine_tune(corrmap0::Corrmap &corr, std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&pair, cut_param &param);
void align_fine_tune_out(corrmap0::Corrmap &corr, std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&pair, cut_param &param);
bool align_fine_tune_with_shift(corrmap0::Corrmap &corr, std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&pair, cut_param &param);

void unique_linklet(std::vector<output_format_link> &link, double &rms_dal, double &rms_dl);
double calc_connection_value(output_format_link &link, double rms_dal, double rms_dl);
void output_link_bin(std::string filename, std::vector<output_format_link>&link);
void select_linklet(std::vector<output_format_link> &link, double &rms_dal, double &rms_dl);
void cacl_cut_x(std::vector<output_format_link>&link, std::map<int, double> &cut_dx, std::map<int, double> &cut_dax);
void cacl_cut_y(std::vector<output_format_link>&link, std::map<int, double> &cut_dy, std::map<int, double> &cut_day);
void cacl_cut_rl(std::vector<output_format_link>&link, std::map<int, double> &cut_dr, std::map<int, double> &cut_dar, std::map<int, double> &cut_dl, std::map<int, double> &cut_dal);
int ax_id(double angle);
int ay_id(double angle);
int ang_id(double angle);
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_sigma(std::vector<vxx::base_track_t>&base1, std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t>&base0, double &hash_pos, double  &hash_ang, std::tuple<double, double, double, double> &min, cut_param &param);
bool judge_connect_sigma(vxx::base_track_t &t1, vxx::base_track_t &t2, cut_param &param);
void Get_param_average(corrmap0::Corrmap&corr, std::map<std::pair<int, int>, corrmap0::Corrmap*> &corr_adjacent_view);
std::vector<corrmap0::Corrmap> cut_corrmap(std::vector<corrmap0::Corrmap>&corr);

bool decide_dz_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich, bool output_flg);
std::pair<double, double> fit_pol2_dz(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini, bool output_flg);
bool decide_dx_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich, bool output_flg);
std::pair<double, double> fit_pol2_dx(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini, bool output_flg);
bool decide_dy_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich, bool output_flg);
std::pair<double, double>fit_pol2_dy(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini, bool output_flg);

bool decide_daz_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich,bool output_flg);
std::pair<double, double> fit_pol2_daz(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini,  bool output_flg);
bool decide_dax_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich, bool output_flg);
std::pair<double, double> fit_pol2_dax(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini, bool output_flg);
bool decide_day_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich, bool output_flg);
std::pair<double, double> fit_pol2_day(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini, bool output_flg);

bool align_fine_tune_all(corrmap0::Corrmap &corr, std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&pair, cut_param &param);


void GaussJorden(double in[3][3], double b[3], double c[3]);

std::vector<vxx::base_track_t> base_angle_cut(std::vector<vxx::base_track_t>&base, double angle_max);
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_unique(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&track_pair, cut_param &param);
std::pair<vxx::base_track_t, vxx::base_track_t> unique_linklet(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&multi, cut_param &param);
std::vector<vxx::base_track_t> base_small_anlge_PH_cut(std::vector<vxx::base_track_t>&base, int *ph_min, int size_ph_cut);
std::string Set_file_read_bvxx_path_sel(std::string file_ECC_path, int pl, int area);

int main(int argc, char**argv) {
	if (argc != 6&&argc!=7) {
		fprintf(stderr, "prg ECC_path pl0 pl1 output-file-old output-file-new (mode)\n");
		fprintf(stderr, "mode=0 : default mode, use bxxx.sel.cor.vxx\n");
		fprintf(stderr, "mode=1 : use bxxx.sel.cor.connetct.vxx\n");
		exit(1);
	}
	int area = 0;
	int pl0 = std::stoi(argv[2]);
	int pl1 = std::stoi(argv[3]);
	int pl[2];
	pl[0] = std::min(pl0, pl1);
	pl[1] = std::max(pl0, pl1);

	int ph_cut[20] = { 12,10,10,10,10,10 ,8,8,8,8,8,8,8,8,8,8,7,7,7,7 };
	int ph_cut_size = sizeof(ph_cut) / sizeof(int);
	printf("PH cut size:%d\n", ph_cut_size);
	std::string file_ECC_path = argv[1];
	std::string file_output_old = argv[4];
	std::string file_output_new = argv[5];
	int mode = 0;
	if (argc == 7) {
		mode = std::stoi(argv[6]);
	}
	double gap = nominal_gap(file_ECC_path, pl);
	//読み込みfile名設定
	std::string file_in_base[2];
	for (int i = 0; i < 2; i++) {
		if (mode == 0) {
			file_in_base[i] = Set_file_read_bvxx_path(file_ECC_path, pl[i], area);
		}
		else if (mode == 1) {
			file_in_base[i] = Set_file_read_bvxx_path_sel(file_ECC_path, pl[i], area);
		}
	}
	std::string file_in_ali = Set_file_read_ali_path(file_ECC_path, pl, area);


	//fileの存在確認・読み込み
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base[2];
	for (int i = 0; i < 2; i++) {
		if (!std::filesystem::exists(file_in_base[i])) {
			fprintf(stderr, "%s not exist\n", file_in_base[i].c_str());
			exit(1);
		}
		base[i] = br.ReadAll(file_in_base[i], pl[i], 0);
		base[i] = base_angle_cut(base[i], 4);
		base[i] = base_small_anlge_PH_cut(base[i], ph_cut, ph_cut_size);
	}
	std::vector<corrmap0::Corrmap> corr;
	if (!std::filesystem::exists(file_in_ali)) {
		fprintf(stderr, "%s not exist\n", file_in_ali.c_str());
		exit(1);
	}
	corrmap0::read_cormap(file_in_ali, corr);
	Calc_align_param(corr);
	//corr = cut_corrmap(corr);
	//signalの分布を得るためにつなぐ
	//mean+-RMS*2までのsignal分布を使う
	//-->mean+-RMS*2までを再計算

	//signaleの範囲内に入るまでfine tune
	//中央から回す?
	std::vector<vxx::base_track_t> base_sel[2];
	double cx, cy;
	double angle_max = 2;
	std::chrono::system_clock::time_point point0, point1, point2, point3, point4, end;

	//カット決める際view(半径)
	std::vector< std::pair<corrmap0::Corrmap, std::vector<vxx::base_track_t>>> corr_base_map = Make_base_corrmap_pair(base[1], corr, 1000,3.5);

	printf("base0 hash");
	double hash_pos = 2000;
	if (fabs(pl1 - pl0) > 3) {
		hash_pos = 4000;
	}
	double hash_ang = 0.25;
	std::tuple<double, double, double, double>min;
	std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t>hash_base = base_hash(base[0], hash_pos, hash_ang, min);
	printf(" --> fin\n");

	std::vector<output_format_link> link;
	int count = 0;
#pragma omp parallel for num_threads(use_thread(CPU_RATIO,false)) schedule(guided)
	for (int i = 0; i < corr_base_map.size(); i++) {
		if (count % 100 == 0) {
			printf("\r connect base %d/%d(%4.1lf%%)", count, corr_base_map.size(), count*100. / corr_base_map.size());
		}
#pragma omp atomic
		count++;
		//corr[i]ごとにfine tune
		//point0 = std::chrono::system_clock::now();
		//if (corr_base_map[i].first.notuse_i[1] != 50 || corr_base_map[i].first.notuse_i[2] != 50)continue;

		std::vector<vxx::base_track_t>base_trans = corr_base_map[i].second;

		//point1 = std::chrono::system_clock::now();

		//変換
		basetrack_trans_affine(base_trans, corr_base_map[i].first, gap);
		//point2 = std::chrono::system_clock::now();

		//hash化&接続
		std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_pair = connect_track_all(base_trans, hash_base, hash_pos, hash_ang, min);
		//point3 = std::chrono::system_clock::now();

		//signal数取得
		//printf("%d\n", get_signal_num(connect_track_pair));

		corr_base_map[i].first.signal = get_signal_num(connect_track_pair);
#pragma omp critical
		for (auto itr = connect_track_pair.begin(); itr != connect_track_pair.end(); itr++) {
			link.push_back(output_format(itr->first, itr->second));
		}
		//if (corr_base_map[i].first.notuse_i[1] != 50 || corr_base_map[i].first.notuse_i[2] != 50)continue;
		//output_pair_bin("test_out0.bin", connect_track_pair);
		//align_fine_tune(corr_base_map[i].first, connect_track_pair);
	}
	printf("\r connect base %d/%d(%4.1lf%%)\n", count, corr_base_map.size(), count*100. / corr_base_map.size());

	//output_link_bin("test_out.bin", link);

	//dalの分布-->rms/dlの分布-->rms
	//linkletのunique化dal^2+dl^2
	double rms_dal, rms_dl;
	unique_linklet(link, rms_dal, rms_dl);
	select_linklet(link, rms_dal, rms_dl);
	//出力して分布見る
	//output_link_bin("test_out.bin", link);
	//dax,day,dx,dy,dal,dar,dl,drのカットの決定
	cut_param sigma;
	sigma.id_max = 30;
	//cacl_cut_x(link, cut_dx, cut_dax);
	//printf("cut x fin\n");
	cacl_cut_y(link, sigma.cut_dy, sigma.cut_day);
	sigma.cut_dax = sigma.cut_day;
	sigma.cut_dx = sigma.cut_dy;
	//printf("cut y fin\n");
	cacl_cut_rl(link, sigma.cut_dr, sigma.cut_dar, sigma.cut_dl, sigma.cut_dal);
	//printf("cut rl fin\n");

	sigma.calc_ex_dax();
	sigma.calc_ex_dx();
	sigma.calc_ex_day();
	sigma.calc_ex_dy();
	sigma.calc_ex_dar();
	sigma.calc_ex_dr();
	sigma.calc_ex_dal();
	sigma.calc_ex_dl();

	//printf("dax %.4lf %.4lf\n", sigma.ex_dax.first, sigma.ex_dax.second);
	//printf("dx %.4lf %.4lf\n", sigma.ex_dx.first, sigma.ex_dx.second);
	//printf("day %.4lf %.4lf\n", sigma.ex_day.first, sigma.ex_day.second);
	//printf("dy %.4lf %.4lf\n", sigma.ex_dy.first, sigma.ex_dy.second);
	//printf("dar %.4lf %.4lf\n", sigma.ex_dar.first, sigma.ex_dar.second);
	//printf("dr %.4lf %.4lf\n", sigma.ex_dr.first, sigma.ex_dr.second);
	//printf("dal %.4lf %.4lf\n", sigma.ex_dal.first, sigma.ex_dal.second);
	//printf("dl %.4lf %.4lf\n", sigma.ex_dl.first, sigma.ex_dl.second);

	//return 0;
	//for (auto itr = sigma.cut_dx.begin(); itr != sigma.cut_dx.end(); itr++) {
	//	printf("%d %.1lf\n", itr->first, itr->second);
	//}
	//for (auto itr = sigma.cut_dax.begin(); itr != sigma.cut_dax.end(); itr++) {
	//	printf("%d %.5lf\n", itr->first, itr->second);
	//}
	//for (auto itr = sigma.cut_dy.begin(); itr != sigma.cut_dy.end(); itr++) {
	//	printf("%d %.1lf\n", itr->first, itr->second);
	//}
	//for (auto itr = sigma.cut_day.begin(); itr != sigma.cut_day.end(); itr++) {
	//	printf("%d %.5lf\n", itr->first, itr->second);
	//}
	//for (auto itr = sigma.cut_dr.begin(); itr != sigma.cut_dr.end(); itr++) {
	//	printf("%d %.1lf\n", itr->first, itr->second);
	//}
	//for (auto itr = sigma.cut_dar.begin(); itr != sigma.cut_dar.end(); itr++) {
	//	printf("%d %.5lf\n", itr->first, itr->second);
	//}
	//for (auto itr = sigma.cut_dl.begin(); itr != sigma.cut_dl.end(); itr++) {
	//	printf("%d %.1lf\n", itr->first, itr->second);
	//}
	//for (auto itr = sigma.cut_dal.begin(); itr != sigma.cut_dal.end(); itr++) {
	//	printf("%d %.5lf\n", itr->first, itr->second);
	//}
	//分布の3sigma
	//正と負で分けない
	//角度1.0-2.0で直線にして2.0以上は外挿?
	count = 0;
	link.clear();
	//signal取得+ fine tune

	//補正行う際のview
	corr_base_map = Make_base_corrmap_pair(base[1], corr, 2000,3);
	
	//角度shrink+gap補正
	printf("1st fine tune\n");
#pragma omp parallel for num_threads(use_thread(CPU_RATIO,false)) schedule(guided)
	for (int i = 0; i < corr_base_map.size(); i++) {
		if (count % 100 == 0) {
			printf("\r connect base %d/%d(%4.1lf%%)", count, corr_base_map.size(), count*100. / corr_base_map.size());
		}
#pragma omp atomic
		count++;
		std::vector<vxx::base_track_t>base_trans = corr_base_map[i].second;
		//変換
		basetrack_trans_affine(base_trans, corr_base_map[i].first, gap);
		//basetrack_trans_affine2(base_trans, corr_base_map[i].first, gap);
		//hash化&接続
		std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_pair = connect_track_sigma(base_trans, hash_base, hash_pos, hash_ang, min, sigma);
		//signal数取得
		corr_base_map[i].first.signal = get_signal_num(connect_track_pair);
		//#pragma omp critical
		//		for (auto itr = connect_track_pair.begin(); itr != connect_track_pair.end(); itr++) {
		//			link.push_back(output_format(itr->first, itr->second));
		//		}
				//if (corr_base_map[i].first.notuse_i[1] != 50 || corr_base_map[i].first.notuse_i[2] != 50)continue;
				//printf("\n-------------------\n");
				//output_pair_bin("test_out0.bin", connect_track_pair);
			//align_fine_tune(corr_base_map[i].first, connect_track_pair, sigma);
//		if (corr_base_map[i].first.notuse_i[1] != 55 || corr_base_map[i].first.notuse_i[2] != 55)continue;
//#pragma omp critical
//		{
		if (!align_fine_tune_all(corr_base_map[i].first, connect_track_pair, sigma)) {
			corr_base_map[i].first.signal = 0;
			corr_base_map[i].first.rms_angle[0] = -1;
			corr_base_map[i].first.rms_angle[1] = -1;
			corr_base_map[i].first.rms_pos[0] = -1;
			corr_base_map[i].first.rms_pos[1] = -1;
		}
		//}
		//output_pair_bin("test_out1.bin", connect_track_pair);
		//printf("\n-------------------\n");

	}
	printf("\r connect base %d/%d(%4.1lf%%)\n", count, corr_base_map.size(), count*100. / corr_base_map.size());

	//出力して分布見る
	//output_link_bin("test_out.bin", link);
	//std::vector<corrmap0::Corrmap> out;
	//for (auto itr = corr_base_map.begin(); itr != corr_base_map.end(); itr++) {
	//	out.push_back(itr->first);
	//}
	//corrmap0::write_corrmap(file_output_old, out);
	//for (auto itr = corr_base_map.begin(); itr != corr_base_map.end(); itr++) {
	//	out.push_back(itr->first);
	//}
	//std::vector<align_param> ali_params = align_change_format(out, gap);

	//output_corrmap(file_output_new, ali_params);

	count = 0;
	link.clear();

	//signal数を得るための接続
#pragma omp parallel for num_threads(use_thread(CPU_RATIO,false)) schedule(guided)
	for (int i = 0; i < corr_base_map.size(); i++) {
		if (count % 100 == 0) {
			printf("\r connect base %d/%d(%4.1lf%%)", count, corr_base_map.size(), count*100. / corr_base_map.size());
		}
#pragma omp atomic
		count++;
		std::vector<vxx::base_track_t>base_trans = corr_base_map[i].second;
		//変換
		basetrack_trans_affine(base_trans, corr_base_map[i].first, gap);
		//basetrack_trans_affine2(base_trans, corr_base_map[i].first, gap);
		//hash化&接続
		std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_pair = connect_track_sigma(base_trans, hash_base, hash_pos, hash_ang, min, sigma);

		//signal数取得
		corr_base_map[i].first.signal = get_signal_num(connect_track_pair);
	}
	printf("\r connect base %d/%d(%4.1lf%%)\n", count, corr_base_map.size(), count*100. / corr_base_map.size());


	//出力して分布見る
	//output_link_bin("test_out.bin", link);

	double signal_center,signal_rms, signal_min, signal_max;
	signal_region(corr_base_map, signal_center, signal_rms);
	signal_min = signal_center - signal_rms * 2;
	signal_max = signal_center + signal_rms * 3;

	//signal が規定本数内のcorrmapに対して探索終了
	std::map<std::pair<int,int>, corrmap0::Corrmap*> good_corrmap;
	for (int i = 0; i < corr_base_map.size(); i++) {
		if (signal_min <= corr_base_map[i].first.signal&&corr_base_map[i].first.signal < signal_max) {
			good_corrmap.insert(std::make_pair(std::make_pair(corr_base_map[i].first.notuse_i[1], corr_base_map[i].first.notuse_i[2]), &(corr_base_map[i].first)));
		}
	}
	printf("all corrmap = %d sucess =%d\n", corr_base_map.size(), good_corrmap.size());

	//signal範囲を広げる
	signal_min = signal_center - signal_rms * 3;
	signal_max = signal_center + signal_rms * 4;

	std::map<std::pair<int, int>, corrmap0::Corrmap*> to_good_corrmap;

	
	//signalが少ないor多い部分は隣接からひっぱってきてもう一回
	for (int ali_loop = 0; ali_loop <= 1; ali_loop++) {
		printf("align iteration %d\n", ali_loop);
		to_good_corrmap.clear();
		count = 0;
#pragma omp parallel for num_threads(use_thread(CPU_RATIO,false)) schedule(dynamic) 
		for (int i = 0; i < corr_base_map.size(); i++) {
			if (count % 100 == 0) {
				printf("\r connect base %d/%d(%4.1lf%%)", count, corr_base_map.size(), count*100. / corr_base_map.size());
			}
			//printf("ix,iy = %d,%d\n", corr_base_map[i].first.notuse_i[1], corr_base_map[i].first.notuse_i[2]);
#pragma omp atomic
			count++;
			std::pair<int, int> corr_id;
			corr_id.first = corr_base_map[i].first.notuse_i[1];
			corr_id.second = corr_base_map[i].first.notuse_i[2];
			if (good_corrmap.count(corr_id) == 1)continue;
			int corr_loop = 1;
			std::map<std::pair<int, int>, corrmap0::Corrmap*> corr_adjacent_view;
			while (corr_adjacent_view.size() < 3) {
				for (int ix = corr_id.first - corr_loop; ix <= corr_id.first + corr_loop; ix++) {
					for (int iy = corr_id.second - corr_loop; iy <= corr_id.second + corr_loop; iy++) {
						auto res = good_corrmap.find(std::make_pair(ix, iy));
						if (res == good_corrmap.end())continue;
						corr_adjacent_view.insert(std::make_pair(res->first, res->second));
					}
				}
				corr_loop++;
			}
			//ぱらめーたの平均をとる
			Get_param_average(corr_base_map[i].first, corr_adjacent_view);

			std::vector<vxx::base_track_t>base_trans = corr_base_map[i].second;
			//変換
			basetrack_trans_affine(base_trans, corr_base_map[i].first, gap);
			//basetrack_trans_affine2(base_trans, corr_base_map[i].first, gap);
			//hash化&接続
			std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_pair = connect_track_sigma(base_trans, hash_base, hash_pos, hash_ang, min, sigma);
			//微調整
			//align_fine_tune(corr_base_map[i].first, connect_track_pair, sigma);


			//失敗したらcontinue
			if (!align_fine_tune_all(corr_base_map[i].first, connect_track_pair, sigma)) {
				corr_base_map[i].first.signal = 0;
				corr_base_map[i].first.rms_angle[0] = -1;
				corr_base_map[i].first.rms_angle[1] = -1;
				corr_base_map[i].first.rms_pos[0] = -1;
				corr_base_map[i].first.rms_pos[1] = -1;
				continue;
			}
			else {
				//成功ならsignal本数確認
				base_trans = corr_base_map[i].second;
				basetrack_trans_affine(base_trans, corr_base_map[i].first, gap);
				//再度接続
				connect_track_pair = connect_track_sigma(base_trans, hash_base, hash_pos, hash_ang, min, sigma);
				//signal数取得
				corr_base_map[i].first.signal = get_signal_num(connect_track_pair);
				//OKなら追加
				if (signal_min < corr_base_map[i].first.signal&&corr_base_map[i].first.signal < signal_max) {
#pragma omp critical
					to_good_corrmap.insert(std::make_pair(std::make_pair(corr_base_map[i].first.notuse_i[1], corr_base_map[i].first.notuse_i[2]), &(corr_base_map[i].first)));
				}
			}
		}
		printf("\r connect base %d/%d(%4.1lf%%)\n", count, corr_base_map.size(), count*100. / corr_base_map.size());
		for (auto itr = to_good_corrmap.begin(); itr != to_good_corrmap.end(); itr++) {
			good_corrmap.insert(*itr);
		}
	}
	

	//内側ならfine tune shiftして入力
	bool loop_flg = true;
	while (loop_flg) {
		loop_flg = false;
		count = 0;
		to_good_corrmap.clear();

#pragma omp parallel for num_threads(use_thread(CPU_RATIO,false)) schedule(dynamic) 
		for (int i = 0; i < corr_base_map.size(); i++) {
			if (count % 100 == 0) {
				printf("\r connect base %d/%d(%4.1lf%%)", count, corr_base_map.size(), count*100. / corr_base_map.size());
			}
#pragma omp atomic
			count++;
			std::pair<int, int> corr_id;
			corr_id.first = corr_base_map[i].first.notuse_i[1];
			corr_id.second = corr_base_map[i].first.notuse_i[2];
			if (good_corrmap.count(corr_id) == 1)continue;

			//周囲何視野見るか
			int corr_loop = 3;
			bool range_flg[4] = { false, false, false, false };
			std::map<std::pair<int, int>, corrmap0::Corrmap*> corr_adjacent_view;
			for (int ix = corr_id.first - corr_loop; ix <= corr_id.first + corr_loop; ix++) {
				for (int iy = corr_id.second - corr_loop; iy <= corr_id.second + corr_loop; iy++) {
					auto res = good_corrmap.find(std::make_pair(ix, iy));
					if (res == good_corrmap.end())continue;
					corr_adjacent_view.insert(std::make_pair(res->first, res->second));
					if (ix < corr_id.first)range_flg[0] = true;
					if (ix > corr_id.first)range_flg[1] = true;
					if (iy < corr_id.second)range_flg[2] = true;
					if (iy > corr_id.second)range_flg[3] = true;
				}
			}
			if (range_flg[0] && range_flg[1] && range_flg[2] && range_flg[3]) {
				//ぱらめーたの平均をとる
				Get_param_average(corr_base_map[i].first, corr_adjacent_view);

				std::vector<vxx::base_track_t>base_trans = corr_base_map[i].second;
				//変換
				basetrack_trans_affine(base_trans, corr_base_map[i].first, gap);
				//basetrack_trans_affine2(base_trans, corr_base_map[i].first, gap);
				//hash化&接続
				std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_pair = connect_track_sigma(base_trans, hash_base, hash_pos, hash_ang, min, sigma);
				//失敗したらcontinue

				if (!align_fine_tune_all(corr_base_map[i].first, connect_track_pair, sigma)) {
					corr_base_map[i].first.signal = 0;
					corr_base_map[i].first.rms_angle[0] = -1;
					corr_base_map[i].first.rms_angle[1] = -1;
					corr_base_map[i].first.rms_pos[0] = -1;
					corr_base_map[i].first.rms_pos[1] = -1;
					continue;
				}
				else {
					//成功ならsignal本数確認
					base_trans = corr_base_map[i].second;
					basetrack_trans_affine(base_trans, corr_base_map[i].first, gap);
					//再度接続
					connect_track_pair = connect_track_sigma(base_trans, hash_base, hash_pos, hash_ang, min, sigma);
					//signal数取得
					corr_base_map[i].first.signal = get_signal_num(connect_track_pair);
					//条件を課さずに追加
					to_good_corrmap.insert(std::make_pair(std::make_pair(corr_base_map[i].first.notuse_i[1], corr_base_map[i].first.notuse_i[2]), &(corr_base_map[i].first)));
					loop_flg = true;
				}
			}
		}
		printf("\r connect base %d/%d(%4.1lf%%)\n", count, corr_base_map.size(), count*100. / corr_base_map.size());
		printf("good corrmap %d + %d --> ", good_corrmap.size(),to_good_corrmap.size());

		for (auto itr = to_good_corrmap.begin(); itr != to_good_corrmap.end(); itr++) {
			good_corrmap.insert(*itr);
		}
		printf(" %d \n", good_corrmap.size());
	}

	std::vector<corrmap0::Corrmap> out;
	for (auto itr = good_corrmap.begin(); itr != good_corrmap.end(); itr++) {
		out.push_back(*(itr->second));
	}
	corrmap0::write_corrmap(file_output_old, out);

	std::vector<align_param> ali_params = align_change_format(out, gap);

	output_corrmap(file_output_new, ali_params);

}
std::vector<vxx::base_track_t> base_angle_cut(std::vector<vxx::base_track_t>&base, double angle_max) {
	std::vector<vxx::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (fabs(itr->ax) > angle_max)continue;
		if (fabs(itr->ay) > angle_max)continue;
		ret.push_back(*itr);
	}
	fprintf(stderr, "basetrack angle cut |ax(ay)|<%.1lf %d --> %d\n", angle_max, base.size(), ret.size());
	return ret;
}
std::vector<vxx::base_track_t> base_small_anlge_PH_cut(std::vector<vxx::base_track_t>&base, int *ph_min, int size_ph_cut) {
	std::vector<vxx::base_track_t> ret;
	double angle;
	int i_ang;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		i_ang = (int)(angle * 10);
		if (i_ang < size_ph_cut) {
			if (int(itr->m[0].ph / 10000) < ph_min[i_ang])continue;
			if (int(itr->m[1].ph / 10000) < ph_min[i_ang])continue;
			ret.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	fprintf(stderr, "basetrack ph cut  %d --> %d\n", base.size(), ret.size());
	return ret;
}
void Get_param_average(corrmap0::Corrmap&corr, std::map<std::pair<int, int>, corrmap0::Corrmap*> &corr_adjacent_view) {
	int count = 0;
	double xy_shrink=0, rot=0, dx = 0, dy = 0, dax = 0, day = 0, angle_shrink=0, dz=0;
	corrmap0::Corrmap* param;
	for (auto itr = corr_adjacent_view.begin(); itr != corr_adjacent_view.end(); itr++) {
		param = itr->second;
		xy_shrink += sqrt(param->position[0] * param->position[3] - param->position[1] * param->position[2]);
		rot += atan(param->angle[2] / param->angle[3]);
		angle_shrink += param->notuse_d[0];
		dx += param->position[4];
		dy += param->position[5];
		dax += param->angle[4];
		dax += param->angle[5];
		dz += param->dz;
		count++;
	}
	xy_shrink = xy_shrink / count;
	rot = rot / count;
	angle_shrink = angle_shrink / count;
	dx = dx / count;
	dy = dy / count;
	dz = dz / count;
	dax = dax / count;
	day = day / count;

	corr.position[0] = cos(rot)*xy_shrink;
	corr.position[1] = -1*sin(rot)*xy_shrink;
	corr.position[2] = sin(rot)*xy_shrink;
	corr.position[3] = cos(rot)*xy_shrink;
	corr.position[4] = dx;
	corr.position[5] = dy;

	corr.angle[0] = cos(rot);
	corr.angle[1] = -1*sin(rot);
	corr.angle[2] = sin(rot);
	corr.angle[3] = cos(rot);
	corr.angle[4] = dax;
	corr.angle[5] = day;

	corr.dz = dz;
	corr.notuse_d[0] = angle_shrink;

}

std::string Set_file_read_bvxx_path(std::string file_ECC_path, int pl, int area) {
	std::stringstream file_in_base;
	file_in_base << file_ECC_path << "\\Area" << area << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	return file_in_base.str();

}
std::string Set_file_read_bvxx_path_sel(std::string file_ECC_path, int pl, int area) {
	std::stringstream file_in_base;
	file_in_base << file_ECC_path << "\\Area" << area << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.connect.vxx";
	return file_in_base.str();

}
std::string Set_file_read_ali_path(std::string file_ECC_path, int pl[2], int area) {
	std::stringstream file_in_ali;
	file_in_ali << file_ECC_path << "\\Area" << area << "\\0\\align\\corrmap-align-"
		<< std::setw(3) << std::setfill('0') << pl[0] << "-"
		<< std::setw(3) << std::setfill('0') << pl[1] << ".lst";
	return file_in_ali.str();

}
double nominal_gap(std::string file_in_ECC, int pl[2]) {

	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	if (z_map.count(pl[0]) + z_map.count(pl[1]) != 2) {
		fprintf(stderr, "exception nominal gap not found\n");
		exit(1);
	}
	//pl[0]のz=0の座標系
	//printf("nominal gap=%.1lf\n", z_map[pl[1]] - z_map[pl[0]]);
	return z_map[pl[1]] - z_map[pl[0]];
}

std::pair<double, double> corrmap_center(corrmap0::Corrmap corr) {

	std::pair<double, double> ret;
	double cx, cy;
		double area[4], factor;
		area[0] = corr.areax[0] - corr.position[4];
		area[1] = corr.areax[1] - corr.position[4];
		area[2] = corr.areay[0] - corr.position[5];
		area[3] = corr.areay[1] - corr.position[5];

		factor = 1 / (corr.position[0] * corr.position[3] - corr.position[1] * corr.position[2]);
		corr.areax[0] = factor * (area[0] * corr.position[3] - area[2] * corr.position[1]);
		corr.areay[0] = factor * (area[2] * corr.position[0] - area[0] * corr.position[2]);
		corr.areax[1] = factor * (area[1] * corr.position[3] - area[3] * corr.position[1]);
		corr.areay[1] = factor * (area[3] * corr.position[0] - area[1] * corr.position[2]);

		cx = (corr.areax[0] + corr.areax[1]) / 2;
		cy = (corr.areay[0] + corr.areay[1]) / 2;
		ret.first = cx;
		ret.second = cy;

		return ret;
}

void Calc_align_param(std::vector<corrmap0::Corrmap> &corr) {
	double x, y;
	double sqrt_det;
	double cos, sin;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		sqrt_det = sqrt(itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);
		cos = itr->position[0] / sqrt_det;
		sin = -1 * itr->position[1] / sqrt_det;

		itr->angle[0] = cos;
		itr->angle[1] = -1 * sin;
		itr->angle[2] = sin;
		itr->angle[3] = cos;

		//角度shrink
		itr->notuse_d[0] = 1;

	}

}

void basetrack_trans_affine(std::vector<vxx::base_track_t> &base, corrmap0::Corrmap &param, double &gap) {
	double x, y;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		x = itr->x;
		y = itr->y;
		itr->x = param.position[0] * x + param.position[1] * y + param.position[4];
		itr->y = param.position[2] * x + param.position[3] * y + param.position[5];
		x = itr->ax;
		y = itr->ay;
		itr->ax = (param.angle[0] * x + param.angle[1] * y)*param.notuse_d[0] + param.angle[4];
		itr->ay = (param.angle[2] * x + param.angle[3] * y)*param.notuse_d[0] + param.angle[5];
		itr->z = gap + param.dz;
	}

}
std::vector<vxx::base_track_t> pickup_base(double center_x, double center_y, double width, double angle_max, std::vector<vxx::base_track_t> &base) {
	std::vector<vxx::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (fabs(itr->ax) > angle_max)continue;
		if (fabs(itr->ay) > angle_max)continue;
		if (pow(center_x - itr->x, 2) + pow(center_y - itr->y, 2) > width*width)continue;
		ret.push_back(*itr);
	}
	return ret;
}


int get_signal_num(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&pair) {
	std::set<int> rawid;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		rawid.insert(itr->second.rawid);
	}
	return rawid.size();
}



std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_all(std::vector<vxx::base_track_t>&base1, std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t>&base0,double &hash_pos,double  &hash_ang,std::tuple<double,double,double,double> &min ) {

	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connected;

	//base1
	double ex_x, ex_y;
	double x_min, x_max, y_min, y_max;
	double ax_min, ax_max, ay_min, ay_max;
	double angle_acc_x, angle_acc_y;
	int i_ax_min, i_ax_max, i_ay_min, i_ay_max;
	int i_x_min, i_x_max, i_y_min, i_y_max;
	std::tuple<int, int, int, int> id;

	std::vector<vxx::base_track_t> connect_cand;
	int count = 0;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		//if (count % 10000 == 0) {
		//	printf("\r connect track %10d/%10d (%4.1lf%%)", count, base1.size(), count*100. / base1.size());
		//}
		count++;

		connect_cand.clear();
		//角度精度
		angle_acc_x = 0.05 + 0.1*fabs(itr->ax);
		angle_acc_y = 0.05 + 0.1*fabs(itr->ay);
		//z=0に外挿
		ex_x = itr->ax*(-1)*itr->z + itr->x;
		ex_y = itr->ay*(-1)*itr->z + itr->y;
		//pos_center+-(angle_acc*gap)*sigma
		x_min = ex_x - angle_acc_x * fabs(itr->z);
		x_max = ex_x + angle_acc_x * fabs(itr->z);
		y_min = ex_y - angle_acc_y * fabs(itr->z);
		y_max = ex_y + angle_acc_y * fabs(itr->z);
		//ang_center+-(angle_acc)*sigma
		ax_min = itr->ax - angle_acc_x;
		ax_max = itr->ax + angle_acc_x;
		ay_min = itr->ay - angle_acc_y;
		ay_max = itr->ay + angle_acc_y;

		//hash idに変換
		i_x_min = (x_min - std::get<0>(min)) / hash_pos;
		i_x_max = (x_max - std::get<0>(min)) / hash_pos;
		i_y_min = (y_min - std::get<1>(min)) / hash_pos;
		i_y_max = (y_max - std::get<1>(min)) / hash_pos;
		i_ax_min = (ax_min - std::get<2>(min)) / hash_ang;
		i_ax_max = (ax_max - std::get<2>(min)) / hash_ang;
		i_ay_min = (ay_min - std::get<3>(min)) / hash_ang;
		i_ay_max = (ay_max - std::get<3>(min)) / hash_ang;

		//hash mapの中から該当trackを探す
		for (int ix = i_x_min; ix <= i_x_max; ix++) {
			for (int iy = i_y_min; iy <= i_y_max; iy++) {
				for (int iax = i_ax_min; iax <= i_ax_max; iax++) {
					for (int iay = i_ay_min; iay <= i_ay_max; iay++) {
						std::get<0>(id) = ix;
						std::get<1>(id) = iy;
						std::get<2>(id) = iax;
						std::get<3>(id) = iay;
						if (base0.count(id) == 0)continue;
						auto range = base0.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							connect_cand.push_back(res->second);
						}
					}
				}
			}
		}
		connect_cand = connect_track(*itr, connect_cand);
		if (connect_cand.size() == 0)continue;
		for (auto itr2 = connect_cand.begin(); itr2 != connect_cand.end(); itr2++) {
			connected.push_back(std::make_pair((*itr2), (*itr)));
		}
	}
	//printf("\r connect track %10d/%10d (%4.1lf%%)\n", count, base1.size(), count*100. / base1.size());

	return connected;

}
std::vector<vxx::base_track_t> connect_track(vxx::base_track_t &t, std::vector<vxx::base_track_t> &connect_cand) {
	std::vector<vxx::base_track_t> ret;
	for (auto itr = connect_cand.begin(); itr != connect_cand.end(); itr++) {
		if (judge_connect_xy(t, (*itr)) && judge_connect_rl(t, (*itr))) {
			ret.push_back(*itr);
		}
	}
	return ret;
}
std::vector<vxx::base_track_t> connect_track(vxx::base_track_t &t, std::vector<vxx::base_track_t> &connect_cand, cut_param &param) {
	std::vector<vxx::base_track_t> ret;
	for (auto itr = connect_cand.begin(); itr != connect_cand.end(); itr++) {
		if (judge_connect_sigma(t, (*itr), param)) {
			ret.push_back(*itr);
		}
	}
	return ret;
}
bool judge_connect_xy(vxx::base_track_t &t1, vxx::base_track_t &t2) {
	double angle, d_pos_x, d_pos_y, d_ang_x, d_ang_y;
	double all_pos_x, all_pos_y, all_ang_x, all_ang_y;

	all_ang_x = 0.05*fabs(t2.ax + t1.ax) / 2 + 0.05;
	all_ang_y = 0.05*fabs(t2.ay + t1.ay) / 2 + 0.05;

	all_pos_x = (0.05*fabs(t2.ax + t1.ax) / 2 + 0.05)*(t1.z - t2.z);
	all_pos_y = (0.05*fabs(t2.ay + t1.ay) / 2 + 0.05)*(t1.z - t2.z);

	d_pos_x = t2.x - t1.x - (t1.ax + t2.ax) / 2 * (t2.z - t1.z);
	d_pos_y = t2.y - t1.y - (t1.ay + t2.ay) / 2 * (t2.z - t1.z);

	d_ang_x = (t2.ax - t1.ax);
	d_ang_y = (t2.ay - t1.ay);

	if (fabs(d_ang_x) > fabs(all_ang_x))return false;
	if (fabs(d_ang_y) > fabs(all_ang_y))return false;

	if (fabs(d_pos_x) > fabs(all_pos_x))return false;
	if (fabs(d_pos_y) > fabs(all_pos_y))return false;

	return true;
}
bool judge_connect_rl(vxx::base_track_t &t1, vxx::base_track_t &t2) {
	double angle, d_pos_r, d_pos_l, d_ang_r, d_ang_l;
	double all_pos_r, all_pos_l, all_ang_r, all_ang_l;
	angle = sqrt(t1.ax*t1.ax + t1.ay*t1.ay);

	all_ang_r = 0.05*angle + 0.05;
	all_ang_l = 0.05;
	all_pos_r = (0.05*angle + 0.05)*(t1.z - t2.z);
	all_pos_l = 0.05*(t1.z - t2.z);


	d_ang_r = ((t2.ax - t1.ax)*t1.ax + (t2.ay - t1.ay)*t1.ay);
	d_ang_l = ((t2.ax - t1.ax)*t1.ay - (t2.ay - t1.ay)*t1.ax);
	if (fabs(d_ang_r) > fabs(all_ang_r)*angle)return false;
	if (fabs(d_ang_l) > fabs(all_ang_l)*angle)return false;

	Calc_position_difference(t1, t2, d_pos_r, d_pos_l);
	if (fabs(d_pos_r) > fabs(all_pos_r))return false;
	if (fabs(d_pos_l) > fabs(all_pos_l))return false;

	return true;
}
bool judge_connect_sigma(vxx::base_track_t &t1, vxx::base_track_t &t2, cut_param &param) {

	//ay_idであってる
	int id_x = ay_id(t1.ax);
	int id_y = ay_id(t1.ay);
	double angle = sqrt(t1.ax*t1.ax + t1.ay*t1.ay);
	int id_ang = ang_id(angle);
	auto param_min = param.cut_dax.find(id_x);
	auto param_max = param.cut_dax.find(id_x + 1);

	//範囲外
	double cut_sigma = 3.5;
	double sigma,x,y;
	//daxのカット
	{
		param_min = param.cut_dax.find(id_x);
		param_max = param.cut_dax.find(id_x + 1);
		x = fabs(t1.ax);
		y = fabs(t1.ax - t2.ax);

		if (param_min == param.cut_dax.end() || param_max == param.cut_dax.end()) {
			sigma = (x*param.ex_dax.first + param.ex_dax.second);
			//printf("not find ix %d ax %.4lf sigma %.4lf ", id_x, t1.ax, sigma);

			sigma = y / sigma;
			//printf("sig %.4lf\n", sigma);

		}
		else {
			sigma= ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
			//printf("find ix %d ax %.4lf sigma %.4lf ", id_x, t1.ax, sigma);

			sigma = y / sigma;
			//printf("sig %.4lf\n", sigma);
		}
		if (sigma > cut_sigma)return false;
	}
	//dayのカット
	{
		param_min = param.cut_day.find(id_y);
		param_max = param.cut_day.find(id_y + 1);
		x = fabs(t1.ay);
		y = fabs(t1.ay - t2.ay);

		if (param_min == param.cut_day.end() || param_max == param.cut_day.end()) {
			sigma = y / (x*param.ex_day.first + param.ex_day.second);
		}
		else {
			sigma = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
		}
		if (sigma > cut_sigma)return false;
	}

	//dxのカット
	{
		param_min = param.cut_dx.find(id_x);
		param_max = param.cut_dx.find(id_x + 1);
		x = fabs(t1.ax);
		y = fabs(t2.x - t1.x - (t1.ax + t2.ax) / 2 * (t2.z - t1.z));

		if (param_min == param.cut_dx.end() || param_max == param.cut_dx.end()) {
			sigma = y / (x*param.ex_dx.first + param.ex_dx.second);
		}
		else {
			sigma = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
		}
		if (sigma > cut_sigma)return false;
	}
	//dyのカット
	{
		param_min = param.cut_dy.find(id_y);
		param_max = param.cut_dy.find(id_y + 1);
		x = fabs(t1.ay);
		y =fabs( t2.y - t1.y - (t1.ay + t2.ay) / 2 * (t2.z - t1.z));

		if (param_min == param.cut_dy.end() || param_max == param.cut_dy.end()) {
			sigma = y / (x*param.ex_dy.first + param.ex_dy.second);
		}
		else {
			sigma = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
		}
		if (sigma > cut_sigma)return false;
	}


	if (angle < 0.1)return true;
	cut_sigma = 5;
	x = angle;
	double  d_pos_r, d_pos_l, d_ang_r, d_ang_l;
	d_ang_r = ((t2.ax - t1.ax)*t1.ax + (t2.ay - t1.ay)*t1.ay)/angle;
	d_ang_l = ((t2.ax - t1.ax)*t1.ay - (t2.ay - t1.ay)*t1.ax) / angle;
	//dalのカット
	{
		param_min = param.cut_dal.find(id_ang);
		param_max = param.cut_dal.find(id_ang + 1);
		y = fabs(d_ang_l);

		if (param_min == param.cut_dal.end() || param_max == param.cut_dal.end()) {
			sigma = y / (x*param.ex_dal.first + param.ex_dal.second);
		}
		else {
			sigma = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
		}
		if (sigma > cut_sigma)return false;
	}
	//darのカット
	{
		param_min = param.cut_dar.find(id_ang);
		param_max = param.cut_dar.find(id_ang + 1);
		y = fabs(d_ang_r);

		if (param_min == param.cut_dar.end() || param_max == param.cut_dar.end()) {
			sigma = y / (x*param.ex_dar.first + param.ex_dar.second);
		}
		else {
			sigma = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
		}
		if (sigma > cut_sigma)return false;
	}


	Calc_position_difference(t1, t2, d_pos_r, d_pos_l);
	//dlのカット
	{
		param_min = param.cut_dl.find(id_ang);
		param_max = param.cut_dl.find(id_ang + 1);
		y = fabs(d_pos_l);

		if (param_min == param.cut_dl.end() || param_max == param.cut_dl.end()) {
			sigma = y / (x*param.ex_dl.first + param.ex_dl.second);
		}
		else {
			sigma = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
		}
		if (sigma > cut_sigma)return false;
	}
	//drのカット
	{
		param_min = param.cut_dr.find(id_ang);
		param_max = param.cut_dr.find(id_ang + 1);
		y = fabs(d_pos_r);

		if (param_min == param.cut_dr.end() || param_max == param.cut_dr.end()) {
			sigma = y / (x*param.ex_dr.first + param.ex_dr.second);
		}
		else {
			sigma = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
		}
		if (sigma > cut_sigma)return false;
	}
	return true;
}
void Calc_position_difference(vxx::base_track_t &t1, vxx::base_track_t &t2, double &dr, double &dl) {
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = t1.x;
	pos0.y = t1.y;
	pos0.z = t1.z;
	dir0.x = t1.ax;
	dir0.y = t1.ay;
	dir0.z = 1;
	pos1.x = t2.x;
	pos1.y = t2.y;
	pos1.z = t2.z;
	dir1.x = t2.ax;
	dir1.y = t2.ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);

}
output_format_link output_format(vxx::base_track_t &t1, vxx::base_track_t &t2) {
	output_format_link l;
	l.b[0].pl = t1.pl;
	l.b[0].rawid = t1.rawid;
	l.b[0].ph0 = t1.m[0].ph;
	l.b[0].ph1 = t1.m[1].ph;
	l.b[0].ax = t1.ax;
	l.b[0].ay = t1.ay;
	l.b[0].x = t1.x;
	l.b[0].y = t1.y;
	l.b[0].z = t1.z;

	l.b[1].pl = t2.pl;
	l.b[1].rawid = t2.rawid;
	l.b[1].ph0 = t2.m[0].ph;
	l.b[1].ph1 = t2.m[1].ph;
	l.b[1].ax = t2.ax;
	l.b[1].ay = t2.ay;
	l.b[1].x = t2.x;
	l.b[1].y = t2.y;
	l.b[1].z = t2.z;

	l.Calc_difference();
	return l;
}
void output_format_link::Calc_difference() {

	dax = b[1].ax - b[0].ax;
	day = b[1].ay - b[0].ay;
	dx = b[1].x - b[0].x - (b[0].ax + b[1].ax) / 2 * (b[1].z - b[0].z);
	dy = b[1].y - b[0].y - (b[0].ay + b[1].ay) / 2 * (b[1].z - b[0].z);
	dar = (dax*b[0].ax + day * b[0].ay) / sqrt(b[0].ax*b[0].ax + b[0].ay*b[0].ay);
	dal = (dax*b[0].ay - day * b[0].ax) / sqrt(b[0].ax*b[0].ax + b[0].ay*b[0].ay);


	//dr,dlの計算
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = b[0].x;
	pos0.y = b[0].y;
	pos0.z = b[0].z;
	dir0.x = b[0].ax;
	dir0.y = b[0].ay;
	dir0.z = 1;
	pos1.x = b[1].x;
	pos1.y = b[1].y;
	pos1.z = b[1].z;
	dir1.x = b[1].ax;
	dir1.y = b[1].ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);


}
std::vector<align_param> align_change_format(std::vector<corrmap0::Corrmap> &corr, double nominal_gap) {
	std::vector<align_param> ret;
	for (int i = 0; i < corr.size(); i++) {
		align_param param;
		param.id = i;
		param.ix = corr[i].notuse_i[1];
		param.iy = corr[i].notuse_i[2];
		param.signal = corr[i].signal;
		param.dx = corr[i].position[4];
		param.dy = corr[i].position[5];
		param.dz = corr[i].dz;
		double factor = corr[i].position[0] * corr[i].position[3] - corr[i].position[1] * corr[i].position[2];
		param.x_shrink = sqrt(factor);
		param.y_shrink = sqrt(factor);
		//param.z_shrink = 1;
		param.z_shrink = corr[i].notuse_d[0];

		param.z_rot = atan((corr[i].position[2] / corr[i].position[0]));

		double area[4];
		area[0] = corr[i].areax[0] - corr[i].position[4];
		area[1] = corr[i].areax[1] - corr[i].position[4];
		area[2] = corr[i].areay[0] - corr[i].position[5];
		area[3] = corr[i].areay[1] - corr[i].position[5];

		corr[i].areax[0] = 1. / factor * (area[0] * corr[i].position[3] - area[2] * corr[i].position[1]);
		corr[i].areay[0] = 1. / factor * (area[2] * corr[i].position[0] - area[0] * corr[i].position[2]);
		corr[i].areax[1] = 1. / factor * (area[1] * corr[i].position[3] - area[3] * corr[i].position[1]);
		corr[i].areay[1] = 1. / factor * (area[3] * corr[i].position[0] - area[1] * corr[i].position[2]);

		param.x = (corr[i].areax[0] + corr[i].areax[1]) / 2;
		param.y = (corr[i].areay[0] + corr[i].areay[1]) / 2;
		param.z = nominal_gap;

		param.x_rot = 0;
		param.y_rot = 0;
		param.yx_shear = 0;
		param.zx_shear = 0;
		param.zy_shear = 0;

		ret.push_back(param);
	}
	return ret;
}





std::vector< std::pair<corrmap0::Corrmap, std::vector<vxx::base_track_t>>> Make_base_corrmap_pair(std::vector<vxx::base_track_t>&base, std::vector<corrmap0::Corrmap> &corr,double view,double anlge_max) {
	std::vector< std::pair<corrmap0::Corrmap, std::vector<vxx::base_track_t>>> ret;

	double hash_size = 4000;
	std::multimap<std::pair<int, int>, vxx::base_track_t> hash_base;
	double xmin, ymin;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			xmin = itr->x;
			ymin = itr->y;
		}
		xmin = std::min(itr->x, xmin);
		ymin = std::min(itr->y, ymin);
	}

	std::pair<int, int>id;

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		id.first = (itr->x - xmin) / hash_size;
		id.second = (itr->y - ymin) / hash_size;
		hash_base.insert(std::make_pair(id, *itr));
	}

	std::pair<double, double> center;
	int ix_min,ix_max,iy_min,iy_max,iy,width = view / hash_size + 1;
	int count = 0;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (count % 100 == 0) {
			printf("\r correspond corrmap-base %d/%d(%4.1lf%%)", count, corr.size(), count*100. / corr.size());

		}
		count++;

		std::vector<vxx::base_track_t> base_cand;

		center = corrmap_center(*itr);
		ix_min = (center.first - xmin - view) / hash_size;
		ix_max = (center.first - xmin + view) / hash_size;
		iy_min = (center.second - ymin - view) / hash_size;
		iy_max = (center.second - ymin + view) / hash_size;
		for (int iix = ix_min; iix <= ix_max; iix++) {
			for (int iiy = iy_min; iiy <= iy_max; iiy++) {
				id.first = iix;
				id.second = iiy;
				if (hash_base.count(id) == 0)continue;
				auto range = hash_base.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					base_cand.push_back(res->second);
				}
			}
		}
		base_cand = pickup_base(center.first, center.second, view, anlge_max, base_cand);
		ret.push_back(std::make_pair(*itr, base_cand));


	}

	printf("\r correspond corrmap-base %d/%d(%4.1lf%%)\n", count, corr.size(), count*100. / corr.size());
	return ret;

}

std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t>base_hash(std::vector < vxx::base_track_t>&base, double hash_pos_size, double hash_ang_size, std::tuple<double, double, double, double> &min) {
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		itr->z = 0;
		if (itr == base.begin()) {
			std::get<0>(min) = itr->x;
			std::get<1>(min) = itr->y;
			std::get<2>(min) = itr->ax;
			std::get<3>(min) = itr->ay;
		}
		if (std::get<0>(min) > itr->x)std::get<0>(min) = itr->x;
		if (std::get<1>(min) > itr->y)std::get<1>(min) = itr->y;
		if (std::get<2>(min) > itr->ax)std::get<2>(min) = itr->ax;
		if (std::get<3>(min) > itr->ay)std::get<3>(min) = itr->ay;

	}

	std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t>base_map;
	std::tuple<int, int, int, int> id;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::get<0>(id) = (itr->x - std::get<0>(min)) / hash_pos_size;
		std::get<1>(id) = (itr->y - std::get<1>(min)) / hash_pos_size;
		std::get<2>(id) = (itr->ax - std::get<2>(min)) / hash_ang_size;
		std::get<3>(id) = (itr->ay - std::get<3>(min)) / hash_ang_size;
		base_map.insert(std::make_pair(id, (*itr)));
	}
	return base_map;
}


void output_pair_bin(std::string filename, std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&track_pair) {
	std::vector<output_format_link> link;
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		link.push_back(output_format((itr->first), (itr->second)));
	}

	std::ofstream ofs(filename, std::ios::binary);
	int count = 0;
	for (auto l : link) {
		if (count % 10000 == 0) {
			printf("\r write linklet %10d/%10d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;
		ofs.write((char*)& l, sizeof(output_format_link));
	}

	printf("\r write linklet %10d/%10d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

}
void output_pair(std::string filename, std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&track_pair) {
	std::vector<output_format_link> link;
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		link.push_back(output_format(*(itr->first), *(itr->second)));
	}

	std::ofstream ofs(filename);
	int count = 0;
	for (auto l : link) {
		if (count % 10000 == 0) {
			printf("\r write linklet %10d/%10d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;

		ofs << std::right << std::fixed;
		for (int i = 0; i < 2; i++) {
			ofs << std::setw(4) << std::setprecision(0) << l.b[i].pl << " "
				<< std::setw(10) << std::setprecision(0) << l.b[i].rawid << " "
				<< std::setw(6) << std::setprecision(0) << l.b[i].ph0 << " "
				<< std::setw(6) << std::setprecision(0) << l.b[i].ph1 << " "
				<< std::setw(7) << std::setprecision(4) << l.b[i].ax << " "
				<< std::setw(7) << std::setprecision(4) << l.b[i].ay << " "
				<< std::setw(8) << std::setprecision(1) << l.b[i].x << " "
				<< std::setw(8) << std::setprecision(1) << l.b[i].y << " "
				<< std::setw(8) << std::setprecision(1) << l.b[i].z << " ";
		}
		ofs << std::setw(7) << std::setprecision(4) << l.dax << " "
			<< std::setw(7) << std::setprecision(4) << l.day << " "
			<< std::setw(6) << std::setprecision(1) << l.dx << " "
			<< std::setw(6) << std::setprecision(1) << l.dy << " "
			<< std::setw(7) << std::setprecision(4) << l.dar << " "
			<< std::setw(7) << std::setprecision(4) << l.dal << " "
			<< std::setw(6) << std::setprecision(1) << l.dr << " "
			<< std::setw(6) << std::setprecision(1) << l.dl << std::endl;
	}
	printf("\r write linklet %10d/%10d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

}
void output_pair_bin(std::string filename, std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&track_pair) {
	std::vector<output_format_link> link;
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		link.push_back(output_format(*(itr->first), *(itr->second)));
	}

	std::ofstream ofs(filename, std::ios::binary);
	int count = 0;
	for (auto l : link) {
		if (count % 10000 == 0) {
			printf("\r write linklet %10d/%10d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;
		ofs.write((char*)& l, sizeof(output_format_link));
	}

	printf("\r write linklet %10d/%10d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

}
void output_link_bin(std::string filename, std::vector<output_format_link>&link) {

	std::ofstream ofs(filename, std::ios::binary);
	int count = 0;
	for (auto l : link) {
		if (count % 10000 == 0) {
			printf("\r write linklet %10d/%10d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;
		ofs.write((char*)& l, sizeof(output_format_link));
	}

	printf("\r write linklet %10d/%10d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

}


void output_corrmap(std::string filename, std::vector<align_param> &corr) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (corr.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)", count, int(corr.size()), count*100. / corr.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(0) << itr->id << " "
				<< std::setw(4) << std::setprecision(0) << itr->ix << " "
				<< std::setw(4) << std::setprecision(0) << itr->iy << " "
				<< std::setw(4) << std::setprecision(0) << itr->signal << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << " "
				<< std::setw(8) << std::setprecision(1) << itr->z << " "
				<< std::setw(9) << std::setprecision(7) << itr->x_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->y_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->z_rot << " "
				<< std::setw(8) << std::setprecision(6) << itr->x_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->y_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->z_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->yx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zy_shear << " "
				<< std::setw(8) << std::setprecision(2) << itr->dx << " "
				<< std::setw(8) << std::setprecision(2) << itr->dy << " "
				<< std::setw(8) << std::setprecision(2) << itr->dz << std::endl;
		}
		fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)\n", count, int(corr.size()), count*100. / corr.size());

	}



}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return std::max(1,(int)(num_all_thread * ratio));
}

void signal_region(std::vector< std::pair<corrmap0::Corrmap, std::vector<vxx::base_track_t>>>&corr, double &center, double &rms) {
	
	int count = 0;
	double sum = 0,sum2=0;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (itr->first.signal == 0)continue;
		count++;
		sum += itr->first.signal;
		sum2 += itr->first.signal*itr->first.signal;
	}
	double mean = sum / count;
	rms = sqrt(sum2 / count - mean * mean);

	printf("%.1lf += %.1lf\n", mean, rms);
	count = 0;
	sum = 0;
	sum2 = 0;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (mean - 2 * rms < itr->first.signal&&itr->first.signal < mean + 2 * rms) {
			count++;
			sum += itr->first.signal;
			sum2 += itr->first.signal*itr->first.signal;
		}
	}

	mean = sum / count;
	rms = sqrt(sum2 / count - mean * mean);
	center = mean;
	printf("%.1lf += %.1lf\n", mean, rms);


}

void align_fine_tune(corrmap0::Corrmap &corr, std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&pair, cut_param &param) {

	std::vector<output_format_link>link;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		link.push_back(output_format(itr->first, itr->second));
	}
	int id_x, id_y;
	double sigma, px;

	//出力
	//output_link_bin("test_out0.bin", link);



	//角度
	double xy[2] = {}, x[2] = {}, y[2] = {}, x2[2] = {}, n[2] = {};
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		//ay_idであってる
		int id_x = ay_id(itr->b[0].ax);
		int id_y = ay_id(itr->b[0].ay);
		auto param_min = param.cut_dax.find(id_x);
		auto param_max = param.cut_dax.find(id_x + 1);

		//daxのsigma
		{
			param_min = param.cut_dax.find(id_x);
			param_max = param.cut_dax.find(id_x + 1);
			px = fabs(itr->b[0].ax);

			if (param_min == param.cut_dax.end() || param_max == param.cut_dax.end()) {
				sigma = (px*param.ex_dax.first + param.ex_dax.second);
			}
			else {
				sigma = ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(px - param_min->first*0.1) + param_min->second);
			}
		}
		sigma = 1 / (sigma * sigma);
		x[0] += itr->b[0].ax*sigma;
		x2[0] += itr->b[0].ax*itr->b[0].ax*sigma;
		xy[0] += itr->b[0].ax*itr->dax*sigma;
		y[0] += itr->dax*sigma;
		n[0] += sigma;


		//dayのsigma
		{
			param_min = param.cut_day.find(id_y);
			param_max = param.cut_day.find(id_y + 1);
			px = fabs(itr->b[0].ay);

			if (param_min == param.cut_day.end() || param_max == param.cut_day.end()) {
				sigma = (px*param.ex_day.first + param.ex_day.second);
			}
			else {
				sigma = ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(px - param_min->first*0.1) + param_min->second);
			}
		}
		sigma = 1 / (sigma * sigma);
		x[1] += itr->b[0].ay*sigma;
		x2[1] += itr->b[0].ay*itr->b[0].ay*sigma;
		xy[1] += itr->b[0].ay*itr->day*sigma;
		y[1] += itr->day*sigma;
		n[1] += sigma;
	}
	double dax, day, dax_slope, day_slope;
	//dax = (n[0] * xy[0] - x[0] * y[0]) / (n[0] * x2[0] - x[0] * x[0]);
	//dax_slope = (x2[0] * y[0] - xy[0] * x[0]) / (n[0] * x2[0] - x[0] * x[0]);
	//day = (n[1] * xy[1] - x[1] * y[1]) / (n[1] * x2[1] - x[1] * x[1]);
	//day_slope = (x2[1] * y[1] - xy[1] * x[1]) / (n[1] * x2[1] - x[1] * x[1]);
	//切片0で固定
	dax = 0;
	day = 0;
	dax_slope = xy[0] / x2[0];
	day_slope = xy[1] / x2[1];
	//printf("dax = %.6lf slope = %.6lf\n", dax, dax_slope);
	//printf("day = %.6lf slope = %.6lf\n", day, day_slope);

	corr.notuse_d[0] = corr.notuse_d[0] / (1 + day_slope);
	corr.notuse_d[1] = corr.notuse_d[1] - dax;
	corr.notuse_d[2] = corr.notuse_d[2] - day;

	//初期化
	for (int i = 0; i < 2; i++) {
		xy[i] = 0;
		x[i] = 0;
		y[i] = 0;
		x2[i] = 0;
		n[i] = 0;
	}

	//位置
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		itr->b[1].ax = itr->b[1].ax * corr.notuse_d[0] - dax;
		itr->b[1].ay = itr->b[1].ay * corr.notuse_d[0] - day;
		itr->Calc_difference();

		//ay_idであってる
		int id_x = ay_id(itr->b[0].ax);
		int id_y = ay_id(itr->b[0].ay);
		auto param_min = param.cut_dx.find(id_x);
		auto param_max = param.cut_dx.find(id_x + 1);

		//dxのsigma
		{
			param_min = param.cut_dx.find(id_x);
			param_max = param.cut_dx.find(id_x + 1);
			px = fabs(itr->dx);

			if (param_min == param.cut_dx.end() || param_max == param.cut_dx.end()) {
				sigma = (px*param.ex_dx.first + param.ex_dx.second);
			}
			else {
				sigma = ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(px - param_min->first*0.1) + param_min->second);
			}
		}
		sigma = 1 / (sigma * sigma);
		x[0] += itr->b[0].ax*sigma;
		x2[0] += itr->b[0].ax*itr->b[0].ax*sigma;
		xy[0] += itr->b[0].ax*itr->dx*sigma;
		y[0] += itr->dx*sigma;
		n[0] += sigma;


		//dyのsigma
		{
			param_min = param.cut_dy.find(id_y);
			param_max = param.cut_dy.find(id_y + 1);
			px = fabs(itr->b[0].ay);

			if (param_min == param.cut_dy.end() || param_max == param.cut_dy.end()) {
				sigma = (px*param.ex_dy.first + param.ex_dy.second);
			}
			else {
				sigma = ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(px - param_min->first*0.1) + param_min->second);
			}
		}
		sigma = 1 / (sigma * sigma);
		x[1] += itr->b[0].ay*sigma;
		x2[1] += itr->b[0].ay*itr->b[0].ay*sigma;
		xy[1] += itr->b[0].ay*itr->dy*sigma;
		y[1] += itr->dy*sigma;
		n[1] += sigma;
	}
	double dx, dy, dx_slope, dy_slope;
	//dx = (n[0] * xy[0] - x[0] * y[0]) / (n[0] * x2[0] - x[0] * x[0]);
	//dx_slope = (x2[0] * y[0] - xy[0] * x[0]) / (n[0] * x2[0] - x[0] * x[0]);
	//dy = (n[1] * xy[1] - x[1] * y[1]) / (n[1] * x2[1] - x[1] * x[1]);
	//dy_slope = (x2[1] * y[1] - xy[1] * x[1]) / (n[1] * x2[1] - x[1] * x[1]);
	dx = 0;
	dy = 0;
	dx_slope = xy[0] / x2[0];
	dy_slope = xy[1] / x2[1];
	//printf("dx = %.1lf slope = %.6lf\n", dx, dx_slope);
	//printf("dy = %.1lf slope = %.6lf\n", dy, dy_slope);

	//角度補正後出力
	//output_link_bin("test_out1.bin", link);

	corr.position[4] = corr.position[4] - dx;
	corr.position[5] = corr.position[5] - dy;
	corr.dz = corr.dz + dy_slope;

	for (auto itr = link.begin(); itr != link.end(); itr++) {
		itr->b[1].x = itr->b[1].x - dx;
		itr->b[1].y = itr->b[1].y - dy;
		itr->b[1].z = itr->b[1].z + dy_slope;
		itr->Calc_difference();
	}
	//位置補正後出力
	//output_link_bin("test_out2.bin", link);

}
/*
bool align_fine_tune_with_shift(corrmap0::Corrmap &corr, std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&pair, cut_param &param) {

	//本数が少ないと無理
	if (pair.size() < 100) {
		return false;
	}

	//test 出力部
	//RMSでzをはかる
	std::vector<track_difference> track_diff;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		track_difference diff;
		diff.ax0 = itr->first.ax;
		diff.ay0 = itr->first.ay;
		diff.x0 = itr->first.x;
		diff.y0 = itr->first.y;
		diff.z0 = itr->first.z;
		diff.ax1 = itr->second.ax;
		diff.ay1 = itr->second.ay;
		diff.x1 = itr->second.x;
		diff.y1 = itr->second.y;
		diff.z1 = itr->second.z;
		//sigmaの逆数が入る
		diff.sigma_ax = 1. / param.Get_sigma_ax(diff.ax0);
		diff.sigma_ay = 1. / param.Get_sigma_ay(diff.ay0);
		diff.sigma_px = 1. / param.Get_sigma_px(diff.ax0);
		diff.sigma_py = 1. / param.Get_sigma_py(diff.ay0);
		track_diff.push_back(diff);
	}

	//std::ofstream ofs("test_out.txt");
	//int count = 0;
	//for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
	//	if (count % 10000 == 0) {
	//		fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)", count, int(track_diff.size()), count*100. / track_diff.size());
	//	}
	//	count++;
	//	ofs << std::right << std::fixed
	//		<< std::setw(8) << std::setprecision(4) << itr->ax0 << " "
	//		<< std::setw(8) << std::setprecision(4) << itr->ay0 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->x0 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->y0 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->z0 << " "
	//		<< std::setw(8) << std::setprecision(4) << itr->ax1 << " "
	//		<< std::setw(8) << std::setprecision(4) << itr->ay1 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->x1 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->y1 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->z1 << " "

	//		<< std::setw(8) << std::setprecision(5) << 1./itr->sigma_ax << " "
	//		<< std::setw(8) << std::setprecision(5) << 1./itr->sigma_ay << " "
	//		<< std::setw(8) << std::setprecision(2) << 1./itr->sigma_px << " "
	//		<< std::setw(8) << std::setprecision(2) << 1./itr->sigma_py << std::endl;
	//}
	//fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)\n", count, int(track_diff.size()), count*100. / track_diff.size());



	double z_ini = track_diff.begin()->z1 - track_diff.begin()->z0;
	double dz_pich = std::max(10., fabs(z_ini*0.01));

	//z_iniを中心に隣の点がdz_pich離れるようにして5点とる。
	//最小がedgeではない場合最小値をとるdzを中心としてdz_pichは半分に
	//dz pichが10um以下になるまでloop
	//5分割10um以下
	decide_dz_center(track_diff, 5, dz_pich, z_ini, 10., false);
	//2次関数でfit
	//z_ini+-dz_pichを20分割して2次関数でfit
	double z_tune = fit_pol2_dz(track_diff, 20, dz_pich * 2, z_ini, false);
	//z_tuneが真のzの値
	double dz_tune = z_tune - (track_diff.begin()->z1 - track_diff.begin()->z0);
	if (fabs(dz_tune) > param.Get_sigma_py(3.) *2. / 3.)return false;
	//z1の値の更新
	for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
		itr->z1 = z_tune;
	}

	//dy_pich-->4,thr --> 2
	double dy_pich = 2, dy_ini = 0;
	decide_dy_center(track_diff, 5, dy_pich, dy_ini, 1., false);
	double dy_tune = fit_pol2_dy(track_diff, 20, dy_pich * 4, dy_ini, false);
	if (fabs(dy_tune) > param.Get_sigma_py(0.001) * 2)return false;

	double dx_pich = 2, dx_ini = 0;
	decide_dx_center(track_diff, 5, dx_pich, dx_ini, 1., false);
	double dx_tune = fit_pol2_dx(track_diff, 20, dx_pich * 4, dx_ini, false);
	if (fabs(dx_tune) > param.Get_sigma_px(0.001) * 2)return false;

	corr.position[4] += dx_tune;
	corr.position[5] += dy_tune;
	corr.dz += dz_tune;

	return true;
	////x1,y1の値の更新
	//for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
	//	itr->x1 += dx_tune;
	//	itr->y1 += dy_tune;
	//}
	////出力
	//std::ofstream ofs("test_out.txt");
	//int count = 0;
	//for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
	//	if (count % 10000 == 0) {
	//		fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)", count, int(track_diff.size()), count*100. / track_diff.size());
	//	}
	//	count++;
	//	ofs << std::right << std::fixed
	//		<< std::setw(8) << std::setprecision(4) << itr->ax0 << " "
	//		<< std::setw(8) << std::setprecision(4) << itr->ay0 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->x0 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->y0 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->z0 << " "
	//		<< std::setw(8) << std::setprecision(4) << itr->ax1 << " "
	//		<< std::setw(8) << std::setprecision(4) << itr->ay1 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->x1 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->y1 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->z1 << " "

	//		<< std::setw(8) << std::setprecision(5) << 1./itr->sigma_ax << " "
	//		<< std::setw(8) << std::setprecision(5) << 1. / itr->sigma_ay << " "
	//		<< std::setw(8) << std::setprecision(2) << 1. / itr->sigma_px << " "
	//		<< std::setw(8) << std::setprecision(2) << 1. / itr->sigma_py << std::endl;
	//}
	//fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)\n", count, int(track_diff.size()), count*100. / track_diff.size());

}
*/
bool align_fine_tune_all(corrmap0::Corrmap &corr, std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&pair, cut_param &param) {

	//本数が少ないと無理
	if (pair.size() < 100) {
		return false;
	}

	//test 出力部
	//RMSでzをはかる
	std::vector<track_difference> track_diff;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		track_difference diff;
		diff.ax0 = itr->first.ax;
		diff.ay0 = itr->first.ay;
		diff.x0 = itr->first.x;
		diff.y0 = itr->first.y;
		diff.z0 = itr->first.z;
		diff.ax1 = itr->second.ax;
		diff.ay1 = itr->second.ay;
		diff.x1 = itr->second.x;
		diff.y1 = itr->second.y;
		diff.z1 = itr->second.z;
		//sigmaの逆数が入る
		diff.sigma_ax = 1. / param.Get_sigma_ax(diff.ax0);
		diff.sigma_ay = 1. / param.Get_sigma_ay(diff.ay0);
		diff.sigma_px = 1. / param.Get_sigma_px(diff.ax0);
		diff.sigma_py = 1. / param.Get_sigma_py(diff.ay0);
		if (!isfinite(diff.ax0))continue;
		if (!isfinite(diff.ay0))continue;
		if (!isfinite(diff.x0))continue;
		if (!isfinite(diff.y0))continue;
		if (!isfinite(diff.z0))continue;
		if (!isfinite(diff.ax1))continue;
		if (!isfinite(diff.ay1))continue;
		if (!isfinite(diff.x1))continue;
		if (!isfinite(diff.y1))continue;
		if (!isfinite(diff.z1))continue;

		track_diff.push_back(diff);
	}
	//本数が少ないと無理
	if (track_diff.size() < 100) {
		return false;
	}

	/*

	std::ofstream ofs("test_out.txt");
	int count = 0;
	for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)", count, int(track_diff.size()), count*100. / track_diff.size());
		}
		count++;
		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(4) << itr->ax0 << " "
			<< std::setw(8) << std::setprecision(4) << itr->ay0 << " "
			<< std::setw(8) << std::setprecision(1) << itr->x0 << " "
			<< std::setw(8) << std::setprecision(1) << itr->y0 << " "
			<< std::setw(8) << std::setprecision(1) << itr->z0 << " "
			<< std::setw(8) << std::setprecision(4) << itr->ax1 << " "
			<< std::setw(8) << std::setprecision(4) << itr->ay1 << " "
			<< std::setw(8) << std::setprecision(1) << itr->x1 << " "
			<< std::setw(8) << std::setprecision(1) << itr->y1 << " "
			<< std::setw(8) << std::setprecision(1) << itr->z1 << " "

			<< std::setw(8) << std::setprecision(5) << 1./itr->sigma_ax << " "
			<< std::setw(8) << std::setprecision(5) << 1./itr->sigma_ay << " "
			<< std::setw(8) << std::setprecision(2) << 1./itr->sigma_px << " "
			<< std::setw(8) << std::setprecision(2) << 1./itr->sigma_py << std::endl;
	}
	fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)\n", count, int(track_diff.size()), count*100. / track_diff.size());
	*/

	//各種初期parameterの設定
	double daz_ini = 1;
	double daz_pich = 0.01;
	double daz_pich_min = 0.002;

	double dax_ini = 0;
	double dax_pich = 0.01;
	double dax_pich_min = 0.002;

	double day_ini = 0;
	double day_pich = 0.01;
	double day_pich_min = 0.002;

	double dz_ini = 0;
	double dz_pich_min = 3;
	//もとのgapの何%か
	double dz_pich = std::max(dz_pich_min, fabs(track_diff.begin()->z1 - track_diff.begin()->z0)*0.01/4);

	double dx_ini = 0;
	double dx_pich_min = 1;
	double dx_pich = 2;

	double dy_ini = 0;
	double dy_pich_min = 1;
	double dy_pich = 2;
	if (fabs(track_diff.begin()->z1 - track_diff.begin()->z0) > 3000) {
		dx_pich_min = 5;
		dx_pich = 10;
		dy_pich_min = 5;
		dy_pich = 10;
	}

	if (!decide_daz_center(track_diff, 5, daz_pich, daz_ini, daz_pich_min, false)) {
		printf("daz center not found\n");
		printf("ix = %d , iy = %d\n", corr.notuse_i[1], corr.notuse_i[2]);
		return false;
	}
	std::pair<double,double>shrinkz_tune = fit_pol2_daz(track_diff, 20, daz_pich * 2, daz_ini, false);
	//値の更新
	corr.notuse_d[0] = corr.notuse_d[0] * shrinkz_tune.first;
	for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
		itr->ax1 = itr->ax1*shrinkz_tune.first;
		itr->ay1 = itr->ay1*shrinkz_tune.first;
	}

	if (!decide_dax_center(track_diff, 5, dax_pich, dax_ini, dax_pich_min,false)) {
		printf("dax center not found\n");
		printf("ix = %d , iy = %d\n", corr.notuse_i[1], corr.notuse_i[2]);
		return false;
	}
	std::pair<double, double> dax_tune = fit_pol2_dax(track_diff, 20, dax_pich * 2, dax_ini, false);
	if (!decide_day_center(track_diff, 5, day_pich, day_ini, day_pich_min, false)) {
		printf("day center not found\n");
		printf("ix = %d , iy = %d\n", corr.notuse_i[1], corr.notuse_i[2]);
		return false;
	}
	std::pair<double, double> day_tune = fit_pol2_day(track_diff, 20, day_pich * 2, day_ini, false);

	//値の更新
	corr.angle[4] = corr.angle[4] + dax_tune.first;
	corr.angle[5] = corr.angle[5] + day_tune.first;
	corr.rms_angle[0] = dax_tune.second;
	corr.rms_angle[1] = day_tune.second;
	for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
		itr->ax1 = itr->ax1 + dax_tune.first;
		itr->ay1 = itr->ay1 + day_tune.first;
	}

	if (!decide_dz_center(track_diff, 5, dz_pich, dz_ini, dz_pich_min,false)) {
		printf("dz center not found\n");
		printf("ix = %d , iy = %d\n", corr.notuse_i[1], corr.notuse_i[2]);
		return false;
	}
	std::pair<double, double> dz_tune = fit_pol2_dz(track_diff, 20, dz_pich * 2, dz_ini,false);

	//値の更新
	corr.dz = corr.dz + dz_tune.first;
	for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
		itr->z1 = itr->z1 + dz_tune.first;
	}

	if (!decide_dy_center(track_diff, 5, dy_pich, dy_ini, dy_pich_min, false)) {
		printf("dy center not found\n");
		printf("ix = %d , iy = %d\n", corr.notuse_i[1], corr.notuse_i[2]);
		return false;
	}
	std::pair<double, double> dy_tune = fit_pol2_dy(track_diff, 20, dy_pich * 4, dy_ini, false);
	if (fabs(dy_tune.first) > param.Get_sigma_py(0.001) * 2)return false;


	if (!decide_dx_center(track_diff, 5, dx_pich, dx_ini, dx_pich_min, false)) {
		printf("dx center not found\n");
		printf("ix = %d , iy = %d\n", corr.notuse_i[1], corr.notuse_i[2]);
		return false;
	}
	std::pair<double, double> dx_tune = fit_pol2_dx(track_diff, 20, dx_pich * 4, dx_ini, false);
	if (fabs(dx_tune.first) > param.Get_sigma_px(0.001) * 2)return false;

	//値の更新
	corr.position[4] = corr.position[4] + dx_tune.first;
	corr.position[5] = corr.position[5] + dy_tune.first;
	corr.rms_pos[0] = dx_tune.second;
	corr.rms_pos[1] = dy_tune.second;

	for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
		itr->x1 = itr->x1 + dx_tune.first;
		itr->y1 = itr->y1 + dy_tune.first;
	}

	return true;

	////x1,y1の値の更新
	//for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
	//	itr->x1 += dx_tune;
	//	itr->y1 += dy_tune;
	//}
	////出力
	//std::ofstream ofs("test_out.txt");
	//int count = 0;
	//for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
	//	if (count % 10000 == 0) {
	//		fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)", count, int(track_diff.size()), count*100. / track_diff.size());
	//	}
	//	count++;
	//	ofs << std::right << std::fixed
	//		<< std::setw(8) << std::setprecision(4) << itr->ax0 << " "
	//		<< std::setw(8) << std::setprecision(4) << itr->ay0 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->x0 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->y0 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->z0 << " "
	//		<< std::setw(8) << std::setprecision(4) << itr->ax1 << " "
	//		<< std::setw(8) << std::setprecision(4) << itr->ay1 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->x1 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->y1 << " "
	//		<< std::setw(8) << std::setprecision(1) << itr->z1 << " "

	//		<< std::setw(8) << std::setprecision(5) << 1./itr->sigma_ax << " "
	//		<< std::setw(8) << std::setprecision(5) << 1. / itr->sigma_ay << " "
	//		<< std::setw(8) << std::setprecision(2) << 1. / itr->sigma_px << " "
	//		<< std::setw(8) << std::setprecision(2) << 1. / itr->sigma_py << std::endl;
	//}
	//fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)\n", count, int(track_diff.size()), count*100. / track_diff.size());

}

//位置
//シフト
bool decide_dz_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich, bool output_flg) {
	double start_z = val_ini;

	double *dval_sum = new double[Divide_n];
	double *dval_sum2 = new double[Divide_n];
	double *dval = new double[Divide_n];
	double dval_tmp;
	int loop_num = 0, count;
	while (true) {
		for (int i = 0; i < Divide_n; i++) {
			dval[i] = (i - Divide_n / 2) * pich + val_ini;
			dval_sum[i] = 0;
			dval_sum2[i] = 0;
			count = 0;
			for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
				dval_tmp = (itr->y1 - itr->y0 - (itr->ay0 + itr->ay1)*0.5*(itr->z1 + dval[i] - itr->z0)) * itr->sigma_py;
				dval_sum[i] += dval_tmp;
				dval_sum2[i] += dval_tmp * dval_tmp;
				count++;
			}
			dval_sum2[i] = dval_sum2[i] / count - dval_sum[i] * dval_sum[i] / count / count;
		}
		double min_dx = dval_sum2[0];
		int min_i = 0;
		for (int i = 0; i < Divide_n; i++) {
			if (min_dx > dval_sum2[i]) {
				min_dx = dval_sum2[i];
				min_i = i;
			}
		}

		if (output_flg) {
			for (int i = 0; i < Divide_n; i++) {
				printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			}
			printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
		}
		val_ini = dval[min_i];

		if (min_i != 0 && min_i != Divide_n - 1) {
			loop_num = 0;
			//pol2の軸
			val_ini = -1 / 2. * (dval_sum2[min_i + 1] - dval_sum2[min_i - 1]) / (dval_sum2[min_i + 1] + dval_sum2[min_i - 1] - 2 * dval_sum2[min_i]) * pich + dval[min_i];
			//printf("dy_ini %.2lf pich %.2lf\n", dy_ini, dy_pich);
			//終了条件
			if (pich < min_pich + 0.000001)break;
			pich = std::max(pich / 2, min_pich);
		}
		loop_num++;
		//変なところに捕まったら・
		if (loop_num > 10) {
			printf("dz count %d\n", count);
			for (int i = 0; i < Divide_n; i++) {
				printf("%g %g : %g %g\n", dval[i], sqrt(dval_sum2[i]), dval_sum[i], dval_sum2[i]);
			}
			printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
			if (loop_num > 15) {
				fprintf(stderr, "dz center not found\n");
				//忘れるとメモリリーク
				delete[] dval;
				delete[] dval_sum;
				delete[] dval_sum2;
				return false;
			}
		}
	}

	double end_z = val_ini;
	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_sum;
	delete[] dval_sum2;

	if (fabs(start_z - end_z) > 100)return false;
	return true;
}
std::pair<double, double> fit_pol2_dz(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini, bool output_flg) {
	//double *dy_sum = new double[Divide_n];
	//double *dy_sum2 = new double[Divide_n];

	double *dval = new double[Divide_n];
	double *dval_rms = new double[Divide_n];
	double dval_tmp, dval_sum, dval_sum2;

	int count = 0;
	double pich = val_all_width / Divide_n;

	//各dzに対するdyRMSを求める
	for (int i = 0; i < Divide_n; i++) {
		dval[i] = val_ini + (i - Divide_n / 2)* pich;
		dval_sum = 0;
		dval_sum2 = 0;
		count = 0;
		for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
			dval_tmp = (itr->y1 - itr->y0 - (itr->ay0 + itr->ay1)*0.5*(itr->z1 + dval[i] - itr->z0)) * itr->sigma_py;
			dval_sum += dval_tmp;
			dval_sum2 += dval_tmp * dval_tmp;
			count++;
		}
		dval_rms[i] = sqrt(dval_sum2 / count - dval_sum * dval_sum / (count *count));
	}

	//各点を最小二乗法でFIT
	double x4, x3, x2, x1, x0, x2y, xy, y;
	x4 = 0;
	x3 = 0;
	x2 = 0;
	x1 = 0;
	x0 = 0;
	x2y = 0;
	xy = 0;
	y = 0;

	double shift_x = val_ini, shift_y = dval_rms[Divide_n / 2];
	double shrink_x = val_all_width / (2 * 10.), shrink_y = fabs(dval_rms[0] - shift_y) / 3;
	for (int i = 0; i < Divide_n; i++) {
		if (output_flg) {
			printf("%g %g\n", dval[i], dval_rms[i]);
		}

		dval[i] = (dval[i] - shift_x) / shrink_x;
		dval_rms[i] = (dval_rms[i] - shift_y) / shrink_y;

	}
	//printf("\n");
	//printf("x shift %g shrink%g\n", shift_x, shrink_x);
	//printf("y shift %g shrink%g\n", shift_y, shrink_y);

	//for (int i = 0; i < Divide_n; i++) {
	//	printf("%.3lf %.7lf\n", dy[i], dy_rms[i]);
	//}

	for (int i = 0; i < Divide_n; i++) {
		x4 += dval[i] * dval[i] * dval[i] * dval[i];
		x3 += dval[i] * dval[i] * dval[i];
		x2 += dval[i] * dval[i];
		x1 += dval[i];
		x0 += 1;

		x2y += dval[i] * dval[i] * dval_rms[i];
		xy += dval[i] * dval_rms[i];
		y += dval_rms[i];
		//printf("%.2lf %.5lf\n", dz[i], dy_rms[i]);
	}


	//gaus
	double in[3][3] = { {x4,x3,x2},{x3,x2,x1},{x2,x1,x0} }, vec[3] = { x2y,xy,y }, out[3] = {};
	GaussJorden(in, vec, out);

	//printf("a = %g\n", out[0]);
	//printf("b = %g\n", out[1]);
	//printf("c = %g\n", out[2]);

	//printf("axis = %g\n", -out[1] / (2 * out[0]));


	std::pair<double, double> ret;
	ret.first = -out[1] / (2 * out[0])*shrink_x + shift_x;
	ret.second = -1 / 4 * (out[1] * out[1] - 4 * out[0] * out[2]) / out[0] * shrink_y + shift_y;
	if (output_flg) {
		printf("result (%g, %g)\n", ret.first, ret.second);
	}
	if (!isfinite(ret.first) || ret.first< val_ini - (Divide_n / 2)* pich || ret.first>val_ini + (Divide_n - 1 + Divide_n / 2)* pich) {
		ret.first = val_ini;
	}

	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_rms;

	return ret;
}


bool decide_dx_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich, bool output_flg) {
	double start_val = val_ini;

	double *dval_sum = new double[Divide_n];
	double *dval_sum2 = new double[Divide_n];
	double *dval = new double[Divide_n];
	double dval_tmp;
	int loop_num = 0, count;
	while (true) {
		for (int i = 0; i < Divide_n; i++) {
			dval[i] = (i - Divide_n / 2) * pich + val_ini;
			dval_sum[i] = 0;
			dval_sum2[i] = 0;
			count = 0;
			for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
				dval_tmp = (itr->x1+dval[i] - itr->x0 - (itr->ax0 + itr->ax1)*0.5*(itr->z1 - itr->z0)) * itr->sigma_px;
				dval_sum[i] += dval_tmp;
				dval_sum2[i] += dval_tmp * dval_tmp;
				count++;
			}
			dval_sum2[i] = dval_sum2[i] / count - dval_sum[i] * dval_sum[i] / count / count;
		}
		double min_dx = dval_sum2[0];
		int min_i = 0;
		for (int i = 0; i < Divide_n; i++) {
			if (min_dx > dval_sum2[i]) {
				min_dx = dval_sum2[i];
				min_i = i;
			}
		}

		if (output_flg) {
			for (int i = 0; i < Divide_n; i++) {
				printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			}
			printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
		}
		val_ini = dval[min_i];

		if (min_i != 0 && min_i != Divide_n - 1) {
			loop_num = 0;
			//pol2の軸
			val_ini = -1 / 2. * (dval_sum2[min_i + 1] - dval_sum2[min_i - 1]) / (dval_sum2[min_i + 1] + dval_sum2[min_i - 1] - 2 * dval_sum2[min_i]) * pich + dval[min_i];
			//printf("dy_ini %.2lf pich %.2lf\n", dy_ini, dy_pich);
			//終了条件
			if (pich < min_pich + 0.000001)break;
			pich = std::max(pich / 2, min_pich);
		}
		loop_num++;
		//変なところに捕まったら・
		if (loop_num > 10) {
			//for (int i = 0; i < Divide_n; i++) {
			//	printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			//}
			//printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
			if (loop_num > 15) {
				fprintf(stderr, "dx center not found\n");
				//忘れるとメモリリーク
				delete[] dval;
				delete[] dval_sum;
				delete[] dval_sum2;
				return false;
			}
		}
	}
	double end_val = val_ini;
	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_sum;
	delete[] dval_sum2;
	if (fabs(start_val - end_val) > 100)return false;

	return true;
}
std::pair<double, double> fit_pol2_dx(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini, bool output_flg) {
	//double *dy_sum = new double[Divide_n];
	//double *dy_sum2 = new double[Divide_n];

	double *dval = new double[Divide_n];
	double *dval_rms = new double[Divide_n];
	double dval_tmp, dval_sum, dval_sum2;

	int count = 0;
	double pich = val_all_width / Divide_n;

	//各dzに対するdyRMSを求める
	for (int i = 0; i < Divide_n; i++) {
		dval[i] = val_ini + (i - Divide_n / 2)* pich;
		dval_sum = 0;
		dval_sum2 = 0;
		count = 0;
		for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
			dval_tmp = (itr->x1 + dval[i] - itr->x0 - (itr->ax0 + itr->ax1)*0.5*(itr->z1 - itr->z0)) * itr->sigma_px;
			dval_sum += dval_tmp;
			dval_sum2 += dval_tmp * dval_tmp;
			count++;
		}
		dval_rms[i] = sqrt(dval_sum2 / count - dval_sum * dval_sum / (count *count));
	}

	//各点を最小二乗法でFIT
	double x4, x3, x2, x1, x0, x2y, xy, y;
	x4 = 0;
	x3 = 0;
	x2 = 0;
	x1 = 0;
	x0 = 0;
	x2y = 0;
	xy = 0;
	y = 0;

	double shift_x = val_ini, shift_y = dval_rms[Divide_n / 2];
	double shrink_x = val_all_width / (2 * 10.), shrink_y = fabs(dval_rms[0] - shift_y) / 3;
	for (int i = 0; i < Divide_n; i++) {
		if (output_flg) {
			printf("%g %g\n", dval[i], dval_rms[i]);
		}

		dval[i] = (dval[i] - shift_x) / shrink_x;
		dval_rms[i] = (dval_rms[i] - shift_y) / shrink_y;

	}
	//printf("\n");
	//printf("x shift %g shrink%g\n", shift_x, shrink_x);
	//printf("y shift %g shrink%g\n", shift_y, shrink_y);

	//for (int i = 0; i < Divide_n; i++) {
	//	printf("%.3lf %.7lf\n", dy[i], dy_rms[i]);
	//}

	for (int i = 0; i < Divide_n; i++) {
		x4 += dval[i] * dval[i] * dval[i] * dval[i];
		x3 += dval[i] * dval[i] * dval[i];
		x2 += dval[i] * dval[i];
		x1 += dval[i];
		x0 += 1;

		x2y += dval[i] * dval[i] * dval_rms[i];
		xy += dval[i] * dval_rms[i];
		y += dval_rms[i];
		//printf("%.2lf %.5lf\n", dz[i], dy_rms[i]);
	}


	//gaus
	double in[3][3] = { {x4,x3,x2},{x3,x2,x1},{x2,x1,x0} }, vec[3] = { x2y,xy,y }, out[3] = {};
	GaussJorden(in, vec, out);

	//printf("a = %g\n", out[0]);
	//printf("b = %g\n", out[1]);
	//printf("c = %g\n", out[2]);

	//printf("axis = %g\n", -out[1] / (2 * out[0]));

	std::pair<double, double> ret;
	ret.first = -out[1] / (2 * out[0])*shrink_x + shift_x;
	ret.second = -1 / 4 * (out[1] * out[1] - 4 * out[0] * out[2]) / out[0] * shrink_y + shift_y;
	if (output_flg) {
		printf("result (%g, %g)\n", ret.first, ret.second);
	}
	if (!isfinite(ret.first) || ret.first< val_ini - (Divide_n / 2)* pich || ret.first>val_ini + (Divide_n - 1 + Divide_n / 2)* pich) {
		ret.first = val_ini;
	}



	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_rms;

	return ret;
}

bool decide_dy_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich, bool output_flg) {
	double start_val = val_ini;

	double *dval_sum = new double[Divide_n];
	double *dval_sum2 = new double[Divide_n];
	double *dval = new double[Divide_n];
	double dval_tmp;
	int loop_num = 0, count;
	while (true) {
		for (int i = 0; i < Divide_n; i++) {
			dval[i] = (i - Divide_n / 2) * pich + val_ini;
			dval_sum[i] = 0;
			dval_sum2[i] = 0;
			count = 0;
			for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
				dval_tmp = (itr->y1 +dval[i]- itr->y0 - (itr->ay0 + itr->ay1)*0.5*(itr->z1 - itr->z0)) * itr->sigma_py;
				dval_sum[i] += dval_tmp;
				dval_sum2[i] += dval_tmp * dval_tmp;
				count++;
			}
			dval_sum2[i] = dval_sum2[i] / count - dval_sum[i] * dval_sum[i] / count / count;
		}
		double min_dx = dval_sum2[0];
		int min_i = 0;
		for (int i = 0; i < Divide_n; i++) {
			if (min_dx > dval_sum2[i]) {
				min_dx = dval_sum2[i];
				min_i = i;
			}
		}

		if (output_flg) {
			for (int i = 0; i < Divide_n; i++) {
				printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			}
			printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
		}
		val_ini = dval[min_i];

		if (min_i != 0 && min_i != Divide_n - 1) {
			loop_num = 0;
			//pol2の軸
			val_ini = -1 / 2. * (dval_sum2[min_i + 1] - dval_sum2[min_i - 1]) / (dval_sum2[min_i + 1] + dval_sum2[min_i - 1] - 2 * dval_sum2[min_i]) * pich + dval[min_i];
			//printf("dy_ini %.2lf pich %.2lf\n", dy_ini, dy_pich);
			//終了条件
			if (pich < min_pich + 0.000001)break;
			pich = std::max(pich / 2, min_pich);
		}
		loop_num++;
		//変なところに捕まったら・
		if (loop_num > 10) {
			//for (int i = 0; i < Divide_n; i++) {
			//	printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			//}
			//printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
			if (loop_num > 15) {
				fprintf(stderr, "dy center not found\n");
				//忘れるとメモリリーク
				delete[] dval;
				delete[] dval_sum;
				delete[] dval_sum2;
				return false;
			}
		}
	}
	double end_val = val_ini;
	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_sum;
	delete[] dval_sum2;

	if (fabs(start_val - end_val) > 100)return false;

	return true;
}
std::pair<double, double> fit_pol2_dy(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini, bool output_flg) {
	//double *dy_sum = new double[Divide_n];
	//double *dy_sum2 = new double[Divide_n];

	double *dval = new double[Divide_n];
	double *dval_rms = new double[Divide_n];
	double dval_tmp, dval_sum, dval_sum2;

	int count = 0;
	double pich = val_all_width / Divide_n;

	//各dzに対するdyRMSを求める
	for (int i = 0; i < Divide_n; i++) {
		dval[i] = val_ini + (i - Divide_n / 2)* pich;
		dval_sum = 0;
		dval_sum2 = 0;
		count = 0;
		for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
			dval_tmp = (itr->y1 + dval[i] - itr->y0 - (itr->ay0 + itr->ay1)*0.5*(itr->z1 - itr->z0)) * itr->sigma_py;
			dval_sum += dval_tmp;
			dval_sum2 += dval_tmp * dval_tmp;
			count++;
		}
		dval_rms[i] = sqrt(dval_sum2 / count - dval_sum * dval_sum / (count *count));
	}

	//各点を最小二乗法でFIT
	double x4, x3, x2, x1, x0, x2y, xy, y;
	x4 = 0;
	x3 = 0;
	x2 = 0;
	x1 = 0;
	x0 = 0;
	x2y = 0;
	xy = 0;
	y = 0;

	double shift_x = val_ini, shift_y = dval_rms[Divide_n / 2];
	double shrink_x = val_all_width / (2 * 10.), shrink_y = fabs(dval_rms[0] - shift_y) / 3;
	for (int i = 0; i < Divide_n; i++) {
		if (output_flg) {
			printf("%g %g\n", dval[i], dval_rms[i]);
		}

		dval[i] = (dval[i] - shift_x) / shrink_x;
		dval_rms[i] = (dval_rms[i] - shift_y) / shrink_y;

	}
	//printf("\n");
	//printf("x shift %g shrink%g\n", shift_x, shrink_x);
	//printf("y shift %g shrink%g\n", shift_y, shrink_y);

	//for (int i = 0; i < Divide_n; i++) {
	//	printf("%.3lf %.7lf\n", dy[i], dy_rms[i]);
	//}

	for (int i = 0; i < Divide_n; i++) {
		x4 += dval[i] * dval[i] * dval[i] * dval[i];
		x3 += dval[i] * dval[i] * dval[i];
		x2 += dval[i] * dval[i];
		x1 += dval[i];
		x0 += 1;

		x2y += dval[i] * dval[i] * dval_rms[i];
		xy += dval[i] * dval_rms[i];
		y += dval_rms[i];
		//printf("%.2lf %.5lf\n", dz[i], dy_rms[i]);
	}


	//gaus
	double in[3][3] = { {x4,x3,x2},{x3,x2,x1},{x2,x1,x0} }, vec[3] = { x2y,xy,y }, out[3] = {};
	GaussJorden(in, vec, out);

	//printf("a = %g\n", out[0]);
	//printf("b = %g\n", out[1]);
	//printf("c = %g\n", out[2]);

	//printf("axis = %g\n", -out[1] / (2 * out[0]));
	std::pair<double, double> ret;
	ret.first = -out[1] / (2 * out[0])*shrink_x + shift_x;
	ret.second = -1 / 4 * (out[1] * out[1] - 4 * out[0] * out[2]) / out[0] * shrink_y + shift_y;
	if (output_flg) {
		printf("result (%g, %g)\n", ret.first, ret.second);
	}
	if (!isfinite(ret.first) || ret.first< val_ini - (Divide_n / 2)* pich || ret.first>val_ini + (Divide_n - 1 + Divide_n / 2)* pich) {
		ret.first = val_ini;
	}




	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_rms;

	return ret;
}


/*
bool decide_dy_center(std::vector<track_difference> &track_diff, const int Divide_n, double &dy_pich, double &dy_ini, const double min_dy_pich) {

	double *dy_sum = new double[Divide_n];
	double *dy_sum2 = new double[Divide_n];
	double *dy = new double[Divide_n];
	double sigma, dy_tmp;

	int loop_num = 0, count;
	while (true) {
		for (int i = 0; i < Divide_n; i++) {
			dy[i] = (i - Divide_n / 2) * dy_pich + dy_ini;
			dy_sum[i] = 0;
			dy_sum2[i] = 0;
			count = 0;
			for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
				dy_tmp = (itr->y1 - itr->y0 - (itr->ay0 + itr->ay1)*0.5*(itr->z1 - itr->z0) + dy[i]) * itr->sigma_py;

				dy_sum[i] += dy_tmp;
				dy_sum2[i] += dy_tmp * dy_tmp;
				count++;
			}
			dy_sum2[i] = dy_sum2[i] / count - dy_sum[i] * dy_sum[i] / count / count;
		}
		double min_dy = dy_sum2[0];
		int min_i = 0;
		for (int i = 0; i < Divide_n; i++) {
			if (min_dy > dy_sum2[i]) {
				min_dy = dy_sum2[i];
				min_i = i;
			}
		}

		//for (int i = 0; i < Divide_n; i++) {
		//	printf("%.2lf %.4lf\n", dy[i], sqrt(dy_sum2[i]));
		//}
		//printf(" min %d %.4lf\n", dy[min_i], sqrt(dy_sum2[min_i]));
		//printf("dy_ini %.2lf pich %.2lf\n", dy_ini, dy_pich);
		//min_iが探索範囲のedgeの場合中心を変更。それ以外は+でpichを半減
		dy_ini = dy[min_i];

		if (min_i != 0 && min_i != Divide_n - 1) {
			loop_num = 0;
			//pol2の軸
			dy_ini = -1 / 2. * (dy_sum2[min_i + 1] - dy_sum2[min_i - 1]) / (dy_sum2[min_i + 1] + dy_sum2[min_i - 1] - 2 * dy_sum2[min_i]) * dy_pich + dy[min_i];
			//printf("dy_ini %.2lf pich %.2lf\n", dy_ini, dy_pich);
			//終了条件
			if (dy_pich < min_dy_pich + 0.0001)break;
			dy_pich = std::max(dy_pich / 2, min_dy_pich);
		}
		loop_num++;
		//変なところに捕まったら・
		if (loop_num > 10) {
			for (int i = 0; i < Divide_n; i++) {
				printf("%.2lf %.4lf\n", dy[i], sqrt(dy_sum2[i]));
			}
			printf(" min %d %.4lf\n", dy[min_i], sqrt(dy_sum2[min_i]));
			if (loop_num > 15) {
				fprintf(stderr, "dx center not found\n");
				//忘れるとメモリリーク
				delete[] dy;
				delete[] dy_sum;
				delete[] dy_sum2;
				return false;
			}
		}
	}

	//忘れるとメモリリーク
	delete[] dy;
	delete[] dy_sum;
	delete[] dy_sum2;

	return true;
}
*/
double fit_pol2_dy(std::vector<track_difference> &track_diff, const int Divide_n, double dy_all_width, double dy_ini) {
	//double *dy_sum = new double[Divide_n];
	//double *dy_sum2 = new double[Divide_n];

	double *dy = new double[Divide_n];
	double *dy_rms = new double[Divide_n];
	double dy_tmp, dy_sum, dy_sum2;

	int count = 0;
	double dy_pich = dy_all_width / Divide_n;

	//各dzに対するdyRMSを求める
	for (int i = 0; i < Divide_n; i++) {
		dy[i] = dy_ini + (i - Divide_n / 2)* dy_pich;
		dy_sum = 0;
		dy_sum2 = 0;
		count = 0;
		for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
			dy_tmp = (itr->y1 - itr->y0 - (itr->ay0 + itr->ay1)*0.5*(itr->z1 - itr->z0) + dy[i]) * itr->sigma_py;

			dy_sum += dy_tmp;
			dy_sum2 += dy_tmp * dy_tmp;
			count++;
		}
		dy_rms[i] = sqrt(dy_sum2 / count - dy_sum * dy_sum / (count *count));
	}

	//各点を最小二乗法でFIT
	double x4, x3, x2, x1, x0, x2y, xy, y;
	x4 = 0;
	x3 = 0;
	x2 = 0;
	x1 = 0;
	x0 = 0;
	x2y = 0;
	xy = 0;
	y = 0;

	double shift_x = dy_ini, shift_y = dy_rms[Divide_n / 2];
	double shrink_x = dy_all_width / (2 * 10.), shrink_y = fabs(dy_rms[0] - shift_y) / 3;
	for (int i = 0; i < Divide_n; i++) {
		//printf("%.3lf %.7lf\n", dy[i], dy_rms[i]);

		dy[i] = (dy[i] - shift_x) / shrink_x;
		dy_rms[i] = (dy_rms[i] - shift_y) / shrink_y;

	}
	//printf("\n");
	//printf("x shift %g shrink%g\n", shift_x, shrink_x);
	//printf("y shift %g shrink%g\n", shift_y, shrink_y);

	//for (int i = 0; i < Divide_n; i++) {
	//	printf("%.3lf %.7lf\n", dy[i], dy_rms[i]);
	//}

	for (int i = 0; i < Divide_n; i++) {
		x4 += dy[i] * dy[i] * dy[i] * dy[i];
		x3 += dy[i] * dy[i] * dy[i];
		x2 += dy[i] * dy[i];
		x1 += dy[i];
		x0 += 1;

		x2y += dy[i] * dy[i] * dy_rms[i];
		xy += dy[i] * dy_rms[i];
		y += dy_rms[i];
		//printf("%.2lf %.5lf\n", dz[i], dy_rms[i]);
	}


	//gaus
	double in[3][3] = { {x4,x3,x2},{x3,x2,x1},{x2,x1,x0} }, vec[3] = { x2y,xy,y }, out[3] = {};
	GaussJorden(in, vec, out);

	//printf("a = %g\n", out[0]);
	//printf("b = %g\n", out[1]);
	//printf("c = %g\n", out[2]);

	//printf("axis = %g\n", -out[1] / (2 * out[0]));

	//printf("dy_trans= %.4lf\n", -out[1] / (2 * out[0])*shrink_x + shift_x);



	//忘れるとメモリリーク
	delete[] dy;
	delete[] dy_rms;

	return -out[1] / (2 * out[0])*shrink_x + shift_x;
}
/*
bool decide_dx_center(std::vector<track_difference> &track_diff, const int Divide_n, double &dx_pich, double &dx_ini, const double min_dx_pich) {

	double *dx_sum = new double[Divide_n];
	double *dx_sum2 = new double[Divide_n];
	double *dx = new double[Divide_n];
	double sigma, dx_tmp;

	int loop_num = 0, count;
	while (true) {
		for (int i = 0; i < Divide_n; i++) {
			dx[i] = (i - Divide_n / 2) * dx_pich + dx_ini;
			dx_sum[i] = 0;
			dx_sum2[i] = 0;
			count = 0;
			for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
				dx_tmp = (itr->x1 - itr->x0 - (itr->ax0 + itr->ax1)*0.5*(itr->z1 - itr->z0) + dx[i]) * itr->sigma_px;

				dx_sum[i] += dx_tmp;
				dx_sum2[i] += dx_tmp * dx_tmp;
				count++;
			}
			dx_sum2[i] = dx_sum2[i] / count - dx_sum[i] * dx_sum[i] / count / count;
		}
		double min_dx = dx_sum2[0];
		int min_i = 0;
		for (int i = 0; i < Divide_n; i++) {
			if (min_dx > dx_sum2[i]) {
				min_dx = dx_sum2[i];
				min_i = i;
			}
		}

		//for (int i = 0; i < Divide_n; i++) {
		//	printf("%.2lf %.4lf\n", dx[i], sqrt(dx_sum2[i]));
		//}
		//printf(" min %d %.4lf\n", dx[min_i], sqrt(dx_sum2[min_i]));
		//printf("dy_ini %.2lf pich %.2lf\n", dx_ini, dx_pich);
		//min_iが探索範囲のedgeの場合中心を変更。それ以外は+でpichを半減
		dx_ini = dx[min_i];

		if (min_i != 0 && min_i != Divide_n - 1) {
			loop_num = 0;
			//pol2の軸
			dx_ini = -1 / 2. * (dx_sum2[min_i + 1] - dx_sum2[min_i - 1]) / (dx_sum2[min_i + 1] + dx_sum2[min_i - 1] - 2 * dx_sum2[min_i]) * dx_pich + dx[min_i];
			//printf("dy_ini %.2lf pich %.2lf\n", dy_ini, dy_pich);
			//終了条件
			if (dx_pich < min_dx_pich + 0.0001)break;
			dx_pich = std::max(dx_pich / 2, min_dx_pich);
		}
		loop_num++;
		//変なところに捕まったら
		if (loop_num > 10) {
			for (int i = 0; i < Divide_n; i++) {
				printf("%.2lf %.4lf\n", dx[i], sqrt(dx_sum2[i]));
			}
			printf(" min %d %.4lf\n", dx[min_i], sqrt(dx_sum2[min_i]));
			if (loop_num > 15) {
				fprintf(stderr, "dx center not found\n");
				//忘れるとメモリリーク
				delete[] dx;
				delete[] dx_sum;
				delete[] dx_sum2;
				return false;
			}
		}
	}
	//忘れるとメモリリーク
	delete[] dx;
	delete[] dx_sum;
	delete[] dx_sum2;

	return true;
}
*/
double fit_pol2_dx(std::vector<track_difference> &track_diff, const int Divide_n, double dx_all_width, double dx_ini) {
	//double *dy_sum = new double[Divide_n];
	//double *dy_sum2 = new double[Divide_n];

	double *dx = new double[Divide_n];
	double *dx_rms = new double[Divide_n];
	double dx_tmp, dx_sum, dx_sum2;

	int count = 0;
	double dx_pich = dx_all_width / Divide_n;

	//各dzに対するdyRMSを求める
	for (int i = 0; i < Divide_n; i++) {
		dx[i] = dx_ini + (i - Divide_n / 2)* dx_pich;
		dx_sum = 0;
		dx_sum2 = 0;
		count = 0;
		for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
			dx_tmp = (itr->x1 - itr->x0 - (itr->ax0 + itr->ax1)*0.5*(itr->z1 - itr->z0) + dx[i]) * itr->sigma_px;

			dx_sum += dx_tmp;
			dx_sum2 += dx_tmp * dx_tmp;
			count++;
		}
		dx_rms[i] = sqrt(dx_sum2 / count - dx_sum * dx_sum / (count *count));
	}

	//各点を最小二乗法でFIT
	double x4, x3, x2, x1, x0, x2y, xy, y;
	x4 = 0;
	x3 = 0;
	x2 = 0;
	x1 = 0;
	x0 = 0;
	x2y = 0;
	xy = 0;
	y = 0;

	double shift_x = dx_ini, shift_y = dx_rms[Divide_n / 2];
	double shrink_x = dx_all_width / (2 * 10.), shrink_y = fabs(dx_rms[0] - shift_y) / 3;
	for (int i = 0; i < Divide_n; i++) {
		//printf("%.3lf %.7lf\n", dx[i], dx_rms[i]);

		dx[i] = (dx[i] - shift_x) / shrink_x;
		dx_rms[i] = (dx_rms[i] - shift_y) / shrink_y;

	}
	//printf("\n");
	//printf("x shift %g shrink%g\n", shift_x, shrink_x);
	//printf("y shift %g shrink%g\n", shift_y, shrink_y);

	//for (int i = 0; i < Divide_n; i++) {
	//	printf("%.3lf %.7lf\n", dy[i], dy_rms[i]);
	//}

	for (int i = 0; i < Divide_n; i++) {
		x4 += dx[i] * dx[i] * dx[i] * dx[i];
		x3 += dx[i] * dx[i] * dx[i];
		x2 += dx[i] * dx[i];
		x1 += dx[i];
		x0 += 1;

		x2y += dx[i] * dx[i] * dx_rms[i];
		xy += dx[i] * dx_rms[i];
		y += dx_rms[i];
		//printf("%.2lf %.5lf\n", dz[i], dy_rms[i]);
	}


	//gaus
	double in[3][3] = { {x4,x3,x2},{x3,x2,x1},{x2,x1,x0} }, vec[3] = { x2y,xy,y }, out[3] = {};
	GaussJorden(in, vec, out);

	//printf("a = %g\n", out[0]);
	//printf("b = %g\n", out[1]);
	//printf("c = %g\n", out[2]);

	//printf("axis = %g\n", -out[1] / (2 * out[0]));

	//printf("dx_trans= %.4lf\n", -out[1] / (2 * out[0])*shrink_x + shift_x);



	//忘れるとメモリリーク
	delete[] dx;
	delete[] dx_rms;

	return -out[1] / (2 * out[0])*shrink_x + shift_x;
}


//角度
//shrink
bool decide_daz_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich,bool output_flg) {

	double *dval_sum = new double[Divide_n];
	double *dval_sum2 = new double[Divide_n];
	double *dval = new double[Divide_n];
	double dval_tmp;
	int loop_num = 0, count;
	while (true) {
		for (int i = 0; i < Divide_n; i++) {
			dval[i] = (i - Divide_n / 2) * pich + val_ini;
			dval_sum[i] = 0;
			dval_sum2[i] = 0;
			count = 0;
			for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
				dval_tmp = (itr->ay0 - itr->ay1*dval[i])*itr->sigma_ay;

				dval_sum[i] += dval_tmp;
				dval_sum2[i] += dval_tmp * dval_tmp;
				count++;
			}
			dval_sum2[i] = dval_sum2[i] / count - dval_sum[i] * dval_sum[i] / count / count;
		}
		double min_dx = dval_sum2[0];
		int min_i = 0;
		for (int i = 0; i < Divide_n; i++) {
			if (min_dx > dval_sum2[i]) {
				min_dx = dval_sum2[i];
				min_i = i;
			}
		}
		if (output_flg) {
			for (int i = 0; i < Divide_n; i++) {
				printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			}
			printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
		}

		//min_iが探索範囲のedgeの場合中心を変更。それ以外は+でpichを半減
		val_ini = dval[min_i];

		if (min_i != 0 && min_i != Divide_n - 1) {
			loop_num = 0;
			//pol2の軸
			val_ini = -1 / 2. * (dval_sum2[min_i + 1] - dval_sum2[min_i - 1]) / (dval_sum2[min_i + 1] + dval_sum2[min_i - 1] - 2 * dval_sum2[min_i]) * pich + dval[min_i];
			//printf("dy_ini %.2lf pich %.2lf\n", dy_ini, dy_pich);
			//終了条件
			if (pich < min_pich + 0.000001)break;
			pich = std::max(pich / 2, min_pich);
		}
		loop_num++;
		//変なところに捕まったら・
		if (loop_num > 10) {
			//printf("val_ini %.2lf pich %.2lf\n", val_ini, pich);
			for (int i = 0; i < Divide_n; i++) {
				printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			}
			printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
			if (loop_num > 15) {
				fprintf(stderr, "daz center not found\n");
				//忘れるとメモリリーク
				delete[] dval;
				delete[] dval_sum;
				delete[] dval_sum2;

				return false;
			}
		}

	}

	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_sum;
	delete[] dval_sum2;

	return true;
}
std::pair<double, double> fit_pol2_daz(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini,bool output_flg) {
	//double *dy_sum = new double[Divide_n];
	//double *dy_sum2 = new double[Divide_n];

	double *dval = new double[Divide_n];
	double *dval_rms = new double[Divide_n];
	double dval_tmp, dval_sum, dval_sum2;

	int count = 0;
	double pich = val_all_width / Divide_n;

	//各dzに対するdyRMSを求める
	for (int i = 0; i < Divide_n; i++) {
		dval[i] = val_ini + (i - Divide_n / 2)* pich;
		dval_sum = 0;
		dval_sum2 = 0;
		count = 0;
		for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
			dval_tmp = (itr->ay0 - itr->ay1*dval[i])*itr->sigma_ay;

			dval_sum += dval_tmp;
			dval_sum2 += dval_tmp * dval_tmp;
			count++;
		}
		dval_rms[i] = sqrt(dval_sum2 / count - dval_sum * dval_sum / (count *count));
	}

	//各点を最小二乗法でFIT
	double x4, x3, x2, x1, x0, x2y, xy, y;
	x4 = 0;
	x3 = 0;
	x2 = 0;
	x1 = 0;
	x0 = 0;
	x2y = 0;
	xy = 0;
	y = 0;

	double shift_x = val_ini, shift_y = dval_rms[Divide_n / 2];
	double shrink_x = val_all_width / (2 * 10.), shrink_y = fabs(dval_rms[0] - shift_y) / 3;
	for (int i = 0; i < Divide_n; i++) {
		if (output_flg) {
			printf("%g %g\n", dval[i], dval_rms[i]);
		}
		dval[i] = (dval[i] - shift_x) / shrink_x;
		dval_rms[i] = (dval_rms[i] - shift_y) / shrink_y;

	}
	//printf("\n");
	//printf("x shift %g shrink%g\n", shift_x, shrink_x);
	//printf("y shift %g shrink%g\n", shift_y, shrink_y);

	//for (int i = 0; i < Divide_n; i++) {
	//	printf("%.3lf %.7lf\n", dy[i], dy_rms[i]);
	//}

	for (int i = 0; i < Divide_n; i++) {
		x4 += dval[i] * dval[i] * dval[i] * dval[i];
		x3 += dval[i] * dval[i] * dval[i];
		x2 += dval[i] * dval[i];
		x1 += dval[i];
		x0 += 1;

		x2y += dval[i] * dval[i] * dval_rms[i];
		xy += dval[i] * dval_rms[i];
		y += dval_rms[i];
		//printf("%.2lf %.5lf\n", dz[i], dy_rms[i]);
	}


	//gaus
	double in[3][3] = { {x4,x3,x2},{x3,x2,x1},{x2,x1,x0} }, vec[3] = { x2y,xy,y }, out[3] = {};
	GaussJorden(in, vec, out);

	//printf("a = %g\n", out[0]);
	//printf("b = %g\n", out[1]);
	//printf("c = %g\n", out[2]);

	//printf("axis = %g\n", -out[1] / (2 * out[0]));

	std::pair<double, double> ret;
	ret.first = -out[1] / (2 * out[0])*shrink_x + shift_x;
	ret.second = -1 / 4 * (out[1] * out[1] - 4 * out[0] * out[2]) / out[0] * shrink_y + shift_y;
	if (output_flg) {
		printf("result (%g, %g)\n", ret.first, ret.second);
	}
	if (!isfinite(ret.first) || ret.first< val_ini - (Divide_n / 2)* pich || ret.first>val_ini + (Divide_n - 1 + Divide_n / 2)* pich) {
		ret.first = val_ini;
	}


	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_rms;

	return ret;
}

//シフト
bool decide_dax_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich,bool output_flg) {

	double *dval_sum = new double[Divide_n];
	double *dval_sum2 = new double[Divide_n];
	double *dval = new double[Divide_n];
	double dval_tmp;
	int loop_num = 0, count;
	while (true) {
		for (int i = 0; i < Divide_n; i++) {
			dval[i] = (i - Divide_n / 2) * pich + val_ini;
			dval_sum[i] = 0;
			dval_sum2[i] = 0;
			count = 0;
			for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
				dval_tmp = (itr->ax0 - (itr->ax1 + dval[i]))*itr->sigma_ax;

				dval_sum[i] += dval_tmp;
				dval_sum2[i] += dval_tmp * dval_tmp;
				count++;
			}
			dval_sum2[i] = dval_sum2[i] / count - dval_sum[i] * dval_sum[i] / count / count;
		}
		double min_dx = dval_sum2[0];
		int min_i = 0;
		for (int i = 0; i < Divide_n; i++) {
			if (min_dx > dval_sum2[i]) {
				min_dx = dval_sum2[i];
				min_i = i;
			}
		}

		if (output_flg) {
			for (int i = 0; i < Divide_n; i++) {
				printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			}
			printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
		}
		//min_iが探索範囲のedgeの場合中心を変更。それ以外は+でpichを半減
		val_ini = dval[min_i];

		if (min_i != 0 && min_i != Divide_n - 1) {
			loop_num = 0;
			//pol2の軸
			val_ini = -1 / 2. * (dval_sum2[min_i + 1] - dval_sum2[min_i - 1]) / (dval_sum2[min_i + 1] + dval_sum2[min_i - 1] - 2 * dval_sum2[min_i]) * pich + dval[min_i];
			//printf("dy_ini %.2lf pich %.2lf\n", dy_ini, dy_pich);
			//終了条件
			if (pich < min_pich + 0.000001)break;
			pich = std::max(pich / 2, min_pich);
		}
		loop_num++;
		//変なところに捕まったら・
		if (loop_num > 10) {
			//printf("val_ini %.2lf pich %.2lf\n", val_ini, pich);
			for (int i = 0; i < Divide_n; i++) {
				printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			}
			printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
			if (loop_num > 15) {
				fprintf(stderr, "dax center not found\n");
				//忘れるとメモリリーク
				delete[] dval;
				delete[] dval_sum;
				delete[] dval_sum2;

				return false;
			}
		}

	}

	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_sum;
	delete[] dval_sum2;

	return true;
}
std::pair<double, double> fit_pol2_dax(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini, bool output_flg) {
	//double *dy_sum = new double[Divide_n];
	//double *dy_sum2 = new double[Divide_n];

	double *dval = new double[Divide_n];
	double *dval_rms = new double[Divide_n];
	double dval_tmp, dval_sum, dval_sum2;

	int count = 0;
	double pich = val_all_width / Divide_n;

	//各dzに対するdyRMSを求める
	for (int i = 0; i < Divide_n; i++) {
		dval[i] = val_ini + (i - Divide_n / 2)* pich;
		dval_sum = 0;
		dval_sum2 = 0;
		count = 0;
		for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
			dval_tmp = (itr->ax0 - (itr->ax1 + dval[i]))*itr->sigma_ax;

			dval_sum += dval_tmp;
			dval_sum2 += dval_tmp * dval_tmp;
			count++;
		}
		dval_rms[i] = sqrt(dval_sum2 / count - dval_sum * dval_sum / (count *count));
	}

	//各点を最小二乗法でFIT
	double x4, x3, x2, x1, x0, x2y, xy, y;
	x4 = 0;
	x3 = 0;
	x2 = 0;
	x1 = 0;
	x0 = 0;
	x2y = 0;
	xy = 0;
	y = 0;

	double shift_x = val_ini, shift_y = dval_rms[Divide_n / 2];
	double shrink_x = val_all_width / (2 * 10.), shrink_y = fabs(dval_rms[0] - shift_y) / 3;
	for (int i = 0; i < Divide_n; i++) {
		if (output_flg) {
			printf("%g %g\n", dval[i], dval_rms[i]);
		}
		dval[i] = (dval[i] - shift_x) / shrink_x;
		dval_rms[i] = (dval_rms[i] - shift_y) / shrink_y;

	}
	//printf("\n");
	//printf("x shift %g shrink%g\n", shift_x, shrink_x);
	//printf("y shift %g shrink%g\n", shift_y, shrink_y);

	//for (int i = 0; i < Divide_n; i++) {
	//	printf("%.3lf %.7lf\n", dy[i], dy_rms[i]);
	//}

	for (int i = 0; i < Divide_n; i++) {
		x4 += dval[i] * dval[i] * dval[i] * dval[i];
		x3 += dval[i] * dval[i] * dval[i];
		x2 += dval[i] * dval[i];
		x1 += dval[i];
		x0 += 1;

		x2y += dval[i] * dval[i] * dval_rms[i];
		xy += dval[i] * dval_rms[i];
		y += dval_rms[i];
		//printf("%.2lf %.5lf\n", dz[i], dy_rms[i]);
	}


	//gaus
	double in[3][3] = { {x4,x3,x2},{x3,x2,x1},{x2,x1,x0} }, vec[3] = { x2y,xy,y }, out[3] = {};
	GaussJorden(in, vec, out);

	//printf("a = %g\n", out[0]);
	//printf("b = %g\n", out[1]);
	//printf("c = %g\n", out[2]);

	//printf("axis = %g\n", -out[1] / (2 * out[0]));

	//printf("dx_trans= %.4lf\n", -out[1] / (2 * out[0])*shrink_x + shift_x);

	std::pair<double, double> ret;
	ret.first = -out[1] / (2 * out[0])*shrink_x + shift_x;
	ret.second = -1 / 4 * (out[1] * out[1] - 4 * out[0] * out[2]) / out[0] * shrink_y + shift_y;
	if (output_flg) {
		printf("result (%g, %g)\n", ret.first, ret.second);
	}
	if (!isfinite(ret.first) || ret.first< val_ini - (Divide_n / 2)* pich || ret.first>val_ini + (Divide_n - 1 + Divide_n / 2)* pich) {
		ret.first = val_ini;
	}


	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_rms;

	return ret;
}

bool decide_day_center(std::vector<track_difference> &track_diff, const int Divide_n, double &pich, double &val_ini, const double min_pich,bool output_flg) {

	double *dval_sum = new double[Divide_n];
	double *dval_sum2 = new double[Divide_n];
	double *dval = new double[Divide_n];
	double dval_tmp;
	int loop_num = 0, count;
	while (true) {
		for (int i = 0; i < Divide_n; i++) {
			dval[i] = (i - Divide_n / 2) * pich + val_ini;
			dval_sum[i] = 0;
			dval_sum2[i] = 0;
			count = 0;
			for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
				dval_tmp = (itr->ay0 - (itr->ay1 + dval[i]))*itr->sigma_ay;

				dval_sum[i] += dval_tmp;
				dval_sum2[i] += dval_tmp * dval_tmp;
				count++;
			}
			dval_sum2[i] = dval_sum2[i] / count - dval_sum[i] * dval_sum[i] / count / count;
		}
		double min_dx = dval_sum2[0];
		int min_i = 0;
		for (int i = 0; i < Divide_n; i++) {
			if (min_dx > dval_sum2[i]) {
				min_dx = dval_sum2[i];
				min_i = i;
			}
		}

		if (output_flg) {
			for (int i = 0; i < Divide_n; i++) {
				printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			}
			printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
		}
		//min_iが探索範囲のedgeの場合中心を変更。それ以外は+でpichを半減
		val_ini = dval[min_i];

		if (min_i != 0 && min_i != Divide_n - 1) {
			loop_num = 0;
			//pol2の軸
			val_ini = -1 / 2. * (dval_sum2[min_i + 1] - dval_sum2[min_i - 1]) / (dval_sum2[min_i + 1] + dval_sum2[min_i - 1] - 2 * dval_sum2[min_i]) * pich + dval[min_i];
			//printf("dy_ini %.2lf pich %.2lf\n", dy_ini, dy_pich);
			//終了条件
			if (pich < min_pich + 0.000001)break;
			pich = std::max(pich / 2, min_pich);
		}
		loop_num++;
		//変なところに捕まったら・
		if (loop_num > 10) {
			//printf("val_ini %.2lf pich %.2lf\n", val_ini, pich);
			for (int i = 0; i < Divide_n; i++) {
				printf("%g %g\n", dval[i], sqrt(dval_sum2[i]));
			}
			printf(" min %g %g\n", dval[min_i], sqrt(dval_sum2[min_i]));
			if (loop_num > 15) {
				fprintf(stderr, "day center not found\n");
				//忘れるとメモリリーク
				delete[] dval;
				delete[] dval_sum;
				delete[] dval_sum2;

				return false;
			}
		}

	}

	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_sum;
	delete[] dval_sum2;

	return true;
}
std::pair<double, double> fit_pol2_day(std::vector<track_difference> &track_diff, const int Divide_n, double val_all_width, double val_ini, bool output_flg) {
	//double *dy_sum = new double[Divide_n];
	//double *dy_sum2 = new double[Divide_n];

	double *dval = new double[Divide_n];
	double *dval_rms = new double[Divide_n];
	double dval_tmp, dval_sum, dval_sum2;

	int count = 0;
	double pich = val_all_width / Divide_n;

	//各dzに対するdyRMSを求める
	for (int i = 0; i < Divide_n; i++) {
		dval[i] = val_ini + (i - Divide_n / 2)* pich;
		dval_sum = 0;
		dval_sum2 = 0;
		count = 0;
		for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
			dval_tmp = (itr->ay0 - (itr->ay1 + dval[i]))*itr->sigma_ay;

			dval_sum += dval_tmp;
			dval_sum2 += dval_tmp * dval_tmp;
			count++;
		}
		dval_rms[i] = sqrt(dval_sum2 / count - dval_sum * dval_sum / (count *count));
	}

	//各点を最小二乗法でFIT
	double x4, x3, x2, x1, x0, x2y, xy, y;
	x4 = 0;
	x3 = 0;
	x2 = 0;
	x1 = 0;
	x0 = 0;
	x2y = 0;
	xy = 0;
	y = 0;

	double shift_x = val_ini, shift_y = dval_rms[Divide_n / 2];
	double shrink_x = val_all_width / (2 * 10.), shrink_y = fabs(dval_rms[0] - shift_y) / 3;
	for (int i = 0; i < Divide_n; i++) {
		if (output_flg) {
			printf("%g %g\n", dval[i], dval_rms[i]);
		}

		dval[i] = (dval[i] - shift_x) / shrink_x;
		dval_rms[i] = (dval_rms[i] - shift_y) / shrink_y;

	}
	//printf("\n");
	//printf("x shift %g shrink%g\n", shift_x, shrink_x);
	//printf("y shift %g shrink%g\n", shift_y, shrink_y);

	//for (int i = 0; i < Divide_n; i++) {
	//	printf("%.3lf %.7lf\n", dy[i], dy_rms[i]);
	//}

	for (int i = 0; i < Divide_n; i++) {
		x4 += dval[i] * dval[i] * dval[i] * dval[i];
		x3 += dval[i] * dval[i] * dval[i];
		x2 += dval[i] * dval[i];
		x1 += dval[i];
		x0 += 1;

		x2y += dval[i] * dval[i] * dval_rms[i];
		xy += dval[i] * dval_rms[i];
		y += dval_rms[i];
		//printf("%.2lf %.5lf\n", dz[i], dy_rms[i]);
	}


	//gaus
	double in[3][3] = { {x4,x3,x2},{x3,x2,x1},{x2,x1,x0} }, vec[3] = { x2y,xy,y }, out[3] = {};
	GaussJorden(in, vec, out);

	//printf("a = %g\n", out[0]);
	//printf("b = %g\n", out[1]);
	//printf("c = %g\n", out[2]);

	//printf("axis = %g\n", -out[1] / (2 * out[0]));

	//printf("dx_trans= %.4lf\n", -out[1] / (2 * out[0])*shrink_x + shift_x);

	std::pair<double, double> ret;
	ret.first = -out[1] / (2 * out[0])*shrink_x + shift_x;
	ret.second = -1 / 4 * (out[1] * out[1] - 4 * out[0] * out[2]) / out[0] * shrink_y + shift_y;
	if (output_flg) {
		printf("result (%g, %g)\n", ret.first, ret.second);
	}
	if (!isfinite(ret.first) || ret.first< val_ini - (Divide_n / 2)* pich || ret.first>val_ini + (Divide_n - 1 + Divide_n / 2)* pich) {
		ret.first = val_ini;
	}



	//忘れるとメモリリーク
	delete[] dval;
	delete[] dval_rms;

	return ret;
}
/*
void GaussJorden(double in[3][3], double b[3], double c[3]) {


	double a[3][4];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			if (j < 3) {
				a[i][j] = in[i][j];
			}
			else {
				a[i][j] = b[i];
			}
		}
	}
	int N = 3;
	double p, d;         // ピボット係数、ピボット行ｘ係数
	double max, dummy;   // 最大絶対値、入れ替え時ダミー
	int s;

	//元の連立方程式をコンソール出力
   //for (int i = 0; i < N; i++) {
   //	for (int j = 0; j < N; j++)
   //		printf("%+fx%d ", a[i][j], j + 1);
   //	printf("= %+f\n", a[i][N]);
   //}

	for (int k = 0; k < N; k++) {
		// 行入れ替え
		max = 0; s = k;
		for (int j = k; j < N; j++) {
			if (fabs(a[j][k]) > max) {
				max = fabs(a[j][k]);
				s = j;
			}
		}
		if (max == 0) {
			printf("解けない！");
			exit(1);
		}
		for (int j = 0; j <= N; j++) {
			dummy = a[k][j];
			a[k][j] = a[s][j];
			a[s][j] = dummy;
		}

		// ピボット係数
		p = a[k][k];

		// ピボット行を p で除算
		for (int j = k; j < N + 1; j++)
			a[k][j] /= p;

		// ピボット列の掃き出し
		for (int i = 0; i < N; i++) {
			if (i != k) {
				d = a[i][k];
				for (int j = k; j < N + 1; j++)
					a[i][j] -= d * a[k][j];
			}
		}
	}

	// 結果出力
	for (int k = 0; k < N; k++) {
		c[k] = a[k][N];
		//printf("x%d = %f\n", k + 1, a[k][N]);
	}
}
*/






void align_fine_tune_out(corrmap0::Corrmap &corr, std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&pair, cut_param &param) {

	std::vector<output_format_link>link;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		link.push_back(output_format(itr->first, itr->second));
	}
	int id_x, id_y;
	double sigma, px;

	//出力
	output_link_bin("test_out0.bin", link);



	//角度
	double xy[2] = {}, x[2] = {}, y[2] = {}, x2[2] = {}, n[2] = {};
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		//ay_idであってる
		int id_x = ay_id(itr->b[0].ax);
		int id_y = ay_id(itr->b[0].ay);
		auto param_min = param.cut_dax.find(id_x);
		auto param_max = param.cut_dax.find(id_x + 1);

		//daxのsigma
		{
			param_min = param.cut_dax.find(id_x);
			param_max = param.cut_dax.find(id_x + 1);
			px = fabs(itr->b[0].ax);

			if (param_min == param.cut_dax.end() || param_max == param.cut_dax.end()) {
				sigma = (px*param.ex_dax.first + param.ex_dax.second);
			}
			else {
				sigma = ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(px - param_min->first*0.1) + param_min->second);
			}
		}
		sigma = 1 / (sigma * sigma);
		x[0] += itr->b[0].ax*sigma;
		x2[0] += itr->b[0].ax*itr->b[0].ax*sigma;
		xy[0] += itr->b[0].ax*itr->dax*sigma;
		y[0] += itr->dax*sigma;
		n[0] += sigma;


		//dayのsigma
		{
			param_min = param.cut_day.find(id_y);
			param_max = param.cut_day.find(id_y + 1);
			px = fabs(itr->b[0].ay);

			if (param_min == param.cut_day.end() || param_max == param.cut_day.end()) {
				sigma = (px*param.ex_day.first + param.ex_day.second);
			}
			else {
				sigma = ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(px - param_min->first*0.1) + param_min->second);
			}
		}
		sigma = 1 / (sigma * sigma);
		x[1] += itr->b[0].ay*sigma;
		x2[1] += itr->b[0].ay*itr->b[0].ay*sigma;
		xy[1] += itr->b[0].ay*itr->day*sigma;
		y[1] += itr->day*sigma;
		n[1] += sigma;
	}
	double dax, day, dax_slope, day_slope;
	//dax = (n[0] * xy[0] - x[0] * y[0]) / (n[0] * x2[0] - x[0] * x[0]);
	//dax_slope = (x2[0] * y[0] - xy[0] * x[0]) / (n[0] * x2[0] - x[0] * x[0]);
	//day = (n[1] * xy[1] - x[1] * y[1]) / (n[1] * x2[1] - x[1] * x[1]);
	//day_slope = (x2[1] * y[1] - xy[1] * x[1]) / (n[1] * x2[1] - x[1] * x[1]);
	//切片0で固定
	dax = 0;
	day = 0;
	dax_slope = xy[0] / x2[0];
	day_slope = xy[1] / x2[1];
	printf("dax = %.6lf slope = %.6lf\n", dax, dax_slope);
	printf("day = %.6lf slope = %.6lf\n", day, day_slope);

	corr.notuse_d[0] = corr.notuse_d[0] / (1 + day_slope);
	corr.notuse_d[1] = corr.notuse_d[1] - dax;
	corr.notuse_d[2] = corr.notuse_d[2] - day;

	//初期化
	for (int i = 0; i < 2; i++) {
		xy[i] = 0;
		x[i] = 0;
		y[i] = 0;
		x2[i] = 0;
		n[i] = 0;
	}

	//位置
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		itr->b[1].ax = itr->b[1].ax / (1 + day_slope) - dax;
		itr->b[1].ay = itr->b[1].ay / (1 + day_slope) - day;
		itr->Calc_difference();

		//ay_idであってる
		int id_x = ay_id(itr->b[0].ax);
		int id_y = ay_id(itr->b[0].ay);
		auto param_min = param.cut_dx.find(id_x);
		auto param_max = param.cut_dx.find(id_x + 1);

		//dxのsigma
		{
			param_min = param.cut_dx.find(id_x);
			param_max = param.cut_dx.find(id_x + 1);
			px = fabs(itr->dx);

			if (param_min == param.cut_dx.end() || param_max == param.cut_dx.end()) {
				sigma = (px*param.ex_dx.first + param.ex_dx.second);
			}
			else {
				sigma = ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(px - param_min->first*0.1) + param_min->second);
			}
		}
		sigma = 1 / (sigma * sigma);
		x[0] += itr->b[0].ax*sigma;
		x2[0] += itr->b[0].ax*itr->b[0].ax*sigma;
		xy[0] += itr->b[0].ax*itr->dx*sigma;
		y[0] += itr->dx*sigma;
		n[0] += sigma;


		//dyのsigma
		{
			param_min = param.cut_dy.find(id_y);
			param_max = param.cut_dy.find(id_y + 1);
			px = fabs(itr->b[0].ay);

			if (param_min == param.cut_dy.end() || param_max == param.cut_dy.end()) {
				sigma = (px*param.ex_dy.first + param.ex_dy.second);
			}
			else {
				sigma = ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(px - param_min->first*0.1) + param_min->second);
			}
		}
		sigma = 1 / (sigma * sigma);
		x[1] += itr->b[0].ay*sigma;
		x2[1] += itr->b[0].ay*itr->b[0].ay*sigma;
		xy[1] += itr->b[0].ay*itr->dy*sigma;
		y[1] += itr->dy*sigma;
		n[1] += sigma;
	}
	double dx, dy, dx_slope, dy_slope;
	//dx = (n[0] * xy[0] - x[0] * y[0]) / (n[0] * x2[0] - x[0] * x[0]);
	//dx_slope = (x2[0] * y[0] - xy[0] * x[0]) / (n[0] * x2[0] - x[0] * x[0]);
	//dy = (n[1] * xy[1] - x[1] * y[1]) / (n[1] * x2[1] - x[1] * x[1]);
	//dy_slope = (x2[1] * y[1] - xy[1] * x[1]) / (n[1] * x2[1] - x[1] * x[1]);
	dx = 0;
	dy = 0;
	dx_slope = xy[0] / x2[0];
	dy_slope = xy[1] / x2[1];
	printf("dx = %.1lf slope = %.6lf\n", dx, dx_slope);
	printf("dy = %.1lf slope = %.6lf\n", dy, dy_slope);

	//角度補正後出力
	output_link_bin("test_out1.bin", link);

	corr.position[4] = corr.position[4] - dx;
	corr.position[5] = corr.position[5] - dy;
	corr.dz = corr.dz + dy_slope;

	for (auto itr = link.begin(); itr != link.end(); itr++) {
		itr->b[1].x = itr->b[1].x - dx;
		itr->b[1].y = itr->b[1].y - dy;
		itr->b[1].z = itr->b[1].z + dy_slope;
		itr->Calc_difference();
	}
	//位置補正後出力
	output_link_bin("test_out2.bin", link);

}

void unique_linklet(std::vector<output_format_link> &link, double &rms_dal, double &rms_dl) {
	int begin_size = link.size();

	std::vector<output_format_link> link_sel;

	double connection_vale;
	output_format_link link_tmp;
	std::map<int, output_format_link> link_map0, link_map1;
	//rawid0でmulti消し
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		auto res = link_map0.insert(std::make_pair(itr->b[0].rawid, *itr));
		if (!res.second) {
			if (itr->dal*itr->dal < res.first->second.dal*res.first->second.dal) {
				res.first->second = *itr;
			}
		}
	}
	//rawid1でmulti消し
	for (auto itr = link_map0.begin(); itr != link_map0.end(); itr++) {
		auto res = link_map1.insert(std::make_pair(itr->second.b[1].rawid, itr->second));
		if (!res.second) {
			if (itr->second.dal*itr->second.dal < res.first->second.dal*res.first->second.dal) {
				res.first->second = itr->second;
			}
		}
	}
	link_sel.clear();
	for (auto itr = link_map1.begin(); itr != link_map1.end(); itr++) {
		link_sel.push_back(itr->second);
	}
	link_map0.clear();
	link_map1.clear();

	//output_link_bin("test_out.bin",link_sel);

	double p_rms_dl = 100, p_rms_dal = 0.02;
	double mean_dl = 0, mean_dal = 0;
	double dal_min, dal_max, dl_min, dl_max;
	double rms_ratio[2] = {};
	bool flg = true;
	int loop_num = 0,count=0;
	while (flg) {
		dal_min =  - 1*p_rms_dal * 3;
		dal_max =  p_rms_dal * 3;
		dl_min = - 1*p_rms_dl * 3;
		dl_max =  + p_rms_dl * 3;

		count = 0;
		rms_dl = 0;
		rms_dal = 0;
		mean_dl = 0;
		mean_dal = 0;

		for (auto itr = link_sel.begin(); itr != link_sel.end(); itr++) {
			if (!isfinite(itr->dal))continue;
			if (!isfinite(itr->dl))continue;
			if (itr->dal < dal_min)continue;
			if (itr->dal > dal_max)continue;
			if (itr->dl < dl_min)continue;
			if (itr->dl > dl_max)continue;
			count++;
			mean_dl += itr->dl;
			mean_dal += itr->dal;
			rms_dl += itr->dl*itr->dl;
			rms_dal += itr->dal*itr->dal;
		}
		mean_dal = mean_dal / count;
		mean_dl = mean_dl / count;
		rms_dal = sqrt(rms_dal / count - mean_dal * mean_dal);
		rms_dl = sqrt(rms_dl / count - mean_dl * mean_dl);

		rms_ratio[0] = rms_dal / p_rms_dal;
		rms_ratio[1] = rms_dl / p_rms_dl;
		//printf("LOOP %d count %d\n", loop_num,count);

		//printf("rms ratio: dal %.4lf dl %.4lf\n", rms_ratio[0], rms_ratio[1]);
		if (fabs(1 - rms_ratio[0]) < 0.1) {
			if (fabs(1 - rms_ratio[1]) < 0.1) {
				flg = false;
			}
		}
		//printf("mean dl %4.2lf dal%6.5lf\n", mean_dl, mean_dal);
		//printf("rms dl %5.1lf dal%5.4lf\n", rms_dl, rms_dal);
		p_rms_dal = rms_dal;
		p_rms_dl = rms_dl;

		loop_num++;
		if (loop_num > 100) {
			fprintf(stderr, "dal dl distributiuon exception\n");
			exit(1);
		}
	}

	//rawid0でmulti消し
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		auto res = link_map0.insert(std::make_pair(itr->b[0].rawid, *itr));
		if (!res.second) {
			if (calc_connection_value(*itr, rms_dal, rms_dl) < calc_connection_value(res.first->second, rms_dal, rms_dl))
				res.first->second = *itr;
		}
	}

	//rawid1でmulti消し
	for (auto itr = link_map0.begin(); itr != link_map0.end(); itr++) {
		auto res = link_map1.insert(std::make_pair(itr->second.b[1].rawid, itr->second));
		if (!res.second) {
			if (calc_connection_value(itr->second, rms_dal, rms_dl) < calc_connection_value(res.first->second, rms_dal, rms_dl))
				res.first->second = itr->second;
		}
	}
	link.clear();
	for (auto itr = link_map1.begin(); itr != link_map1.end(); itr++) {
		link.push_back(itr->second);
	}
	link_map0.clear();
	link_map1.clear();

	printf("multi delete %d -->%d\n", begin_size, link.size());
}
void select_linklet(std::vector<output_format_link> &link, double &rms_dal, double &rms_dl) {
	int begin_size = link.size();

	std::vector<output_format_link> link_sel;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (!isfinite(itr->dal))continue;
		if (!isfinite(itr->dl))continue;
		if (fabs(itr->dal / rms_dal) > 3)continue;
		if (fabs(itr->dl / rms_dl) > 3)continue;
		link_sel.push_back(*itr);
	}
	link = link_sel;

	printf("multi delete %d -->%d\n", begin_size, link.size());
}

double calc_connection_value(output_format_link &link,double rms_dal,double rms_dl) {
	return link.dal / rms_dal * link.dal / rms_dal + link.dl / rms_dl * link.dl / rms_dl;
}
void cacl_cut_x(std::vector<output_format_link>&link, std::map<int, double> &cut_dx, std::map<int, double> &cut_dax) {
	std::multimap<int, output_format_link*> link_map;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (ax_id(itr->b[0].ax) > 19)continue;
		link_map.insert(std::make_pair(ax_id(itr->b[0].ax), &(*itr)));
	}

	std::vector<output_format_link> link_tmp;
	int count;
	for (auto itr = link_map.begin(); itr != link_map.end(); itr++) {
		count = link_map.count(itr->first);

		link_tmp.clear();
		auto range = link_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			link_tmp.push_back(*(res->second));
		}

		double p_rms_dx = 1000, p_rms_dax = 1;
		double mean_dx = 0, mean_dax = 0;
		double rms_dx, rms_dax;
		double rms_ratio[2] = {};
		bool flg = true;
		int loop_num = 0;
		while (flg) {

			count = 0;
			rms_dx = 0;
			rms_dax = 0;
			mean_dx = 0;
			mean_dax = 0;

			for (auto itr2 = link_tmp.begin(); itr2 != link_tmp.end(); itr2++) {
				count++;
				if (p_rms_dax * 3 < fabs(itr2->dax))continue;
				if (p_rms_dx * 3 < fabs(itr2->dx))continue;
				mean_dx += itr2->dx;
				mean_dax += itr2->dax;
				rms_dx += itr2->dx*itr2->dx;
				rms_dax += itr2->dax*itr2->dax;
			}
			mean_dax = mean_dax / count;
			mean_dx = mean_dx / count;
			rms_dax = sqrt(rms_dax / count - mean_dax * mean_dax);
			rms_dx = sqrt(rms_dx / count - mean_dx * mean_dx);

			rms_ratio[0] = rms_dax / p_rms_dax;
			rms_ratio[1] = rms_dx / p_rms_dx;
			//printf("LOOP %d count %d\n", loop_num, count);
			//printf("rms ratio: dal %.4lf dl %.4lf\n", rms_ratio[0], rms_ratio[1]);
			if (fabs(1 - rms_ratio[0]) < 0.1) {
				if (fabs(1 - rms_ratio[1]) < 0.1) {
					flg = false;
				}
			}
			//printf("mean dl %4.2lf dal%6.5lf\n", mean_dl, mean_dal);
			//printf("rms dl %5.1lf dal%5.4lf\n", rms_dl, rms_dal);
			p_rms_dax = rms_dax;
			p_rms_dx = rms_dx;

			loop_num++;
			if (loop_num > 100) {
				fprintf(stderr, "dal dl distributiuon exception\n");
				exit(1);
			}
		}
		cut_dax.insert(std::make_pair(itr->first, rms_dax));
		cut_dx.insert(std::make_pair(itr->first, rms_dx));
		count = link_map.count(itr->first);

		itr = std::next(itr, count - 1);
	}
}
void cacl_cut_y(std::vector<output_format_link>&link, std::map<int, double> &cut_dy, std::map<int, double> &cut_day) {
	std::multimap<int, output_format_link*> link_map;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (ay_id(itr->b[0].ay) > 30)continue;
		link_map.insert(std::make_pair(ay_id(itr->b[0].ay), &(*itr)));
	}

	std::vector<output_format_link> link_tmp;
	int count;
	for (auto itr = link_map.begin(); itr != link_map.end(); itr++) {
		//printf("ay id = %d\n", itr->first);
		count = link_map.count(itr->first);

		link_tmp.clear();
		auto range = link_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			link_tmp.push_back(*(res->second));
		}

		double p_rms_dy = 10000, p_rms_day = 1;
		double mean_dy = 0, mean_day = 0;
		double rms_dy, rms_day;
		double rms_ratio[2] = {};
		bool flg = true;
		int loop_num = 0;
		while (flg) {

			count = 0;
			rms_dy = 0;
			rms_day = 0;
			mean_dy = 0;
			mean_day = 0;

			for (auto itr2 = link_tmp.begin(); itr2 != link_tmp.end(); itr2++) {
				if (p_rms_day * 3 < fabs(itr2->day))continue;
				if (p_rms_dy * 3 < fabs(itr2->dy))continue;

				count++;
				mean_dy += itr2->dy;
				mean_day += itr2->day;
				rms_dy += itr2->dy*itr2->dy;
				rms_day += itr2->day*itr2->day;
			}
			mean_day = mean_day / count;
			mean_dy = mean_dy / count;
			rms_day = sqrt(rms_day / count - mean_day * mean_day);
			rms_dy = sqrt(rms_dy / count - mean_dy * mean_dy);

			rms_ratio[0] = rms_day / p_rms_day;
			rms_ratio[1] = rms_dy / p_rms_dy;
			//printf("LOOP %d count %d\n", loop_num, count);
			//printf("rms ratio: dal %.4lf dl %.4lf\n", rms_ratio[0], rms_ratio[1]);
			if (fabs(1 - rms_ratio[0]) < 0.1) {
				if (fabs(1 - rms_ratio[1]) < 0.1) {
					flg = false;
				}
			}
			//printf("mean dl %4.2lf dal%6.5lf\n", mean_dy, mean_day);
			//printf("rms dl %5.1lf dal%5.4lf\n", rms_dy, rms_day);
			p_rms_day = rms_day;
			p_rms_dy = rms_dy;

			loop_num++;
			if (loop_num > 100) {
				fprintf(stderr, "day dy distributiuon exception\n");
				exit(1);
			}
		}
		cut_day.insert(std::make_pair(itr->first, rms_day));
		cut_dy.insert(std::make_pair(itr->first, rms_dy));
		count = link_map.count(itr->first);
		itr = std::next(itr, count - 1);

	}
}
void cacl_cut_rl(std::vector<output_format_link>&link, std::map<int, double> &cut_dr, std::map<int, double> &cut_dar, std::map<int, double> &cut_dl, std::map<int, double> &cut_dal) {
	std::multimap<int, output_format_link*> link_map;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (ang_id(sqrt(itr->b[0].ax*itr->b[0].ax+itr->b[0].ay*itr->b[0].ay)) > 30)continue;
		link_map.insert(std::make_pair(ang_id(sqrt(itr->b[0].ax*itr->b[0].ax + itr->b[0].ay*itr->b[0].ay)), &(*itr)));
	}

	std::vector<output_format_link> link_tmp;
	int count;
	for (auto itr = link_map.begin(); itr != link_map.end(); itr++) {
		//printf("\n\n\nangle id = %d\n", itr->first);
		count = link_map.count(itr->first);

		link_tmp.clear();
		auto range = link_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			link_tmp.push_back(*(res->second));
		}

		double p_rms_dr = 10000, p_rms_dar = 1, p_rms_dl = 10000, p_rms_dal = 1;
		double mean_dr = 0, mean_dar = 0, mean_dl = 0, mean_dal = 0;
		double rms_dr, rms_dar, rms_dl, rms_dal;
		double rms_ratio[4] = {};
		bool flg = true;
		int loop_num = 0;
		while (flg) {

			count = 0;
			rms_dr = 0;
			rms_dar = 0;
			rms_dl = 0;
			rms_dal = 0;
			mean_dl = 0;
			mean_dal = 0;

			for (auto itr2 = link_tmp.begin(); itr2 != link_tmp.end(); itr2++) {
				if (p_rms_dar * 3.5 < fabs(itr2->dar))continue;
				if (p_rms_dr * 3.5 < fabs(itr2->dr))continue;
				if (p_rms_dal * 3.5 < fabs(itr2->dal))continue;
				if (p_rms_dl * 3.5 < fabs(itr2->dl))continue;

				if (!isfinite(itr2->dr))continue;
				if (!isfinite(itr2->dar))continue;
				if (!isfinite(itr2->dl))continue;
				if (!isfinite(itr2->dal))continue;

				count++;
				mean_dr += itr2->dr;
				mean_dar += itr2->dar;
				rms_dr += itr2->dr*itr2->dr;
				rms_dar += itr2->dar*itr2->dar;
				mean_dl += itr2->dl;
				mean_dal += itr2->dal;
				rms_dl += itr2->dl*itr2->dl;
				rms_dal += itr2->dal*itr2->dal;
			}
			if (count < 10) {
				p_rms_dar *= 1.1;
				p_rms_dr *= 1.1;
				p_rms_dal *= 1.1;
				p_rms_dl *= 1.1;
				continue;
			}
			mean_dar = mean_dar / count;
			mean_dr = mean_dr / count;
			rms_dar = sqrt(rms_dar / count - mean_dar * mean_dar);
			rms_dr = sqrt(rms_dr / count - mean_dr * mean_dr);
			mean_dal = mean_dal / count;
			mean_dl = mean_dl / count;
			rms_dal = sqrt(rms_dal / count - mean_dal * mean_dal);
			rms_dl = sqrt(rms_dl / count - mean_dl * mean_dl);
			if (!isfinite(rms_dar) || !isfinite(rms_dr) ||
				!isfinite(rms_dal) || !isfinite(rms_dl)) {
				p_rms_dar *= 1.1;
				p_rms_dr *= 1.1;
				p_rms_dal *= 1.1;
				p_rms_dl *= 1.1;
				continue;
			}
			rms_ratio[0] = rms_dar / p_rms_dar;
			rms_ratio[1] = rms_dr / p_rms_dr;
			rms_ratio[2] = rms_dal / p_rms_dal;
			rms_ratio[3] = rms_dl / p_rms_dl;
			//printf("LOOP %d count %d\n", loop_num, count);

			//printf("rms ratio: dal %.2lf dl %.2lf %.2lf %.2lf\n", rms_ratio[0], rms_ratio[1], rms_ratio[2], rms_ratio[3]);
			if (fabs(1 - rms_ratio[0]) < 0.1) {
				if (fabs(1 - rms_ratio[1]) < 0.1) {
					if (fabs(1 - rms_ratio[2]) < 0.1) {
						if (fabs(1 - rms_ratio[3]) < 0.1) {
							flg = false;
						}
					}
				}
			}
			//printf("mean dl %4.2lf dal%6.5lf\n", mean_dl, mean_dal);
			//printf("rms dl %5.1lf dal%5.4lf\n", rms_dl, rms_dal);
			p_rms_dar = rms_dar;
			p_rms_dr = rms_dr;
			p_rms_dal = rms_dal;
			p_rms_dl = rms_dl;

			loop_num++;
			if (loop_num > 100) {
				fprintf(stderr, "dal dl distributiuon exception\n");
				//exit(1);
				break;
			}
		}
		cut_dar.insert(std::make_pair(itr->first, rms_dar));
		cut_dr.insert(std::make_pair(itr->first, rms_dr));
		cut_dal.insert(std::make_pair(itr->first, rms_dal));
		cut_dl.insert(std::make_pair(itr->first, rms_dl));
		count = link_map.count(itr->first);

		itr = std::next(itr, count - 1);

	}

}
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_sigma(std::vector<vxx::base_track_t>&base1, std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t>&base0, double &hash_pos, double  &hash_ang, std::tuple<double, double, double, double> &min,cut_param &param) {

	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connected;

	//base1
	double ex_x, ex_y;
	double x_min, x_max, y_min, y_max;
	double ax_min, ax_max, ay_min, ay_max;
	double angle_acc_x, angle_acc_y;
	int i_ax_min, i_ax_max, i_ay_min, i_ay_max;
	int i_x_min, i_x_max, i_y_min, i_y_max;
	std::tuple<int, int, int, int> id;
	double sigma_ax, sigma_ay;
	std::vector<vxx::base_track_t> connect_cand;
	int count = 0;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		//if (count % 10000 == 0) {
		//	printf("\r connect track %10d/%10d (%4.1lf%%)", count, base1.size(), count*100. / base1.size());
		//}
		count++;
		connect_cand.clear();

		if (!isfinite(itr->ax))continue;
		if (!isfinite(itr->ay))continue;
		if (!isfinite(itr->x))continue;
		if (!isfinite(itr->y))continue;

		//ay_idであってる
		int id_x = ay_id(itr->ax);
		int id_y = ay_id(itr->ay);
		auto param_min = param.cut_dax.find(id_x);
		auto param_max = param.cut_dax.find(id_x + 1);
		//daxのsigma
		{
			param_min = param.cut_dax.find(id_x);
			param_max = param.cut_dax.find(id_x + 1);

			if (param_min == param.cut_dax.end() || param_max == param.cut_dax.end()) {
				sigma_ax = (fabs(itr->ax)*param.ex_dax.first + param.ex_dax.second);
			}
			else {
				sigma_ax = ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(fabs(itr->ax) - param_min->first*0.1) + param_min->second);
			}
		}
		//dayのsigma
		{
			param_min = param.cut_day.find(id_y);
			param_max = param.cut_day.find(id_y + 1);

			if (param_min == param.cut_day.end() || param_max == param.cut_day.end()) {
				sigma_ay = (fabs(itr->ay)*param.ex_day.first + param.ex_day.second);
			}
			else {
				sigma_ay = ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(fabs(itr->ay) - param_min->first*0.1) + param_min->second);
			}
		}

		//角度精度
		angle_acc_x = sigma_ax*4;
		angle_acc_y = sigma_ay*4;
		//z=0に外挿
		ex_x = itr->ax*(-1)*itr->z + itr->x;
		ex_y = itr->ay*(-1)*itr->z + itr->y;
		//pos_center+-(angle_acc*gap)*sigma
		x_min = ex_x - angle_acc_x * fabs(itr->z);
		x_max = ex_x + angle_acc_x * fabs(itr->z);
		y_min = ex_y - angle_acc_y * fabs(itr->z);
		y_max = ex_y + angle_acc_y * fabs(itr->z);
		//ang_center+-(angle_acc)*sigma
		ax_min = itr->ax - angle_acc_x;
		ax_max = itr->ax + angle_acc_x;
		ay_min = itr->ay - angle_acc_y;
		ay_max = itr->ay + angle_acc_y;

		//hash idに変換
		i_x_min = (x_min - std::get<0>(min)) / hash_pos;
		i_x_max = (x_max - std::get<0>(min)) / hash_pos;
		i_y_min = (y_min - std::get<1>(min)) / hash_pos;
		i_y_max = (y_max - std::get<1>(min)) / hash_pos;
		i_ax_min = (ax_min - std::get<2>(min)) / hash_ang;
		i_ax_max = (ax_max - std::get<2>(min)) / hash_ang;
		i_ay_min = (ay_min - std::get<3>(min)) / hash_ang;
		i_ay_max = (ay_max - std::get<3>(min)) / hash_ang;

		if (abs(i_x_max - i_x_min) > 100)continue;
		if (abs(i_y_max - i_y_min) > 100)continue;
		if (abs(i_ax_max - i_ax_min) > 100)continue;
		if (abs(i_ay_max - i_ay_min) > 100)continue;
		//double hash_pos = 2000;
		//double hash_ang = 0.25;

		//hash mapの中から該当trackを探す
		for (int ix = i_x_min; ix <= i_x_max; ix++) {
			for (int iy = i_y_min; iy <= i_y_max; iy++) {
				for (int iax = i_ax_min; iax <= i_ax_max; iax++) {
					for (int iay = i_ay_min; iay <= i_ay_max; iay++) {
						std::get<0>(id) = ix;
						std::get<1>(id) = iy;
						std::get<2>(id) = iax;
						std::get<3>(id) = iay;
						if (base0.count(id) == 0)continue;
						auto range = base0.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							connect_cand.push_back(res->second);
						}
					}
				}
			}
		}
		connect_cand = connect_track(*itr, connect_cand,param);
		if (connect_cand.size() == 0)continue;
		for (auto itr2 = connect_cand.begin(); itr2 != connect_cand.end(); itr2++) {
			connected.push_back(std::make_pair((*itr2), (*itr)));
		}
	}
	//printf("\r connect track %10d/%10d (%4.1lf%%)\n", count, base1.size(), count*100. / base1.size());


	connected = connect_track_unique(connected, param);

	return connected;

}

std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connect_track_unique(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&track_pair, cut_param &param) {
	std::multimap<int, std::pair<vxx::base_track_t, vxx::base_track_t>> link_multi;
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> sel0;
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> sel1;
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>link_v;

	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		link_multi.insert(std::make_pair(itr->first.rawid, *itr));
	}
	for (auto itr = link_multi.begin(); itr != link_multi.end();itr++) {
		link_v.clear();
		auto range = link_multi.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			link_v.push_back(res->second);
		}

		sel0.push_back(unique_linklet(link_v, param));

		itr = std::next(itr, link_multi.count(itr->first) - 1);
	}
	link_multi.clear();
	for (auto itr = sel0.begin(); itr != sel0.end(); itr++) {
		link_multi.insert(std::make_pair(itr->second.rawid, *itr));
	}
	for (auto itr = link_multi.begin(); itr != link_multi.end(); itr++) {
		link_v.clear();
		auto range = link_multi.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			link_v.push_back(res->second);
		}

		sel1.push_back(unique_linklet(link_v, param));

		itr = std::next(itr, link_multi.count(itr->first) - 1);
	}

	//printf("base0 multi del %d-->%d\n", track_pair.size(), sel0.size());
	//printf("base1 multi del %d-->%d\n", sel0.size(), sel1.size());
	return sel1;
}

std::pair<vxx::base_track_t, vxx::base_track_t> unique_linklet(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&multi, cut_param &param) {
	std::map<double, std::pair<vxx::base_track_t, vxx::base_track_t> > link_chi2;

	double x, y, angle, chi2,sigma[4] = {};
	double  d_pos_r, d_pos_l, d_ang_r, d_ang_l;
	int id_ang;

	for (auto itr = multi.begin(); itr != multi.end(); itr++) {
		angle = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);
		id_ang = ang_id(angle);
		auto param_min = param.cut_dax.find(id_ang);
		auto param_max = param.cut_dax.find(id_ang + 1);

		x = angle;
		if (angle < 0.01) {
			d_ang_r = itr->second.ay - itr->first.ay;
			d_ang_l = itr->second.ax - itr->first.ax;
			d_pos_r = itr->second.y - itr->first.y - (itr->first.ay + itr->second.ay) / 2 * (itr->second.z - itr->first.z);
			d_pos_l = itr->second.x - itr->first.x - (itr->first.ax + itr->second.ax) / 2 * (itr->second.z - itr->first.z);
		}
		else {
			d_ang_r = ((itr->second.ax - itr->first.ax)*itr->first.ax + (itr->second.ay - itr->first.ay)*itr->first.ay) / angle;
			d_ang_l = ((itr->second.ax - itr->first.ax)*itr->first.ay - (itr->second.ay - itr->first.ay)*itr->first.ax) / angle;
			Calc_position_difference(itr->first, itr->second, d_pos_r, d_pos_l);
		}
		//dalのカット
		{
			param_min = param.cut_dal.find(id_ang);
			param_max = param.cut_dal.find(id_ang + 1);
			y = fabs(d_ang_l);

			if (param_min == param.cut_dal.end() || param_max == param.cut_dal.end()) {
				sigma[0] = y / (x*param.ex_dal.first + param.ex_dal.second);
			}
			else {
				sigma[0] = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
			}
		}
		//darのカット
		{
			param_min = param.cut_dar.find(id_ang);
			param_max = param.cut_dar.find(id_ang + 1);
			y = fabs(d_ang_r);

			if (param_min == param.cut_dar.end() || param_max == param.cut_dar.end()) {
				sigma[1] = y / (x*param.ex_dar.first + param.ex_dar.second);
			}
			else {
				sigma[1] = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
			}
		}


		//dlのカット
		{
			param_min = param.cut_dl.find(id_ang);
			param_max = param.cut_dl.find(id_ang + 1);
			y = fabs(d_pos_l);

			if (param_min == param.cut_dl.end() || param_max == param.cut_dl.end()) {
				sigma[2] = y / (x*param.ex_dl.first + param.ex_dl.second);
			}
			else {
				sigma[2] = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
			}
		}
		//drのカット
		{
			param_min = param.cut_dr.find(id_ang);
			param_max = param.cut_dr.find(id_ang + 1);
			y = fabs(d_pos_r);

			if (param_min == param.cut_dr.end() || param_max == param.cut_dr.end()) {
				sigma[3] = y / (x*param.ex_dr.first + param.ex_dr.second);
			}
			else {
				sigma[3] = y / ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);
			}
		}
		chi2 = pow(sigma[0], 2) + pow(sigma[1], 2) + pow(sigma[2], 2) + pow(sigma[3], 2);
		link_chi2.insert(std::make_pair(chi2, *itr));

	}
	return link_chi2.begin()->second;
}





int ax_id(double angle) {
	if (fabs(angle) < 1) {
		return int(fabs(angle) / 0.1);
	}
	else {
		return int(fabs(angle - 1) / 0.2 + 10);
	}
}
int ay_id(double angle) {
	return int(fabs(angle) / 0.1);
}
int ang_id(double angle) {
	return int(fabs(angle) / 0.1);
}


void cut_param::calc_ex_dx() {
	double x = 0, y = 0, xy = 0, x2 = 0;
	int n = 0;

	for (auto itr = cut_dx.rbegin(); itr != cut_dx.rend() && n < 5; itr++) {
		x += itr->first*0.1;
		y += itr->second;
		xy += itr->first*0.1*itr->second;
		x2 += itr->first*0.1*itr->first*0.1;
		n++;
	}

	ex_dx.first = (n*xy - x * y) / (n*x2 - x * x);
	ex_dx.second = (x2*y - xy * x) / (n*x2 - x * x);
	if (ex_dx.first < 0) {
		ex_dx.first = 0;
		ex_dx.second = y / n;
	}

}
void cut_param::calc_ex_dax() {
	double x = 0, y = 0, xy = 0, x2 = 0;
	int n = 0;

	for (auto itr = cut_dax.rbegin(); itr != cut_dax.rend() && n < 5; itr++) {
		x += itr->first*0.1;
		y += itr->second;
		xy += itr->first*0.1*itr->second;
		x2 += itr->first*0.1*itr->first*0.1;
		n++;
	}

	ex_dax.first = (n*xy - x * y) / (n*x2 - x * x);
	ex_dax.second = (x2*y - xy * x) / (n*x2 - x * x);
	if (ex_dax.first < 0) {
		ex_dax.first = 0;
		ex_dax.second = y / n;
	}

}
void cut_param::calc_ex_dy() {
	double x = 0, y = 0, xy = 0, x2 = 0;
	int n = 0;

	for (auto itr = cut_dy.rbegin(); itr != cut_dy.rend() && n < 5; itr++) {
		x += itr->first*0.1;
		y += itr->second;
		xy += itr->first*0.1*itr->second;
		x2 += itr->first*0.1*itr->first*0.1;
		n++;
	}

	ex_dy.first = (n*xy - x * y) / (n*x2 - x * x);
	ex_dy.second = (x2*y - xy * x) / (n*x2 - x * x);
	if (ex_dy.first < 0) {
		ex_dy.first = 0;
		ex_dy.second = y / n;
	}

}
void cut_param::calc_ex_day() {
	double x = 0, y = 0, xy = 0, x2 = 0;
	int n = 0;

	for (auto itr = cut_day.rbegin(); itr != cut_day.rend() && n < 5; itr++) {
		x += itr->first*0.1;
		y += itr->second;
		xy += itr->first*0.1*itr->second;
		x2 += itr->first*0.1*itr->first*0.1;
		n++;
	}

	ex_day.first = (n*xy - x * y) / (n*x2 - x * x);
	ex_day.second = (x2*y - xy * x) / (n*x2 - x * x);
	if (ex_day.first < 0) {
		ex_day.first = 0;
		ex_day.second = y / n;
	}

}

void cut_param::calc_ex_dar() {
	double x = 0, y = 0, xy = 0, x2 = 0;
	int n = 0;

	for (auto itr = cut_dar.rbegin(); itr != cut_dar.rend() && n < 5; itr++) {
		x += itr->first*0.1;
		y += itr->second;
		xy += itr->first*0.1*itr->second;
		x2 += itr->first*0.1*itr->first*0.1;
		n++;
	}

	ex_dar.first = (n*xy - x * y) / (n*x2 - x * x);
	ex_dar.second = (x2*y - xy * x) / (n*x2 - x * x);
	if (ex_dar.first < 0) {
		ex_dar.first = 0;
		ex_dar.second = y / n;
	}

}
void cut_param::calc_ex_dr() {
	double x = 0, y = 0, xy = 0, x2 = 0;
	int n = 0;

	for (auto itr = cut_dr.rbegin(); itr != cut_dr.rend() && n < 5; itr++) {
		x += itr->first*0.1;
		y += itr->second;
		xy += itr->first*0.1*itr->second;
		x2 += itr->first*0.1*itr->first*0.1;
		n++;
	}

	ex_dr.first = (n*xy - x * y) / (n*x2 - x * x);
	ex_dr.second = (x2*y - xy * x) / (n*x2 - x * x);
	if (ex_dr.first < 0) {
		ex_dr.first = 0;
		ex_dr.second = y / n;
	}

}
void cut_param::calc_ex_dal() {
	double x = 0, y = 0, xy = 0, x2 = 0;
	int n = 0;

	for (auto itr = cut_dal.rbegin(); itr != cut_dal.rend() && n < 5; itr++) {
		x += itr->first*0.1;
		y += itr->second;
		xy += itr->first*0.1*itr->second;
		x2 += itr->first*0.1*itr->first*0.1;
		n++;
	}

	ex_dal.first = (n*xy - x * y) / (n*x2 - x * x);
	ex_dal.second = (x2*y - xy * x) / (n*x2 - x * x);
	if (ex_dal.first < 0) {
		ex_dal.first = 0;
		ex_dal.second = y / n;
	}

}
void cut_param::calc_ex_dl() {
	double x = 0, y = 0, xy = 0, x2 = 0;
	int n = 0;

	for (auto itr = cut_dl.rbegin(); itr != cut_dl.rend() && n < 5; itr++) {
		x += itr->first*0.1;
		y += itr->second;
		xy += itr->first*0.1*itr->second;
		x2 += itr->first*0.1*itr->first*0.1;
		n++;
	}

	ex_dl.first = (n*xy - x * y) / (n*x2 - x * x);
	ex_dl.second = (x2*y - xy * x) / (n*x2 - x * x);
	if (ex_dl.first < 0) {
		ex_dl.first = 0;
		ex_dl.second = y / n;
	}

}

bool cut_param::judge_connect(vxx::base_track_t &t0, vxx::base_track_t &t1) {

	///////x-y方向のカット
	//int ix, iy;
	//ix = ang_id(t0.ax);
	//if (ix >= id_max) {
	//	
	//}


	///////r-l方向のカット


	//double angle, d_pos_r, d_pos_l, d_ang_r, d_ang_l;
	//double all_pos_r, all_pos_l, all_ang_r, all_ang_l;
	//angle = sqrt(t1.ax*t1.ax + t1.ay*t1.ay);

	//all_ang_r = 0.15*angle + 0.1;
	//all_ang_l = 0.1;
	//all_pos_r = (0.1*angle + 0.15)*(t1.z - t2.z);
	//all_pos_l = 0.1*(t1.z - t2.z);


	//d_ang_r = ((t2.ax - t1.ax)*t1.ax + (t2.ay - t1.ay)*t1.ay);
	//d_ang_l = ((t2.ax - t1.ax)*t1.ay - (t2.ay - t1.ay)*t1.ax);
	//if (fabs(d_ang_r) > fabs(all_ang_r)*angle)return false;
	//if (fabs(d_ang_l) > fabs(all_ang_l)*angle)return false;

	//Calc_position_difference(t1, t2, d_pos_r, d_pos_l);
	//if (fabs(d_pos_r) > fabs(all_pos_r))return false;
	//if (fabs(d_pos_l) > fabs(all_pos_l))return false;

	//return true;




}

std::vector<corrmap0::Corrmap> cut_corrmap(std::vector<corrmap0::Corrmap>&corr) {
	std::vector<corrmap0::Corrmap> ret;
	int ix_min, ix_max, iy_min, iy_max;
	ix_min = 50;
	iy_min = 50;
	ix_max = 60;
	iy_max = 60;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (ix_min <= itr->notuse_i[1] && itr->notuse_i[1] <= ix_max) {
			if (iy_min <= itr->notuse_i[2] && itr->notuse_i[2] <= iy_max) {
				ret.push_back(*itr);
			}
		}
	}
	return ret;

}
/*
void affine_fine_tune(std::pair<corrmap0::Corrmap, std::vector<vxx::base_track_t>> &base1,std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t>&base0, double &hash_pos, double  &hash_ang, std::tuple<double, double, double, double> &min, cut_param &param,double connect_sigma,double gap) {

	//base1
	double ax, ay, x, y, z;
	double ex_x, ex_y;
	double x_min, x_max, y_min, y_max;
	double ax_min, ax_max, ay_min, ay_max;
	double angle_acc_x, angle_acc_y;
	int i_ax_min, i_ax_max, i_ay_min, i_ay_max;
	int i_x_min, i_x_max, i_y_min, i_y_max;
	double all_dx, all_dy;
	std::tuple<int, int, int, int> id;

	std::vector<vxx::base_track_t> connect_cand;
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> connected;
	connected.reserve(base1.second.size() );

	int count = 0;
	for (auto itr = base1.second.begin(); itr != base1.second.end(); itr++) {
		//if (count % 10000 == 0) {
		//	printf("\r connect track %10d/%10d (%4.1lf%%)", count, base1.size(), count*100. / base1.size());
		//}
		count++;

		connect_cand.clear();

		x = itr->x*base1.first.position[0] + itr->y*base1.first.position[1] + base1.first.position[4];
		y = itr->x*base1.first.position[2] + itr->y*base1.first.position[3] + base1.first.position[5];
		z = gap * base1.first.dz;

		ax = (itr->x*base1.first.angle[0] + itr->y*base1.first.angle[1])*base1.first.notuse_d[0] + base1.first.angle[4];
		ay = (itr->x*base1.first.angle[2] + itr->y*base1.first.angle[3]) * base1.first.notuse_d[0] + base1.first.angle[5];



		//角度精度
		angle_acc_x = param.Get_sigma_ax(ax)*connect_sigma;
		angle_acc_y = param.Get_sigma_ay(ay)*connect_sigma;
		//位置ずれ接続allowance
		all_dx = param.Get_sigma_px(ax)*connect_sigma;
		all_dy = param.Get_sigma_py(ay)*connect_sigma;

		//z=0に外挿
		ex_x = ax*(-1)*z + x;
		ex_y = ay*(-1)*z + y;
		//pos_center+-(angle_acc*gap)*sigma
		//位置ずれ使ってないから広め
		x_min = ex_x - angle_acc_x * fabs(z)*2*2;
		x_max = ex_x + angle_acc_x * fabs(z)*2*2;
		y_min = ex_y - angle_acc_y * fabs(z)*2*2;
		y_max = ex_y + angle_acc_y * fabs(z)*2*2;
		//ang_center+-(angle_acc)*sigma
		ax_min = ax - angle_acc_x;
		ax_max = ax + angle_acc_x;
		ay_min = ay - angle_acc_y;
		ay_max = ay + angle_acc_y;

		//hash idに変換
		i_x_min = (x_min - std::get<0>(min)) / hash_pos;
		i_x_max = (x_max - std::get<0>(min)) / hash_pos;
		i_y_min = (y_min - std::get<1>(min)) / hash_pos;
		i_y_max = (y_max - std::get<1>(min)) / hash_pos;
		i_ax_min = (ax_min - std::get<2>(min)) / hash_ang;
		i_ax_max = (ax_max - std::get<2>(min)) / hash_ang;
		i_ay_min = (ay_min - std::get<3>(min)) / hash_ang;
		i_ay_max = (ay_max - std::get<3>(min)) / hash_ang;

		//hash mapの中から該当trackを探す
		for (int ix = i_x_min; ix <= i_x_max; ix++) {
			for (int iy = i_y_min; iy <= i_y_max; iy++) {
				for (int iax = i_ax_min; iax <= i_ax_max; iax++) {
					for (int iay = i_ay_min; iay <= i_ay_max; iay++) {
						std::get<0>(id) = ix;
						std::get<1>(id) = iy;
						std::get<2>(id) = iax;
						std::get<3>(id) = iay;
						if (base0.count(id) == 0)continue;
						auto range = base0.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							connect_cand.push_back(res->second);
						}
					}
				}
			}
		}
		connect_cand = connect_track(*itr, connect_cand, param);
		if (connect_cand.size() == 0)continue;
		for (auto itr2 = connect_cand.begin(); itr2 != connect_cand.end(); itr2++) {
			connected.push_back(std::make_pair((*itr2), (*itr)));
		}
	}
	//printf("\r connect track %10d/%10d (%4.1lf%%)\n", count, base1.size(), count*100. / base1.size());

	//return connected;




	double x, y;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		x = itr->x;
		y = itr->y;
		itr->x = param.position[0] * x + param.position[1] * y + param.position[4];
		itr->y = param.position[2] * x + param.position[3] * y + param.position[5];
		x = itr->ax;
		y = itr->ay;
		itr->ax = (param.angle[0] * x + param.angle[1] * y)*param.notuse_d[0] + param.angle[4];
		itr->ay = (param.angle[2] * x + param.angle[3] * y)*param.notuse_d[0] + param.angle[5];
		itr->z = gap + param.dz;
	}


}
*/
double cut_param::Get_sigma_ax(const double &ax) {

	double x = fabs(ax);

	//ay_idであってる
	int id = ay_id(x);
	auto param_min = cut_dax.find(id);
	auto param_max = cut_dax.find(id + 1);

	if (param_min == cut_dax.end() || param_max == cut_dax.end()) {
		return (x*ex_dax.first + ex_dax.second);

	}
	return ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);

}
double cut_param::Get_sigma_ay(const double &ay) {

	double x = fabs(ay);

	//ay_idであってる
	int id = ay_id(x);
	auto param_min = cut_day.find(id);
	auto param_max = cut_day.find(id + 1);

	if (param_min == cut_day.end() || param_max == cut_day.end()) {
		return (x*ex_day.first + ex_day.second);

	}
	return ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);

}
double cut_param::Get_sigma_px(const double &ax) {

	double x = fabs(ax);

	//ay_idであってる
	int id = ay_id(x);
	auto param_min = cut_dx.find(id);
	auto param_max = cut_dx.find(id + 1);

	if (param_min == cut_dx.end() || param_max == cut_dx.end()) {
		return (x*ex_dx.first + ex_dx.second);

	}
	return ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);

}
double cut_param::Get_sigma_py(const double &ay) {

	double x = fabs(ay);

	//ay_idであってる
	int id = ay_id(x);
	auto param_min = cut_dy.find(id);
	auto param_max = cut_dy.find(id + 1);

	if (param_min == cut_dy.end() || param_max == cut_dy.end()) {
		return (x*ex_dy.first + ex_dy.second);

	}
	return ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);

}
double cut_param::Get_sigma_ar(const double &angle) {

	double x = fabs(angle);

	int id = ang_id(x);
	auto param_min = cut_dar.find(id);
	auto param_max = cut_dar.find(id + 1);

	if (param_min == cut_dar.end() || param_max == cut_dar.end()) {
		return (x*ex_dar.first + ex_dar.second);

	}
	return ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);

}
double cut_param::Get_sigma_al(const double &angle) {

	double x = fabs(angle);

	int id = ang_id(x);
	auto param_min = cut_dal.find(id);
	auto param_max = cut_dal.find(id + 1);

	if (param_min == cut_dal.end() || param_max == cut_dal.end()) {
		return (x*ex_dal.first + ex_dal.second);
	}
	return ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);

}
double cut_param::Get_sigma_pr(const double &angle) {

	double x = fabs(angle);

	int id = ang_id(x);
	auto param_min = cut_dr.find(id);
	auto param_max = cut_dr.find(id + 1);

	if (param_min == cut_dr.end() || param_max == cut_dr.end()) {
		return (x*ex_dr.first + ex_dr.second);
	}
	return ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);

}
double cut_param::Get_sigma_pl(const double &angle) {

	double x = fabs(angle);

	int id = ang_id(x);
	auto param_min = cut_dl.find(id);
	auto param_max = cut_dl.find(id + 1);

	if (param_min == cut_dl.end() || param_max == cut_dl.end()) {
		return (x*ex_dl.first + ex_dl.second);
	}
	return ((param_max->second - param_min->second) / (param_max->first*0.1 - param_min->first*0.1)*(x - param_min->first*0.1) + param_min->second);

}


