#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>
#include <omp.h>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <string>
class microtrack_inf {
public:
	int zone, pos, col, row, isg, ph, pixelnum, hitnum;
};

std::set<std::tuple<int, int, int>> read_base_id(std::string file_path, std::string filename, int num, int pl);
void fvxx_file_divide(std::set<std::tuple<int, int, int>>base_id[8], std::set<int> micro_id[8]);
void read_micro_inf(std::string file_path, std::string filename, int num, int pos, std::set<int>&micro_id, std::map<int, microtrack_inf>&micro_inf);

void read_micro(std::string file_path, std::string filename, int num, int pos, int zone0, int zone1, int area, std::multimap<std::pair<int, int>, int>base_id[2][4], std::vector<vxx::micro_track_t>micro[2][4]);
void write_fvxx_inf(std::string filename, std::set<std::tuple<int, int, int>>&base_id, std::map<int, microtrack_inf>&m_inf_pos1, std::map<int, microtrack_inf>&m_inf_pos2);
int use_thread(double ratio, bool output);

int main(int argc, char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-bvxx-folder pl area fvxx-folder out-file-path\n");
		fprintf(stderr, "*************[[[ caution ]]]***********\n");
		fprintf(stderr, "this program depend on zone structure\n");
		exit(1);
	}
	std::string file_in_bvxx_path = argv[1];
	int pl = std::stoi(argv[2]);
	int area = std::stoi(argv[3]);
	std::string path_in_fvxx = argv[4];
	std::string file_out_path = argv[5];
	double CPU_ratio = 0.5;

	//microtrackはbasetrack中のpos,zone,rawidでuniqueになる。
	std::set<std::tuple<int, int, int>>base_id[8];
#pragma omp parallel for num_threads(std::min(8,use_thread(CPU_ratio,false))) schedule(dynamic,1)
	for (int i = 0; i < 8; i++) {
		if (int(i / 4) == 0) {
			base_id[i] = read_base_id(file_in_bvxx_path, "thick", i % 4, pl);
		}
		else if (int(i / 4) == 1) {
			base_id[i] = read_base_id(file_in_bvxx_path, "thin", i % 4, pl);
		}
		else {
			fprintf(stderr, "id = %d > 7, id error id\n", i);
			exit(1);
		}
	}

	std::set <int> micro_id[8];
	fvxx_file_divide(base_id, micro_id);

	std::map<int,microtrack_inf> micro_inf[8];

	//並列処理 fvxx読み込み-->情報の抽出
#pragma omp parallel for num_threads(std::min(8,use_thread(CPU_ratio,false))) schedule(dynamic,1)
	for (int i = 0; i < 8; i++) {
		if (i == 0)	read_micro_inf(path_in_fvxx, "thick", 0, pl * 10 + 1, micro_id[i], micro_inf[i]);
		else if (i == 1)read_micro_inf(path_in_fvxx, "thick", 0, pl * 10 + 2, micro_id[i], micro_inf[i]);
		else if (i == 2)read_micro_inf(path_in_fvxx, "thick", 1, pl * 10 + 1, micro_id[i], micro_inf[i]);
		else if (i == 3)read_micro_inf(path_in_fvxx, "thick", 1, pl * 10 + 2, micro_id[i], micro_inf[i]);
		else if (i == 4)read_micro_inf(path_in_fvxx, "thin", 0, pl * 10 + 1, micro_id[i], micro_inf[i]);
		else if (i == 5)read_micro_inf(path_in_fvxx, "thin", 0, pl * 10 + 2, micro_id[i], micro_inf[i]);
		else if (i == 6)read_micro_inf(path_in_fvxx, "thin", 1, pl * 10 + 1, micro_id[i], micro_inf[i]);
		else if (i == 7)read_micro_inf(path_in_fvxx, "thin", 1, pl * 10 + 2, micro_id[i], micro_inf[i]);
		else {
			fprintf(stderr, "id = %d > 7, id error id\n", i);
			exit(1);
		}
	}

	for (int i = 0; i < 8; i++) {
		std::stringstream file_out_file;
		if (i / 4 == 0) {
			file_out_file << file_out_path << "\\micro_inf_thick_" << std::setw(1) << i % 4;
		}
		else if (i / 4 == 1) {
			file_out_file << file_out_path << "\\micro_inf_thin_" << std::setw(1) << i % 4;
		}
		else {
			fprintf(stderr, "id = %d > 7, id error id\n", i);
			exit(1);
		}
		//base_id[i]に対応するmicro_infを抽出。出力
		if (i == 0)write_fvxx_inf(file_out_file.str(), base_id[i], micro_inf[0], micro_inf[1]);
		if (i == 1)write_fvxx_inf(file_out_file.str(), base_id[i], micro_inf[0], micro_inf[3]);
		if (i == 2)write_fvxx_inf(file_out_file.str(), base_id[i], micro_inf[2], micro_inf[1]);
		if (i == 3)write_fvxx_inf(file_out_file.str(), base_id[i], micro_inf[2], micro_inf[3]);
		if (i == 4)write_fvxx_inf(file_out_file.str(), base_id[i], micro_inf[4], micro_inf[5]);
		if (i == 5)write_fvxx_inf(file_out_file.str(), base_id[i], micro_inf[4], micro_inf[7]);
		if (i == 6)write_fvxx_inf(file_out_file.str(), base_id[i], micro_inf[6], micro_inf[5]);
		if (i == 7)write_fvxx_inf(file_out_file.str(), base_id[i], micro_inf[6], micro_inf[7]);
	}

}


std::set<std::tuple<int, int, int>> read_base_id(std::string file_path, std::string filename, int num, int pl) {
	std::stringstream bvxxname;
	bvxxname << file_path << "\\b" << std::setw(3) << std::setfill('0') << pl << "_" << filename << "_" << std::setw(1) << num << ".sel.vxx";

	std::set<std::tuple<int, int, int>> ret;

	//bvxx 176 byte/track
	//1Mtrack=1.76GB
	int read_track_num = (1 * 1000 * 1000) / 2;
	int id = 0;
	int t_num = -1;
	std::vector<vxx::base_track_t> b;
	b.reserve(read_track_num);
	vxx::BvxxReader br;

	while (t_num != 0) {
		std::array<int, 2> index = { id*read_track_num, (id + 1)*read_track_num };//1234<=rawid<=5678であるようなものだけを読む。
		b = br.ReadAll(bvxxname.str(), pl, 0, vxx::opt::index = index);
		t_num = b.size();


		for (auto itr = b.begin(); itr != b.end(); itr++) {
			ret.insert(std::make_tuple(itr->m[0].pos, itr->m[0].zone, itr->m[0].rawid));
			ret.insert(std::make_tuple(itr->m[1].pos, itr->m[1].zone, itr->m[1].rawid));
		}
		b.clear();
		id++;
	}

	b.clear();
	b.shrink_to_fit();
	return ret;
}
void fvxx_file_divide(std::set<std::tuple<int, int, int>>base_id[8], std::set<int> micro_id[8]) {

	//0 thick0 pos1 --> area + 6 * 0, area + 6 * 1
	//1 thick0 pos2 --> area + 6 * 0, area + 6 * 2
	//2 thick1 pos1 --> area + 6 * 2, area + 6 * 3
	//3 thick1 pos2 --> area + 6 * 1, area + 6 * 3

	//4 thin0 pos1 --> area + 6 * 4, area + 6 * 5
	//5 thin0 pos2 --> area + 6 * 4, area + 6 * 6
	//6 thin1 pos1 --> area + 6 * 6, area + 6 * 7
	//7 thin1 pos2 --> area + 6 * 5, area + 6 * 7

	//area 1-6
	int id = 0, pos, zone, rawid;
	std::tuple<int, int, int> id_tuple;
	int count, all;
	for (int i = 0; i < 8; i++) {
		count = 0;
		all = base_id[i].size();
		for (auto itr = base_id[i].begin(); itr != base_id[i].end(); itr++) {
			if (count % 1000000 == 0) {
				fprintf(stderr, "\r now fvxx %d id divide %d/%d(%4.1lf%%)", i,count, all, count*100. / all);
			}
			count++;
			id_tuple = (*itr);
			pos = std::get<0>(id_tuple);
			pos = pos % 10;
			zone = std::get<1>(id_tuple);
			zone = (zone - 1) / 6;
			rawid = std::get<2>(id_tuple);
			if (pos == 1 && (zone == 0 || zone == 1)) {
				id = 0;
			}
			else if (pos == 1 && (zone == 2 || zone == 3)) {
				id = 2;
			}
			else if (pos == 1 && (zone == 4 || zone == 5)) {
				id = 4;
			}
			else if (pos == 1 && (zone == 6 || zone == 7)) {
				id = 6;
			}
			else if (pos == 2 && (zone == 0 || zone == 2)) {
				id = 1;
			}
			else if (pos == 2 && (zone == 1 || zone == 3)) {
				id = 3;
			}
			else if (pos == 2 && (zone == 4 || zone == 6)) {
				id = 5;
			}
			else if (pos == 2 && (zone == 5 || zone == 7)) {
				id = 7;
			}
			else {
				fprintf(stderr, "zone error\n");
				fprintf(stderr, "pos =%d, zone=%d\n", std::get<0>(*itr), std::get<1>(*itr));
				exit(1);
			}
			micro_id[id].insert(rawid);
		}
	}
	fprintf(stderr, "\r now fvxx id divide %d/%d(%4.1lf%%)\n", count, all, count*100. / all);
}

void read_micro_inf(std::string file_path, std::string filename, int num, int pos, std::set<int>&micro_id, std::map<int,microtrack_inf>&micro_inf) {
	
	std::stringstream fvxxname;
	fvxxname << file_path << "\\f" << std::setw(4) << std::setfill('0') << pos << "_" << filename << "_" << std::setw(1) << num << ".vxx";

	//fvxx xx byte/track
	//1Mtrack=xxGB
	int read_track_num = (1 * 1000 * 1000);
	int id = 0;
	int t_num = -1;
	std::vector<vxx::micro_track_t> m;
#pragma omp critical
	m.reserve(read_track_num);
	vxx::FvxxReader fr;
	microtrack_inf m_tmp;
	int num2, vola;
	while (t_num != 0) {
		std::array<int, 2> index = { id*read_track_num, (id + 1)*read_track_num };//1234<=rawid<=5678であるようなものだけを読む。
		m = fr.ReadAll(fvxxname.str(), pos, 0, vxx::opt::index = index);
		t_num = m.size();

		for (auto itr = m.begin(); itr != m.end(); itr++) {
			if (micro_id.count(itr->rawid) == 0)continue;
			num2 = itr->px;
			vola = itr->py;

			m_tmp.zone = itr->zone;
			m_tmp.pos = itr->pos;
			m_tmp.col = itr->col;
			m_tmp.row = itr->row;
			m_tmp.isg = itr->isg;
			m_tmp.pixelnum = num2 >> 5;
			m_tmp.ph = num2 & 0x1f;
			m_tmp.hitnum = vola;
			micro_inf.insert(std::make_pair(itr->rawid, m_tmp));
		}
		m.clear();
		id++;
	}
	printf("microtrack num %d-->%d\n", micro_id.size(), micro_inf.size());
	m.clear();
	m.shrink_to_fit();

	return;
}
void write_fvxx_inf(std::string filename, std::set<std::tuple<int, int, int>>&base_id, std::map<int,microtrack_inf>&m_inf_pos1, std::map<int,microtrack_inf>&m_inf_pos2) {
	
	std::vector<microtrack_inf> m_inf;
	m_inf.reserve(base_id.size());
	std::tuple<int, int, int> id_tuple;
	int pos, zone, rawid;
	int64_t count = 0;
	int64_t max = base_id.size();

	for (auto itr = base_id.begin(); itr != base_id.end(); itr++) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r now mcirotrack inf extract %d/%d(%4.1lf%%)",  count, max, count*100. / max);
		}
		count++;




		id_tuple = (*itr);
		pos = std::get<0>(id_tuple);
		pos = pos % 10;
		zone = std::get<1>(id_tuple);
		rawid = std::get<2>(id_tuple);
		if (pos == 1) {
			auto res = m_inf_pos1.find(rawid);
			if (res == m_inf_pos1.end()) {
				fprintf(stderr, "pos=%d zone=%d rawid=%d not found\n", std::get<0>(id_tuple), std::get<1>(id_tuple), std::get<2>(id_tuple));
				exit(1);
			}
			res->second.zone = zone;
			m_inf.push_back(res->second);
		}
		else if (pos == 2) {
			auto res = m_inf_pos2.find(rawid);
			if (res == m_inf_pos2.end()) {
				fprintf(stderr, "pos=%d zone=%d rawid=%d not found\n", std::get<0>(id_tuple), std::get<1>(id_tuple), std::get<2>(id_tuple));
				exit(1);
			}
			res->second.zone = zone;
			m_inf.push_back(res->second);
		}
		else {
			fprintf(stderr, "pos error\n");
			fprintf(stderr, "pos=%d zone=%d rawid=%d not found\n", std::get<0>(id_tuple), std::get<1>(id_tuple), std::get<2>(id_tuple));
		}

	}
	fprintf(stderr, "\r now mcirotrack inf extract %d/%d(%4.1lf%%)\n", count, max, count*100. / max);

	printf("mcirotrack inf extract %d --> %d\n", base_id.size(), m_inf.size());
	
	
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (m_inf.size() == 0) {
		fprintf(stderr, "target file ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	count = 0;
	 max = m_inf.size();
	for (int i = 0; i < m_inf.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& m_inf[i], sizeof(microtrack_inf));
		//printf("%d %d %d %d %d %d\n", m_inf[i].col, m_inf[i].row, m_inf[i].isg, m_inf[i].ph, m_inf[i].pixelnum, m_inf[i].hitnum);
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();
}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
