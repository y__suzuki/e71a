#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include <functions.hpp>
#include <VxxReader.h>

std::vector<vxx::micro_track_t> base2micro(std::vector<vxx::base_track_t>&base);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg in-bvxx pl out-fvxx\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	std::string file_out_fvxx = argv[3];
	int pl = std::stoi(argv[2]);

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);
	std::vector<vxx::micro_track_t>micro = base2micro(base);
	vxx::FvxxWriter fw;
	fw.Write(file_out_fvxx, pl * 10 + 2, 0, micro);

}
std::vector<vxx::micro_track_t> base2micro(std::vector<vxx::base_track_t>&base) {
	std::vector<vxx::micro_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		vxx::micro_track_t m;
		m.ax = itr->m[1].ax;
		m.ay = itr->m[1].ay;
		m.x = itr->x + (itr->m[1].z - itr->m[0].z)*itr->ax - 30 * itr->m[1].ax;
		m.y = itr->y + (itr->m[1].z - itr->m[0].z)*itr->ay - 30 * itr->m[1].ay;
		m.col = itr->m[1].col;
		m.isg = itr->m[1].isg;
		m.ph = itr->m[1].ph;
		m.pos = itr->m[1].pos;
		m.px = 0;
		m.py = 0;
		m.rawid = itr->m[1].rawid;
		m.row = itr->m[1].row;
		m.z = 0;
		m.z1 = itr->m[1].z;
		m.z2 = itr->m[1].z - 60;
		m.zone = itr->m[1].zone;
		ret.push_back(m);
	}
	return ret;
}