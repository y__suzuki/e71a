#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

bool fit_affine(std::vector<netscan::linklet_t> link, double &rot, double &shr, double &dx, double &dy, int mode);
std::vector<netscan::linklet_t> linklet_extra(std::vector<netscan::linklet_t> link);
std::vector<netscan::linklet_t> linklet_add_param(std::vector<netscan::linklet_t> link, double dx, double dy, double dz);
void Calc_least_square(std::vector<double>x, std::vector<double> y, double &slope, double &intercept);
void calc_shift(std::vector<netscan::linklet_t>&link, double &dx, double &dy, double &dz);
void calc_rot(std::vector<netscan::linklet_t>&link, double &rot);
std::vector<netscan::linklet_t> anglecut(std::vector<netscan::linklet_t>&link, double thr);
std::vector<netscan::linklet_t> linklet_add_param(std::vector<netscan::linklet_t> link, double rot, double shr);
void calc_shrink(std::vector<netscan::linklet_t>&link, double &shr);
void PrintParam(double rot, double shr, double shift_x, double shift_y, double shift_z);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-link fixed-param-allfile-out-link \n");
		exit(1);
	}
	std::string file_in_link = argv[1];
	std::string file_out_param_global = argv[2];
	std::string file_out_link = argv[3];


	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_bin(file_in_link, link);

	double rot = 0, shr = 1, dx =0, dy = 0, dz = 0;
	calc_shift(link, dx, dy, dz);
	std::vector<netscan::linklet_t> link_tmp = linklet_add_param(link, dx, dy, dz);

	std::vector<netscan::linklet_t>link_cut = anglecut(link_tmp, 0.5);

	calc_rot(link_cut, rot);
	link_tmp = linklet_add_param(link_cut, rot,shr);
	calc_shrink(link_tmp, shr);

	link_tmp = linklet_add_param(link, rot, shr);
	calc_shift(link_tmp, dx, dy, dz);
	std::vector<netscan::linklet_t>link_out= linklet_add_param(link_tmp, dx, dy, dz);

	//std::vector<netscan::linklet_t> link_ex = linklet_extra(link_tmp);
	//fit_affine(link_ex, rot, shr, dx, dy, 3);
	printf("\n-----------------------\n");
	printf("rotation %.4lf\n", rot);
	printf("shrink %.4lf\n", shr);
	printf("x shift %.1lf\n", dx);
	printf("y shift %.1lf\n", dy);
	printf("z shift %.1lf\n", dz);
	printf("-----------------------\n");

	PrintParam(rot, shr, dx, dy, dz);

	netscan::write_linklet_bin(file_out_link, link_out);
}
std::vector<netscan::linklet_t> linklet_extra(std::vector<netscan::linklet_t> link) {
	std::vector<netscan::linklet_t> ret;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		itr->b[0].x = itr->b[0].x + itr->b[0].ax*(itr->b[1].z - itr->b[0].z) / 2;
		itr->b[0].y = itr->b[0].y + itr->b[0].ay*(itr->b[1].z - itr->b[0].z) / 2;
		itr->b[1].x = itr->b[1].x + itr->b[1].ax*(itr->b[0].z - itr->b[1].z) / 2;
		itr->b[1].y = itr->b[1].y + itr->b[1].ay*(itr->b[0].z - itr->b[1].z) / 2;
		ret.push_back(*itr);
	}
	return ret;
}
bool fit_affine(std::vector<netscan::linklet_t> link,double &rot,double &shr, double &dx,double &dy, int mode)
{
	//
	// k_pair.a : つなぐ元
	// k_pair.b : つなぐ先
	//

	if (link.empty()) return false;
	const int num = link.size();
	if (num == 1) mode = 0;

	double	xa1 = 0;
	double	xa2 = 0;
	double	ya1 = 0;
	double	ya2 = 0;
	double	xb1 = 0;
	double	xb2 = 0;
	double	yb1 = 0;
	double	yb2 = 0;
	double	xaya = 0;
	double	xbxa = 0;
	double	xbya = 0;
	double	ybxa = 0;
	double	ybya = 0;

	for (auto ii = link.begin(); ii != link.end(); ++ii) {
		netscan::base_track_t a = ii->b[0];
		netscan::base_track_t b= ii->b[1];
		xa1 += a.x;
		ya1 += a.y;
		xb1 += b.x;
		yb1 += b.y;
	}
	xa1 /= (double)num;
	ya1 /= (double)num;
	xb1 /= (double)num;
	yb1 /= (double)num;

	for (auto ii = link.begin(); ii != link.end(); ++ii) {
		netscan::base_track_t a = ii->b[0];
		netscan::base_track_t b = ii->b[1];

		xa2 += (a.x - xa1)*(a.x - xa1);
		ya2 += (a.y - ya1)*(a.y - ya1);
		xaya += (a.x - xa1)*(a.y - ya1);
		xbxa += (b.x - xb1)*(a.x - xa1);
		xbya += (b.x - xb1)*(a.y - ya1);
		ybxa += (b.y - yb1)*(a.x - xa1);
		ybya += (b.y - yb1)*(a.y - ya1);
	}
	xa2 /= (double)num;
	ya2 /= (double)num;
	xaya /= (double)num;
	xbxa /= (double)num;
	xbya /= (double)num;
	ybxa /= (double)num;
	ybya /= (double)num;

	if (num > 1 && (mode == 1 || mode == 3)) {
		rot = -((xbya)-(ybxa)) / ((xbxa)+(ybya));
	}

	double c = 1 / (1 + rot * rot);
	double ct = sqrt(c);
	double st = rot * ct;

	if (num > 1 && (mode == 2 || mode == 3)) {
		shr = ((xbxa + ybya)*ct - (xbya - ybxa)*st) / (xa2 + ya2);
	}
	double param[4];
	param[0] = shr * sqrt(c);
	param[1] = -shr * rot*sqrt(c);
	param[2] = shr * rot*sqrt(c);
	param[3] = shr * sqrt(c);
	dx = xb1 - (param[0] *xa1 + param[1] *ya1);
	dy = yb1 - (param[2] *xa1 + param[3] *ya1);

	return true;
}
bool shift_z(std::vector<netscan::linklet_t> link, double &dz) {

}

std::vector<netscan::linklet_t> linklet_add_param(std::vector<netscan::linklet_t> link, double dx, double dy, double dz) {
	std::vector<netscan::linklet_t> ret;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		itr->b[1].x += dx;
		itr->b[1].y += dy;
		itr->b[1].z += dz;
		itr->dx = itr->b[1].x - itr->b[0].x - (itr->b[0].ax + itr->b[1].ax) / 2 * (itr->b[1].z - itr->b[0].z);
		itr->dy = itr->b[1].y - itr->b[0].y - (itr->b[0].ay + itr->b[1].ay) / 2 * (itr->b[1].z - itr->b[0].z);

		ret.push_back(*itr);
	}
	return ret;
}

std::vector<netscan::linklet_t> linklet_add_param(std::vector<netscan::linklet_t> link, double rot,double shr) {
	std::vector<netscan::linklet_t> ret;
	double tmp_x, tmp_y;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		tmp_x = itr->b[1].x;
		tmp_y = itr->b[1].y;
		itr->b[1].x = (cos(rot)*tmp_x + sin(rot)*tmp_y)*shr;
		itr->b[1].y = (-1*sin(rot)*tmp_x + cos(rot)*tmp_y)*shr;

		tmp_x = itr->b[1].ax;
		tmp_y = itr->b[1].ay;
		itr->b[1].ax = cos(rot)*tmp_x + sin(rot)*tmp_y;
		itr->b[1].ay = -1*sin(rot)*tmp_x + cos(rot)*tmp_y;

		itr->dx = itr->b[1].x - itr->b[0].x - (itr->b[0].ax + itr->b[1].ax) / 2 * (itr->b[1].z - itr->b[0].z);
		itr->dy = itr->b[1].y - itr->b[0].y - (itr->b[0].ay + itr->b[1].ay) / 2 * (itr->b[1].z - itr->b[0].z);

		ret.push_back(*itr);
	}
	return ret;
}

void calc_shift(std::vector<netscan::linklet_t>&link, double &dx, double &dy, double &dz) {

	std::vector<double>x, y;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		x.push_back((itr->b[0].ax + itr->b[1].ax) / 2);
		y.push_back(itr->dx);
	}
	double dx0, dz0;
	Calc_least_square(x, y, dz0, dx0);
	dx0 = -1 * dx0;
	printf("dx %.1lf\n", dx0);
	printf("dz %.1lf\n", dz0);

	x.clear();
	y.clear();
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		x.push_back((itr->b[0].ay + itr->b[1].ay) / 2);
		y.push_back(itr->dy);
	}
	double dy1, dz1;
	Calc_least_square(x, y, dz1, dy1);
	dy1 = -1 * dy1;

	printf("dy %.1lf\n", dy1);
	printf("dz %.1lf\n", dz1);

	dx = dx0;
	dy = dy1;
	dz = (dz0 + dz1) / 2;

}

void calc_rot(std::vector<netscan::linklet_t>&link, double &rot) {

	std::vector<double>x, y;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		x.push_back((itr->b[0].x + itr->b[1].x) / 2);
		y.push_back(itr->dy);
	}
	double dy0, rot0;
	Calc_least_square(x, y, rot0, dy0);
	dy0 = -1 * dy0;
	printf("dy %.1lf\n", dy0);
	printf("rotation %.5lf\n", rot0);

	x.clear();
	y.clear();
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		x.push_back((itr->b[0].y + itr->b[1].y) / 2);
		y.push_back(itr->dx);
	}
	double dx1, rot1;
	Calc_least_square(x, y, rot1, dx1);
	dx1 = -1 * dx1;
	rot1 = -1 * rot1;
	printf("dx %.1lf\n", dx1);
	printf("rotation %.5lf\n", rot1);

	rot = (rot1 + rot0) / 2;

}

void calc_shrink(std::vector<netscan::linklet_t>&link, double &shr) {

	std::vector<double>x, y;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		x.push_back((itr->b[0].x + itr->b[1].x) / 2);
		y.push_back(itr->dx);
	}
	double dx0, shr0;
	Calc_least_square(x, y, shr0, dx0);
	dx0 = -1 * dx0;
	shr0 += 1;
	shr0 = 1 / shr0;
	printf("dx %.1lf\n", dx0);
	printf("shrink %.5lf\n", shr0);

	x.clear();
	y.clear();
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		x.push_back((itr->b[0].y + itr->b[1].y) / 2);
		y.push_back(itr->dy);
	}
	double dy1, shr1;
	Calc_least_square(x, y, shr1, dy1);
	shr1 += 1;
	shr1 = 1 / shr1;

	dy1 = -1 * dy1;
	printf("dy %.1lf\n", dy1);
	printf("shrink %.5lf\n", shr1);

	shr = (shr1 + shr0) / 2;

}
void Calc_least_square(std::vector<double>x, std::vector<double> y, double &slope, double &intercept) {

	double x2 = 0, y1 = 0, x1 = 0, xy = 0, n = 0;
	for (int i = 0; i < x.size(); i++) {
		x1 += x[i];
		y1 += y[i];
		xy += x[i] * y[i];
		x2 += x[i] * x[i];
		n++;
	}
	double denominator = n * x2 - x1 * x1;
	slope = (n * xy - x1 * y1) / denominator;
	intercept = (x2*y1 - x1 * xy) / denominator;
}

std::vector<netscan::linklet_t> anglecut(std::vector<netscan::linklet_t>&link, double thr) {
	std::vector<netscan::linklet_t> ret;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		double ax = (itr->b[0].ax + itr->b[1].ax) / 2;
		double ay = (itr->b[0].ay + itr->b[1].ay) / 2;
		double angle = sqrt(ax*ax + ay * ay);
		if (angle < thr) {
			ret.push_back(*itr);
		}
	}
	return ret;
}
std::map<std::pair<int, int>, std::vector<netscan::linklet_t>> link_divide(std::vector<netscan::linklet_t> link, double &pitch_x, double &pitch_y) {

}
void PrintParam(double rot, double shr, double shift_x, double shift_y, double shift_z) {
	double a, b, c, d,p,q;
	a = cos(rot)*shr;
	b = sin(rot)*shr;
	c = -1*sin(rot)*shr;
	d = cos(rot)*shr;
	printf("%.5lf %.5lf %.5lf %.5lf %.1lf %.1lf %.1lf\n", a, b, c, d, shift_x, shift_y, shift_z);
}