#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void read_CL_file(std::string filename, std::vector<std::pair<uint64_t, double>>&chain);
void write_CL_file(std::string filename, std::vector<std::pair<uint64_t, double>>&chain);

int main(int argc, char** argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg in-file out-file\n");
		exit(1);
	}
	std::string file_in = argv[1];
	std::string file_out = argv[2];
	std::vector<std::pair<uint64_t, double>> chain;
	read_CL_file(file_in, chain);
	write_CL_file(file_out, chain);
}
void read_CL_file(std::string filename, std::vector<std::pair<uint64_t, double>>&chain) {
	std::ifstream ifs(filename, std::ios::binary);
	int64_t count = 0;
	uint64_t chainid;
	double CL;

	while (ifs.read((char*)& chainid, sizeof(uint64_t))) {
		ifs.read((char*)& CL, sizeof(double));
		chain.push_back(std::make_pair(chainid, CL));
		count++;
	}
	printf("read chain=%llu\n", count);

	return;
 }
void write_CL_file(std::string filename, std::vector<std::pair<uint64_t, double>>&chain) {
	std::ofstream ofs(filename, std::ios::app | std::ios::binary);
	uint64_t count = 0;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ofs.write((char *)&itr->first, sizeof(uint64_t));
		ofs.write((char *)&itr->second, sizeof(double));
		count++;
	}
	printf("write chain=%llu\n", count);
	return;
}