#include <iostream>
class output_format_micro {
public:
	int pos, view, imager, zone, isg, ph, vph, px;
};
class output_format_base {
public:
	int pl, rawid;
	double ax, ay, x, y, z;
	output_format_micro m[2];
};

class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};

void GaussJorden(double in[4][4], double b[4], double c[4]);
double Calc_cv2(output_format_link &l);

int main(int argc, char**argv) {
	double x0, x1, ax0, ax1, z0, z1,y0,y1,ay0,ay1;
	x0 = 10;
	x1 = 1;
	ax0 = 0.3;
	ax1 = 2;
	z0 = 502;
	z1 = 50;
	y0 = 20;
	y1 = 5;
	ay0 = 1.3;
	ay1 = -2;

	double x[4] = { x0,x1,ax0,ax1 };
	double y[4] = { y0,y1,ay0,ay1 };
	double in[4][4] = { {pow(z0,3),pow(z0,2),z0,1},{pow(z1,3),pow(z1,2),z1,1},{3 * pow(z0,2),2 * z0,1,0}, {3 * pow(z1,2),2 * z1,1,0} };
	double c[4];

	output_format_link l;
	l.b[0].x = x0;
	l.b[1].x = x1;
	l.b[0].ax = ax0;
	l.b[1].ax = ax1;
	l.b[0].y = y0;
	l.b[1].y = y1;
	l.b[0].ay = ay0;
	l.b[1].ay = ay1;
	l.b[0].z = z0;
	l.b[1].z = z1;
	GaussJorden(in, x, c);
	printf("a = %g\n", c[0]);
	printf("b = %g\n", c[1]);
	printf("c = %g\n", c[2]);
	printf("d = %g\n", c[3]);
	GaussJorden(in, y, c);
	printf("a = %g\n", c[0]);
	printf("b = %g\n", c[1]);
	printf("c = %g\n", c[2]);
	printf("d = %g\n", c[3]);
	Calc_cv2(l);

}
void GaussJorden(double in[4][4], double b[4], double c[4]) {


	double a[4][5];
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 5; j++) {
			if (j < 4) {
				a[i][j] = in[i][j];
			}
			else {
				a[i][j] = b[i];
			}
		}
	}
	int N = 4;
	double p, d;         // ピボット係数、ピボット行ｘ係数
	double max, dummy;   // 最大絶対値、入れ替え時ダミー
	int s;

	//元の連立方程式をコンソール出力
   //for (int i = 0; i < N; i++) {
   //	for (int j = 0; j < N; j++)
   //		printf("%+fx%d ", a[i][j], j + 1);
   //	printf("= %+f\n", a[i][N]);
   //}

	for (int k = 0; k < N; k++) {
		// 行入れ替え
		max = 0; s = k;
		for (int j = k; j < N; j++) {
			if (fabs(a[j][k]) > max) {
				max = fabs(a[j][k]);
				s = j;
			}
		}
		if (max == 0) {
			printf("解けない！");
			exit(1);
		}
		for (int j = 0; j <= N; j++) {
			dummy = a[k][j];
			a[k][j] = a[s][j];
			a[s][j] = dummy;
		}

		// ピボット係数
		p = a[k][k];

		// ピボット行を p で除算
		for (int j = k; j < N + 1; j++)
			a[k][j] /= p;

		// ピボット列の掃き出し
		for (int i = 0; i < N; i++) {
			if (i != k) {
				d = a[i][k];
				for (int j = k; j < N + 1; j++)
					a[i][j] -= d * a[k][j];
			}
		}
	}

	// 結果出力
	for (int k = 0; k < N; k++) {
		c[k] = a[k][N];
		//printf("x%d = %f\n", k + 1, a[k][N]);
	}
}

double Calc_cv2(output_format_link &l) {
	double dz_inv = 1 / (l.b[0].z - l.b[1].z);
	double a[2], b[2], c[2], d[2];
	a[0] = -2 * (l.b[0].x - l.b[1].x)*pow(dz_inv, 3) + (l.b[0].ax + l.b[1].ax)*pow(dz_inv, 2);
	a[1] = -2 * (l.b[0].y - l.b[1].y)*pow(dz_inv, 3) + (l.b[0].ay + l.b[1].ay)*pow(dz_inv, 2);
	b[0] = 3 * (l.b[0].z + l.b[1].z)* (l.b[0].x - l.b[1].x)*pow(dz_inv, 3)
		- 3 / 2.*(l.b[0].z + l.b[1].z)*(l.b[0].ax + l.b[1].ax)*pow(dz_inv, 2)
		+ 1 / 2. * (l.b[0].ax - l.b[1].ax)*dz_inv;
	b[1] = 3 * (l.b[0].z + l.b[1].z)* (l.b[0].y - l.b[1].y)*pow(dz_inv, 3)
		- 3 / 2.*(l.b[0].z + l.b[1].z)*(l.b[0].ay + l.b[1].ay)*pow(dz_inv, 2)
		+ 1 / 2. * (l.b[0].ay - l.b[1].ay)*dz_inv;
	c[0] = -6 * (l.b[0].x - l.b[1].x)*l.b[0].z*l.b[1].z*pow(dz_inv, 3)
		+ 3 * (l.b[0].ax + l.b[1].ax)*l.b[0].z*l.b[1].z*pow(dz_inv, 2)
		- 1 / 2.*(l.b[0].z + l.b[1].z)*(l.b[0].ax - l.b[1].ax)*dz_inv
		+ 1 / 2.*(l.b[0].ax + l.b[1].ax);
	c[1] = -6 * (l.b[0].y - l.b[1].y)*l.b[0].z*l.b[1].z*pow(dz_inv, 3)
		+ 3 * (l.b[0].ay + l.b[1].ay)*l.b[0].z*l.b[1].z*pow(dz_inv, 2)
		- 1 / 2.*(l.b[0].z + l.b[1].z)*(l.b[0].ay - l.b[1].ay)*dz_inv
		+ 1 / 2.*(l.b[0].ay + l.b[1].ay);
	d[0] = -1 / 2.*(l.b[0].x - l.b[1].x)*(l.b[0].z + l.b[1].z)*(pow(l.b[0].z - l.b[1].z, 2) - 2 * l.b[0].z*l.b[1].z)*pow(dz_inv, 3)
		+ 1 / 4.*(l.b[0].z + l.b[1].z)*(l.b[0].ax + l.b[1].ax)*(pow(l.b[0].z - l.b[1].z, 2) - 2 * l.b[0].z*l.b[1].z)*pow(dz_inv, 2)
		+ 1 / 2.*l.b[0].z*l.b[1].z*(l.b[0].ax - l.b[1].ax)*dz_inv
		- 1 / 4.*(l.b[0].z + l.b[1].z)*(l.b[0].ax + l.b[1].ax) + 1 / 2.*(l.b[0].x + l.b[1].x);
	d[1] = -1 / 2.*(l.b[0].y - l.b[1].y)*(l.b[0].z + l.b[1].z)*(pow(l.b[0].z - l.b[1].z, 2) - 2 * l.b[0].z*l.b[1].z)*pow(dz_inv, 3)
		+ 1 / 4.*(l.b[0].z + l.b[1].z)*(l.b[0].ay + l.b[1].ay)*(pow(l.b[0].z - l.b[1].z, 2) - 2 * l.b[0].z*l.b[1].z)*pow(dz_inv, 2)
		+ 1 / 2.*l.b[0].z*l.b[1].z*(l.b[0].ay - l.b[1].ay)*dz_inv
		- 1 / 4.*(l.b[0].z + l.b[1].z)*(l.b[0].ay + l.b[1].ay) + 1 / 2.*(l.b[0].y + l.b[1].y);

	printf("a = %g\n", a[0]);
	printf("b = %g\n", b[0]);
	printf("c = %g\n", c[0]);
	printf("d = %g\n", d[0]);

	printf("a = %g\n", a[1]);
	printf("b = %g\n", b[1]);
	printf("c = %g\n", c[1]);
	printf("d = %g\n", d[1]);

	double path_length = 0;
	double dz, z;
	int pich = 100;
	dz = (l.b[1].z - l.b[0].z) / pich;
	for (int i = 1; i <= pich; i++) {
		z = l.b[0].z + dz * i;
		path_length += sqrt(
			9 * (a[0] * a[0] + a[1] * a[1])*pow(z, 4)
			+ 12 * (a[0] * b[0] + a[1] * b[1])*pow(z, 3)
			+ (4 * (b[0] * b[0] + b[1] * b[1]) + 6 * (a[0] * c[0] + a[1] * c[1]))*pow(z, 2)
			+ 4 * (b[0] * c[0] * b[1] * c[1])*z
			+ c[0] * c[0] + c[1] * c[1] + 1
		)*dz;
	}

	printf("path length = %g\n", path_length);
	return 0;
}