#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class Prediction {
public:
	int pl, rawid, hit;
	double ax, ay, x, y,dax,day,dx,dy;
};

netscan::linklet_t select_best_link(std::vector<netscan::linklet_t> &link_v);
std::vector<Prediction> Calc_Efficiency(std::vector<vxx::base_track_t> &pred, std::vector<netscan::linklet_t> &link);
void output_prediction(std::string filename, std::vector<Prediction>&pred);
void Calc_efficiency(std::vector<Prediction>&pred, double angle_min, double angle_max, int &num, double &eff, double &bg);
void output_efficiency(std::string filename, std::vector<Prediction>&pred);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file_link_path file-prediction pl file-out-path\n");
		exit(1);
	}
	std::string file_link_path = argv[1];
	std::string file_pred = argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_out_path = argv[4];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> pred = br.ReadAll(file_pred, pl, 0);
	



	//return 0;
	for (int sensor = 1; sensor <= 72; sensor++) {
		//if (sensor >=33)continue;
		std::stringstream file_in_link;
		file_in_link << file_link_path << "\\l-"
			<< std::setw(3) << std::setfill('0') << pl << "-"
			<< std::setw(2) << std::setfill('0') << sensor << ".sel0.bll";
		std::vector<netscan::linklet_t> link;
		netscan::read_linklet_bin(file_in_link.str(), link);
		std::vector<Prediction> pair = Calc_Efficiency(pred, link);

		std::stringstream file_out_pair,file_out_eff;

		file_out_pair << file_out_path << "\\pair_"
			<< std::setw(2) << std::setfill('0') << sensor % 72 << ".txt";

		file_out_eff << file_out_path << "\\eff_"
			<< std::setw(2) << std::setfill('0') << sensor % 72 << ".txt";

		output_prediction(file_out_pair.str(), pair);
		output_efficiency(file_out_eff.str(), pair);

	}

}
std::vector<Prediction> Calc_Efficiency(std::vector<vxx::base_track_t> &pred, std::vector<netscan::linklet_t> &link) {
	std::multimap<int, netscan::linklet_t> link_map;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		link_map.insert(std::make_pair(itr->b[0].m[0].rawid, *itr));
	}

	std::vector<Prediction> ret;
	std::vector<netscan::linklet_t> link_v;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		Prediction p;
		p.rawid = itr->m[0].rawid;
		p.ax = itr->ax;
		p.ay = itr->ay;
		p.x = itr->x;
		p.y = itr->y;
		p.pl = itr->pl;

		p.hit = link_map.count(p.rawid);
		if (p.hit == 0) {
			p.dax = 0;
			p.day = 0;
			p.dx = 0;
			p.dy = 0;
		}
		else {
			link_v.clear();
			auto range = link_map.equal_range(p.rawid);
			for (auto res = range.first; res != range.second; res++) {
				link_v.push_back(res->second);
			}
			netscan::linklet_t l = select_best_link(link_v);
			p.dax = l.b[1].ax - l.b[0].ax;
			p.day = l.b[1].ay - l.b[0].ay;
			p.dx = l.b[1].x - l.b[0].x;
			p.dy = l.b[1].y - l.b[0].y;
		}
		ret.push_back(p);
	}
	return ret;
}
netscan::linklet_t select_best_link(std::vector<netscan::linklet_t> &link_v) {
	netscan::linklet_t ret;
	double val;
	for (auto itr = link_v.begin(); itr != link_v.end(); itr++) {
		if (itr == link_v.begin()) {
			val = pow(itr->b[0].x - itr->b[1].x, 2) + pow(itr->b[0].y - itr->b[1].y, 2);
			ret = *itr;
		}
		if (val > pow(itr->b[0].x - itr->b[1].x, 2) + pow(itr->b[0].y - itr->b[1].y, 2)) {
			val = pow(itr->b[0].x - itr->b[1].x, 2) + pow(itr->b[0].y - itr->b[1].y, 2);
			ret = *itr;
		}
	}
	return ret;
}

void output_prediction(std::string filename, std::vector<Prediction>&pred) {

	std::ofstream ofs(filename);
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {

		ofs << std::fixed << std::right
			<< std::setw(3) << std::setprecision(0) << itr->pl << " "
			<< std::setw(10) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(8) << std::setprecision(4) << itr->ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(5) << std::setprecision(0) << itr->hit << " "
			<< std::setw(6) << std::setprecision(4) << itr->dax << " "
			<< std::setw(6) << std::setprecision(4) << itr->day << " "
			<< std::setw(4) << std::setprecision(1) << itr->dx << " "
			<< std::setw(4) << std::setprecision(1) << itr->dy << std::endl;
	}

}
void output_efficiency(std::string filename, std::vector<Prediction>&pred) {
	double eff, bg;
	int num;
	std::ofstream ofs(filename);
	double angle_min, angle_max;
	angle_min = 0;
	angle_max = 3;

	Calc_efficiency(pred, angle_min, angle_max, num, eff, bg);
	ofs << std::fixed << std::right << std::setfill(' ')
		<< std::setw(3) << std::setprecision(1) << angle_min << " "
		<< std::setw(3) << std::setprecision(1) << angle_max << " "
		<< std::setw(5) << std::setprecision(0) << num << " "
		<< std::setw(6) << std::setprecision(5) << eff << " "
		<< std::setw(6) << std::setprecision(5) << bg << std::endl;

	double angle_pich[13] =
	{ 0,0.1,0.2,0.3,0.5,0.7,0.9,1.2,1.5,1.8,2.1,2.5,3.0 };
	for (int i = 0; i + 1 < 13; i++) {
		angle_min = angle_pich[i];
		angle_max = angle_pich[i + 1];
		Calc_efficiency(pred, angle_min, angle_max, num, eff, bg);
		ofs << std::fixed << std::right << std::setfill(' ')
			<< std::setw(3) << std::setprecision(1) << angle_min << " "
			<< std::setw(3) << std::setprecision(1) << angle_max << " "
			<< std::setw(5) << std::setprecision(0) << num << " "
			<< std::setw(6) << std::setprecision(5) << eff << " "
			<< std::setw(6) << std::setprecision(5) << bg << std::endl;

	}

}
void Calc_efficiency(std::vector<Prediction>&pred, double angle_min, double angle_max, int &num,double &eff,double &bg) {
	std::vector<Prediction> sel;
	double angle;
	int count[3] = {};
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angle_min > angle)continue;
		if (angle_max <= angle)continue;
		if (itr->hit == 0)count[0]++;
		else if (itr->hit == 1)count[1]++;
		else count[2]++;
		sel.push_back(*itr);
	}
	num = sel.size();

	double p0, p1, p2;
	p0 = count[0] * 1. / num;
	p1 = count[1] * 1. / num;
	p2 = count[2] * 1. / num;

	eff = ((p2 - p0 + 1) + sqrt(pow(p2 - p0 + 1, 2) - 4 * p2)) / 2;
	bg = ((p2 - p0 + 1) - sqrt(pow(p2 - p0 + 1, 2) - 4 * p2)) / 2;

}