#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>

#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>


class Track_file {
public:
	int eventid, trackid, pl, rawid;
};
class Group_file :public Track_file {
public:
	int link_num;
	std::vector<std::tuple<int, int, int, int>> linklet;
};
class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};
struct Segment {
	int16_t pos;
	int32_t rawid;
	bool operator==(const Segment& rhs) const
	{
		return pos == rhs.pos && rawid == rhs.rawid;
	}
	bool operator<(const Segment& rhs) const {
		if (pos == rhs.pos) {
			return rawid < rhs.rawid;
		}
		return pos < rhs.pos;
	}
};
size_t hash_value(const Segment& d)
{
	// 複数の値のハッシュ値を組み合わせてハッシュ値を計算するには、
	// boost::hash_combine を使います。
	size_t h = 0;
	boost::hash_combine(h, d.pos);
	boost::hash_combine(h, d.rawid);
	return h;
}

std::vector<Track_file> read_track(std::string filename);
void read_linklet_list(std::string filename, boost::unordered_multimap<Segment, Segment> &link);
Group_file make_group(Track_file&t, boost::unordered_multimap<Segment, Segment> &link);
void output_group(std::string filename, std::vector<Group_file>&g);
std::vector<Track_file> read_link(std::string filename);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-track file_in_link file-out-track\n");
		exit(1);
	}
	std::string file_in_track = argv[1];
	std::string file_in_link = argv[2];
	std::string file_out_gruop = argv[3];

	//std::vector<Track_file> track = read_track(file_in_track);
	std::vector<Track_file> track = read_link(file_in_track);

	boost::unordered_multimap<Segment, Segment> link;
	read_linklet_list(file_in_link, link);

	std::vector<Group_file> group;
	for (int i = 0; i < track.size(); i++) {
		printf("make group %d/%d\r", i, track.size());
		group.push_back(make_group(track[i], link));
	}
	printf("\n");

	output_group(file_out_gruop, group);

	exit(0);
}
std::vector<Track_file> read_track(std::string filename) {
	std::ifstream ifs(filename);
	Track_file t;
	std::vector<Track_file> ret;
	int count = 0;
	while (ifs >> t.eventid >> t.trackid >> t.pl >> t.rawid) {
		if (count % 1000 == 0) {
			printf("read track %d\r", count);
		}
		count++;
		ret.push_back(t);

	}
	printf("read track %d\n", count);
	return ret;
}
std::vector<Track_file> read_link(std::string filename) {
	std::ifstream ifs(filename);
	Track_file t;
	std::vector<Track_file> ret;
	int count = 0;
	int pos0, pos1, raw0, raw1;
	while (ifs >> pos0 >> pos1 >> raw0 >> raw1) {
		if (count % 1000 == 0) {
			printf("read track %d\r", count);
		}

		t.eventid = count;
		t.pl = pos0 / 10;
		t.rawid = raw0;
		t.trackid = 0;
		ret.push_back(t);

		t.eventid = count;
		t.pl = pos1 / 10;
		t.rawid = raw1;
		t.trackid = 1;
		ret.push_back(t);

		count++;

	}
	printf("read track %d\n", count);
	return ret;




}

int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}

void read_linklet_list(std::string filename, boost::unordered_multimap<Segment, Segment> &link) {
	int64_t link_num = Linklet_header_num(filename);

	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	linklet_header l;
	Segment seg0, seg1;

	while (ifs.read((char*)& l, sizeof(linklet_header))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		seg0.pos = l.pos0;
		seg0.rawid = l.raw0;
		seg1.pos = l.pos1;
		seg1.rawid = l.raw1;
		link.insert(std::make_pair(seg0, seg1));
		link.insert(std::make_pair(seg1, seg0));
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
}

Group_file make_group(Track_file&t, boost::unordered_multimap<Segment, Segment> &link) {
	Group_file ret;
	ret.eventid = t.eventid;
	ret.trackid = t.trackid;
	ret.pl = t.pl;
	ret.rawid = t.rawid;

	Segment start, seg0, seg1;
	start.pos = t.pl * 10;
	start.rawid = t.rawid;

	std::set<std::pair<Segment, Segment>> all_link;
	std::set<Segment> seen;
	std::set<Segment> add, now;

	add.insert(start);
	int loop_num = 0;
	while (add.size() != 0) {
		now = add;
		add.clear();
		for (auto itr = now.begin(); itr != now.end(); itr++) {
			seen.insert(*itr);
			//printf("loop=%d %d %d\n", loop_num, itr->pos, itr->rawid);
		}
		for (auto itr = now.begin(); itr != now.end(); itr++) {
			if (link.find(*itr) == link.end())continue;
			auto range = link.equal_range(*itr);
			for (auto res = range.first; res != range.second; res++) {
				if (seen.count(res->second) == 0) {
					add.insert(res->second);
				}
				if (res->first.pos < res->second.pos) {
					seg0.pos = res->first.pos;
					seg0.rawid = res->first.rawid;
					seg1.pos = res->second.pos;
					seg1.rawid = res->second.rawid;
				}
				else {
					seg1.pos = res->first.pos;
					seg1.rawid = res->first.rawid;
					seg0.pos = res->second.pos;
					seg0.rawid = res->second.rawid;
				}
				all_link.insert(std::make_pair(seg0, seg1));
			}
		}
		loop_num++;
	}
	//printf("loop fin\n");
	//printf("link size=%d\n", all_link.size());
	if (all_link.size() > 0) {
		for (auto itr = all_link.begin(); itr != all_link.end(); itr++) {
			ret.linklet.push_back(std::make_tuple(itr->first.pos, itr->second.pos, itr->first.rawid, itr->second.rawid));
		}
	}
	ret.link_num = ret.linklet.size();
	return ret;
}

void output_group(std::string filename, std::vector<Group_file>&g) {
	std::ofstream ofs(filename);
	int count = 0;
	for (auto itr = g.begin(); itr != g.end(); itr++) {
		if (count % 1000 == 0) {
			printf("\r wrtie file %d/%d(%4.1lf%%)", count, g.size(), count*100. / g.size());
		}
		count++;

		ofs << std::fixed << std::right
			<< std::setw(10) << std::setprecision(0) << itr->eventid << " "
			<< std::setw(5) << std::setprecision(0) << itr->trackid << " "
			<< std::setw(4) << std::setprecision(0) << itr->pl << " "
			<< std::setw(4) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(10) << std::setprecision(0) << itr->link_num << std::endl;
		if (itr->linklet.size() == 0)continue;
		for (auto itr2 = itr->linklet.begin(); itr2 != itr->linklet.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(5) << std::setprecision(0) << std::get<0>(*itr2) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(*itr2) << std::endl;
		}
	}
	printf("\r wrtie file %d/%d(%4.1lf%%)\n", count, g.size(), count*100. / g.size());

}