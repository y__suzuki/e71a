#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>
using namespace l2c;

class Chain_baselist {
public:
	int64_t groupid, trackid;
	std::pair<int32_t, int64_t> target_track;
	std::set<std::pair<int32_t, int64_t>> btset;
	std::set<std::tuple<int, int, int, int>>ltlist;
	std::vector<int32_t> usepos;
	int chain_num;
	std::vector<Linklet> make_link_list();
	void set_usepos();
};

std::vector < Chain_baselist > read_linklet_list2(std::string filename);
std::vector < Chain_baselist > select_group(std::vector < Chain_baselist >&chain_list, int pl, int rawid);
std::set<std::pair<int, int>> pick_base_rawid(std::vector < Chain_baselist >&chain_list);
mfile0::M_Chain make_chain_signle_base(int pl, int rawid, int eventid);
void add_base_information(std::string file_in_ECC, std::multimap<int, mfile0::M_Base*>&base_map_single);
void trans_local(std::multimap<int, mfile0::M_Base*>&base_map_single, std::map<int, std::vector<corrmap_3d::align_param2>> &corr);


int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage\n");
		exit(1);
	}

	int eventid = std::stoi(argv[1]);
	int pl = std::stoi(argv[2]);
	int rawid = std::stoi(argv[3]);
	std::string file_in_ECC = argv[4];
	std::string file_out_mfile_all = argv[5];

	char file_in_link_list[1024];
	sprintf_s(file_in_link_list, "K:\\NINJA\\E71a\\work\\suzuki\\muon_analysis\\03_0_BG\\re_connection\\group_conv_done\\track_group1_%010d.txt", eventid);




	std::vector<Chain_baselist> chain_list = read_linklet_list2(file_in_link_list);
	chain_list = select_group(chain_list, pl, rawid);
	std::set<std::pair<int, int>> base_set = pick_base_rawid(chain_list);

	std::string file_in_corrmap = file_in_ECC + "\\Area0\\0\\align\\fine\\local\\corrmap-local-abs.lst";
	std::map<int, std::vector<corrmap_3d::align_param>>corrmap = corrmap_3d::read_ali_param_abs(file_in_corrmap, 1);
	std::map<int, std::vector<corrmap_3d::align_param2>>corrmap_dd = corrmap_3d::DelaunayDivide_map(corrmap);
	std::multimap<int, mfile0::M_Base*>base_map_pl;

	mfile0::Mfile m;
	mfile0::set_header(1, 133, m);
	int cnt = 0;
	for (auto itr = base_set.begin(); itr != base_set.end(); itr++) {
		//single basetrackの場合
		mfile0::M_Chain chain = make_chain_signle_base(itr->first, itr->second, eventid);
		chain.chain_id = cnt;
		cnt++;
		m.chains.push_back(chain);
		mfile0::M_Chain* cp = &m.chains[m.chains.size() - 1];
		for (int k = 0; k < cp->basetracks.size(); k++) {
			base_map_pl.insert(std::make_pair(cp->basetracks[k].pos / 10, &cp->basetracks[k]));
		}

	}

	printf("\n");
	printf("chain size=%d\n", m.chains.size());
	add_base_information(file_in_ECC, base_map_pl);

	//ここ
	//trans_global(base_map_pl, corrmap, z_map);
	trans_local(base_map_pl, corrmap_dd);


	mfile0::write_mfile(file_out_mfile_all, m);


}

std::vector < Chain_baselist > read_linklet_list2(std::string filename) {
	std::vector < Chain_baselist > ret;
	std::ifstream ifs(filename);
	int gid, tid, muon_num, t_pl, t_rawid;
	int64_t link_num;
	int pl, raw;
	double weight;
	std::tuple<int, int, int, int> link;
	int count = 0;

	while (ifs >> gid >> tid >> t_pl >> t_rawid >> link_num) {
		printf("\r read group %d gid=%d link=%lld", count, gid, link_num);
		count++;
		Chain_baselist c;
		c.groupid = gid;
		c.trackid = tid;
		c.target_track.first = t_pl;
		c.target_track.second = t_rawid;
		for (int64_t i = 0; i < link_num; i++) {
			ifs >> std::get<0>(link) >> std::get<1>(link) >> std::get<2>(link) >> std::get<3>(link);
			c.btset.insert(std::make_pair(std::get<0>(link), std::get<2>(link)));
			c.btset.insert(std::make_pair(std::get<1>(link), std::get<3>(link)));
			c.ltlist.insert(link);
		}
		ret.push_back(c);
	}
	printf("\r read group %d gid=%d link=%lld\n", count, gid, link_num);

	return ret;

}

std::vector < Chain_baselist > select_group(std::vector < Chain_baselist >&chain_list,int pl, int rawid) {
	std::vector < Chain_baselist > ret;
	for (auto &gr : chain_list) {
		bool flg = false;
		for (auto &b : gr.btset) {
			if (b.first / 10 == pl and b.second == rawid) {
				flg = true;
			}
		}
		if (flg) {
			ret.push_back(gr);
		}


	}
	return ret;
}

std::set<std::pair<int,int>> pick_base_rawid(std::vector < Chain_baselist >&chain_list) {
	std::set<std::pair<int, int>> ret;
	for (auto &gr : chain_list) {
		for (auto &b : gr.btset) {
			int pl = b.first / 10;
			int rawid = b.second;
			ret.insert(std::make_pair(pl, rawid));
		}
	}
	return ret;
}


mfile0::M_Chain make_chain_signle_base(int pl,int rawid,int eventid) {
	mfile0::M_Chain c;
	mfile0::M_Base b;
	b.pos = pl * 10;
	b.rawid = rawid;
	b.group_id = eventid;
	b.flg_i[0] = 1;
	b.flg_i[1] = 0;
	b.flg_i[2] = 0;
	b.flg_i[3] = 0;
	b.flg_d[0] = 0;
	b.flg_d[1] = 0;
	c.basetracks.push_back(b);
	c.chain_id = 0;
	c.pos0 = c.basetracks.begin()->pos;
	c.pos1 = c.basetracks.rbegin()->pos;
	c.nseg = c.basetracks.size();
	return c;
}

void add_base_information(std::string file_in_ECC, std::multimap<int, mfile0::M_Base*>&base_map_single) {
	int count = 0;
	int64_t pickup_base_num = 0;
	for (auto itr = base_map_single.begin(); itr != base_map_single.end(); itr++) {
		count = base_map_single.count(itr->first);
		int pl = itr->first;
		printf("PL%03d basetrack read ", pl);

		std::multimap<int, mfile0::M_Base*>base_map;
		int raw_min = INT32_MAX, raw_max = 0;
		auto range = base_map_single.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_map.insert(std::make_pair(res->second->rawid, res->second));
			raw_min = std::min(raw_min, int(res->second->rawid));
			raw_max = std::max(raw_max, int(res->second->rawid));
		}

		std::stringstream file_in_base;
		file_in_base << file_in_ECC << "\\Area0\\PL"
			<< std::setw(3) << std::setfill('0') << pl << "\\b"
			<< std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
		std::vector<vxx::base_track_t >base;
		vxx::BvxxReader br;
		std::array<int, 2> index = { raw_min,raw_max + 1 };//1234<=rawid<=5678であるようなものだけを読む。
		printf("rawid %10d - %10d\n", raw_min, raw_max);
		base = br.ReadAll(file_in_base.str(), pl, 0, vxx::opt::index = index);

		for (auto itr = base.begin(); itr != base.end(); itr++) {
			auto res = base_map.find(itr->rawid);
			if (res == base_map.end())continue;
			auto range = base_map.equal_range(itr->rawid);
			for (auto res = range.first; res != range.second; res++) {
				res->second->ax = itr->ax;
				res->second->ay = itr->ay;
				res->second->x = itr->x;
				res->second->y = itr->y;
				res->second->z = 0;
				res->second->ph = itr->m[0].ph + itr->m[1].ph;
				pickup_base_num++;
			}
		}

		itr = std::next(itr, count - 1);
	}
}


void trans_local(std::multimap<int, mfile0::M_Base*>&base_map_single, std::map<int, std::vector<corrmap_3d::align_param2>> &corr) {
	int count = 0;
	for (auto itr = base_map_single.begin(); itr != base_map_single.end(); itr++) {
		count = base_map_single.count(itr->first);
		int pl = itr->first;
		printf("PL%03d basetrack tans\n", pl);
		if (corr.count(pl) == 0) {
			fprintf(stderr, "PL%03d corrmap not found\n", pl);
		}
		std::vector<corrmap_3d::align_param2> param = corr.at(pl);

		std::vector< mfile0::M_Base*> base_trans;
		auto range = base_map_single.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_trans.push_back(res->second);
		}
		std::vector <std::pair<mfile0::M_Base*, corrmap_3d::align_param2*>> base_trans_map = corrmap_3d::track_affineparam_correspondence(base_trans, param);
		trans_base_all(base_trans_map);


		itr = std::next(itr, count - 1);
	}

}