#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
class parameter_format {
public:
	double angle_min, angle_max, parameter[3];
};

int judege_sensor_id(int id);
std::map<double, parameter_format> ReadParam(std::string filename);
void pixel_correction(std::vector<Momentum_recon::Event_information> &momch, std::map<double, parameter_format> &param);


int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-momch file-in-corr file-out-momch\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_corr = argv[2];
	std::string file_out_momch = argv[3];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	std::map<double, parameter_format> param = ReadParam(file_corr);
	pixel_correction(momch, param);
	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);
	exit(0);
}
std::map<double, parameter_format> ReadParam(std::string filename) {
	std::map<double, parameter_format> ret;
	parameter_format p;
	std::ifstream ifs(filename);
	double mom_min, mom_max, angle_min, angle_max, param[3], error[3];
	while (ifs >> mom_min >> mom_max >> angle_min >> angle_max
		>> param[0] >> error[0] >> param[1] >> error[1] >> param[2] >> error[2]) {
		p.angle_min = angle_min;
		p.angle_max = angle_max;
		p.parameter[0] = param[0];
		p.parameter[1] = param[1];
		p.parameter[2] = param[2];
		ret.insert(std::make_pair(p.angle_max, p));
	}
	return ret;

}
void pixel_correction(std::vector<Momentum_recon::Event_information> &ev_v, std::map<double, parameter_format> &param) {
	double hitnum = 0;
	double angle, par[3],path;
	int count = 0, all = ev_v.size();
	for (auto &ev : ev_v) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r VPH correction %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		for (auto &c : ev.chains) {

			std::map<std::tuple<int, int, int>, int>after_corr_val;
			for (auto &b : c.base) {
				for (int i = 0; i < 2; i++) {
					if (b.m[i].hitnum < 0)continue;
					if (judege_sensor_id(b.m[i].imager) == 1)continue;
					path = sqrt(b.ax*b.ax + b.ay*b.ay + 1);
					hitnum = b.m[i].hitnum / path;
					angle = sqrt(b.ax*b.ax + b.ay*b.ay);
					auto p = param.lower_bound(angle);
					if (p == param.end()) {
						p = std::prev(p, 1);
					}
					par[0] = p->second.parameter[0];
					par[1] = p->second.parameter[1];
					par[2] = p->second.parameter[2];
					hitnum = (exp(hitnum / par[1]) - exp(-1 * hitnum / par[1])) / (exp(hitnum / par[1]) + exp(-1 * hitnum / par[1]))*par[2] + par[0];
					//補正後のほうが値が大きくなることを要求
					if (hitnum*path > b.m[i].hitnum) {
						b.m[i].hitnum = hitnum * path;
						after_corr_val.insert(std::make_pair(std::make_tuple(b.pl, b.rawid, i), b.m[i].hitnum));
					}
				}
			}
			for (auto &pair : c.base_pair) {
				for (int i = 0; i < 2; i++) {
					auto res = after_corr_val.find(std::make_tuple(pair.first.pl, pair.first.rawid, i));
					if (res != after_corr_val.end()) {
						pair.first.m[i].hitnum = res->second;
					}
					res = after_corr_val.find(std::make_tuple(pair.second.pl, pair.second.rawid, i));
					if (res != after_corr_val.end()) {
						pair.second.m[i].hitnum = res->second;
					}
				}
			}
		}
	}
	fprintf(stderr, "\r pixel correction %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

}
int judege_sensor_id(int id) {
	if ((24 <= id && id <= 35) || id == 52)return 0;
	else return 1;
}
