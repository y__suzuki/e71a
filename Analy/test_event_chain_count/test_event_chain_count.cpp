#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch);
void out_event_count(std::string filename, std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> &mom_event);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file_in_momch\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_out_count = argv[2];
	std::string file_out_all = argv[3];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);

	std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> mom_event = divide_event(momch);

	out_event_count(file_out_count, mom_event);

}
std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch) {
	std::multimap<int, Momentum_recon::Mom_chain> partner;
	std::map<int, Momentum_recon::Mom_chain> muon;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		if (itr->chainid == 0) {
			muon.insert(std::make_pair(itr->groupid, *itr));
		}
		else {
			partner.insert(std::make_pair(itr->groupid, *itr));
		}
	}

	std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> ret;

	for (auto itr = muon.begin(); itr != muon.end(); itr++) {
		std::vector<Momentum_recon::Mom_chain> p;
		if (partner.count(itr->first) == 0) {
			ret.insert(std::make_pair(itr->first, std::make_pair(itr->second, p)));
			continue;
		}
		auto range = partner.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			p.push_back(res->second);
		}
		ret.insert(std::make_pair(itr->first, std::make_pair(itr->second, p)));
	}
	return ret;
}

void out_event_count(std::string filename, std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> &mom_event) {

	std::ofstream ofs(filename);
	for (auto itr = mom_event.begin(); itr != mom_event.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(5) << std::setprecision(0) << itr->first << " "
			<< std::setw(5) << std::setprecision(0) << itr->second.first.base.rbegin()->pl << " "
			<< std::setw(5) << std::setprecision(0) << itr->second.second.size() << std::endl;
	}


}