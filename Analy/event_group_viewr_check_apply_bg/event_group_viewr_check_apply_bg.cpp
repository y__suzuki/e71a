#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::map<std::pair<int, int>, int> read_txt(std::string filename);
std::vector<Momentum_recon::Event_information> remove_chain(std::vector<Momentum_recon::Event_information> &momch, std::vector<Momentum_recon::Event_information> &momch_p, std::map<std::pair<int, int>, int>&remove_list);
std::vector<Momentum_recon::Event_information> group_extract(std::vector<Momentum_recon::Event_information>&momch, std::vector < mfile0::M_Chain >&chain);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:file-in-event-momch file-in-mfile file-in-viwer-check file_out-event-momch file-out-proton-momch\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_in_mfile = argv[2];
	std::string file_in_txt = argv[3];
	std::string file_out_momch = argv[4];
	std::string file_out_momch_proton = argv[5];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	momch = group_extract(momch, m.chains);

	
	std::map<std::pair<int, int>, int> remove_list = read_txt(file_in_txt);

	std::vector<Momentum_recon::Event_information> momch_p;
	momch = remove_chain(momch, momch_p, remove_list);

	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);
	Momentum_recon::Write_Event_information_extension(file_out_momch_proton, momch_p);

	for (auto itr = momch_p.begin(); itr != momch_p.end(); itr++) {
		printf("%d\n", itr->groupid);
	}
	//for (auto itr = remove_chain.begin(); itr != remove_chain.end(); itr++) {
	//	printf("%5d %5d %d\n", itr->first.first, itr->first.second, itr->second);
	//}

}
std::map<std::pair<int, int>, int> read_txt(std::string filename) {
	std::map<std::pair<int, int>, int> ret;
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	std::pair<int, int> id;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		if (str_v.size() != 1 && str_v.size() != 2)continue;
		int64_t id_all = stoll(str_v[0]);
		id.first = id_all / 100000;
		id.second = (id_all % 100000) / 10;
		if (str_v.size() == 1) {
			ret.insert(std::make_pair(id, 0));
		}
		else {
			ret.insert(std::make_pair(id, 1));
		}
	}
	return ret;
}
std::vector<Momentum_recon::Event_information> group_extract(std::vector<Momentum_recon::Event_information>&momch, std::vector < mfile0::M_Chain >&chain) {
	std::vector<Momentum_recon::Event_information> ret;

	std::set<int> group_set;
	for (auto &c : chain) {
		int64_t gid = c.basetracks.begin()->group_id;
		int groupid = gid / 100000;
		group_set.insert(groupid);
	}

	for (auto &ev : momch) {
		if (group_set.count(ev.groupid) == 0)continue;
		ret.push_back(ev);
	}

	fprintf(stderr, "group selection %d-->%d\n", momch.size(), ret.size());
	return ret;
}
std::vector<Momentum_recon::Event_information> remove_chain(std::vector<Momentum_recon::Event_information> &momch, std::vector<Momentum_recon::Event_information> &momch_p, std::map<std::pair<int, int>, int>&remove_list) {
	std::vector<Momentum_recon::Event_information> ret;
	for (auto &ev : momch) {
		Momentum_recon::Event_information ev_tmp = ev;
		Momentum_recon::Event_information ev_proton = ev;
		ev_tmp.chains.clear();
		ev_proton.chains.clear();
		for (auto &c : ev.chains) {
			if (c.chainid == 0) {
				ev_proton.chains.push_back(c);
			}
			auto res = remove_list.find(std::make_pair(ev.groupid, c.chainid));
			if (res == remove_list.end()) {
				ev_tmp.chains.push_back(c);
			}
			else if (res->second == 1) {
				ev_proton.chains.push_back(c);
			}
		}
		ret.push_back(ev_tmp);
		if (ev_proton.chains.size() > 1) {
			momch_p.push_back(ev_proton);
		}

	}
	return ret;
}