#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <sstream>
#include <ios>     // std::left, std::right
#include <iomanip> 

#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
#pragma comment(lib, "VxxReader.lib")

class GrainInfo {
public:
	double x, y, z, brightness;
	int layer, npixel;

};

std::map<std::pair<int, int >, int> count_grain(std::vector<GrainInfo>&grain, double ax, double ay, double area);
std::vector<GrainInfo> read_grain_inf(std::string filename);

int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:fileanme\n");
		exit(1);
	}
	std::string file_in_data = argv[1];
	std::vector<GrainInfo> grain=read_grain_inf(file_in_data);

	double track_ax = 0.2295;
	double track_ay = -0.3018;
	
	std::map<std::pair<int, int >, int> hist = count_grain(grain, track_ax, track_ay, 5);

	std::ofstream ofs("out_tmp.txt");
	for (auto itr = hist.begin(); itr != hist.end(); itr++) {
		ofs << itr->first.first << " "
			<< itr->first.second << " "
			<< itr->second << std::endl;
	}
}
std::vector<GrainInfo> read_grain_inf(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<GrainInfo> ret;
	GrainInfo inf;
	while (ifs >> inf.x >> inf.y >> inf.z >> inf.brightness >> inf.layer >> inf.npixel) {
		inf.x = inf.x*0.149;
		inf.y = inf.y*-0.149;
		inf.z = inf.z*1.5;
		ret.push_back(inf);
	}
	return ret;
}

std::map<std::pair<int, int >, int> count_grain(std::vector<GrainInfo>&grain, double ax, double ay, double area) {

	double xmin, xmax, ymin, ymax, zmin, zmax;
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		if (itr == grain.begin()) {
			xmin = itr->x;
			xmax = itr->x;
			ymin = itr->y;
			ymax = itr->y;
			zmin = itr->z;
			zmax = itr->z;

		}
		xmin = std::min(xmin, itr->x);
		xmax = std::max(xmax, itr->x);
		ymin = std::min(ymin, itr->y);
		ymax = std::max(ymax, itr->y);
		zmin = std::min(zmin, itr->z);
		zmax = std::max(zmax, itr->z);
	}
	printf("%g %g %g\n", xmin, ymin, zmin);
	std::map<std::tuple<int, int, int>, int> grain_hash;
	double hash_size_x, hash_size_y, hash_size_z;
	hash_size_x = 2.5;
	hash_size_y = 2.5;
	hash_size_z = 2.5;
	std::tuple<int, int, int>id;
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		std::get<0>(id) = (itr->x - xmin) / hash_size_x;
		std::get<1>(id) = (itr->y - ymin) / hash_size_y;
		std::get<2>(id) = (itr->z - zmin) / hash_size_z;
		auto res = grain_hash.insert(std::make_pair(id, 1));
		if (!res.second) {
			res.first++;
		}
	}
	printf("bin width %g\n", hash_size_x);
	printf("xmin=%d\n", int(xmin / hash_size_x)*hash_size_x);
	printf("xmax=%d\n", int(xmax / hash_size_x)*hash_size_x + 1);

	printf("bin width %g\n", hash_size_y);
	printf("ymin=%d\n", int(ymin / hash_size_y)*hash_size_y);
	printf("ymax=%d\n", int(ymax / hash_size_y)*hash_size_y + 1);

	std::set<double> center_z;
	for (double z = zmin; z < zmax; z += hash_size_z) {
		center_z.insert(z + hash_size_z / 2);
	}

	std::map<std::pair<int, int >, int> ret;
	std::pair<int, int> position;
	int count;
	double track_x, track_y, track_z;
	for (double x = xmin; x < xmax; x += hash_size_x) {
		for (double y = ymin; y < ymax; y += hash_size_y) {
			count = 0;
			position.first = x;
			position.second = y;
			for (auto itr = center_z.begin(); itr != center_z.end(); itr++) {
				track_x = x + ax * ((*itr) - zmin);
				track_y = y + ay * ((*itr) - zmin);
				track_z = (*itr);
				std::get<0>(id) = (track_x - xmin) / hash_size_x;
				std::get<1>(id) = (track_y - ymin) / hash_size_y;
				std::get<2>(id) = (track_z - zmin) / hash_size_z;
				if (grain_hash.count(id) == 0)continue;
				count += grain_hash.at(id);
			}
			ret.insert(std::make_pair(std::make_pair(std::get<0>(id), std::get<1>(id)), count));

		}
	}
	return ret;

}