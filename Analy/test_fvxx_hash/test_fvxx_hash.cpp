#pragma comment(lib, "VxxReader.lib")
#include <VxxReader.h>
#include <map>
std::vector<vxx::micro_track_t> edge_noise_cut(std::vector<vxx::micro_track_t> &micro, int area, double range[4], double cut);
void get_fvxx_area(std::vector<vxx::micro_track_t> &micro, double range[4]);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg fvxx pos area\n");
		exit(1);
	}
	std::string filename = argv[1];
	int pos = std::stoi(argv[2]);
	int area = std::stoi(argv[3]);
	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> micro = fr.ReadAll(filename, pos, 0);
	double range[4];
	get_fvxx_area(micro, range);
	micro = edge_noise_cut(micro, area, range, 5000);


}
std::vector<vxx::micro_track_t> edge_noise_cut(std::vector<vxx::micro_track_t> &micro, int area, double range[4], double cut) {

	std::vector<vxx::micro_track_t> ret;
	std::vector<vxx::micro_track_t> edge;
	bool flg;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		flg = true;
		if (area % 2 == 1) {
			if (itr->x < range[0] + cut) {
				flg = false;
			}
		}
		if (area % 2 == 0) {
			if (range[1] - cut < itr->x) {
				flg = false;
			}
		}
		if (area == 5 || area == 6) {
			if (range[3] - cut < itr->y) {
				flg = false;
			}
		}
		if (flg) {
			ret.push_back(*itr);
		}
		else {
			edge.push_back(*itr);
		}
	}

	if (edge.size() == 0) {
		return ret;
	}
	double edge_range[4] = {};
	get_fvxx_area(edge, edge_range);
	double hash = 10;
	std::multimap<std::pair<int, int>, vxx::micro_track_t> m_hash;
	int ix, iy;
	for (auto itr = edge.begin(); itr != edge.end(); itr++) {
		ix = (itr->x - edge_range[0]) / hash;
		iy = (itr->y - edge_range[2]) / hash;
		m_hash.insert(std::make_pair(std::make_pair(ix, iy), *itr));
	}
	std::vector<int> count;
	int num;
	for (auto itr = m_hash.begin(); itr != m_hash.end(); itr++) {
		num = m_hash.count(itr->first);
		count.push_back(num);
		itr = std::next(itr, num-1);
		//printf("%d\n", num);
	}
	double mean = 0,rms=0;
	for (auto itr = count.begin(); itr != count.end(); itr++) {
		mean += *itr;
		rms += (*itr)*(*itr);
	}
	mean = mean / count.size();
	rms = sqrt(rms / count.size() - mean * mean);
	int threshold = std::min(int(mean + rms * 5), 10);
	for (auto itr = m_hash.begin(); itr != m_hash.end(); itr++) {
		num = m_hash.count(itr->first);
		if (num < threshold) {
			auto res = m_hash.equal_range(itr->first);
			for (auto itr2 = res.first; itr2 != res.second; itr2++) {
				ret.push_back(itr2->second);
			}
		}
		itr = std::next(itr, num - 1);
	}
	return ret;

}

void get_fvxx_area(std::vector<vxx::micro_track_t> &micro, double range[4]) {

	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		if (itr == micro.begin()) {
			range[0] = itr->x;
			range[1] = itr->x;
			range[2] = itr->y;
			range[3] = itr->y;
		}
		range[0] = std::min(itr->x, range[0]);
		range[1] = std::max(itr->x, range[1]);
		range[2] = std::min(itr->y, range[2]);
		range[3] = std::max(itr->y, range[3]);
	}

	printf("xmin %lf\n", range[0]);
	printf("xmax %lf\n", range[1]);
	printf("ymin %lf\n", range[2]);
	printf("ymax %lf\n", range[3]);
}