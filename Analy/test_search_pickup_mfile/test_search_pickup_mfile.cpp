
#define _CRT_SECURE_NO_WARNINGS
//#pragma comment(lib, "VxxReader.lib")
//#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <filesystem>

std::set<int> pick_groupid(std::vector<Sharing_file::Sharing_file> &sf);
std::vector<mfile0::M_Chain> pick_chian(std::vector<mfile0::M_Chain>&chain, std::set<int> groupid);

int main(int argc,char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage\n");
		exit(1);
	}

	std::string file_in_sf = argv[1];
	std::string file_in_mfile = argv[2];
	std::string file_out_mfile = argv[3];

	std::vector<Sharing_file::Sharing_file> sf;
	sf=Sharing_file::Read_sharing_file_extension(file_in_sf);

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::set<int> groupid = pick_groupid(sf);
	m.chains = pick_chian(m.chains, groupid);

	mfile1::write_mfile_extension(file_out_mfile, m);


}
std::set<int> pick_groupid(std::vector<Sharing_file::Sharing_file> &sf) {
	std::set<int> groupid;
	for (auto &mu : sf) {
		groupid.insert(mu.eventid);
	}
	for (auto &id : groupid) {
		printf("%d\n", id);
	}
	return groupid;
}

std::vector<mfile0::M_Chain> pick_chian(std::vector<mfile0::M_Chain>&chain, std::set<int> groupid) {
	std::vector<mfile0::M_Chain>ret;
	for (auto &c : chain) {
		int gid = c.basetracks.begin()->group_id;
		if (groupid.count(gid) == 1) {
			ret.push_back(c);
		}
	}
	return ret;

}