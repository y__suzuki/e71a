#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <omp.h>
#include <set>


std::vector<mfile0::M_Chain> proton_selection(std::vector<mfile0::M_Chain> c);
bool judge_mip_short(mfile0::M_Chain c);
bool judge_mip_long(mfile0::M_Chain c);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg file-in-mfile file-out\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out = argv[2];
	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);
	m.chains = proton_selection(m.chains);

	mfile0::write_mfile(file_out, m);
	std::ofstream ofs("proton_inf.txt");
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		double vph, mom, angle;
		vph = 0;
		mom = -1;
		angle = 0;
		double ax = 0, ay = 0;
		int count = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			count++;
			ax += itr2->ax;
			ay += itr2->ay;
			vph += itr2->ph % 10000;
		}

		ax = ax / count;
		ay = ay / count;
		vph = vph / count;

		angle = sqrt(ax*ax + ay * ay);
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr->chain_id << " "
			<< std::setw(10) << std::setprecision(4) << angle << " "
			<< std::setw(6) << std::setprecision(1) << vph << " "
			<< std::setw(6) << std::setprecision(1) << mom << std::endl;


	}


}
std::vector<mfile0::M_Chain> proton_selection(std::vector<mfile0::M_Chain> c) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->chain_id % 100000 == 0)continue;
		if ((itr->pos1 - itr->pos0) / 10 > 10) {
			if (!judge_mip_long(*itr)) {
				ret.push_back(*itr);
			}
		}
		else {
			if (!judge_mip_short(*itr)) {
				ret.push_back(*itr);
			}
		}
	}
	return ret;
}
bool judge_mip_short(mfile0::M_Chain c) {
	double angle = 0, vph = 0;
	double ax = 0, ay = 0;
	int count = 0;	for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
		ax += itr->ax;
		ay += itr->ay;
		vph += itr->ph % 10000;

		count++;
	}
	ax = ax / count;
	ay = ay / count;
	angle = sqrt(ax*ax + ay * ay);
	vph = vph / count;

	double param[3];

	param[0] = 3319.48;
	param[1] = -3.71618;
	param[2] = 13.4601;
	bool ret = vph < std::max(param[0] / pow(angle - param[1], 2) + param[2], 100.);

	return ret;
}
bool judge_mip_long(mfile0::M_Chain c) {
	double angle = 0, vph = 0;
	double ax = 0, ay = 0;
	int count = 0;
	for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
		ax += itr->ax;
		ay += itr->ay;
		vph += itr->ph % 10000;
		count++;
	}
	ax = ax / count;
	ay = ay / count;
	angle = sqrt(ax*ax + ay * ay);
	vph = vph / count;

	double param[3];
	param[0] = 15.1341;
	param[1] = -0.355553;
	param[2] = 80.2505;

	//sprintf(fomula, "%.4lf/(x-%.4lf)^2+%.4lf", param[0], param[1], param[2]);

	return vph < std::max(param[0] / pow(angle - param[1], 2) + param[2], 80.);
}

