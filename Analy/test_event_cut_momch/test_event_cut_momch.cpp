#define _CRT_SECURE_NO_WARNINGS
//#pragma comment(lib, "VxxReader.lib")
//#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <filesystem>

std::vector<Momentum_recon::Event_information> cut_momch(std::vector<Momentum_recon::Event_information> &momch);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	std::vector<Momentum_recon::Event_information> momch2=cut_momch(momch);

	Momentum_recon::Write_Event_information_extension(file_out_momch, momch2);

}
std::vector<Momentum_recon::Event_information> cut_momch(std::vector<Momentum_recon::Event_information> &momch) {
	std::vector<Momentum_recon::Event_information> ret;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		if (itr->groupid == 414
			|| itr->groupid == 756
			|| itr->groupid == 765
			|| itr->groupid == 2057
			|| itr->groupid == 2405
			|| itr->groupid == 2911
			|| itr->groupid == 4602
			|| itr->groupid == 4728
			|| itr->groupid == 5332
			|| itr->groupid == 5947
			|| itr->groupid == 6205
			|| itr->groupid == 8061
			|| itr->groupid == 9846
			|| itr->groupid == 9899
			|| itr->groupid == 10609
			|| itr->groupid == 11911) {
			continue;
		}
		ret.push_back(*itr);
	}
	printf("event num = %d --> %d\n", momch.size(), ret.size());
	return ret;

}
