#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_inf {
public:
	int groupid, chainid, nseg, npl;
	std::vector<int>pl_all;
	double ax, ay, vph_ave, vph_sigma;

};

std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch);
std::map<double, std::pair<double, double>> vph_mip_distribution();
std::vector<output_inf> Calc_nseg_npl(std::vector<Momentum_recon::Mom_chain> &momch, std::map<double, std::pair<double, double>> &vph_mip);
void output_file(std::string filename, std::vector<output_inf>&output);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage\n");
	}
	std::string file_in_momch = argv[1];
	std::string file_out = argv[2];
	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> momch_ev = divide_event(momch);

	std::map<double, std::pair<double, double>> vph_mip = vph_mip_distribution();
	printf("hoge\n");

	std::vector<output_inf> out = Calc_nseg_npl(momch, vph_mip);
	printf("hoge\n");

	output_file(file_out, out);


}

std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch) {

	std::multimap<int, Momentum_recon::Mom_chain> divide_ev;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		divide_ev.insert(std::make_pair(itr->groupid, *itr));
	}
	std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> ret;
	for (auto itr = divide_ev.begin(); itr != divide_ev.end(); itr++) {
		Momentum_recon::Mom_chain muon;
		std::vector<Momentum_recon::Mom_chain>partner;
		muon.groupid = -1;
		auto range = divide_ev.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.chainid == 0) {
				muon = res->second;
			}
			else {
				partner.push_back(res->second);
			}
		}
		if (muon.groupid < 0) {
			fprintf(stderr, "exception event %d not found\n", itr->first);
			itr = std::next(itr, divide_ev.count(itr->first) - 1);
		}
		ret.push_back(std::make_pair(muon, partner));
		itr = std::next(itr, divide_ev.count(itr->first) - 1);
	}
	return ret;
}
std::map<double, std::pair<double, double>> vph_mip_distribution() {
	std::map<double, std::pair<double, double>> ret;
	ret.insert(std::make_pair(0.05, std::make_pair(61.86, 6.8)));
	ret.insert(std::make_pair(0.15, std::make_pair(49.68, 7.6)));
	ret.insert(std::make_pair(0.25, std::make_pair(36.38, 5.0)));
	ret.insert(std::make_pair(0.35, std::make_pair(30.85, 4.0)));
	ret.insert(std::make_pair(0.45, std::make_pair(29.20, 3.8)));
	ret.insert(std::make_pair(0.55, std::make_pair(27.17, 3.9)));
	ret.insert(std::make_pair(0.65, std::make_pair(25.14, 3.8)));
	ret.insert(std::make_pair(0.80, std::make_pair(22.26, 3.6)));
	ret.insert(std::make_pair(1.00, std::make_pair(19.83, 3.3)));
	ret.insert(std::make_pair(1.20, std::make_pair(18.39, 3.2)));
	ret.insert(std::make_pair(1.40, std::make_pair(17.24, 3.1)));
	ret.insert(std::make_pair(1.70, std::make_pair(16.02, 3.0)));
	ret.insert(std::make_pair(2.10, std::make_pair(15.29, 3.0)));
	ret.insert(std::make_pair(2.50, std::make_pair(14.19, 3.0)));
	return ret;
}

std::vector<output_inf> Calc_nseg_npl(std::vector<Momentum_recon::Mom_chain> &momch, std::map<double, std::pair<double, double>> &vph_mip) {
	std::vector<output_inf> ret;
	output_inf out;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		if (itr->chainid == 0)continue;
		out.groupid = itr->groupid;
		out.chainid = itr->chainid;
		out.npl = itr->base.rbegin()->pl - itr->base.begin()->pl + 1;
		out.nseg = itr->base.size();
		out.ax = 0;
		out.ay = 0;
		out.vph_ave = 0;
		out.pl_all.clear();
		int count_angle = 0;
		int count_vph = 0;
		for (auto &b : itr->base) {
			out.ax += b.ax;
			out.ay += b.ay;
			out.vph_ave += b.m[0].ph % 10000;
			out.vph_ave += b.m[1].ph % 10000;
			count_angle += 1;
			count_vph += 2;
			out.pl_all.push_back(b.pl);
		}
		out.ax /= count_angle;
		out.ay /= count_angle;
		out.vph_ave /= count_vph;
		double angle = sqrt(out.ax*out.ax + out.ay * out.ay);

		double sigma = 0;
		auto vph_angle = vph_mip.upper_bound(angle);
		if (vph_angle == vph_mip.begin()) {
			sigma = (out.vph_ave - vph_angle->second.first) / vph_angle->second.second;
		}
		else if (vph_angle == vph_mip.end()) {
			sigma = (out.vph_ave - vph_mip.rbegin()->second.first) / vph_mip.rbegin()->second.second;
		}
		else {
			double vph_mean, vph_sigma;
			auto val0 = std::next(vph_angle, -1);
			auto val1 = vph_angle;
			vph_mean = (val1->second.first - val0->second.first) / (val1->first - val0->first)*(angle - val0->first) + val0->second.first;
			vph_sigma = (val1->second.second - val0->second.second) / (val1->first - val0->first)*(angle - val0->first) + val0->second.second;
			sigma = (out.vph_ave - vph_mean) / vph_sigma;
		}
		out.vph_sigma = sigma;
		ret.push_back(out);
	}
	return ret;

}
void output_file(std::string filename, std::vector<output_inf>&output) {
	std::ofstream ofs(filename);
	for (auto itr = output.begin(); itr != output.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(6) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(6) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(6) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(6) << std::setprecision(0) << itr->npl << " "
			<< std::setw(6) << std::setprecision(1) << itr->vph_ave << " "
			<< std::setw(6) << std::setprecision(4) << itr->vph_sigma << std::endl;
		for(int i=0;i<itr->pl_all.size();i++){
			if (i + 1 == itr->pl_all.size()) {
				ofs << std::setw(3) << std::setprecision(0) << itr->pl_all[i] << std::endl;
			}
			else {
				ofs << std::setw(3) << std::setprecision(0) << itr->pl_all[i] << " ";
			}
		}
	}
}