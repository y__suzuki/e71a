#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::map<int64_t, std::vector<mfile0::M_Chain>> chain_pickup(std::vector<mfile0::M_Chain> &chain);
std::vector<std::vector<mfile0::M_Chain>> group_divide(std::map<int64_t, std::vector<mfile0::M_Chain>>&chain_map);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out_path = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::map<int64_t, std::vector<mfile0::M_Chain>> m_group = chain_pickup(m.chains);
	std::vector<std::vector<mfile0::M_Chain>> m_group_vec = group_divide(m_group);

	for (int i = 0; i < m_group_vec.size(); i++) {
		m.chains = m_group_vec[i];
		char ofname[256];
		sprintf_s(ofname, "%s\\m_%03d.all", file_out_path, i);
		mfile0::write_mfile(ofname, m);

	}



}

std::map<int64_t, std::vector<mfile0::M_Chain>> chain_pickup(std::vector<mfile0::M_Chain> &chain) {
	std::map<int64_t, std::vector<mfile0::M_Chain>> ret;
	int count = 0;
	int all = chain.size();

	for (auto &c : chain) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r group divide %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		int64_t groupid = c.basetracks.begin()->group_id/100000;
		auto res = ret.find(groupid);
		if (res == ret.end()) {
			std::vector<mfile0::M_Chain> c_tmp;
			c_tmp.push_back(c);
			ret.insert(std::make_pair(groupid, c_tmp));
		}
		else {
			res->second.push_back(c);
		}
	}
	fprintf(stderr, "\r group divide %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

	fprintf(stderr, "group size =%d\n", ret.size());
	return ret;
}

std::vector<std::vector<mfile0::M_Chain>> group_divide(std::map<int64_t, std::vector<mfile0::M_Chain>>&chain_map) {
	std::vector<std::vector<mfile0::M_Chain>> ret;

	int divide = 100;
	for (int i = 0; i < divide; i++) {
		std::vector<mfile0::M_Chain> tmp;
		ret.push_back(tmp);
	}

	int count = 0;
	int all = chain_map.size();
	for(auto itr=chain_map.begin();itr!=chain_map.end();itr++){
		if (count % 100 == 0) {
			fprintf(stderr, "\r divide group fill %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		int res = count % divide;
		count++;

		for(auto &c:itr->second){
			ret[res].push_back(c);
		}
	}
	fprintf(stderr, "\r divide group fill %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

	return ret;

}