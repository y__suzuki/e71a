#include "basetrack_pair.h"

double basetrack_pair::Get_dx_mean() {
	double dx1 = 0, num = 0;
	for (auto itr = dx.begin(); itr != dx.end(); itr++) {
		dx1 += *itr;
		num++;
	}
	if (num < 10) {
		return 0;
	}
	return  (dx1 / num);
}
double basetrack_pair::Get_dy_mean() {
	double dx1 = 0, num = 0;
	for (auto itr = dy.begin(); itr != dy.end(); itr++) {
		dx1 += *itr;
		num++;
	}
	if (num < 10) {
		return 0;
	}
	return  (dx1 / num);
}
double basetrack_pair::Get_dx_rms() {
	double dx1 = 0, dx2 = 0, num = 0;
	for (auto itr = dx.begin(); itr != dx.end(); itr++) {
		dx1 += *itr;
		dx2 += (*itr)*(*itr);
		num++;
	}
	if (num < 10) {
		return 0;
	}
	return sqrt(dx2 / num - (dx1 / num)*(dx1 / num));
}
double basetrack_pair::Get_dy_rms() {
	double dx1 = 0, dx2 = 0, num = 0;
	for (auto itr = dy.begin(); itr != dy.end(); itr++) {
		dx1 += *itr;
		dx2 += (*itr)*(*itr);
		num++;
	}
	if (num < 10) {
		return 0;
	}
	return sqrt(dx2 / num - (dx1 / num)*(dx1 / num));
}
std::pair<int, int> basetrack_pair::Get_area() {
	return area;
}
void basetrack_pair::SetArea(int area0, int area1) {
	area = std::make_pair(area0, area1);
}
void basetrack_pair::SetPair(int area0, vxx::base_track_t &b0, int area1, vxx::base_track_t &b1) {
	if (area.first != area0 || area.second != area1)return;
	double pos_slope[2], pos_intercept[2], ang_slope[2], ang_intercept[2];
	pos_intercept[0] = 10;
	pos_intercept[1] = 10;
	pos_slope[0] = 10;
	pos_slope[0] = 0;
	ang_intercept[0] = 0.01;
	ang_intercept[1] = 0.01;
	ang_slope[0] = 0.01;
	ang_slope[0] = 0;

	//角度小さい場合(10mrad以下)
	if (b0.ax*b0.ax + b0.ay*b0.ay < 0.01*0.01) {
		if (fabs(b0.x - b1.x) > pos_intercept[0])return;
		if (fabs(b0.y - b1.y) > pos_intercept[0])return;
		if (fabs(b0.ax - b1.ax) > ang_intercept[0])return;
		if (fabs(b0.ay - b1.ay) > ang_intercept[0])return;
		b_pair.push_back(std::make_pair(&(b0), &(b1)));
	}
	else {
		double d_pos[2], d_ang[2],ang;
		ang = sqrt(b0.ax*b0.ax + b0.ay*b0.ay);
		d_pos[0] = (b0.x - b1.x)*b0.ax + (b0.y - b1.y)*b0.ay;
		if (fabs(d_pos[0]) > (pos_intercept[0] + pos_slope[0] * ang)*ang)return;
		d_pos[1] = (b0.x - b1.x)*b0.ay - (b0.y - b1.y)*b0.ax;
		if (fabs(d_pos[1]) > (pos_intercept[1] + pos_slope[1] * ang)*ang)return;
		d_ang[0] = (b0.ax - b1.ax)*b0.ax + (b0.ay - b1.ay)*b0.ay;
		if (fabs(d_ang[0]) > (ang_intercept[0] + ang_slope[0] * ang)*ang)return;
		d_ang[1] = (b0.ax - b1.ax)*b0.ay - (b0.ay - b1.ay)*b0.ax;
		if (fabs(d_ang[1]) > (ang_intercept[1] + ang_slope[1] * ang)*ang)return;
		b_pair.push_back(std::make_pair(&(b0), &(b1)));
	}
}
void basetrack_pair::DeleteSamePair() {
	//まったく同じpairがあれば消す
}
void basetrack_pair::Setdifference(double area1[6], double area2[6]) {

}
