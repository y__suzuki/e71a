#pragma once
#include "VxxReader.h"

class basetrack_pair {
private:
	std::pair<int, int> area;
	std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> b_pair;
	std::vector<double> dx, dy;
public:
	double Get_dx_mean();
	double Get_dy_mean();
	double Get_dx_rms();
	double Get_dy_rms();
	std::pair<int, int>Get_area();
	void SetArea(int area0, int area1);
	void SetPair(int area0, vxx::base_track_t &b0, int area1, vxx::base_track_t &b1);
	void DeleteSamePair();
	void Setdifference(double area1[6], double area2[6]);
};
