#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

std::vector<std::vector<mfile0::M_Chain>> divide_event(mfile0::Mfile &m);
std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>&c);
std::vector<mfile0::M_Chain> penetrate_selection(std::vector<mfile0::M_Chain>&c);

bool sort_base(const mfile0::M_Base&left, const  mfile0::M_Base&right) {
	if (left.pos != right.pos) {
		return left.pos < right.pos;
	}
	return left.rawid < right.rawid;
}
int main(int argc, char**argv) {

	if (argc != 3) {
		fprintf(stderr, "usage:prg in-mfile out-path\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out_mfile_path = argv[2];
	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	mfile0::Mfile m_out;
	m_out.header = m.header;
	mfile0::Mfile m_out_one;
	m_out_one.header = m.header;

	int eventid = 0;
	std::vector<std::vector<mfile0::M_Chain>> event_chains = divide_event(m);
	std::vector<mfile0::M_Chain> all_chain;
	for (auto itr = event_chains.begin(); itr != event_chains.end(); itr++) {
		std::vector<mfile0::M_Chain> chains = penetrate_selection(*itr);
		m_out_one.chains = chains;
		eventid =(int) chains.begin()->basetracks.begin()->group_id / 10000;
		for (auto itr = chains.begin(); itr != chains.end(); itr++) {
			m_out.chains.push_back(*itr);
		}

		std::stringstream file_out_mfile_one;
		file_out_mfile_one << file_out_mfile_path << "\\event_" << std::setw(10) << std::setfill('0') << eventid << ".all";
		mfile0::write_mfile(file_out_mfile_one.str(), m_out_one);

	}

	std::string file_out_mfile = file_out_mfile_path + "\\m_all_eve2.all";
	mfile0::write_mfile(file_out_mfile, m_out);

}
std::vector<std::vector<mfile0::M_Chain>> divide_event(mfile0::Mfile &m) {
	std::vector<std::vector<mfile0::M_Chain>> ret;
	std::multimap<int64_t, mfile0::M_Chain> chain_map;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		chain_map.insert(std::make_pair(itr->basetracks.begin()->group_id / 10000, *itr));
	}
	int count = 0;
	for (auto itr = chain_map.begin(); itr != chain_map.end(); itr++) {
		count = chain_map.count(itr->first);
		auto range = chain_map.equal_range(itr->first);
		std::vector<mfile0::M_Chain> chain_v;
		for (auto res = range.first; res != range.second; res++) {
			chain_v.push_back(res->second);
		}
		ret.push_back(chain_v);
		itr = std::next(itr, count - 1);
	}
	return ret;
}

std::vector<mfile0::M_Chain> penetrate_selection(std::vector<mfile0::M_Chain>&c) {
	mfile0::M_Chain muon;
	std::vector<mfile0::M_Chain> ret;

	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->basetracks.begin()->group_id % 10000 == 0) {
			muon = *itr;
			ret.push_back(muon);
		}
	}

	int stopPL = muon.pos1 / 10;
	int pl0, pl1;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->chain_id == muon.chain_id)continue;
		pl0 = itr->pos0 / 10;
		pl1 = itr->pos1 / 10;
		if ((pl0 - stopPL)*(pl1 - stopPL) < 0)continue;
		ret.push_back(*itr);
	}
	return ret;

}