#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void each_pl_range(std::vector<mfile0::M_Chain> &c, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax);
void set_z(std::vector<mfile0::M_Chain> &c, std::map <int, double> &z);
void divide_downstream_chain(std::vector<mfile0::M_Chain> &c, std::vector<mfile0::M_Chain> &up, std::vector<mfile0::M_Chain> &down, int PL_thr);
void divide_edgeout_chain(std::vector<mfile0::M_Chain> &c, std::vector<mfile0::M_Chain> &out, std::vector<mfile0::M_Chain> &in, std::map <int, double> &z, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax);
void count_track(std::vector<mfile0::M_Chain> &c);
void output(std::string filename, std::vector<mfile0::M_Chain> &c);
void chain_angle_cut(std::vector<mfile0::M_Chain> &c, double thr);
bool VPH_cut(mfile0::M_Chain c);
std::vector<mfile0::M_Chain> start_track_selection(std::vector<mfile0::M_Chain> chain);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg in-mfile(txt) output-file\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::map <int, double> xmin, ymin, xmax, ymax, z;
	each_pl_range(m.chains, xmin, ymin, xmax, ymax);
	set_z(m.chains, z);
	std::vector<mfile0::M_Chain> up, down, in, out;
	divide_downstream_chain(m.chains, up, down, *m.header.all_pos.begin() / 10 + 2);
	divide_edgeout_chain(up, out, in, z, xmin, ymin, xmax, ymax);
	chain_angle_cut(in, 4);
	count_track(in);
	std::vector<mfile0::M_Chain> sel = start_track_selection(in);
	count_track(sel);
	output(file_out, sel);
}
void chain_angle_cut(std::vector<mfile0::M_Chain> &c, double thr) {
	std::vector<mfile0::M_Chain> sel;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		double ax = mfile0::chain_ax(*itr);
		double ay = mfile0::chain_ay(*itr);
		if (fabs(ax) > thr)continue;
		if (fabs(ay) > thr)continue;
		sel.push_back(*itr);
	}
	printf("chain angle cut %d --> %d\n", c.size(), sel.size());
	c.clear();
	c = sel;
	return;
}
void each_pl_range(std::vector<mfile0::M_Chain> &c, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax) {
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			auto res1 = xmin.insert(std::make_pair(itr->pos / 10, itr->x));
			auto res2 = xmax.insert(std::make_pair(itr->pos / 10, itr->x));
			auto res3 = ymin.insert(std::make_pair(itr->pos / 10, itr->y));
			auto res4 = ymax.insert(std::make_pair(itr->pos / 10, itr->y));
			if (!res1.second)res1.first->second = std::min(res1.first->second, itr->x);
			if (!res2.second)res2.first->second = std::max(res2.first->second, itr->x);
			if (!res3.second)res3.first->second = std::min(res3.first->second, itr->y);
			if (!res4.second)res4.first->second = std::max(res4.first->second, itr->y);
		}
	}

	auto itr1 = xmin.begin();
	auto itr2 = xmax.begin();
	auto itr3 = ymin.begin();
	auto itr4 = ymax.begin();
	for (auto itr = xmin.begin(); itr != xmin.end(); itr++) {
		if (itr->first != itr1->first || itr->first != itr2->first || itr->first != itr3->first || itr->first != itr4->first) {
			fprintf(stderr, "exception different PL\n");
			printf("xmin:PL%03d xmax:PL%03d ymin:PL%03d ymax:PL%03d\n", itr1->first, itr2->first, itr3->first, itr4->first);
			exit(1);
		}
		printf("PL%03d (%8.1lf, %8.1lf) (%8.1lf, %8.1lf)\n", itr->first, itr1->second, itr2->second, itr3->second, itr4->second);
		itr1++;
		itr2++;
		itr3++;
		itr4++;
	}
	return;
}
void set_z(std::vector<mfile0::M_Chain> &c, std::map <int, double> &z) {
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			z.insert(std::make_pair(itr->pos / 10, itr->z));
		}
	}

}
void divide_downstream_chain(std::vector<mfile0::M_Chain> &c, std::vector<mfile0::M_Chain> &up, std::vector<mfile0::M_Chain> &down, int PL_thr) {
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->pos0 / 10 <= PL_thr) {
			down.push_back(*itr);
		}
		else {
			up.push_back(*itr);
		}
	}
	printf("downstream track %lld\n", down.size());
	printf("upstream track %lld\n", up.size());

}
void divide_edgeout_chain(std::vector<mfile0::M_Chain> &c, std::vector<mfile0::M_Chain> &out, std::vector<mfile0::M_Chain> &in, std::map <int, double> &z, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax) {
	double z_down, x_down, y_down;
	int pl;
	double edge_cut = 5000;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		pl = itr->pos0 / 10;
		if (z.find(pl - 1) == z.end()) {
			fprintf(stderr, "z not found (PL%03d)\n", pl - 1);
			exit(1);
		}
		if (xmin.find(pl - 1) == xmin.end()) {
			fprintf(stderr, "xmin not found (PL%03d)\n", pl - 1);
			exit(1);
		}
		z_down = z.find(pl - 1)->second;
		x_down = itr->basetracks.begin()->x + itr->basetracks.begin()->ax*(z_down - itr->basetracks.rbegin()->z);
		y_down = itr->basetracks.begin()->y + itr->basetracks.begin()->ay*(z_down - itr->basetracks.rbegin()->z);
		if (xmin[pl - 1] + edge_cut > x_down)out.push_back(*itr);
		else if (ymin[pl - 1] + edge_cut > y_down)out.push_back(*itr);
		else if (xmax[pl - 1] - edge_cut < x_down)out.push_back(*itr);
		else if (ymax[pl - 1] - edge_cut < y_down)out.push_back(*itr);
		else if (xmin[pl] + edge_cut > itr->basetracks.begin()->x)out.push_back(*itr);
		else if (ymin[pl] + edge_cut > itr->basetracks.begin()->y)out.push_back(*itr);
		else if (xmax[pl] - edge_cut < itr->basetracks.begin()->x)out.push_back(*itr);
		else if (ymax[pl] - edge_cut < itr->basetracks.begin()->y)out.push_back(*itr);
		else {
			in.push_back(*itr);
		}
	}
	printf("edge out %lld\n", out.size());
	printf("stop     %lld\n", in.size());
}
void count_track(std::vector<mfile0::M_Chain> &c) {
	std::map<int, int> nseg, npl, stop;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		auto res1 = nseg.insert(std::make_pair(itr->nseg, 1));
		auto res2 = npl.insert(std::make_pair((itr->pos1 - itr->pos0) / 10 + 1, 1));
		auto res3 = stop.insert(std::make_pair(itr->pos1 / 10, 1));
		if (!res1.second)res1.first->second++;
		if (!res2.second)res2.first->second++;
		if (!res3.second)res3.first->second++;
	}
	for (auto itr = nseg.begin(); itr != nseg.end(); itr++) {
		printf("nseg %3d: %8d\n", itr->first, itr->second);
	}
	for (auto itr = npl.begin(); itr != npl.end(); itr++) {
		printf("npl %3d: %8d\n", itr->first, itr->second);
	}
	for (auto itr = stop.begin(); itr != stop.end(); itr++) {
		printf("stop pl %3d: %8d\n", itr->first, itr->second);
	}
}
void output(std::string filename, std::vector<mfile0::M_Chain> &c) {
	std::ofstream ofs(filename);
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(20) << std::setprecision(0) << itr->chain_id << " "
			<< std::setw(4) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(4) << std::setprecision(0) << itr->pos0 / 10 << " "
			<< std::setw(4) << std::setprecision(0) << itr->pos1 / 10 << " "
			<< std::setw(20) << std::setprecision(0) << itr->basetracks.begin()->group_id << " "
			<< std::setw(12) << std::setprecision(0) << itr->basetracks.begin()->rawid << " "
			<< std::setw(6) << std::setprecision(0) << itr->basetracks.begin()->ph << " "
			<< std::setw(7) << std::setprecision(4) << itr->basetracks.begin()->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->basetracks.begin()->ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->basetracks.begin()->x << " "
			<< std::setw(8) << std::setprecision(1) << itr->basetracks.begin()->y << " "
			<< std::setw(8) << std::setprecision(1) << itr->basetracks.begin()->z << std::endl;
	}

}
std::vector<mfile0::M_Chain> start_track_selection(std::vector<mfile0::M_Chain> chain) {
	std::vector<mfile0::M_Chain> ret;
	bool flg = false;
	int nPL = 0;
	double eff;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		flg = false;
		nPL = (itr->pos1 - itr->pos0) / 10 + 1;
		eff = itr->nseg*1.0 / nPL;
		if (eff < 0.500000000000000001)continue;
		if (itr->nseg == 2) {
			flg = VPH_cut(*itr);
		}
		else if (itr->nseg == 3) {
			flg = VPH_cut(*itr);
		}
		else if (itr->nseg == 4) {
			flg = VPH_cut(*itr);
		}
		else if (itr->nseg == 5) {
			flg = VPH_cut(*itr);
		}
		else {
			flg = true;
		}
		if (flg) {
			ret.push_back(*itr);
		}

	}

	fprintf(stderr, "start chain selection %d --> %d\n", chain.size(), ret.size());
	return ret;
}
bool VPH_cut(mfile0::M_Chain c) {
	double ax = 0, ay = 0, VPH = 0;
	int count = 0;

	for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
		ax += itr->ax;
		ay += itr->ay;
		VPH += itr->ph % 10000;
		count++;
	}
	ax = ax / count;
	ay = ay / count;
	VPH = VPH / count;
	double angle = sqrt(ax*ax + ay * ay);

	if (angle < 1.0) {
		return VPH > 190 - 70 * angle;
	}
	else if (1.0 <= angle && angle < 2.0) {
		return VPH > 150 - 30 * angle;
	}
	else if (2.0 <= angle && angle < 3.0) {
		return VPH > 110 - 10 * angle;
	}
	else if (3.0 <= angle) {
		return VPH > 95 - 5 * angle;
	}
	return false;
}