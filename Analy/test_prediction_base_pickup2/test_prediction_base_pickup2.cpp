#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#include <chrono>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

void chain_nseg_selection(std::vector<mfile0::M_Chain>&c);
void chain_PL_selection(std::vector<mfile0::M_Chain>&c, int pl);
void chain_mom_selection(std::vector<mfile0::M_Chain>&c, int pl);

std::set<int> prediction_base(std::vector<mfile0::M_Chain>&c, int pl);

int main(int argc,char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file_in_mfile file_in_base file_out_base\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_base = argv[2];
	std::string file_out_base = argv[3];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	chain_nseg_selection(m.chains);
	chain_PL_selection(m.chains, 85);
	chain_mom_selection(m.chains, 85);

	std::set<int> rawid_set = prediction_base(m.chains, 84);

	std::vector<vxx::base_track_t>base;
	std::vector<vxx::base_track_t>base_sel;
	vxx::BvxxReader br;
	base = br.ReadAll(file_in_base, 84, 0);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (rawid_set.count(itr->rawid) == 0)continue;
		if (itr->x < 10000)continue;
		if (itr->y < 10000)continue;
		if (itr->x > 100000)continue;
		if (itr->y > 80000)continue;
		base_sel.push_back(*itr);
	}

	vxx::BvxxWriter bw;
	bw.Write(file_out_base, 84, 0, base_sel);

}
void chain_nseg_selection(std::vector<mfile0::M_Chain>&c) {

	std::vector<mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->nseg < 10)continue;
		ret.push_back(*itr);
	}

	printf("nseg selection fin %lld --> %lld\n",c.size(),ret.size());
	std::swap(c, ret);
}
void chain_PL_selection(std::vector<mfile0::M_Chain>&c,int pl) {

	std::vector<mfile0::M_Chain> ret;
	int count = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		count = 0;
		for (auto &b : itr->basetracks) {
			if (abs(b.pos / 10 - pl) == 1)count++;
			if (abs(b.pos / 10 - pl) == 2)count++;

		}
		if (count == 4) {
			ret.push_back(*itr);
		}
	}

	printf("PL selection fin %lld --> %lld\n", c.size(), ret.size());
	std::swap(c, ret);
}
void chain_mom_selection(std::vector<mfile0::M_Chain>&c, int pl){
	std::vector<mfile0::M_Chain> ret;

	std::ofstream ofs("out_diff_theta.txt");
	for (auto &chain : c) {

		double diff1 = 0,diff2=0;
		int count = 0;
		for (int i = 0; i < chain.basetracks.size(); i++) {
			if (i + 1 == chain.basetracks.size())continue;
			if (chain.basetracks[i].pos / 10 == pl)continue;
			if (chain.basetracks[i + 1].pos / 10 == pl)continue;
			if (chain.basetracks[i].pos / 10 % 2 == 1)continue;
			if (chain.basetracks[i + 1].pos / 10 - chain.basetracks[i].pos / 10 != 1)continue;

			double ax0, ax1, ay0, ay1,diff;
			ax0 = chain.basetracks[i].ax;
			ax1 = chain.basetracks[i+1].ax;
			ay0 = chain.basetracks[i].ay;
			ay1 = chain.basetracks[i+1].ay;
			if (sqrt(ax0*ax0 + ay0 * ay0) > 0.01) {
				double denominator;
				denominator = 1 / sqrt(ax0*ax0 + ay0 * ay0 + 1);
				double t[3] = {
					ax0 *denominator,
				ay0 * denominator,
				1 * denominator
				};
				denominator = 1 / sqrt(ax0*ax0 + ay0 * ay0 + pow(ax0*ax0 + ay0 * ay0, 2));
				double r[3] = {
				-1 * ax0 * denominator,
				-1 * ay0 * denominator,
					(ax0*ax0 + ay0 * ay0)*denominator
				};
				denominator = 1 / sqrt(ax0*ax0 + ay0 * ay0);
				double l[3] = {
					-1 * ay0*denominator,
					ax0*denominator,
					0
				};
				
				denominator = 1 / (ax1*t[0] + ay1*t[1] + t[2]);
				diff = (ax1*l[0] + ay1 * l[1] + l[2])*denominator;
			}
			else {
				diff = ay1 - ay0;
			}
			diff1 += diff;
			diff2 += diff * diff;
			count++;
		}
		
		if (count < 5)continue;
		
		double diff_sig = sqrt(diff2 / count - pow(diff1 / count, 2));
		double ax = mfile0::chain_ax(chain);
		double ay = mfile0::chain_ay(chain);
		ofs << std::right<<std::fixed 
			<< std::setw(7) << std::setprecision(5) << ax << " "
			<< std::setw(7) << std::setprecision(5) << ay << " "
			<< std::setw(7) << std::setprecision(5) << diff_sig << std::endl;
		//if (diff_sig < 0.01) {
		if (diff_sig < 0.002) {
		//if (13.6 / diff_sig * sqrt(0.5*sqrt(1 + ax * ax + ay * ay) / 17.57) > 1000) {
			ret.push_back(chain);
		}

	}

	printf("mom selection fin %lld --> %lld\n", c.size(), ret.size());
	std::swap(c, ret);

}

std::set<int> prediction_base(std::vector<mfile0::M_Chain>&c, int pl) {
	std::set<int> ret;
	for (auto &chain : c) {
		for (auto itr = chain.basetracks.begin(); itr != chain.basetracks.end(); itr++) {
			if (itr->pos / 10 == pl) {
				ret.insert(itr->rawid);
			}
		}
	}
	return ret;
}