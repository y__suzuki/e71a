#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib,"FILE_structure.lib")

#include <VxxReader.h>
#include <FILE_structure.hpp>

#include <fstream>
#include <algorithm>
#include <map>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <omp.h>
#include <chrono>

class Prediction{
public:
	double x, y, ax, ay;
	int pl,hit;
};
void trans_base(std::vector<vxx::base_track_t>&base, corrmap0::Corrmap&corr);
std::vector<Prediction> read_prediction(std::string filename, int pl);
std::vector<Prediction> base_matching(std::vector<vxx::base_track_t>&base, std::vector<Prediction>&pred);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "in-bvxx pl in-merge-corr abs-corr prediction.txt\n");
		exit(1);
	}

	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_merge_corr = argv[3];
	std::string file_in_abs_corr = argv[4];
	std::string file_in_pred = argv[5];

	std::vector<corrmap0::Corrmap> corr_abs;
	std::vector<corrmap0::Corrmap> corr_area_merge;

	corrmap0::read_cormap(file_in_merge_corr, corr_area_merge);
	corrmap0::read_cormap(file_in_abs_corr, corr_abs);

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base, pl, 0);

	trans_base(base, *corr_area_merge.begin());
	corrmap0::Corrmap param_abs;
	for (int i = 0; i < corr_abs.size(); i++) {
		if (corr_abs[i].pos[0] / 10 == pl) {
			param_abs = corr_abs[i];
			break;
		}
	}
	trans_base(base, param_abs);
	std::vector<Prediction> pred = read_prediction(file_in_pred, pl);

	pred = base_matching(base, pred);
}
void trans_base(std::vector<vxx::base_track_t>&base, corrmap0::Corrmap&corr) {
	double tmp_x, tmp_y;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		tmp_x = itr->x;
		tmp_y = itr->y;

		itr->x = corr.position[0] * tmp_x + corr.position[1] * tmp_y + corr.position[4];
		itr->y = corr.position[2] * tmp_x + corr.position[3] * tmp_y + corr.position[5];

		tmp_x = itr->ax;
		tmp_y = itr->ay;

		itr->ax = corr.angle[0] * tmp_x + corr.angle[1] * tmp_y + corr.angle[4];
		itr->ay = corr.angle[2] * tmp_x + corr.angle[3] * tmp_y + corr.angle[5];

	}
}
std::vector<Prediction> read_prediction(std::string filename, int pl) {
	std::vector<Prediction> ret;
	Prediction pred;
	std::ifstream ifs(filename);
	uint64_t count = 0;
	while (ifs >> pred.pl >> pred.ax >> pred.ay >> pred.x >> pred.y >> pred.hit) {
		if (pl == pred.pl) {
			ret.push_back(pred);
		}
		count++;
		if (count % 10000 == 0) {
			fprintf(stderr, "\r prediction read ...%d",count);
		}
	}
	fprintf(stderr, "\r prediction read ...%d\n", count);

	return ret;

}
std::vector<Prediction> base_matching(std::vector<vxx::base_track_t>&base, std::vector<Prediction>&pred) {

	std::tuple<double, double, double, double >base_range;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			std::get<0>(base_range) = itr->x;
			std::get<1>(base_range) = itr->x;
			std::get<2>(base_range) = itr->y;
			std::get<3>(base_range) = itr->y;
		}
		std::get<0>(base_range) = std::min(std::get<0>(base_range), itr->x);
		std::get<1>(base_range) = std::max(std::get<1>(base_range), itr->x);
		std::get<2>(base_range) = std::min(std::get<2>(base_range), itr->y);
		std::get<3>(base_range) = std::max(std::get<3>(base_range), itr->y);
	}
	printf("base area %.1lf %.1lf %.1lf %.1lf\n", std::get<0>(base_range), std::get<1>(base_range), std::get<2>(base_range), std::get<3>(base_range));
	std::get<0>(base_range) += 5000;
	std::get<1>(base_range) -= 5000;
	std::get<2>(base_range) += 5000;
	std::get<3>(base_range) -= 5000;

	std::vector<Prediction> ret;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		if (std::get<0>(base_range) > itr->x)continue;
		if (std::get<1>(base_range) < itr->x)continue;
		if (std::get<2>(base_range) > itr->y)continue;
		if (std::get<3>(base_range) < itr->y)continue;
		itr->hit = 0;
		ret.push_back(*itr);
	}

	double x_min, y_min;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		y_min = std::min(itr->y, y_min);
	}

	std::multimap<std::pair<int, int>, vxx::base_track_t*>base_map;
	double hash = 1000;
	std::pair<int, int>id;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		id.first = (itr->x - x_min) / hash;
		id.second = (itr->y - y_min) / hash;
		base_map.insert(std::make_pair(id, &(*itr)));
	}
	std::ofstream ofs("diff.txt");
	double angle, all_pos[2], all_ang[2], diff_pos[2], diff_ang[2];
	int ix, iy;
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		ix = (itr->x - x_min) / hash;
		iy = (itr->y - y_min) / hash;
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		all_pos[0] = 50;
		all_pos[1] = 50 + 20 * angle;
		all_ang[0] = 0.02;
		all_ang[1] = 0.02 + 0.05 * angle;

		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				id.first = ix + iix;
				id.second = iy + iiy;
				if (base_map.count(id) == 0)continue;
				auto range = base_map.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					if (angle > 0.1) {
						diff_pos[0] = (res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax;
						diff_pos[1] = (res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay;
						diff_ang[0] = (res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax;
						diff_ang[1] = (res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay;
						if (fabs(diff_pos[0]) > all_pos[0] * angle)continue;
						if (fabs(diff_pos[1]) > all_pos[1] * angle)continue;
						if (fabs(diff_ang[0]) > all_ang[0] * angle)continue;
						if (fabs(diff_ang[1]) > all_ang[1] * angle)continue;
						ofs << angle << " " << diff_pos[0] / angle << " " << diff_pos[1] / angle << " " << diff_ang[0] / angle << " " << diff_ang[1] / angle << std::endl;
						itr->hit = 1;
						break;
					}
					else {
						diff_pos[0] = (res->second->x - itr->x);
						diff_pos[1] = (res->second->y - itr->y);
						diff_ang[0] = (res->second->ax - itr->ax);
						diff_ang[1] = (res->second->ay - itr->ay);
						if (fabs(diff_pos[0]) > all_pos[0])continue;
						if (fabs(diff_pos[1]) > all_pos[0])continue;
						if (fabs(diff_ang[0]) > all_ang[0])continue;
						if (fabs(diff_ang[1]) > all_ang[0])continue;
						ofs << angle << " " << diff_pos[0] << " " << diff_pos[1] << " " << diff_ang[0] << " " << diff_ang[1] << std::endl;
						itr->hit = 1;
						break;
					}
				}
			}
		}

	}


	int all, hit;
	double ang_min, ang_max;
	for (int i = 0; i < 40; i++) {
		all = 0;
		hit = 0;
		ang_min = i * 0.1;
		ang_max = (i + 1)*0.1;
		for (auto itr = ret.begin(); itr != ret.end(); itr++) {
			angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
			if (ang_min > angle)continue;
			if (ang_max <= angle)continue;
			all++;
			hit += itr->hit;
		}
		printf("%.1lf %.1lf %d %d %4.1lf\n", ang_min, ang_max, hit, all, hit*100. / all);
	}
	return ret;
}