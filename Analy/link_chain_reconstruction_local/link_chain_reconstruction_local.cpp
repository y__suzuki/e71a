//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
//大体2^50
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

//#pragma comment(lib, "VxxReader.lib")
//#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>


#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <stack>
#include <unordered_set>
#include <omp.h>
using namespace l2c;
// 自分で作った型
struct Segment {
	int16_t pos;
	int32_t rawid;
	bool operator==(const Segment& rhs) const
	{
		return pos == rhs.pos && rawid == rhs.rawid;
	}
	bool operator<(const Segment& rhs) const {
		if (pos == rhs.pos) {
			return rawid < rhs.rawid;
		}
		return pos < rhs.pos;
	}
};
class Line {
public:
	std::vector<int> points;
	int prev_edge;
	int next_edge;

};

// 自分で作った型をunorderdコンテナに入れたいときは、operator== の他に
// 型と同じ名前空間で hash_value 関数を定義

size_t hash_value(const Segment& d)
{
	// 複数の値のハッシュ値を組み合わせてハッシュ値を計算するには、
	// boost::hash_combine を使います。
	size_t h = 0;
	boost::hash_combine(h, d.pos);
	boost::hash_combine(h, d.rawid);
	return h;
}

class Chain_baselist {
public:
	int64_t groupid, trackid;
	std::pair<int32_t, int64_t> target_track;
	std::set<std::pair<int32_t, int64_t>> btset;
	std::set<std::tuple<int, int, int, int>>ltlist;
	std::vector<int32_t> usepos;
	int chain_num;
	std::vector<Linklet> make_link_list();
	void set_usepos();
};
class Chain_baselist_compress : public Chain_baselist
{
public:
	std::set<std::pair<int32_t, int64_t>> comp_btset;
	std::multimap<std::tuple<int, int, int, int>, std::vector<std::pair<int32_t, int64_t>>>comp_ltlist;
	std::set<std::tuple<int, int, int, int>>cut_ltlist;
	std::vector<Linklet> make_comp_link_list();
	std::vector<Linklet> make_comp_link_list(std::set<std::tuple<int, int, int, int>>&cut_list);
	void set_comp_usepos();

};

class output_format {
public:
	int groupid, trackid;
	int num_comfirmed_path, num_cut_path, num_select_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> comfirmed_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> cut_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> select_path;
};
class process_foramt {
	std::vector<output_format> out;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> cut_path;

};

bool sort_M_Base(const mfile0::M_Base &left, const mfile0::M_Base &right) {

	if (left.pos == right.pos) {
		return left.rawid < right.rawid;
	}
	return left.pos < right.pos;
}

std::vector < Chain_baselist > read_linklet_list2(std::string filename);
mfile0::Mfile SetMfile(std::vector < Chain_baselist >&c, std::multimap<int, mfile0::M_Base*>&base);
void apply_corrmap_local(std::multimap<int, mfile0::M_Base*>&base_map, std::map<int, std::vector<corrmap_3d::align_param2>>&corr);
int use_thread(double ratio, bool output);


int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file_in_link file-in-ECC file_out-mfile\n");

		exit(1);
	}

	std::string file_in_link_list = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_out_mfile = argv[3];

	std::vector<Chain_baselist> chain_list = read_linklet_list2(file_in_link_list);

	//新alignmentを使う場合
	std::string file_in_corrmap = file_in_ECC + "\\Area0\\0\\align\\fine\\local\\corrmap-local-abs.lst";

	std::map<int, std::vector<corrmap_3d::align_param>>corrmap = corrmap_3d::read_ali_param_abs(file_in_corrmap, 1);
	std::map<int, std::vector<corrmap_3d::align_param2>>corrmap_dd = corrmap_3d::DelaunayDivide_map(corrmap);

	//std::vector<corrmap0::Corrmap> corr;
	//corrmap0::read_cormap(file_in_ECC + "\\Area0\\0\\align\\corrmap-abs.lst", corr);
	//std::map<int, corrmap0::Corrmap> corr_map;
	//for (auto itr = corr.begin(); itr != corr.end(); itr++) {
	//	corr_map.insert(std::make_pair(itr->pos[0] / 10, *itr));
	//}
	////gap nominal read
	//std::stringstream structure_path;
	//structure_path << file_in_ECC << "\\st\\st.dat";
	//chamber1::Chamber chamber;
	//chamber1::read_structure(structure_path.str(), chamber);
	//std::map<int, double> z_map = chamber1::base_z_convert(chamber);

	//printf("z map ize=%d\n", z_map.size());

	std::multimap<int, mfile0::M_Base*>base;
	mfile0::Mfile m = SetMfile(chain_list, base);
	printf("base size =%d\n", base.size());
	vxx::BvxxReader br;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		//if (corr_map.count(itr->first) == 0)continue;
		//if (z_map.count(itr->first) == 0)continue;

		//corrmap0::Corrmap param = corr_map.at(itr->first);
		//double nominal_z = z_map.at(itr->first);
		printf("PL%03d read\n", itr->first);
		int raw_min = INT_MAX, raw_max = 0;
		std::vector< mfile0::M_Base*> base_vec;
		auto range = base.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_vec.push_back( res->second);
			raw_min = std::min(raw_min, int(res->second->rawid));
			raw_max = std::max(raw_max, int(res->second->rawid));
		}

		std::stringstream file_in_base;
		file_in_base << file_in_ECC << "\\Area0\\PL"
			<< std::setw(3) << std::setfill('0') << itr->first << "\\b"
			<< std::setw(3) << std::setfill('0') << itr->first << ".sel.cor.vxx";

		std::array<int, 2> index = { raw_min,raw_max + 1 };//1234<=rawid<=5678であるようなものだけを読む。

		std::vector<vxx::base_track_t> base_v = br.ReadAll(file_in_base.str(), itr->first, 0, vxx::opt::index = index);
		std::map<int, vxx::base_track_t> base_map;
		for (auto itr2 = base_v.begin(); itr2 != base_v.end(); itr2++) {
			base_map.insert(std::make_pair(itr2->rawid, *itr2));
		}

		for (auto itr2 = base_vec.begin(); itr2 != base_vec.end(); itr2++) {
			vxx::base_track_t base0 = base_map.at((*itr2)->rawid);
			(*itr2)->ax = base0.ax;
			(*itr2)->ay = base0.ay;
			(*itr2)->x = base0.x;
			(*itr2)->y = base0.y;
			(*itr2)->z = 0;
			(*itr2)->ph = base0.m[0].ph + base0.m[1].ph;
		}

		itr = std::next(itr, base.count(itr->first) - 1);
	}

	apply_corrmap_local(base, corrmap_dd);

	mfile0::write_mfile(file_out_mfile, m);

}


std::vector < Chain_baselist > read_linklet_list2(std::string filename) {
	std::vector < Chain_baselist > ret;
	std::ifstream ifs(filename);
	int gid, tid, muon_num, t_pl, t_rawid;
	int64_t link_num;
	int pl, raw;
	double weight;
	std::tuple<int, int, int, int> link;
	int count = 0;

	while (ifs >> gid >> tid >> t_pl >> t_rawid >> link_num) {
		printf("\r read group %d gid=%d link=%lld", count, gid, link_num);
		count++;
		Chain_baselist c;
		c.groupid = gid;
		c.trackid = tid;
		c.target_track.first = t_pl;
		c.target_track.second = t_rawid;
		for (int64_t i = 0; i < link_num; i++) {
			ifs >> std::get<0>(link) >> std::get<1>(link) >> std::get<2>(link) >> std::get<3>(link);
			c.btset.insert(std::make_pair(std::get<0>(link), std::get<2>(link)));
			c.btset.insert(std::make_pair(std::get<1>(link), std::get<3>(link)));
			c.ltlist.insert(link);
		}

		ret.push_back(c);
	}
	printf("\r read group %d gid=%d link=%lld\n", count, gid, link_num);

	return ret;

}

mfile0::Mfile SetMfile(std::vector < Chain_baselist >&c, std::multimap<int, mfile0::M_Base*>&base) {

	mfile0::Mfile ret;
	mfile0::set_header(3, 133, ret);
	int cid = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		for (auto itr2 = itr->btset.begin(); itr2 != itr->btset.end(); itr2++) {
			mfile0::M_Chain c;
			mfile0::M_Base b;

			b.group_id = itr->groupid;
			b.pos = itr2->first + 1;
			b.rawid = itr2->second;
			b.flg_d[0] = 0;
			b.flg_d[1] = 0;
			b.flg_i[0] = 0;
			b.flg_i[1] = 0;
			b.flg_i[2] = 0;
			b.flg_i[3] = 0;

			c.basetracks.push_back(b);
			c.chain_id = cid;
			cid++;
			c.nseg = c.basetracks.size();
			c.pos0 = c.basetracks.begin()->pos;
			c.pos1 = c.basetracks.rbegin()->pos;
			ret.chains.push_back(c);
		}
	}

	for (auto itr = ret.chains.begin(); itr != ret.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			base.insert(std::make_pair(itr2->pos / 10, &(*itr2)));
		}
	}

	return ret;
}

void apply_corrmap_local(std::multimap<int, mfile0::M_Base*>&base_map, std::map<int, std::vector<corrmap_3d::align_param2>>&corr) {

	//ここを書く
	std::set<int> all_pl_set;
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		all_pl_set.insert(itr->first);
	}
	std::vector<int> all_pl;
	for (auto itr = all_pl_set.begin(); itr != all_pl_set.end(); itr++) {
		all_pl.push_back(*itr);
	}

	int all = all_pl.size(), count = 0;
#pragma omp parallel for num_threads(use_thread(0.4,true)) schedule(dynamic,1)
	for (int i = 0; i < all_pl.size(); i++) {
		//for (auto itr = base_map_single.begin(); itr != base_map_single.end(); itr++) {
			//count = base_map_single.count(itr->first);
		int pl = all_pl[i];
#pragma omp critical
		{
			printf("PL%03d basetrack tans %3d/%3d\n", pl, count, all);
			count++;
			if (corr.count(pl) == 0) {
				fprintf(stderr, "PL%03d corrmap not found\n", pl);
				exit(1);
			}
			if (base_map.count(pl) == 0) {
				fprintf(stderr, "PL%03d basetrack not found\n", pl);
				exit(1);
			}
		}

		std::vector<corrmap_3d::align_param2> param = corr.at(pl);
		auto range = base_map.equal_range(pl);
		std::vector< mfile0::M_Base*> base_trans;
		base_trans.reserve(base_map.count(pl));
		for (auto itr = range.first; itr != range.second; itr++) {
			base_trans.push_back((itr->second));
		}
		std::vector <std::pair<mfile0::M_Base*, corrmap_3d::align_param2*>> base_trans_map = corrmap_3d::track_affineparam_correspondence(base_trans, param);
		trans_base_all(base_trans_map);

	}
}


int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
