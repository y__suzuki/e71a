#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <ios>
#include <iomanip> 

class microtrack_inf {
public:
	int zone, pos, col, row, isg, ph, pixelnum, hitnum;
};
std::vector<microtrack_inf> read_microtrack_inf(std::string filename);

int main(int argc, char ** argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:prg filename\n");
		exit(1);
	}
	std::string file_in = argv[1];
	std::vector<microtrack_inf> m = read_microtrack_inf(file_in);

	for (auto itr = m.begin(); itr != m.end(); itr++) {
		printf("%3d %2d %5d %5d %5d %3d %5d %5d\n", itr->pos, itr->zone, itr->col, itr->row, itr->isg, itr->ph, itr->hitnum, itr->pixelnum);
	}
}	
std::vector<microtrack_inf> read_microtrack_inf(std::string filename) {
	std::vector<microtrack_inf> ret;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	microtrack_inf m;
	while (ifs.read((char*)& m, sizeof(microtrack_inf))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(m);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;
}
