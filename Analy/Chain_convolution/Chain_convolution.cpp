//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
//大体2^50
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include "Graph_header.h"

#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <stack>
#include <unordered_set>
using namespace l2c;
// 自分で作った型
struct Segment {
	int16_t pos;
	int32_t rawid;
	bool operator==(const Segment& rhs) const
	{
		return pos == rhs.pos && rawid == rhs.rawid;
	}
	bool operator<(const Segment& rhs) const {
		if (pos == rhs.pos) {
			return rawid < rhs.rawid;
		}
		return pos < rhs.pos;
	}
};

// 自分で作った型をunorderdコンテナに入れたいときは、operator== の他に
// 型と同じ名前空間で hash_value 関数を定義

size_t hash_value(const Segment& d)
{
	// 複数の値のハッシュ値を組み合わせてハッシュ値を計算するには、
	// boost::hash_combine を使います。
	size_t h = 0;
	boost::hash_combine(h, d.pos);
	boost::hash_combine(h, d.rawid);
	return h;
}

class Chain_baselist {
public:
	int64_t groupid;
	std::set<std::pair<int32_t, int64_t>> muon_base;
	std::set<std::pair<int32_t, int64_t>> btset;
	std::set<std::tuple<int, int, int, int>>ltlist;
	std::vector<int32_t> usepos;
	int chain_num;
	std::vector<Linklet> make_link_list();
	void set_usepos();
};
class Chain_baselist_compress : public Chain_baselist
{
public:
	std::set<std::pair<int32_t, int64_t>> comp_btset;
	std::multimap<std::tuple<int, int, int, int>, std::vector<std::pair<int32_t, int64_t>>>comp_ltlist;
	std::set<std::tuple<int, int, int, int>>cut_ltlist;
	std::vector<Linklet> make_comp_link_list();
	std::vector<Linklet> make_comp_link_list(std::set<std::tuple<int, int, int, int>>&cut_list);
	void set_comp_usepos();

};

class output_format {
public:
	int gid;
	int num_comfirmed_path, num_cut_path, num_select_path;
	std::vector<std::pair<int,std::tuple<int, int, int, int>>> comfirmed_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> cut_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> select_path;
};

bool sort_M_Base(const mfile0::M_Base &left, const mfile0::M_Base &right) {

	if (left.pos == right.pos) {
		return left.rawid < right.rawid;
	}
	return left.pos < right.pos;
}

std::vector < Chain_baselist > read_linklet_list(std::string filename);
l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim= DEFAULT_CHAIN_UPPERLIM, bool output=false);

void output_linklet_list(std::string filename, std::vector<Chain_baselist>&chain_list);
void output_linklet_list_comp1(std::string filename, std::vector<Chain_baselist_compress>&chain_list);
void output_linklet_list_comp1_cut(std::string filename, std::vector<Chain_baselist_compress>&chain_list);
void output_linklet(std::string filename, std::vector<output_format> &out);

bool judeg_overflow(l2c::Cdat &cdat);
Chain_baselist link_convolution(Chain_baselist b, l2c::Cdat &cdat);
Chain_baselist link_convolution_closed_path(Chain_baselist b, l2c::Cdat &cdat);
Chain_baselist link_convolution_closed_path2(Chain_baselist b);
std::vector<Segment> cycle_pickup(mfile0::M_Chain &c0, mfile0::M_Chain&c1);
bool judeg_search(mfile0::M_Chain &c0, mfile0::M_Chain&c1);
void insert_all_cycles(std::vector<std::vector<Segment>>&all_cycles, std::vector<Segment>&input_seg);
bool judge_cycle_uniquebase(std::vector<Segment>&seg);
void link_convolution_closed_path3(Chain_baselist b);
void DFS_all_path(const std::vector < std::vector<int >> &G, int v, int p, std::vector<bool>&seen, std::stack<int>&hist, std::vector<bool>&finished, std::vector<std::vector<int>>&path);
void detect_branch_hangnail(boost::unordered_multimap<Segment, Segment> &link, boost::unordered_multimap<Segment, Segment> &link_inv, std::set<Segment> &seg_edge, std::set<Segment> &remove_vertex, std::set<std::tuple<int, int, int, int>>&remove_path);
Chain_baselist remove_branch_hangnail(Chain_baselist b);
Chain_baselist_compress path_compress(Chain_baselist b);
Chain_baselist_compress gragh_cut(Chain_baselist_compress &b);
bool Judge_complete_bipartite_graph(boost::unordered_multimap <int, int> &path_prev, boost::unordered_multimap <int, int> &path_next, std::set<int>&up, std::set<int>&down);
bool Judge_bipartite_graph(int target, boost::unordered_multimap <int, int> &path_prev, boost::unordered_multimap <int, int> &path_next, std::set<int>&up, std::set<int>&down);

l2c::Cdat l2c_x_one_chain(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos);
Chain_baselist_compress select_cross_path(Chain_baselist_compress &b, l2c::Cdat &cdat);
output_format change_format(Chain_baselist_compress &b, l2c::Cdat &cdat);
output_format cut_path_organize(output_format &out);


int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg file_in_link file_out_link\n");
		exit(1);
	}

	std::string file_in_link_list = argv[1];
	std::string file_out_link_list = argv[2];
	std::vector<Chain_baselist> chain_list = read_linklet_list(file_in_link_list);

	std::vector<Chain_baselist_compress> chain_list_2;
	std::vector<output_format> out_form;
	for (int i = 0; i < chain_list.size(); i++) {
		//if (chain_list[i].groupid != 73800000)continue;
		printf("%d\n", chain_list[i].groupid);
		chain_list[i].set_usepos();
		std::vector<Linklet> link = chain_list[i].make_link_list();
		if (chain_list[i].ltlist.size() == 0) {

		}
		else {
			//linkletの畳み込み
			l2c::Cdat cdat = l2c_x(chain_list[i].btset, link, chain_list[i].usepos, 1, false);
			chain_list[i] = link_convolution(chain_list[i], cdat);
			//閉路の畳み込み
			auto res = link_convolution_closed_path2(chain_list[i]);

			link = res.make_link_list();
			cdat = l2c_x(res.btset, link, res.usepos, 1, false);
			res = link_convolution(res, cdat);
			//ささくれの除去
			res = remove_branch_hangnail(res);
			//1本路の圧縮
			Chain_baselist_compress res2 = path_compress(res);
			//完全2部グラフ以外の切断
			res2 = gragh_cut(res2);
			link = res2.make_comp_link_list(res2.cut_ltlist);
			res2.set_comp_usepos();

			//連結成分の抽出
			cdat = l2c_x(res2.btset, link, res2.usepos,1,false);
			//format変更
			auto out_tmp = change_format(res2, cdat);
			//圧縮の解除、切断の連結
			out_tmp = cut_path_organize(out_tmp);

			out_form.push_back(out_tmp);

			chain_list_2.push_back(res2);

		}
		


	}
	//output_linklet_list_comp1_cut(file_out_link_list, chain_list_2);
	output_linklet(file_out_link_list, out_form);

}
std::vector < Chain_baselist > read_linklet_list(std::string filename) {
	std::vector < Chain_baselist > ret;
	std::ifstream ifs(filename);
	int gid, muon_num, link_num;
	int pl, raw;
	double weight;
	std::tuple<int, int, int, int> link;
	int count = 0;
	while (ifs >> gid >> muon_num >> link_num) {
		printf("\r read group %d", count);
		count++;
		Chain_baselist c;
		c.groupid = gid;
		for (int i = 0; i < muon_num; i++) {
			ifs >> pl >> raw;
			pl = 4;
			c.muon_base.insert(std::make_pair(pl, raw));
		}
		for (int i = 0; i < link_num; i++) {
			ifs >> std::get<0>(link) >> std::get<2>(link) >> std::get<1>(link) >> std::get<3>(link) >> weight;
			std::get<0>(link) = std::get<0>(link) * 10 + 1;
			std::get<1>(link) = std::get<1>(link) * 10 + 1;
			c.btset.insert(std::make_pair(std::get<0>(link) , std::get<2>(link)));
			c.btset.insert(std::make_pair(std::get<1>(link) , std::get<3>(link)));
			c.ltlist.insert(link);
		}
		ret.push_back(c);
	}
	printf("\r read group %d\n", count);

	return ret;

}

l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output) {
	try
	{
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		if (output) {
			l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
			return cdat;
		}
		l2c::Cdat cdat = l2c::MakeCdat_no_out(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
		return cdat;

	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}

l2c::Cdat l2c_x_one_chain(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos) {
	try
	{

		//std::vector<int32_t> usepos = { 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500 };
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		l2c::Cdat cdat = l2c::MakeCdat_no_out(ltlist, opt::usepos = usepos, opt::upperlim = 1, opt::output_isolated_linklet = true);

		return cdat;

		//中身を出力する。
		auto grsize = cdat.GetNumOfGroups();
		for (size_t grid = 0; grid < grsize; ++grid)
		{
			const Group& gr = cdat.GetGroup(grid);
			size_t chsize = gr.GetNumOfChains();
			if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
			int32_t spl = gr.GetStartPL();
			int32_t epl = gr.GetEndPL();

			//l2c-xのPLは一般に使われるPLとは意味が異なり、useposの中で"何番目のposであるか"を意味する。
			//LibL2c-xもl2c-xの仕様に倣うことにした（ただしl2c-xが1始まりなのに対してこちらは0始まり）。
			//例えば今回は、pos==310〜500の20枚でChain Groupを作成しているが、
			//GetStartPL()の戻り値が"3"であったとすると、これはこのGroupがusepos[3]すなわち380から始まっているという意味になる。

			fprintf(stdout, "Group ID:%-9llu nchain:%-7llu start:%-4d end:%-4d\n", gr.GetID(), chsize, spl, epl);
			if (gr.IsOverUpperLim())
			{
				//upperlimを超過している場合、chainの情報はない。
				//ただしchainの本数はGetNumOfChainsで正しく取得できる。
				//また属すBaseTrackの情報は保持しているので、GetBaseTracksで全BaseTrackを取得できる。
				fprintf(stdout, "over upperlim. nchain:%-9lld\n", chsize);
				continue;
			}
			for (size_t ich = 0; ich < chsize; ++ich)
			{
				Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
				int64_t chid = ch.GetID();
				int32_t nseg = ch.GetNSeg();
				int32_t spl = ch.GetStartPL();
				int32_t epl = ch.GetEndPL();
				fprintf(stdout, "    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d\n", chid, nseg, spl, epl);

				for (size_t pl = 0; pl < possize; ++pl)
				{
					//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
					//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
					BaseTrackID bt = ch.GetBaseTrack(pl);
					if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
					int32_t btpl = bt.GetPL();
					int64_t btid = bt.GetRawID();
					//btplとplは厳密に一致しなければおかしい。
					if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
					if (btset.find(std::make_pair(usepos[btpl], btid)) == btset.end())
					{
						//エラーチェック。
						//Chain内の全てのBaseTrackはbtset（ファイルから読み込まれた全BaseTrack）に必ず含まれているはず。
						//含まれていなければエラー。
						throw std::exception("BaseTrack is not found in btset.");
					}
					fprintf(stdout, "        pl:%-3d pos:%-4d rawid:%-9lld\n", btpl, usepos[pl], btid);
				}
			}

		}
	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}

std::vector<Linklet> Chain_baselist::make_link_list() {
	std::vector<Linklet> ret;
	ret.reserve(ltlist.size());
	for (auto itr = ltlist.begin(); itr != ltlist.end(); itr++) {
		ret.emplace_back(std::get<0>(*itr), std::get<1>(*itr), std::get<2>(*itr), std::get<3>(*itr));
	}
	return ret;
}
std::vector<Linklet> Chain_baselist_compress::make_comp_link_list() {
	std::vector<Linklet> ret;
	ret.reserve(comp_ltlist.size());
	for (auto itr = comp_ltlist.begin(); itr != comp_ltlist.end(); itr++) {
		ret.emplace_back(std::get<0>(itr->first), std::get<1>(itr->first), std::get<2>(itr->first), std::get<3>(itr->first));
	}
	return ret;
}
std::vector<Linklet> Chain_baselist_compress::make_comp_link_list(std::set<std::tuple<int, int, int, int>>&cut_list) {
	std::vector<Linklet> ret;
	ret.reserve(comp_ltlist.size());
	for (auto itr = comp_ltlist.begin(); itr != comp_ltlist.end(); itr++) {
		if (cut_list.count(itr->first))continue;
		ret.emplace_back(std::get<0>(itr->first), std::get<1>(itr->first), std::get<2>(itr->first), std::get<3>(itr->first));
	}
	return ret;

}

void Chain_baselist::set_usepos() {
	std::set<int> pos_set;
	for (auto itr = btset.begin(); itr != btset.end(); itr++) {
		pos_set.insert(itr->first);
	}
	usepos.clear();
	for (auto itr = pos_set.begin(); itr != pos_set.end(); itr++) {
		usepos.push_back(*itr);
	}
}
void Chain_baselist_compress::set_comp_usepos() {
	std::set<int> pos_set;
	for (auto itr = comp_btset.begin(); itr != comp_btset.end(); itr++) {
		pos_set.insert(itr->first);
	}
	usepos.clear();
	for (auto itr = pos_set.begin(); itr != pos_set.end(); itr++) {
		usepos.push_back(*itr);
	}
}




bool judeg_overflow(l2c::Cdat &cdat) {
	size_t grsize = cdat.GetNumOfGroups();
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		if (gr.IsOverUpperLim())return true;
	}
	return false;
}
Chain_baselist link_convolution(Chain_baselist b, l2c::Cdat &cdat) {
	Chain_baselist ret;
	ret.muon_base = b.muon_base;
	ret.groupid = b.groupid;

	size_t grsize = cdat.GetNumOfGroups();
	size_t possize = b.usepos.size();
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
		int32_t spl = gr.GetStartPL();
		int32_t epl = gr.GetEndPL();
		auto link_list = gr.GetLinklets();
		for (auto itr = link_list.begin(); itr != link_list.end(); itr++) {
			int32_t btpl0 = b.usepos[itr->first.GetPL()];
			int64_t btid0 = itr->first.GetRawID();
			int32_t btpl1 = b.usepos[itr->second.GetPL()];
			int64_t btid1 = itr->second.GetRawID();
			ret.btset.insert(std::make_pair(btpl0, btid0));
			ret.btset.insert(std::make_pair(btpl1, btid1));
			ret.ltlist.insert(std::make_tuple(btpl0, btpl1, btid0, btid1));
		}
	}
	ret.set_usepos();
	return ret;
}


void dfs(const std::vector < std::vector<int >> &G, int v, int p, std::vector<bool>&seen, std::stack<int>&hist, std::vector<bool>&finished,int &pos) {
	seen[v] = true;
	hist.push(v);
	//for (int i = 0; i < hist.size(); i++) {
	//	printf("%d --> ", hist[i]);
	//}
	//printf("\n");
	for (auto nv : G[v]) {
		if (nv == p) continue; // 逆流を禁止する

		// 完全終了した頂点はスルー
		if (finished[nv]) continue;

		//printf("%d --> %d\n", v, nv);
		// サイクルを検出
		if (seen[nv] && !finished[nv]) {
			pos = nv;
			return;
		}

		// 再帰的に探索
		dfs(G, nv, v,seen,hist,finished,pos);
		if (pos != -1) return;
	}
	hist.pop();
	finished[v] = true;
}
std::vector<int> outputcycle(std::stack<int>hist, int pos) {

	// サイクルを復元
	std::vector<int> cycle;
	while (!hist.empty()) {
		int t = hist.top();
		cycle.push_back(t);
		hist.pop();
		if (t == pos) break;
	}
	//for (int i = 0; i < cycle.size(); i++) {
	//	if (i + 1 != cycle.size()) {
	//		printf("%d-->", cycle[i]);
	//	}
	//	else {
	//		printf("%d\n", cycle[i]);
	//	}
	//}
	return cycle;

}

void dfs_search_cycle(const std::vector < std::vector<int >> &G, int v, int p, std::vector<bool>&seen, std::stack<int>&hist, std::vector<bool>&finished, int &pos, std::vector<std::vector<int>>&all_cycle) {
	seen[v] = true;
	hist.push(v);
	for (auto nv : G[v]) {
		if (nv == p) continue; // 逆流を禁止する

		// 完全終了した頂点はスルー
		if (finished[nv]) continue;

		//printf("%d --> %d\n", v, nv);
		// サイクルを検出
		if (seen[nv] && !finished[nv]) {
			pos = nv;
			all_cycle.push_back(outputcycle(hist, pos));
			continue;
		}

		// 再帰的に探索
		dfs_search_cycle(G, nv, v, seen, hist, finished, pos, all_cycle);
	}
	hist.pop();
	finished[v] = true;
}

void bfs_search_cycle_different_pl(Segment &start, boost::unordered_multimap<Segment, std::set<Segment>>&all_path, std::vector<std::vector<int>>&all_cycle) {

	std::vector<std::set<Segment>> path;
	std::set<Segment> finished;
	std::set<Segment> next;



}


std::vector<Segment> cycle_pickup(std::vector<Segment> &c0, std::vector<Segment> &c1) {
	//閉路1この保証あり
	std::set<Segment>btset;
	std::set<std::pair<Segment, Segment>> all_path;
	Segment seg0, seg1;
	for (int i = 0; i < c0.size(); i++) {
		btset.insert(c0[i]);
		if (i + 1 == c0.size())continue;
		all_path.insert(std::make_pair(c0[i], c0[i + 1]));
	}
	for (int i = 0; i < c1.size(); i++) {
		btset.insert(c1[i]);
		if (i + 1 == c1.size())continue;
		all_path.insert(std::make_pair(c1[i], c1[i + 1]));
	}

	std::map<int, Segment> index_vertex;
	std::map<Segment, int> vertex_index;
	int count = 0;
	for (auto itr = btset.begin(); itr != btset.end(); itr++) {
		seg0 = *itr;
		//seg0.rawid = itr->second;
		index_vertex.insert(std::pair(count, seg0));
		vertex_index.insert(std::pair(seg0, count));
		count++;
	}
	int N = count;
	std::vector < std::vector<int >> G(count);
	for (auto itr = all_path.begin(); itr != all_path.end(); itr++) {
		seg0 = itr->first;
		seg1 = itr->second;
		int index0 = vertex_index.at(seg0);
		int index1 = vertex_index.at(seg1);
		G[index0].push_back(index1);
		G[index1].push_back(index0);
	}
	// 探索
	std::vector<bool> seen, finished;
	seen.assign(N, false), finished.assign(N, false);
	int pos = -1;
	std::stack<int> hist; // 訪問履歴
	std::vector<std::set<int>>all_cycle;
	dfs(G, 0, -1, seen, hist, finished, pos);

	// サイクルを復元
	std::set<int> restored_cycle;
	while (!hist.empty()) {
		int t = hist.top();
		restored_cycle.insert(t);
		hist.pop();
		if (t == pos) break;
	}

	std::vector<Segment> cycle;
	for (auto itr = restored_cycle.begin(); itr != restored_cycle.end(); itr++) {
		cycle.push_back(index_vertex.at(*itr));
		//printf("(%d,%d)-->", index_vertex.at(*itr).pos, index_vertex.at(*itr).rawid);
		if (std::next(itr, 1) == restored_cycle.end()) {
			//printf("(%d,%d)\n", index_vertex.at(*restored_cycle.begin()).pos, index_vertex.at(*restored_cycle.begin()).rawid);
		}
	}
	return cycle;

}
bool judeg_search(std::vector<Segment> &c0, std::vector<Segment> &c1) {
	std::set<std::pair<int, int>>base_list;
	for (int i = 0; i < c0.size(); i++) {
		auto res = base_list.insert(std::make_pair(c0[i].pos, c0[i].rawid));
	}
	for (int i = 0; i < c1.size(); i++) {
		auto res = base_list.insert(std::make_pair(c1[i].pos, c1[i].rawid));
	}

	std::set<int>pos_list;
	for (auto itr = base_list.begin(); itr != base_list.end(); itr++) {
		auto res = pos_list.insert(itr->first);
		if (!res.second)return false;
	}
	return true;
}

Chain_baselist link_convolution_closed_path2(Chain_baselist b) {

	std::map<int, Segment> index_vertex;
	std::map<Segment, int> vertex_index;
	Segment seg0, seg1;
	int count = 0;
	for (auto itr = b.btset.begin(); itr != b.btset.end(); itr++) {
		seg0.pos = itr->first;
		seg0.rawid = itr->second;
		index_vertex.insert(std::pair(count, seg0));
		vertex_index.insert(std::pair(seg0, count));
		count++;
	}
	int N = count;
	std::vector < std::vector<int >> G(count);

	boost::unordered_map<Segment, std::set<Segment>>all_path;
	//同一posへのpathの切断
	for (auto itr = b.ltlist.begin(); itr != b.ltlist.end(); itr++) {
		seg0.pos = std::get<0>(*itr);
		seg0.rawid = std::get<2>(*itr);
		seg1.pos = std::get<1>(*itr);
		seg1.rawid = std::get<3>(*itr);
		auto res2 = all_path.find(seg0);
		if (res2 == all_path.end()) {
			std::set<Segment> set_tmp;
			set_tmp.insert(seg1);
			all_path.insert(std::make_pair(seg0, set_tmp));
		}
		else {
			res2->second.insert(seg1);
		}
		res2 = all_path.find(seg1);
		if (res2 == all_path.end()) {
			std::set<Segment> set_tmp;
			set_tmp.insert(seg0);
			all_path.insert(std::make_pair(seg1, set_tmp));
		}
		else {
			res2->second.insert(seg0);
		}
	}

	for (auto itr = all_path.begin(); itr != all_path.end();) {
		std::map<int, int> pos_map;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end();itr2++) {
			auto res = pos_map.insert(std::make_pair(itr2->pos, 1));
			if (!res.second)res.first->second++;
		}
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end();) {
			if (pos_map.at(itr2->pos) < 2) {
				itr2++;
			}
			else {
				itr2 = itr->second.erase(itr2);
			}
		}
		if (itr->second.size() == 0) {
			itr = all_path.erase(itr);
		}
		else {
			itr++;
		}
	}

	//経路の記憶
	for (auto itr = all_path.begin(); itr != all_path.end(); itr++) {
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (itr->first.pos > itr2->pos)continue;
			seg0 = itr->first;
			seg1 = *itr2;
			int index0 = vertex_index.at(seg0);
			int index1 = vertex_index.at(seg1);
			G[index0].push_back(index1);
			G[index1].push_back(index0);
		}
	}

	// 探索
	std::vector<bool> seen, finished;
	seen.assign(N, false), finished.assign(N, false);
	std::vector<std::vector<int>>all_cycle;
	for (int i = 0; i < N; i++) {
		int pos = -1;
		std::stack<int> hist; // 訪問履歴
		dfs_search_cycle(G, i, -1, seen, hist, finished, pos, all_cycle);
	}

	//for (int i = 0; i < all_cycle.size(); i++) {
	//	printf("%d %d\n", i, all_cycle[i].size());
	//	for (int j = 0; j < all_cycle[i].size(); j++) {
	//		auto ver0 = index_vertex.at(all_cycle[i][j]);
	//		auto ver1 = index_vertex.at(all_cycle[i][j]);
	//		if (j + 1 == all_cycle[i].size())ver1 = index_vertex.at(all_cycle[i][0]);
	//		else ver1 = index_vertex.at(all_cycle[i][j + 1]);
	//		if (ver0.pos > ver1.pos) std::swap(ver0, ver1);
	//		printf("%d %d %d %d\n", ver0.pos, ver0.rawid, ver1.pos, ver1.rawid);
	//	}
	//}

	//l2cを使って全経路を列挙
	std::vector<std::vector<Segment>>all_cycles;

	std::set<std::pair<int32_t, int64_t>> calc_path_btset;
	std::set<std::tuple<int,int,int,int>> set_calc_path_ltlist;
	std::vector<Linklet> calc_path_ltlist;
	std::set<int32_t> set_calc_path_usepos;
	std::vector<int32_t> calc_path_usepos;

	for (int i = 0; i < all_cycle.size(); i++) {
		for (int j = 0; j < all_cycle[i].size(); j++) {
			auto ver0 = index_vertex.at(all_cycle[i][j]);
			auto ver1 = index_vertex.at(all_cycle[i][j]);
			if (j + 1 == all_cycle[i].size())ver1 = index_vertex.at(all_cycle[i][0]);
			else ver1 = index_vertex.at(all_cycle[i][j + 1]);
			if (ver0.pos > ver1.pos) std::swap(ver0, ver1);
			calc_path_btset.insert(std::make_pair(ver0.pos, ver0.rawid));
			calc_path_btset.insert(std::make_pair(ver1.pos, ver1.rawid));
			set_calc_path_usepos.insert(ver0.pos);
			set_calc_path_usepos.insert(ver1.pos);
			set_calc_path_ltlist.insert(std::make_tuple(ver0.pos, ver1.pos, ver0.rawid, ver1.rawid));
			//printf("%d %d %d %d\n", ver0.pos, ver0.rawid, ver1.pos, ver1.rawid);
		}
	}
	calc_path_usepos.reserve(set_calc_path_usepos.size());
	for (auto itr = set_calc_path_usepos.begin(); itr != set_calc_path_usepos.end(); itr++) {
		calc_path_usepos.push_back(*itr);
	}
	calc_path_ltlist.reserve(set_calc_path_ltlist.size());
	for (auto itr = set_calc_path_ltlist.begin(); itr != set_calc_path_ltlist.end(); itr++) {
		calc_path_ltlist.emplace_back(std::get<0>(*itr), std::get<1>(*itr), std::get<2>(*itr), std::get<3>(*itr));
	}
	l2c::Cdat cdat_all_path = l2c_x(calc_path_btset, calc_path_ltlist, calc_path_usepos);

	size_t grsize = cdat_all_path.GetNumOfGroups();
	size_t possize = calc_path_usepos.size();
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat_all_path.GetGroup(grid);
		size_t chsize = gr.GetNumOfChains();
		if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
		int32_t spl = gr.GetStartPL();
		int32_t epl = gr.GetEndPL();
		if (gr.IsOverUpperLim())
		{
			//upperlimを超過している場合、chainの情報はない。
			//ただしchainの本数はGetNumOfChainsで正しく取得できる。
			//また属すBaseTrackの情報は保持しているので、GetBaseTracksで全BaseTrackを取得できる。
			fprintf(stdout, "over upperlim. nchain:%-9lld\n", chsize);
			exit(1);
		}
		else {
			std::vector<std::vector<Segment>>group_path;

			for (size_t ich = 0; ich < chsize; ++ich)
			{
				std::vector<Segment> chain_path;
				Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
				for (size_t pl = 0; pl < possize; ++pl)
				{
					BaseTrackID bt = ch.GetBaseTrack(pl);
					if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
					int32_t btpl = bt.GetPL();
					int64_t btid = bt.GetRawID();
					if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
					if (calc_path_btset.find(std::make_pair(calc_path_usepos[btpl], btid)) == calc_path_btset.end())
					{
						//エラーチェック。
						//Chain内の全てのBaseTrackはbtset（ファイルから読み込まれた全BaseTrack）に必ず含まれているはず。
						//含まれていなければエラー。
						throw std::exception("BaseTrack is not found in btset.");
					}
					Segment seg;
					seg.pos = calc_path_usepos[btpl];
					seg.rawid = btid;
					chain_path.push_back(seg);
				}
				group_path.push_back(chain_path);
			}
			for (int i = 0; i < group_path.size(); i++) {
				for (int j = i + 1; j < group_path.size(); j++) {
					//2このchainで同一PLの異なるbasetrackを持つ場合
					if (!judeg_search(group_path[i], group_path[j]))continue;

					std::vector<Segment> cycle_seg = cycle_pickup(group_path[i], group_path[j]);
					if (!judge_cycle_uniquebase(cycle_seg)) continue;
					insert_all_cycles(all_cycles, cycle_seg);
				}
			}
		}
	}

	//for (int i = 0; i < all_cycles.size(); i++) {
	//	for (int j = 0; j < all_cycles[i].size(); j++) {
	//		printf("(%d,%d)-->", all_cycles[i][j].pos, all_cycles[i][j].rawid);
	//		if (j + 1 == all_cycles[i].size()) {
	//			printf("(%d,%d)\n", all_cycles[i][0].pos, all_cycles[i][0].rawid);
	//		}
	//	}
	//}

	Chain_baselist ret = b;
	std::map < std::pair<Segment, Segment>, std::vector<std::pair<Segment, Segment>>>replacement_link_list;
	for (int i = 0; i < all_cycles.size(); i++) {
		std::map<int, Segment> seg_map;
		for (int j = 0; j < all_cycles[i].size(); j++) {
			seg_map.insert(std::make_pair(all_cycles[i][j].pos, all_cycles[i][j]));
		}
		for (auto itr = seg_map.begin(); itr != seg_map.end(); itr++) {
			//printf("%d %d\n", itr->second.pos, itr->second.rawid);
			if (std::next(itr, 1) == seg_map.end())continue;
			auto itr2 = std::next(itr, 1);
			ret.ltlist.insert(std::make_tuple(
				itr->second.pos,
				itr2->second.pos,
				itr->second.rawid,
				itr2->second.rawid
			));
		}
	}
	ret.set_usepos();
	return ret;
}

Chain_baselist link_convolution_closed_path(Chain_baselist b, l2c::Cdat &cdat) {
	return b;
	boost::unordered_multimap<Segment, Segment> link_id_up, link_id_down;
	Segment seg0, seg1;

	std::multimap<std::pair<Segment, Segment>, mfile0::M_Chain> chain_map;


	//中身を出力する。
	std::vector<mfile0::M_Chain> chains;
	size_t grsize = cdat.GetNumOfGroups();
	size_t possize = b.usepos.size();
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		size_t chsize = gr.GetNumOfChains();
		if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
		int32_t spl = gr.GetStartPL();
		int32_t epl = gr.GetEndPL();

		//l2c-xのPLは一般に使われるPLとは意味が異なり、useposの中で"何番目のposであるか"を意味する。
		//LibL2c-xもl2c-xの仕様に倣うことにした（ただしl2c-xが1始まりなのに対してこちらは0始まり）。
		//例えば今回は、pos==310〜500の20枚でChain Groupを作成しているが、
		//GetStartPL()の戻り値が"3"であったとすると、これはこのGroupがusepos[3]すなわち380から始まっているという意味になる。

		fprintf(stdout, "Group ID:%-9llu nchain:%-7llu start:%-4d end:%-4d\n", gr.GetID(), chsize, spl, epl);
		for (size_t ich = 0; ich < chsize; ++ich)
		{
			Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
			int64_t chid = ch.GetID();
			int32_t nseg = ch.GetNSeg();
			int32_t spl = ch.GetStartPL();
			int32_t epl = ch.GetEndPL();
			if (ich % 1000 == 0) {
				fprintf(stdout, "\r    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d", chid, nseg, spl, epl);
			}
			if (ich + 1 == chsize) {
				fprintf(stdout, "\r    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d\n", chid, nseg, spl, epl);
			}
			mfile0::M_Chain c;
			for (size_t pl = 0; pl < possize; ++pl)
			{
				//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
				//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
				BaseTrackID bt = ch.GetBaseTrack(pl);
				if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
				int32_t btpl = bt.GetPL();
				int64_t btid = bt.GetRawID();
				//btplとplは厳密に一致しなければおかしい。
				if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
				if (b.btset.find(std::make_pair(b.usepos[btpl], btid)) == b.btset.end())
				{
					//エラーチェック。
					//Chain内の全てのBaseTrackはbtset（ファイルから読み込まれた全BaseTrack）に必ず含まれているはず。
					//含まれていなければエラー。
					throw std::exception("BaseTrack is not found in btset.");
				}
				//auto res = base_map.find(std::make_pair(b.usepos[btpl] / 10, btid));
				//if (res == base_map.end()) {
				//	printf("PL%03d rawid=%lld not found\n", b.usepos[btpl] / 10, btid);
				//	continue;
				//}
				mfile0::M_Base m_b;
				m_b.pos = b.usepos[btpl];
				m_b.rawid = btid;
				m_b.group_id = b.groupid;
				if (b.muon_base.count(std::make_pair(b.usepos[btpl] / 10, btid)) == 1) {
					m_b.flg_i[0] = 1;
				}
				c.basetracks.push_back(m_b);
			}
			sort(c.basetracks.begin(), c.basetracks.end(), sort_M_Base);
			c.nseg = c.basetracks.size();
			c.pos0 = c.basetracks.begin()->pos;
			c.pos1 = c.basetracks.rbegin()->pos;
			c.chain_id = b.groupid + ich;
			chains.push_back(c);
		}
	}


	for (auto itr = chains.begin(); itr != chains.end(); itr++) {

		seg0.pos = itr->basetracks.begin()->pos;
		seg0.rawid = itr->basetracks.begin()->rawid;
		seg1.pos = itr->basetracks.rbegin()->pos;
		seg1.rawid = itr->basetracks.rbegin()->rawid;
		chain_map.insert(std::make_pair(std::make_pair(seg0, seg1), *itr));

	}
	std::vector<std::vector<Segment>>all_cycles;
	int count = 0;
	for (auto itr = chain_map.begin(); itr != chain_map.end(); itr++) {
		count = chain_map.count(itr->first);
		if (count >= 2) {
			auto range = chain_map.equal_range(itr->first);
			for (auto chain0 = range.first; chain0 != range.second; chain0++) {
				if (std::next(chain0, 1) == range.second)continue;
				for (auto chain1 = std::next(chain0, 1); chain1 != range.second; chain1++) {
					//2このchainで同一PLの異なるbasetrackを持つ場合
					if (!judeg_search(chain0->second, chain1->second))continue;
						
					//chain0-chain1の比較
					//printf("%d - %d\n", chain0->second.chain_id, chain1->second.chain_id);
					std::vector<Segment> cycle_seg = cycle_pickup(chain0->second, chain1->second);
					if(!judge_cycle_uniquebase(cycle_seg)) continue;
					insert_all_cycles(all_cycles, cycle_seg);

				}
			}
		}

		itr = std::next(itr, count - 1);
	}

	for (int i = 0; i < all_cycles.size(); i++) {
		for (int j = 0; j < all_cycles[i].size(); j++) {
			printf("(%d,%d)-->", all_cycles[i][j].pos, all_cycles[i][j].rawid);
			if (j + 1 == all_cycles[i].size()) {
				printf("(%d,%d)\n", all_cycles[i][0].pos, all_cycles[i][0].rawid);
			}
		}
	}

	Chain_baselist ret = b;
	std::map < std::pair<Segment, Segment>, std::vector<std::pair<Segment, Segment>>>replacement_link_list;
	for (int i = 0; i < all_cycles.size(); i++) {
		std::map<int, Segment> seg_map;
		for (int j = 0; j < all_cycles[i].size(); j++) {
			seg_map.insert(std::make_pair(all_cycles[i][j].pos, all_cycles[i][j]));
		}
		for (auto itr = seg_map.begin(); itr != seg_map.end(); itr++) {
			printf("%d %d\n", itr->second.pos, itr->second.rawid);
			if (std::next(itr, 1) == seg_map.end())continue;
			auto itr2 = std::next(itr, 1);
			ret.ltlist.insert(std::make_tuple(
				itr->second.pos,
				itr2->second.pos,
				itr->second.rawid,
				itr2->second.rawid
			));
		}
	}
	ret.set_usepos();
	return ret;

}
/*
Chain_baselist link_convolution_closed_path(Chain_baselist b, l2c::Cdat &cdat) {
	Chain_baselist ret;
	boost::unordered_multimap<Segment, Segment> link_id_up, link_id_down;
	Segment seg0, seg1;
	std::set<Segment> seg_all, start_seg_list;
	for (auto itr = b.ltlist.begin(); itr != b.ltlist.end(); itr++) {
		seg0.pos = std::get<0>(*itr);
		seg0.rawid = std::get<2>(*itr);
		seg1.pos = std::get<1>(*itr);
		seg1.rawid = std::get<3>(*itr);
		seg_all.insert(seg0);
		seg_all.insert(seg1);
		link_id_up.insert(std::make_pair(seg0, seg1));
		link_id_down.insert(std::make_pair(seg1, seg0));
	}
	for (auto itr = seg_all.begin(); itr != seg_all.end(); itr++) {
		if (link_id_down.count(*itr) == 0) {
			start_seg_list.insert(*itr);
		}
	}

	std::set<Segment> finished_seg;
	Segment search_seg;
	std::set<Segment> next_search_seg;
	std::vector<Segment> next_search_seg_v;
	int count;
	for (auto itr_down = start_seg_list.begin(); itr_down != start_seg_list.end(); itr_down++) {
		Segment search_seg = *itr_down;
		count = link_id_up.count(search_seg);
		if (count == 0) {
			fprintf(stderr, "error not found connection\n");
			exit(1);
		}
		else if (count == 1) {
			finished_seg.insert(search_seg);

		}
		else {

		}

	}






	seg0.pos = b.muon_base.begin()->first;
	seg0.rawid = b.muon_base.begin()->second;
	std::set<Segment> finished_seg;
	Segment search_seg;
	std::set<Segment> next_search_seg;
	std::vector<Segment> next_search_seg_v;

	search_seg = seg0;
	bool flg = true;
	while (flg) {
		//深さ優先探索
		flg = false;
		if (link_id.count(search_seg) == 0) {
			fprintf(stderr, "error not found connection\n");
			exit(1);
		}
		auto range = link_id.equal_range(search_seg);
		for (auto res = range.first; res != range.second; res++) {
			if (finished_seg.count(res->second) == 0) {
				flg = true;
				if (next_search_seg.count(res->second) == 0) {
					next_search_seg_v.push_back(res->second);
				}
			}
			else {
				//閉路あり
				//finished segにsearch_segを追加するとできる閉路
			}
		}
		finished_seg.insert(search_seg);
		search_seg = *(next_search_seg_v.end());
	}



}
*/
bool judeg_search(mfile0::M_Chain &c0, mfile0::M_Chain&c1) {
	std::set<std::pair<int,int>>base_list;
	for (int i = 0; i < c0.basetracks.size(); i++) {
		auto res = base_list.insert(std::make_pair(c0.basetracks[i].pos,c0.basetracks[i].rawid));
	}
	for (int i = 0; i < c1.basetracks.size(); i++) {
		auto res = base_list.insert(std::make_pair(c1.basetracks[i].pos, c1.basetracks[i].rawid));
	}

	std::set<int>pos_list;
	for (auto itr = base_list.begin(); itr != base_list.end(); itr++) {
		auto res = pos_list.insert(itr->first);
		if (!res.second)return false;
	}
	return true;
}
std::vector<Segment> cycle_pickup(mfile0::M_Chain &c0, mfile0::M_Chain&c1) {
	//閉路1この保証あり
	std::set<Segment>btset;
	std::set<std::pair<Segment, Segment>> all_path;
	Segment seg0, seg1;
	for (int i = 0; i < c0.basetracks.size(); i++) {
		seg0.pos = c0.basetracks[i].pos;
		seg0.rawid = c0.basetracks[i].rawid;
		btset.insert(seg0);
		if (i + 1 == c0.basetracks.size())continue;
		seg1.pos = c0.basetracks[i + 1].pos;
		seg1.rawid = c0.basetracks[i + 1].rawid;

		all_path.insert(std::make_pair(seg0, seg1));
	}
	for (int i = 0; i < c1.basetracks.size(); i++) {
		seg0.pos = c1.basetracks[i].pos;
		seg0.rawid = c1.basetracks[i].rawid;
		btset.insert(seg0);
		if (i + 1 == c1.basetracks.size())continue;
		seg1.pos = c1.basetracks[i + 1].pos;
		seg1.rawid = c1.basetracks[i + 1].rawid;

		all_path.insert(std::make_pair(seg0, seg1));
	}

	std::map<int, Segment> index_vertex;
	std::map<Segment, int> vertex_index;
	int count = 0;
	for (auto itr = btset.begin(); itr !=btset.end(); itr++) {
		seg0=*itr;
		//seg0.rawid = itr->second;
		index_vertex.insert(std::pair(count, seg0));
		vertex_index.insert(std::pair(seg0, count));
		count++;
	}
	int N = count;
	std::vector < std::vector<int >> G(count);
	for (auto itr = all_path.begin(); itr != all_path.end(); itr++) {
		seg0 = itr->first;
		seg1 = itr->second;
		int index0 = vertex_index.at(seg0);
		int index1 = vertex_index.at(seg1);
		G[index0].push_back(index1);
		G[index1].push_back(index0);
	}
	// 探索
	std::vector<bool> seen, finished;
	seen.assign(N, false), finished.assign(N, false);
	int pos = -1;
	std::stack<int> hist; // 訪問履歴
	std::vector<std::set<int>>all_cycle;
	dfs(G, 0, -1, seen, hist, finished, pos);

	// サイクルを復元
	std::set<int> restored_cycle;
	while (!hist.empty()) {
		int t = hist.top();
		restored_cycle.insert(t);
		hist.pop();
		if (t == pos) break;
	}

	std::vector<Segment> cycle;
	for (auto itr = restored_cycle.begin(); itr != restored_cycle.end(); itr++) {
		cycle.push_back(index_vertex.at(*itr));
		//printf("(%d,%d)-->", index_vertex.at(*itr).pos, index_vertex.at(*itr).rawid);
		if (std::next(itr, 1) == restored_cycle.end()) {
			//printf("(%d,%d)\n", index_vertex.at(*restored_cycle.begin()).pos, index_vertex.at(*restored_cycle.begin()).rawid);
		}
	}
	return cycle;

}
bool judge_cycle_uniquebase(std::vector<Segment>&seg) {
	std::set<std::pair<int, int>>base_list;
	for (int i = 0; i < seg.size(); i++) {
		auto res = base_list.insert(std::make_pair(seg[i].pos, seg[i].rawid));
	}

	std::set<int>pos_list;
	for (auto itr = base_list.begin(); itr != base_list.end(); itr++) {
		auto res = pos_list.insert(itr->first);
		if (!res.second)return false;
	}
	return true;



}
void output_linklet_list(std::string filename, std::vector<Chain_baselist>&chain_list) {

	std::ofstream ofs(filename);
	double weight = 1;
	for (auto itr = chain_list.begin(); itr != chain_list.end(); itr++) {
		if (itr->chain_num > 1) {
			printf("%d %d\n", itr->groupid, itr->chain_num);
		}
		ofs << std::fixed << std::right
			<< std::setw(3) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(3) << std::setprecision(0) << itr->muon_base.size() << " "
			<< std::setw(3) << std::setprecision(0) << itr->ltlist.size() << std::endl;
		std::vector<Linklet> link = itr->make_link_list();
		if (itr->muon_base.size() > 0) {
			for (auto itr2 = itr->muon_base.begin(); itr2 != itr->muon_base.end(); itr2++) {
				ofs << std::fixed << std::right
					<< std::setw(4) << std::setprecision(0) << itr2->first << " "
					<< std::setw(12) << std::setprecision(0) << itr2->second << std::endl;
			}
		}
		for (auto itr2 = link.begin(); itr2 != link.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(4) << std::setprecision(0) << itr2->pos1 / 10 << " "
				<< std::setw(12) << std::setprecision(0) << itr2->id1 << " "
				<< std::setw(4) << std::setprecision(0) << itr2->pos2 / 10 << " "
				<< std::setw(12) << std::setprecision(0) << itr2->id2 << " "
				<< std::setw(4) << std::setprecision(3) << weight << std::endl;
		}
	}
}

void output_linklet_list_comp1(std::string filename, std::vector<Chain_baselist_compress>&chain_list) {

	std::ofstream ofs(filename);
	double weight = 1;
	for (auto itr = chain_list.begin(); itr != chain_list.end(); itr++) {
		if (itr->chain_num > 1) {
			printf("%d %d\n", itr->groupid, itr->chain_num);
		}
		ofs << std::fixed << std::right
			<< std::setw(3) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(3) << std::setprecision(0) << itr->muon_base.size() << " "
			<< std::setw(3) << std::setprecision(0) << itr->comp_ltlist.size() << std::endl;
		if (itr->muon_base.size() > 0) {
			for (auto itr2 = itr->muon_base.begin(); itr2 != itr->muon_base.end(); itr2++) {
				ofs << std::fixed << std::right
					<< std::setw(4) << std::setprecision(0) << itr2->first << " "
					<< std::setw(12) << std::setprecision(0) << itr2->second << std::endl;
			}
		}
		for (auto itr2 = itr->comp_ltlist.begin(); itr2 != itr->comp_ltlist.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(4) << std::setprecision(0) << std::get<0>(itr2->first) / 10 << " "
				<< std::setw(12) << std::setprecision(0) << std::get<2>(itr2->first) << " "
				<< std::setw(4) << std::setprecision(0) << std::get<1>(itr2->first) / 10 << " "
				<< std::setw(12) << std::setprecision(0) << std::get<3>(itr2->first) << " "
				<< std::setw(6) << std::setprecision(3) << itr2->second.size() << std::endl;
		}
	}
}

void output_linklet_list_comp1_cut(std::string filename, std::vector<Chain_baselist_compress>&chain_list) {

	std::ofstream ofs(filename);
	double weight = 1;
	int link_num = 0;

	for (auto itr = chain_list.begin(); itr != chain_list.end(); itr++) {
		if (itr->chain_num > 1) {
			printf("%d %d\n", itr->groupid, itr->chain_num);
		}
		link_num = 0;
		for (auto itr2 = itr->comp_ltlist.begin(); itr2 != itr->comp_ltlist.end(); itr2++) {
			if (itr->cut_ltlist.count(itr2->first) == 1)continue;
			link_num++;
		}
		ofs << std::fixed << std::right
			<< std::setw(3) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(3) << std::setprecision(0) << itr->muon_base.size() << " "
			<< std::setw(3) << std::setprecision(0) << link_num << std::endl;
		if (itr->muon_base.size() > 0) {
			for (auto itr2 = itr->muon_base.begin(); itr2 != itr->muon_base.end(); itr2++) {
				ofs << std::fixed << std::right
					<< std::setw(4) << std::setprecision(0) << itr2->first << " "
					<< std::setw(12) << std::setprecision(0) << itr2->second << std::endl;
			}
		}
		for (auto itr2 = itr->comp_ltlist.begin(); itr2 != itr->comp_ltlist.end(); itr2++) {
			if (itr->cut_ltlist.count(itr2->first) == 1)continue;
			ofs << std::fixed << std::right
				<< std::setw(4) << std::setprecision(0) << std::get<0>(itr2->first) / 10 << " "
				<< std::setw(12) << std::setprecision(0) << std::get<2>(itr2->first) << " "
				<< std::setw(4) << std::setprecision(0) << std::get<1>(itr2->first) / 10 << " "
				<< std::setw(12) << std::setprecision(0) << std::get<3>(itr2->first) << " "
				<< std::setw(6) << std::setprecision(3) << itr2->second.size() << std::endl;
		}
	}
}

void output_linklet(std::string filename, std::vector<output_format> &out) {
	std::ofstream ofs(filename);
	double weight = 1;
	int link_num = 0;
	int all = out.size();
	int count = 0;
	for (auto itr = out.begin(); itr != out.end(); itr++) {

		if (count % 1000 == 0) {
			printf("\r write path %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;


		ofs << std::fixed << std::right
			<< std::setw(3) << std::setprecision(0) << itr->gid << " "
			<< std::setw(3) << std::setprecision(0) << itr->num_comfirmed_path << " "
			<< std::setw(3) << std::setprecision(0) << itr->num_cut_path << " "
			<< std::setw(3) << std::setprecision(0) << itr->num_select_path << std::endl;
		for (auto itr2 = itr->comfirmed_path.begin(); itr2 != itr->comfirmed_path.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(3) << std::setprecision(0) << itr2->first << " "
				<< std::setw(5) << std::setprecision(0) << std::get<0>(itr2->second) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(itr2->second) << std::endl;
		}

		for (auto itr2 = itr->cut_path.begin(); itr2 != itr->cut_path.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(3) << std::setprecision(0) << itr2->first << " "
				<< std::setw(5) << std::setprecision(0) << std::get<0>(itr2->second) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(itr2->second) << std::endl;
		}

		for (auto itr2 = itr->select_path.begin(); itr2 != itr->select_path.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(3) << std::setprecision(0) << itr2->first << " "
				<< std::setw(5) << std::setprecision(0) << std::get<0>(itr2->second) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(itr2->second) << std::endl;
		}
	}
	printf("\r write path %d/%d(%4.1lf%%)\n", count, all, count*100. / all);


}
void insert_all_cycles(std::vector<std::vector<Segment>>&all_cycles, std::vector<Segment>&input_seg) {
	std::vector < std::set<Segment>> all_cycles_set;
	for (int i = 0; i < all_cycles.size(); i++) {
		if (input_seg.size() != all_cycles[i].size())continue;
		std::set<Segment>cycles_set;
		for (int j = 0; j < all_cycles[i].size(); j++) {
			cycles_set.insert(all_cycles[i][j]);
		}
		all_cycles_set.push_back(cycles_set);
	}

	bool flg = true;
	int set_size = 0, hit_size = 0;
	for (int i = 0; i < all_cycles_set.size(); i++) {
		set_size = all_cycles_set[i].size();
		hit_size = 0;
		for (int j = 0; j < input_seg.size(); j++) {
			hit_size += all_cycles_set[i].count(input_seg[j]);
		}
		if (hit_size == set_size)flg = false;
	}
	if (flg) {
		all_cycles.push_back(input_seg);
	}

}

Chain_baselist remove_branch_hangnail(Chain_baselist b) {
	boost::unordered_multimap<Segment, Segment> link_id_up, link_id_down;
	Segment seg0, seg1;
	std::set<Segment> seg_all, down_seg_edge, up_seg_edge;
	for (auto itr = b.ltlist.begin(); itr != b.ltlist.end(); itr++) {
		seg0.pos = std::get<0>(*itr);
		seg0.rawid = std::get<2>(*itr);
		seg1.pos = std::get<1>(*itr);
		seg1.rawid = std::get<3>(*itr);
		seg_all.insert(seg0);
		seg_all.insert(seg1);
		link_id_up.insert(std::make_pair(seg0, seg1));
		link_id_down.insert(std::make_pair(seg1, seg0));
	}
	for (auto itr = seg_all.begin(); itr != seg_all.end(); itr++) {
		if (link_id_down.count(*itr) == 0) {
			down_seg_edge.insert(*itr);
		}
		if (link_id_up.count(*itr) == 0) {
			up_seg_edge.insert(*itr);
		}
	}
	std::set<Segment> remove_vertex;
	std::set<std::tuple<int, int, int, int>>remove_path;
	detect_branch_hangnail(link_id_up, link_id_down, up_seg_edge, remove_vertex, remove_path);
	detect_branch_hangnail(link_id_down, link_id_up, down_seg_edge, remove_vertex, remove_path);

	Chain_baselist ret;
	for (auto itr = b.btset.begin(); itr != b.btset.end(); itr++) {
		seg0.pos = itr->first;
		seg0.rawid = itr->second;
		if (remove_vertex.count(seg0) == 1)continue;
		ret.btset.insert(*itr);
	}
	for (auto itr = b.ltlist.begin(); itr != b.ltlist.end(); itr++) {
		if (remove_path.count(*itr) == 1)continue;
		ret.ltlist.insert(*itr);
	}
	ret.groupid = b.groupid;
	ret.muon_base = b.muon_base;
	ret.set_usepos();
	return ret;
}
void detect_branch_hangnail(boost::unordered_multimap<Segment, Segment> &link, boost::unordered_multimap<Segment, Segment> &link_inv, std::set<Segment> &seg_edge, std::set<Segment> &remove_vertex,std::set<std::tuple<int,int,int,int>>&remove_path) {
	Segment seg0, seg1;
	bool flg = false;
	for (auto itr = seg_edge.begin(); itr != seg_edge.end(); itr++) {
		auto range = link_inv.equal_range(*itr);
		flg = false;
		for (auto res = range.first; res != range.second; res++) {
			if (link.count(res->second) < 2)continue;
			auto range2 = link.equal_range(res->second);
			for (auto res2 = range.first; res2 != range.second;res2++) {
				//分岐のもう片方も端点だった場合は残す
				if (seg_edge.count(res2->second) == 1)continue;
				flg = true;
			}
		}
		//flg=trueでささくれ判定
		if (!flg)continue;
		seg0 = *itr;
		remove_vertex.insert(seg0);

		for (auto res = range.first; res != range.second; res++) {
			seg1 = res->second;
			if (seg0.pos < seg1.pos) {
				remove_path.insert(std::make_tuple(
					seg0.pos,
					seg1.pos,
					seg0.rawid,
					seg1.rawid
				));
			}
			else {
				remove_path.insert(std::make_tuple(
					seg1.pos,
					seg0.pos,
					seg1.rawid,
					seg0.rawid
				));
			}
		}
	}
}


std::vector<std::set<int >> straight_line(const std::vector < std::vector<int >> &G, const std::vector < std::vector<int >> &G_inv, std::stack<int>&hist, std::vector<bool>&finished) {
	std::vector < std::set<int >> ret;
	int count = 0;
	std::set<int >path;
	while (!hist.empty()) {
		int t = hist.top();
		if (G[t].size() != 1) {
			count++;
		}
		else {
			finished[t] = true;
		}
		path.insert(t);
		if (G_inv[t].size() > 1) {
			ret.push_back(path);
			path.clear();
			path.insert(t);
		}

		if (count == 2)break;
		hist.pop();
	}
	ret.push_back(path);

	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	if (std::next(itr, 1) != ret.end()) {
	//		printf("%d-->", *itr);
	//	}
	//	else {
	//		printf("%d\n", *itr);
	//	}
	//}
	return ret;
}
void print_hist(std::stack<int>hist) {
	while (!hist.empty()) {
		int t = hist.top();
		if (hist.size() > 1) {
			printf("%d-->", t);
		}
		else {
			printf("%d\n", t);
		}
		hist.pop();
	}
}
void DFS_straight_path(const std::vector < std::vector<int >> &G, const std::vector < std::vector<int >> &G_inv, int v, int p, std::vector<bool>&seen, std::stack<int>&hist, std::vector<bool>&finished, std::vector<std::set<int >> &clustered_index) {
	//print_hist(hist);
	seen[v] = true;
	hist.push(v);
	bool flg = true;
	for (auto nv : G[v]) {
		// 完全終了した頂点はスルー
		if (finished[nv]) continue;

		// 再帰的に探索
		DFS_straight_path(G,G_inv, nv, v, seen, hist, finished, clustered_index);
		//探索辺がfinishedになった場合
		if (finished[nv]) {
			//戻りながら分岐があるまでfinishにしていく
			//print_hist(hist);
			auto line_v = straight_line(G, G_inv, hist, finished);
			for (auto itr = line_v.begin(); itr != line_v.end(); itr++) {
				if (itr->size() <3)continue;
				clustered_index.push_back(*itr);

			}
			
		}
		//if (pos != -1) return;
	}
	//hist.pop();
	finished[v] = true;
	//v = hist.top();
}
bool set_match(std::set<int>&set0, std::set<int >&set1) {
	if (set0.size() != set1.size())return false;
	int count = 0;
	for (auto itr = set0.begin(); itr != set0.end(); itr++) {
		count += set1.count(*itr);
	}
	return set0.size() == count;
}
bool judge_push_back_cluster(std::vector<std::set<int >> &clustered_index, std::vector<std::pair<std::set<int>,std::set<int >>> &rhombus_cycle_index, std::set<int >&cluster) {
	if (cluster.size() < 3)return false;
	int count = 0;
	for (int i = 0; i < clustered_index.size(); i++) {
		count = 0;
		for (auto itr = cluster.begin(); itr != cluster.end(); itr++) {
			if (clustered_index[i].count(*itr) == 1)count++;
		}
		if (count == cluster.size())return false;
		//3点以上同一 --> 同じcluster(端点の有無)
		if (count > 2) {
			for (auto itr = cluster.begin(); itr != cluster.end(); itr++) {
				clustered_index[i].insert(*itr);
			}
			return false;
		}
		else if (count == 2) {
			//ひし形閉路
			//rhombus_cycle_indexに入れる
			int match_count = 0;
			std::set<int> add0 = clustered_index[i];
			std::set<int> add1 = cluster;

			for (int j = 0; j < rhombus_cycle_index.size(); j++) {
				match_count = 0;
				std::set<int> ori0 = rhombus_cycle_index[j].first;
				std::set<int> ori1 = rhombus_cycle_index[j].second;
				if (set_match(add0, ori0))match_count += 1;
				if (set_match(add1, ori0))match_count += 1;
				if (set_match(add0, ori1))match_count += 1;
				if (set_match(add1, ori1))match_count += 1;
				if (match_count < 2) {
					rhombus_cycle_index.push_back(std::make_pair(add0, add1));
				}
				else if (match_count > 2) {
					printf("\n rhombus cycle path error\n");
					for (auto itr = cluster.begin(); itr != cluster.end(); itr++) {
						printf("%d ", *itr);
					}
					printf("\n");
					for (auto itr = clustered_index[i].begin(); itr != clustered_index[i].end(); itr++) {
						printf("%d ", *itr);
					}
					printf("\n");
				}
			}
			//同時にclustered_indexにも入る
			return true;
		}
	}
	return true;
}

void DFS_straight_path(const std::vector < std::vector<int >> &G, const std::vector < std::vector<int >> &G_inv, int start, std::vector<bool>&finished,std::vector<int> &hist ,std::vector<std::set<int >> &clustered_index, std::vector<std::pair<std::set<int>, std::set<int >>> &rhombus_cycle_index) {
	//点startに立ち寄る
	hist.push_back(start);
	for (int i = 0; i < G[start].size(); i++) {
		int next = G[start][i];
		if (finished[next]) {
			//次の辺が探索済み+後方分岐の場合次の辺まで入れて探索
			if (G_inv[next].size() != 1) {
				std::set<int > cluster;
				cluster.insert(next);
				for (auto itr = hist.rbegin(); itr != hist.rend(); itr++) {
					bool flg = true;
					cluster.insert(*itr);
					if (G[*itr].size() != 1 || G_inv[*itr].size() != 1) {
						if (judge_push_back_cluster(clustered_index, rhombus_cycle_index, cluster)) {
							clustered_index.push_back(cluster);
						}
						cluster.clear();
						cluster.insert(*itr);
					}
				}
			}
		}
		DFS_straight_path(G, G_inv, next, finished,hist, clustered_index, rhombus_cycle_index);
	}
	//ここで点stratに対しての探索が終了する
	finished[start] = true;
	//startが一本道ではない場合
	if (G[start].size() != 1) {
		
		std::set<int > cluster;
		for (auto itr = hist.rbegin(); itr != hist.rend(); itr++) {
			bool flg = true;
			//if (std::next(itr, 1) != hist.rend()) {
			//	printf("%d-->", *itr);
			//}
			//else {
			//	printf("%d\n", *itr);
			//}
			cluster.insert(*itr);
			if (itr != hist.rbegin()) {
				if (G[*itr].size() != 1|| G_inv[*itr].size() != 1) {
					if (judge_push_back_cluster(clustered_index,rhombus_cycle_index, cluster)) {
						clustered_index.push_back(cluster);
					}
					cluster.clear();
					cluster.insert(*itr);
				}
			}

		}
	}
	hist.pop_back();
}
/////実装
void DFS_straight_path_compress(boost::unordered_multimap <int, Line> &line_prev, boost::unordered_multimap <int, Line> &line_next,int start, std::unordered_set<int>&edge, std::vector<std::vector<int>> & compress_point_list){
	
	auto range = line_next.equal_range(start);
	for (auto res = range.first; res != range.second; res++) {
		std::vector<int> hist;
		hist.push_back(start);
		int next = res->second.next_edge;

		while (edge.count(next) == 0) {
			//line_next.count(next)==1&&line_prev.count(next)==1
			//の条件と等価
			hist.push_back(next);
			next = line_next.find(next)->second.next_edge;
		}
		hist.push_back(next);
		compress_point_list.push_back(hist);
	}

}
Chain_baselist_compress path_compress(Chain_baselist b) {
	std::map<int, Segment> index_vertex;
	std::map<Segment, int> vertex_index;
	Segment seg0, seg1;
	int count = 0;
	for (auto itr = b.btset.begin(); itr != b.btset.end(); itr++) {
		seg0.pos = itr->first;
		seg0.rawid = itr->second;
		index_vertex.insert(std::pair(count, seg0));
		vertex_index.insert(std::pair(seg0, count));
		count++;
	}
	int N = count;
	std::unordered_set<int> all_edge;
	boost::unordered_multimap <int, Line> line_prev, line_next;
	for (auto itr = b.ltlist.begin(); itr != b.ltlist.end(); itr++) {
		seg0.pos = std::get<0>(*itr);
		seg0.rawid = std::get<2>(*itr);
		seg1.pos = std::get<1>(*itr);
		seg1.rawid = std::get<3>(*itr);
		int index0 = vertex_index.at(seg0);
		int index1 = vertex_index.at(seg1);
		all_edge.insert(index0);
		all_edge.insert(index1);
		Line line;
		line.prev_edge = index0;
		line.next_edge = index1;
		line_next.insert(std::make_pair(index0, line));
		line_prev.insert(std::make_pair(index1, line));
	}
	//分岐,端点を記録
	std::unordered_set<int>edge;
	for (auto itr = all_edge.begin(); itr != all_edge.end(); itr++) {
		if (line_prev.count(*itr) != 1 || line_next.count(*itr) != 1) {
			edge.insert(*itr);
			//printf("branch: %d %d\n", index_vertex.at(*itr).pos, index_vertex.at(*itr).rawid);
		}
	}
	std::vector<std::vector<int>> compress_point_list;
	for (auto itr = edge.begin(); itr != edge.end(); itr++) {
		//edge-->edge の経路を縮退させる
		if (line_next.count(*itr) == 0)continue;
		DFS_straight_path_compress(line_prev, line_next, *itr, edge, compress_point_list);
	}
	//printf("compressed line\n");
	//for (int i = 0; i < compress_point_list.size(); i++) {
	//	for (int j = 0; j < compress_point_list[i].size(); j++) {
	//		if (j + 1 != compress_point_list[i].size()) {
	//			printf("(%d,%d)-->", index_vertex.at(compress_point_list[i][j]).pos, index_vertex.at(compress_point_list[i][j]).rawid);
	//		}
	//		else {
	//			printf("(%d,%d)\n", index_vertex.at(compress_point_list[i][j]).pos, index_vertex.at(compress_point_list[i][j]).rawid);
	//		}
	//	}
	//}
	std::unordered_set<int> new_edge;
	std::vector<Line>new_line;

	for (int i = 0; i < compress_point_list.size(); i++) {
		Line l;
		for (int j = 0; j < compress_point_list[i].size(); j++) {
			l.points.push_back(compress_point_list[i][j]);
		}
		l.prev_edge = *l.points.begin();
		l.next_edge = *l.points.rbegin();

		new_line.push_back(l);
		new_edge.insert(l.next_edge);
		new_edge.insert(l.prev_edge);
	}

	Chain_baselist_compress ret;
	ret.groupid = b.groupid;
	ret.muon_base = b.muon_base;
	ret.btset = b.btset;
	ret.ltlist = b.ltlist;

	for (auto itr = new_edge.begin(); itr != new_edge.end(); itr++) {
		auto res = index_vertex.at(*itr);
		ret.comp_btset.insert(std::make_pair(res.pos, res.rawid));
	}
	for (auto itr = new_line.begin(); itr != new_line.end(); itr++) {
		std::vector<std::pair<int32_t, int64_t>> comp_lt;
		std::tuple<int, int, int, int> lt;
		for (auto itr2 = itr->points.begin(); itr2 != itr->points.end(); itr2++) {
			auto res = index_vertex.at(*itr2);
			comp_lt.push_back(std::make_pair(res.pos, res.rawid));
		}
		auto res0 = index_vertex.at(itr->prev_edge);
		auto res1 = index_vertex.at(itr->next_edge);
		std::get<0>(lt) = res0.pos;
		std::get<1>(lt) = res1.pos;
		std::get<2>(lt) = res0.rawid;
		std::get<3>(lt) = res1.rawid;
		ret.comp_ltlist.insert(std::make_pair(lt, comp_lt));
	}

	return ret;
}

Chain_baselist_compress gragh_cut(Chain_baselist_compress &b) {

	std::map<int, Segment> index_vertex;
	std::map<Segment, int> vertex_index;
	Segment seg0, seg1;
	int count = 0;
	for (auto itr = b.comp_btset.begin(); itr != b.comp_btset.end(); itr++) {
		seg0.pos = itr->first;
		seg0.rawid = itr->second;
		index_vertex.insert(std::pair(count, seg0));
		vertex_index.insert(std::pair(seg0, count));
		count++;
	}
	int N = count;
	std::unordered_set<int> all_edge;
	boost::unordered_multimap <int,int> path_prev,path_next;
	for (auto itr = b.comp_ltlist.begin(); itr != b.comp_ltlist.end(); itr++) {
		seg0.pos = std::get<0>(itr->first);
		seg0.rawid = std::get<2>(itr->first);
		seg1.pos = std::get<1>(itr->first);
		seg1.rawid = std::get<3>(itr->first);
		int index0 = vertex_index.at(seg0);
		int index1 = vertex_index.at(seg1);
		all_edge.insert(index0);
		all_edge.insert(index1);
		path_next.insert(std::make_pair(index0, index1));
		path_prev.insert(std::make_pair(index1, index0));
	}

	std::unordered_set<int> fin_edge;
	boost::unordered_set <std::pair<int, int>> delete_path;
	for (auto itr = all_edge.begin(); itr != all_edge.end(); itr++) {
		if (fin_edge.count(*itr) == 1)continue;
		//2部グラフ判定
		std::set<int>up, down;
		//2部グラフではない
		if (!Judge_bipartite_graph(*itr, path_prev, path_next, up, down)) {
			if (path_next.count(*itr) != 0) {
				auto range = path_next.equal_range(*itr);
				for (auto res = range.first; res != range.second; res++) {
					delete_path.insert(*res);
				}
			}
			fin_edge.insert(*itr);
		}
		//2部グラフである
		else {
			//完全2部グラフでない
			//完全2部グラフK (n,n) ではない
			if (!Judge_complete_bipartite_graph(path_prev, path_next, up, down)
				||
				up.size() != down.size()) {
				for (auto itr2 = up.begin(); itr2 != up.end(); itr2++) {
					auto range = path_next.equal_range(*itr2);
					for (auto res = range.first; res != range.second; res++) {
						delete_path.insert(*res);
					}
					fin_edge.insert(*itr2);
				}
			}
			else {
				//完全2部グラフK (n,n) である
				for (auto itr2 = up.begin(); itr2 != up.end(); itr2++) {
					fin_edge.insert(*itr2);
				}
			}

		}
	}


	std::multimap<std::tuple<int, int, int, int>, std::vector<std::pair<int32_t, int64_t>>>comp_ltlist;
	for (auto itr = b.comp_ltlist.begin(); itr != b.comp_ltlist.end(); itr++) {
		seg0.pos = std::get<0>(itr->first);
		seg0.rawid = std::get<2>(itr->first);
		seg1.pos = std::get<1>(itr->first);
		seg1.rawid = std::get<3>(itr->first);
		int index0 = vertex_index.at(seg0);
		int index1 = vertex_index.at(seg1);

		if (delete_path.count(std::make_pair(index0,index1)) == 1) {
			//削除する場合
			b.cut_ltlist.insert(itr->first);
		}
	}
	return b;

}
bool Judge_bipartite_graph(int target, boost::unordered_multimap <int, int> &path_prev, boost::unordered_multimap <int, int> &path_next, std::set<int>&up, std::set<int>&down) {
	up.insert(target);
	int sum = 0, sum_p = -1;
	while (sum != sum_p) {
		sum_p = sum;
		for (auto itr = up.begin(); itr != up.end(); itr++) {
			if (path_next.count(*itr) == 0)continue;
			auto range = path_next.equal_range(*itr);
			for (auto res = range.first; res != range.second; res++) {
				down.insert(res->second);
			}
		}
		for (auto itr = down.begin(); itr != down.end(); itr++) {
			if (path_prev.count(*itr) == 0)continue;
			auto range = path_prev.equal_range(*itr);
			for (auto res = range.first; res != range.second; res++) {
				up.insert(res->second);
			}
		}
		sum = up.size() + down.size();
	}

	//2部グラフの判定
	bool flg = true;
	for (auto itr = up.begin(); itr != up.end(); itr++) {
		if (down.count(*itr) == 1)flg = false;
	}
	for (auto itr = down.begin(); itr != down.end(); itr++) {
		if (up.count(*itr) == 1)flg = false;
	}
	
	return flg;
}
bool Judge_complete_bipartite_graph(boost::unordered_multimap <int, int> &path_prev, boost::unordered_multimap <int, int> &path_next, std::set<int>&up, std::set<int>&down) {
	bool flg = true;
	for (auto itr = up.begin(); itr != up.end(); itr++) {
		if (path_next.count(*itr) != down.size())return false;
	}
	for (auto itr = down.begin(); itr != down.end(); itr++) {
		if (path_prev.count(*itr) != up.size())return false;
	}
	return true;
}


output_format change_format(Chain_baselist_compress &b, l2c::Cdat &cdat) {
	output_format ret;
	//中身を出力する。
	size_t possize = b.usepos.size();
	//中身を出力する。
	size_t grsize = cdat.GetNumOfGroups();
	std::set<std::tuple<int, int, int, int>> saved_path;

	int path_id = 0;
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		size_t chsize = gr.GetNumOfChains();
		if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
		int32_t spl = gr.GetStartPL();
		int32_t epl = gr.GetEndPL();
		//fprintf(stdout, "Group ID:%-9llu nchain:%-7llu start:%-4d end:%-4d\n", gr.GetID(), chsize, spl, epl);
		if (gr.IsOverUpperLim())
		{
			auto basetracks = gr.GetBaseTracks();
			std::set<std::pair<int, int>>base_list;
			for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
				int pos = b.usepos[itr->GetPL()];
				int rawid = itr->GetRawID();
				base_list.insert(std::make_pair(pos, rawid));
			}
			for (auto itr = b.comp_ltlist.begin(); itr != b.comp_ltlist.end(); itr++) {
				std::pair<int, int> bt0, bt1;
				bt0.first = std::get<0>(itr->first);
				bt0.second = std::get<2>(itr->first);
				bt1.first = std::get<1>(itr->first);
				bt1.second = std::get<3>(itr->first);
				if (base_list.count(bt0) == 1 && base_list.count(bt1) == 1) {
					for (int i = 0; i < itr->second.size() - 1; i++) {
						ret.select_path.push_back(std::make_pair(path_id,
							std::make_tuple(itr->second[i].first, itr->second[i + 1].first, itr->second[i].second, itr->second[i + 1].second)));
					}
				}
				saved_path.insert(itr->first);
			}
			path_id++;
			//upperlimを超過している場合、chainの情報はない。
			//ただしchainの本数はGetNumOfChainsで正しく取得できる。
			//また属すBaseTrackの情報は保持しているので、GetBaseTracksで全BaseTrackを取得できる。
			//fprintf(stdout, "over upperlim. nchain:%-9lld\n", chsize);
			continue;
		}
		for (size_t ich = 0; ich < chsize; ++ich)
		{
			std::vector<std::pair<int, int>> chain;
			Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
			int64_t chid = ch.GetID();
			int32_t nseg = ch.GetNSeg();
			int32_t spl = ch.GetStartPL();
			int32_t epl = ch.GetEndPL();
			//fprintf(stdout, "    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d\n", chid, nseg, spl, epl);

			for (size_t pl = 0; pl < possize; ++pl)
			{
				//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
				//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
				BaseTrackID bt = ch.GetBaseTrack(pl);
				if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
				int32_t btpl = bt.GetPL();
				int64_t btid = bt.GetRawID();
				//btplとplは厳密に一致しなければおかしい。
				if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
				if (b.btset.find(std::make_pair(b.usepos[btpl], btid)) == b.btset.end())throw std::exception("BaseTrack is not found in btset.");
				chain.push_back(std::make_pair(b.usepos[pl], btid));
			}
			for (int i = 0; i < chain.size() - 1; i++) {
				auto path_list = b.comp_ltlist.find(std::make_tuple(chain[i].first, chain[i + 1].first, chain[i].second, chain[i + 1].second));
				if (path_list == b.comp_ltlist.end()) {
					fprintf(stderr, "exception\n");
				}
				for (int j = 0; j < path_list->second.size() - 1; j++) {
					ret.comfirmed_path.push_back(std::make_pair(path_id,
						std::make_tuple(path_list->second[j].first, path_list->second[j + 1].first, path_list->second[j].second, path_list->second[j + 1].second)));
				}
				saved_path.insert(path_list->first);
			}
			path_id++;
		}
	}

	for (auto itr = b.cut_ltlist.begin(); itr != b.cut_ltlist.end(); itr++) {
		if (b.comp_ltlist.count(*itr) == 0)continue;
		auto range = b.comp_ltlist.equal_range(*itr);
		for (auto res = range.first; res != range.second; res++) {
			for (int i = 0; i < res->second.size() - 1; i++) {
				ret.cut_path.push_back(std::make_pair(path_id,
					std::make_tuple(res->second[i].first, res->second[i + 1].first, res->second[i].second, res->second[i + 1].second)));
			}
			path_id++;
			saved_path.insert(res->first);
		}
	}
	for (auto itr = b.comp_ltlist.begin(); itr != b.comp_ltlist.end(); itr++) {
		if (saved_path.count(itr->first) == 1)continue;
		if (b.comp_ltlist.count(itr->first) == 0)continue;
		auto range = b.comp_ltlist.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			for (int i = 0; i < res->second.size() - 1; i++) {
				ret.comfirmed_path.push_back(std::make_pair(path_id,
					std::make_tuple(res->second[i].first, res->second[i + 1].first, res->second[i].second, res->second[i + 1].second)));
			}
			path_id++;
			saved_path.insert(res->first);
		}
	}

	ret.num_comfirmed_path = ret.comfirmed_path.size();
	ret.num_cut_path = ret.cut_path.size();
	ret.num_select_path = ret.select_path.size();

	ret.gid = b.groupid;

	return ret;
}
output_format cut_path_organize(output_format &out) {

	std::multimap<std::pair<int, int>, std::pair<int, std::pair<int, int>>>path_next, path_prev;

	std::set<int> select_path,comfirmed_path,cut_path;
	std::pair<int, int> seg0, seg1;
	for (auto itr = out.select_path.begin(); itr != out.select_path.end(); itr++) {
		select_path.insert(itr->first);
		seg0.first = std::get<0>(itr->second);
		seg0.second = std::get<2>(itr->second);
		seg1.first = std::get<1>(itr->second);
		seg1.second = std::get<3>(itr->second);
		path_next.insert(std::make_pair(seg0, std::make_pair(itr->first, seg1)));
		path_prev.insert(std::make_pair(seg1, std::make_pair(itr->first, seg0)));
	}
	for (auto itr = out.comfirmed_path.begin(); itr != out.comfirmed_path.end(); itr++) {
		comfirmed_path.insert(itr->first);
		seg0.first = std::get<0>(itr->second);
		seg0.second = std::get<2>(itr->second);
		seg1.first = std::get<1>(itr->second);
		seg1.second = std::get<3>(itr->second);
		path_next.insert(std::make_pair(seg0, std::make_pair(itr->first, seg1)));
		path_prev.insert(std::make_pair(seg1, std::make_pair(itr->first, seg0)));
	}
	for (auto itr = out.cut_path.begin(); itr != out.cut_path.end(); itr++) {
		seg0.first = std::get<0>(itr->second);
		seg0.second = std::get<2>(itr->second);
		seg1.first = std::get<1>(itr->second);
		seg1.second = std::get<3>(itr->second);
		path_next.insert(std::make_pair(seg0, std::make_pair(itr->first, seg1)));
		path_prev.insert(std::make_pair(seg1, std::make_pair(itr->first, seg0)));
	}

	std::vector<std::pair<int, std::tuple<int, int, int, int>>> remain_cut_path;
	//std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> change_path;
	std::set<std::pair<std::pair<int, int>, std::pair<int, int>>>change_path;

	for (auto itr = out.cut_path.begin(); itr != out.cut_path.end(); itr++) {
		seg0.first = std::get<0>(itr->second);
		seg0.second = std::get<2>(itr->second);
		seg1.first = std::get<1>(itr->second);
		seg1.second = std::get<3>(itr->second);
		if (path_next.count(seg0) > 1|| path_prev.count(seg1) > 1) {
			remain_cut_path.push_back(*itr);
			continue;
		}
		else {
			change_path.insert(std::make_pair(seg0, seg1));
		}
	}
	int path_id_num = out.num_comfirmed_path + out.num_cut_path + out.num_select_path;
	std::set<std::pair<std::pair<int, int>, std::pair<int, int>> >finished_path;
	std::vector<std::set<std::pair<std::pair<int, int>, std::pair<int, int>>>> path_group;
	bool flg_pass;
	for (auto itr = change_path.begin(); itr != change_path.end(); itr++) {
		if (finished_path.count(*itr) == 1)continue;

		std::set<std::pair<std::pair<int, int>, std::pair<int, int>>> path_cluster;
		std::set<std::pair<std::pair<int, int>, std::pair<int, int>>> path_cluster_add;
		path_cluster.insert(*itr);
		int sum_p = -1;
		while (sum_p != path_cluster.size()) {
			sum_p = path_cluster.size();
			for (auto itr2 = path_cluster.begin(); itr2 != path_cluster.end(); itr2++) {
				if (path_next.count(itr2->second) != 0) {
					auto range = path_next.equal_range(itr2->second);
					for (auto res = range.first; res != range.second; res++) {
						if (change_path.count(std::make_pair(res->first, res->second.second)) == 0)continue;
						path_cluster_add.insert(std::make_pair(res->first, res->second.second));
					}
				}
				if (path_prev.count(itr2->first) != 0) {
					auto  range = path_prev.equal_range(itr2->first);
					for (auto res = range.first; res != range.second; res++) {
						if (change_path.count(std::make_pair(res->second.second, res->first)) == 0)continue;
						path_cluster_add.insert(std::make_pair(res->second.second, res->first));
					}
				}
			}
			for (auto itr2 = path_cluster_add.begin(); itr2 != path_cluster_add.end(); itr2++) {
				path_cluster.insert(*itr2);
			}
			path_cluster_add.clear();
		}

		for (auto itr2 = path_cluster.begin(); itr2 != path_cluster.end(); itr2++) {
			finished_path.insert(*itr2);
		}
		path_group.push_back(path_cluster);
	}
	for (int i = 0; i < path_group.size(); i++) {
		for (auto itr = path_group[i].begin(); itr != path_group[i].end(); itr++) {
			if (itr == path_group[i].begin()) {
				seg0 = itr->first;
				seg1 = itr->second;
			}
			if (seg0.first > itr->first.first) {
				seg0 = itr->first;
			}
			if (seg1.first < itr->second.first) {
				seg1 = itr->second;
			}
		}
		int path_id0=0, path_id1=0,path_id;
		auto res_prev = path_prev.find(seg0);
		auto res_next = path_next.find(seg1);
		//端点はid=0
		//select path  id=1
		//comfirmed path  id=2
		//そのほか(カット辺など) id=0

		if (res_prev == path_prev.end())path_id0 = 0;
		else if (select_path.count(res_prev->second.first) == 1)path_id0 = 1;
		else if (comfirmed_path.count(res_prev->second.first) == 1)path_id0 = 2;
		else path_id0 = 0;

		if (res_next == path_next.end())path_id1 = 0;
		else if (select_path.count(res_next->second.first) == 1)path_id1 = 1;
		else if (comfirmed_path.count(res_next->second.first) == 1)path_id1 = 2;
		else path_id1 = 0;
		//各辺に割り振る
		if (path_id0 == 0 && path_id1 == 1) {
			path_id = res_next->second.first;
			for (auto itr = path_group[i].begin(); itr != path_group[i].end(); itr++) {
				out.select_path.push_back(std::make_pair(path_id, std::make_tuple(
					itr->first.first, itr->second.first, itr->first.second, itr->second.second)));
			}
		}
		else if (path_id0 == 1 && path_id1 == 0) {
			path_id = res_prev->second.first;
			for (auto itr = path_group[i].begin(); itr != path_group[i].end(); itr++) {
				out.select_path.push_back(std::make_pair(path_id, std::make_tuple(
					itr->first.first, itr->second.first, itr->first.second, itr->second.second)));
			}
		}
		else if (path_id0 == 0 && path_id1 == 2) {
			path_id = res_next->second.first;
			for (auto itr = path_group[i].begin(); itr != path_group[i].end(); itr++) {
				out.comfirmed_path.push_back(std::make_pair(path_id, std::make_tuple(
					itr->first.first, itr->second.first, itr->first.second, itr->second.second)));
			}
		}
		else if (path_id0 == 2 && path_id1 == 0) {
			path_id = res_prev->second.first;
			for (auto itr = path_group[i].begin(); itr != path_group[i].end(); itr++) {
				out.comfirmed_path.push_back(std::make_pair(path_id, std::make_tuple(
					itr->first.first, itr->second.first, itr->first.second, itr->second.second)));
			}
		}
		else {
			path_id = path_id_num;
			path_id_num++;
			for (auto itr = path_group[i].begin(); itr != path_group[i].end(); itr++) {
				out.comfirmed_path.push_back(std::make_pair(path_id, std::make_tuple(
					itr->first.first, itr->second.first, itr->first.second, itr->second.second)));
			}
		}
	}
	out.cut_path = remain_cut_path;
	out.num_comfirmed_path = out.comfirmed_path.size();
	out.num_select_path = out.select_path.size();
	out.num_cut_path = out.cut_path.size();


	std::map<int, int> id_re_roll;
	for (auto itr = out.comfirmed_path.begin(); itr != out.comfirmed_path.end(); itr++) {
		id_re_roll.insert(std::make_pair(itr->first, 0));
	}
	for (auto itr = out.select_path.begin(); itr != out.select_path.end(); itr++) {
		id_re_roll.insert(std::make_pair(itr->first, 0));
	}
	for (auto itr = out.cut_path.begin(); itr != out.cut_path.end(); itr++) {
		id_re_roll.insert(std::make_pair(itr->first, 0));
	}
	int count = 0;
	for (auto itr = id_re_roll.begin(); itr != id_re_roll.end(); itr++) {
		itr->second = count;
		count++;
	}

	for (auto itr = out.comfirmed_path.begin(); itr != out.comfirmed_path.end(); itr++) {
		itr->first = id_re_roll.at(itr->first);
	}
	for (auto itr = out.select_path.begin(); itr != out.select_path.end(); itr++) {
		itr->first = id_re_roll.at(itr->first);
	}
	for (auto itr = out.cut_path.begin(); itr != out.cut_path.end(); itr++) {
		itr->first = id_re_roll.at(itr->first);
	}



	return out;
}