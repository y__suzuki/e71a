#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_format {
public:
	int eventid, trackid, pl, rawid, mu_pl, mu_rawid;
	double ax, ay, vph, md, oa, x, y;
};
bool sort_chain_base(const mfile0::M_Base&left, const mfile0::M_Base&right) {
	if (left.pos == right.pos)return left.rawid < right.rawid;
	return left.pos < right.pos;
}
std::vector<mfile0::M_Chain> reject_penetrate(std::vector<mfile0::M_Chain>&chain);
int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage: prg in-mfile out-mfile\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);
	printf("mfile read fin\n");
	m.chains = reject_penetrate(m.chains);

	mfile0::write_mfile(file_out_mfile, m);
}
bool judge_mip(double angle, double vph) {
	if (angle < 0.4) {
		return vph < -200 * angle + 200;
	}
	else if (angle < 1.0) {
		return vph < (-100 * angle + 400) / 3;
	}
	return vph < 100;


}
bool judge_mip(mfile0::M_Chain &c) {
	double ax_mean = 0, ay_mean = 0, angle, vph = 0;
	int count = 0;
	for (auto b : c.basetracks) {
		ax_mean += b.ax;
		ay_mean += b.ay;
		vph += b.ph % 10000;
		count++;
	}
	ax_mean = ax_mean / count;
	ay_mean = ay_mean / count;
	angle = sqrt(ax_mean*ax_mean + ay_mean * ay_mean);
	vph = vph / count;

	bool res = judge_mip(angle, vph);
	if (!res) {
		//printf("%d %.4lf %.1lf\n", c.chain_id, angle, vph);
	}
	return res;
}
bool judge_attachtrack(mfile0::M_Chain&c, int muon_pl) {
	mfile0::M_Chain new_chain;
	new_chain.chain_id = c.chain_id;
	int attach_pl = 0;
	for (auto &b : c.basetracks) {
		//new_chain.basetracks.push_back(b);
		if (b.flg_i[0] == 1) {
			attach_pl = b.pos / 10;
		}
	}
	if (attach_pl < 1) return false;


	//forwardのattach
	if (attach_pl <= muon_pl) {
		//下流側のみpush back
		for (int i = 0; i < c.basetracks.size() && c.basetracks[i].pos / 10 <= muon_pl; i++) {
			new_chain.basetracks.push_back(c.basetracks[i]);
		}
		new_chain.nseg = new_chain.basetracks.size();
		new_chain.pos0 = new_chain.basetracks.begin()->pos;
		new_chain.pos1 = new_chain.basetracks.rbegin()->pos;
		//blackなら返す
		if (!judge_mip(new_chain)) {
			c = new_chain;
			return true;
		}
		//mipでも貫通していないなら返す
		else {
			if (c.pos1/10 <= muon_pl) {
				c = new_chain;
				return true;
			}
		}
		return false;
	}
	//backwordのattach
	else {
		//上流側のみpush back
		for (int i = c.basetracks.size() - 1; i >= 0 && c.basetracks[i].pos / 10 > muon_pl; i--) {
			new_chain.basetracks.push_back(c.basetracks[i]);
		}
		sort(new_chain.basetracks.begin(), new_chain.basetracks.end(), sort_chain_base);

		new_chain.nseg = new_chain.basetracks.size();
		new_chain.pos0 = new_chain.basetracks.begin()->pos;
		new_chain.pos1 = new_chain.basetracks.rbegin()->pos;
		//blackなら返す
		if (!judge_mip(new_chain)) {
			c = new_chain;
			return true;
		}
		//mipでも貫通していないなら返す
		else {
			if (c.pos0 /10> muon_pl) {
				c = new_chain;
				return true;
			}
		}
		return false;

	}
	return false;

}
std::vector<mfile0::M_Chain> reject_penetrate(std::vector<mfile0::M_Chain>&chain) {

	std::vector<mfile0::M_Chain> ret;


	int muon_pl = -1;
	for (auto &c : chain) {
		//printf("\r eventid=%d chainid=%d %d %d %d", c.basetracks.begin()->group_id, c.chain_id,c.pos0,c.pos1,c.nseg);

		if (c.chain_id == 0) {
			ret.push_back(c);
			muon_pl = c.basetracks.rbegin()->pos / 10;
		}
		else {
			if (judge_attachtrack(c, muon_pl)) {
				ret.push_back(c);
			}
		}
	}
	return ret;

}
