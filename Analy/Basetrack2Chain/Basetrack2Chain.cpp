#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <set>
#include <list>
#include <filesystem>

void linkelt_search_track_upstream(std::string filename, std::set<std::pair<int, int>> &track);
void linkelt_search_track_downstream(std::string filename, std::set<std::pair<int, int>> &track);

std::pair<bool, mfile0::M_Chain> basetrack2chain(vxx::base_track_t base, std::vector<corrmap0::Corrmap>&corr, int &chain, std::map<int, double> &z_val);
mfile0::Mfile basetrack2chian(std::string file_path, std::multimap<int, vxx::base_track_t>&base);
void write_basetracks(std::string filename, std::multimap<int, vxx::base_track_t> &chain);
std::multimap<int, vxx::base_track_t> basetrack_pick_up(std::string file_path, std::set<std::pair<int, int>> track);

//厳密にはchainではない。linklet情報で上流方向、下流方向に追っていくだけ。
//イタレーションするとchainと同等なものになる
int main(int argc, char**argv) {
	if (argc != 9) {
		fprintf(stderr, "usage:prg track_data_path(area) pl rawid mode pl_min pl_max output-mfile-format output-txt\n");
		exit(1);
	}
	std::string file_input = argv[1];
	int pl = std::stoi(argv[2]);
	int rawid = std::stoi(argv[3]);
	int mode = std::stoi(argv[4]);

	int pl_min = std::stoi(argv[5]);
	int pl_max = std::stoi(argv[6]);

	std::string file_out_mfile = argv[7];
	std::string file_out_txt = argv[8];

	std::set<std::pair<int, int>> track;
	track.insert(std::make_pair(pl, rawid));
	//上流方向への探索
	for (int search_pl = pl; search_pl <= pl_max; search_pl++) {
		for (int peke = 1; peke <= 4; peke++) {

			std::stringstream linklet_path;
			if (mode == 0) {
				linklet_path << file_input << "\\0\\linklet\\l-" << std::setw(3) << std::setfill('0') << search_pl << "-" << std::setw(3) << std::setfill('0') << search_pl + peke << ".bll";
			}
			else if (mode == 1) {
				linklet_path << file_input << "\\0\\linklet\\l-" << std::setw(3) << std::setfill('0') << search_pl << "-" << std::setw(3) << std::setfill('0') << search_pl + peke << ".sel.bll";
			}
			else {
				fprintf(stderr, "mode exception\n");
				fprintf(stderr, "mode=0 input:l***-***.bll\n");
				fprintf(stderr, "mode=1 input:l***-***.sel.bll\n");
			}
			if (!std::filesystem::exists(linklet_path.str())) continue;
			linkelt_search_track_upstream(linklet_path.str(), track);
			printf("search %03d-%03d track num=%d\n", search_pl, search_pl + peke, track.size());
		}
	}
	//下流方向への探索
	for (int search_pl = pl; search_pl >= pl_min; search_pl--) {
		for (int peke = 1; peke <= 4; peke++) {

			std::stringstream linklet_path;
			if (mode == 0) {
				linklet_path << file_input << "\\0\\linklet\\l-" << std::setw(3) << std::setfill('0') << search_pl - peke << "-" << std::setw(3) << std::setfill('0') << search_pl << ".bll";
			}
			else if (mode == 1) {
				linklet_path << file_input << "\\0\\linklet\\l-" << std::setw(3) << std::setfill('0') << search_pl - peke << "-" << std::setw(3) << std::setfill('0') << search_pl << ".sel.bll";
			}
			else {
				fprintf(stderr, "mode exception\n");
				fprintf(stderr, "mode=0 input:l***-***.bll\n");
				fprintf(stderr, "mode=1 input:l***-***.sel.bll\n");
			}
			if (!std::filesystem::exists(linklet_path.str())) continue;
			linkelt_search_track_downstream(linklet_path.str(), track);
			printf("search %03d-%03d track num=%d\n", search_pl - peke, search_pl, track.size());
		}
	}


	std::multimap<int,vxx::base_track_t> chain = basetrack_pick_up(file_input, track);
	mfile0::Mfile m = basetrack2chian(file_input, chain);

	mfile0::write_mfile(file_out_mfile, m);
	write_basetracks(file_out_txt, chain);
}
void linkelt_search_track_upstream(std::string filename, std::set<std::pair<int, int>> &track) {
	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_bin(filename, link);

	std::pair<int, int> id;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		id.first = itr->b[0].pl;
		id.second = itr->b[0].rawid;
		if (track.count(id) == 1) {
			track.insert(std::make_pair(itr->b[1].pl, itr->b[1].rawid));
		}
	}
}
void linkelt_search_track_downstream(std::string filename, std::set<std::pair<int, int>> &track) {
	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_bin(filename, link);

	std::pair<int, int> id;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		id.first = itr->b[1].pl;
		id.second = itr->b[1].rawid;
		if (track.count(id) == 1) {
			track.insert(std::make_pair(itr->b[0].pl, itr->b[0].rawid));
		}
	}
}

std::multimap<int, vxx::base_track_t> basetrack_pick_up(std::string file_path, std::set<std::pair<int, int>> track) {
	std::multimap<int, vxx::base_track_t> ret;
	std::map<int64_t, vxx::base_track_t> base_map;
	std::vector<vxx::base_track_t> base;
	int pl = -1;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		if (itr->first != pl) {
			pl = itr->first;
			base.clear();
			base_map.clear();
			std::stringstream file_in_bvxx;
			file_in_bvxx << file_path << "\\PL" << std::setw(3) << std::setfill('0') << pl
				<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
			if (!std::filesystem::exists(file_in_bvxx.str())) {
				fprintf(stderr, "file[%s] not exist\n", file_in_bvxx.str().c_str());
				exit(1);
			}
			vxx::BvxxReader br;
			base = br.ReadAll(file_in_bvxx.str(), pl, 0);
			for (auto itr2 = base.begin(); itr2 != base.end(); itr2++) {
				base_map.insert(std::make_pair(itr2->rawid, *itr2));
			}
			printf("now... PL%03d read fin\n", pl);
		}
		auto res = base_map.find(itr->second);
		if (res == base_map.end()) {
			fprintf(stderr, "PL%03d rawid=%d not found\n", itr->first, itr->second);
			continue;
		}
		ret.insert(std::make_pair(res->second.pl, res->second));
	}
	printf("basetrack pick up fin\n", pl);

	return ret;
}
mfile0::Mfile basetrack2chian(std::string file_path, std::multimap<int, vxx::base_track_t>&base) {
	mfile0::Mfile ret;

	std::vector<corrmap0::Corrmap> corr;
	std::stringstream file_in_corr;
	file_in_corr << file_path << "\\0\\align\\corrmap-abs.lst";
	if (!std::filesystem::exists(file_in_corr.str())) {
		fprintf(stderr, "corrmap abs not found\n");
		exit(1);
	}
	corrmap0::read_cormap(file_in_corr.str(), corr);

	mfile0::Mfile m;
	std::map<int, double> z_val;
	std::stringstream file_in_mfile;
	file_in_mfile << file_path << "\\0\\mfile\\m1.bmf";
	if (!std::filesystem::exists(file_in_mfile.str())) {
		fprintf(stderr, "m1.bmf abs not found\n");
		exit(1);
	}
	mfile1::read_mfile_extension(file_in_mfile.str(), m);
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			z_val.insert(std::make_pair(itr2->pos / 10, itr2->z));
		}
	}
	ret.header = m.header;
	m.chains.clear();
	m.chains.shrink_to_fit();

	int chainid = 0;
	for (auto itr = base.begin(); itr != base.end();itr++) {
		printf("PL%03d rawid=%d\n", itr->second.pl, itr->second.rawid);
		std::pair<bool, mfile0::M_Chain> res = basetrack2chain(itr->second, corr, chainid, z_val);
		if (res.first) {
			ret.chains.push_back(res.second);
		}
	}

	return ret;

}
std::pair<bool,mfile0::M_Chain> basetrack2chain(vxx::base_track_t base, std::vector<corrmap0::Corrmap>&corr,int &chain, std::map<int, double> &z_val){
	mfile0::M_Chain ret;
	mfile0::M_Base b;
	int pl = base.pl;
	b.pos = pl * 10+1;
	b.flg_d[0] = 0;
	b.flg_d[1] = 0;
	b.flg_i[0] = 0;
	b.flg_i[1] = 0;
	b.flg_i[2] = 0;
	b.flg_i[3] = 0;
	b.group_id = chain;
	b.ph = base.m[0].ph + base.m[1].ph;
	b.rawid = base.rawid;
	auto res = z_val.find(pl);
	if (res == z_val.end()) {
		return std::make_pair(false, ret);
	}
	b.z = res->second;

	corrmap0::Corrmap param;
	bool flg = false;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (itr->pos[0] / 10 == pl) {
			param = *itr;
			flg = true;
			break;
		}
	}
	if (!flg) {
		return std::make_pair(false, ret);
	}

	b.ax = base.ax*param.angle[0] + base.ay*param.angle[1] + param.angle[4];
	b.ay = base.ax*param.angle[2] + base.ay*param.angle[3] + param.angle[5];

	b.x = base.x*param.position[0] + base.y*param.position[1] + param.position[4];
	b.y = base.x*param.position[2] + base.y*param.position[3] + param.position[5];

	ret.basetracks.push_back(b);
	ret.chain_id = chain;
	chain++;
	ret.pos0 = ret.basetracks.begin()->pos;
	ret.pos1 = ret.basetracks.rend()->pos;
	ret.nseg = ret.basetracks.size();
	return std::make_pair(true, ret);
}
void write_basetracks(std::string filename, std::multimap<int, vxx::base_track_t> &chain) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (chain.size() == 0) {
		fprintf(stderr, "target ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = chain.begin(); itr != chain.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write  ... %d/%d (%4.1lf%%)", count, int(chain.size()), count*100. / chain.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr->first << " "
				<< std::setw(12) << std::setprecision(0) << itr->second.rawid << std::endl;
		}
		fprintf(stderr, "\r Write  ... %d/%d (%4.1lf%%)\n", count, int(chain.size()), count*100. / chain.size());
	}
}