#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_format {
public:
	int eventid, trackid, pl, rawid,mu_pl,mu_rawid;
	double ax, ay, vph, md, oa, x, y;
};

std::vector<output_format> extract_partner(std::vector<mfile0::M_Chain>&chain);
std::map<std::tuple<int, int, int, int>, std::tuple<double, double, double>>read_md(std::string filename);
void output_file(std::string filename, std::vector<output_format>&out, std::map<std::tuple<int, int, int, int>, std::tuple<double, double, double>>&md);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage: prg in-mfile in-md-file\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_mdfile = argv[2];
	std::string file_out = argv[3];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);
	printf("mfile read fin\n");
	std::vector<output_format> out = extract_partner(m.chains);
	printf("extract track  fin\n");
	std::map<std::tuple<int, int, int, int>, std::tuple<double, double, double>>md=read_md(file_in_mdfile);
	printf("read md file fin\n");
	output_file(file_out, out, md);

}
std::vector<output_format> extract_partner(std::vector<mfile0::M_Chain>&chain) {
	std::vector<output_format> ret;
	int mu_pl = 0, mu_rawid = 0;
	for (auto &c : chain) {
		for (auto &b : c.basetracks) {
			if (b.flg_i[1] == 0) {
				mu_pl = c.basetracks.rbegin()->pos / 10;
				mu_rawid = c.basetracks.rbegin()->rawid;
			}
			if (b.flg_i[0] == 0)continue;
			output_format out;
			out.ax = b.ax;
			out.ay = b.ay;
			out.x = b.x;
			out.y = b.y;
			out.eventid = b.group_id;
			out.trackid = b.flg_i[1];
			out.pl = b.pos / 10;
			out.rawid = b.rawid;
			out.mu_pl = mu_pl;
			out.mu_rawid = mu_rawid;
			double vph_sum = 0;
			int count = 0;
			for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
				count++;
				vph_sum += itr->ph % 10000;
			}
			out.vph = vph_sum / count;
			ret.push_back(out);
		}
	}
	return ret;
}
std::map<std::tuple<int, int, int, int>, std::tuple<double,double, double>>read_md(std::string filename) {
	std::ifstream ifs(filename);
	int pl0, pl1, raw0, raw1;
	double extra0, extra1, md;
	std::map<std::tuple<int, int, int, int>, std::tuple<double, double, double>> ret;
	while (ifs >> pl0 >> raw0 >> pl1 >> raw1 >> extra0 >> extra1 >> md) {
		ret.insert(std::make_pair(
			std::make_tuple(
				pl0,raw0,pl1,raw1
			),
			std::make_tuple(
				extra0,extra1,md
			)
		));
	}
	return ret;

}

void output_file(std::string filename, std::vector<output_format>&out, std::map<std::tuple<int, int, int, int>, std::tuple<double, double, double>>&md) {

	std::ofstream ofs(filename);
	for (auto itr = out.begin(); itr != out.end(); itr++) {
		std::tuple<int, int, int, int> key;
		std::get<0>(key) = itr->mu_pl;
		std::get<1>(key) = itr->mu_rawid;
		std::get<2>(key) = itr->pl;
		std::get<3>(key) = itr->rawid;
		if (itr->mu_pl == itr->pl&&itr->mu_rawid == itr->rawid)continue;
		auto res = md.find(key);
		if (res == md.end())continue;
		ofs << std::fixed << std::right
			<< std::setw(0) << std::setprecision(0) << itr->eventid << " "
			<< std::setw(0) << std::setprecision(0) << itr->trackid << " "
			<< std::setw(0) << std::setprecision(0) << std::get<0>(key) << " "
			<< std::setw(0) << std::setprecision(0) << std::get<1>(key) << " "
			<< std::setw(0) << std::setprecision(0) << std::get<2>(key) << " "
			<< std::setw(0) << std::setprecision(0) << std::get<3>(key) << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->x << " "
			<< std::setw(8) << std::setprecision(1) << itr->y << " "
			<< std::setw(6) << std::setprecision(2) << itr->vph << " "
			<< std::setw(6) << std::setprecision(1) << std::get<0>(res->second) << " "
			<< std::setw(6) << std::setprecision(1) << std::get<1>(res->second) << " "
			<< std::setw(6) << std::setprecision(4) << std::get<2>(res->second) << std::endl;


	}
}