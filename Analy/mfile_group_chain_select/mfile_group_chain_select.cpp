#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <omp.h>
#include <set>

mfile0::M_Base select_best_base(std::vector<mfile0::M_Base >&base_v, mfile0::M_Base& prev);
mfile0::Mfile chain_select(mfile0::Mfile &m, mfile0::Mfile &mu);
bool sort_base(const mfile0::M_Base &left, const mfile0::M_Base &right) {
	return left.pos < right.pos;
}

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-mfile file-in-muon file-out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_mu = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m;
	mfile0::Mfile mu;
	mfile0::read_mfile(file_in_mfile, m);
	mfile0::read_mfile(file_in_mu, mu);

	m = chain_select(m, mu);

	mfile0::write_mfile(file_out_mfile, m);
}
mfile0::Mfile chain_select(mfile0::Mfile &m, mfile0::Mfile &mu) {
	mfile0::Mfile ret;
	ret.header = m.header;

	int gid = 0;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (itr->basetracks.begin()->group_id % 100000 == 0) {
			gid = itr->basetracks.begin()->group_id / 100000;
		}
	}
	int muon_pl;
	for (auto itr = mu.chains.begin(); itr != mu.chains.end(); itr++) {
		if (itr->basetracks.begin()->group_id / 100000 == gid) {
			ret.chains.push_back(*itr);
			muon_pl = itr->pos1 / 10;
		}
	}

	int pl0, pl1;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (itr->basetracks.begin()->group_id % 100000 == 0)continue;
		if (itr->nseg == 1)continue;
		pl0 = itr->pos0 / 10;
		pl1 = itr->pos1 / 10;
		std::multimap<int, mfile0::M_Base> base;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			base.insert(std::make_pair(itr2->pos / 10, *itr2));
		}
		mfile0::M_Chain c;
		mfile0::M_Base prev;
		if (abs(pl0 - muon_pl) < abs(pl1 - muon_pl)) {
			prev = base.begin()->second;
			for (auto itr2 = base.begin(); itr2 != base.end(); itr2++) {
				
				int count = base.count(itr2->first);
				std::vector<mfile0::M_Base >base_v;
				auto range = base.equal_range(itr2->first);
				for (auto res = range.first; res != range.second; res++) {
					base_v.push_back(res->second);
				}
				c.basetracks.push_back(select_best_base(base_v, prev));
				itr2 = std::next(itr2, count - 1);
			}
		}
		else {
			prev = base.rbegin()->second;
			for (auto itr2 = base.rbegin(); itr2 != base.rend(); itr2++) {

				int count = base.count(itr2->first);
				std::vector<mfile0::M_Base >base_v;
				auto range = base.equal_range(itr2->first);
				for (auto res = range.first; res != range.second; res++) {
					base_v.push_back(res->second);
				}
				c.basetracks.push_back(select_best_base(base_v, prev));
				itr2 = std::next(itr2, count - 1);
			}

		}
	
		sort(c.basetracks.begin(), c.basetracks.end(), sort_base);
		c.chain_id = itr->chain_id;
		c.nseg = c.basetracks.size();
		c.pos0 = c.basetracks.begin()->pos;
		c.pos1 = c.basetracks.rbegin()->pos;
		ret.chains.push_back(c);
	}
	return ret;
}
mfile0::M_Base select_best_base(std::vector<mfile0::M_Base >&base_v, mfile0::M_Base& prev) {
	double z0, z1;
	z0 = prev.z;
	z1 = base_v.begin()->z;
	double ex_x, ex_y;
	ex_x = prev.x + prev.ax*(z1 - z0);
	ex_y = prev.y + prev.ay*(z1 - z0);
	double dist;
	mfile0::M_Base ret;
	for (auto itr = base_v.begin(); itr != base_v.end(); itr++) {
		if (itr == base_v.begin()) {
			dist = pow(itr->x - ex_x, 2) + pow(itr->y - ex_y, 2);
			ret = *itr;
		}
		if (dist > pow(itr->x - ex_x, 2) + pow(itr->y - ex_y, 2)) {
			dist = pow(itr->x - ex_x, 2) + pow(itr->y - ex_y, 2);
			ret = *itr;
		}
	}
	prev = ret;
	return ret;
}