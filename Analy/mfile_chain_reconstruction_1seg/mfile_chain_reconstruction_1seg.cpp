//partnerのbasetrackをつなぐ用のprg

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

class Sharing_file {
public:
	int32_t pl, ecc_id, fixedwall_id, trackerwall_id, spotid, zone[2], rawid[2], unix_time, tracker_track_id;
	//spotid:spotA * 100 + spotB
};

class Chain_baselist {
public:
	int64_t groupid;
	std::set<std::pair<int, int>>rawid;
};

class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};



std::vector<Chain_baselist> read_mfile(std::string filename);

void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs);

std::vector<Chain_baselist> read_chain_list(std::string filename);
void read_mfile(std::string filename, std::vector<Chain_baselist>&chain, std::map<int, double>&z_map);

int64_t read_linklet_list_001to133(std::string filename, std::vector<Chain_baselist>&chain, int pl_min, int pl_max);
int64_t read_linklet_list_133to001(std::string filename, std::vector<Chain_baselist>&chain, int pl_min, int pl_max);

std::multimap<int, mfile0::M_Base> Chain_base(std::vector<Chain_baselist> &chain_list, std::map<int, double> &z_map);

void basetrack_pick_up(std::string file_path, int pl, std::vector<mfile0::M_Base*>&base_p);
void basetrack_pick_up2(std::string file_path, int pl, std::vector<mfile0::M_Base*>&base_p);

void basetrack_mfile_trans(std::vector<mfile0::M_Base*>&base_p, int pl, std::vector<corrmap0::Corrmap>&corr_abs);

mfile0::Mfile basetrack2chian(std::multimap<int, mfile0::M_Base> &chains);
int64_t Linklet_header_num(std::string filename);
void output_Saringfile_w_gid(std::string filename, std::vector<Chain_baselist>&gsf);

void read_linklet_list(std::string filename, std::multimap<std::pair<short, int>, std::pair<short, int>> &link_id_to_up, std::multimap<std::pair<short, int>, std::pair<short, int>> &link_id_to_down);
int64_t make_chain_001to133(std::multimap<std::pair<short, int>, std::pair<short, int>> &linked_id, std::vector<Chain_baselist>&chain, int pl_min, int pl_max);
int64_t make_chain_133to001(std::multimap<std::pair<short, int>, std::pair<short, int>> &linked_id, std::vector<Chain_baselist>&chain, int pl_min, int pl_max);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-mfile ECC-Area-path mode output-mfile linklet-header-list(bin)\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_ECC = argv[2];
	int mode = std::stoi(argv[3]);
	std::string file_out_mfile = argv[4];
	std::string file_in_link = argv[5];

	std::string file_in_corrabs = file_in_ECC + "\\0\\align\\corrmap-abs.lst";

	if (mode != -1 && mode != 1 && mode != 2 && mode != 3) {
		fprintf(stderr, "mode exception\n");
		fprintf(stderr, "mode=-1 PL133 --> PL001 follow down\n");
		fprintf(stderr, "mode= 1 PL001 --> PL133 scan back\n");
		fprintf(stderr, "mode= 2 PL001 --> PL133 -->PL001\n");
		fprintf(stderr, "mode= 3 PL001 --> PL133 -->PL001--->:endless\n");
		exit(1);
	}

	//corrmap absの読み込み
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrabs, corr_abs);

	//mfileの読み込み
	std::vector<Chain_baselist> chain_list = read_mfile(file_in_mfile);
	printf("num of track %d\n", chain_list.size());
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	z_map_correction(z_map, corr_abs);

	//全linkletの読み込み
	std::multimap<std::pair<short, int>, std::pair<short, int>> link_id_to_up, link_id_to_down;
	read_linklet_list(file_in_link, link_id_to_up, link_id_to_down);

	//全linkletからchainを再作成
	int64_t base_num = 0;
	if (mode == 3) {
		bool flg = true;
		int64_t base_num_prev = 0;
		while (flg) {
			base_num = make_chain_001to133(link_id_to_up, chain_list, 1, 133);
			base_num = make_chain_133to001(link_id_to_down, chain_list, 1, 133);
			printf("base_num=%lld\n", base_num);
			if (base_num == base_num_prev)flg = false;
			base_num_prev = base_num;
		}
	}
	else {
		if (mode == 1 || mode == 2) {
			base_num = make_chain_001to133(link_id_to_up, chain_list, 1, 133);
		}
		if (mode == -1 || mode == 2) {
			base_num = make_chain_133to001(link_id_to_down, chain_list, 1, 133);
		}
	}

	//chainを構成するbasetrackを読み出し-->mfile座標系に変換
	std::map<int, int64_t> gid_cid;
	std::multimap<int, mfile0::M_Base> basetracks = Chain_base(chain_list, z_map);
	int count = 0;
	for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
		count = basetracks.count(itr->first);
		std::vector<mfile0::M_Base*> base_p;
		auto range = basetracks.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_p.push_back(&(res->second));
		}
		basetrack_pick_up2(file_in_ECC, itr->first, base_p);
		basetrack_mfile_trans(base_p, itr->first, corr_abs);

		itr = std::next(itr, count - 1);
	}

	std::multimap<int, mfile0::M_Base> chains;
	for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
		chains.insert(std::make_pair(itr->second.group_id, itr->second));
	}

	mfile0::Mfile m = basetrack2chian(chains);

	mfile0::write_mfile(file_out_mfile, m);

}

std::vector<Chain_baselist> read_mfile(std::string filename) {
	mfile0::Mfile m;
	mfile1::read_mfile_extension(filename, m);

	std::multimap<int64_t, mfile0::M_Base> group;
	std::map<std::pair<int64_t, int>, mfile0::M_Base> group_map;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			group_map.insert(std::make_pair(std::make_pair(itr2->group_id, itr2->rawid), *itr2));
		}
	}
	for (auto itr = group_map.begin(); itr != group_map.end(); itr++) {
		group.insert(std::make_pair(itr->first.first, itr->second));
	}
	std::vector<Chain_baselist> ret;
	int count = 0;
	for (auto itr = group.begin(); itr != group.end(); itr++) {
		count = group.count(itr->first);

		Chain_baselist c;
		c.groupid = itr->second.group_id;
		auto range = group.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			c.rawid.insert(std::make_pair(res->second.pos / 10, res->second.rawid));
		}
		ret.push_back(c);

		itr = std::next(itr, count - 1);
	}
	return ret;
}

void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs) {

	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		auto res = z_map.find(pl);
		if (res == z_map.end())continue;
		res->second += itr->dz;
	}

}

void read_linklet_list(std::string filename, std::multimap<std::pair<short, int>, std::pair<short, int>> &link_id_to_up, std::multimap<std::pair<short, int>, std::pair<short, int>> &link_id_to_down) {
	int64_t link_num = Linklet_header_num(filename);
	FILE*fp_in;
	if ((fp_in = fopen(filename.c_str(), "rb")) == NULL) {
		printf("%s file not open!\n", filename.c_str());
		exit(EXIT_FAILURE);
	}

	int count = 0;
	const int read_num_max = 10000;
	//std::vector<linklet_header> link;
	//link.reserve(read_num_max);
	linklet_header link[read_num_max];
	int64_t  now = 0;
	int read_num;
	bool flg = true;
	while (flg) {
		if (link_num - now == read_num_max) {
			read_num = read_num_max;
			flg = false;
		}
		else if (link_num - now < read_num_max) {
			read_num = link_num - now;
			flg = false;
		}
		else {
			read_num = read_num_max;
		}
		fread(&link, sizeof(linklet_header), read_num, fp_in);
		for (int i = 0; i < read_num; i++) {
			link_id_to_up.insert(std::make_pair(std::make_pair(link[i].pos0 / 10, link[i].raw0), std::make_pair(link[i].pos1 / 10, link[i].raw1)));
			link_id_to_down.insert(std::make_pair(std::make_pair(link[i].pos1 / 10, link[i].raw1), std::make_pair(link[i].pos0 / 10, link[i].raw0)));
		}
		if (count % 100 == 0) {
			printf("\r read fin %12lld/%12lld (%4.1lf%%)", now, link_num, now*100. / link_num);
		}
		now += read_num;
		count++;
	}
	printf("\r read fin %12lld/%12lld (%4.1lf%%)\n", now, link_num, now*100. / link_num);
}
int64_t make_chain_001to133(std::multimap<std::pair<short, int>, std::pair<short, int>> &linked_id, std::vector<Chain_baselist>&chain, int pl_min, int pl_max) {
	bool flg;
	int all = chain.size(), cnt = 0, plus = 0;
	std::vector<std::pair<int, int>> pair_buf;
	pair_buf.reserve(1000);
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		printf("\r now search linklet %d/%d", cnt, all);
		cnt++;
		flg = true;
		while (flg) {
			flg = false;
			plus = 0;
			pair_buf.clear();
			//printf("rawid size =%d flg=%d\n", itr->rawid.size(), flg);
			for (auto itr2 = itr->rawid.begin(); itr2 != itr->rawid.end(); itr2++) {
				if (linked_id.count(*itr2) == 0)continue;
				auto range = linked_id.equal_range(*itr2);
				for (auto res = range.first; res != range.second; res++) {
					pair_buf.push_back(res->second);
				}
			}
			for (auto itr2 = pair_buf.begin(); itr2 != pair_buf.end(); itr2++) {
				auto res = itr->rawid.insert(*itr2);
				if (res.second)plus++;
			}
			if (plus != 0) {
				flg = true;
			}
			//printf("rawid size =%d flg=%d plus=%d\n", itr->rawid.size(), flg, plus);

		}
	}
	printf("\r now search linklet %d/%d\n", cnt, all);

	int64_t all_base_num = 0;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		all_base_num += itr->rawid.size();
	}
	return all_base_num;
}
int64_t make_chain_133to001(std::multimap<std::pair<short, int>, std::pair<short, int>> &linked_id, std::vector<Chain_baselist>&chain, int pl_min, int pl_max) {
	bool flg;
	int all = chain.size(), cnt = 0, plus = 0;
	std::vector<std::pair<int, int>> pair_buf;
	pair_buf.reserve(1000);
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		printf("\r now search linklet %d/%d", cnt, all);
		cnt++;
		flg = true;
		while (flg) {
			flg = false;
			plus = 0;
			pair_buf.clear();
			//printf("rawid size =%d flg=%d\n", itr->rawid.size(), flg);
			//printf("\n");
			for (auto itr2 = itr->rawid.rbegin(); itr2 != itr->rawid.rend(); itr2++) {
				//printf("%3d %10d\n", itr2->first, itr2->second);

				if (linked_id.count(*itr2) == 0)continue;
				//挿入が発生したらbreak
				auto range = linked_id.equal_range(*itr2);
				for (auto res = range.first; res != range.second; res++) {
					pair_buf.push_back(res->second);
				}
			}
			//printf("\n");
			for (auto itr2 = pair_buf.begin(); itr2 != pair_buf.end(); itr2++) {
				auto res = itr->rawid.insert(*itr2);
				if (res.second)plus++;
			}
			if (plus != 0) {
				flg = true;
			}
			//printf("rawid size =%d flg=%d plus=%d\n", itr->rawid.size(), flg, plus);

		}
	}
	printf("\r now search linklet %d/%d\n", cnt, all);

	int64_t all_base_num = 0;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		all_base_num += itr->rawid.size();
	}
	return all_base_num;
}


int64_t read_linklet_list_001to133(std::string filename, std::vector<Chain_baselist>&chain, int pl_min, int pl_max) {
	int64_t link_num = Linklet_header_num(filename);

	std::multimap<std::pair<short, int>, std::pair<short, int>> linked_id;
	FILE*fp_in;
	if ((fp_in = fopen(filename.c_str(), "rb")) == NULL) {
		printf("%s file not open!\n", filename.c_str());
		exit(EXIT_FAILURE);
	}

	int count = 0;
	const int read_num_max = 10000;
	//std::vector<linklet_header> link;
	//link.reserve(read_num_max);
	linklet_header link[read_num_max];
	int64_t  now = 0;
	int read_num;
	bool flg = true;
	while (flg) {
		if (link_num - now == read_num_max) {
			read_num = read_num_max;
			flg = false;
		}
		else if (link_num - now < read_num_max) {
			read_num = link_num - now;
			flg = false;
		}
		else {
			read_num = read_num_max;
		}
		fread(&link, sizeof(linklet_header), read_num, fp_in);
		for (int i = 0; i < read_num; i++) {
			if (link[i].pos0 / 10 < pl_min)continue;
			if (link[i].pos0 / 10 > pl_max)continue;
			linked_id.insert(std::make_pair(std::make_pair(link[i].pos0 / 10, link[i].raw0), std::make_pair(link[i].pos1 / 10, link[i].raw1)));
		}
		if (count % 100 == 0) {
			printf("\r read fin %lld/%lld", now, link_num);
		}
		now += read_num;
		count++;
	}
	printf("\r read fin %lld/%lld\n", now, link_num);

	int all = chain.size(), cnt = 0, plus = 0;
	std::vector<std::pair<int, int>> pair_buf;
	pair_buf.reserve(1000);
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		printf("\r now search linklet %d/%d", cnt, all);
		cnt++;
		flg = true;
		while (flg) {
			flg = false;
			plus = 0;
			pair_buf.clear();
			//printf("rawid size =%d flg=%d\n", itr->rawid.size(), flg);
			for (auto itr2 = itr->rawid.begin(); itr2 != itr->rawid.end(); itr2++) {
				if (linked_id.count(*itr2) == 0)continue;
				auto range = linked_id.equal_range(*itr2);
				for (auto res = range.first; res != range.second; res++) {
					pair_buf.push_back(res->second);
				}
			}
			for (auto itr2 = pair_buf.begin(); itr2 != pair_buf.end(); itr2++) {
				auto res = itr->rawid.insert(*itr2);
				if (res.second)plus++;
			}
			if (plus != 0) {
				flg = true;
			}
			//printf("rawid size =%d flg=%d plus=%d\n", itr->rawid.size(), flg, plus);

		}
	}
	printf("\r now search linklet %d/%d\n", cnt, all);

	int64_t all_base_num = 0;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		all_base_num += itr->rawid.size();
	}
	return all_base_num;
}
int64_t read_linklet_list_133to001(std::string filename, std::vector<Chain_baselist>&chain, int pl_min, int pl_max) {
	int64_t link_num = Linklet_header_num(filename);

	std::multimap<std::pair<short, int>, std::pair<short, int>> linked_id;
	FILE*fp_in;
	if ((fp_in = fopen(filename.c_str(), "rb")) == NULL) {
		printf("%s file not open!\n", filename.c_str());
		exit(EXIT_FAILURE);
	}

	int count = 0;
	const int read_num_max = 10000;
	//std::vector<linklet_header> link;
	//link.reserve(read_num_max);
	linklet_header link[read_num_max];
	int64_t  now = 0;
	int read_num;
	bool flg = true;
	while (flg) {
		if (link_num - now == read_num_max) {
			read_num = read_num_max;
			flg = false;
		}
		else if (link_num - now < read_num_max) {
			read_num = link_num - now;
			flg = false;
		}
		else {
			read_num = read_num_max;
		}
		fread(&link, sizeof(linklet_header), read_num, fp_in);
		for (int i = 0; i < read_num; i++) {
			if (link[i].pos1 / 10 < pl_min)continue;
			if (link[i].pos1 / 10 > pl_max)continue;
			linked_id.insert(std::make_pair(std::make_pair(link[i].pos1 / 10, link[i].raw1), std::make_pair(link[i].pos0 / 10, link[i].raw0)));
		}
		if (count % 100 == 0) {
			printf("\r read fin %lld/%lld", now, link_num);
		}
		now += read_num;
		count++;
	}
	printf("\r read fin %lld/%lld\n", now, link_num);

	int all = chain.size(), cnt = 0, plus = 0;
	std::vector<std::pair<int, int>> pair_buf;
	pair_buf.reserve(1000);
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		printf("\r now search linklet %d/%d", cnt, all);
		cnt++;
		flg = true;
		while (flg) {
			flg = false;
			plus = 0;
			pair_buf.clear();
			//printf("rawid size =%d flg=%d\n", itr->rawid.size(), flg);
			//printf("\n");
			for (auto itr2 = itr->rawid.rbegin(); itr2 != itr->rawid.rend(); itr2++) {
				//printf("%3d %10d\n", itr2->first, itr2->second);

				if (linked_id.count(*itr2) == 0)continue;
				//挿入が発生したらbreak
				auto range = linked_id.equal_range(*itr2);
				for (auto res = range.first; res != range.second; res++) {
					pair_buf.push_back(res->second);
				}
			}
			//printf("\n");
			for (auto itr2 = pair_buf.begin(); itr2 != pair_buf.end(); itr2++) {
				auto res = itr->rawid.insert(*itr2);
				if (res.second)plus++;
			}
			if (plus != 0) {
				flg = true;
			}
			//printf("rawid size =%d flg=%d plus=%d\n", itr->rawid.size(), flg, plus);

		}
	}
	printf("\r now search linklet %d/%d\n", cnt, all);

	int64_t all_base_num = 0;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		all_base_num += itr->rawid.size();
	}
	return all_base_num;
}

std::multimap<int, mfile0::M_Base> Chain_base(std::vector<Chain_baselist> &chain_list, std::map<int, double> &z_map) {
	std::multimap<int, mfile0::M_Base> ret;

	mfile0::M_Base base;
	int gid = 0;
	for (auto itr = chain_list.begin(); itr != chain_list.end(); itr++) {
		gid = itr->groupid;
		for (auto b : itr->rawid) {
			base.group_id = gid;
			base.pos = b.first * 10 + 1;
			base.rawid = b.second;
			if (z_map.count(b.first) == 0) base.z = 0;
			else base.z = z_map.at(b.first);

			base.flg_d[0] = 0;
			base.flg_d[1] = 0;
			base.flg_i[0] = 0;
			base.flg_i[1] = 0;
			base.flg_i[2] = 0;
			base.flg_i[3] = 0;

			ret.insert(std::make_pair(b.first, base));
		}
		gid++;
	}
	return ret;
}

void basetrack_pick_up2(std::string file_path, int pl, std::vector<mfile0::M_Base*>&base_p) {

	std::stringstream file_in_bvxx;
	file_in_bvxx << file_path << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	if (!std::filesystem::exists(file_in_bvxx.str())) {
		fprintf(stderr, "file[%s] not exist\n", file_in_bvxx.str().c_str());
		exit(1);
	}

	printf("Read %s\n", file_in_bvxx.str().c_str());
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx.str(), pl, 0);
	std::map<int, vxx::base_track_t> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_map.insert(std::make_pair(itr->rawid, *itr));
	}
	int rawid = 0;
	for (auto itr = base_p.begin(); itr != base_p.end(); itr++) {
		auto res = base_map.find((*itr)->rawid);
		(*itr)->ax = res->second.ax;
		(*itr)->ay = res->second.ay;
		(*itr)->x = res->second.x;
		(*itr)->y = res->second.y;

		(*itr)->ph = res->second.m[0].ph + res->second.m[1].ph;

	}
}
void basetrack_pick_up(std::string file_path, int pl, std::vector<mfile0::M_Base*>&base_p) {

	std::stringstream file_in_bvxx;
	file_in_bvxx << file_path << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	if (!std::filesystem::exists(file_in_bvxx.str())) {
		fprintf(stderr, "file[%s] not exist\n", file_in_bvxx.str().c_str());
		exit(1);
	}

	printf("Read %s\n", file_in_bvxx.str().c_str());
	vxx::BvxxReader br;

	int rawid = 0;
	for (auto itr = base_p.begin(); itr != base_p.end(); itr++) {
		rawid = (*itr)->rawid;
		std::vector<vxx::base_track_t> b;
		vxx::BvxxReader br;
		std::array<int, 2> index = { rawid, rawid + 1 };//1234<=rawid<=5678であるようなものだけを読む。
		b = br.ReadAll(file_in_bvxx.str(), pl, 0, vxx::opt::index = index);
		(*itr)->ax = b.begin()->ax;
		(*itr)->ay = b.begin()->ay;
		(*itr)->x = b.begin()->x;
		(*itr)->y = b.begin()->y;

		(*itr)->ph = b.begin()->m[0].ph + b.begin()->m[1].ph;

	}
}
void basetrack_mfile_trans(std::vector<mfile0::M_Base*>&base_p, int pl, std::vector<corrmap0::Corrmap>&corr_abs) {

	corrmap0::Corrmap param;
	bool flg = false;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		if (itr->pos[0] / 10 == pl) {
			flg = true;
			param = *itr;
			break;
		}
	}
	if (!flg) {
		fprintf(stderr, "PL%03d corrmap abs not found\n", pl);
		return;
	}

	double tmp_x, tmp_y;
	for (auto itr = base_p.begin(); itr != base_p.end(); itr++) {
		tmp_x = (*itr)->x;
		tmp_y = (*itr)->y;
		(*itr)->x = tmp_x * param.position[0] + tmp_y * param.position[1] + param.position[4];
		(*itr)->y = tmp_x * param.position[2] + tmp_y * param.position[3] + param.position[5];
		tmp_x = (*itr)->ax;
		tmp_y = (*itr)->ay;
		(*itr)->ax = tmp_x * param.angle[0] + tmp_y * param.angle[1] + param.angle[4];
		(*itr)->ay = tmp_x * param.angle[2] + tmp_y * param.angle[3] + param.angle[5];
	}


}

mfile0::Mfile basetrack2chian(std::multimap<int, mfile0::M_Base> &chains) {
	//chains(gid,M_Base)
	//gid_cid(gid,chainid)

	mfile0::Mfile ret;
	int  num = 0, count = 0;
	int64_t gid;
	for (auto itr = chains.begin(); itr != chains.end(); itr++) {
		count = chains.count(itr->first);
		auto range = chains.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			mfile0::M_Chain c;
			c.basetracks.push_back(res->second);
			c.chain_id = num;
			c.nseg = c.basetracks.size();
			c.pos0 = c.basetracks.begin()->pos;
			c.pos1 = c.basetracks.rbegin()->pos;
			ret.chains.push_back(c);
			num++;
		}
		itr = std::next(itr, count - 1);
	}
	//chainを構成するbasetrackをlistにつめる

	ret.header.head[0] = "% Created by mkmf";
	ret.header.head[1] = "% on 2007 / 12 / 25 23:28 : 09 + 09:00 (JST)";
	ret.header.head[2] = "	0       0   3   0      0.0   0.0000";
	std::set<int> pos;
	for (int i = 0; i < ret.chains.size(); i++) {
		for (int j = 0; j < ret.chains[i].basetracks.size(); j++) {
			pos.insert(ret.chains[i].basetracks[j].pos);
		}
	}

	ret.header.num_all_plate = int(pos.size());

	for (auto itr = pos.begin(); itr != pos.end(); itr++) {
		ret.header.all_pos.push_back(*itr);
	}

	return ret;

}
int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}


