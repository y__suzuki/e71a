#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>

void mfile_read_write_bin(std::string file_in_mfile, std::string file_out_mfile_path);
void read_chain(std::ifstream &ifs, mfile1::MFileChain &chain, std::vector< mfile1::MFileBase> &basetracks);
int write_group(std::ofstream ofs_array[10], const int of_num, std::vector< mfile1::MFileChain> &group, std::vector< std::vector< mfile1::MFileBase>> &all_basetracks);
void write_header(std::ofstream &ofs, uint64_t chain_num, uint64_t base_num, mfile1::MFileHeader &head, mfile1::MFileInfoHeader &inf_head);
int group_classify(std::vector< mfile1::MFileChain> &group, std::vector< std::vector< mfile1::MFileBase>> &all_basetracks);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage : prg_name [input m-file-bin] [output m-file-bin]\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile_read_write_bin(file_in_mfile, file_out_mfile);



}

void mfile_read_write_bin(std::string file_in_mfile, std::string file_out_mfile_path) {

	std::string file_out_mfile_1 = file_out_mfile_path + "\\m_chain1.bmf";
	std::string file_out_mfile_2 = file_out_mfile_path + "\\m_base1.bmf";
	std::string file_out_mfile_3 = file_out_mfile_path + "\\m_base2_no_continue.bmf";
	std::string file_out_mfile_4 = file_out_mfile_path + "\\m_base2.bmf";
	std::string file_out_mfile_5 = file_out_mfile_path + "\\m_base3.bmf";
	std::string file_out_mfile_6 = file_out_mfile_path + "\\m_base4.bmf";
	std::string file_out_mfile_7 = file_out_mfile_path + "\\m_base5.bmf";
	std::string file_out_mfile_8 = file_out_mfile_path + "\\m_base6.bmf";
	std::string file_out_mfile_9 = file_out_mfile_path + "\\m_base7.bmf";
	std::string file_out_mfile_10 = file_out_mfile_path + "\\m_base_over7.bmf";

	const int of_num = 10;
	std::ofstream ofs[of_num];
	ofs[0].open(file_out_mfile_1, std::ios::binary);
	ofs[1].open(file_out_mfile_2, std::ios::binary);
	ofs[2].open(file_out_mfile_3, std::ios::binary);
	ofs[3].open(file_out_mfile_4, std::ios::binary);
	ofs[4].open(file_out_mfile_5, std::ios::binary);
	ofs[5].open(file_out_mfile_6, std::ios::binary);
	ofs[6].open(file_out_mfile_7, std::ios::binary);
	ofs[7].open(file_out_mfile_8, std::ios::binary);
	ofs[8].open(file_out_mfile_9, std::ios::binary);
	ofs[9].open(file_out_mfile_10, std::ios::binary);
	std::ifstream ifs(file_in_mfile, std::ios::binary);


	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }
	//Mfile headerの書き込み
	//mfile info headerの書き込み
	for (int i = 0; i < of_num; i++) {
		ofs[i].write((char*)& mfile.header, sizeof(mfile1::MFileHeader));
		ofs[i].write((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	}

	std::vector< mfile1::MFileChain> group;
	std::vector< std::vector< mfile1::MFileBase>> all_basetracks;
	mfile1::MFileChain w_chain;
	std::vector< mfile1::MFileBase> w_base;
	uint64_t count = 0, r_base_num = 0, r_chain_num = 0, r_group_num = 0;
	uint64_t w_chain_num[10] = {}, w_base_num[10] = {};
	int64_t gid = -1;
	int num;
	for (int64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		mfile1::MFileChain chain;
		std::vector< mfile1::MFileBase> basetracks;
		//chainの読み込み
		read_chain(ifs, chain, basetracks);

		//読んだchainが前のchainと同じgroupか確認
		//最後のchainを読み込んだ時
		if (c + 1 == mfile.info_header.Nchain) {
			//最後のchainが違うgroups
			if (gid != basetracks.begin()->group_id) {
				////////////////////////////
				//今までのgroupの書き出し
				r_group_num++;
				gid = basetracks.begin()->group_id;
				num = write_group(ofs,of_num, group, all_basetracks);
				for (int i = 0; i < group.size(); i++) {
					w_base_num[num] += group[i].nseg;
					w_chain_num[num] += 1;
				}
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();
				////////////////////////////
				//最後のgroupの書き出し
				all_basetracks.emplace_back(basetracks);
				group.emplace_back(chain);
				r_group_num++;
				gid = basetracks.begin()->group_id;
				num = write_group(ofs, of_num, group, all_basetracks);
				for (int i = 0; i < group.size(); i++) {
					w_base_num[num] += group[i].nseg;
					w_chain_num[num] += 1;
				}
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();
			}
			else {
				//今のchainをpush back
				all_basetracks.emplace_back(basetracks);
				group.emplace_back(chain);
				r_group_num++;
				gid = basetracks.begin()->group_id;
				////////////////////////////
				//今までのgroupの書き出し
				r_group_num++;
				gid = basetracks.begin()->group_id;
				num = write_group(ofs, of_num, group, all_basetracks);
				for (int i = 0; i < group.size(); i++) {
					w_base_num[num] += group[i].nseg;
					w_chain_num[num] += 1;
				}
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();
			}
		}
		else if (group.size() != 0 && gid != basetracks.begin()->group_id) {
			r_group_num++;
			gid = basetracks.begin()->group_id;
			num = write_group(ofs, of_num, group, all_basetracks);
			for (int i = 0; i < group.size(); i++) {
				w_base_num[num] += group[i].nseg;
				w_chain_num[num] += 1;
			}
			group.clear();
			for (int64_t i = 0; i < all_basetracks.size(); i++) {
				all_basetracks[i].clear();
			}
			all_basetracks.clear();
		}
		else if (c == 0) {
			gid = basetracks.begin()->group_id;
		}
		all_basetracks.emplace_back(basetracks);
		group.emplace_back(chain);

	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;
	for (int i = 0; i < of_num; i++) {
		write_header(ofs[i], w_chain_num[i], w_base_num[i], mfile.header, mfile.info_header);
	}

	printf("mfile info\n");
	for (int i = 0; i < 10; i++) {
		printf("group-%d num =%lld\n", i + 1, w_chain_num[i] / (i + 1));
		printf("\t chain num =%lld \n", w_chain_num[i]);
		printf("\t base  num =%lld \n", w_base_num[i]);
	}
}

void read_chain(std::ifstream &ifs, mfile1::MFileChain &chain, std::vector< mfile1::MFileBase> &basetracks) {

	//一旦chainを読む
	ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
	if (ifs.eof()) { throw std::exception(); }
	basetracks.clear();
	basetracks.reserve(chain.nseg);

	for (int b = 0; b < chain.nseg; b++) {
		mfile1::MFileBase base;
		ifs.read((char*)& base, sizeof(mfile1::MFileBase));
		if (ifs.eof()) { throw std::exception(); }
		basetracks.emplace_back(base);
	}
}
int write_group(std::ofstream ofs_array[10],const int of_num , std::vector< mfile1::MFileChain> &group, std::vector< std::vector< mfile1::MFileBase>> &all_basetracks) {
	int g_cls = group_classify(group, all_basetracks);
	int num = 0;
	if (g_cls < of_num) {
		num = g_cls;
	}
	else {
		num = of_num - 1;
	}

	for (int i = 0; i < group.size(); i++) {
		//書き出し
		ofs_array[num].write((char*)& group[i], sizeof(mfile1::MFileChain));
		assert(group[i].nseg == all_basetracks[i].size());
		for (int b = 0; b < group[i].nseg; b++) {
			ofs_array[num].write((char*)& all_basetracks[i][b], sizeof(mfile1::MFileBase));
		}
	}
	return num;

}
int group_classify(std::vector< mfile1::MFileChain> &group, std::vector< std::vector< mfile1::MFileBase>> &all_basetracks) {
	//0 1chain
	//1 base_max=1
	//2 base_max=2 but not continue
	//3 base_max=2
	//4- return base_max+1
	if (group.size() == 1)return 0;
	std::map<std::pair<int, int>, mfile1::MFileBase> unique_base;
	for (auto &c : all_basetracks) {
		for (auto &b : c) {
			auto res = unique_base.insert(std::make_pair(std::make_pair(b.pos / 10, b.rawid), b));
		}
	}
	std::map<int, int> unique_base_count;
	for (auto itr = unique_base.begin(); itr != unique_base.end(); itr++) {
		auto res = unique_base_count.insert(std::make_pair(itr->first.first, 1));
		if (!res.second) {
			res.first->second++;
		}
	}

	int max_base = 0;
	for (auto itr = unique_base_count.begin(); itr != unique_base_count.end();itr++) {
		max_base = std::max(itr->second, max_base);
	}
	if (max_base == 1)return 1;
	else if (max_base == 2) {
		bool flg = false;
		for (auto itr = unique_base_count.begin(); itr != unique_base_count.end(); itr++) {
			if (itr->second == 2) {
				if (flg)return 3;
				flg = true;
			}
			else {
				flg = false;
			}
		}
		return 2;
	}
	else {
		return max_base+1 ;
	}
	return max_base + 1;
}
void write_header(std::ofstream &ofs, uint64_t chain_num, uint64_t base_num, mfile1::MFileHeader &head, mfile1::MFileInfoHeader &inf_head) {

	inf_head.Nchain = chain_num;
	inf_head.Nbasetrack = base_num;

	ofs.clear();
	ofs.seekp(0, std::ios::beg);

	ofs.write((char*)& head, sizeof(mfile1::MFileHeader));
	ofs.write((char*)& inf_head, sizeof(mfile1::MFileInfoHeader));

}