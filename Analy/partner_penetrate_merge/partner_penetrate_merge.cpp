#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#include <chrono>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

bool sort_chain(const mfile0::M_Chain &left, const mfile0::M_Chain &right) {
	int gid_l = left.basetracks.begin()->group_id;
	int gid_r = right.basetracks.begin()->group_id;
	if (gid_l != gid_r)return gid_l < gid_r;

	int tid_l = left.chain_id;
	int tid_r = right.chain_id;
	if (tid_l != tid_r)return tid_l < tid_r;

	int tid2_l = left.basetracks.begin()->flg_i[1];
	int tid2_r = right.basetracks.begin()->flg_i[1];
	return tid2_l < tid2_r;
}

std::vector<mfile0::M_Chain>  merge_chain(std::vector<mfile0::M_Chain> &c1, std::vector<mfile0::M_Chain> &c2);
mfile0::M_Chain merge_chain(mfile0::M_Chain &c1, mfile0::M_Chain &c2);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-mfile-event file-in-mfile-penetarte file-out-mfile\n");
		exit(1);
	}
	std::string file_in_event = argv[1];
	std::string file_in_penetrate_cand = argv[2];
	std::string file_out_muon = argv[3];

	mfile0::Mfile m, pene_cand;
	mfile0::read_mfile(file_in_event, m);
	mfile0::read_mfile(file_in_penetrate_cand, pene_cand);

	m.chains = merge_chain(m.chains, pene_cand.chains);

	mfile0::write_mfile(file_out_muon, m);
}
std::vector<mfile0::M_Chain>  merge_chain(std::vector<mfile0::M_Chain> &c1, std::vector<mfile0::M_Chain> &c2) {
	std::vector<mfile0::M_Chain> ret;

	std::map<std::pair<int, int>, mfile0::M_Chain> partner;
	std::multimap<int, mfile0::M_Chain> event_chains;
	std::multimap<std::pair<int, int>, mfile0::M_Chain> pene_cand;
	for (auto itr = c1.begin(); itr != c1.end(); itr++) {
		partner.insert(std::make_pair(std::make_pair(itr->basetracks.begin()->group_id, itr->chain_id), *itr));
		event_chains.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}
	for (auto itr = c2.begin(); itr != c2.end(); itr++) {
		pene_cand.insert(std::make_pair(std::make_pair(itr->basetracks.begin()->group_id, itr->basetracks.begin()->flg_i[1] / 100000), *itr));
	}

	int gid = 0;
	int cid = 0;
	int tid = 0;
	int prev_cid = 0;
	for (auto itr = partner.begin(); itr != partner.end(); itr++) {
		//muonはスルー
		if (itr->second.chain_id == 0)continue;

		gid = itr->first.first;
		cid = itr->first.second;
		tid = 0;
		prev_cid = itr->second.chain_id;
		//penetrateのIDごとに表示したい
		for (auto &b : itr->second.basetracks) {
			b.group_id = gid * 100000 + cid;
			b.flg_i[2] = 1;
		}
		itr->second.chain_id = tid;
		tid++;
		ret.push_back(itr->second);
		if (event_chains.count(itr->first.first) != 0) {
			//eventのchainも同時に乗せる
			auto  range = event_chains.equal_range(itr->first.first);
			for (auto res = range.first; res != range.second; res++) {
				if (prev_cid == res->second.chain_id)continue;
				mfile0::M_Chain c_tmp = res->second;
				for (auto &b : c_tmp.basetracks) {
					b.group_id = gid * 100000 + cid;
					b.flg_i[2] = 1;
				}
				c_tmp.chain_id = tid;
				tid++;
				ret.push_back(c_tmp);

			}
		}
		if (pene_cand.count(itr->first) == 0)continue;
		auto range = pene_cand.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			//partnerと重複basetrackは削除
			mfile0::M_Chain chain_add = merge_chain(itr->second, res->second);
			if (chain_add.basetracks.size() == 0)continue;
			chain_add.chain_id = tid;
			for (auto &b : chain_add.basetracks) {
				b.group_id = gid * 100000 + cid;
			}
			tid++;

			ret.push_back(chain_add);

		}
	}

	return ret;
}
mfile0::M_Chain merge_chain(mfile0::M_Chain &c1, mfile0::M_Chain &c2) {
	std::set<std::pair<int, int>> base_id;
	for (auto itr = c1.basetracks.begin(); itr != c1.basetracks.end(); itr++) {
		base_id.insert(std::make_pair(itr->pos / 10, itr->rawid));
	}
	mfile0::M_Chain ret;
	for (auto itr = c2.basetracks.begin(); itr != c2.basetracks.end(); itr++) {
		if (base_id.count(std::make_pair(itr->pos / 10, itr->rawid)) == 0) {
			ret.basetracks.push_back(*itr);
		}
	}
	ret.chain_id = c2.chain_id;
	ret.nseg = ret.basetracks.size();
	if (ret.nseg != 0) {
		ret.pos0 = ret.basetracks.begin()->pos;
		ret.pos1 = ret.basetracks.rbegin()->pos;
	}
	else {
		ret.pos0 = 0;
		ret.pos1 = 0;
	}
	return ret;
}

