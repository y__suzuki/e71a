#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <map>
class Track_file {
public:
	int eventid, trackid, pl, rawid;
};
class Group_file :public Track_file {
public:
	int link_num;
	std::vector<std::tuple<int, int, int, int>> linklet;
};

void output_group(std::string filename, std::vector<Group_file>&g);
std::vector<Group_file> mfile_to_gourp(std::vector<mfile0::M_Chain>&chains);
void set_linklet_vector(std::set<std::tuple<int, int, int, int>> &linklet_set, Group_file &g);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string file_in_group_mfile = argv[1];
	std::string file_out_group_file = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_group_mfile, m);

	std::vector<Group_file> groups = mfile_to_gourp(m.chains);


	output_group(file_out_group_file, groups);
}
std::vector<Group_file> mfile_to_gourp(std::vector<mfile0::M_Chain>&chains) {
	std::vector<Group_file> ret;

	std::multimap<int,mfile0::M_Chain> group;
	Group_file g;
	int gid = -1;
	int vertex_pl = -1;
	std::set<std::tuple<int, int, int, int>> linklet_set;

	for (auto &c : chains) {
		if (c.chain_id == 0 && gid != c.basetracks.begin()->group_id / 10) {
			vertex_pl = c.basetracks.rbegin()->pos / 10;
		}
		if (c.chain_id == 0)continue;
		if (gid  != c.basetracks.begin()->group_id / 10) {
			if (linklet_set.size() > 0) {
				g.trackid = (gid % 10000) * 10 + 1;
				set_linklet_vector(linklet_set, g);
				ret.push_back(g);
			}

			gid = c.basetracks.begin()->group_id / 10;
			printf("event %5d chain %5d\n", gid / 10000, gid % 10000);
			g.eventid = gid / 10000;
			g.trackid = (gid % 10000) * 10 + 0;
			
			for (auto &b : c.basetracks) {
				g.pl = b.pos / 10;
				g.rawid = b.rawid;
				if (b.pos / 10 >= vertex_pl) {
					break;
				}
			}
			printf("PL%03d rawid=%d\n", g.pl, g.rawid);
			linklet_set.clear();
			for (int i = 1; i < c.basetracks.size(); i++) {
				linklet_set.insert(std::make_tuple(
					(c.basetracks[i - 1].pos / 10) * 10, (c.basetracks[i].pos / 10) * 10,
					c.basetracks[i - 1].rawid, c.basetracks[i].rawid
				));
			}
			set_linklet_vector(linklet_set, g);
			ret.push_back(g);
			linklet_set.clear();
		}
		else  {
			for (int i = 1; i < c.basetracks.size(); i++) {
				linklet_set.insert(std::make_tuple(
					(c.basetracks[i - 1].pos/10)*10, (c.basetracks[i].pos / 10) * 10,
					c.basetracks[i - 1].rawid, c.basetracks[i].rawid
				));
			}
		}
	}
	if (linklet_set.size() > 0) {
		g.trackid = (gid % 10000) * 10 + 1;
		set_linklet_vector(linklet_set, g);
		ret.push_back(g);
	}


	return ret;
}
void set_linklet_vector(std::set<std::tuple<int, int, int, int>> &linklet_set, Group_file &g) {
	g.linklet.clear();
	for (auto itr = linklet_set.begin(); itr != linklet_set.end(); itr++) {
		g.linklet.push_back(*itr);
	}
	g.link_num = g.linklet.size();
}
void output_group(std::string filename, std::vector<Group_file>&g) {
	std::ofstream ofs(filename);
	int count = 0;
	for (auto itr = g.begin(); itr != g.end(); itr++) {
		if (count % 1000 == 0) {
			fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)", count, g.size(), count*100. / g.size());
		}
		count++;

		ofs << std::fixed << std::right
			<< std::setw(10) << std::setprecision(0) << itr->eventid << " "
			<< std::setw(5) << std::setprecision(0) << itr->trackid << " "
			<< std::setw(4) << std::setprecision(0) << itr->pl << " "
			<< std::setw(4) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(10) << std::setprecision(0) << itr->link_num << std::endl;
		if (itr->linklet.size() == 0)continue;
		for (auto itr2 = itr->linklet.begin(); itr2 != itr->linklet.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(5) << std::setprecision(0) << std::get<0>(*itr2) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(*itr2) << std::endl;
		}
	}
	fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)\n", count, g.size(), count*100. / g.size());

}

