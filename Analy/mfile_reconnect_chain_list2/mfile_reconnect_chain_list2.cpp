#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>

void chain_start_stop_extract(chain::Chain_file &chain, std::set<std::pair<int, int>>&start, std::set<std::pair<int, int>>&stop);
void read_overflow_link(std::string filename, std::set<std::tuple<int, int, int, int>>&linklet);
void linklet_start_stop_extract(std::set<std::tuple<int, int, int, int>>&linklet, std::set<std::pair<int, int>>&start, std::set<std::pair<int, int>>&stop);

void write_track_list(std::string filename, std::set<std::pair<int, int>>&track);

class Chain_id {
public:
	int pl;
	uint64_t rawid;
};
int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-chain-path in-overflow-path out-start-list(bin) out-stop-list(bin)\n");
		exit(1);
	}
	std::string file_in_chain = argv[1];
	std::string file_in_overflow = argv[2];
	std::string file_out_txt0 = argv[3];
	std::string file_out_txt1 = argv[4];

	std::set<std::pair<int, int>>start, stop;
	//start PL小
	//stop PL大
	std::set<std::tuple<int, int, int, int>>linklet;
	read_overflow_link(file_in_overflow,linklet);
	linklet_start_stop_extract(linklet, start, stop);


	chain::Chain_file chain;
	chain::read_chain(file_in_chain, chain);
	chain_start_stop_extract(chain, start, stop);

	write_track_list(file_out_txt0, start);
	write_track_list(file_out_txt1, stop);

	//write_reconnect_list(file_in_mfile, file_out_txt0, file_out_txt1);
}

void chain_start_stop_extract(chain::Chain_file &chain, std::set<std::pair<int, int>>&start, std::set<std::pair<int, int>>&stop) {
	int64_t count = 0;
	for (int64_t i = 0; i < chain.chains.size(); i++) {
		if (count % 100000 == 0) {
			printf("\r group extract edge basetrack %d/%d", count, chain.chains.size());
		}
		count++;

		for (int64_t j = 0; j < chain.chains[i].size(); j++) {
			if (chain.chains[i][j].nseg < 1)continue;
			start.insert(*chain.chains[i][j].rawid.begin());
			stop.insert(*chain.chains[i][j].rawid.rbegin());
		}
	}

	printf("\r group extract edge basetrack %d/%d\n", count, chain.chains.size());

}

void read_overflow_link(std::string filename, std::set<std::tuple<int, int, int, int>>&linklet) {
	std::ifstream ifs(filename);
	if (!ifs.is_open()) {
		printf("file[%s]not exist\n", filename.c_str());
		printf("overflow is nothing\n");
		return;
	}
	std::string str;
	int64_t link_num = 0,count=0;
	std::tuple<int, int, int, int> link;
	while (std::getline(ifs, str)) {
		if (count % 10000 == 0) {
			printf("\r read overflow group %lld", count);
		}
		count++;

		auto strs = StringSplit_with_tab(str);
		if (strs[0] == "#") continue;
		if (strs.size() != 4) {
			fprintf(stderr, "format error(overflow file)\n");
			exit(1);
		}
		//pos-raw-pos-raw
		std::get<0>(link) = std::stoi(strs[0]);
		std::get<1>(link) = std::stoi(strs[1]);
		std::get<2>(link) = std::stoi(strs[2]);
		std::get<3>(link) = std::stoi(strs[3]);
		linklet.insert(link);
	}
	printf("\r read overflow group %lld\n", count);



}
void linklet_start_stop_extract(std::set<std::tuple<int, int, int, int>>&linklet, std::set<std::pair<int, int>>&start, std::set<std::pair<int, int>>&stop) {
	if (linklet.size() == 0)return;

	std::set<std::pair<int, int>> all_base;
	std::multimap<std::pair<int, int>, std::pair<int, int>> next_path;
	std::multimap<std::pair<int, int>, std::pair<int, int>> prev_path;

	std::pair<int, int>base0, base1;
	for (auto itr = linklet.begin(); itr != linklet.end(); itr++) {
		base0.first = std::get<0>(*itr);
		base0.second = std::get<1>(*itr);
		base1.first = std::get<2>(*itr);
		base1.second = std::get<3>(*itr);

		all_base.insert(base0);
		all_base.insert(base1);
		next_path.insert(std::make_pair(base0, base1));
		prev_path.insert(std::make_pair(base1, base0));

	}
	for (auto itr = all_base.begin(); itr != all_base.end(); itr++) {
		//PL番号最大
		if (next_path.count(*itr) == 0) {
			stop.insert(*itr);
		}
		//PL番号最小
		if (prev_path.count(*itr) == 0) {
			start.insert(*itr);
		}
	}

}


void write_track_list(std::string filename, std::set<std::pair<int, int>>&track) {
	std::ofstream ofs(filename, std::ios::binary);
	std::vector< Chain_id >track_v;
	track_v.reserve(track.size());

	for (auto itr = track.begin(); itr != track.end(); itr++) {
		Chain_id c;
		c.pl = itr->first / 10;
		c.rawid = itr->second;
		track_v.push_back(c);
	}
	
	if (track_v.size() == 0) {
		fprintf(stderr, "target track ... null\n");
		fprintf(stderr, "File[%s] has no data\n", filename.c_str());
	}
	else {
		int64_t count = 0;
		int64_t max = track_v.size();

		for (int i = 0; i < max; i++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)", count, int(track_v.size()), count*100. / track_v.size());
			}
			count++;
			ofs.write((char*)& track_v[i], sizeof(Chain_id));
		}
		fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)\n", count, int(track_v.size()), count*100. / track_v.size());
	}

}

