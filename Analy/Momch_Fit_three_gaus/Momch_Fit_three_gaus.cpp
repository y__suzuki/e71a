
#define _CRT_SECURE_NO_WARNINGS
//#pragma comment(lib, "VxxReader.lib")
//#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <filesystem>
#include <omp.h>

std::vector<Momentum_recon::Mom_chain> cut_momch(std::vector<Momentum_recon::Mom_chain> &momch, int i_ang, int i_mom);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-momch\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::vector<Momentum_recon::Mom_chain> sel = cut_momch(momch, 1, 1);
}

std::vector<Momentum_recon::Mom_chain> cut_momch(std::vector<Momentum_recon::Mom_chain> &momch, int i_ang, int i_mom) {
	double mom_min = i_mom * 100;
	double mom_max = (i_mom + 1) * 100;
	double angle_min, angle_max;
	if (i_ang < 7) {
		angle_min = i_ang * 0.1;
		angle_max = (i_ang + 1)*0.1;
	}
	else if (i_ang < 11) {
		angle_min = (i_ang - 7) * 0.2 + 0.7;
		angle_max = (i_ang - 7 + 1)*0.2 + 0.7;
	}
	else if (i_ang < 14) {
		angle_min = (i_ang - 11) * 0.4 + 1.5;
		angle_max = (i_ang - 11 + 1)*0.4 + 1.5;
	}
	else {
		angle_min = (i_ang - 14) * 0.6 + 3.1;
		angle_max = (i_ang - 14 + 1)*0.6 + 3.1;
	}

	std::vector<Momentum_recon::Mom_chain> ret;
	double ax, ay,angle;
	int count = 0;
	for (auto &c : momch) {
		ax = 0;
		ay = 0;
		count = 0;
		for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
			ax += itr->ax;
			ay += itr->ay;
			count++;
		}
		ax = ax / count;
		ay = ay / count;
		angle = sqrt(ax*ax + ay * ay);
		if (angle < angle_min)continue;
		if (angle_max <= angle)continue;
		if (c.ecc_mcs_mom < mom_min)continue;
		if (mom_max<=c.ecc_mcs_mom )continue;
		ret.push_back(c);
	}
	printf("%.1lf<angle<%.1lf , %.0lf<pb<%.0lf, %d --> %d\n", angle_min, angle_max, mom_min, mom_max);
	return ret;
}
