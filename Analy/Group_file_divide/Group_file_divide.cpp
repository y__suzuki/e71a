#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

using namespace l2c;

class Chain_baselist {
public:
	int64_t groupid, trackid;
	std::pair<int32_t, int64_t> target_track;
	std::vector<std::tuple<int, int, int, int>>ltlist;
};

std::vector < Chain_baselist > read_linklet_list2_bin(std::string filename);
void output_group_bin(std::string filename, std::vector< Chain_baselist>&g);

int main(int argc,char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:\n");
		exit(1);
	}
	std::string file_in_group = argv[1];
	int interval = std::stoi(argv[2]);
	std::string file_out_path = argv[3];

	std::vector < Chain_baselist > group = read_linklet_list2_bin(file_in_group);
	std::vector < Chain_baselist > g_out;
	g_out.reserve(interval);
	
	char out_file[256];
	int loop_num = 0;
	std::vector<std::string >file_list;
	for (int i = 0; i < group.size(); i++) {
		g_out.push_back(group[i]);
		if (g_out.size() == interval) {
			sprintf_s(out_file, "%s\\group_%04d.bin", file_out_path.c_str(),loop_num);
			file_list.push_back(out_file);
			output_group_bin(out_file, g_out);
			loop_num++;
			g_out.clear();
		}
	}
	if (g_out.size() > 0) {
		sprintf_s(out_file, "%s\\group_%04d.bin", file_out_path.c_str(), loop_num);
		file_list.push_back(out_file);
		output_group_bin(out_file, g_out);
	}

	sprintf_s(out_file, "%s\\group_outlist.txt", file_out_path.c_str());
	std::ofstream ofs(out_file);
	for (int i = 0; i < file_list.size(); i++) {
		ofs << file_list[i] << std::endl;
	}

}
std::vector < Chain_baselist > read_linklet_list2_bin(std::string filename) {
	std::vector < Chain_baselist > ret;
	std::ifstream ifs(filename, std::ios::binary);
	int gid, tid, muon_num, t_pl, t_rawid;
	int64_t link_num;
	int pl, raw;
	double weight;
	std::tuple<int, int, int, int> link;
	int count = 0;
	while (ifs.read((char*)& gid, sizeof(int))) {
		//ifs.read((char*)& gid, sizeof(int));
		ifs.read((char*)& tid, sizeof(int));
		ifs.read((char*)& t_pl, sizeof(int));
		ifs.read((char*)& t_rawid, sizeof(int));
		ifs.read((char*)& link_num, sizeof(int));
		//while (ifs >> gid >> tid >> t_pl >> t_rawid >> link_num) {
		if (count % 10000 == 0) {
			printf("\r read group %d gid=%d link=%lld", count, gid, link_num);
		}
		count++;
		Chain_baselist c;
		c.groupid = gid;
		c.trackid = tid;
		c.target_track.first = t_pl;
		c.target_track.second = t_rawid;
		for (int64_t i = 0; i < link_num; i++) {
			ifs.read((char*)& std::get<0>(link), sizeof(int));
			ifs.read((char*)& std::get<1>(link), sizeof(int));
			ifs.read((char*)& std::get<2>(link), sizeof(int));
			ifs.read((char*)& std::get<3>(link), sizeof(int));
			//ifs >> std::get<0>(link) >> std::get<1>(link) >> std::get<2>(link) >> std::get<3>(link);
			c.ltlist.push_back(link);
		}
		ret.push_back(c);
	}
	printf("\r read group %d gid=%d link=%lld\n", count, gid, link_num);

	return ret;

}

void output_group_bin(std::string filename, std::vector< Chain_baselist>&g) {
	std::ofstream ofs(filename, std::ios::binary);
	int count = 0, link_num, pl, rawid;
	for (auto itr = g.begin(); itr != g.end(); itr++) {
		if (count % 1000 == 0) {
			fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)", count, g.size(), count*100. / g.size());
		}
		count++;
		link_num = itr->ltlist.size();
		pl = itr->target_track.first;
		rawid = itr->target_track.second;
		ofs.write((char*)& itr->groupid, sizeof(int));
		ofs.write((char*)& itr->trackid, sizeof(int));
		ofs.write((char*)& pl, sizeof(int));
		ofs.write((char*)& rawid, sizeof(int));
		ofs.write((char*)& link_num, sizeof(int));
		if (link_num == 0)continue;
		for (auto itr2 = itr->ltlist.begin(); itr2 != itr->ltlist.end(); itr2++) {
			ofs.write((char*)& std::get<0>(*itr2), sizeof(int));
			ofs.write((char*)& std::get<1>(*itr2), sizeof(int));
			ofs.write((char*)& std::get<2>(*itr2), sizeof(int));
			ofs.write((char*)& std::get<3>(*itr2), sizeof(int));
		}
	}
	fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)\n", count, g.size(), count*100. / g.size());

}
