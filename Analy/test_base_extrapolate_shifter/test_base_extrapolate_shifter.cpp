//数字の小さいPLから大きいPLにbasetrackを外挿する。
//下流-->上流
#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib,"FILE_structure.lib")

#include <VxxReader.h>
#include <FILE_structure.hpp>
#include <set>
void Apply_alignment1(std::vector<vxx::base_track_t> &track, std::vector<corrmap0::Corrmap> &corr);
void basetrack_trans(std::vector<vxx::base_track_t> &track, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size);
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size);
void track_tans_affin(vxx::base_track_t  *track, corrmap0::Corrmap corr);

void base_extra(std::vector<vxx::base_track_t> &track, double nominal_gap);

void base_output_txt(std::vector<vxx::base_track_t> &track, std::set<int > rawid0, std::set<int > rawid1);
std::vector<vxx::base_track_t> base_sel(std::vector<vxx::base_track_t> &track, std::set<int > rawid0, std::set<int > rawid1);

//main関数
int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg input-bvxx1 pl corrmap nominal-gap output-bvxx\n");
		//prg終了
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_corrmap = argv[3];
	double nominal_gap = std::stod(argv[4]);
	std::string file_out_bvxx = argv[5];



	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);
	//std::set<int > rawid0{ 348774749,693686779,197789727, 692833227,693389283 };
	//std::set<int > rawid1{ 549239721, 548998730,548639519, 549398507, 145211309 };
	//base = base_sel(base, rawid0, rawid1);

	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corrmap, corr);


	Apply_alignment1(base, corr);
	//base_output_txt(base,rawid0,rawid1);
	//printf("\n\n");
	base_extra(base, nominal_gap);
	//base_output_txt(base,rawid0,rawid1);

	vxx::BvxxWriter bw;
	bw.Write(file_out_bvxx, pl, 0, base);

}



//上流-->下流に外挿する際の上流basetrackの変換
void Apply_alignment1(std::vector<vxx::base_track_t> &track, std::vector<corrmap0::Corrmap> &corr) {
	//corrmapの範囲を逆変換
	double hash_size = 4000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	corrmap_area_inverse(corr, corr_map, hash_size);
	basetrack_trans(track, corr_map, hash_size);

}
void basetrack_trans(std::vector<vxx::base_track_t> &track, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size) {

	
	double base_hash = hash_size ;
	std::pair<int, int> id;
	std::multimap < std::pair<int, int>, vxx::base_track_t*> base_hash_map;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		id.first = itr->x / base_hash;
		id.second= itr->y / base_hash;
		base_hash_map.insert(std::make_pair(id, &(*itr)));
	}
	
	//hashしたbasetrackの平均位置とbasetrack vectorを詰める
	std::map < std::pair<double, double >, std::vector<vxx::base_track_t*>> base_hash_map_v;
	for (auto itr = base_hash_map.begin(); itr != base_hash_map.end(); itr++) {
		int count = base_hash_map.count(itr->first);
		std::vector<vxx::base_track_t*> base_v;
		std::pair<double, double> center;
		center.first = 0;
		center.second = 0;
		auto range = base_hash_map.equal_range(itr->first);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			center.first += itr2->second->x;
			center.second += itr2->second->y;
			base_v.push_back(itr2->second);
		}
		center.first =center.first/count;
		center.second = center.second / count;
		base_hash_map_v.insert(std::make_pair(center, base_v));

		itr = std::next(itr, count - 1);
	}
	
	
	
	
	int ix, iy;
	int count = 0;

	for (auto itr = base_hash_map_v.begin(); itr != base_hash_map_v.end(); itr++) {
		printf("\r basetrack trans %d/%d(%.1lf%%)", count, track.size(), count*100. / track.size());
		count += itr->second.size();

		std::pair<int, int> id;
		ix = (itr)->first.first / hash_size;
		iy = (itr)->first.second / hash_size;
		std::vector<corrmap0::Corrmap> corr_list;
		int loop = 0;
		while (corr_list.size() <= 10) {
			loop++;
			for (int iix = -1 * loop; iix <= 1 * loop; iix++) {
				for (int iiy = -1 * loop; iiy <= 1 * loop; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (corr_map.count(id) == 0)continue;
					auto range = corr_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						corr_list.push_back(res->second);
					}
				}
			}
		}
		for (auto itr_b = itr->second.begin(); itr_b != itr->second.end(); itr_b++) {

			double dist;
			corrmap0::Corrmap param;
			for (auto itr2 = corr_list.begin(); itr2 != corr_list.end(); itr2++) {
				double cx, cy;
				cx = (itr2->areax[0] + itr2->areax[1]) / 2;
				cy = (itr2->areay[0] + itr2->areay[1]) / 2;
				if (itr2 == corr_list.begin()) {
					dist = ((*itr_b)->x - cx)*((*itr_b)->x - cx) + ((*itr_b)->y - cy)*((*itr_b)->y - cy);
					param = *itr2;
				}
				if (dist > ((*itr_b)->x - cx)*((*itr_b)->x - cx) + ((*itr_b)->y - cy)*((*itr_b)->y - cy)) {
					dist = ((*itr_b)->x - cx)*((*itr_b)->x - cx) + ((*itr_b)->y - cy)*((*itr_b)->y - cy);
					param = *itr2;
				}

			}
			//printf("dist=%.1lf\n", sqrt(dist));
			track_tans_affin(*itr_b, param);

		}



	}



	printf("\r basetrack trans %d/%d(%.1lf%%)\n", count, track.size(), count*100. / track.size());

}
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size) {

	//hash_sizeはcorrmapのサイズより大きくする
	double cx, cy;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		double area[4], factor;
		area[0] = itr->areax[0] - itr->position[4];
		area[1] = itr->areax[1] - itr->position[4];
		area[2] = itr->areay[0] - itr->position[5];
		area[3] = itr->areay[1] - itr->position[5];

		factor = 1 / (itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);
		itr->areax[0] = factor * (area[0] * itr->position[3] - area[2] * itr->position[1]);
		itr->areay[0] = factor * (area[2] * itr->position[0] - area[0] * itr->position[2]);
		itr->areax[1] = factor * (area[1] * itr->position[3] - area[3] * itr->position[1]);
		itr->areay[1] = factor * (area[3] * itr->position[0] - area[1] * itr->position[2]);

		cx = (itr->areax[0] + itr->areax[1]) / 2;
		cy = (itr->areay[0] + itr->areay[1]) / 2;

		std::pair<int, int>id;
		id.first = cx / hash_size;
		id.second = cy / hash_size;
		corr_map.insert(std::make_pair(id, *itr));
	}

}
void track_tans_affin(vxx::base_track_t  *track, corrmap0::Corrmap corr) {
	double x_tmp, y_tmp;
	x_tmp = track->x;
	y_tmp = track->y;
	track->x = x_tmp * corr.position[0] + y_tmp * corr.position[1] + corr.position[4];
	track->y = x_tmp * corr.position[2] + y_tmp * corr.position[3] + corr.position[5];

	//x_tmp = atan(track->ax);
	//y_tmp = atan(track->ay);
	x_tmp = track->ax;
	y_tmp = track->ay;
	track->ax = x_tmp * corr.angle[0] + y_tmp * corr.angle[1] + corr.angle[4];
	track->ay = x_tmp * corr.angle[2] + y_tmp * corr.angle[3] + corr.angle[5];
	//track->ax = tan(track->ax);
	//track->ay = tan(track->ay);

	track->z = corr.dz;
}

//上流-->下流への外挿
void base_extra(std::vector<vxx::base_track_t> &track, double nominal_gap) {
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		itr->x = itr->ax*-1*(nominal_gap + itr->z) + itr->x;
		itr->y = itr->ay*-1*(nominal_gap + itr->z) + itr->y;
	}
}

void base_output_txt(std::vector<vxx::base_track_t> &track, std::set<int > rawid0, std::set<int > rawid1) {
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		if (rawid0.count(itr->m[0].rawid)+ rawid1.count(itr->m[1].rawid) !=2)continue;
		printf("%d %d %d %d %.4lf %.4lf %.1lf %.1lf %.1lf\n", itr->pl, itr->m[0].rawid,itr->m[1].rawid,itr->m[0].ph+itr->m[1].ph, itr->ax, itr->ay, itr->x, itr->y, itr->z);

	}
}

std::vector<vxx::base_track_t> base_sel(std::vector<vxx::base_track_t> &track, std::set<int > rawid0, std::set<int > rawid1) {
	std::vector<vxx::base_track_t> ret;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		if (rawid0.count(itr->m[0].rawid) + rawid1.count(itr->m[1].rawid) != 2)continue;
		ret.push_back(*itr);
		//printf("%d %d %d %d %.4lf %.4lf %.1lf %.1lf %.1lf\n", itr->pl, itr->m[0].rawid, itr->m[1].rawid, itr->m[0].ph + itr->m[1].ph, itr->ax, itr->ay, itr->x, itr->y, itr->z);

	}
	return ret;
}
