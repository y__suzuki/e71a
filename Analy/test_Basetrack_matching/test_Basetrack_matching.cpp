#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>
std::vector<netscan::base_track_t> base_matching(std::vector<netscan::base_track_t>&base_ori, std::vector<netscan::base_track_t>& base_all);
netscan::base_track_t select_best_base(netscan::base_track_t base, std::vector<netscan::base_track_t>base_all);
void wrtie_pos_raw_list(std::string filename, std::vector<netscan::base_track_t> base);
void wrtie_matched_list(std::string filename, std::vector<netscan::base_track_t> base);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-bvxx in-bvxx_all pl zone out-file-list\n");
		exit(1);
	}
	std::string file_in_base_ori = argv[1];
	std::string file_in_base_all = argv[2];
	int pl = std::stoi(argv[3]);
	int zone = std::stoi(argv[4]);
	std::string file_out_list = argv[5];

	std::vector<netscan::base_track_t> base_ori;
	std::vector<netscan::base_track_t> base_all;
	netscan::read_basetrack_extension(file_in_base_ori, base_ori, pl, zone);
	netscan::read_basetrack_extension(file_in_base_all, base_all, pl, zone);

	std::vector<netscan::base_track_t> base_sel = base_matching(base_ori, base_all);
	//netscan::write_basetrack_vxx(file_out_base_sel, base_sel, pl, zone);
	//wrtie_pos_raw_list(file_out_list, base_sel);
	wrtie_matched_list(file_out_list, base_sel);
}
std::vector<netscan::base_track_t> base_matching(std::vector<netscan::base_track_t>&base_ori, std::vector<netscan::base_track_t>& base_all) {
	double x_min, y_min;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		if (itr == base_all.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(x_min, itr->x);
		y_min = std::min(y_min, itr->y);
	}
	double hash = 1000;
	std::multimap < std::pair<int, int>, netscan::base_track_t* > base_map;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		std::pair<int, int>id;
		id.first = (itr->x - x_min) / hash;
		id.second = (itr->y - y_min) / hash;
		base_map.insert(std::make_pair(id, &(*itr)));
	}
	std::vector<netscan::base_track_t> ret;
	for (int i = 0; i < base_ori.size(); i++) {
		std::vector<netscan::base_track_t> match_cand;
		double angle, all_pos[2], all_ang[2];
		angle = sqrt(base_ori[i].ax*base_ori[i].ax + base_ori[i].ay*base_ori[i].ay);
		all_pos[0] = 5 + angle *5;
		all_pos[1] = 5;
		all_ang[0] = 0.025 + angle * 0.025;
		all_ang[1] = 0.025;
		double factor = 50;

		all_pos[0] = all_pos[0] * factor;
		all_pos[1] = all_pos[1] * factor;
		all_ang[0] = all_ang[0] * factor;
		all_ang[1] = all_ang[1] * factor;

		int range[2][2];
		range[0][0] = (base_ori[i].x - x_min - all_pos[0]) / hash;
		range[0][1] = (base_ori[i].x - x_min + all_pos[0]) / hash;
		range[1][0] = (base_ori[i].y - y_min - all_pos[0]) / hash;
		range[1][1] = (base_ori[i].y - y_min + all_pos[0]) / hash;
		std::pair<int, int>id;
		double diff_pos[2], diff_ang[2];
		for (int ix = range[0][0]; ix <= range[0][1]; ix++) {
			for (int iy = range[1][0]; iy <= range[1][1]; iy++) {
				id.first = ix;
				id.second = iy;

				if (base_map.count(id) == 0)continue;
				else if (base_map.count(id) == 1) {
					auto res = base_map.find(id);
					if (angle > 0.1) {
						diff_pos[0] = ((res->second->x - base_ori[i].x)*base_ori[i].ax + (res->second->y - base_ori[i].y)*base_ori[i].ay) / angle;
						diff_pos[1] = ((res->second->x - base_ori[i].x)*base_ori[i].ay - (res->second->y - base_ori[i].y)*base_ori[i].ax) / angle;
						diff_ang[0] = ((res->second->ax - base_ori[i].ax)*base_ori[i].ax + (res->second->ay - base_ori[i].ay)*base_ori[i].ay) / angle;
						diff_ang[1] = ((res->second->ax - base_ori[i].ax)*base_ori[i].ay - (res->second->ay - base_ori[i].ay)*base_ori[i].ax) / angle;
						if (fabs(diff_ang[0]) > all_ang[0])continue;
						if (fabs(diff_ang[1]) > all_ang[1])continue;
						if (fabs(diff_pos[0]) > all_pos[0])continue;
						if (fabs(diff_pos[1]) > all_pos[1])continue;
						match_cand.push_back(*res->second);
					}
					else {
						diff_pos[0] = res->second->x - base_ori[i].x;
						diff_pos[1] = res->second->y - base_ori[i].y;
						diff_ang[0] = res->second->ax - base_ori[i].ax;
						diff_ang[1] = res->second->ay - base_ori[i].ay;
						if (fabs(diff_ang[0]) > all_ang[1])continue;
						if (fabs(diff_ang[1]) > all_ang[1])continue;
						if (fabs(diff_pos[0]) > all_pos[1])continue;
						if (fabs(diff_pos[1]) > all_pos[1])continue;
						match_cand.push_back(*res->second);
					}
				}
				else {
					auto range = base_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						if (angle > 0.1) {
							diff_pos[0] = ((res->second->x - base_ori[i].x)*base_ori[i].ax + (res->second->y - base_ori[i].y)*base_ori[i].ay) / angle;
							diff_pos[1] = ((res->second->x - base_ori[i].x)*base_ori[i].ay - (res->second->y - base_ori[i].y)*base_ori[i].ax) / angle;
							diff_ang[0] = ((res->second->ax - base_ori[i].ax)*base_ori[i].ax + (res->second->ay - base_ori[i].ay)*base_ori[i].ay) / angle;
							diff_ang[1] = ((res->second->ax - base_ori[i].ax)*base_ori[i].ay - (res->second->ay - base_ori[i].ay)*base_ori[i].ax) / angle;
							if (fabs(diff_ang[0]) > all_ang[0])continue;
							if (fabs(diff_ang[1]) > all_ang[1])continue;
							if (fabs(diff_pos[0]) > all_pos[0])continue;
							if (fabs(diff_pos[1]) > all_pos[1])continue;
							match_cand.push_back(*res->second);
						}
						else {
							diff_pos[0] = res->second->x - base_ori[i].x;
							diff_pos[1] = res->second->y - base_ori[i].y;
							diff_ang[0] = res->second->ax - base_ori[i].ax;
							diff_ang[1] = res->second->ay - base_ori[i].ay;
							if (fabs(diff_ang[0]) > all_ang[1])continue;
							if (fabs(diff_ang[1]) > all_ang[1])continue;
							if (fabs(diff_pos[0]) > all_pos[1])continue;
							if (fabs(diff_pos[1]) > all_pos[1])continue;
							match_cand.push_back(*res->second);
						}
					}
				}
			}
		}
		if (match_cand.size() == 0)continue;
		else {
			ret.push_back(select_best_base(base_ori[i], match_cand));
		}
	}

	return ret;
}
netscan::base_track_t select_best_base(netscan::base_track_t base, std::vector<netscan::base_track_t>base_all) {
	netscan::base_track_t ret;
	double distance = -1,dist_tmp;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		dist_tmp = sqrt(pow(itr->x - base.x, 2) + pow(itr->y - base.y, 2));
		if (distance<0 || distance>dist_tmp) {
			distance = dist_tmp;
			ret = *itr;
		}
	}
	//�����o��
	/*
	std::ofstream ofs("base_diff.txt", std::ios::app);
	ofs << std::right << std::fixed << std::setfill(' ')
		<< std::setw(12) << std::setprecision(0) << base.rawid << " "
		<< std::setw(12) << std::setprecision(0) << ret.rawid << " "
		<< std::setw(7) << std::setprecision(4) << base.ax << " "
		<< std::setw(7) << std::setprecision(4) << base.ay << " "
		<< std::setw(10) << std::setprecision(1) << base.x << " "
		<< std::setw(10) << std::setprecision(1) << base.y << " "
		<< std::setw(7) << std::setprecision(4) << ret.ax << " "
		<< std::setw(7) << std::setprecision(4) << ret.ay << " "
		<< std::setw(10) << std::setprecision(1) << ret.x << " "
		<< std::setw(10) << std::setprecision(1) << ret.y << std::endl;
	*/
	return ret;
}
void wrtie_pos_raw_list(std::string filename, std::vector<netscan::base_track_t> base) {

	std::ofstream ofs(filename);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ofs << std::right << std::fixed << std::setfill(' ')
			<< std::setw(4) << std::setprecision(0) << itr->m[0].pos << " "
			<< std::setw(12) << std::setprecision(0) << itr->m[0].rawid << std::endl
			<< std::setw(4) << std::setprecision(0) << itr->m[1].pos << " "
			<< std::setw(12) << std::setprecision(0) << itr->m[1].rawid << std::endl;

	}
}
void wrtie_matched_list(std::string filename, std::vector<netscan::base_track_t> base) {

	std::ofstream ofs(filename);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ofs << std::right << std::fixed << std::setfill(' ')
			<< std::setw(12) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(7) << std::setprecision(0) << itr->m[0].ph+itr->m[1].ph << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(4) << std::setprecision(0) << itr->m[0].pos << " "
			<< std::setw(12) << std::setprecision(0) << itr->m[0].rawid << " "
			<< std::setw(4) << std::setprecision(0) << itr->m[1].pos << " "
			<< std::setw(12) << std::setprecision(0) << itr->m[1].rawid << std::endl;

	}
}