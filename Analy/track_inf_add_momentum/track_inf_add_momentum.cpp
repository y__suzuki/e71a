#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"PID_correct.lib")
#include <PID_correct.h>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <iostream>
#include <set>
#include <omp.h>

class mom_inf {
public:
	uint64_t chainid;
	int nseg;
	double pb, ax, ay, angle, vph_ave,mip,hip,pid;
};

std::vector<mom_inf> read_moentum_inf(std::string filename, bool output=false);
void add_inf(std::vector<PID_track>&t, std::vector<mom_inf> &m);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg filename pb-file file-out\n");
		exit(1);
	}

	std::string file_in_inf = argv[1];
	std::string file_in_mom = argv[2];
	std::string file_out = argv[3];

	std::vector<PID_track> t;
	PID_track::read_pid_track(file_in_inf, t);

	std::vector<mom_inf> m = read_moentum_inf(file_in_mom,true);
	add_inf(t, m);

	std::stringstream  filename_txt;
	filename_txt << file_out <<".txt";
	PID_track::wrtie_pid_track(file_out,  t);
	//PID_track::wrtie_pid_track_txt(filename_txt.str(), t);


}
std::vector<mom_inf> read_moentum_inf(std::string filename,bool output) {
	std::vector<mom_inf> ret;
	std::ifstream ifs(filename);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (output) {
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
	}

	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	mom_inf m;
	int cnt = 0;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);

		m.chainid= stoull(str_v[0]);
		m.nseg = stoi(str_v[1]);
		m.pb = stod(str_v[2]);
		m.ax = stod(str_v[3]);
		m.ay = stod(str_v[4]);
		m.vph_ave = stod(str_v[5]);
		m.mip = stod(str_v[6]);
		m.hip = stod(str_v[7]);
		m.pid = stod(str_v[8]);

		m.angle = sqrt(m.ax*m.ax + m.ay*m.ay);

		ret.push_back(m);
		if (output) {
			if (cnt % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
		}
		cnt++;
	}

	if (output ) {
		auto size1 = eofpos - begpos;
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	}
	if (cnt == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;

}
void add_inf(std::vector<PID_track>&t, std::vector<mom_inf> &m) {
	std::map<uint64_t, mom_inf>mom_map;
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		mom_map.insert(std::make_pair(itr->chainid, *itr));
	}

	for (auto itr = t.begin(); itr != t.end(); itr++) {
		if (mom_map.count(itr->chainid) == 0)continue;
		auto res = mom_map.at(itr->chainid);

		itr->pb = res.pb;
		itr->pid = res.pid;
	}
	return;
}