//thick: basetrackからmicrotrackをIDで探索
//thin : microtrackのまま読む
//microtrackの角度再測定(iteration)
//読み込みエリアを分割-->省メモリ


#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <VxxReader.h>
#include <picojson.h>
#include <omp.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <cassert>  // std::assert
#include <sstream>
#include <iomanip>
#include <string>
#include <numeric>

class EachImager_Param {
public:
	//CamareaID * 5 + SensorID = ImagerID
	//Width,Height 縦横のpixel数
	double Aff_coef[6], Aff_coef_offset[6], DZ;
	int CameraID, GridX, GridY, Height, ImagerID, SensorID, Width;
	std::string LastReportFilePath;
};
class EachShot_Param {
public:
	int View, Imager, StartAnalysisPicNo, GridX, GridY;
	double Z_begin, X_center, Y_center;
	std::string TrackFilePath;
	//GridX,Y intでダイジョブ?
};
class EachView_Param {
public:
	int  LayerID, NPicThickOfLayer;
	double Stage_x, Stage_y, ThickOfLayer, Z_begin, Z_end;
};
struct microtrack_center {
	//中心(32層での14層、16層での7層)でのx,yのpixel座標
	//ax,ay 15層で何pixel　シフトしたか
	//tan ax = ax*0.45 [um]/thick of 15 layer[um]
	double px_center, py_center, ax, ay;
	int ViewID, CameraID, SensorID, pixelnum, hitnum, ph2;
	vxx::micro_track_t *m;
};
bool sort_id_layer(const  microtrack_center &left, const  microtrack_center &right) {
	if (left.CameraID != right.CameraID) {
		return left.CameraID < right.CameraID;
	}
	else if (left.SensorID != right.SensorID) {
		return left.SensorID < right.SensorID;
	}
	else {
		return left.ViewID < right.ViewID;
	}
}
bool sort_pair(const std::pair<char, char> &left, const std::pair<char, char> &right) {
	if (left.second == right.second) {
		return left.first < right.first;
	}
	return left.second < right.second;

}
int use_thread(double ratio, bool output = true);

std::map<int, EachImager_Param> read_EachImager(std::string filename);
std::vector<EachShot_Param> read_EachShot(std::string filename);
std::map<int, EachView_Param> read_EachView(std::string filename);
std::vector<EachShot_Param> EachShot_center(std::vector<EachShot_Param>shot, std::map<int, EachImager_Param>&imager, std::map<int, EachView_Param>&view, int LayerID);
std::multimap<std::pair<int, int>, EachShot_Param>EachShot_hash(std::vector<EachShot_Param>&shot, double x_width, double y_width, double &x_min, double &x_max, double &y_min, double &y_max);

void read_all_microtrack(std::string file_input_path, int pos, int LayerID, std::vector<vxx::micro_track_t> micro[4], std::vector<vxx::CutArea> &area, std::set<std::tuple<int, int, int>> read_list[4]);
std::vector<vxx::micro_track_t> read_match_microtrack(int pos, std::string bvxxfile0, std::string bvxxfile1, std::string fvxxname, std::vector<vxx::CutArea> &area, std::set<std::tuple<int, int, int>> &read_list);
std::vector<vxx::micro_track_t> read_microtrack(int pos, std::string fvxxname, std::vector<vxx::CutArea> &area, std::set<std::tuple<int, int, int>> &read_list);

void microtrack_pixel_convert(vxx::micro_track_t &m, std::vector<microtrack_center> &m_layer, std::multimap<std::pair<int, int>, EachShot_Param> &shot, std::map<int, EachImager_Param>&imager, std::map<int, EachView_Param>&view, int LayerID, double x_width, double y_width, double x_min, double y_min);
std::pair<bool, microtrack_center> microtrack_transformation(EachImager_Param imager, EachView_Param view, vxx::micro_track_t &m);
vxx::micro_track_t microtrack_transformation_invert(EachImager_Param imager, EachView_Param view, microtrack_center &m, double dz);
void microtrack_divide_image(std::vector<microtrack_center> &m_center, std::map<std::tuple<int, int, int>, std::vector<microtrack_center>> &m_map, std::set<std::tuple<int, int, int>> &input_image_list);

void ReCalc_microtrack_inf1(std::string file_beta_path, std::tuple<int, int, int> input_image, std::vector<microtrack_center> m[4], int LayerID, int mode);
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin);
std::vector<std::pair<char, char>> make_pixels(double pax, double pay, cv::Mat& buffer, int dc);
bool measure_angle2(std::vector<int> hit_x, std::vector<int> hit_y, std::vector<double> hit_z, double &ax, double &ay, double &x_center, double &y_center, double z_center);

std::vector<vxx::micro_track_t> edge_noise_cut(std::vector<vxx::micro_track_t> &micro, int area, double range[4], double cut);
void get_fvxx_area(std::vector<vxx::micro_track_t> &micro, double range[4]);

void fvxx_merge_filter(std::string output, std::string output_tmp, std::vector<std::string> input, int pos);
void Print_track_image(std::string file_beta_path, std::tuple<int, int, int> input_image, std::vector<microtrack_center> m, int LayerID);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg fvxx_path pos beta_path image_path output-path\n");
		exit(1);
	}

	//muon 33631280
	//prton 33676569

	//コマンドライン引数の読み込み
	std::string file_input_fvxx = argv[1];
	int pos = std::stoi(argv[2]);
	std::string file_in_beta_path = argv[3];
	std::string file_in_image_path = argv[4];
	std::string file_out_image_path = argv[5];

	//pl,LayerIDの設定
	int pl = pos / 10;
	int LayerID;
	if (pos % 10 == 1) {
		LayerID = 1;
	}
	else if (pos % 10 == 2) {
		LayerID = 0;
	}
	else {
		fprintf(stderr, "Layer ID exception\n");
		fprintf(stderr, "pos:xx2 -->LayerID = 0\n");
		fprintf(stderr, "pos:xx1 -->LayerID = 1\n");
		exit(1);
	}

	//EachImager:1視野内での各sensorの値 センサー数(72)個
	//EachShot:全視野での各sensorの値 視野数*センサー数(72)個
	//EachView:各視野の値 視野数
	std::string file_in_Beta_EachImagerParam = file_in_beta_path + "\\Beta_EachImagerParam.json";
	std::string file_in_Beta_EachShotParam = file_in_beta_path + "\\Beta_EachShotParam.json";
	std::string file_in_Beta_EachViewParam = file_in_beta_path + "\\Beta_EachViewParam.json";

	std::map<int, EachImager_Param> imager_map = read_EachImager(file_in_Beta_EachImagerParam);
	std::vector<EachShot_Param> shot_vec = read_EachShot(file_in_Beta_EachShotParam);
	std::map<int, EachView_Param> view_map = read_EachView(file_in_Beta_EachViewParam);

	//shot jsonのhash化
	shot_vec = EachShot_center(shot_vec, imager_map, view_map, LayerID);
	double x_min, y_min, x_max, y_max;
	//sonsorの大きさ,mm
	//大きくすると計算量が増える
	//小さくすると、探索しないAreaが増える
	//領域間の間隔より大きい量
	double x_width = 2048 * 0.45 / 1000;
	double y_width = 1088 * 0.45 / 1000;
	std::multimap<std::pair<int, int>, EachShot_Param>shot_hash = EachShot_hash(shot_vec, x_width, y_width, x_min, x_max, y_min, y_max);

	//microtrack読み込み
	std::array<int, 2> index = { 33631280, 33676570 };//1234<=rawid<=5678であるようなものだけを読む。
	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> micro = fr.ReadAll(file_input_fvxx, pos, 0, vxx::opt::index = index);
	vxx::micro_track_t m[2];
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		if (itr->rawid == 33631280) {
			m[0]=(*itr);
		}
		if (itr->rawid == 33676569) {
			m[1]=(*itr);
		}
	}
	std::vector<microtrack_center> m_center;
	microtrack_pixel_convert(m[0], m_center, shot_hash, imager_map, view_map, LayerID, x_width, y_width, x_min, y_min);
	microtrack_pixel_convert(m[1], m_center, shot_hash, imager_map, view_map, LayerID, x_width, y_width, x_min, y_min);

	//microtrackがどの画像にあるか分けて再保存
	std::map<std::tuple<int, int, int>, std::vector<microtrack_center>> m_map;
	std::set<std::tuple<int, int, int>> input_image_list;
	fprintf(stderr, "\nsort begin :");
	sort(m_center.begin(), m_center.end(), sort_id_layer);
	microtrack_divide_image(m_center, m_map, input_image_list);
	fprintf(stderr, ": sort end\n");


	//測定
	//pixel count & angle measure
	int image_num = 0;
	auto image_begin = input_image_list.begin();
	for (int i = 0; i < input_image_list.size(); i++) {
		auto image_list = std::next(image_begin, i);
		image_num++;
		fprintf(stderr, "\r input image %d/%d(%4.1lf%%)", image_num, input_image_list.size(), image_num*100. / input_image_list.size());
		std::vector<microtrack_center > m_vec;
		m_vec = m_map[*image_list];
		Print_track_image(file_in_beta_path, *image_list, m_vec, LayerID);
	}
	fprintf(stderr, "\r input image %d/%d(%4.1lf%%)\n", image_num, input_image_list.size(), image_num*100. / input_image_list.size());




}

std::map<int, EachImager_Param> read_EachImager(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	std::map<int, EachImager_Param> ret;
	//mapのkey=imagerID
	int ImagerID = 0;
	picojson::array &all = v.get<picojson::array>();
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachImager_Param param;
		picojson::array &Aff_coef = obj["Aff_coef"].get<picojson::array>();
		picojson::array &Aff_coef_offset = obj["Aff_coef_offset"].get<picojson::array>();
		int i = 0;
		for (int i = 0; i < 6; i++) {
			param.Aff_coef[i] = Aff_coef[i].get<double>();
			param.Aff_coef_offset[i] = Aff_coef_offset[i].get<double>();
		}
		param.DZ = obj["DZ"].get<double>();
		param.CameraID = (int)obj["CameraID"].get<double>();
		param.GridX = (int)obj["GridX"].get<double>();
		param.GridY = (int)obj["GridY"].get<double>();
		param.Height = (int)obj["Height"].get<double>();
		param.ImagerID = (int)obj["ImagerID"].get<double>();
		param.SensorID = (int)obj["SensorID"].get<double>();
		param.Width = (int)obj["Width"].get<double>();
		param.LastReportFilePath = obj["LastReportFilePath"].get<std::string>();
		ret.insert(std::make_pair(ImagerID, param));
		ImagerID++;

		//printf("imagerID %d factor %.10lf\n", param.ImagerID, param.Aff_coef[0] * param.Aff_coef[3] - param.Aff_coef[1] * param.Aff_coef[2]);
	}
	printf("number of imager = %d\n", ImagerID);
	return ret;
}
std::vector<EachShot_Param> read_EachShot(std::string filename) {
	//JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}

	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	//位置-->shot paramに行けるようにしたい-->全beta.json読み込んだ後でやる
	std::vector<EachShot_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachShot_Param param;
		param.Z_begin = obj["Z_begin"].get<double>();
		param.View = (int)obj["View"].get<double>();
		param.Imager = (int)obj["Imager"].get<double>();
		param.StartAnalysisPicNo = (int)obj["StartAnalysisPicNo"].get<double>();
		param.GridX = (int)obj["GridX"].get<double>();
		param.GridY = (int)obj["GridY"].get<double>();
		param.TrackFilePath = obj["TrackFilePath"].get<std::string>();
		param.X_center = -1;
		param.Y_center = -1;
		ret.push_back(param);
	}
	printf("number of shot = %zd\n", ret.size());

	return ret;
}
std::map<int, EachView_Param> read_EachView(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}
	std::map<int, EachView_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	int ViewID = 0;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachView_Param param;
		param.Stage_x = obj["Stage_x"].get<double>();
		param.Stage_y = obj["Stage_y"].get<double>();
		param.ThickOfLayer = obj["ThickOfLayer"].get<double>();
		param.Z_begin = obj["Z_begin"].get<double>();
		param.Z_end = obj["Z_end"].get<double>();
		param.LayerID = (int)obj["LayerID"].get<double>();
		param.NPicThickOfLayer = (int)obj["NPicThickOfLayer"].get<double>();
		ret.insert(std::make_pair(ViewID, param));
		ViewID++;
	}
	printf("number of view = %d\n", ViewID);
	return ret;
}
std::vector<EachShot_Param> EachShot_center(std::vector<EachShot_Param>shot, std::map<int, EachImager_Param>&imager, std::map<int, EachView_Param>&view, int LayerID) {
	std::vector<EachShot_Param> ret;
	for (auto itr = shot.begin(); itr != shot.end(); itr++) {
		if (view[itr->View].LayerID != LayerID)continue;
		//範囲外アクセス例外処理したほうが良いかも
		//Aff_coef_offsetも?
		itr->X_center = view[itr->View].Stage_x + imager[itr->Imager].Aff_coef[4] + imager[itr->Imager].Aff_coef_offset[4];
		itr->Y_center = view[itr->View].Stage_y + imager[itr->Imager].Aff_coef[5] + imager[itr->Imager].Aff_coef_offset[5];
		ret.push_back(*itr);
	}
	return ret;
}
std::multimap<std::pair<int, int>, EachShot_Param>EachShot_hash(std::vector<EachShot_Param>&shot, double x_width, double y_width, double &x_min, double &x_max, double &y_min, double &y_max) {
	std::multimap<std::pair<int, int>, EachShot_Param>ret;
	for (auto itr = shot.begin(); itr != shot.end(); itr++) {
		if (itr == shot.begin()) {
			x_min = itr->X_center;
			x_max = itr->X_center;
			y_min = itr->Y_center;
			y_max = itr->Y_center;
		}
		x_min = std::min(itr->X_center, x_min);
		x_max = std::max(itr->X_center, x_max);
		y_min = std::min(itr->Y_center, y_min);
		y_max = std::max(itr->Y_center, y_max);
	}
	std::pair<int, int> id;
	int ix, iy;
	for (auto itr = shot.begin(); itr != shot.end(); itr++) {
		ix = int((itr->X_center - x_min) / x_width + 0.5);
		iy = int((itr->Y_center - y_min) / y_width + 0.5);
		//printf("%d %d %lf %lf\n", ix, iy, itr->X_center, itr->Y_center);
		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				id.first = ix + iix;
				id.second = iy + iiy;
				ret.insert(std::make_pair(id, *itr));
			}
		}
	}
	return ret;
}

void microtrack_pixel_convert(vxx::micro_track_t &m, std::vector<microtrack_center> &m_center, std::multimap<std::pair<int, int>, EachShot_Param> &shot, std::map<int, EachImager_Param>&imager, std::map<int, EachView_Param>&view, int LayerID, double x_width, double y_width, double x_min, double y_min) {
	//microtrack-->各層でのpixel座標に
	double min_dis = 10000;
	double min_shot = -1;
	double dis;
	double x, y;
	std::pair<int, int> id;
	EachShot_Param shot_param;
	//um-->mmへ変換
	x = m.x / 1000;
	y = m.y / 1000;
	id.first = (x - x_min) / x_width;
	id.second = (y - y_min) / y_width;
	//各視野をloop中心の最も近い視野を探す
	//ここのloopはうまくhashとかすれば短縮できそう-->適当にhashした
	if (shot.count(id) == 0) {
		fprintf(stderr, "Not seach view");
		exit(1);
	}
	else if (shot.count(id) == 1) {
		auto res = shot.find(id);
		dis = sqrt((res->second.X_center - x)*(res->second.X_center - x) + (res->second.Y_center - y)*(res->second.Y_center - y));
		if (min_dis > dis) {
			shot_param = res->second;
			min_dis = dis;
		}
	}
	else {
		auto range = shot.equal_range(id);
		for (auto res = range.first; res != range.second; res++) {
			dis = sqrt((res->second.X_center - x)*(res->second.X_center - x) + (res->second.Y_center - y)*(res->second.Y_center - y));
			if (min_dis > dis) {
				shot_param = res->second;
				min_dis = dis;
			}
		}
	}

	if (min_dis > 1) {
		fprintf(stderr, "Not seach view");
		printf("distance = %lf\n", min_dis);
		exit(1);
	}
	//各IDの取得
	int CameraID, SensorID, ViewID;
	CameraID = imager[shot_param.Imager].CameraID;
	SensorID = imager[shot_param.Imager].SensorID;
	ViewID = shot_param.View;
	if (view[shot_param.View].LayerID != LayerID) {
		fprintf(stderr, "Layer ID mismatch\n");
		exit(1);
	}
	//microtrack の変換
	//基準面-->
	auto res = microtrack_transformation(imager[CameraID * 12 + SensorID], view[ViewID], m);
	if (res.first) {
		res.second.CameraID = CameraID;
		res.second.SensorID = SensorID;
		res.second.ViewID = ViewID;
#pragma omp critical
		{
			m_center.push_back(res.second);
		}
	}
}
std::pair<bool, microtrack_center> microtrack_transformation(EachImager_Param imager, EachView_Param view, vxx::micro_track_t &m) {
	//stage-->pixel 座標へ
	//角度は回転のみ
	double px, py, pax, pay, x_tmp, y_tmp;
	//1層の厚み*層数*角度/(pixel length)+dz補正
	x_tmp = m.x;
	y_tmp = m.y;

	//um --> mm & shift成分の計算
	x_tmp = x_tmp / 1000 - imager.Aff_coef[4] - imager.Aff_coef_offset[4] - view.Stage_x;
	y_tmp = y_tmp / 1000 - imager.Aff_coef[5] - imager.Aff_coef_offset[5] - view.Stage_y;
	double factor = 1.0 / (imager.Aff_coef[0] * imager.Aff_coef[3] - imager.Aff_coef[1] * imager.Aff_coef[2]);
	px = factor * (imager.Aff_coef[3] * x_tmp - imager.Aff_coef[1] * y_tmp);
	py = factor * (imager.Aff_coef[0] * y_tmp - imager.Aff_coef[2] * x_tmp);
	px = px + 2048 / 2;
	py = py + 1088 / 2;
	//角度の変換 回転のみ

	//m.ax-->1umで何umシフトするか。
		//15layer毎のシフトピクセル量
	//60=15layerの厚み

	x_tmp = m.ax * 60 / 1000;
	y_tmp = m.ay * 60 / 1000;
	//fabsとっていい?-->signの適用
	pax = factor * (imager.Aff_coef[3] * x_tmp - imager.Aff_coef[1] * y_tmp);
	pay = factor * (imager.Aff_coef[0] * y_tmp - imager.Aff_coef[2] * x_tmp);

	microtrack_center ret;
	ret.CameraID = 0;
	ret.m = &m;
	ret.ax = -1 * pax;
	ret.ay = -1 * pay;
	ret.px_center = px;
	ret.py_center = py;
	ret.SensorID = 0;
	ret.ViewID = 0;
	ret.pixelnum = 0;
	ret.hitnum = 0;
	return std::make_pair(true, ret);
}
vxx::micro_track_t microtrack_transformation_invert(EachImager_Param imager, EachView_Param view, microtrack_center &m, double dz) {
	//pixel-->fvxx座標
	vxx::micro_track_t ret = *(m.m);
	//ph2,vph2をfvxx px,pyに埋め込む
	//pxの
	ret.px = m.ph2 + m.pixelnum * 100;
	ret.py = m.hitnum;

	double ax, ay, x, y;
	//15layer毎のシフトピクセル量
	//60=15layerの厚み-->fvxxのaxに

	//pixel-->stage 座標へ
	//角度は回転のみ
	double px, py, pax, pay, x_tmp, y_tmp;
	//1層の厚み*層数*角度/(pixel length)+dz補正
	x_tmp = m.px_center - 2048 / 2;
	y_tmp = m.py_center - 1088 / 2;
	ret.x = x_tmp * imager.Aff_coef[0] + y_tmp * imager.Aff_coef[1] + imager.Aff_coef[4] + imager.Aff_coef_offset[4] + view.Stage_x;
	ret.y = x_tmp * imager.Aff_coef[2] + y_tmp * imager.Aff_coef[3] + imager.Aff_coef[5] + imager.Aff_coef_offset[5] + view.Stage_y;
	ret.x = ret.x * 1000;
	ret.y = ret.y * 1000;

	//60umで何mmシフトするか
	pax = (imager.Aff_coef[0] * m.ax + imager.Aff_coef[1] * m.ay);
	pay = (imager.Aff_coef[2] * m.ax + imager.Aff_coef[3] * m.ay);

	ret.ax = -1 * pax * 1000 / 60;
	ret.ay = -1 * pay * 1000 / 60;

	return ret;
}
void microtrack_divide_image(std::vector<microtrack_center> &m_center, std::map<std::tuple<int, int, int>, std::vector<microtrack_center>> &m_map, std::set<std::tuple<int, int, int>> &input_image_list) {
	std::tuple<int, int, int>imageID = std::make_tuple(-1, -1, -1);
	int Layer_num = -1;
	int64_t all = m_center.size();
	std::vector<microtrack_center> m_map_tmp;
	//microtrackを画像毎に分ける
	for (int64_t i = 0; i < all; i++) {
		if (i % 1000000 == 0) {
			fprintf(stderr, "\r microtrack divide image %d/%d(%4.1lf%%)", i, all, i*100. / all);
		}
		if (imageID != std::make_tuple(m_center[i].CameraID, m_center[i].SensorID, m_center[i].ViewID)) {
			if (m_map_tmp.size() > 0) {
				m_map.insert(std::make_pair(imageID, m_map_tmp));
			}
			m_map_tmp.clear();
			std::get<0>(imageID) = m_center[i].CameraID;
			std::get<1>(imageID) = m_center[i].SensorID;
			std::get<2>(imageID) = m_center[i].ViewID;
			input_image_list.insert(imageID);
		}
		m_map_tmp.push_back(m_center[i]);
	}
	if (m_map_tmp.size() > 0) {
		m_map.insert(std::make_pair(imageID, m_map_tmp));
	}
	m_map_tmp.clear();
	fprintf(stderr, "\r microtrack divide image %d/%d(%4.1lf%%)\n", all, all, all*100. / all);

}



void Print_track_image(std::string file_beta_path, std::tuple<int, int, int> input_image, std::vector<microtrack_center> m, int LayerID) {
	//画像入力
	std::vector<cv::Mat> vmat1;
	std::string input_image_bin;
	{
		std::stringstream ss;
		ss << file_beta_path << "\\DATA\\" << std::setw(2) << std::setfill('0') << std::get<0>(input_image)
			<< "_" << std::setw(2) << std::setfill('0') << std::get<1>(input_image)
			<< "\\TrackHit2_0_" << std::setw(8) << std::setfill('0') << std::get<2>(input_image)
			<< "_" << std::setw(1) << LayerID << "_000.spng";
		input_image_bin = ss.str();
	}

	std::vector<std::vector<uchar>> vvin;
	read_vbin(input_image_bin, vvin);
	for (int j = 0; j < vvin.size(); j++) {
		cv::Mat mat1 = cv::imdecode(vvin[j], 0);
		vmat1.emplace_back(mat1);
	}
	vvin.clear();

	//1trackずつpixel count
	//angle measure
	int factor = vmat1.size() / 16;
	int width = vmat1[0].cols;
	int height = vmat1[0].rows;

	double xmin, xmax, ymin, ymax;
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		if (itr == m.begin()) {
			xmin = itr->px_center;
			xmax = itr->px_center;
			ymin = itr->py_center;
			ymax = itr->py_center;
		}
		xmin = std::min(xmin, itr->px_center - fabs(itr->ax));
		xmax = std::max(xmax, itr->px_center + fabs(itr->ax));
		ymin = std::min(ymin, itr->py_center - fabs(itr->ay));
		ymax = std::max(ymax, itr->py_center + fabs(itr->ay));
	}
	int buf = 30;
	xmin = xmin - buf;
	xmax = xmax + buf;
	ymin = ymin - buf;
	ymax = ymax + buf;


	cv::Mat image_stack = cv::Mat::zeros(height, width, CV_8UC3);
	for (int i = 0; i < vmat1.size(); i++) {

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				if (vmat1[i].at<uchar>(y, x) == 0x00) {
					//image.at<cv::Vec3b>(y, x)[0] = 0x00; //青
					//image.at<cv::Vec3b>(y, x)[1] = 0x00; //緑
					//image.at<cv::Vec3b>(y, x)[2] = 0x00; //赤
				}
				else {
					image_stack.at<cv::Vec3b>(y, x)[0] = 0xff; //青
					image_stack.at<cv::Vec3b>(y, x)[1] = 0xff; //緑
					image_stack.at<cv::Vec3b>(y, x)[2] = 0xff; //赤
				}
			}
		}
	}



	std::vector<cv::Mat> vmat_color;
	for (int i = 0; i < vmat1.size(); i++) {
		cv::Mat image = cv::Mat::zeros(height, width, CV_8UC3);
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				if (vmat1[i].at<uchar>(y, x) == 0x00) {
					image.at<cv::Vec3b>(y, x)[0] = 0x00; //青
					image.at<cv::Vec3b>(y, x)[1] = 0x00; //緑
					image.at<cv::Vec3b>(y, x)[2] = 0x00; //赤
				}
				else {
					image.at<cv::Vec3b>(y, x)[0] = 0xff; //青
					image.at<cv::Vec3b>(y, x)[1] = 0xff; //緑
					image.at<cv::Vec3b>(y, x)[2] = 0xff; //赤
				}
			}
		}
		vmat_color.push_back(image);
	}

	cv::Mat buffer = cv::Mat::zeros(64, 64, CV_8U);
	double radial_length = 1;
	double dc = 2;
	for (auto& v : m) {

		const double pax = v.ax / 15;
		const double pay = v.ay / 15;

		auto pixels = make_pixels(pax *radial_length, pay * radial_length, buffer, dc);
		for (int i = 0; i < 16 * factor; i++) {
			std::set<std::pair<int, int>>trace_pixel;
			std::set<std::pair<int, int>>frame_pixel;
			//各画像での飛跡の中心座標[pixel]
			const int cpx = std::round(v.px_center + pax * (i - 7 * factor) * ((16. - 1) / (16 * factor - 1)));//四捨五入
			const int cpy = std::round(v.py_center + pay * (i - 7 * factor) * ((16. - 1) / (16 * factor - 1)));//四捨五入

			for (const auto& r : pixels) {
				const int x = cpx + r.first;
				const int y = cpy + r.second;
				if (x < 0)continue;
				if (y < 0)continue;
				if (x >= width)continue;
				if (y >= height)continue;
				trace_pixel.insert(std::make_pair(x, y));
				if (vmat1[i].data[y * width + x] == 0)continue;
			}
			//frame_pixelにデータを入れる
			for (auto itr2 = trace_pixel.begin(); itr2 != trace_pixel.end(); itr2++) {
				for (int ix = -1; ix <= 1; ix++) {
					for (int iy = -1; iy <= 1; iy++) {
						if (trace_pixel.count(std::make_pair(itr2->first + ix, itr2->second + iy)) == 0) {
							if (itr2->first + ix < 0)continue;
							if (itr2->second + iy < 0)continue;
							if (itr2->first + ix >= width)continue;
							if (itr2->second + iy >= height)continue;
							frame_pixel.insert(std::make_pair(itr2->first + ix, itr2->second + iy));
						}
					}
				}
			}
			//vmat_color[Layer_num]のframepixelを赤にする
			for (auto itr2 = frame_pixel.begin(); itr2 != frame_pixel.end(); itr2++) {
				vmat_color[i].at<cv::Vec3b>(itr2->second, itr2->first)[0] = 0xff; //青
				vmat_color[i].at<cv::Vec3b>(itr2->second, itr2->first)[1] = 0x00; //緑
				vmat_color[i].at<cv::Vec3b>(itr2->second, itr2->first)[2] = 0x00; //赤
			}
		}
	}

	//png出力
//飛跡毎、周囲のみ
	std::string out_filename;
	std::vector<cv::Mat> vmat_color_out;
	for (int k = 0; k < vmat_color.size(); k++) {
		cv::Rect rect = cv::Rect(xmin, ymin, xmax - xmin, ymax - ymin);
		cv::Mat out(vmat_color[k], rect);
		vmat_color_out.push_back(out);
	}
	for (int k = 0; k < vmat_color_out.size(); k++) {
		{
			std::stringstream ss;
			ss << "tmp_" << std::setfill('0') << std::setw(3) << k << ".png";
			out_filename = ss.str();
		}
		cv::imwrite(out_filename, vmat_color_out[k]);
	}
	cv::Rect rect = cv::Rect(xmin, ymin, xmax - xmin, ymax - ymin);
	cv::Mat out(image_stack, rect);
	cv::imwrite("tmp.png", out);

	
}









void ReCalc_microtrack_inf0(std::string file_beta_path, std::tuple<int, int, int> input_image, std::vector<microtrack_center> m[4], int LayerID, int mode) {
	if (mode != 1 && mode != 2) {
		fprintf(stderr, "mode exception\n");
		fprintf(stderr, "mode=1: calc for 16layer\n");
		fprintf(stderr, "mode=2: calc for 32layer\n");
	}
	//画像入力
	std::vector<cv::Mat> vmat1;
	std::string input_image_bin;
	{
		std::stringstream ss;
		ss << file_beta_path << "\\DATA\\" << std::setw(2) << std::setfill('0') << std::get<0>(input_image)
			<< "_" << std::setw(2) << std::setfill('0') << std::get<1>(input_image)
			<< "\\TrackHit2_0_" << std::setw(8) << std::setfill('0') << std::get<2>(input_image)
			<< "_" << std::setw(1) << LayerID << "_000.spng";
		input_image_bin = ss.str();
	}
	//start_layerの設定
	int start_layer, skip;
	if (mode == 1) {
		start_layer = 0;
		skip = 2;
	}
	else {
		start_layer = 0;
		skip = 1;
	}

	std::vector<std::vector<uchar>> vvin;
	read_vbin(input_image_bin, vvin);
	for (int j = start_layer; j < start_layer + 32; j += skip) {
		cv::Mat mat1 = cv::imdecode(vvin[j], 0);
		vmat1.emplace_back(mat1);
	}
	vvin.clear();

	//1trackずつpixel count
	//angle measure
	int factor = vmat1.size() / 16;
	int width = vmat1[0].cols;
	int height = vmat1[0].rows;
	double z_demoninator = 1. / (factor * 16 - 1);
	if (vmat1[0].cols != vmat1[0].step)throw std::exception();

	cv::Mat buffer = cv::Mat::zeros(64, 64, CV_8U);
	double radial_length = 1;
	int dc = 1;
	//飛跡ごとにピクセルを探索する
	std::vector<int> vhit;
	std::vector<std::pair<char, char>> pixels;
	for (int i = 0; i < 4; i++) {
		for (auto& v : m[i]) {
			//角度再測定iteration 
			double mk_pixel_ax, mk_pixel_ay;
			bool mk_pixel_flg = true;
			for (int iteration = 0; iteration < 5; iteration++) {
				const double pax = v.ax / 15;
				const double pay = v.ay / 15;
				//radial方向広めに見たい
				pixels.clear();
				if (iteration == 0) {
					radial_length = 2;
				}
				else {
					radial_length = 1;
				}
				//角度が大きく変わってなかったら変えなくてよい?
				if (iteration == 0 || (mk_pixel_ax - pax * radial_length) > 1 || (mk_pixel_ax - pax * radial_length) > 1) {
					if (iteration != 0) {
						printf("id %d iteration %d\n", v.m->rawid, iteration);
						printf("pax %5.1lf pay %5.1lf\n", mk_pixel_ax, mk_pixel_ay);
						printf("pax %5.1lf pay %5.1lf\n", pax * radial_length, pay * radial_length);
					}
					mk_pixel_ax = pax * radial_length;
					mk_pixel_ay = pay * radial_length;

					pixels = make_pixels(mk_pixel_ax, mk_pixel_ay, buffer, dc);
				}
				//z,x,yのtuple
				std::vector<double> hit_z;
				hit_z.reserve(pixels.size() * 16 * factor);
				std::vector<int> hit_x, hit_y;
				hit_x.reserve(pixels.size() * 16 * factor);
				hit_y.reserve(pixels.size() * 16 * factor);

				//<std::tuple<double, int, int>> hit_pixel;
				vhit.clear();

				for (int j = 0; j < vmat1.size(); j++) {

					//各画像での飛跡の中心座標[pixel]
					const int cpx = std::round(v.px_center + pax * (j - 7 * factor)  * ((16. - 1) / (16 * factor - 1)));//四捨五入
					const int cpy = std::round(v.py_center + pay * (i - 7 * factor)  * ((16. - 1) / (16 * factor - 1)));//四捨五入

					int hit = 0;
					//飛跡中心が外に出たら測定終了したほうがバイアスしない?
					for (const auto& r : pixels) {
						const int x = cpx + r.first;
						const int y = cpy + r.second;
						if (x < 0)continue;
						if (y < 0)continue;
						if (x >= width)continue;
						if (y >= height)continue;
						if (vmat1[j].data[y * width + x] == 0)continue;
						hit_z.push_back(j*z_demoninator);
						hit_x.push_back(x);
						hit_y.push_back(y);
						hit++;
					}
					vhit.emplace_back(hit);
				}
				bool flg = measure_angle2(hit_x, hit_y, hit_z, v.ax, v.ay, v.px_center, v.py_center, 7 * factor*z_demoninator);
				if (!flg) {
					if (false) {
						printf("measure angle mistake\n");
						printf("hit pixels\n");
						for (int j = 0; j < hit_z.size(); j++) {
							printf("%d %lf %d %d\n", std::round(hit_z[i] / z_demoninator), hit_x[i], hit_y[i], hit_z[i]);
						}
					}
				}
			}

			//結果の代入
			int vol2 = std::accumulate(vhit.begin(), vhit.end(), 0);
			int ph2 = 0;
			for (int i = 0; i < vhit.size(); i++) {
				if (vhit[i] == 0)continue;
				ph2++;
			}
			v.pixelnum = pixels.size();
			v.hitnum = vol2;
			v.ph2 = ph2;
		}
	}
}
void ReCalc_microtrack_inf1(std::string file_beta_path, std::tuple<int, int, int> input_image, std::vector<microtrack_center> m[4], int LayerID, int mode) {
	if (mode != 1 && mode != 2) {
		fprintf(stderr, "mode exception\n");
		fprintf(stderr, "mode=1: calc for 16layer\n");
		fprintf(stderr, "mode=2: calc for 32layer\n");
	}
	//画像入力
	std::vector<cv::Mat> vmat1;
	std::string input_image_bin;
	{
		std::stringstream ss;
		ss << file_beta_path << "\\DATA\\" << std::setw(2) << std::setfill('0') << std::get<0>(input_image)
			<< "_" << std::setw(2) << std::setfill('0') << std::get<1>(input_image)
			<< "\\TrackHit2_0_" << std::setw(8) << std::setfill('0') << std::get<2>(input_image)
			<< "_" << std::setw(1) << LayerID << "_000.spng";
		input_image_bin = ss.str();
	}
	//start_layerの設定
	int start_layer, skip;
	if (mode == 1) {
		start_layer = 0;
		skip = 2;
	}
	else {
		start_layer = 0;
		skip = 1;
	}

	std::vector<std::vector<uchar>> vvin;
	read_vbin(input_image_bin, vvin);
	for (int j = start_layer; j < start_layer + 32; j += skip) {
		cv::Mat mat1 = cv::imdecode(vvin[j], 0);
		vmat1.emplace_back(mat1);
	}
	vvin.clear();

	//1trackずつpixel count
	//angle measure
	int factor = vmat1.size() / 16;
	int width = vmat1[0].cols;
	int height = vmat1[0].rows;
	double z_demoninator = 1. / (factor * 16 - 1);
	if (vmat1[0].cols != vmat1[0].step)throw std::exception();

	cv::Mat buffer = cv::Mat::zeros(64, 64, CV_8U);
	double radial_length = 1;
	int dc = 1;
	//飛跡ごとにピクセルを探索する
	std::vector<int> vhit;
	std::vector<std::pair<char, char>> pixels;
	uchar* ptr = new uchar;
	for (int i = 0; i < 4; i++) {
		for (auto& v : m[i]) {
			double mk_pixel_ax, mk_pixel_ay;
			//角度再測定iteration 
			for (int iteration = 0; iteration < 5; iteration++) {
				const double pax = v.ax / 15;
				const double pay = v.ay / 15;
				//radial方向広めに見たい
				pixels.clear();
				if (iteration == 0) {
					radial_length = 2;
				}
				else {
					radial_length = 1;
				}
				//角度が大きく変わってなかったら変えなくてよい?
				if (iteration == 0 || (mk_pixel_ax - pax * radial_length) > 0.5 || (mk_pixel_ax - pax * radial_length) > 0.5) {
					//if (iteration != 0&&iteration!=1) {
					//	printf("id %d iteration %d\n", v.m->rawid, iteration);
					//	printf("pax %5.1lf pay %5.1lf\n", mk_pixel_ax, mk_pixel_ay);
					//	printf("pax %5.1lf pay %5.1lf\n", pax * radial_length, pay * radial_length);
					//}
					mk_pixel_ax = pax * radial_length;
					mk_pixel_ay = pay * radial_length;

					pixels = make_pixels(mk_pixel_ax, mk_pixel_ay, buffer, dc);
				}

				//z,x,yのtuple
				std::vector<double> hit_z;
				hit_z.reserve(pixels.size() * 16 * factor);
				std::vector<int> hit_x, hit_y;
				hit_x.reserve(pixels.size() * 16 * factor);
				hit_y.reserve(pixels.size() * 16 * factor);

				//<std::tuple<double, int, int>> hit_pixel;
				vhit.clear();
				sort(pixels.begin(), pixels.end(), sort_pair);

				for (int j = 0; j < vmat1.size(); j++) {

					//各画像での飛跡の中心座標[pixel]
					const int cpx = std::round(v.px_center + pax * (j - 7 * factor)  * ((16. - 1) / (16 * factor - 1)));//四捨五入
					const int cpy = std::round(v.py_center + pay * (j - 7 * factor)  * ((16. - 1) / (16 * factor - 1)));//四捨五入
					int y_tmp = -9999;
					int hit = 0;
					//飛跡中心が外に出たら測定終了したほうがバイアスしない?
					for (const auto& r : pixels) {
						const int x = cpx + r.first;
						const int y = cpy + r.second;

						if (x < 0)continue;
						if (y < 0)continue;
						if (x >= width)continue;
						if (y >= height)continue;
						if (y_tmp != y) {
							y_tmp = y;
							ptr = vmat1[j].ptr<uchar>(y);
						}
						//printf("%d %d\n", vmat1[j].data[y * width + x], ptr[x]);
						//if (vmat1[j].data[y * width + x] != ptr[x]) {
						//	system("pause");
						//}

						if (ptr[x] == 0)continue;
						hit_z.push_back(j*z_demoninator);
						hit_x.push_back(x);
						hit_y.push_back(y);
						hit++;
					}
					vhit.emplace_back(hit);
				}
				bool flg = measure_angle2(hit_x, hit_y, hit_z, v.ax, v.ay, v.px_center, v.py_center, 7 * factor*z_demoninator);
				if (!flg) {
					if (false) {
						printf("measure angle mistake\n");
						printf("hit pixels\n");
						for (int j = 0; j < hit_z.size(); j++) {
							printf("%d %lf %d %d\n", std::round(hit_z[i] / z_demoninator), hit_x[i], hit_y[i], hit_z[i]);
						}
					}
				}
				if (i == 0 || i == 1) {
					//小角度スキャンは3以上は殺す
					if (fabs(v.ax)*0.45 > 3 * 60 || fabs(v.ay)*0.45 > 3 * 60)break;
				}
				else if (i == 2 || i == 3) {
					//大角度スキャンは6以上は殺す
					if (fabs(v.ax)*0.45 > 6 * 60 || fabs(v.ay)*0.45 > 6 * 60)break;
				}
			}

			//結果の代入
			int vol2 = std::accumulate(vhit.begin(), vhit.end(), 0);
			int ph2 = 0;
			for (int i = 0; i < vhit.size(); i++) {
				if (vhit[i] == 0)continue;
				ph2++;
			}
			v.pixelnum = pixels.size();
			v.hitnum = vol2;
			v.ph2 = ph2;
		}
	}
}

template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin)
{
	std::ifstream ifs(filepath, std::ios::binary);
	if (!ifs.is_open()) { std::cout << "cannot open " << filepath << std::endl; throw std::exception(); }
	while (true)
	{
		uint64_t i64;
		ifs.read((char*)&i64, sizeof(uint64_t));
		if (ifs.eof()) { break; }

		std::vector<T> vin;
		if (i64 == -1)
		{
			vvin.push_back(vin);
			continue;
		}
		if (i64 > vin.max_size()) { throw std::exception(""); }
		vin.reserve(i64);
		for (uint64_t j = 0; j < i64; j++)
		{
			T p;
			ifs.read((char*)&p, sizeof(T));
			vin.push_back(p);
		}
		vvin.push_back(vin);
	}
	ifs.close();
}
std::vector<std::pair<char, char>> make_pixels(double pax, double pay, cv::Mat& buffer, int dc)
{
	if (buffer.cols != buffer.rows)throw std::exception();
	if (dc < 1 || dc>4)throw std::exception();

	int half_width = buffer.cols / 2;
	int width = buffer.cols;

	buffer = cv::Scalar(0);

	int max_half_length = half_width - 1;

	double half_length_buf = std::hypot(pax, pay);

	std::vector<std::pair<char, char>> ret;
	//角度、dc,pax,payでreserve決めてもいいかもしれん
	ret.reserve(1000);

	if (half_length_buf >= dc) {
		double half_length = std::min(half_length_buf, double(max_half_length));
		int target_area = std::floor((half_length * 2 + 1) * (dc * 2 + 1));

		double aax = pax / half_length_buf;
		double aay = pay / half_length_buf;

		float aax3 = aax * 0.3333;
		float aay3 = aay * 0.3333;

		int area = 0;

		//中央線 1/3ピクセルずつ探索
		for (int i = 0; i <= half_length * 3; i++) {
			int x = half_width + std::roundf(aax3 * i);
			int y = half_width + std::roundf(aay3 * i);
			if (buffer.at<uchar>(cv::Point(x, y)) == 0) {
				area += 1;
				buffer.at<uchar>(cv::Point(x, y)) = 1;
				ret.push_back(std::make_pair(x - half_width, y - half_width));
			}
			x = width - x;
			y = width - y;
			if (buffer.at<uchar>(cv::Point(x, y)) == 0) {
				area += 1;
				buffer.at<uchar>(cv::Point(x, y)) = 1;
				ret.push_back(std::make_pair(x - half_width, y - half_width));
			}
		}

		//中央から1/3ピクセルずつずらした線
		for (int shift = 1;; shift++) {
			for (int i = 0; i <= half_length * 3; i++) {

				const float bax = aax3 * i;
				const float bay = aay3 * i;

				const float cax = (-aay3) * shift;
				const float cay = (+aax3) * shift;
				int x = half_width + int(std::roundf(bax + cax)); //ここが遅い1
				int y = half_width + int(std::roundf(bay + cay)); //ここが遅い2
				if (buffer.at<uchar>(cv::Point(x, y)) == 0) {
					area += 1;
					buffer.at<uchar>(cv::Point(x, y)) = 1;
					ret.push_back(std::make_pair(x - half_width, y - half_width));
				}
				//180度逆側
				x = width - x;
				y = width - y;
				if (buffer.at<uchar>(cv::Point(x, y)) == 0) {
					area += 1;
					buffer.at<uchar>(cv::Point(x, y)) = 1;
					ret.push_back(std::make_pair(x - half_width, y - half_width));
				}

				x = half_width + int(std::roundf(bax - cax)); //ここが遅い3
				y = half_width + int(std::roundf(bay - cay)); //ここが遅い4
				if (buffer.at<uchar>(cv::Point(x, y)) == 0) {
					area += 1;
					buffer.at<uchar>(cv::Point(x, y)) = 1;
					ret.push_back(std::make_pair(x - half_width, y - half_width));
				}
				//180度逆側
				x = width - x;
				y = width - y;
				if (buffer.at<uchar>(cv::Point(x, y)) == 0) {
					area += 1;
					buffer.at<uchar>(cv::Point(x, y)) = 1;
					ret.push_back(std::make_pair(x - half_width, y - half_width));
				}

				if (area >= target_area)break;
			}

			if (area >= target_area)break;
		}
	}
	else {
		if (dc == 1 || dc == 2) {
			for (int x = -dc; x <= dc; x++) {
				for (int y = -dc; y <= dc; y++) {
					ret.push_back(std::make_pair(x, y));
				}
			}
		}
		else {
			cv::circle(buffer, cv::Point(half_width, half_width), dc + 1, cv::Scalar(1), -1, 8, 0);
			std::vector<cv::Point2i> locations;
			cv::findNonZero(buffer, locations);

			ret.reserve(locations.size());
			for (const auto& p : locations) {
				ret.push_back(std::make_pair(p.x - half_width, p.y - half_width));
			}
		}
	}
	return ret;
}
bool measure_angle2(std::vector<int> hit_x, std::vector<int> hit_y, std::vector<double> hit_z, double &ax, double &ay, double &x_center, double &y_center, double z_center) {
	//hit pixelを最小二乗法でfitting
	//求めている角度は(16*factor -1)層進んだときに何pixel変化するか

	double x = 0, y = 0, z = 0, zz = 0, zx = 0, zy = 0, num = 0;
	for (int i = 0; i < hit_z.size(); i++) {
		num++;
		z += hit_z[i];
		x += hit_x[i];
		y += hit_y[i];
		zz += hit_z[i] * hit_z[i];
		zx += hit_z[i] * hit_x[i];
		zy += hit_z[i] * hit_y[i];
	}
	double slope_x, intercept_x, slope_y, intercept_y;
	double	denominator;
	denominator = num * zz - z * z;
	if (denominator == 0)return false;
	slope_x = (num*zx - z * x) / denominator;
	slope_y = (num*zy - z * y) / denominator;
	intercept_x = (zz*x - zx * z) / denominator;
	intercept_y = (zz*y - zy * z) / denominator;
	//	pax = -1 * pax * 60 / 0.45;

	//printf("angle (%5.4lf, %5.4lf) --> (%5.4lf, %5.4lf)\n", ax*0.45/60, ay*0.45 / 60, slope_x*0.45/2, slope_y*0.45/2);
	//printf("position (%5.1lf, %5.1lf) --> (%5.1lf, %5.1lf)\n", x_center, y_center, slope_x * Layer_center + intercept_x, slope_y * Layer_center + intercept_y);
	ax = slope_x;
	ay = slope_y;
	x_center = slope_x * z_center + intercept_x;
	y_center = slope_y * z_center + intercept_y;
	return true;
}

std::vector<vxx::micro_track_t> edge_noise_cut(std::vector<vxx::micro_track_t> &micro, int area, double range[4], double cut) {

	std::vector<vxx::micro_track_t> ret;
	std::vector<vxx::micro_track_t> edge;
	bool flg;
	//edge trackの抽出
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		flg = true;
		if (area % 2 == 1) {
			if (itr->x < range[0] + cut) {
				flg = false;
			}
		}
		if (area % 2 == 0) {
			if (range[1] - cut < itr->x) {
				flg = false;
			}
		}
		if (area == 5 || area == 6) {
			if (range[3] - cut < itr->y) {
				flg = false;
			}
		}
		if (flg) {
			ret.push_back(*itr);
		}
		else {
			edge.push_back(*itr);
		}
	}

	if (edge.size() == 0) {
		return ret;
	}

	//10umでhash化
	double edge_range[4] = {};
	get_fvxx_area(edge, edge_range);
	double hash = 10;
	std::multimap<std::pair<int, int>, vxx::micro_track_t> m_hash;
	int ix, iy;
	for (auto itr = edge.begin(); itr != edge.end(); itr++) {
		ix = (itr->x - edge_range[0]) / hash;
		iy = (itr->y - edge_range[2]) / hash;
		m_hash.insert(std::make_pair(std::make_pair(ix, iy), *itr));
	}

	//各binの数を数える
	std::vector<int> count;
	int num;
	for (auto itr = m_hash.begin(); itr != m_hash.end(); itr++) {
		num = m_hash.count(itr->first);
		count.push_back(num);
		itr = std::next(itr, num - 1);
		//printf("%d\n", num);
	}
	//平均と偏差を見る
	double mean = 0, rms = 0;
	for (auto itr = count.begin(); itr != count.end(); itr++) {
		mean += *itr;
		rms += (*itr)*(*itr);
	}
	mean = mean / count.size();
	rms = sqrt(rms / count.size() - mean * mean);

	//thresholdを決める
	//さすがに10track/(10um)^2は切っていいだろう
	int threshold;
	threshold = mean + rms * 5;
	threshold = std::max(threshold, 3);
	for (auto itr = m_hash.begin(); itr != m_hash.end(); itr++) {
		num = m_hash.count(itr->first);
		if (num < threshold) {
			auto res = m_hash.equal_range(itr->first);
			for (auto itr2 = res.first; itr2 != res.second; itr2++) {
				ret.push_back(itr2->second);
			}
		}
		itr = std::next(itr, num - 1);
	}
	return ret;

}
void get_fvxx_area(std::vector<vxx::micro_track_t> &micro, double range[4]) {

	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		if (itr == micro.begin()) {
			range[0] = itr->x;
			range[1] = itr->x;
			range[2] = itr->y;
			range[3] = itr->y;
		}
		range[0] = std::min(itr->x, range[0]);
		range[1] = std::max(itr->x, range[1]);
		range[2] = std::min(itr->y, range[2]);
		range[3] = std::max(itr->y, range[3]);
	}


}

void fvxx_merge_filter(std::string output, std::string output_tmp, std::vector<std::string> input, int pos) {
	std::stringstream exe_command0;
	std::string path_exe = "M:\\data\\NINJA\\NETSCAN\\win-msvc-x64-15\\bin\\";
	exe_command0 << path_exe << "f_join " << pos << " " << output_tmp;
	for (int i = 0; i < input.size(); i++) {
		exe_command0 << " " << input[i];
	}
	system(exe_command0.str().c_str());

	std::stringstream exe_command1;
	exe_command1 << path_exe << "f_filter " << pos << " " << output_tmp << " --o " << output
		<< " --ghost-rl 5 5 0.01 0.01 0.05 5 --view 10000 1000";
	system(exe_command1.str().c_str());

	for (int i = 0; i < input.size(); i++) {
		remove(input[i].c_str());
	}
	remove(output_tmp.c_str());
}
