#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Event_information> eventid_selection(std::vector<Momentum_recon::Event_information> &momch, int eventid);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-momch eventid file-out-momch\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[3];
	int eventid = std::stoi(argv[2]);

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	momch = eventid_selection(momch, eventid);
	if (momch.size() == 0) {
		fprintf(stderr,"eventid=%d not found\n",eventid);
		exit(1);
	}

	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);

}
std::vector<Momentum_recon::Event_information> eventid_selection(std::vector<Momentum_recon::Event_information> &momch,int eventid) {
	std::vector<Momentum_recon::Event_information>ret;
	for (auto &ev : momch) {
		if (ev.groupid == eventid) {
			ret.push_back(ev);
		}
	}
	return ret;


}