#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>
#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <stack>
#include <unordered_set>
#include <chrono>
using namespace l2c;
class output_format {
public:
	int groupid, trackid;
	int num_comfirmed_path, num_cut_path, num_select_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> comfirmed_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> cut_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> select_path;
};

class Chain_baselist {
public:
	int64_t groupid, trackid;
	std::pair<int32_t, int64_t> target_track;
	std::set<std::pair<int32_t, int64_t>> btset;
	std::set<std::tuple<int, int, int, int>>ltlist;
	std::vector<int32_t> usepos;
	int chain_num;
	std::vector<Linklet> make_link_list();
	void set_usepos();
};
std::vector < Chain_baselist > read_linklet_list2_bin(std::string filename);
void output_group(std::string filename, std::vector<Chain_baselist>&g);
Chain_baselist  select_group(std::vector < Chain_baselist >group, int groupid);
std::vector< output_format> read_chain_file_bin(std::string filename);
void output_linklet(std::string filename, std::vector<output_format> &out);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}

	std::string file_in_group = argv[1];
	int groupid = std::stoi(argv[2]);
	std::string file_out_group = argv[3];

	std::vector< output_format> g2 = read_chain_file_bin(file_in_group);
	output_linklet(file_out_group, g2);

	//std::vector < Chain_baselist >g = read_linklet_list2_bin(file_in_group);
	//std::vector < Chain_baselist >out;
	//out.push_back(select_group(g, groupid));
	//output_group(file_out_group, out);


}
std::vector < Chain_baselist > read_linklet_list2_bin(std::string filename) {
	std::vector < Chain_baselist > ret;
	std::ifstream ifs(filename, std::ios::binary);
	int gid, tid, muon_num, t_pl, t_rawid;
	int64_t link_num;
	int pl, raw;
	double weight;
	std::tuple<int, int, int, int> link;
	int count = 0;
	while (ifs.read((char*)& gid, sizeof(int))) {
		//ifs.read((char*)& gid, sizeof(int));
		ifs.read((char*)& tid, sizeof(int));
		ifs.read((char*)& t_pl, sizeof(int));
		ifs.read((char*)& t_rawid, sizeof(int));
		ifs.read((char*)& link_num, sizeof(int));
		//while (ifs >> gid >> tid >> t_pl >> t_rawid >> link_num) {
		if (count % 10000 == 0) {
			printf("\r read group %d gid=%d link=%lld", count, gid, link_num);
		}
		count++;
		Chain_baselist c;
		c.groupid = gid;
		c.trackid = tid;
		c.target_track.first = t_pl;
		c.target_track.second = t_rawid;
		for (int64_t i = 0; i < link_num; i++) {
			ifs.read((char*)& std::get<0>(link), sizeof(int));
			ifs.read((char*)& std::get<1>(link), sizeof(int));
			ifs.read((char*)& std::get<2>(link), sizeof(int));
			ifs.read((char*)& std::get<3>(link), sizeof(int));
			//ifs >> std::get<0>(link) >> std::get<1>(link) >> std::get<2>(link) >> std::get<3>(link);
			c.btset.insert(std::make_pair(std::get<0>(link), std::get<2>(link)));
			c.btset.insert(std::make_pair(std::get<1>(link), std::get<3>(link)));
			c.ltlist.insert(link);
		}
		ret.push_back(c);
	}
	printf("\r read group %d gid=%d link=%lld\n", count, gid, link_num);

	return ret;

}
 Chain_baselist  select_group(std::vector < Chain_baselist >group,int groupid) {
	 Chain_baselist ret;
	 for (auto &g : group) {
		 if (g.groupid == groupid) {
			 return g;
		 }
	 }
	 return ret;

}
void output_group(std::string filename, std::vector<Chain_baselist>&g) {
	std::ofstream ofs(filename);
	int count = 0;
	for (auto itr = g.begin(); itr != g.end(); itr++) {
		if (count % 1000 == 0) {
			fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)", count, g.size(), count*100. / g.size());
		}
		count++;
		ofs << std::fixed << std::right
			<< std::setw(10) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(5) << std::setprecision(0) << itr->trackid<< " "
			<< std::setw(4) << std::setprecision(0) << itr->target_track.first << " "
			<< std::setw(4) << std::setprecision(0) << itr->target_track.second << " "
			<< std::setw(10) << std::setprecision(0) <<itr->ltlist.size() << std::endl;
		if (itr->ltlist.size() == 0)continue;
		for (auto itr2 = itr->ltlist.begin(); itr2 != itr->ltlist.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(5) << std::setprecision(0) << std::get<0>(*itr2) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(*itr2) << std::endl;
		}
	}
	fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)\n", count, g.size(), count*100. / g.size());

}


std::vector< output_format> read_chain_file_bin(std::string filename) {
	std::vector< output_format> ret;
	std::ifstream ifs(filename, std::ios::binary);
	output_format out;
	std::tuple<int, int, int, int> path;
	int id;
	int pl, rawid;
	while (ifs.read((char*)&out.groupid, sizeof(int))) {

		ifs.read((char*)&out.trackid, sizeof(int));
		ifs.read((char*)&out.num_comfirmed_path, sizeof(int));
		ifs.read((char*)&out.num_cut_path, sizeof(int));
		ifs.read((char*)&out.num_select_path, sizeof(int));

		for (int i = 0; i < out.num_comfirmed_path; i++) {
			ifs.read((char*)&id, sizeof(int));
			ifs.read((char*)&std::get<0>(path), sizeof(int));
			ifs.read((char*)&std::get<1>(path), sizeof(int));
			ifs.read((char*)&std::get<2>(path), sizeof(int));
			ifs.read((char*)&std::get<3>(path), sizeof(int));
			out.comfirmed_path.push_back(std::make_pair(id, path));
		}
		for (int i = 0; i < out.num_cut_path; i++) {
			ifs.read((char*)&id, sizeof(int));
			ifs.read((char*)&std::get<0>(path), sizeof(int));
			ifs.read((char*)&std::get<1>(path), sizeof(int));
			ifs.read((char*)&std::get<2>(path), sizeof(int));
			ifs.read((char*)&std::get<3>(path), sizeof(int));
			out.cut_path.push_back(std::make_pair(id, path));
		}
		for (int i = 0; i < out.num_select_path; i++) {
			ifs.read((char*)&id, sizeof(int));
			ifs.read((char*)&std::get<0>(path), sizeof(int));
			ifs.read((char*)&std::get<1>(path), sizeof(int));
			ifs.read((char*)&std::get<2>(path), sizeof(int));
			ifs.read((char*)&std::get<3>(path), sizeof(int));
			out.select_path.push_back(std::make_pair(id, path));
		}

		ret.push_back(out);
		out.comfirmed_path.clear();
		out.cut_path.clear();
		out.select_path.clear();
	}

	return ret;
}


void output_linklet(std::string filename, std::vector<output_format> &out) {
	std::ofstream ofs(filename);
	double weight = 1;
	int link_num = 0;
	int all = out.size();
	int count = 0;
	for (auto itr = out.begin(); itr != out.end(); itr++) {

		if (count % 1000 == 0) {
			printf("\r write path %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		if (itr->groupid != 60428323)continue;

		ofs << std::fixed << std::right
			<< std::setw(5) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(5) << std::setprecision(0) << itr->trackid << " "
			<< std::setw(3) << std::setprecision(0) << itr->num_comfirmed_path << " "
			<< std::setw(3) << std::setprecision(0) << itr->num_cut_path << " "
			<< std::setw(3) << std::setprecision(0) << itr->num_select_path << std::endl;
		for (auto itr2 = itr->comfirmed_path.begin(); itr2 != itr->comfirmed_path.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(3) << std::setprecision(0) << itr2->first << " "
				<< std::setw(5) << std::setprecision(0) << std::get<0>(itr2->second) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(itr2->second) << std::endl;
		}

		for (auto itr2 = itr->cut_path.begin(); itr2 != itr->cut_path.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(3) << std::setprecision(0) << itr2->first << " "
				<< std::setw(5) << std::setprecision(0) << std::get<0>(itr2->second) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(itr2->second) << std::endl;
		}

		for (auto itr2 = itr->select_path.begin(); itr2 != itr->select_path.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(3) << std::setprecision(0) << itr2->first << " "
				<< std::setw(5) << std::setprecision(0) << std::get<0>(itr2->second) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(itr2->second) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(itr2->second) << std::endl;
		}
	}
	printf("\r write path %d/%d(%4.1lf%%)\n", count, all, count*100. / all);


}

