#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class Chain_angle_difference {
public:
	int groupid, chainid, pl,count_angle,count_vph[2];
	double rms_al, rms_ar,dal,dar,sd_vph[2],mean_vph[2];
	Chain_angle_difference(Momentum_recon::Mom_chain &c,int t_pl);
	double Get_angle_lateral_chi2() {
		if (rms_al > 0)return pow(dal / rms_al, 2);
		else return -1;
	};
	double Get_angle_radial_chi2() {
		if (rms_ar > 0)return pow(dar / rms_ar, 2);
		else return -1;
	};
	double Get_vph_chi2() {
		if (sd_vph[0] > 0 && sd_vph[1] > 0) {
			double ratio = mean_vph[1] / mean_vph[0];
			double error = sqrt(sqrt(pow(sd_vph[1] / mean_vph[0], 2) + pow(ratio*sd_vph[0] / mean_vph[0], 2)));
			return pow((ratio - 1) / error, 2);
		}
		else return -1;
	};
	double Get_vph_chi2_plus() {
		if (sd_vph[0] > 0 && sd_vph[1] > 0) {
			double ratio = mean_vph[1] / mean_vph[0];
			double error = sqrt(sqrt(pow(sd_vph[1] / mean_vph[0], 2) + pow(ratio*sd_vph[0] / mean_vph[0], 2)));
			if (ratio > 1) {
				return pow((ratio - 1) / error, 2);
			}
			else return -1;
		}
		else return -1;
	};
};
std::vector<int> get_pl_list(Momentum_recon::Mom_chain &c);
std::vector<Chain_angle_difference> Calc_chain_angle_difference(std::vector<Momentum_recon::Mom_chain> &momch);
void output(std::string filename, std::vector<Chain_angle_difference>&diff);

int main(int argc,char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out = argv[2];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::vector<Chain_angle_difference> diff = Calc_chain_angle_difference(momch);
	output(file_out, diff);


}
std::vector<Chain_angle_difference> Calc_chain_angle_difference(std::vector<Momentum_recon::Mom_chain> &momch) {
	std::vector<Chain_angle_difference> ret;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		int nseg = itr->base.size();
		std::vector<int>pl_list = get_pl_list(*itr);
		for (auto &pl : pl_list) {
			Chain_angle_difference diff(*itr, pl);
			ret.push_back(diff);
		}
	}
	return ret;
}
std::vector<int> get_pl_list(Momentum_recon::Mom_chain &c) {
	std::vector<int> ret;
	for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
		ret.push_back(itr->pl);
	}
	if (ret.size() >= 1) {
		ret.pop_back();
	}
	return ret;
}
Chain_angle_difference::Chain_angle_difference(Momentum_recon::Mom_chain &c,int t_pl) {
	groupid = c.groupid;
	chainid = c.chainid;
	pl = t_pl;
	count_angle = 0;
	count_vph[0] = 0;
	count_vph[1] = 0;
	
	//angle
	double sum[2] = {}, sum2[2] = {};
	for (auto &p : c.base_pair) {
		double angle = sqrt(p.first.ax*p.first.ax + p.first.ay*p.first.ay);
		double dax = p.second.ax - p.first.ax;
		double day = p.second.ay - p.first.ay;
		if (p.first.pl == t_pl) {
			if (angle < 0.001) {
				dar = day;
				dal =dax;
			}
			else {
				dar = (dax*p.first.ax+day*p.first.ay)/angle;
				dal = (dax*p.first.ay - day * p.first.ax) / angle;
			}
		}
		else {
			if (angle < 0.001) {
				sum[0] += day;
				sum[1] += dax;
				sum2[0] += pow(day,2);
				sum2[1] += pow(dax, 2);
			}
			else {
				sum[0] += (dax*p.first.ax + day * p.first.ay) / angle;
				sum[1] += (dax*p.first.ay - day * p.first.ax) / angle;
				sum2[0] += pow((dax*p.first.ax + day * p.first.ay) / angle,2);
				sum2[1] +=pow( (dax*p.first.ay - day * p.first.ax) / angle,2);
			}
			count_angle += 1;
		}
	}

	if (count_angle > 0) {
		rms_ar = sqrt(sum2[0] / count_angle);
		rms_al = sqrt(sum2[1] / count_angle);
	}
	else {
		rms_ar = -1;
		rms_al = -1;
	}

	sum[0] =0 ;
	sum[1] = 0;
	sum2[0] = 0;
	sum2[1] = 0;
	for (auto &b : c.base) {
		if (b.pl <= pl) {
			for (int i = 0; i < 2; i++) {
				sum[0] += b.m[i].ph % 10000;
				sum2[0] += pow(b.m[i].ph % 10000,2);
				count_vph[0]++;
			}
		}
		else {
			for (int i = 0; i < 2; i++) {
				sum[1] += b.m[i].ph % 10000;
				sum2[1] += pow(b.m[i].ph % 10000, 2);
				count_vph[1]++;
			}
		}
	}
	for (int i = 0; i < 2; i++) {
		if (count_vph[i] <= 0) {
			mean_vph[i] = -1;
			sd_vph[i] = -1;
		}
		else {
			mean_vph[i] = sum[i]/count_vph[i];
			double variance = sum2[i] / count_vph[i] - mean_vph[i] * mean_vph[i];
			if (variance <= 0 || count_vph[i] <= 2) {
				sd_vph[i] = -1;
			}
			else {
				sd_vph[i] = sqrt(variance)*sqrt(count_vph[i]) / sqrt(count_vph[i] - 1);
			}
		}
	}
}

void output(std::string filename, std::vector<Chain_angle_difference>&diff) {
	std::ofstream ofs(filename);
	int count = 0, all = diff.size();
	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		if (count % 1000 == 0) {
			fprintf(stderr, "\r output %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		ofs << std::fixed << std::right
			<< std::setw(10) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(10) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(4) << std::setprecision(0) << itr->pl << " "
			<< std::setw(4) << std::setprecision(0) << itr->count_angle << " "
			<< std::setw(4) << std::setprecision(0) << itr->count_vph[0] << " "
			<< std::setw(4) << std::setprecision(0) << itr->count_vph[1] << " "
			<< std::setw(7) << std::setprecision(4) << itr->dar << " "
			<< std::setw(7) << std::setprecision(4) << itr->dal << " "
			<< std::setw(7) << std::setprecision(5) << itr->rms_ar << " "
			<< std::setw(7) << std::setprecision(5) << itr->rms_al << " "
			<< std::setw(8) << std::setprecision(2) << itr->mean_vph[0] << " "
			<< std::setw(8) << std::setprecision(2) << itr->mean_vph[1] << " "
			<< std::setw(7) << std::setprecision(3) << itr->sd_vph[0] << " "
			<< std::setw(7) << std::setprecision(3) << itr->sd_vph[1] << std::endl;
	}

	fprintf(stderr, "\r output %d/%d(%4.1lf%%)", count, all, count*100. / all);

}