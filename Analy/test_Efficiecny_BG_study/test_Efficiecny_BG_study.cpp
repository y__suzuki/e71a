#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")

#include <FILE_structure.hpp>
#include <random>

struct track {
	//track_end_flg=1は最上流(PL大)
	//track_end_flg=-1は最下流(PL小)
	//track_end_flg=0は中間

	int pl, track_id, track_end_flg, edge_out_flg;
	double x, y, z, ax, ay;
	bool exist_flg;
};
std::vector<track > creat_initial_track(std::vector<netscan::linklet_t> link);
void output_track(std::string filename, std::vector<track> &t);
void output_stop_track(std::string filename, std::vector<track> &t);
double gap_calc(int pl);
std::vector<track> track_extraporate(track t);
std::vector<track>track_extraporate_all(std::vector<track> t);
void track_area_sel(std::vector<track> &t);
void track_inefficiency(std::vector<track> &t, double efficiency);
void track_connect(std::vector<track>&t);

bool sort_track(const track &left, const track &right) {
	return left.track_id == right.track_id ? left.pl < right.pl : left.track_id < right.track_id;

}
int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file-in-linket file-out file-out-stop efficiency\n");
		exit(1);
	}
	std::string file_in_linklet = argv[1];
	std::string file_out_track = argv[2];
	std::string file_out_stop = argv[3];
	double efficiency = std::stod(argv[4]);

	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_txt(file_in_linklet, link);
	std::vector<track > track_ini = creat_initial_track(link);
	std::vector<track> track_all = track_extraporate_all(track_ini);
	track_inefficiency(track_all, efficiency);
	track_connect(track_all);
	//track_area_sel(track_all);
	output_stop_track(file_out_stop, track_all);
	output_track(file_out_track, track_all);
}
std::vector<track > creat_initial_track(std::vector<netscan::linklet_t> link) {
	double x_min, x_max, y_min, y_max;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (itr == link.begin()) {
			x_min = itr->b[0].x;
			x_max = itr->b[0].x;
			y_min = itr->b[0].y;
			y_max = itr->b[0].y;
		}
		x_min = std::min(x_min, itr->b[0].x);
		x_max = std::max(x_max, itr->b[0].x);
		y_min = std::min(y_min, itr->b[0].y);
		y_max = std::max(y_max, itr->b[0].y);
	}

	double area_cut = 10000;
	x_min += area_cut;
	x_max -= area_cut;
	y_min += area_cut;
	y_max -= area_cut;
	std::vector<track>t_vec;
	std::vector<track>ret;
	double x, y;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (itr->b[0].x < x_min)continue;
		if (itr->b[0].y < y_min)continue;
		if (itr->b[0].x > x_max)continue;
		if (itr->b[0].y > y_max)continue;
		track t;
		t.ax = itr->b[0].ax;
		t.ay = itr->b[0].ay;
		t.x = itr->b[0].x - x_min;
		t.y = itr->b[0].y - y_min;
		t.edge_out_flg = 0;
		t.exist_flg = true;
		t.track_end_flg = 0;
		t.pl = 0;
		t.track_id = 0;
		t.z = 0;
		t_vec.push_back(t);
	}
	int count = 0;
	for (int ix = 0; ix < 50; ix++) {
		for (int iy = 0; iy < 50; iy++) {
			for (auto itr = t_vec.begin(); itr != t_vec.end(); itr++) {
				if (itr->x + ix * (x_max - x_min) > 2000000)continue;
				if (itr->y + iy * (y_max - y_min) > 2000000)continue;

				track t;
				t.ax = itr->ax;
				t.ay = itr->ay;

				t.x = itr->x + ix * (x_max - x_min);
				t.y = itr->y + iy * (y_max - y_min);
				t.edge_out_flg = 0;
				t.exist_flg = true;
				t.track_end_flg = 0;
				t.pl = 0;
				t.track_id = count;
				t.z = 0;
				ret.push_back(t);
				count++;
			}
		}
	}
	return ret;
}
std::vector<track> track_extraporate(track t) {
	std::vector<track> ret;
	double gap;
	t.pl = 1;
	t.z = 0;
	ret.push_back(t);
	for (int pl = 1; pl < 133; pl++) {
		t.pl = pl+1;
		gap = gap_calc(pl);
		t.z -= gap;
		t.x -= t.ax*gap;
		t.y -= t.ay*gap;
		ret.push_back(t);
	}
	return ret;

}
double gap_calc(int pl) {
	double one_film = 350;
	double one_acryl = 2000;
	double one_iron = 500;
	double one_water = 2200;
	double one_pack = 110;
	if (pl == 1)return one_film;
	else if (pl == 2)return one_film + one_acryl;
	else if (pl == 3)return one_film;
	else if (pl <= 4&&pl<=14)return one_film + one_iron;
	else if (pl == 15)return one_film + one_pack * 2;
	else if (pl % 2 == 0)return one_film + one_iron;
	else if (pl % 2 == 1)return one_film + one_water + one_pack * 2;
}
std::vector<track>track_extraporate_all(std::vector<track> t) {
	std::vector<track> ret;
	int count = 0;
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r now extrapolate %d/%d(%4.1lf%%)", count, t.size(), count*100. / t.size());
		}
		count++;
		std::vector<track> extra = track_extraporate(*itr);
		track_area_sel(extra);
		for (auto itr2 = extra.begin(); itr2 != extra.end(); itr2++) {
			if (itr2->edge_out_flg != 1) {
				ret.push_back(*itr2);
			}
		}
	}
	fprintf(stderr, "\r now extrapolate %d/%d(%4.1lf%%)\n", count, t.size(), count*100. / t.size());
	return ret;
}
void track_area_sel(std::vector<track> &t) {
	double x_min = 875000;
	double x_max = 1125000;
	double y_min = 875000;
	double y_max = 1125000;
	//double x_min = 975000;
	//double x_max = 1025000;
	//double y_min = 975000;
	//double y_max = 1025000;

	//edge_out_flg
	//2:次(前)のPLでedge out
	//1:edge_out
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		if (itr->x < x_min)itr->edge_out_flg = 1;
		if (itr->y < y_min)itr->edge_out_flg = 1;
		if (itr->x > x_max)itr->edge_out_flg = 1;
		if (itr->y > y_max)itr->edge_out_flg = 1;
	}
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		auto next_t = std::next(itr, 1);
		if (next_t != t.end() && itr->edge_out_flg == 0 && next_t->edge_out_flg == 1)itr->edge_out_flg = 2;
		if (itr == t.begin())continue;
		auto before_t = std::prev(itr, 1);
		if (before_t->edge_out_flg == 1 && itr->edge_out_flg == 0) itr->edge_out_flg = 2;
	}

}
void track_inefficiency(std::vector<track> &t,double efficiency) {
	std::random_device seed_gen;
	std::mt19937 engine(seed_gen());

	// 一様実数分布
	// [-1.0, 1.0)の値の範囲で、等確率に実数を生成する
	std::uniform_real_distribution<> dist1(0, 1.0);
	int all=0, eff=0, ineff=0;
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		all++;
		if (dist1(engine) < efficiency) {
			itr->exist_flg = 1;
			eff++;
		}
		else {
			itr->exist_flg = 0;
			ineff++;
		}

	}
	printf("input efficiecny =%5.4lf\n", efficiency);
	printf("track num=%d\n", all);
	printf("eff track num=%d\n", eff);
	printf("ineff track num=%d\n", ineff);
	printf("efficiecny =%5.4lf\n", eff*1.0 / all);
}
void track_connect(std::vector<track>&t) {
	sort(t.begin(), t.end(), sort_track);
	int t_id = -1;
	int start_flg = 0;
	int stop_flg = 0;
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		if (itr->track_id != t_id) {
			t_id = itr->track_id;
			start_flg = 0;
		}

		if (start_flg == 0) {
			if (itr->exist_flg == 1) {
				start_flg = 1;
				itr->track_end_flg = -1;
			}
		}
		if (start_flg == 1&&itr->exist_flg==1) {
			if (itr->pl % 2 == 0) {
				auto itr2 = itr;
				stop_flg = 1;
				for (int i = 1; i <= 3; i++) {
					itr2 = std::next(itr2, 1);
					if (itr2 == t.end() || itr2->track_id != t_id) {
						break;
					}
					if (itr2->exist_flg == 1) {
						stop_flg = 0;
						break;
					}
				}
			}
			else {
				auto itr2 = itr;
				stop_flg = 1;
				for (int i = 1; i <= 2; i++) {
					itr2 = std::next(itr2, 1);
					if (itr2 == t.end() || itr2->track_id != t_id) {
						break;
					}
					if (itr2->exist_flg == 1) {
						stop_flg = 0;
						break;
					}
				}
			}
			if (stop_flg == 1) {
				itr->track_end_flg = 1;
				start_flg = 0;
			}
		}
	}
}

void output_track(std::string filename, std::vector<track> &t) {

	double x_min, y_min;
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		if (itr == t.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		y_min = std::min(itr->y, y_min);
	}
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		itr->x -= x_min;
		itr->y -= y_min;
	}

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (t.size() == 0) {
		fprintf(stderr, "target track ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = t.begin(); itr != t.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write track ... %d/%d (%4.1lf%%)", count, int(t.size()), count*100. / t.size());
			}
			count++;
			if (itr->edge_out_flg == 1)continue;
			ofs << std::right << std::fixed
				<< std::setw(12) << std::setprecision(0) << itr->track_id << " "
				<< std::setw(4) << std::setprecision(0) << itr->pl << " "
				<< std::setw(7) << std::setprecision(4) << itr->ax << " "
				<< std::setw(7) << std::setprecision(4) << itr->ay << " "
				<< std::setw(10) << std::setprecision(1) << itr->x << " "
				<< std::setw(10) << std::setprecision(1) << itr->y << " "
				<< std::setw(10) << std::setprecision(1) << itr->z << " "
				<< std::setw(3) << std::setprecision(0) << itr->track_end_flg << " "
				<< std::setw(3) << std::setprecision(0) << itr->exist_flg << " "
				<< std::setw(3) << std::setprecision(0) << itr->edge_out_flg << std::endl;
		}
		fprintf(stderr, "\r Write track ... %d/%d (%4.1lf%%)\n", count, int(t.size()), count*100. / t.size());

	}
}
void output_stop_track(std::string filename, std::vector<track> &t) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (t.size() == 0) {
		fprintf(stderr, "target track ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = t.begin(); itr != t.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write track ... %d/%d (%4.1lf%%)", count, int(t.size()), count*100. / t.size());
			}
			count++;
			if (itr->edge_out_flg == 1)continue;
			if (itr->edge_out_flg == 2)continue;
			if (itr->exist_flg == 0)continue;
			if (itr->pl==133)continue;
			if (itr->track_end_flg == 1) {
				ofs << std::right << std::fixed
					<< std::setw(12) << std::setprecision(0) << itr->track_id << " "
					<< std::setw(4) << std::setprecision(0) << itr->pl << " "
					<< std::setw(7) << std::setprecision(4) << itr->ax << " "
					<< std::setw(7) << std::setprecision(4) << itr->ay << " "
					<< std::setw(10) << std::setprecision(1) << itr->x << " "
					<< std::setw(10) << std::setprecision(1) << itr->y << " "
					<< std::setw(10) << std::setprecision(1) << itr->z << " "
					<< std::setw(3) << std::setprecision(0) << itr->track_end_flg << " "
					<< std::setw(3) << std::setprecision(0) << itr->exist_flg << " "
					<< std::setw(3) << std::setprecision(0) << itr->edge_out_flg << std::endl;
			}
		}
		fprintf(stderr, "\r Write track ... %d/%d (%4.1lf%%)\n", count, int(t.size()), count*100. / t.size());

	}
}
