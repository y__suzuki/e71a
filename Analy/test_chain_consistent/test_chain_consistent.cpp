#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void Extract_muon_chain(std::vector<mfile0::M_Chain>&c);
void Extract_diff_chain(std::vector<mfile0::M_Chain>&c0, std::vector<mfile0::M_Chain>&c1, std::vector<mfile0::M_Chain>&ret0, std::vector<mfile0::M_Chain>&ret1);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:file-in-mfile file-in-mfile\n");
		exit(1);
	}
	std::string file_in_mfile_old = argv[1];
	std::string file_in_mfile_new = argv[2];
	std::string file_out_mfile_old = argv[3];
	std::string file_out_mfile_new = argv[4];

	mfile0::Mfile m_old, m_new;
	mfile1::read_mfile_extension(file_in_mfile_old, m_old);
	mfile1::read_mfile_extension(file_in_mfile_new, m_new);

	Extract_muon_chain(m_old.chains);
	Extract_muon_chain(m_new.chains);

	std::vector<mfile0::M_Chain>c0, c1;
	Extract_diff_chain(m_old.chains, m_new.chains, c0, c1);
	m_old.chains = c0;
	m_new.chains = c1;

	mfile0::write_mfile(file_out_mfile_old, m_old);
	mfile0::write_mfile(file_out_mfile_new, m_new);

}
void Extract_muon_chain(std::vector<mfile0::M_Chain>&c) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->chain_id != 0)continue;
		ret.push_back(*itr);
	}
	std::swap(c, ret);
}

void Extract_diff_chain(std::vector<mfile0::M_Chain>&c0, std::vector<mfile0::M_Chain>&c1, std::vector<mfile0::M_Chain>&ret0, std::vector<mfile0::M_Chain>&ret1) {
	std::map<int, mfile0::M_Chain> c1_map;
	for (auto itr = c1.begin(); itr != c1.end(); itr++) {
		c1_map.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}

	for (auto itr = c0.begin(); itr != c0.end(); itr++) {
		if(c1_map.count(itr->basetracks.begin()->group_id)==0)continue;
		mfile0::M_Chain c0_chain = *itr;
		mfile0::M_Chain c1_chain = c1_map.at(itr->basetracks.begin()->group_id);
		bool flg = true;
		if (c0_chain.basetracks.rbegin()->pos != c1_chain.basetracks.rbegin()->pos)flg = false;
		//if (c0_chain.basetracks.size() != c1_chain.basetracks.size())flg = false;
		//else {
		//	//for (int i = 0; i < c0_chain.basetracks.size(); i++) {
		//	//	if (c0_chain.basetracks[i].pos != c1_chain.basetracks[i].pos)flg = false;
		//	//	if (c0_chain.basetracks[i].rawid != c1_chain.basetracks[i].rawid)flg = false;
		//	//}
		//}
		if (!flg) {
			ret0.push_back(c0_chain);
			ret1.push_back(c1_chain);
		}
	}
}

