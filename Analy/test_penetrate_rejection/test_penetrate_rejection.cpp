#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::multimap<int, mfile0::M_Chain> eventdivide(std::vector<mfile0::M_Chain>&c);
bool judge_penetrate(mfile0::M_Chain&c, int pl);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file_in_mfile file_in_mfile_muon file_out_mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_muon = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m, mu;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::vector<mfile0::M_Chain> penetrate_cand;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (itr->basetracks.begin()->flg_i[0] == 0 && itr->basetracks.rbegin()->flg_i[0] == 0) {
			penetrate_cand.push_back(*itr);
		}
	}
	m.chains = penetrate_cand;
	mfile1::write_mfile_extension(file_out_mfile, m);


	std::vector<mfile0::M_Chain> penetrate_true;
	std::vector<mfile0::M_Chain> penetrate_false;

	mfile1::read_mfile_extension(file_in_muon, mu);
	std::multimap<int, mfile0::M_Chain> partner_map = eventdivide(penetrate_cand);
	std::multimap<int, mfile0::M_Chain> muon_map = eventdivide(mu.chains);
	for (auto itr = muon_map.begin(); itr != muon_map.end(); itr = std::next(itr, muon_map.count(itr->first))) {
		printf("\r muon eventid = %d", itr->first);
		int count_partner = partner_map.count(itr->first);
		if (count_partner < 1)continue;
		std::vector<mfile0::M_Chain> partner;
		auto range = partner_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			partner.push_back(res->second);
		}

		int interaction_pl = itr->second.basetracks.rbegin()->pos / 10;
		for (auto &c : partner) {
			if (judge_penetrate(c, interaction_pl)) {
				//貫通している
				penetrate_true.push_back(c);
			}
			else {
				//貫通していない
				penetrate_false.push_back(c);
			}


		}
	}

	m.chains = penetrate_true;
	mfile1::write_mfile_extension("penetrate_true.all", m);
	m.chains = penetrate_false;
	mfile1::write_mfile_extension("penetrate_false.all", m);

}
std::multimap<int, mfile0::M_Chain> eventdivide(std::vector<mfile0::M_Chain>&c) {
	std::multimap<int, mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		ret.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}

	return ret;
}
bool judge_penetrate(mfile0::M_Chain&c, int pl) {
	//true 貫通
	//false 非貫通
	int attach_pl=-1;
	if (c.basetracks.begin()->flg_i[0] == 1 || c.basetracks.rbegin()->flg_i[0] == 1)return false;

	for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
		if (itr->flg_i[0] == 1)attach_pl = itr->pos / 10;
	}
	if (attach_pl == -1) {
		fprintf(stderr, "attach pl not found\n");
		fprintf(stderr, "eventid=%d\n", c.basetracks.begin()->group_id);
		fprintf(stderr, "chainid=%d\n", c.basetracks.begin()->flg_i[1]);
	}


	mfile0::M_Chain c0, c1;

	if (attach_pl <= pl) {
		//forward
		//c0:上流(残すほう)
		//c1:下流流(消す)
		for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
			if (itr->pos / 10 <= attach_pl) {
				c0.basetracks.push_back(*itr);
			}
			else {
				c1.basetracks.push_back(*itr);
			}
			matrix_3D::vector_3D pos0, pos1, dir0, dir1;
			pos0.x = c0.basetracks.rbegin()->x;
			pos0.y = c0.basetracks.rbegin()->y;
			pos0.z = c0.basetracks.rbegin()->z;
			dir0.x = c0.basetracks.rbegin()->ax;
			dir0.y = c0.basetracks.rbegin()->ay;
			dir0.z = 1;
			pos1.x = c1.basetracks.begin()->x;
			pos1.y = c1.basetracks.begin()->y;
			pos1.z = c1.basetracks.begin()->z;
			dir1.x = c1.basetracks.begin()->ax;
			dir1.y = c1.basetracks.begin()->ay;
			dir1.z = 1;
			double extra[2], z_range[2];
			z_range[0] = pos0.z;
			z_range[1] = pos1.z;
			double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
			double dal, dar, angle;
			angle = sqrt(dir0.x*dir0.x + dir0.y*dir0.y);
			if (angle < 0.01) {
				dal = dir1.x - dir0.x;
				dar = dir1.y - dir0.y;
			}
			else {
				dal = ((dir1.x - dir0.x)*dir0.y - (dir1.y - dir0.y)*dir0.x) / angle;
				dar = ((dir1.x - dir0.x)*dir0.x + (dir1.y - dir0.y)*dir0.y) / angle;
			}
			if (md > 100)return false;
			if (dal > 0.1)return false;
		}

	}
	else {
		//backward
		//c1:上流(消す)
		//c0:下流流(残すほう)
		for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
			if (itr->pos / 10 < attach_pl) {
				c0.basetracks.push_back(*itr);
			}
			else {
				c1.basetracks.push_back(*itr);
			}
		}
		matrix_3D::vector_3D pos0, pos1, dir0, dir1;
		pos0.x = c1.basetracks.begin()->x;
		pos0.y = c1.basetracks.begin()->y;
		pos0.z = c1.basetracks.begin()->z;
		dir0.x = c1.basetracks.begin()->ax;
		dir0.y = c1.basetracks.begin()->ay;
		dir0.z = 1;

		pos1.x = c0.basetracks.rbegin()->x;
		pos1.y = c0.basetracks.rbegin()->y;
		pos1.z = c0.basetracks.rbegin()->z;
		dir1.x = c0.basetracks.rbegin()->ax;
		dir1.y = c0.basetracks.rbegin()->ay;
		dir1.z = 1;
		double extra[2], z_range[2];
		z_range[0] = pos0.z;
		z_range[1] = pos1.z;
		double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		double dal, dar, angle;
		angle = sqrt(dir0.x*dir0.x + dir0.y*dir0.y);
		if (angle < 0.01) {
			dal = dir1.x - dir0.x;
			dar = dir1.y - dir0.y;
		}
		else {
			dal = ((dir1.x - dir0.x)*dir0.y - (dir1.y - dir0.y)*dir0.x) / angle;
			dar = ((dir1.x - dir0.x)*dir0.x + (dir1.y - dir0.y)*dir0.y) / angle;
		}
		if (md > 100)return false;
		if (dal > 0.1)return false;


	}

	return true;
}
