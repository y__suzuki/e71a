//#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <set>
#include<vector>

std::vector<uint64_t> nseg_selection(mfile1::MFile &m, std::vector<uint64_t> &all, int threshold);
mfile1::MFile mfile_selection(mfile1::MFile &m, std::vector<uint64_t> &all);

void Mfile_Area(mfile1::MFile&m, std::map<int, std::tuple<double, double, double, double>>&area_range, std::map<int, double> &z_map);
void StoptrackSelection(mfile1::MFile&m, std::map<int, std::tuple<double, double, double, double>>&area_range, std::map<int, double> &z_map, std::vector<uint64_t> &all, std::vector<uint64_t> &sel);

mfile1::MFile mfile_selection(mfile1::MFile &m, std::vector<uint64_t> &all);
std::vector<uint64_t> nseg_selection(mfile1::MFile &m, std::vector<uint64_t> &all, int threshold);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage mfile mfile-out(bin) mfile-out(txt)\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile_bin = argv[2];
	std::string file_out_mfile_txt = argv[3];

	mfile1::MFile m;
	mfile1::read_mfile(file_in_mfile, m);
	std::map<int, std::tuple<double, double, double, double>>area_range;
	std::map<int, double> z_map;
	Mfile_Area(m, area_range,z_map);

	std::vector<uint64_t> all, sel;
	all.reserve(m.chains.size());
	for (uint64_t i = 0; i < m.chains.size(); i++) {
		all.push_back(i);
	}
	all = nseg_selection(m, all, 4);
	StoptrackSelection(m, area_range, z_map, all, sel);
	m = mfile_selection(m, sel);

	mfile1::write_mfile(file_out_mfile_bin, m);

	mfile0::Mfile m_out;
	mfile1::converter(m, m_out);

	mfile0::write_mfile(file_out_mfile_txt, m_out);

}

void Mfile_Area(mfile1::MFile&m,std::map<int, std::tuple<double, double, double, double>>&area_range,std::map<int,double> &z_map) {
	for (auto itr = m.all_basetracks.begin(); itr != m.all_basetracks.end(); itr++) {
		for (auto itr2 = itr->begin(); itr2 != itr->end(); itr2++) {
			auto res = area_range.insert(std::make_pair(itr2->pos / 10, std::make_tuple(itr2->x, itr2->x, itr2->y, itr2->y)));
			if (!res.second) {
				std::get<0>(res.first->second) = std::min(std::get<0>(res.first->second), itr2->x);
				std::get<1>(res.first->second) = std::max(std::get<1>(res.first->second), itr2->x);
				std::get<2>(res.first->second) = std::min(std::get<2>(res.first->second), itr2->y);
				std::get<3>(res.first->second) = std::max(std::get<3>(res.first->second), itr2->y);
			}
			auto res2 = z_map.insert(std::make_pair(itr2->pos / 10, itr2->z));
		}
	}
	//for (auto itr = area_range.begin(); itr != area_range.end(); itr++) {
	//	printf("%3d %.1lf %.1lf %.1lf %.1lf\n", itr->first, std::get<0>(itr->second), std::get<1>(itr->second), std::get<2>(itr->second), std::get<3>(itr->second));

	//}
}

void StoptrackSelection(mfile1::MFile&m, std::map<int, std::tuple<double, double, double, double>>&area_range, std::map<int, double> &z_map,std::vector<uint64_t> &all, std::vector<uint64_t> &sel) {
	mfile1::MFileChain1 chain;
	std::vector<mfile1::MFileBase1> base;
	mfile1::MFileBase1 base_up, base_down;
	double dz, ex_x, ex_y;
	bool flg;
	std::tuple<double, double, double, double> up_range, down_range;
	for (auto num = all.begin(); num != all.end(); num++) {
		base_up = *(m.all_basetracks[*num].rbegin());
		base_down = *(m.all_basetracks[*num].begin());
		//上流veto pl>133-3
		if (base_up.pos / 10 > area_range.rbegin()->first - 3)continue;
		//下流veto pl<4+3
		if (base_down.pos / 10 < area_range.begin()->first + 3)continue;
		//上流edge付近ならcut
		flg = false;
		for (int ex_pl = 0; ex_pl <= 3; ex_pl++) {
			//ex_pl分上流に外挿
			if (z_map.count(base_up.pos / 10 + ex_pl) + z_map.count(base_up.pos / 10) != 2)continue;
			up_range = area_range.at(base_up.pos / 10 + ex_pl);
			dz = z_map.at(base_up.pos / 10 + ex_pl) - z_map.at(base_up.pos / 10);
			ex_x = base_up.ax*dz + base_up.x;
			ex_y = base_up.ay*dz + base_up.y;
			if (fabs(std::get<0>(up_range) - ex_x) < 5000)flg = true;
			if (fabs(std::get<1>(up_range) - ex_x) < 5000)flg = true;
			if (fabs(std::get<2>(up_range) - ex_y) < 5000)flg = true;
			if (fabs(std::get<3>(up_range) - ex_y) < 5000)flg = true;
		}
		if (flg)continue;
		for (int ex_pl = 0; ex_pl <= 3; ex_pl++) {
			//ex_pl分下流に外挿
			if (z_map.count(base_down.pos / 10 - ex_pl) + z_map.count(base_down.pos / 10) != 2)continue;
			down_range = area_range.at(base_down.pos / 10 - ex_pl);
			dz = z_map.at(base_down.pos / 10 - ex_pl) - z_map.at(base_down.pos / 10);
			ex_x = base_down.ax*dz + base_down.x;
			ex_y = base_down.ay*dz + base_down.y;
			if (fabs(std::get<0>(down_range) - ex_x) < 5000)flg = true;
			if (fabs(std::get<1>(down_range) - ex_x) < 5000)flg = true;
			if (fabs(std::get<2>(down_range) - ex_y) < 5000)flg = true;
			if (fabs(std::get<3>(down_range) - ex_y) < 5000)flg = true;
		}
		if (flg)continue;
		sel.push_back(*num);
	}

	printf("in ECC cut %lld --> %lld(%4.1lf%%)\n", all.size(), sel.size(), sel.size()*100. / all.size());
	return;

}
std::vector<uint64_t> nseg_selection(mfile1::MFile &m, std::vector<uint64_t> &all, int threshold) {
	std::vector<uint64_t> ret;
	uint64_t all_size = all.size();
	ret.reserve(all_size);

	for (auto c : all) {
		if (m.chains[c].nseg >= threshold) {
			ret.push_back(c);
		}
	}
	printf("nseg >= %d: %lld --> %lld(%4.1lf%%)\n", threshold, all.size(), ret.size(), ret.size()*100. / all.size());
	return ret;
}

mfile1::MFile mfile_selection(mfile1::MFile &m, std::vector<uint64_t> &all) {
	mfile1::MFile ret;
	ret.header = m.header;
	ret.info_header = m.info_header;
	for (auto c : all) {
		ret.chains.push_back(m.chains[c]);
		std::vector<mfile1::MFileBase1> base;
		for (auto b : m.all_basetracks[c]) {
			base.push_back(b);
		}
		ret.all_basetracks.push_back(base);
	}
	uint64_t N_base = 0;
	uint64_t N_chain = 0;
	for (auto c : ret.chains) {
		N_chain++;
		N_base += c.nseg;
	}
	ret.info_header.Nbasetrack = N_base;
	ret.info_header.Nchain = N_chain;

	return ret;
}

