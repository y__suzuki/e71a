#include <filesystem>
#include <fstream>
#include <map>
#include <sstream>
#include <picojson.h>
#include <set>
class AlignParameter {
public:
	double a, b, c, d, p, q;
};

class GrainInf {
public:
	int layer, pixelnum,  brightness_sum9;
	double x, y, z;
};
bool sort_z(const double &left, const double &right) {
	return left < right;
}
AlignParameter read_param(std::string filepath, int pl, int eventid);
std::vector<GrainInf> read_grain(std::string filepath, int pl, int eventid, int face, int ix, int iy);
void grain_inf_trans(std::vector<GrainInf> &gr, AlignParameter &param);
void output_grain(std::string filename, std::vector<GrainInf> &grain);
void output_grain(std::string filename, std::map<std::tuple<int, int, int>, std::vector<GrainInf>> &grain_all, int face);
void grain_difference(std::vector<GrainInf > &grain0, std::vector<GrainInf>  &grain1,std::string filename);

std::tuple<double, double, double> Calc_position_difference(std::vector<GrainInf > &grain0, std::vector<GrainInf>  &grain1);
std::tuple<double, double, double> Calc_position_difference_id(std::tuple<int, int, int>&viewid, std::map<std::tuple<int, int, int>, std::vector<GrainInf>> &grain_all);
void output_grain_multiview(std::tuple<int, int, int>&viewid, std::map<std::tuple<int, int, int>, std::vector<GrainInf>> &grain_all);
void grain_corrdinate_trans(int face, std::map<std::tuple<int, int, int>, std::vector<GrainInf>> &grain_all);
void GaussJorden_25(double in[25][25], double b[25], double c[25]);
void Set_z_val(std::map<std::tuple<int, int, int>, std::vector<GrainInf>> &grain_all);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "prg file-path-image event pl output-file-face1 output-file-face2\n");
		exit(1);
	}
	std::string file_path = argv[1];
	int eventid = std::stoi(argv[2]);
	int pl = std::stoi(argv[3]);
	std::string file_out_txt1 = argv[4];
	std::string file_out_txt2 = argv[5];

	AlignParameter param = read_param(file_path, pl, eventid);

	std::map<std::tuple<int, int, int>, std::vector<GrainInf>> grain_all;
	int read_grain_count = 0;
	for (int face = 1; face <= 2; face++) {
		for (int ix = 0; ix < 5; ix++) {
			for (int iy = 0; iy < 5; iy++) {
				printf("\r read grain inf %3d",read_grain_count);
				read_grain_count++;
				std::vector<GrainInf> gr = read_grain(file_path, pl, eventid, face,ix,iy);
				//printf("\n%d %d %d num %d\n", face, ix, iy, gr.size());
				grain_inf_trans(gr, param);
				grain_all.insert(std::make_pair(std::make_tuple(face, ix, iy), gr));
			}
		}
	}
	printf("\r read grain inf %3d\n", read_grain_count);

	Set_z_val(grain_all);
	/*
	std::set<std::pair<std::tuple<int, int, int>, std::tuple<int, int, int>>> finish;
	for (int face = 1; face <= 2; face++) {
		for (int ix = 0; ix < 5; ix++) {
			for (int iy = 0; iy < 5; iy++) {
				for (int iix = -1; iix <= 1; iix++) {
					for (int iiy = -1; iiy <= 1; iiy++) {

						std::tuple<int, int, int> id0, id1;
						if (ix < ix + iix) {
							id0 = std::make_tuple(face, ix, iy);
							id1 = std::make_tuple(face, ix + iix, iy + iiy);
						}
						else if (ix == ix + iix) {
							id0 = std::make_tuple(face, ix, std::min(iy, iy + iiy));
							id1 = std::make_tuple(face, ix, std::max(iy, iy + iiy));
						}
						else {
							id0 = std::make_tuple(face, ix + iix, iy + iiy);
							id1 = std::make_tuple(face, ix, iy);
						}
						if (id0 == id1)continue;
						auto res = finish.insert(std::make_pair(id0, id1));
						if (!res.second)continue;
						std::stringstream file_out_diff;
						file_out_diff << "out_pair_" << std::get<0>(id0) << "_"
							<< std::get<1>(id0) << std::get<2>(id0) << "_"
							<< std::get<1>(id1) << std::get<2>(id1) << ".txt";
						if (grain_all.count(id0) == 0)continue;
						if (grain_all.count(id1) == 0)continue;
						grain_difference(grain_all.at(id0), grain_all.at(id1), file_out_diff.str());

					}
				}
			}
		}
	}
	*/


	grain_corrdinate_trans(1, grain_all);
	grain_corrdinate_trans(2, grain_all);

	output_grain(file_out_txt1, grain_all, 1);
	output_grain(file_out_txt2, grain_all, 2);


}
AlignParameter read_param(std::string filepath, int pl, int eventid) {
	std::stringstream filename;
	filename << filepath << "\\"
		<< std::setw(3) << std::setfill('0') << pl << "_"
		<< std::setw(5) << std::setfill('0') << eventid << "_param.txt";
	if (!std::filesystem::exists(filename.str())) {
		fprintf(stderr, "file %s not found\n", filename.str().c_str());
		exit(1);
	}
	std::ifstream ifs(filename.str());
	AlignParameter param;
	ifs >> param.a >> param.b >> param.c >> param.d >> param.p >> param.q;
	return param;
}
std::vector<GrainInf> read_grain(std::string filepath, int pl, int eventid,int face, int ix, int iy) {
	
	std::stringstream folder_path;
	folder_path << filepath << "\\ev"
		<< std::setw(5) << std::setfill('0') << eventid << "\\PL"
		<< std::setw(3) << std::setfill('0') << pl << "\\face"
		<< std::setw(1) << face << "_" << ix << "_" << iy;

	std::stringstream file_param;
	file_param << folder_path.str() << "\\image.json";

	// JSONデータの読み込み。
	std::ifstream ifs(file_param.str(), std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << file_param.str()<< std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}
	picojson::object& obj = v.get<picojson::object>();
	
	picojson::array &images=obj["Images"].get<picojson::array>();
	std::map<int, std::tuple<double, double, double>> param;
	int layer;
	double x, y, z;
	std::string image_path;
	for (auto itr = images.begin(); itr != images.end(); itr++) {
		picojson::object& im_param = itr->get<picojson::object>();
		image_path = im_param["Path"].get<std::string>();
		layer = std::stoi(image_path.substr(0, image_path.size() - 4));
		x = im_param["x"].get<double>();
		y = im_param["y"].get<double>();
		z = im_param["z"].get<double>();
		param.insert(std::make_pair(layer, std::make_tuple(x, y, z)));
		//printf("%s %d %g %g %g\n", image_path.c_str(),layer, x, y, z);
	}
	picojson::object& img_inf = obj["ImageType"].get<picojson::object>();

	int  height = int(img_inf["Height"].get<double>());
	int  width = int(img_inf["Width"].get<double>());
	//grain読み込み
	std::stringstream file_grain;
	file_grain << folder_path.str() << "\\grain_3d.txt";

	std::vector<GrainInf> ret;
	GrainInf grain;
	if (!std::filesystem::exists(file_grain.str())) {
		fprintf(stderr, "file %s not found\n", file_grain.str().c_str());
		exit(1);
	}
	ifs.open(file_grain.str());
	
	//pixel --> um
	double pixel_size = 0.149;
	//1layer --> xum
	double layer_z_trans = 1;
	int count = 0;
	while (ifs >> grain.layer >> grain.x >> grain.y >> grain.z >> grain.pixelnum >> grain.brightness_sum9) {
		//if (count % 1000 == 0) {
		//	printf("\r grain inf read %d", count);
		//}
		count++;
		
		auto res = param.find(grain.layer);
		if (res == param.end()) {
			printf("Layer %d not found\n", grain.layer);
			continue;
		}
		//y軸の反転+0始まり
		grain.y = grain.y*-1 + (height - 1);
		//stage x mm
		grain.x = std::get<0>(res->second) + (grain.x - width / 2)*pixel_size / 1000;
		grain.y = std::get<1>(res->second) + (grain.y - height / 2)*pixel_size / 1000;
		//z軸の反転(ECC座標と駆動系)
		grain.z = -1 * std::get<2>(res->second) + (grain.z - grain.layer)*layer_z_trans / 1000;

		ret.push_back(grain);
	}
	//printf("\r grain inf read %d\n", count);

	return ret;
}
void grain_inf_trans(std::vector<GrainInf> &gr, AlignParameter &param) {
	double tmp_x, tmp_y;
	double factor = 1 / (param.a*param.d - param.b*param.c);

	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		tmp_x = itr->x - param.p;
		tmp_y = itr->y - param.q;

		itr->x = factor * (param.d*tmp_x - param.b*tmp_y);
		itr->y = factor * (-1*param.c*tmp_x + param.a*tmp_y);
		itr->z = itr->z * 1000;
	}
}

void output_grain(std::string filename, std::vector<GrainInf> &grain) {
	std::ofstream ofs(filename);
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr->layer << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(10) << std::setprecision(1) << itr->z << " "
			<< std::setw(10) << std::setprecision(0) << itr->pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->brightness_sum9 << std::endl;
	}



}
void output_grain(std::string filename, std::map<std::tuple<int, int, int>, std::vector<GrainInf>> &grain_all, int face) {
	std::ofstream ofs(filename);

	for (auto itr2 = grain_all.begin(); itr2 != grain_all.end(); itr2++) {


		if (std::get<0>(itr2->first) != face)continue;
		//if (std::get<1>(itr2->first) != 2)continue;
		//if (std::get<2>(itr2->first) != 1 && std::get<2>(itr2->first) != 2)continue;

		for (auto itr = itr2->second.begin(); itr != itr2->second.end(); itr++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << std::get<0>(itr2->first) << " "
				<< std::setw(3) << std::setprecision(0) << std::get<1>(itr2->first) << " "
				<< std::setw(3) << std::setprecision(0) << std::get<2>(itr2->first) << " "
				<< std::setw(10) << std::setprecision(0) << itr->layer << " "
				<< std::setw(10) << std::setprecision(1) << itr->x << " "
				<< std::setw(10) << std::setprecision(1) << itr->y << " "
				<< std::setw(10) << std::setprecision(1) << itr->z << " "
				<< std::setw(10) << std::setprecision(0) << itr->pixelnum << " "
				<< std::setw(10) << std::setprecision(0) << itr->brightness_sum9 << std::endl;
		}
	}
}

void output_grain_multiview( std::tuple<int, int, int>&viewid, std::map<std::tuple<int, int, int>, std::vector<GrainInf>> &grain_all) {
	
	std::tuple<int, int, int> bottom = viewid;
	std::tuple<int, int, int> left = viewid;
	std::tuple<int, int, int> right = viewid;
	std::tuple<int, int, int> top = viewid;
	std::get<2>(bottom) = std::get<2>(bottom) - 1;
	std::get<2>(top) = std::get<2>(top) + 1;
	std::get<1>(left) = std::get<1>(left) - 1;
	std::get<1>(right) = std::get<1>(right) + 1;

	std::tuple<double, double, double> d_bottom = Calc_position_difference(
		grain_all.at(viewid), grain_all.at(bottom));
	std::tuple<double, double, double> d_top = Calc_position_difference(
		grain_all.at(viewid), grain_all.at(top));
	std::tuple<double, double, double> d_left = Calc_position_difference(
		grain_all.at(viewid), grain_all.at(left));
	std::tuple<double, double, double> d_right = Calc_position_difference(
		grain_all.at(viewid), grain_all.at(right));

	printf("%d %d %d %d %d %.1lf %.1lf %.1lf\n",
		std::get<0>(viewid),
		std::get<1>(viewid),
		std::get<2>(viewid),
		std::get<1>(bottom),
		std::get<2>(bottom),
		std::get<0>(d_bottom),
		std::get<1>(d_bottom),
		std::get<2>(d_bottom)
	);
	printf("%d %d %d %d %d %.1lf %.1lf %.1lf\n",
		std::get<0>(viewid),
		std::get<1>(viewid),
		std::get<2>(viewid),
		std::get<1>(top),
		std::get<2>(top),
		std::get<0>(d_top),
		std::get<1>(d_top),
		std::get<2>(d_top)
	);
	printf("%d %d %d %d %d %.1lf %.1lf %.1lf\n",
		std::get<0>(viewid),
		std::get<1>(viewid),
		std::get<2>(viewid),
		std::get<1>(left),
		std::get<2>(left),
		std::get<0>(d_left),
		std::get<1>(d_left),
		std::get<2>(d_left)
	);
	printf("%d %d %d %d %d %.1lf %.1lf %.1lf\n",
		std::get<0>(viewid),
		std::get<1>(viewid),
		std::get<2>(viewid),
		std::get<1>(right),
		std::get<2>(right),
		std::get<0>(d_right),
		std::get<1>(d_right),
		std::get<2>(d_right)
	);

}

void grain_difference(std::vector<GrainInf > &grain0, std::vector<GrainInf>  &grain1,std::string filename) {

	int64_t all = grain0.size()*grain1.size();
	int64_t count = 0;
	std::vector < std::pair<GrainInf, GrainInf>>grain_pair;
	for (auto itr = grain0.begin(); itr != grain0.end(); itr++) {
		if (itr->pixelnum < 10)continue;

		for (auto itr2 = grain1.begin(); itr2 != grain1.end(); itr2++) {
			if (count % 100000 == 0) {
				printf("\r make pair %lld/%lld", count, all);
			}
			count++;
			if (itr2->pixelnum < 10)continue;

			if (fabs(itr->x - itr2->x) > 30)continue;
			if (fabs(itr->y - itr2->y) > 30)continue;
			if (fabs(itr->z - itr2->z) > 10)continue;
				grain_pair.push_back(std::make_pair(*itr, *itr2));
		}
	}
	if (grain_pair.size() < 10)return;

	std::ofstream ofs(filename);
	for (auto itr = grain_pair.begin(); itr != grain_pair.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(2) << itr->first.x << " "
			<< std::setw(10) << std::setprecision(2) << itr->first.y << " "
			<< std::setw(10) << std::setprecision(2) << itr->first.z << " "
			<< std::setw(10) << std::setprecision(2) << itr->second.x << " "
			<< std::setw(10) << std::setprecision(2) << itr->second.y << " "
			<< std::setw(10) << std::setprecision(2) << itr->second.z << std::endl;
	}
}

std::tuple<double, double, double> Calc_position_difference_id(std::tuple<int, int, int>&viewid, std::map<std::tuple<int, int, int>, std::vector<GrainInf>> &grain_all) {
	std::tuple<double, double, double> ret = std::make_tuple(0, 0, 0);
	std::tuple<double, double, double> diff = std::make_tuple(0, 0, 0);
	std::tuple<int, int, int> target_view = std::make_tuple(0, 0, 0);
	int count = 0;
	printf("\n");
	for (int ix = -1; ix <= 1; ix++) {
		for (int iy = -1; iy <= 1; iy++) {
			if (fabs(ix) + fabs(iy) != 1)continue;
			std::get<0>(target_view) = std::get<0>(viewid);
			std::get<1>(target_view) = std::get<1>(viewid) + ix;
			std::get<2>(target_view) = std::get<2>(viewid) + iy;
			if (grain_all.count(target_view) == 0)continue;
			diff = Calc_position_difference(grain_all.at(target_view), grain_all.at(viewid));
			//printf("%d%d %d%d %.1lf %.1lf %.1lf\n",
			//	std::get<1>(viewid), std::get<1>(target_view),
			//	std::get<2>(viewid), std::get<2>(target_view),
			//	std::get<0>(diff), std::get<1>(diff), std::get<2>(diff)
			//);

			std::get<0>(ret) += std::get<0>(diff);
			std::get<1>(ret) += std::get<1>(diff);
			std::get<2>(ret) += std::get<2>(diff);
			count++;
		}
	}
	if (count == 0)return ret;
	std::get<0>(ret) = std::get<0>(ret) / count;
	std::get<1>(ret) = std::get<1>(ret) / count;
	std::get<2>(ret) = std::get<2>(ret) / count;

	printf("%d %d %d %.1lf %.1lf %.1lf\n",
		std::get<0>(viewid), std::get<1>(viewid), std::get<2>(viewid),
		std::get<0>(ret), std::get<1>(ret), std::get<2>(ret));

	return ret;
}
std::tuple<double, double, double> Calc_position_difference(std::vector<GrainInf > &grain0, std::vector<GrainInf>  &grain1) {
	///////////////////////////////
	//重複部分だけ抽出
	double mergin = 50;
	std::vector<GrainInf > grain0_cut, grain1_cut;

	double xmin[2], ymin[2], xmax[2], ymax[2],zmin[2],zmax[2];
	std::vector<double>z0, z1;
	for (auto itr = grain0.begin(); itr != grain0.end(); itr++) {
		if (itr == grain0.begin()) {
			xmin[0] = itr->x;
			xmax[0] = itr->x;
			ymin[0] = itr->y;
			ymax[0] = itr->y;
			zmin[0] = itr->z;
			zmax[0] = itr->z;
		}
		xmin[0] = std::min(itr->x, xmin[0]);
		xmax[0] = std::max(itr->x, xmax[0]);
		ymin[0] = std::min(itr->y, ymin[0]);
		ymax[0] = std::max(itr->y, ymax[0]);
		zmin[0] = std::min(itr->z, zmin[0]);
		zmax[0] = std::max(itr->z, zmax[0]);
		z0.push_back(itr->z);
	}
	for (auto itr = grain1.begin(); itr != grain1.end(); itr++) {
		if (itr == grain1.begin()) {
			xmin[1] = itr->x;
			xmax[1] = itr->x;
			ymin[1] = itr->y;
			ymax[1] = itr->y;
			zmin[1] = itr->z;
			zmax[1] = itr->z;
		}
		xmin[1] = std::min(itr->x, xmin[1]);
		xmax[1] = std::max(itr->x, xmax[1]);
		ymin[1] = std::min(itr->y, ymin[1]);
		ymax[1] = std::max(itr->y, ymax[1]);
		zmin[1] = std::min(itr->z, zmin[1]);
		zmax[1] = std::max(itr->z, zmax[1]);
		z1.push_back(itr->z);
	}
	sort(z0.begin(), z0.end(), sort_z);
	sort(z1.begin(), z1.end(), sort_z);
	zmin[0] = z0[int(0.1*z0.size())];
	zmax[0] = z0[int(0.9*z0.size())];
	zmin[1] = z1[int(0.1*z1.size())];
	zmax[1] = z1[int(0.9*z1.size())];
	double overlap_range[4];
	//[xmin,xmax,ymin,ymax]
	overlap_range[0] = std::max(xmin[0], xmin[1]);
	overlap_range[1] = std::min(xmax[0], xmax[1]);
	overlap_range[2] = std::max(ymin[0], ymin[1]);
	overlap_range[3] = std::min(ymax[0], ymax[1]);

	for (auto itr = grain0.begin(); itr != grain0.end(); itr++) {
		//if (itr->pixelnum < 10)continue;
		if (itr->x < overlap_range[0] - mergin)continue;
		if (itr->x > overlap_range[1] + mergin)continue;
		if (itr->y < overlap_range[2] - mergin)continue;
		if (itr->y > overlap_range[3] + mergin)continue;
		grain0_cut.push_back(*itr);
	}
	for (auto itr = grain1.begin(); itr != grain1.end(); itr++) {
		//if (itr->pixelnum < 10)continue;
		if (itr->x < overlap_range[0] - mergin)continue;
		if (itr->x > overlap_range[1] + mergin)continue;
		if (itr->y < overlap_range[2] - mergin)continue;
		if (itr->y > overlap_range[3] + mergin)continue;
		grain1_cut.push_back(*itr);
	}

	/////////////////////////////////////



	//////////////////////////////
	//一致grainを抽出
	std::vector < std::pair<GrainInf, GrainInf>>grain_pair;
	double x_center, y_center, z_center;
	double binx , biny, binz;
	double dx_max, dy_max, dz_max,dz_ini;
	binx = 0.1;
	biny = 0.1; 
	binz = 0.1;
	dx_max = 30;
	dy_max = 30;
	dz_max = 10;
	dz_ini = (zmin[0] + zmax[0]) / 2 - (zmin[1] + zmax[1]) / 2;
	int64_t all = grain0_cut.size()*grain1_cut.size();
	int64_t count = 0;
	for (auto itr = grain0_cut.begin(); itr != grain0_cut.end(); itr++) {
		for (auto itr2 = grain1_cut.begin(); itr2 != grain1_cut.end(); itr2++) {
			if (fabs(itr->x - itr2->x) > dx_max)continue;
			if (fabs(itr->y - itr2->y) > dy_max)continue;
			if (fabs(itr->z - (itr2->z+dz_ini)) > dz_max)continue;
			grain_pair.push_back(std::make_pair(*itr, *itr2));
		}
	}
	//////////////////////////////////////////////


	//x-yで2d hist
	std::map<std::pair<int, int>, int> hist_2d_xy, hist_2d_xz, hist_2d_yz;
	int ix, iy;
	for (auto itr = grain_pair.begin(); itr != grain_pair.end(); itr++) {
		ix = (itr->first.x - itr->second.x + dx_max) / binx;
		iy = (itr->first.y - itr->second.y + dy_max) / biny;
		auto res = hist_2d_xy.insert(std::make_pair(std::make_pair(ix, iy), 1));
		if (!res.second)res.first->second++;
	}
	//peak探索
	int max = 0;
	for (auto itr = hist_2d_xy.begin(); itr != hist_2d_xy.end(); itr++) {
		if (itr == hist_2d_xy.begin() || max < itr->second) {
			max = itr->second;
			x_center = itr->first.first*binx - dx_max;
			y_center = itr->first.second*biny - dy_max;
		}
	}
	//printf("max %d x center %.1lf y center %.1lf\n", max,x_center,y_center);

	//x-z,y-z
	for (auto itr = grain_pair.begin(); itr != grain_pair.end(); itr++) {
		if (fabs(itr->first.x - itr->second.x - x_center) > 4)continue;
		if (fabs(itr->first.y - itr->second.y - y_center) > 4)continue;
		ix = (itr->first.x - itr->second.x + dx_max) / binx;
		iy = (itr->first.z - itr->second.z + dz_max) / binz;
		auto res = hist_2d_xz.insert(std::make_pair(std::make_pair(ix, iy), 1));
		if (!res.second)res.first->second++;
		ix = (itr->first.y - itr->second.y + dy_max) / biny;
		res = hist_2d_yz.insert(std::make_pair(std::make_pair(ix, iy), 1));
		if (!res.second)res.first->second++;
	}

	//peak探索
	double z_center0, z_center1;
	for (auto itr = hist_2d_xz.begin(); itr != hist_2d_xz.end(); itr++) {
		if (itr == hist_2d_xz.begin() || max < itr->second) {
			max = itr->second;
			z_center0 = itr->first.second*binz - dz_max;
		}
	}
	//printf("max %d z center %.1lf\n", max,z_center0);

	for (auto itr = hist_2d_yz.begin(); itr != hist_2d_yz.end(); itr++) {
		if (itr == hist_2d_yz.begin() || max < itr->second) {
			max = itr->second;
			z_center1 = itr->first.second*binz - dz_max;
		}
	}

	//printf("max %d z center %.1lf\n", max, z_center1);

	z_center = (z_center0 + z_center1) / 2;
	//printf("result dx=%g dy=%g dz=%g\n", x_center, y_center, z_center);

	return std::make_tuple(x_center, y_center, z_center);
}

void Set_z_val(std::map<std::tuple<int, int, int>, std::vector<GrainInf>> &grain_all) {

	double z1_max;
	//z1_max=0になるように?
	std::tuple<int, int, int> id;
	for (int ix = 0; ix < 5; ix++) {
		for (int iy = 0; iy < 5; iy++) {
			std::get<0>(id) = 1;
			std::get<1>(id) = ix;
			std::get<2>(id) = iy;
			if (grain_all.count(id) == 0)continue;
			auto res = grain_all.find(id);
			for (auto itr = res->second.begin(); itr != res->second.end(); itr++) {
				if (itr == res->second.begin())z1_max = itr->z;
				z1_max = std::max(z1_max, itr->z);
			}
			for (auto itr = res->second.begin(); itr != res->second.end(); itr++) {
				itr->z -= z1_max;
			}
			std::get<0>(id) = 2;
			if (grain_all.count(id) == 0)continue;
			res = grain_all.find(id);
			for (auto itr = res->second.begin(); itr != res->second.end(); itr++) {
				itr->z -= z1_max;
			}
		}
	}



}

void grain_corrdinate_trans(int face, std::map<std::tuple<int, int, int>, std::vector<GrainInf>> &grain_all) {
	const int ELEMENT = 5 * 5;
	//ずれ量のbuffer 初期化
	double diff[3][ELEMENT][ELEMENT];
	printf("face %d\n", face);

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < ELEMENT; j++) {
			for (int k = 0; k < ELEMENT; k++) {
				diff[i][j][k] = 0;
			}
		}
	}

	int id;
	std::tuple<int, int, int> id_tuple;
	//idの振り直し
	std::map<int, std::vector<GrainInf>> grain_id;
	for (int ix = 0; ix < 5; ix++) {
		for (int iy = 0; iy < 5; iy++) {
			id = ix + iy * 5;
			//printf("\r shift calc %d-%d-%d", face, ix, iy);
			std::get<0>(id_tuple) = face;
			std::get<1>(id_tuple) = ix;
			std::get<2>(id_tuple) = iy;
			if (grain_all.count(id_tuple) == 0)continue;
			grain_id.insert(std::make_pair(id, grain_all.at(id_tuple)));
		}
	}
	//各視野のずれ量の計算
	int ix[2], iy[2];
	std::vector<GrainInf> g0, g1;
	std::tuple<double, double, double> diff_two_view;
	printf("calc difference ");

	for (int i = 0; i < ELEMENT; i++) {
		if (grain_id.count(i) == 0)continue;
		g0 = grain_id.at(i);
		ix[0] = i % 5;
		iy[0] = i / 5;

		for (int j = i + 1; j < ELEMENT; j++) {
			if (grain_id.count(j) == 0)continue;
			g1 = grain_id.at(j);
			ix[1] = j % 5;
			iy[1] = j / 5;
			if (abs(ix[0] - ix[1]) + abs(iy[0] - iy[1]) != 1)continue;
			//g1-g0
			//printf("view (%1d,%1d) (%1d,%1d)\n", ix[0], iy[0], ix[1], iy[1]);
			diff_two_view = Calc_position_difference(g1, g0);
			diff[0][i][j] = std::get<0>(diff_two_view);
			diff[1][i][j] = std::get<1>(diff_two_view);
			diff[2][i][j] = std::get<2>(diff_two_view);
		}
	}
	printf("done\n");

	//for (int i = 0; i < 3; i++) {
	//	printf("%d\n",i);
	//	for (int j = 0; j < ELEMENT; j++) {
	//		for (int k = 0; k < ELEMENT; k++) {
	//			printf("%4.1lf ", diff[i][j][k]);
	//		}
	//		printf("\n");
	//	}
	//}

	//右辺成分の計算
	double diff_calc[3][ELEMENT];
	//x,y,zの成分
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < ELEMENT; j++) {
			diff_calc[i][j] = 0;
			ix[0] = j % 5;
			iy[0] = j / 5;
			for (int iix = -1; iix <= 1; iix++) {
				for (int iiy = -1; iiy <= 1; iiy++) {
					if (ix[0] + iix < 0)continue;
					if (5 <= ix[0] + iix)continue;
					if (iy[0] + iiy < 0)continue;
					if (5 <= iy[0] + iiy)continue;
					if (abs(iix) + abs(iiy) != 1)continue;
					id = ix[0] + iix + (iy[0] + iiy) * 5;
					if (j < id)diff_calc[i][j] += diff[i][j][id];
					else if (j > id)diff_calc[i][j] -= diff[i][id][j];
				}
			}
		}
	}

	//for (int i = 0; i < 3; i++) {
	//	printf("%d\n",i);
	//	for (int j = 0; j < ELEMENT; j++) {
	//		printf("%4.1lf\n", diff_calc[i][j]);
	//	}
	//}

	//境界条件
	for (int i = 0; i < 3; i++) {
		diff_calc[i][ELEMENT - 1] = 0;
	}
	double matrix[ELEMENT][ELEMENT] = {
		{2,-1,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{-1,3,-1,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,-1,3,-1,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,-1,3,-1,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,-1,2,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{-1,0,0,0,0,3,-1,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,-1,0,0,0,-1,4,-1,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,-1,0,0,0,-1,4,-1,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,-1,0,0,0,-1,4,-1,0,0,0,-1,0,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,-1,0,0,0,-1,3,0,0,0,0,-1,0,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,-1,0,0,0,0,3,-1,0,0,0,-1,0,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,-1,0,0,0,-1,4,-1,0,0,0,-1,0,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,-1,0,0,0,-1,4,-1,0,0,0,-1,0,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,-1,0,0,0,-1,4,-1,0,0,0,-1,0,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,-1,0,0,0,-1,3,0,0,0,0,-1,0,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,3,-1,0,0,0,-1,0,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,-1,4,-1,0,0,0,-1,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,-1,4,-1,0,0,0,-1,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,-1,4,-1,0,0,0,-1,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,-1,3,0,0,0,0,-1},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,2,-1,0,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,-1,3,-1,0,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,-1,3,-1,0},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,-1,3,-1},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
	};
	double shift[3][ELEMENT];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < ELEMENT; j++) {
			shift[i][j] = 0;
		}
	}
	printf("calc equation ");
	for (int i = 0; i < 3; i++) {
		GaussJorden_25(matrix, diff_calc[i], shift[i]);
	}
	printf("done\n");

	//for (int i = 0; i < ELEMENT; i++) {
	//	printf("view %2d x:%4.1lf y%4.1lf z%4.1lf\n", i, shift[0][i], shift[1][i], shift[2][i]);
	//}


	for (int i = 0; i < ELEMENT; i++) {
		//printf("\r shift calc %d-%d-%d", face, ix, iy);
		std::get<0>(id_tuple) = face;
		std::get<1>(id_tuple) = i%5;
		std::get<2>(id_tuple) = i/5;
		if (grain_all.count(id_tuple) == 0)continue;
		for (auto itr = grain_all.at(id_tuple).begin(); itr != grain_all.at(id_tuple).end(); itr++) {
			itr->x += shift[0][i];
			itr->y += shift[1][i];
			itr->z += shift[2][i];
		}

	}
}

void GaussJorden_25(double in[25][25], double b[25], double c[25]) {


	double a[25][26];
	for (int i = 0; i < 25; i++) {
		for (int j = 0; j < 26; j++) {
			if (j < 25) {
				a[i][j] = in[i][j];
			}
			else {
				a[i][j] = b[i];
			}
		}
	}
	int N = 25;
	double p, d;         // ピボット係数、ピボット行ｘ係数
	double max, dummy;   // 最大絶対値、入れ替え時ダミー
	int s;

	//元の連立方程式をコンソール出力
 //  for (int i = 0; i < N; i++) {
 //  	for (int j = 0; j < N; j++)
 //  		printf("%+fx%d ", a[i][j], j + 1);
 //  	printf("= %+f\n", a[i][N]);
 //  }

	for (int k = 0; k < N; k++) {
		// 行入れ替え
		max = 0; s = k;
		for (int j = k; j < N; j++) {
			if (fabs(a[j][k]) > max) {
				max = fabs(a[j][k]);
				s = j;
			}
		}
		if (max == 0) {
			printf("解けない！");
			exit(1);
		}
		for (int j = 0; j <= N; j++) {
			dummy = a[k][j];
			a[k][j] = a[s][j];
			a[s][j] = dummy;
		}

		// ピボット係数
		p = a[k][k];

		// ピボット行を p で除算
		for (int j = k; j < N + 1; j++)
			a[k][j] /= p;

		// ピボット列の掃き出し
		for (int i = 0; i < N; i++) {
			if (i != k) {
				d = a[i][k];
				for (int j = k; j < N + 1; j++)
					a[i][j] -= d * a[k][j];
			}
		}
	}

	// 結果出力
	for (int k = 0; k < N; k++) {
		c[k] = a[k][N];
		//printf("x%d = %f\n", k + 1, a[k][N]);
	}
}

