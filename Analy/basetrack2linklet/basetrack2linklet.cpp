#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <set>
#include <list>

int main(int argc, char**argv) {
	if (argc != 8) {
		fprintf(stderr, "usage:prg input-bvxx1 pl1 input-bvxx2 pl2 corrmap-local nominal-gap output-bll\n");
		exit(1);
	}
	std::string file_in_bvxx[2], file_out_link, file_in_corr;
	int pl[2];
	double gap;
	file_in_bvxx[0] = argv[1];
	pl[0] = std::stoi(argv[2]);
	file_in_bvxx[1] = argv[3];
	pl[1] = std::stoi(argv[4]);
	file_in_corr = argv[5];
	gap = std::stod(argv[6]);
	file_out_link = argv[7];

	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corr, corr);

	std::vector<vxx::base_track_t> base[2];
	vxx::BvxxReader br;
	for (int i = 0; i < 2; i++) {
		base[i] = br.ReadAll(file_in_bvxx[i], pl[i], 0);
	}


}
void basetrack_trans(int pl0, int pl1, std::vector<vxx::base_track_t>&base, std::vector<corrmap0::Corrmap>&corr) {
	//parameter等がOKか確認
	if (!check_param(pl0, pl1, base.begin()->pl, *corr.begin())) {
		fprintf(stderr, "parameter exception\n");
		fprintf(stderr, "pl0=%03d pl1=%03d basetrack=%03d(=pl1) corrmap %03d-%03d\n", pl0, pl1, base.begin()->pl, corr.begin()->pos[0] / 10, corr.begin()->pos[1] / 10);
		exit(1);
	}




}
bool check_param(int pl0, int pl1, int base_pl, corrmap0::Corrmap corr) {
	if (pl0 != corr.pos[0] / 10)return false;
	if (pl1 != corr.pos[1] / 10)return false;
	if (base_pl != pl1)return false;

	return true;
}

void corrmap_area_inverse(std::vector<corrmap0::Corrmap>&corr) {

}