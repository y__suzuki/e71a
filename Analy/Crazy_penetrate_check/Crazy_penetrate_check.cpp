#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <omp.h>
#include <set>


std::vector<mfile0::M_Chain> penetrate_check(mfile0::M_Chain c, mfile1::MFile_minimum&m);
mfile0::M_Base base_converter(mfile1::MFileBase &base);
int use_thread(double ratio, bool output);
mfile1::MFile_minimum mfile_read(std::string file_in_mfile);


int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-mfile file-in-mfile-all out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile_sel = argv[1];
	std::string file_in_mfile_all = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile_sel, m);
	mfile1::MFile_minimum  m_all = mfile_read(file_in_mfile_all);

	mfile0::Mfile m_out;
	m_out.header = m.header;
	int count = 0, all = m.chains.size();
	for (int64_t i = 0; i < m.chains.size(); i++) {
		printf("\r now ... %5d/%5d", count, all);
		count++;

		std::vector<mfile0::M_Chain> connect_cand = penetrate_check(m.chains[i], m_all);
		m_out.chains.push_back(m.chains[i]);
		for (auto itr2 = connect_cand.begin(); itr2 != connect_cand.end(); itr2++) {
			m_out.chains.push_back(*itr2);
		}
	}

	printf("\r now ... %5d/%5d\n", count, all);

	mfile0::write_mfile(file_out_mfile, m_out);
}
mfile1::MFile_minimum mfile_read(std::string file_in_mfile) {
	std::ifstream ifs(file_in_mfile, std::ios::binary);


	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile_minimum mfile;
	//mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }
	//Mfile headerの書き込み
	//mfile info headerの書き込み

	uint64_t count = 0;
	int64_t gid = -1;
	for (int64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		//一旦chainを読む
		mfile1::MFileChain chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }

		std::vector< mfile1::MFileBase> basetracks;
		basetracks.reserve(chain.nseg);

		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			basetracks.emplace_back(base);

		}

		if (chain.nseg < 7 && (chain.pos0 / 10 != 3 || chain.pos0 / 10 != 4) && (chain.pos1 / 10 != 133 || chain.pos1 / 10 != 132))continue;
		mfile.chains.push_back(chain);
		mfile.all_basetracks.push_back(basetracks);
	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;


	int64_t NChain = 0;
	int64_t NBase = 0;
	for (int64_t i = 0; i < mfile.chains.size(); i++) {
		NChain++;
		NBase += mfile.all_basetracks[i].size();
	}
	mfile.info_header.Nbasetrack = NBase;
	mfile.info_header.Nchain = NChain;
	return mfile;
}



std::vector<mfile0::M_Chain> penetrate_check(mfile0::M_Chain c, mfile1::MFile_minimum&m) {
	int64_t gid = c.basetracks.begin()->group_id;
	std::vector<mfile0::M_Chain> ret;

	std::set<int64_t> cand;
#pragma omp parallel for num_threads(use_thread(0.8,false)) schedule(guided)
	for (int i = 0; i < c.basetracks.size(); i++) {
		matrix_3D::vector_3D pos, dir;
		int pl0;
		pl0 = c.basetracks[i].pos / 10;
		pos.x = c.basetracks[i].x;
		pos.y = c.basetracks[i].y;
		pos.z = c.basetracks[i].z;
		dir.x = c.basetracks[i].ax;
		dir.y = c.basetracks[i].ay;
		dir.z = 1;

		for (int64_t j = 0;j < m.all_basetracks.size(); j++) {
			double oa, md;
			double extra[2], z_range[2];
			int t_pl;
			matrix_3D::vector_3D t_pos, t_dir;
			t_pl = m.all_basetracks[j].begin()->pos / 10;
			if (pl0 < t_pl&&t_pl - pl0 <= 10) {
				t_pos.x = m.all_basetracks[j].begin()->x;
				t_pos.y = m.all_basetracks[j].begin()->y;
				t_pos.z = m.all_basetracks[j].begin()->z;
				t_dir.x = m.all_basetracks[j].begin()->ax;
				t_dir.y = m.all_basetracks[j].begin()->ay;
				t_dir.z = 1;
				z_range[1] = pos.z;
				z_range[0] = t_pos.z;
				oa = matrix_3D::opening_angle(dir, t_dir);
				if (oa > 0.3)continue;
				md = matrix_3D::minimum_distance(pos, t_pos, dir, t_dir, z_range, extra);
				if (md > 200)continue;
#pragma omp critical
				cand.insert(j);
			}
		}
	}

	for (auto itr = cand.begin(); itr != cand.end(); itr++) {
		mfile0::M_Chain chain;
		for (auto itr2 = m.all_basetracks[*itr].begin(); itr2 != m.all_basetracks[*itr].end(); itr2++) {
			mfile0::M_Base base = base_converter(*itr2);
			base.group_id = gid;
			chain.basetracks.push_back(base);
		}
		chain.nseg = chain.basetracks.size();
		chain.pos0 = chain.basetracks.begin()->pos;
		chain.pos1 = chain.basetracks.rbegin()->pos;
		chain.chain_id = *itr;
		ret.push_back(chain);
	}
	return ret;



}
mfile0::M_Base base_converter(mfile1::MFileBase &base) {
	mfile0::M_Base ret;
	ret.pos = base.pos;
	ret.rawid = base.rawid;
	ret.ph = base.ph;
	ret.ax = base.ax;
	ret.ay = base.ay;
	ret.x = base.x;
	ret.y = base.y;
	ret.z = base.z;
	ret.group_id = 0;
	ret.flg_d[0] = 0;
	ret.flg_d[1] = 0;
	ret.flg_i[0] = 0;
	ret.flg_i[1] = 0;
	ret.flg_i[2] = 0;
	ret.flg_i[3] = 0;
	return ret;
}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
