#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<vxx::base_track_t> cut_area(std::vector<vxx::base_track_t>&base, double &x_min, double &x_max, double &y_min, double &y_max, double cut_range);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage file-in-base pl file-inn-area file-out-base\n");
		exit(1);
	}

	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_area = argv[3];

	std::string file_out_base = argv[4];

	double x_min, x_max, y_min, y_max;
	std::ifstream ifs(file_in_area);
	ifs >> x_min >> x_max >> y_min >> y_max;
	ifs.close();
	printf("x %5.2lf mm\ny %5.2lf mm\n", (x_max - x_min) / 1000, (y_max - y_min) / 1000);

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base, pl, 0);
	base= cut_area(base, x_min, x_max, y_min, y_max, 500);

	vxx::BvxxWriter bw;
	bw.Write(file_out_base, pl, 0, base);


}
std::vector<vxx::base_track_t> cut_area(std::vector<vxx::base_track_t>&base, double &x_min, double &x_max, double &y_min, double &y_max, double cut_range) {
	std::vector<vxx::base_track_t> ret;
	double x[2], y[2];
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		x[0] = itr->x;
		x[1] = itr->x + 210 * itr->ax;
		y[0] = itr->y;
		y[1] = itr->y + 210 * itr->ay;

		if (x[0] < x_min + cut_range)continue;
		if (x[1] < x_min + cut_range)continue;
		if (x[0] > x_max - cut_range)continue;
		if (x[1] > x_max - cut_range)continue;

		if (y[0] < y_min + cut_range)continue;
		if (y[1] < y_min + cut_range)continue;
		if (y[0] > y_max - cut_range)continue;
		if (y[1] > y_max - cut_range)continue;

		ret.push_back(*itr);
	}
	printf("prediction num = %d\n", ret.size());

	return ret;

}
