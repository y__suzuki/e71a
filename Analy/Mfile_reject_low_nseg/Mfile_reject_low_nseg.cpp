#pragma comment(lib,"FILE_structure.lib")

#include <FILE_structure.hpp>

int main(int argc, char** argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-mfile file-out-mfile nseg>thr\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];
	int nseg_thr = std::stoi(argv[3]);

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m, nseg_thr);

	mfile1::write_mfile_extension(file_out_mfile, m);
}

