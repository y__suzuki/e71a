//#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <filesystem>
#include <fstream>
#include <map>

std::vector<cv::Mat> read_image(std::string file_path, int start, int end);
cv::Mat image_sum(std::vector<cv::Mat> &image, std::string file_out_path, bool output);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage: image-path image-start image-end output-path\n");
		exit(1);
	}
	std::string file_in_image_path = argv[1];
	int image_start = std::stoi(argv[2]);
	int image_end = std::stoi(argv[3]);
	std::string file_out_image_path = argv[4];

	std::vector<cv::Mat> image_gray = read_image(file_in_image_path, image_start, image_end);
	cv::Mat image_tmp = image_sum(image_gray, file_out_image_path, true);

}
std::vector<cv::Mat> read_image(std::string file_path, int start,int end) {
	std::vector<cv::Mat> ret;
	int count = 0;
	for (int i = start; i <=end; i++) {
		printf("\r read image %3d/%3d", count, end-start+1);
		count++;
		std::stringstream filename;
		filename << file_path << "\\" << std::setw(4) << std::setfill('0') << i << ".png";
		if (!std::filesystem::exists(filename.str())) {
			printf("\n file %s not found\n", filename.str().c_str());
			exit(1);
		}
		cv::Mat image = cv::imread(filename.str(), cv::IMREAD_GRAYSCALE);
		ret.push_back(image);
	}
	printf("\r read image %3d/%3d\n", count, end - start + 1);
	return ret;
}
cv::Mat image_sum(std::vector<cv::Mat> &image, std::string file_out_path, bool output) {

	int width, height;
	//x
	width = image[0].cols;
	//y
	height = image[0].rows;
	//cv::Mat image_out = cv::Mat::zeros(height, width, CV_8UC1);
	cv::Mat image_out(cv::Size(width, height), CV_8UC3, cv::Scalar::all(255));
	unsigned char br0, br1;
	int bright;

	for (int i = 0; i < image.size(); i++) {
		for (int ix = 0; ix < width; ix++) {
			for (int iy = 0; iy < height; iy++) {
				br0 = image[i].at<unsigned char>(iy, ix);//��
				br1 = image_out.at<cv::Vec3b>(iy, ix)[0];//��
				if (br0 < br1) {
					//image_out.at<unsigned char>(iy, ix) = br0;
					image_out.at<cv::Vec3b>(iy, ix)[0] = br0;
					image_out.at<cv::Vec3b>(iy, ix)[1] = br0;
					image_out.at<cv::Vec3b>(iy, ix)[2] = br0;
				}

			}
		}
	}
	if (output) {
		std::stringstream filename;
		filename << file_out_path << "\\test_img.png";
		cv::imwrite(filename.str(), image_out);
	}
	return  image_out;
}
