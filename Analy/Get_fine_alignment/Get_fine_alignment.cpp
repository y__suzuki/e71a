#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <omp.h>

class output_format_micro {
public:
	int pos, view, imager, zone, isg, ph, vph, px;
};
class output_format_base {
public:
	int pl, rawid;
	double ax, ay, x, y, z;
	output_format_micro m[2];
};
class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};

std::string Get_linklet_file(std::string file_in_ECC, int pl0, int pl1);
std::vector<output_format_link> read_link_bin(std::string filename);
std::vector<vxx::base_track_t> read_file(std::string filename);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string file_in_track = argv[1];
	std::string file_in_ECC = argv[2];
	int pl0 = std::stoi(argv[3]);
	int pl1 = std::stoi(argv[4]);

	std::vector<vxx::base_track_t> base=read_file(file_in_track);

	std::string file_in_link = Get_linklet_file(file_in_ECC, pl0, pl1);
	std::vector<output_format_link> link=read_link_bin(file_in_link);


}
std::vector<vxx::base_track_t> read_file(std::string filename) {
	std::vector<vxx::base_track_t> ret;
	vxx::base_track_t b;

	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	while (std::getline(ifs, str))
	{
		b.pl = -1;
		b.rawid = -1;

		b.ax = NAN;
		b.ay = NAN;
		b.x = NAN;
		b.y = NAN;
		
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		if (str_v.size() == 2) {
			b.pl = std::stoi(str_v[0]);
			b.rawid = std::stoi(str_v[1]);
		}
		else if (str_v.size() == 5) {
			b.pl = std::stoi(str_v[0]);
			b.ax = std::stoi(str_v[1]);
			b.ay = std::stoi(str_v[2]);
			b.x = std::stoi(str_v[3]);
			b.y = std::stoi(str_v[4]);
		}
		else {
			fprintf(stderr, "format exception\n");
			fprintf(stderr, "permitted format\n");
			fprintf(stderr, "[pl] [rawid]\n");
			fprintf(stderr, "[pl] [ax] [ay] [x] [y]\n");
			continue;
		}
		ret.push_back(b);
	}
	fprintf(stderr, "read basetrack %d tracks\n", ret.size());
	return ret;
}
std::string Get_linklet_file(std::string file_in_ECC,int pl0,int pl1) {
	std::stringstream file_in_link;
	file_in_link << file_in_ECC << "\\Area0\\0\\linklet_3d\\link_"
		<< std::setw(3) << std::setfill(0) << pl0 << "_"
		<< std::setw(3) << std::setfill(0) << pl1 << ".sel.bin";
		return file_in_link.str();
}
std::vector<output_format_link> read_link_bin(std::string filename) {
	std::vector<output_format_link> link;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	output_format_link l;
	while (ifs.read((char*)& l, sizeof(output_format_link))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		link.emplace_back(l);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return link;
}
