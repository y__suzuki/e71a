#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"
//#include "basetrack_pair.h"
#include <set>
#include <list>
#include <omp.h>
std::vector<vxx::base_track_t>read_bvxx(std::string folder, int pl, int area, std::string corrmap);
void read_corrmap(std::string folder, int pl, int area0, int area1, std::map<std::pair<int, int>, corrmap0::Corrmap> &corr);
std::map<int, std::string> corrmap_Calc(std::map<std::pair<int, int>, corrmap0::Corrmap> &corr, std::string file_corr);
corrmap0::Corrmap search_corrmap(int area0, int area1, std::map<std::pair<int, int>, corrmap0::Corrmap> &corr);
corrmap0::Corrmap CalcCorrrmap_2trans(corrmap0::Corrmap corr1, corrmap0::Corrmap corr2);
corrmap0::Corrmap InverseCorrmap(corrmap0::Corrmap corr);
corrmap0::Corrmap CorrmapAverage(corrmap0::Corrmap corr1, corrmap0::Corrmap corr2);
void Input_CorrAll(std::vector<corrmap0::Corrmap>&corr_all, corrmap0::Corrmap corr, int Area);
corrmap0::Corrmap calc_rot_shift(corrmap0::Corrmap corr, double rot, double shift_x, double shift_y);

void out_difference(std::string filename, int area0, std::vector<vxx::base_track_t> &b0, int area1, std::vector<vxx::base_track_t> &b1);
void delete_multi(std::vector<vxx::base_track_t> &b, double pos_lat, double pos_rad_intercept, double pos_rad_slope, double ang_lat, double ang_rad_intercept, double ang_rad_slope);

void base_shift(std::vector<vxx::base_track_t> &b, double dx, double dy);
std::pair<double, double> re_calc_corr_shift(std::vector<vxx::base_track_t> &b0, std::vector<vxx::base_track_t> &b1);
void corrmap_rewrite(std::string filename, double shift_x, double shift_y);
void GaussJorden_6(double in[6][6], double b[6], double c[6]);
void Calc_p(std::pair<double, double> p[6], std::map < int, std::vector<vxx::base_track_t>>base);


int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg out-vxx-folder pl input-bvxx-folder input-param-folder output-param-folder\n");
		exit(1);
	}
	std::string folder_out_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	std::string folder_in_bvxx = argv[3];
	std::string folder_in_corr = argv[4];
	std::string folder_out_corr = argv[5];
	std::map<std::pair<int, int>, corrmap0::Corrmap> corr;

	//corrmap (grobal) read
	read_corrmap(folder_in_corr, pl, 1, 2, corr);
	read_corrmap(folder_in_corr, pl, 1, 3, corr);
	read_corrmap(folder_in_corr, pl, 2, 4, corr);
	read_corrmap(folder_in_corr, pl, 3, 4, corr);
	read_corrmap(folder_in_corr, pl, 3, 5, corr);
	read_corrmap(folder_in_corr, pl, 4, 6, corr);
	read_corrmap(folder_in_corr, pl, 5, 6, corr);

	printf("corr size=%d\n", corr.size());
	//areaは0-5ではなく1-6なのでmapにして入れてる
	std::map<int, std::string> corr_area = corrmap_Calc(corr, folder_out_corr);
	std::map < int, std::vector<vxx::base_track_t>>base;
	for (int area = 1; area <= 6; area++) {
		//basetrack読み込み
		std::vector<vxx::base_track_t> base_tmp = read_bvxx(folder_in_bvxx, pl, area, corr_area[area]);
		base.insert(std::make_pair(area, base_tmp));
	}
	std::pair<double, double> p[6];
	Calc_p(p, base);

	for (int i = 0; i < 6; i++) {
		//printf("Area%d (%5.2lf,%5.2lf)\n", i+1, p[i].first,p[i].second);
		base_shift(base[i + 1], p[i].first, p[i].second);
		//corrmapにshift分足して書き換え
		corrmap_rewrite(corr_area[i + 1], p[i].first, p[i].second);
	}
	//matchしたbasetrackの出力
	std::stringstream file_out_pair;
	file_out_pair << folder_out_bvxx << "\\pair.txt";
	out_difference(file_out_pair.str(), 1, base[1], 2, base[2]);
	out_difference(file_out_pair.str(), 1, base[1], 3, base[3]);
	out_difference(file_out_pair.str(), 2, base[2], 4, base[4]);
	out_difference(file_out_pair.str(), 3, base[3], 4, base[4]);
	out_difference(file_out_pair.str(), 3, base[3], 5, base[5]);
	out_difference(file_out_pair.str(), 4, base[4], 6, base[6]);
	out_difference(file_out_pair.str(), 5, base[5], 6, base[6]);

	//basetrackのzoneにareaを指定してmerge(zoneは出力時に保存されない.prg内のみ有効)
	std::vector<vxx::base_track_t>base_all;
	for (int i = 1; i <= 6; i++) {
		for (auto itr = base[i].begin(); itr != base[i].end(); itr++) {
			itr->zone = i;
			base_all.push_back(*itr);
		}
	}
	//重複飛跡の削除
	delete_multi(base_all, 10, 10, 10, 0.015, 0.015, 0.05);
	//merge後出力
	std::stringstream file_out_bvxx;
	file_out_bvxx << folder_out_bvxx << "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	vxx::BvxxWriter bw;
	bw.Write(file_out_bvxx.str(), pl, 0, base_all);
}

std::vector<vxx::base_track_t>read_bvxx(std::string folder, int pl, int area, std::string corrmap) {
	std::stringstream file_in_bvxx;
	file_in_bvxx << folder << "\\Area" << std::setw(1) << area << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	//file_in_bvxx << folder << "\\PL" << std::setw(3) << std::setfill('0') << pl << "\\Area" << std::setw(1) << area
	//	<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx.str(), pl, 0, vxx::opt::c = corrmap);

	std::vector<vxx::base_track_t> ret;
	int vph = 0;
	double angle = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		vph = (itr->m[0].ph + itr->m[0].ph) % 10000;
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angle < 1.0) {
			if (int(itr->m[0].ph) / 10000 < 8 && int(itr->m[1].ph) / 10000 < 8)continue;
		}
		ret.push_back(*itr);
	}



	return base;
}
void read_corrmap(std::string folder, int pl, int area0, int area1, std::map<std::pair<int, int>, corrmap0::Corrmap> &corr) {
	corrmap0::Corrmap param;
	std::stringstream file_in_corr;
	file_in_corr << folder << "\\corrmap-g-" << std::setw(3) << std::setfill('0') << pl << "-" << std::setw(1) << area0 << "-" << std::setw(1) << area1 << ".lst";
	corrmap0::read_cormap(file_in_corr.str(), param);
	param.angle[4] = 0;
	param.angle[5] = 0;
	corr.insert(std::make_pair(std::make_pair(area0, area1), param));
}
std::map<int, std::string> corrmap_Calc(std::map<std::pair<int, int>, corrmap0::Corrmap> &corr, std::string file_corr) {
	corrmap0::Corrmap corr11, corr12, corr13, corr24, corr34, corr35, corr46, corr56;
	corrmap0::Corrmap corr31, corr32, corr36;
	std::vector<corrmap0::Corrmap> corr_all;

	corr12 = search_corrmap(1, 2, corr);
	corr13 = search_corrmap(1, 3, corr);
	corr24 = search_corrmap(2, 4, corr);
	corr34 = search_corrmap(3, 4, corr);
	corr35 = search_corrmap(3, 5, corr);
	corr46 = search_corrmap(4, 6, corr);
	corr56 = search_corrmap(5, 6, corr);

	corr31 = InverseCorrmap(corr13);

	corrmap0::Corrmap corr42, corr65;
	corr42 = InverseCorrmap(corr24);
	corr65 = InverseCorrmap(corr56);
	corrmap0::Corrmap corr312, corr342, corr356, corr346;
	corr312 = CalcCorrrmap_2trans(corr31, corr12);
	corr342 = CalcCorrrmap_2trans(corr34, corr42);
	corr356 = CalcCorrrmap_2trans(corr35, corr56);
	corr346 = CalcCorrrmap_2trans(corr34, corr46);

	corr32 = CorrmapAverage(corr312, corr342);
	corr36 = CorrmapAverage(corr346, corr356);

	corrmap0::Corrmap corr14, corr15, corr16;
	corr11 = CalcCorrrmap_2trans(corr13, corr31);
	corr12 = CalcCorrrmap_2trans(corr13, corr32);
	corr14 = CalcCorrrmap_2trans(corr13, corr34);
	corr15 = CalcCorrrmap_2trans(corr13, corr35);
	corr16 = CalcCorrrmap_2trans(corr13, corr36);

	std::string output_corr = file_corr;

	corrmap0::write_corrmap(output_corr + "\\corrmap-connect-1-1.lst", corr11);
	corrmap0::write_corrmap(output_corr + "\\corrmap-connect-1-2.lst", corr12);
	corrmap0::write_corrmap(output_corr + "\\corrmap-connect-1-3.lst", corr13);
	corrmap0::write_corrmap(output_corr + "\\corrmap-connect-1-4.lst", corr14);
	corrmap0::write_corrmap(output_corr + "\\corrmap-connect-1-5.lst", corr15);
	corrmap0::write_corrmap(output_corr + "\\corrmap-connect-1-6.lst", corr16);

	std::map<int, std::string > ret;
	ret.insert(std::make_pair(1, output_corr + "\\corrmap-connect-1-1.lst"));
	ret.insert(std::make_pair(2, output_corr + "\\corrmap-connect-1-2.lst"));
	ret.insert(std::make_pair(3, output_corr + "\\corrmap-connect-1-3.lst"));
	ret.insert(std::make_pair(4, output_corr + "\\corrmap-connect-1-4.lst"));
	ret.insert(std::make_pair(5, output_corr + "\\corrmap-connect-1-5.lst"));
	ret.insert(std::make_pair(6, output_corr + "\\corrmap-connect-1-6.lst"));

	return ret;
}
corrmap0::Corrmap search_corrmap(int area0, int area1, std::map<std::pair<int, int>, corrmap0::Corrmap> &corr) {
	auto res = corr.find(std::make_pair(area0, area1));
	if (res == corr.end()) {
		printf("area%d - area%d corrmap not found\n", area0, area1);
		exit(1);
	}
	return res->second;
}
corrmap0::Corrmap CalcCorrrmap_2trans(corrmap0::Corrmap corr1, corrmap0::Corrmap corr2) {
	//初期化
	corrmap0::Corrmap ret = corr1;
	ret.areax[0] = 0.0;
	ret.areax[1] = 100000.0;
	ret.areay[0] = 0.0;
	ret.areay[1] = 100000.0;

	//2回変換
	ret.position[0] = corr1.position[0] * corr2.position[0] + corr1.position[1] * corr2.position[2];
	ret.position[1] = corr1.position[0] * corr2.position[1] + corr1.position[1] * corr2.position[3];
	ret.position[2] = corr1.position[2] * corr2.position[0] + corr1.position[3] * corr2.position[2];
	ret.position[3] = corr1.position[2] * corr2.position[1] + corr1.position[3] * corr2.position[3];

	ret.position[4] = corr1.position[0] * corr2.position[4] + corr1.position[1] * corr2.position[5] + corr1.position[4];
	ret.position[5] = corr1.position[2] * corr2.position[4] + corr1.position[3] * corr2.position[5] + corr1.position[5];

	ret.angle[0] = corr1.angle[0] * corr2.angle[0] + corr1.angle[1] * corr2.angle[2];
	ret.angle[1] = corr1.angle[0] * corr2.angle[1] + corr1.angle[1] * corr2.angle[3];
	ret.angle[2] = corr1.angle[2] * corr2.angle[0] + corr1.angle[3] * corr2.angle[2];
	ret.angle[3] = corr1.angle[2] * corr2.angle[1] + corr1.angle[3] * corr2.angle[3];

	ret.angle[4] = corr1.angle[0] * corr2.angle[4] + corr1.angle[1] * corr2.angle[5] + corr1.angle[4];
	ret.angle[5] = corr1.angle[2] * corr2.angle[4] + corr1.angle[3] * corr2.angle[5] + corr1.angle[5];

	return ret;
}
corrmap0::Corrmap InverseCorrmap(corrmap0::Corrmap corr) {
	corrmap0::Corrmap ret = corr;
	ret.position[1] = corr.position[1] * -1.;
	ret.position[2] = corr.position[2] * -1.;
	ret.position[4] = -1 * (ret.position[0] * corr.position[4] + ret.position[1] * corr.position[5]);
	ret.position[5] = -1 * (ret.position[2] * corr.position[4] + ret.position[3] * corr.position[5]);

	ret.angle[1] = corr.angle[1] * -1.;
	ret.angle[2] = corr.angle[2] * -1.;
	ret.angle[4] = -1 * (ret.angle[0] * corr.angle[4] + ret.angle[1] * corr.angle[5]);
	ret.angle[5] = -1 * (ret.angle[2] * corr.angle[4] + ret.angle[3] * corr.angle[5]);

	return ret;
}
corrmap0::Corrmap CorrmapAverage(corrmap0::Corrmap corr1, corrmap0::Corrmap corr2) {
	corrmap0::Corrmap ret = corr1;

	double theta, shift_x, shift_y, shift_ax, shift_ay;


	theta = (asin(corr1.position[2]) + asin(corr2.position[2])) / 2;
	shift_x = (corr1.position[4] + corr2.position[4]) / 2;
	shift_y = (corr1.position[5] + corr2.position[5]) / 2;
	shift_ax = (corr1.angle[4] + corr2.angle[4]) / 2;
	shift_ay = (corr1.angle[5] + corr2.angle[5]) / 2;


	ret.position[0] = cos(theta);
	ret.position[1] = sin(theta)*-1;
	ret.position[2] = sin(theta);
	ret.position[3] = cos(theta);
	ret.position[4] = shift_x;
	ret.position[5] = shift_y;

	ret.angle[0] = cos(theta);
	ret.angle[1] = sin(theta)*-1;
	ret.angle[2] = sin(theta);
	ret.angle[3] = cos(theta);
	ret.angle[4] = shift_ax;
	ret.angle[5] = shift_ay;

	return ret;
}
void Input_CorrAll(std::vector<corrmap0::Corrmap>&corr_all, corrmap0::Corrmap corr, int Area) {
	corr.id = Area;
	corr_all.push_back(corr);
}

corrmap0::Corrmap calc_rot_shift(corrmap0::Corrmap corr, double rot, double shift_x, double shift_y) {
	double theta;
	theta = asin(corr.position[2]);
	shift_x = corr.position[4];
	shift_y = corr.position[5];
	theta -= rot;
	corr.position[0] = cos(theta);
	corr.position[1] = -1 * sin(theta);
	corr.position[2] = sin(theta);
	corr.position[3] = cos(theta);
	corr.position[4] -= shift_x;
	corr.position[5] -= shift_y;

	corr.angle[0] = cos(theta);
	corr.angle[1] = -1 * sin(theta);
	corr.angle[2] = sin(theta);
	corr.angle[3] = cos(theta);

	return corr;
}
void out_difference(std::string filename, int area0, std::vector<vxx::base_track_t> &b0, int area1, std::vector<vxx::base_track_t> &b1) {
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> b_pair;
	//track matching
	double x_min, y_min;
	double hash = 1000;
	for (auto itr = b1.begin(); itr != b1.end(); itr++) {
		if (itr == b1.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		y_min = std::min(itr->y, y_min);
	}
	std::multimap<std::pair<int, int>, vxx::base_track_t *> b1_map;
	//
	std::pair<int, int> id;
	for (auto itr = b1.begin(); itr != b1.end(); itr++) {
		id.first = (itr->x - x_min) / hash;
		id.second = (itr->y - y_min) / hash;
		b1_map.insert(std::make_pair(id, &(*itr)));
	}

	double all_pos_slope[2], all_pos_intercept[2], all_ang_slope[2], all_ang_intercept[2];
	all_pos_intercept[0] = 10;
	all_pos_intercept[1] = 10;
	all_pos_slope[0] = 0;
	all_pos_slope[1] = 10;

	all_ang_intercept[0] = 0.015;
	all_ang_intercept[1] = 0.015;
	all_ang_slope[0] = 0;
	all_ang_slope[1] = 0.05;

	int ix, iy;
	double all_pos[2], all_ang[2], angle;
	int all = b0.size(), cnt = 0;
	for (auto itr = b0.begin(); itr != b0.end(); itr++) {
		if (cnt % 10000 == 0) {
			printf("\r matching ... %d/%d(%4.1lf%%)", cnt, all, cnt*100. / all);
		}
		cnt++;
		ix = (itr->x - x_min) / hash;
		iy = (itr->y - y_min) / hash;

		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		all_pos[0] = all_pos_slope[0] * angle + all_pos_intercept[0];
		all_pos[1] = all_pos_slope[1] * angle + all_pos_intercept[1];
		all_ang[0] = all_ang_slope[0] * angle + all_ang_intercept[0];
		all_ang[1] = all_ang_slope[1] * angle + all_ang_intercept[1];

		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				id.first = ix + iix;
				id.second = iy + iiy;
				if (b1_map.count(id) == 0)continue;
				auto range = b1_map.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					double d_pos[2], d_ang[2];
					if (angle > 0.1) {
						d_pos[0] = (res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax;
						d_pos[1] = (res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay;

						d_ang[0] = (res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax;
						d_ang[1] = (res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay;

						if (fabs(d_pos[0]) > all_pos[0] * angle)continue;
						if (fabs(d_pos[1]) > all_pos[1] * angle)continue;
						if (fabs(d_ang[0]) > all_ang[0] * angle)continue;
						if (fabs(d_ang[1]) > all_ang[1] * angle)continue;
					}
					else {
						d_pos[0] = (res->second->x - itr->x);
						d_pos[1] = (res->second->y - itr->y);
						d_ang[0] = (res->second->ax - itr->ax);
						d_ang[1] = (res->second->ay - itr->ay);

						if (fabs(d_pos[0]) > all_pos[0])continue;
						if (fabs(d_pos[1]) > all_pos[0])continue;
						if (fabs(d_ang[0]) > all_ang[0])continue;
						if (fabs(d_ang[1]) > all_ang[0])continue;
					}

					b_pair.push_back(std::make_pair(*itr, *res->second));
				}
			}
		}
	}
	printf("\r matching ... %d/%d(%4.1lf%%)\n", cnt, all, cnt*100. / all);

	//ずれの出力
	cnt = 0;
	all = b_pair.size();
	std::ofstream ofs(filename, std::ios::app);
	for (auto itr = b_pair.begin(); itr != b_pair.end(); itr++) {
		if (cnt % 10000 == 0) {
			printf("\r writing ... %d/%d(%4.1lf%%)", cnt, all, cnt*100. / all);
		}
		cnt++;
		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << area0 << " "
			<< std::setw(3) << std::setprecision(0) << area1 << " "
			<< std::setw(10) << std::setprecision(0) << itr->first.rawid << " "
			<< std::setw(7) << std::setprecision(0) << itr->first.m[0].ph + itr->first.m[1].ph << " "
			<< std::setw(7) << std::setprecision(4) << itr->first.ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->first.ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->first.x << " "
			<< std::setw(8) << std::setprecision(1) << itr->first.y << " "
			<< std::setw(8) << std::setprecision(1) << itr->first.m[1].z - itr->first.m[0].z << " "
			<< std::setw(10) << std::setprecision(0) << itr->second.rawid << " "
			<< std::setw(7) << std::setprecision(0) << itr->second.m[0].ph + itr->second.m[1].ph << " "
			<< std::setw(7) << std::setprecision(4) << itr->second.ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->second.ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.x << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.y << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.m[1].z - itr->second.m[0].z << std::endl;
	}
	printf("\r writing ... %d/%d(%4.1lf%%)\n", cnt, all, cnt*100. / all);


}
void delete_multi(std::vector<vxx::base_track_t> &b, double pos_lat, double pos_rad_intercept, double pos_rad_slope, double ang_lat, double ang_rad_intercept, double ang_rad_slope) {
	//key:((zone,rawid),(zone,rawid))  val(base*,base*)
	std::multimap<std::pair<int, int>, std::pair<int, int>> b_pair_id;
	std::map < std::pair<int, int>, vxx::base_track_t*>b_id_search;
	std::set<std::pair<int, int>> delete_list;

	std::map<std::pair<std::pair<int, int>, std::pair<int, int>>, std::pair<vxx::base_track_t*, vxx::base_track_t*>> pair_map;

	//track matching
	double x_min, y_min;
	double hash = 1000;
	for (auto itr = b.begin(); itr != b.end(); itr++) {
		if (itr == b.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		y_min = std::min(itr->y, y_min);
		b_id_search.insert(std::make_pair(std::make_pair(itr->zone, itr->rawid), &(*itr)));
	}
	std::multimap<std::pair<int, int>, vxx::base_track_t *> b_map;
	//
	std::pair<int, int> id;
	for (auto itr = b.begin(); itr != b.end(); itr++) {
		id.first = (itr->x - x_min) / hash;
		id.second = (itr->y - y_min) / hash;
		b_map.insert(std::make_pair(id, &(*itr)));
	}

	int ix, iy;
	double all_pos[2], all_ang[2], angle;
	int all = b.size(), cnt = 0;
	for (auto itr = b.begin(); itr != b.end(); itr++) {
		if (cnt % 10000 == 0) {
			printf("\r matching ... %d/%d(%4.1lf%%)", cnt, all, cnt*100. / all);
		}
		cnt++;
		ix = (itr->x - x_min) / hash;
		iy = (itr->y - y_min) / hash;

		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		all_pos[0] = pos_rad_slope * angle + pos_rad_intercept;
		all_pos[1] = pos_lat;
		all_ang[0] = ang_rad_slope * angle + ang_rad_intercept;
		all_ang[1] = ang_lat;

		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				id.first = ix + iix;
				id.second = iy + iiy;
				if (b_map.count(id) == 0)continue;
				auto range = b_map.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					if (itr->zone == res->second->zone)continue;
					double d_pos[2], d_ang[2];
					if (angle > 0.1) {
						d_pos[0] = (res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay;
						d_pos[1] = (res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax;

						d_ang[0] = (res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay;
						d_ang[1] = (res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax;

						if (fabs(d_pos[0]) > all_pos[0] * angle)continue;
						if (fabs(d_pos[1]) > all_pos[1] * angle)continue;
						if (fabs(d_ang[0]) > all_ang[0] * angle)continue;
						if (fabs(d_ang[1]) > all_ang[1] * angle)continue;
					}
					else {
						d_pos[0] = (res->second->x - itr->x);
						d_pos[1] = (res->second->y - itr->y);
						d_ang[0] = (res->second->ax - itr->ax);
						d_ang[1] = (res->second->ay - itr->ay);

						if (fabs(d_pos[0]) > all_pos[0])continue;
						if (fabs(d_pos[1]) > all_pos[0])continue;
						if (fabs(d_ang[0]) > all_ang[0])continue;
						if (fabs(d_ang[1]) > all_ang[0])continue;
					}
					//zoneの小さいほうを残す
					if (itr->zone < res->second->zone) {
						b_pair_id.insert(std::make_pair(std::make_pair(itr->zone, itr->rawid), std::make_pair(res->second->zone, res->second->rawid)));
						delete_list.insert(std::make_pair(res->second->zone, res->second->rawid));
					}
					else {
						b_pair_id.insert(std::make_pair(std::make_pair(res->second->zone, res->second->rawid), std::make_pair(itr->zone, itr->rawid)));
						delete_list.insert(std::make_pair(itr->zone, itr->rawid));
					}
				}
			}
		}
	}
	printf("\r matching ... %d/%d(%4.1lf%%)\n", cnt, all, cnt*100. / all);
	//multiの中から消すトラックを判定
	std::vector<vxx::base_track_t> ret;
	for (auto itr = b.begin(); itr != b.end(); itr++) {
		if (delete_list.count(std::make_pair(itr->zone, itr->rawid)) == 1)continue;
		ret.push_back(*itr);
	}
	printf("multi delete %d --> %d\n", b.size(), ret.size());
	b = ret;
}


std::pair<double, double> re_calc_corr_shift(std::vector<vxx::base_track_t> &b0, std::vector<vxx::base_track_t> &b1) {
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> b_pair;
	//track matching
	double x_min, y_min;
	double hash = 1000;
	for (auto itr = b1.begin(); itr != b1.end(); itr++) {
		if (itr == b1.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		y_min = std::min(itr->y, y_min);
	}
	std::multimap<std::pair<int, int>, vxx::base_track_t *> b1_map;
	//
	std::pair<int, int> id;
	for (auto itr = b1.begin(); itr != b1.end(); itr++) {
		id.first = (itr->x - x_min) / hash;
		id.second = (itr->y - y_min) / hash;
		b1_map.insert(std::make_pair(id, &(*itr)));
	}

	double all_pos_slope[2], all_pos_intercept[2], all_ang_slope[2], all_ang_intercept[2];
	all_pos_intercept[0] = 10;
	all_pos_intercept[1] = 10;
	all_pos_slope[0] = 0;
	all_pos_slope[1] = 10;

	all_ang_intercept[0] = 0.015;
	all_ang_intercept[1] = 0.015;
	all_ang_slope[0] = 0;
	all_ang_slope[1] = 0.05;

	int ix, iy;
	double all_pos[2], all_ang[2], angle;
	int all = b0.size(), cnt = 0;
	for (auto itr = b0.begin(); itr != b0.end(); itr++) {
		if (cnt % 10000 == 0) {
			printf("\r matching ... %d/%d(%4.1lf%%)", cnt, all, cnt*100. / all);
		}
		cnt++;
		ix = (itr->x - x_min) / hash;
		iy = (itr->y - y_min) / hash;

		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		all_pos[0] = all_pos_slope[0] * angle + all_pos_intercept[0];
		all_pos[1] = all_pos_slope[1] * angle + all_pos_intercept[1];
		all_ang[0] = all_ang_slope[0] * angle + all_ang_intercept[0];
		all_ang[1] = all_ang_slope[1] * angle + all_ang_intercept[1];

		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				id.first = ix + iix;
				id.second = iy + iiy;
				if (b1_map.count(id) == 0)continue;
				auto range = b1_map.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					double d_pos[2], d_ang[2];
					if (angle > 0.1) {
						d_pos[0] = (res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax;
						d_pos[1] = (res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay;

						d_ang[0] = (res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax;
						d_ang[1] = (res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay;

						if (fabs(d_pos[0]) > all_pos[0] * angle)continue;
						if (fabs(d_pos[1]) > all_pos[1] * angle)continue;
						if (fabs(d_ang[0]) > all_ang[0] * angle)continue;
						if (fabs(d_ang[1]) > all_ang[1] * angle)continue;
					}
					else {
						d_pos[0] = (res->second->x - itr->x);
						d_pos[1] = (res->second->y - itr->y);
						d_ang[0] = (res->second->ax - itr->ax);
						d_ang[1] = (res->second->ay - itr->ay);

						if (fabs(d_pos[0]) > all_pos[0])continue;
						if (fabs(d_pos[1]) > all_pos[0])continue;
						if (fabs(d_ang[0]) > all_ang[0])continue;
						if (fabs(d_ang[1]) > all_ang[0])continue;
					}

					b_pair.push_back(std::make_pair(*itr, *res->second));
				}
			}
		}
	}
	printf("\r matching ... %d/%d(%4.1lf%%)\n", cnt, all, cnt*100. / all);
	double dx = 0, dy = 0;
	int count = 0;
	for (auto itr = b_pair.begin(); itr != b_pair.end(); itr++) {
		dx += itr->second.x - itr->first.x;
		dy += itr->second.y - itr->first.y;
		count++;
	}
	dx = dx / count;
	dy = dy / count;
	return std::make_pair(dx, dy);
}
void base_shift(std::vector<vxx::base_track_t> &b, double dx, double dy) {
	for (auto itr = b.begin(); itr != b.end(); itr++) {
		itr->x += dx;
		itr->y += dy;
	}
}
void corrmap_rewrite(std::string filename, double shift_x, double shift_y) {
	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(filename, corr);
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		itr->position[4] += shift_x;
		itr->position[5] += shift_y;
	}
	corrmap0::write_corrmap(filename, corr);
}

void Calc_p(std::pair<double, double> p[6], std::map < int, std::vector<vxx::base_track_t>>base){
	std::pair<double, double>d_12, d_13, d_24, d_34, d_35, d_46, d_56;
	//D_ijの計算
	d_12 = re_calc_corr_shift(base[1], base[2]);
	d_13 = re_calc_corr_shift(base[1], base[3]);
	d_24 = re_calc_corr_shift(base[2], base[4]);
	d_34 = re_calc_corr_shift(base[3], base[4]);
	d_35 = re_calc_corr_shift(base[3], base[5]);
	d_46 = re_calc_corr_shift(base[4], base[6]);
	d_56 = re_calc_corr_shift(base[5], base[6]);

	double in[2][6][6] = {
		{
		{1,0,0,0,0,0},
		{-1,2,0,-1,0,0},
		{-1,0,3,-1,-1,0},
		{0,-1,-1,3,0,-1},
		{0,0,-1,0,2,-1},
		{0,0,0,-1,-1,2}
	},
		{
		{1,0,0,0,0,0},
		{-1,2,0,-1,0,0},
		{-1,0,3,-1,-1,0},
		{0,-1,-1,3,0,-1},
		{0,0,-1,0,2,-1},
		{0,0,0,-1,-1,2}
	}
	};
	double b[2][6] = {
		{
		0,
		-1 * d_12.first + d_24.first,
		-1 * d_13.first + d_34.first + d_35.first,
		-1 * d_24.first - d_34.first + d_46.first,
		-1 * d_35.first + d_56.first,
		-1 * d_46.first - d_56.first
		},
		{
		0,
		-1 * d_12.second + d_24.second,
		-1 * d_13.second + d_34.second + d_35.second,
		-1 * d_24.second - d_34.second + d_46.second,
		-1 * d_35.second + d_56.second,
		-1 * d_46.second - d_56.second
		}
	};
	double res[2][6];
	GaussJorden_6(in[0], b[0], res[0]);
	GaussJorden_6(in[1], b[1], res[1]);

	//Piの計算
	for(int i=0;i<6;i++){
		p[i] = std::make_pair(res[0][i], res[1][i]);
		printf("%g %g\n", p[i].first, p[i].second);
	}
	p[0].first = 0;
	p[0].second = 0;
}

void GaussJorden_6(double in[6][6], double b[6], double c[6]) {


	double a[6][7];
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 7; j++) {
			if (j < 6) {
				a[i][j] = in[i][j];
			}
			else {
				a[i][j] = b[i];
			}
		}
	}
	int N = 6;
	double p, d;         // ピボット係数、ピボット行ｘ係数
	double max, dummy;   // 最大絶対値、入れ替え時ダミー
	int s;

	//元の連立方程式をコンソール出力
 //  for (int i = 0; i < N; i++) {
 //  	for (int j = 0; j < N; j++)
 //  		printf("%+fx%d ", a[i][j], j + 1);
 //  	printf("= %+f\n", a[i][N]);
 //  }

	for (int k = 0; k < N; k++) {
		// 行入れ替え
		max = 0; s = k;
		for (int j = k; j < N; j++) {
			if (fabs(a[j][k]) > max) {
				max = fabs(a[j][k]);
				s = j;
			}
		}
		if (max == 0) {
			printf("解けない！");
			exit(1);
		}
		for (int j = 0; j <= N; j++) {
			dummy = a[k][j];
			a[k][j] = a[s][j];
			a[s][j] = dummy;
		}

		// ピボット係数
		p = a[k][k];

		// ピボット行を p で除算
		for (int j = k; j < N + 1; j++)
			a[k][j] /= p;

		// ピボット列の掃き出し
		for (int i = 0; i < N; i++) {
			if (i != k) {
				d = a[i][k];
				for (int j = k; j < N + 1; j++)
					a[i][j] -= d * a[k][j];
			}
		}
	}

	// 結果出力
	for (int k = 0; k < N; k++) {
		c[k] = a[k][N];
		//printf("x%d = %f\n", k + 1, a[k][N]);
	}
}

