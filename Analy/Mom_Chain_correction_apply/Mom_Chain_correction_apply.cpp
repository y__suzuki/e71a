#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class correction_val {
public:
	int pl, face, sensor_id,count;
	double angle_min, angle_max, angle, mean, sigma;
};
bool sort_corr(const correction_val&left, const correction_val&right) {
	if (left.pl != right.pl)return left.pl < right.pl;
	if (fabs(left.angle - right.angle) > 0.01)return left.angle < right.angle;
	if (left.sensor_id != right.sensor_id)return left.sensor_id < right.sensor_id;
	return left.face < right.face;
}

std::vector<correction_val> input_correction(std::string filename);

void Apply_correction_vph(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v);
void Apply_correction_pixel(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v);

int judege_sensor_id(int id);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file-in-momch file-correction file-out-momch mode\n");
		fprintf(stderr, "mode=0:vph\n");
		fprintf(stderr, "mode=1:pixel\n");

		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_in_corr = argv[2];
	std::string file_out_momch = argv[3];
	int mode = std::stoi(argv[4]);

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::vector<correction_val> corr_v = input_correction(file_in_corr);

	if (mode == 0) {
		Apply_correction_vph(momch, corr_v);
	}
	else if (mode == 1) {
		Apply_correction_pixel(momch, corr_v);
	}
	Momentum_recon::Write_mom_chain_extension(file_out_momch, momch);

}
void Apply_correction_pixel(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v) {

	std::map<std::tuple<int, int, int>, std::map<double, correction_val>> corr_map;
	std::tuple<int, int, int>id;
	for (auto itr = corr_v.begin(); itr != corr_v.end(); itr++) {
		std::get<0>(id) = itr->pl;
		std::get<1>(id) = itr->face;
		std::get<2>(id) = itr->sensor_id;
		if (corr_map.count(id) == 0) {
			std::map<double, correction_val> corr_v;
			corr_v.insert(std::make_pair(itr->angle_max, *itr));
			corr_map.insert(std::make_pair(id, corr_v));
		}
		else {
			corr_map.at(id).insert(std::make_pair(itr->angle_max, *itr));
		}
	}

	double angle;
	for (auto itr = mom_ch.begin(); itr != mom_ch.end(); itr++) {
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			angle = sqrt(itr2->ax*itr2->ax + itr2->ay*itr2->ay);
			std::get<0>(id) = itr2->pl;
			for (int face = 0; face <= 1; face++) {
				if (itr2->m[face].hitnum < 0)continue;
				std::get<1>(id) = face;
				std::get<2>(id) = judege_sensor_id(itr2->m[face].imager);
				if (corr_map.count(id) == 0) {
					fprintf(stderr, "correction map PL%03d face%d sensor%d not found\n", std::get<0>(id), std::get<1>(id), std::get<2>(id));
					exit(1);
				}
				auto corr_map_id = corr_map.at(id);
				auto range_end = corr_map_id.upper_bound(angle);
				//0-0.2
				if (range_end == corr_map_id.begin()) {
					itr2->m[face].hitnum = (itr2->m[face].hitnum / sqrt(1 + itr2->ax*itr2->ax + itr2->ay*itr2->ay)) / range_end->second.mean * 100;
				}
				//0.2-5.0
				else if (range_end != corr_map_id.end()) {
					auto range_start = std::next(range_end, -1);
					double x0 = range_start->second.angle;
					double x1 = range_end->second.angle;
					double y0 = range_start->second.mean;
					double y1 = range_end->second.mean;
					double mean_interporate = (y1 - y0) / (x1 - x0)*(angle - x0) + y0;
					itr2->m[face].hitnum = (itr2->m[face].hitnum / sqrt(1 + itr2->ax*itr2->ax + itr2->ay*itr2->ay)) / mean_interporate * 100;
				}
				//5.0-
				else {
					itr2->m[face].hitnum = (itr2->m[face].hitnum / sqrt(1 + itr2->ax*itr2->ax + itr2->ay*itr2->ay)) / corr_map_id.rend()->second.mean * 100;
				}
			}
		}
	}


}
void Apply_correction_vph(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v) {

	std::map<std::tuple<int, int, int>, std::map<double, correction_val>> corr_map;
	std::tuple<int, int, int>id;
	for (auto itr = corr_v.begin(); itr != corr_v.end(); itr++) {
		std::get<0>(id) = itr->pl;
		std::get<1>(id) = itr->face;
		std::get<2>(id) = itr->sensor_id;
		if (corr_map.count(id) == 0) {
			std::map<double, correction_val> corr_v;
			corr_v.insert(std::make_pair(itr->angle_max, *itr));
			corr_map.insert(std::make_pair(id, corr_v));
		}
		else {
			corr_map.at(id).insert(std::make_pair(itr->angle_max, *itr));
		}
	}

	double angle;
	for (auto itr = mom_ch.begin(); itr != mom_ch.end(); itr++) {
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			angle = sqrt(itr2->ax*itr2->ax + itr2->ay*itr2->ay);
			std::get<0>(id) = itr2->pl;
			for (int face = 0; face <= 1; face++) {
				std::get<1>(id) = face;
				std::get<2>(id) = judege_sensor_id(itr2->m[face].imager);
				if (corr_map.count(id) == 0) {
					fprintf(stderr, "correction map PL%03d face%d sensor%d not found\n", std::get<0>(id), std::get<1>(id), std::get<2>(id));
					exit(1);
				}
				auto corr_map_id = corr_map.at(id);
				auto range_end = corr_map_id.upper_bound(angle);
				int vph = itr2->m[face].ph % 10000;
				//0-0.2
				if (range_end == corr_map_id.begin()) {
					itr2->m[face].ph = vph / range_end->second.mean * 100;
				}
				//0.2-5.0
				else if (range_end != corr_map_id.end()) {
					auto range_start = std::next(range_end, -1);
					double x0 = range_start->second.angle;
					double x1 = range_end->second.angle;
					double y0 = range_start->second.mean;
					double y1 = range_end->second.mean;
					double mean_interporate = (y1 - y0) / (x1 - x0)*(angle - x0) + y0;
					itr2->m[face].ph = vph / mean_interporate * 100;
				}
				//5.0-
				else {
					itr2->m[face].ph= vph / corr_map_id.rend()->second.mean * 100;
				}
			}
		}
	}


}
std::vector<correction_val> input_correction(std::string filename) {
	std::vector<correction_val> ret;
	std::ifstream ifs(filename);
	correction_val corr;
	int count = 0;
	while (ifs >> corr.pl >> corr.face >> corr.sensor_id >> corr.angle >> corr.angle_min >> corr.angle_max >> corr.count >> corr.mean >> corr.sigma) {
		count++;
		ret.push_back(corr);
	}
	printf("input finish %d\n", ret.size());
	return ret;

}

int judege_sensor_id(int id) {
	if ((24 <= id && id <= 35) || id == 52)return 0;
	else return 1;
}