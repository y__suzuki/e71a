#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <unordered_map>
#include <set>
#include <map>

vxx::micro_track_t search_fvxx(std::string filename, int pos, int id);
void fvxx_dc(std::vector<vxx::micro_track_t>&m, double dz, double ddz, double shrink, double dax, double day);
std::vector<vxx::micro_track_t> read_fvxx_area(std::string filename, int pos, double x, double y, int zone);
void fvxx_dc(std::vector<vxx::micro_track_t>&m, double dz, double ddz, double shrink, double dax, double day);
void search_vertex(vxx::micro_track_t mu, std::vector<vxx::micro_track_t>&m, double z_range[2]);

int main(int argc, char **argv) {

	std::string file_in_fvxx_pos2[4];
	file_in_fvxx_pos2[0] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_4484\\test\\f0472_thick_0.vxx";
	file_in_fvxx_pos2[1] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_4484\\test\\f0472_thick_1.vxx";
	file_in_fvxx_pos2[2] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_4484\\test\\f0472_thin_0.vxx";
	file_in_fvxx_pos2[3] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_4484\\test\\f0472_thin_1.vxx";

	std::string file_in_fvxx_pos1[4];
	file_in_fvxx_pos1[0] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_4484\\test\\f0471_thick_0.vxx";
	file_in_fvxx_pos1[1] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_4484\\test\\f0471_thick_1.vxx";
	file_in_fvxx_pos1[2] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_4484\\test\\f0471_thin_0.vxx";
	file_in_fvxx_pos1[3] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_4484\\test\\f0471_thin_1.vxx";

	int pos = 472;
	int raw_mu = 33676569;

	//std::string file_in_fvxx_pos2[4];
	//file_in_fvxx_pos2[0] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0322_thick_0.vxx";
	//file_in_fvxx_pos2[1] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0322_thick_1.vxx";
	//file_in_fvxx_pos2[2] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0322_thin_0.vxx";
	//file_in_fvxx_pos2[3] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0322_thin_1.vxx";

	//std::string file_in_fvxx_pos1[4];
	//file_in_fvxx_pos1[0] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0321_thick_0.vxx";
	//file_in_fvxx_pos1[1] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0321_thick_1.vxx";
	//file_in_fvxx_pos1[2] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0321_thin_0.vxx";
	//file_in_fvxx_pos1[3] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0321_thin_1.vxx";

	//int pos = 322;
	//int raw_mu = 55720395;

	vxx::micro_track_t mu = search_fvxx(file_in_fvxx_pos2[0], pos, raw_mu);

	std::vector<vxx::micro_track_t> m_pos2, m_pos1;
	for (int i = 0; i < 4; i++) {
		std::vector<vxx::micro_track_t> m = read_fvxx_area(file_in_fvxx_pos2[i], pos, mu.x, mu.y,i);
		for (auto itr = m.begin(); itr != m.end(); itr++) {
			//if (i == 0 && itr->rawid == 33676569)continue;
			m_pos2.push_back(*itr);
		}
	}
	pos = 471;
	//pos = 321;
	for (int i = 0; i < 4; i++) {
		std::vector<vxx::micro_track_t> m = read_fvxx_area(file_in_fvxx_pos1[i], pos, mu.x, mu.y, i);
		for (auto itr = m.begin(); itr != m.end(); itr++) {
			m_pos1.push_back(*itr);
		}
	}

	fvxx_dc(m_pos2, 5.6, 6.715902, 1.060667, 0.017333, 0.003667);
	fvxx_dc(m_pos1, 0, -7.123277, 1.076167, 0.025667, -0.000333);

	//fvxx_dc(m_pos2, -8.3, 9.093264, 1.158000, 0.028000, -0.002000);
	//fvxx_dc(m_pos1, 0, -9.792564, 1.190125, 0.064000, 0.008000);

	double z_range[2];
	for (auto itr = m_pos2.begin(); itr != m_pos2.end(); itr++) {
		if (itr->zone == 0 && itr->rawid == raw_mu) {
			mu = *itr;
		}
		if (itr == m_pos2.begin()) {
			z_range[0] = itr->z1;
			z_range[1] = itr->z2;
		}
		z_range[0] = std::min(z_range[0], itr->z1);
		z_range[1] = std::max(z_range[1], itr->z2);
	}
	for (auto itr = m_pos1.begin(); itr != m_pos1.end(); itr++) {
		z_range[0] = std::min(z_range[0], itr->z1);
		z_range[1] = std::max(z_range[1], itr->z2);
	}
	printf("search z %5.1lf<------->%5.1lf\n", z_range[0], z_range[1]);
	search_vertex(mu, m_pos1, z_range);
	search_vertex(mu, m_pos2, z_range);

	//for (auto itr = m_pos1.begin(); itr != m_pos1.end(); itr++) {
	//	if (itr->zone == 2 && itr->rawid == 64637370) {
	//		printf("%5.4lf %5.4lf %8.1lf %8.1lf %5.1lf %5.1lf %5.1lf\n", itr->ax, itr->ay, itr->x, itr->y, itr->z, itr->z1, itr->z2);
	//		printf("%5.4lf %5.4lf %8.1lf %8.1lf %5.1lf %5.1lf %5.1lf\n", itr->ax, itr->ay, itr->x+(itr->z2-itr->z)*itr->ax, itr->y + (itr->z2 - itr->z)*itr->ay, itr->z, itr->z1, itr->z2);
	//	}
	//}
	//for (auto itr = m_pos2.begin(); itr != m_pos2.end(); itr++) {
	//	if (itr->zone == 2 && itr->rawid == 26587458) {
	//		printf("%5.4lf %5.4lf %8.1lf %8.1lf %5.1lf %5.1lf %5.1lf\n", itr->ax, itr->ay, itr->x, itr->y, itr->z, itr->z1, itr->z2);
	//		printf("%5.4lf %5.4lf %8.1lf %8.1lf %5.1lf %5.1lf %5.1lf\n", itr->ax, itr->ay, itr->x + (itr->z1 - itr->z)*itr->ax, itr->y + (itr->z1 - itr->z)*itr->ay, itr->z, itr->z1, itr->z2);

	//	}
	//}
	//1083530    47   112 - 3.3553 - 2.7524    58536.0    62840.1     
	//100005 - 3.6699 - 3.0131    58536.0    62840.1      189.5  471 - 13917     0 25   119   64637370     
	//100012 - 3.3863 - 2.7772    57805.7    62241.0      407.2  472 - 16062     0 25    29   26587458
	system("pause");
}
vxx::micro_track_t search_fvxx(std::string filename, int pos, int id) {
	vxx::FvxxReader fr;
	std::array<int, 2> index = { id,id + 1 };

	std::vector<vxx::CutArea> area;
	auto res = fr.ReadAll(filename, pos, 0, vxx::opt::index = index);

	vxx::micro_track_t ret;
	if (res.size() != 0) {
		ret = *(res.begin());
	}
	else {
		printf("rawid=%d not found\n", id);
	}

	printf("%d %d %5.4lf %5.4lf %8.1lf %8.1lf\n", ret.rawid, ret.ph, ret.ax, ret.ay, ret.x, ret.y);
	return ret;

}
std::vector<vxx::micro_track_t> read_fvxx_area(std::string filename, int pos, double x,double y,int zone) {
	double pos_width = 2000;
	vxx::FvxxReader fr;
	std::vector<vxx::CutArea> area;
	area.push_back(vxx::CutArea(x - pos_width, x + pos_width, y - pos_width, y + pos_width));
	auto res = fr.ReadAll(filename, pos, 0, vxx::opt::a = area);
	for (auto itr = res.begin(); itr != res.end(); itr++) {
		itr->zone = zone;
	}
	return res;
}
void fvxx_dc(std::vector<vxx::micro_track_t>&m, double dz, double ddz, double shrink, double dax, double day) {

	//axcorrected = shr �~ axraw + dax
	//	aycorrected = shr �~ ayraw + day
	//	xcorrected = xraw - (dzfulcrum + ddz) �~ dax
	//	ycorrected = yraw - (dzfulcrum + ddz) �~ day
	//	zcorrected = zraw + dz
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		itr->ax = itr->ax*shrink + dax;
		itr->ay = itr->ay*shrink + day;
		itr->x = itr->x - (35 + ddz)*dax;
		itr->y = itr->y - (35 + ddz)*dax;
		itr->z = (itr->z2 - itr->z1)*8. / 15 + itr->z1-dz/2;
		//itr->z = (itr->z2 - itr->z1)*0.5 + itr->z1;
	}
}

void search_vertex(vxx::micro_track_t mu, std::vector<vxx::micro_track_t>&m,double z_range[2]) {
	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	pos0.x = mu.x;
	pos0.y = mu.y;
	pos0.z = mu.z;
	dir0.x = mu.ax;
	dir0.y = mu.ay;
	dir0.z =1;

	for (auto itr = m.begin(); itr != m.end(); itr++) {
		pos1.x = itr->x;
		pos1.y = itr->y;
		pos1.z = itr->z;
		dir1.x = itr->ax;
		dir1.y = itr->ay;
		dir1.z = 1;

		double extra[2];
		double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		if (md < 50) {
			if (itr->ph / 10000 >= 14 ) {
				printf("%3.1lf %4.1lf %3d %1d %10d %7d %5.4lf %5.4lf %8.1lf %8.1lf %5.1lf\n", md, extra[0] + pos0.z, itr->pos, itr->zone, itr->rawid, itr->ph, itr->ax, itr->ay, itr->x, itr->y, itr->z);
			}
		}
	}
}
