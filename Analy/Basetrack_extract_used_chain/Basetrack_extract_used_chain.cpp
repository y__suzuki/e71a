#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

//#include <boost/unordered_set.hpp>
//#include <boost/unordered_map.hpp>

class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};
class basetrack_minimum {
public:
	int pl, rawid, ph;
	float ax, ay, x, y;
};
void read_linklet_list(std::string filename, std::set<std::pair<int, int>> &base_id);


std::map<int, std::set<int>> change_container_format(std::set<std::pair<int, int>> &base_id);
void write_base_min(std::string filename, std::vector<basetrack_minimum>&base_min);



int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file_in_link file_ECC file_out_base\n");
		exit(1);
	}
	std::string file_in_link = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_out_base = argv[3];

	std::set<std::pair<int, int>> base_id;
	read_linklet_list(file_in_link, base_id);

	std::vector<basetrack_minimum>base_min;
	base_min.reserve(base_id.size());

	std::map<int, std::set<int>> base_id_pl = change_container_format(base_id);


	vxx::BvxxReader br;
	for (auto itr = base_id_pl.begin(); itr != base_id_pl.end(); itr++) {
		printf("PL%03d basetrack read\n", itr->first);

		std::stringstream file_in_base;
		file_in_base << file_in_ECC << "\\Area0\\PL"
			<< std::setw(3) << std::setfill('0') << itr->first << "\\b"
			<< std::setw(3) << std::setfill('0') << itr->first << ".sel.cor.vxx";
		std::vector<vxx::base_track_t >base= br.ReadAll(file_in_base.str(), itr->first, 0);

		for (auto itr_b = base.begin(); itr_b != base.end(); itr_b++) {
			if (itr->second.count(itr_b->rawid) == 0)continue;
			basetrack_minimum b;
			b.ax = itr_b->ax;
			b.ay = itr_b->ay;
			b.x = itr_b->x;
			b.y = itr_b->y;
			b.pl = itr_b->pl;
			b.ph = itr_b->m[0].ph + itr_b->m[1].ph;
			b.rawid = itr_b->rawid;
			base_min.push_back(b);
		}

	}

	write_base_min(file_out_base, base_min);


}
int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}

void read_linklet_list(std::string filename, std::set<std::pair<int,int>> &base_id) {
	int64_t link_num = Linklet_header_num(filename);

	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	linklet_header l;

	while (ifs.read((char*)& l, sizeof(linklet_header))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		base_id.insert(std::make_pair(l.pos0 / 10, l.raw0));
		base_id.insert(std::make_pair(l.pos1 / 10, l.raw1));
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
}

std::map<int, std::set<int>> change_container_format(std::set<std::pair<int, int>> &base_id) {
	std::multimap<int, int> pl_multimap;
	for (auto itr = base_id.begin(); itr != base_id.end(); itr++) {
		pl_multimap.insert(*itr);
	}

	std::map<int, std::set<int>> ret;
	for (auto itr = pl_multimap.begin(); itr != pl_multimap.end(); itr++) {
		int count = pl_multimap.count(itr->first);

		std::set<int> rawid;
		auto range = pl_multimap.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			rawid.insert(res->second);
		}
		ret.insert(std::make_pair(itr->first, rawid));

		itr = std::next(itr, count - 1);
	}

	base_id.clear();

	return ret;
}

void write_base_min(std::string filename, std::vector<basetrack_minimum>&base_min) {
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (base_min.size() == 0) {
		fprintf(stderr, "target basetrack minimum ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = base_min.size();
	for (int i = 0; i < base_min.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& base_min[i], sizeof(basetrack_minimum));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}