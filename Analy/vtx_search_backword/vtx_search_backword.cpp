#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
# include <iostream>
# include <random>
#include <set>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <list>
class stop_track {
public:
	uint64_t chainid, groupid;
	int  nseg, npl, pl0, pl1, ph, rawid;
	double ax, ay, x, y, z;
};
class track_pair {
public:
	int pl0, pl1, raw0, raw1;
	double x, y, z, md, oa;
};
class track_multi {
public:
	int eventid,pl;
	double x, y, z;
	std::vector< std::pair<double, stop_track>> trk;
	std::vector<track_pair>pair;
};
void read_vtx_file(std::string filename, std::vector<track_multi>&multi);
std::multimap<int, track_multi> vtx_divide_pl(std::vector<track_multi>&in);
void read_start_txt(std::string filename, std::map<int, std::vector<stop_track>> &stop);
void track_tans_affin(stop_track  &track, corrmap0::Corrmap corr);
void Apply_alignment1(std::vector<stop_track> &track, std::vector<corrmap0::Corrmap> &corr);
void basetrack_trans(std::vector<stop_track> &track, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size);
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size);
void add_attach_track(std::vector<track_multi> &vtx, std::vector<stop_track> &start, std::map<int, double> &z_nominal_map, std::string file_ECC_path);
std::vector<std::pair<double, stop_track>> attach_chain_pickup(std::set<std::pair<uint64_t, double> >&attach_chain, std::vector<stop_track> &track);
std::set<std::pair<uint64_t, double> >search_attach(track_multi &vtx, std::vector<stop_track> &track);
void vtx_add_chain(track_multi &vtx, std::vector<std::pair<double, stop_track>> &track, corrmap0::Corrmap&param, double z_off);
void vtx_re_calc_pair(track_multi &vtx);
void output_vtx(std::string filename, std::vector<track_multi> vtx);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-vtx in-start in-ECC out_vtx_path\n");
		exit(1);
	}
	std::string file_in_vtx = argv[1];
	std::string file_in_start = argv[2];
	std::string file_in_ECC = argv[3];
	std::string file_out_vtx_path = argv[4];

	//vtx file読み込み
	std::vector<track_multi> multi;
	read_vtx_file(file_in_vtx, multi);
	std::multimap<int, track_multi> multi_pl = vtx_divide_pl(multi);

	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_nominal_map = chamber1::base_z_convert(chamber);

	//start track read
	std::map<int, std::vector<stop_track>> start;
	read_start_txt(file_in_start, start);

	std::vector<track_multi> multi_new,multi_tmp;
	for (int pl = 1; pl <= 133; pl++) {
		printf("vtx attach search PL%03d\n", pl);

		if (multi_pl.count(pl) == 0)continue;
		if (start.count(pl+1) == 0)continue;
		multi_tmp.clear();
		auto range = multi_pl.equal_range(pl);
		for (auto res = range.first; res != range.second; res++) {
			multi_tmp.push_back(res->second);
		}
		add_attach_track(multi_tmp, start.at(pl + 1), z_nominal_map, file_in_ECC);
		for (auto m : multi_tmp) {
			multi_new.push_back(m);
		}
	}
	output_vtx(file_out_vtx_path, multi_new);
}

void read_vtx_file(std::string filename, std::vector<track_multi>&multi) {
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0, trk_num;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);

		track_multi m;
		m.eventid = std::stoi(str_v[0]);
		m.pl = std::stoi(str_v[1]);
		trk_num = std::stoi(str_v[2]);
		m.x = std::stod(str_v[3]);
		m.y = std::stod(str_v[4]);
		m.z = std::stod(str_v[5]);

		for (int i = 0; i < trk_num*(trk_num - 1) / 2; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			track_pair p;
			p.pl0 = std::stoi(str_v[0]);
			p.raw0 = std::stoi(str_v[1]);
			p.pl1 = std::stoi(str_v[2]);
			p.raw1 = std::stoi(str_v[3]);
			p.x = std::stod(str_v[4]);
			p.y = std::stod(str_v[5]);
			p.z = std::stod(str_v[6]);
			p.oa = std::stod(str_v[7]);
			p.md = std::stod(str_v[8]);
			m.pair.push_back(p);
		}
		for (int i = 0; i < trk_num; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			stop_track s;
			s.pl0 = std::stoi(str_v[0]);
			s.pl1 = std::stoi(str_v[1]);
			s.rawid = std::stoi(str_v[2]);
			s.chainid = std::stoll(str_v[3]);
			s.groupid = std::stoll(str_v[4]);
			s.nseg = std::stoi(str_v[5]);
			s.npl = std::stoi(str_v[6]);
			s.ph = std::stoi(str_v[7]);
			s.ax = std::stod(str_v[8]);
			s.ay = std::stod(str_v[9]);
			s.x = std::stod(str_v[10]);
			s.y = std::stod(str_v[11]);
			s.z = 0;
			double ip = std::stod(str_v[12]);
			m.pl = s.pl1;

			m.trk.push_back(std::make_pair(ip, s));
		}

		multi.push_back(m);
	}

}
std::multimap<int,track_multi> vtx_divide_pl(std::vector<track_multi>&in) {
	std::multimap<int, track_multi> ret;
	for (auto itr = in.begin(); itr != in.end(); itr++) {
		ret.insert(std::make_pair(itr->pl, *itr));
	}
	return ret;
}
void read_start_txt(std::string filename, std::map<int, std::vector<stop_track>> &stop) {
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		stop_track stop_tmp;
		stop_tmp.chainid = stoll(str_v[0]);
		stop_tmp.nseg = stoi(str_v[1]);
		stop_tmp.pl0 = stoi(str_v[2]);
		stop_tmp.pl1 = stoi(str_v[3]);
		stop_tmp.groupid = stoll(str_v[4]);
		stop_tmp.rawid = stoi(str_v[5]);
		stop_tmp.ph = stoi(str_v[6]);
		stop_tmp.ax = stod(str_v[7]);
		stop_tmp.ay = stod(str_v[8]);
		stop_tmp.x = stod(str_v[9]);
		stop_tmp.y = stod(str_v[10]);
		stop_tmp.z = stod(str_v[11]);
		stop_tmp.npl = stop_tmp.pl1 - stop_tmp.pl0 + 1;
		cnt++;
		auto res = stop.find(stop_tmp.pl0);
		if (res == stop.end()) {
			std::vector<stop_track> tmp_s{ stop_tmp };
			stop.insert(std::make_pair(stop_tmp.pl0, tmp_s));
		}
		else {
			res->second.push_back(stop_tmp);
		}
	}

	printf("input fin %d track\n", cnt);
}

void add_attach_track(std::vector<track_multi> &vtx, std::vector<stop_track> &start, std::map<int, double> &z_nominal_map, std::string file_ECC_path) {

	std::vector<stop_track> start_trans = start;
	int pl0 = vtx.begin()->pl;
	int pl1= start.begin()->pl0;
	double z0 = z_nominal_map.at(pl0);
	double z1 = z_nominal_map.at(pl1);

	std::stringstream file_in_corr,file_in_corr_abs;
	file_in_corr<< file_ECC_path << "\\Area0\\0\\align\\corrmap-align-"
		<< std::setw(3) << std::setfill('0') << pl0 << "-"
		<< std::setw(3) << std::setfill('0') << pl1 << ".lst";
	file_in_corr_abs << file_ECC_path << "\\Area0\\0\\align\\corrmap-abs.lst";
	std::vector<corrmap0::Corrmap> corr,corr_abs;
	corrmap0::read_cormap(file_in_corr.str(), corr);
	corrmap0::read_cormap(file_in_corr_abs.str(), corr_abs);

	//vtx,startはmfile座標系
	//basetrack系に変換
	corrmap0::Corrmap param0, param1;
	int flg = 0;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		if (itr->pos[0] / 10 == pl0) {
			param0 = *itr;
			flg++;
		}
		if (itr->pos[0] / 10 == pl1) {
			param1 = *itr;
			flg++;
		}
	}
	if (flg != 2) {
		fprintf(stderr, "corrmap abs not fount pl0=%d,pl1=%d\n", pl0, pl1);
		exit(1);
	}
	double factor= 1 / (param0.position[0] * param0.position[3] - param0.position[1] * param0.position[2]);
	for (auto itr = vtx.begin(); itr != vtx.end(); itr++) {
		double x_tmp, y_tmp;
		x_tmp = itr->x - param0.position[4];
		y_tmp = itr->y - param0.position[5];
		itr->x = factor * (x_tmp * param0.position[3] - y_tmp * param0.position[1]);
		itr->y = factor * (y_tmp *param0.position[0] - x_tmp * param0.position[2]);
		itr->z = z0 + itr->z;
	}
	factor= 1 / (param1.position[0] * param1.position[3] - param1.position[1] * param1.position[2]);
	for (auto itr = start_trans.begin(); itr != start_trans.end(); itr++) {
		double x_tmp, y_tmp;
		x_tmp = itr->x - param1.position[4];
		y_tmp = itr->y - param1.position[5];
		itr->x = factor * (x_tmp * param1.position[3] - y_tmp * param1.position[1]);
		itr->y = factor * (y_tmp *param1.position[0] - x_tmp * param1.position[2]);

		x_tmp = itr->ax - param1.angle[4];
		y_tmp = itr->ay - param1.angle[5];
		itr->ax = factor * (x_tmp * param1.angle[3] - y_tmp * param1.angle[1]);
		itr->ay = factor * (y_tmp *param1.angle[0] - x_tmp * param1.angle[2]);
		itr->z = z1;
	}
	//pl0系に変換
	Apply_alignment1(start_trans, corr);
	//attach探索
	for (auto itr = vtx.begin(); itr != vtx.end(); itr++) {

		std::set<std::pair<uint64_t, double> > attach_chain = search_attach(*itr, start_trans);
		if (attach_chain.size() == 0)continue;
		//printf("event %d attach %d\n", itr->eventid, attach_chain.size());
		std::vector<std::pair<double, stop_track>>attach_chain2 = attach_chain_pickup(attach_chain, start_trans);
		vtx_add_chain(*itr, attach_chain2, param0, z0);
	}

}

//上流-->下流に外挿する際の上流basetrackの変換
void Apply_alignment1(std::vector<stop_track> &track ,std::vector<corrmap0::Corrmap> &corr) {
	//corrmapの範囲を逆変換
	double hash_size = 4000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	corrmap_area_inverse(corr, corr_map, hash_size);
	basetrack_trans(track, corr_map, hash_size);

}
void basetrack_trans(std::vector<stop_track> &track, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size) {
	int ix, iy;
	int count = 0;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		count++;
		std::pair<int, int> id;
		ix = (itr)->x / hash_size;
		iy = (itr)->y / hash_size;
		std::vector<corrmap0::Corrmap> corr_list;
		int loop = 0;
		while (corr_list.size() <= 3) {
			loop++;
			for (int iix = -1 * loop; iix <= 1 * loop; iix++) {
				for (int iiy = -1 * loop; iiy <= 1 * loop; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (corr_map.count(id) == 0)continue;
					auto range = corr_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						corr_list.push_back(res->second);
					}
				}
			}
		}
		double dist;
		corrmap0::Corrmap param;
		for (auto itr2 = corr_list.begin(); itr2 != corr_list.end(); itr2++) {
			double cx, cy;
			cx = (itr2->areax[0] + itr2->areax[1]) / 2;
			cy = (itr2->areay[0] + itr2->areay[1]) / 2;
			if (itr2 == corr_list.begin()) {
				dist = (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy);
				param = *itr2;
			}
			if (dist > (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy)) {
				dist = (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy);
				param = *itr2;
			}

		}
		track_tans_affin(*itr, param);

	}


}
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size) {

	//hash_sizeはcorrmapのサイズより大きくする
	double cx, cy;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		double area[4], factor;
		area[0] = itr->areax[0] - itr->position[4];
		area[1] = itr->areax[1] - itr->position[4];
		area[2] = itr->areay[0] - itr->position[5];
		area[3] = itr->areay[1] - itr->position[5];

		factor = 1 / (itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);
		itr->areax[0] = factor * (area[0] * itr->position[3] - area[2] * itr->position[1]);
		itr->areay[0] = factor * (area[2] * itr->position[0] - area[0] * itr->position[2]);
		itr->areax[1] = factor * (area[1] * itr->position[3] - area[3] * itr->position[1]);
		itr->areay[1] = factor * (area[3] * itr->position[0] - area[1] * itr->position[2]);

		cx = (itr->areax[0] + itr->areax[1]) / 2;
		cy = (itr->areay[0] + itr->areay[1]) / 2;

		std::pair<int, int>id;
		id.first = cx / hash_size;
		id.second = cy / hash_size;
		corr_map.insert(std::make_pair(id, *itr));
	}

}
void track_tans_affin(stop_track  &track, corrmap0::Corrmap corr) {
	double x_tmp, y_tmp;
	x_tmp = track.x;
	y_tmp = track.y;
	track.x = x_tmp * corr.position[0] + y_tmp * corr.position[1] + corr.position[4];
	track.y = x_tmp * corr.position[2] + y_tmp * corr.position[3] + corr.position[5];

	x_tmp = track.ax;
	y_tmp = track.ay;
	track.ax = x_tmp * corr.angle[0] + y_tmp * corr.angle[1] + corr.angle[4];
	track.ay = x_tmp * corr.angle[2] + y_tmp * corr.angle[3] + corr.angle[5];

	track.z = track.z + corr.dz;
}


std::set<std::pair<uint64_t,double> >search_attach(track_multi &vtx, std::vector<stop_track> &track) {
	//chain id & IPを返す
	std::set<std::pair<uint64_t, double> >ret;
	matrix_3D::vector_3D p_vtx,pos,dir;
	p_vtx.x = vtx.x;
	p_vtx.y = vtx.y;
	p_vtx.z = vtx.z;
	double ip_thr,ip;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		pos.x = itr->x;
		pos.y = itr->y;
		pos.z = itr->z;

		dir.x = itr->ax;
		dir.y = itr->ay;
		dir.z = 1;
		ip_thr = 10 + 0.04*fabs(p_vtx.z - pos.z);
		ip = matrix_3D::inpact_parameter(pos, dir, p_vtx);
		if (ip < ip_thr) {
			ret.insert(std::make_pair(itr->chainid, ip));
		}
	}
	return ret;


}
std::vector<std::pair<double, stop_track>> attach_chain_pickup(std::set<std::pair<uint64_t, double> >&attach_chain, std::vector<stop_track> &track) {

	std::vector<std::pair<double, stop_track>> ret;
	std::map<uint64_t, stop_track> track_map;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		track_map.insert(std::make_pair(itr->chainid,*itr));
	}


	for (auto itr = attach_chain.begin(); itr != attach_chain.end(); itr++) {
		auto res = track_map.find(itr->first);
		if (res == track_map.end()) {
			fprintf(stderr, "chainid %lld not found\n", itr->first);
			exit(1);
		}
		ret.push_back(std::make_pair(itr->second, res->second));
	}
	return ret;
}



void vtx_add_chain(track_multi &vtx, std::vector<std::pair<double, stop_track>> &track, corrmap0::Corrmap&param,double z_off) {
	
	//mfile系に変換
	//startはlocal --> mfile
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		double tmp_x, tmp_y;
		tmp_x = itr->second.x;
		tmp_y = itr->second.y;
		itr->second.x = param.position[0] * tmp_x + param.position[1] * tmp_y + param.position[4];
		itr->second.y = param.position[2] * tmp_x + param.position[3] * tmp_y + param.position[5];
		tmp_x = itr->second.ax;
		tmp_y = itr->second.ay;
		itr->second.ax = param.angle[0] * tmp_x + param.angle[1] * tmp_y + param.angle[4];
		itr->second.ay = param.angle[2] * tmp_x + param.angle[3] * tmp_y + param.angle[5];
		itr->second.z = itr->second.z - z_off;
		vtx.trk.push_back(*itr);
	}
	double tmp_x, tmp_y;
	tmp_x = vtx.x;
	tmp_y = vtx.y;
	vtx.x = param.position[0] * tmp_x + param.position[1] * tmp_y + param.position[4];
	vtx.y = param.position[2] * tmp_x + param.position[3] * tmp_y + param.position[5];
	vtx.z = vtx.z - z_off;

	vtx_re_calc_pair(vtx);

}
void vtx_re_calc_pair(track_multi &vtx) {
	//全2trkのmd計算
	int pl = vtx.pl;
	double zrange[2] = { 0,0 };
	if (pl <= 15 || (pl >= 16 && pl % 2 == 0)) {
		zrange[0] = -1000;
	}
	else if (pl % 2 == 1) {
		zrange[0] = -3500;
	}

	double extra[2];
	track_multi multi;
	vtx.pair.clear();
	for (auto itr1 = vtx.trk.begin(); itr1 != vtx.trk.end(); itr1++) {
		for (auto itr2 = std::next(itr1, 1); itr2 != vtx.trk.end(); itr2++) {
			matrix_3D::vector_3D pos0, pos1, dir0, dir1;
			pos0.x = itr1->second.x;
			pos0.y = itr1->second.y;
			pos0.z = itr1->second.z;
			pos1.x = itr2->second.x;
			pos1.y = itr2->second.y;
			pos1.z = itr2->second.z;
			dir0.x = itr1->second.ax;
			dir0.y = itr1->second.ay;
			dir0.z = 1;
			dir1.x = itr2->second.ax;
			dir1.y = itr2->second.ay;
			dir1.z = 1;
			double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, zrange, extra);
			track_pair pair_tmp;
			matrix_3D::vector_3D extra0 = addition(pos0, const_multiple(dir0, extra[0]));
			matrix_3D::vector_3D extra1 = addition(pos1, const_multiple(dir1, extra[1]));

			pair_tmp.x = (extra0.x + extra1.x) / 2;
			pair_tmp.y = (extra0.y + extra1.y) / 2;
			pair_tmp.z = (extra0.z + extra1.z) / 2;
			pair_tmp.md = md;
			pair_tmp.oa = matrix_3D::opening_angle(dir0, dir1);
			if (itr1->second.pl1 <= vtx.pl) {
				pair_tmp.pl0 = itr1->second.pl1;
			}
			else {
				pair_tmp.pl0 = itr1->second.pl0;
			}
			if (itr2->second.pl1 <= vtx.pl) {
				pair_tmp.pl1 = itr2->second.pl1;
			}
			else {
				pair_tmp.pl1 = itr2->second.pl0;
			}
			pair_tmp.raw0 = itr1->second.rawid;
			pair_tmp.raw1 = itr2->second.rawid;
			vtx.pair.push_back(pair_tmp);
		}
	}

}


void output_vtx(std::string filename, std::vector<track_multi> vtx) {
	std::ofstream ofs(filename);
	for (auto itr0 = vtx.begin(); itr0 != vtx.end(); itr0++) {
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr0->eventid << " "
			<< std::setw(4) << std::setprecision(0) << itr0->pl << " "
			<< std::setw(4) << std::setprecision(0) << itr0->trk.size() << " "
			<< std::setw(8) << std::setprecision(1) << itr0->x << " "
			<< std::setw(8) << std::setprecision(1) << itr0->y << " "
			<< std::setw(8) << std::setprecision(1) << itr0->z << std::endl;
		for (auto itr1 = itr0->pair.begin(); itr1 != itr0->pair.end(); itr1++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr1->pl0 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->raw0 << " "
				<< std::setw(4) << std::setprecision(0) << itr1->pl1 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->raw1 << " "
				<< std::setw(8) << std::setprecision(1) << itr1->x << " "
				<< std::setw(8) << std::setprecision(1) << itr1->y << " "
				<< std::setw(8) << std::setprecision(1) << itr1->z << " "
				<< std::setw(6) << std::setprecision(4) << itr1->oa << " "
				<< std::setw(4) << std::setprecision(1) << itr1->md << std::endl;
		}
		for (auto itr1 = itr0->trk.begin(); itr1 != itr0->trk.end(); itr1++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr1->second.pl0 << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.pl1 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->second.rawid << " "
				<< std::setw(12) << std::setprecision(0) << itr1->second.chainid << " "
				<< std::setw(12) << std::setprecision(0) << itr1->second.groupid << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.nseg << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.npl << " "
				<< std::setw(6) << std::setprecision(0) << itr1->second.ph << " "
				<< std::setw(7) << std::setprecision(4) << itr1->second.ax << " "
				<< std::setw(7) << std::setprecision(4) << itr1->second.ay << " "
				<< std::setw(8) << std::setprecision(1) << itr1->second.x << " "
				<< std::setw(8) << std::setprecision(1) << itr1->second.y << " "
				<< std::setw(4) << std::setprecision(1) << itr1->first << std::endl;
		}
	}
}
