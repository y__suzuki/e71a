#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
# include <iostream>
# include <random>
#include <set>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <list>
class stop_track {
public:
	uint64_t chainid, groupid;
	int  nseg, npl, pl0, pl1, ph, rawid;
	double ax, ay, x, y, z;
};
class track_pair {
public:
	int pl0, pl1, raw0, raw1;
	double x, y, z, md, oa;
};
class track_multi {
public:
	int eventid;
	double x, y, z;
	std::vector< std::pair<double, stop_track>> trk;
	std::vector<track_pair>pair;
};

void read_vtx_file(std::string filename, std::vector<track_multi>&multi);
void output_vtx_inf(std::string file_path, std::vector<track_multi>&multi);
void output_vtx_pair_inf(std::string file_path, std::vector<track_multi>&multi);

double black_threshold(double angle);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg in-vtx out_path\n");
		exit(1);
	}
	std::string file_in_vtx = argv[1];
	std::string file_out_path = argv[2];
	std::vector<track_multi> multi;
	read_vtx_file(file_in_vtx, multi);

	output_vtx_inf(file_out_path, multi);
	output_vtx_pair_inf(file_out_path, multi);
}
void read_vtx_file(std::string filename, std::vector<track_multi>&multi) {
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0, trk_num;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);

		track_multi m;
		m.eventid = std::stoi(str_v[0]);
		m.x = std::stod(str_v[2]);
		m.y = std::stod(str_v[3]);
		m.z = std::stod(str_v[4]);
		trk_num = std::stoi(str_v[1]);

		for (int i = 0; i < trk_num*(trk_num - 1) / 2; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			track_pair p;
			p.pl0 = std::stoi(str_v[0]);
			p.raw0 = std::stoi(str_v[1]);
			p.pl1 = std::stoi(str_v[2]);
			p.raw1 = std::stoi(str_v[3]);
			p.x = std::stod(str_v[4]);
			p.y = std::stod(str_v[5]);
			p.z = std::stod(str_v[6]);
			p.oa = std::stod(str_v[7]);
			p.md = std::stod(str_v[8]);
			m.pair.push_back(p);
		}
		for (int i = 0; i < trk_num; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			stop_track s;
			s.pl0 = std::stoi(str_v[0]);
			s.pl1 = std::stoi(str_v[1]);
			s.rawid = std::stoi(str_v[2]);
			s.chainid = std::stoll(str_v[3]);
			s.groupid = std::stoll(str_v[4]);
			s.nseg = std::stoi(str_v[5]);
			s.npl = std::stoi(str_v[6]);
			s.ph = std::stoi(str_v[7]);
			s.ax = std::stod(str_v[8]);
			s.ay = std::stod(str_v[9]);
			s.x = std::stod(str_v[10]);
			s.y = std::stod(str_v[11]);
			s.z = 0;
			double ip = std::stod(str_v[12]);
			m.trk.push_back(std::make_pair(ip, s));
		}
		multi.push_back(m);
	}

}

void output_vtx_inf(std::string file_path, std::vector<track_multi>&multi) {
	std::stringstream file_out_inf;
	file_out_inf << file_path << "\\vtx_inf.txt";
	std::ofstream ofs(file_out_inf.str());

	for (auto v : multi) {
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << v.eventid << " "
			<< std::setw(4) << std::setprecision(0) << v.trk[0].second.pl1 << " "
			<< std::setw(4) << std::setprecision(0) << v.trk.size() << " "
			<< std::setw(8) << std::setprecision(1) << v.x << " "
			<< std::setw(8) << std::setprecision(1) << v.y << " "
			<< std::setw(8) << std::setprecision(1) << v.z << std::endl;
	}

}
void output_vtx_pair_inf(std::string file_path, std::vector<track_multi>&multi) {
	std::stringstream file_out_inf;
	file_out_inf << file_path << "\\vtx_pair_inf.txt";
	std::ofstream ofs(file_out_inf.str());

	for (auto v : multi) {
		std::map<std::pair<int, int>, stop_track> track;
		for (auto t : v.trk) {
			track.insert(std::make_pair(std::make_pair(t.second.pl1, t.second.rawid), t.second));
		}
		for (auto p : v.pair) {
			stop_track tr[2];
			tr[0] = track.at(std::make_pair(p.pl0, p.raw0));
			tr[1] = track.at(std::make_pair(p.pl1, p.raw1));
			ofs << std::right << std::fixed
				<< std::setw(10) << std::setprecision(0) << v.eventid << " "
				<< std::setw(4) << std::setprecision(0) << v.trk[0].second.pl1 << " "
				<< std::setw(4) << std::setprecision(0) << v.trk.size() << " "
				<< std::setw(7) << std::setprecision(4) << tr[0].ax << " "
				<< std::setw(7) << std::setprecision(4) << tr[0].ay << " "
				<< std::setw(8) << std::setprecision(0) << tr[0].ph << " "
				<< std::setw(4) << std::setprecision(0) << tr[0].npl << " "
				<< std::setw(7) << std::setprecision(4) << tr[1].ax << " "
				<< std::setw(7) << std::setprecision(4) << tr[1].ay << " "
				<< std::setw(8) << std::setprecision(0) << tr[1].ph << " "
				<< std::setw(4) << std::setprecision(0) << tr[1].npl << " "
				<< std::setw(8) << std::setprecision(1) << p.x << " "
				<< std::setw(8) << std::setprecision(1) << p.y << " "
				<< std::setw(8) << std::setprecision(1) << p.z << " "
				<< std::setw(5) << std::setprecision(1) << p.md << " "
				<< std::setw(5) << std::setprecision(4) << p.oa << std::endl;
		}
	}

}

double black_threshold(double angle) {
	if (angle < 1.0) {
		return angle * -70 + 190;
	}
	else if (1.0 <= angle && angle < 2.0) {
		return angle * -30 + 150;
	}
	else if (2.0 <= angle && angle < 3.0) {
		return angle * -10 + 110;
	}
	else {
		return angle * -5 + 95;
	}
	return 200;
}

