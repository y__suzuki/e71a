#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_format{
public:
	int groupid, chainid,nseg,npl,vph_count;
	float vph_mean, vph_error, range_min, range_max, ecc_mcs_mom,angle;
};
std::vector<output_format> Calc_range_vph(std::vector<Momentum_recon::Mom_chain> &momch);

void Calc_vph_mean(std::vector<Momentum_recon::Mom_basetrack>&base, output_format &out);
void count_material(int pl_min, int pl_max, int &iron, int &water, int &film);
double Add_range_up(Momentum_recon::Mom_basetrack&b);
double Add_range_down(Momentum_recon::Mom_basetrack&b);
void Calc_range(Momentum_recon::Mom_chain&c, output_format &out);
double Calc_material_range(double density, double thickness);
void output_file_bin(std::string filename, std::vector<output_format>&out);
double Calc_angle(Momentum_recon::Mom_chain&c);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::vector<output_format>out = Calc_range_vph(momch);

	 output_file_bin(file_out_momch,out);

	exit(0);



}
std::vector<output_format> Calc_range_vph(std::vector<Momentum_recon::Mom_chain> &momch) {
	std::vector<output_format> ret;

	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		output_format out;
		out.ecc_mcs_mom = itr->ecc_mcs_mom;
		out.groupid = itr->groupid;
		out.chainid = itr->chainid;
		out.npl = itr->base.rbegin()->pl - itr->base.begin()->pl + 1;
		out.nseg = itr->base.size();
		out.angle = Calc_angle(*itr);
		Calc_vph_mean(itr->base, out);
		Calc_range(*itr, out);
		ret.push_back(out);
	}
	return ret;
}
void Calc_vph_mean(std::vector<Momentum_recon::Mom_basetrack>&base, output_format &out) {
	double sum=0, sum2 = 0;
	int count = 0,vph;
	for (auto &b : base) {
		for (int i = 0; i < 2; i++) {
			vph = b.m[i].ph % 10000;
			sum += vph;
			sum2 += vph * vph;
			count++;
		}
	}
	out.vph_count = count;
	if (count < 1) {
		out.vph_mean = -1;
		out.vph_error = -1;
		return;
	}
	out.vph_mean = sum / count;
	double sd = sum2 / count - pow(sum / count, 2);
	if (sd <= 0.001) {
		out.vph_error = sqrt(out.vph_mean);
	}
	else {
		out.vph_error = sqrt(sd)*sqrt(count) / sqrt(count - 1);
	}
}
void Calc_range(Momentum_recon::Mom_chain&c, output_format &out) {
	int iron, water, film;
	double dz,distance, thick_iron_ratio, thick_water_ratio, thick_film_ratio;

	//密度 g/cm^3
	double water_density = 1.000;
	double iron_density = 7.874;
	double film_density = 2;
	out.range_min = 0;
	out.range_max = 0;
	for (auto itr = c.base_pair.begin(); itr != c.base_pair.end(); itr++) {
		count_material(itr->first.pl, itr->second.pl, iron, water, film);
		dz = fabs(itr->first.z - itr->second.z);
		distance = sqrt(pow(itr->first.x - itr->second.x, 2) + pow(itr->first.y - itr->second.y, 2) + pow(itr->first.z - itr->second.z, 2));

		if (water == 0) {
			thick_film_ratio = (film * 350) / (iron * 500 + film * 350);
			thick_iron_ratio = (iron * 500) / (iron * 500 + film * 350);
			thick_water_ratio = 0;
		}
		else {
			thick_film_ratio = (film * 350) / dz;
			thick_iron_ratio = (iron * 500) / dz;
			thick_water_ratio = 1 - thick_film_ratio - thick_iron_ratio;
		}

		out.range_min += Calc_material_range(iron_density, thick_iron_ratio*distance);
		out.range_min += Calc_material_range(water_density, thick_water_ratio*distance);
		out.range_min += Calc_material_range(film_density, thick_film_ratio*distance);

	}
	out.range_max = out.range_min;
	out.range_max += Add_range_down(*c.base.begin());
	out.range_max += Add_range_up(*c.base.rbegin());

}
double Calc_material_range(double density,double thickness) {
	//thick->um
	//density->g/cm^3
	//return g/cm^2
	return density * thickness / 10000;
}
void count_material(int pl_min, int pl_max, int &iron, int &water, int &film) {
	iron = 0;
	water = 0;
	film = 0;

	//下流pl
	for (int pl = pl_min; pl < pl_max; pl++) {
		film += 1;
		if (pl == 3)continue;
		else if (4 <= pl && pl <= 14)iron += 1;
		else if (pl == 15)continue;
		else if (16 <= pl && pl % 2 == 0)iron += 1;
		else if (16 <= pl && pl % 2 == 1)water += 1;
	}
}
double Add_range_up(Momentum_recon::Mom_basetrack&b) {
	double water_density = 1.000;
	double iron_density = 7.874;
	double film_density = 2;

	int pl = b.pl;
	int iron = 0;
	int water = 0;
	int film = 0;
	double path = sqrt(1 + b.ax*b.ax + b.ay*b.ay);
	//下流pl
	film += 1;
	if (pl == 3) {}
	else if (4 <= pl && pl <= 14)iron += 1;
	else if (pl == 15) {}
	else if (16 <= pl && pl % 2 == 0)iron += 1;

	double ret = 0;
	ret += Calc_material_range(water_density, 2300 * water * path);
	ret += Calc_material_range(iron_density, 500 * iron * path);
	ret += Calc_material_range(film_density, 350 * film * path);
	return ret;
}
double Add_range_down(Momentum_recon::Mom_basetrack&b) {
	double water_density = 1.000;
	double iron_density = 7.874;
	double film_density = 2;

	int pl = b.pl-1;
	int iron = 0;
	int water = 0;
	int film = 0;
	double path = sqrt(1 + b.ax*b.ax + b.ay*b.ay);
	//下流pl
	film += 1;
	if (pl == 3) {}
	else if (4 <= pl && pl <= 14)iron += 1;
	else if (pl == 15) {}
	else if (16 <= pl && pl % 2 == 0)iron += 1;

	double ret = 0;
	ret += Calc_material_range(water_density, 2300 * water * path);
	ret += Calc_material_range(iron_density, 500 * iron * path);
	ret += Calc_material_range(film_density, 350 * film * path);
	return ret;
}
double Calc_angle(Momentum_recon::Mom_chain&c) {
	double ax = 0;
	double ay = 0;
	int count = 0;
	for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
		ax += itr->ax;
		ay += itr->ay;
		count +=1;
	}
	ax = ax / count;
	ay = ay / count;
	return sqrt(ax*ax + ay * ay);


}
void output_file_bin(std::string filename, std::vector<output_format>&out) {
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (out.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = out.size();
	for (int i = 0; i < out.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& out[i], sizeof(output_format));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}

