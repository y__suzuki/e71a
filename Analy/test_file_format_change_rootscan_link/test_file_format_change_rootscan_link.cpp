#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class link_inf {
public:
	int pl0, pl1, rawid0, rawid1;
};


std::vector<link_inf> Read_file(std::string filename);
void output_file_bin(std::string filename, std::vector<link_inf> &link);


int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in \n");
		exit(1);
	}
	std::string file_in = argv[1];
	std::string file_out_bin = argv[2];

	std::vector<link_inf> link = Read_file(file_in);
	printf("file size = %d\n", link.size());
	output_file_bin(file_out_bin, link);

}

std::vector<link_inf> Read_file(std::string filename) {
	std::vector<link_inf> ret;
	std::ifstream ifs(filename);
	link_inf l;
	std::string str;
	int count = 0;
	while (ifs>>l.pl0>>l.pl1>>l.rawid0>>l.rawid1){
		count++;
		if (count % 10000 == 0) {
			printf("\r read file %d", count);
		}
		ret.push_back(l);
	}
	printf("\r read file %d\n", count);

	return ret;

}

void output_file_bin(std::string filename, std::vector<link_inf> &link) {
	std::ofstream ofs(filename, std::ios::binary);
	int count = 0, all = link.size();
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r write file %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		ofs.write((char*)&*itr, sizeof(link_inf));
	}
	printf("\r write file %d/%d(%4.1lf%%)\n", count, all, count*100. / all);


}
