#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <cassert>  // std::assert
#include <sstream>
#include <iomanip>
#include <string>

#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <VxxReader.h>
#include <picojson.h>
#include <omp.h>
#include <time.h>
#include <filesystem>

std::vector<cv::Mat> read_spng(std::string filename);
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin);
void write_spng(std::string filename, std::vector<cv::Mat> &mat0);
template <class T> void write_vbin(std::ofstream& ofs, const std::vector<T>& vout);

void write_png(std::string filename, std::vector<cv::Mat> &mat0);
void check_image(cv::Mat &mat);

int main(int argc, char**argv) {
	//if (argc != 4) {
	//	fprintf(stderr, "usage:in-file out-file\n");
	//	exit(1);
	//}

	std::string file_in_name = "I:\\NINJA\\E71a\\work\\suzuki\\image_bug\\Area2\\PL027\\DATA\\02_02\\TrackHit2_0_00000006_0_000.spng";
	std::string file_out_name = "I:\\NINJA\\E71a\\work\\suzuki\\image_bug\\test";
	std::vector<cv::Mat> vmat = read_spng(file_in_name);
	printf("size = %d\n", vmat.size());
	std::filesystem::create_directory(file_out_name);
	for (int i = 0; i < vmat.size(); i++) {
		check_image(vmat[i]);
	}

	write_png(file_out_name + "\\out",vmat);
}

void write_spng(std::string filename, std::vector<cv::Mat> &mat0) {
	std::vector<int> params = std::vector<int>(2);
	params[0] = cv::IMWRITE_PNG_STRATEGY;
	params[1] = cv::IMWRITE_PNG_STRATEGY_RLE;
	//出力ファイル名を作ってるだけ
	std::ofstream ofs;
	ofs.exceptions(std::ios::failbit | std::ios::badbit);
	ofs.open(filename, std::ios::binary);
	for (int i = 0; i < 32; i++)
	{
		std::vector<uchar> buf;
		cv::imencode(".png", mat0.at(i), buf, params);
		write_vbin(ofs, buf);
	}
	ofs.close();
}
template <class T> void write_vbin(std::ofstream& ofs, const std::vector<T>& vout)
{
	uint64_t j64 = vout.size();
	if (j64 == 0) { j64 = -1; }
	ofs.write((char*)&j64, sizeof(uint64_t));
	ofs.write((char*)vout.data(), sizeof(T) * vout.size());
}

std::vector<cv::Mat> read_spng(std::string filename) {
	std::vector<std::vector<uchar>> vvin;
	read_vbin(filename, vvin);
	std::vector<cv::Mat> vmat1;
	for (int i = 0; i < vvin.size(); i++) {
		printf("start %d\n", i);
		cv::Mat mat1;
		try
		{
			 mat1= cv::imdecode(vvin[i], 0);
		}
		catch (cv::Exception& e)
		{
			const char* err_msg = e.what();
			printf("%s\n", err_msg);
			printf("hoge\n");
		}
		//if (mat1 == NONE)printf("\nerror\n");
		vmat1.emplace_back(mat1);
		printf("end %d\n", i);

	}
	return vmat1;
}
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin)
{
	std::ifstream ifs(filepath, std::ios::binary);
	if (!ifs.is_open()) { std::cout << "cannot open " << filepath << std::endl; throw std::exception(); }
	int i = 0;
	while (true)
	{
		uint64_t i64;
		ifs.read((char*)&i64, sizeof(uint64_t));
		if (ifs.eof()) { break; }

		printf("%d i64 = %llu\n",i, i64);
		i++;

		std::vector<T> vin;
		if (i64 == -1)
		{
			vvin.push_back(vin);
			continue;
		}
		if (i64 > vin.max_size()) { throw std::exception(""); }
		vin.reserve(i64);
		for (uint64_t j = 0; j < i64; j++)
		{
			T p;
			ifs.read((char*)&p, sizeof(T));
			vin.push_back(p);
		}
		vvin.push_back(vin);
	}
	ifs.close();
}

void write_png(std::string filename, std::vector<cv::Mat> &mat0) {

	int width = mat0[1].cols;
	int height = mat0[1].rows;

	std::vector<cv::Mat> vmat_color;
	for (int i = 1; i < mat0.size(); i++) {
		cv::Mat image = cv::Mat::zeros(height, width, CV_8UC3);
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				if (mat0[i].at<uchar>(y, x) == 0x00) {
					image.at<cv::Vec3b>(y, x)[0] = 0x00; //青
					image.at<cv::Vec3b>(y, x)[1] = 0x00; //緑
					image.at<cv::Vec3b>(y, x)[2] = 0x00; //赤
				}
				else {
					image.at<cv::Vec3b>(y, x)[0] = 0xff; //青
					image.at<cv::Vec3b>(y, x)[1] = 0xff; //緑
					image.at<cv::Vec3b>(y, x)[2] = 0xff; //赤
				}
			}
		}
		vmat_color.push_back(image);
	}

	std::vector<int> params = std::vector<int>(2);
	params[0] = cv::IMWRITE_PNG_STRATEGY;
	params[1] = cv::IMWRITE_PNG_STRATEGY_RLE;
	std::string out_filename;
	for (int k = 0; k < vmat_color.size(); k++) {
		{
			std::stringstream ss;
			ss << filename << "_" << std::setfill('0') << std::setw(3) << k << ".png";
			out_filename = ss.str();
		}
		cv::imwrite(out_filename, vmat_color[k]);
	}
}

void check_image(cv::Mat &mat) {
	if (mat.data == NULL) {
		printf("hoge\n");
		return;
	}

	int width = mat.cols;
	int height = mat.rows;
	printf("%d %d\n", width, height);
}