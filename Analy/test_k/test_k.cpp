#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<mfile0::M_Chain> track_selection_PL(std::vector<mfile0::M_Chain>&chain, int pl, int num_pl_up, int num_pl_down);
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold);
void output_base_rawid(std::string filename, std::vector<mfile0::M_Chain>  &chain, int pl);
std::vector<mfile0::M_Chain> chain_nseg_selection(std::vector<mfile0::M_Chain> &chain, int nseg);
std::vector<mfile0::M_Chain> reject_Fe_ECC(std::vector<mfile0::M_Chain> &chain);
std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>  &chain);
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold);
std::vector<mfile0::M_Chain> chain_angle_selection(std::vector<mfile0::M_Chain>  &chain, double thr_ax, double thr_ay);

int main(int argc, char** argv) {

	if (argc != 3) {
		fprintf(stderr, "exception\n");
		exit(1);
	}

	std::string filename_mfile = argv[1];
	std::string filename_output = argv[2];

	mfile0::Mfile m;

	//filename_mfileで指定したファイルを読みこみ --> mに格納
	mfile1::read_mfile_extension(filename_mfile, m);
	m.chains = chain_nseg_selection(m.chains, 15);
	m.chains = reject_Fe_ECC(m.chains);
	m.chains = chain_angle_selection(m.chains, 4.0, 4.0);
	m.chains = chain_dlat_selection(m.chains, 0.005);
	m.chains = group_clustering(m.chains);

	m.chains = track_selection_PL(m.chains, 71, 4, 4);
	//m.chains = chain_dlat_selection(m.chains, 0.01);
	
	output_base_rawid(filename_output, m.chains, 70);

}
std::vector<mfile0::M_Chain> chain_nseg_selection(std::vector<mfile0::M_Chain> &chain, int nseg) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr->nseg < nseg)continue;
		ret.push_back(*itr);
	}
	printf("chain nseg >= %d: %d --> %d (%4.1lf%%)\n", nseg, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::M_Chain> reject_Fe_ECC(std::vector<mfile0::M_Chain> &chain) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr->pos0 / 10 > 15) {
			ret.push_back(*itr);
		}
		else {
			int flg = 0;
			for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
				if (itr2->pos / 10 == 16)flg++;
				if (itr2->pos / 10 == 17)flg++;
				if (itr2->pos / 10 == 18)flg++;
				if (itr2->pos / 10 == 19)flg++;
				if (itr2->pos / 10 == 20)flg++;
				if (itr2->pos / 10 == 21)flg++;
				if (itr2->pos / 10 == 22)flg++;
				if (itr2->pos / 10 == 23)flg++;
			}
			if (flg >= 6 || itr->pos1 / 10 > 23) {
				ret.push_back(*itr);
			}
		}
	}
	printf("reject_Fe_ECC(PL1 >= 24): %d --> %d (%4.1lf%%)\n", chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>  &chain) {

	std::vector<mfile0::M_Chain> ret;
	std::set<int>gid;
	std::multimap<int, mfile0::M_Chain*>group;
	int nseg_max;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		gid.insert(itr->basetracks[0].group_id);
		group.insert(std::make_pair(itr->basetracks[0].group_id, &(*itr)));
	}
	for (auto itr = gid.begin(); itr != gid.end(); itr++) {
		if (group.count(*itr) == 1) {
			ret.push_back(*(group.find(*itr)->second));
		}
		else {
			auto range = group.equal_range(*itr);
			nseg_max = 1;
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				nseg_max = std::max(itr2->second->nseg, nseg_max);
			}
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (itr2->second->nseg == nseg_max) {
					ret.push_back(*(itr2->second));
					break;
				}
			}
		}
	}
	printf("chain group clustering: %d --> %d (%4.1lf%%)\n", chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;

}
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (mfile0::angle_diff_dev_lat(*itr, ax, ay) > threshold)continue;
		ret.push_back(*itr);
	}
	printf("chain lateral selection <= %5.4lf : %d --> %d (%4.1lf%%)\n", threshold, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;

}
std::vector<mfile0::M_Chain> chain_angle_selection(std::vector<mfile0::M_Chain>  &chain, double thr_ax, double thr_ay) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (thr_ax - fabs(ax) < 0)continue;
		if (thr_ay - fabs(ay) < 0)continue;
		ret.push_back(*itr);
	}
	printf("chain average angle selection |ax|<=%3.1lf |ay|<=%3.1lf : %d --> %d (%4.1lf%%)\n", thr_ax, thr_ay, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}

std::vector<mfile0::M_Chain> track_selection_PL(std::vector<mfile0::M_Chain>&chain, int pl,int num_pl_up,int num_pl_down) {

	std::vector<mfile0::M_Chain> ret;
	int hit_up_num, hit_down_num, base_pl,pl_distance;
	int64_t count = 0;
	for (int64_t i = 0; i < chain.size(); i++) {
		if (count % 10000 == 0) {
			printf("\r track selection PL%03d %lld/%lld(%4.1lf%%)", pl, count, chain.size(), count*100. / chain.size());
		}
		count++;

		//初期化
		hit_down_num = 0;
		hit_up_num = 0;
		//i番目のchainを構成するbasetrackについてloop
		for (int j = 0; j < chain[i].basetracks.size(); j++) {
			base_pl = chain[i].basetracks[j].pos / 10;
			pl_distance = abs(base_pl - pl);
			//下流側
			if (base_pl < pl) {
				if (pl_distance <= num_pl_down)hit_down_num++;
			}
			//上流側
			else if (base_pl > pl) {
				if (pl_distance <= num_pl_up)hit_up_num++;
			}
		}
		if (hit_up_num == num_pl_up && hit_down_num == num_pl_down) {
			ret.push_back(chain[i]);
		}

	}
	printf("\r track selection PL%03d %lld/%lld(%4.1lf%%)\n", pl, count, chain.size(), count*100. / chain.size());

	printf("chain num %lld --> %lld (%4.1lf%%)\n", chain.size(), ret.size(), ret.size()*100. / chain.size());

	return ret;
}

void output_base_rawid(std::string filename, std::vector<mfile0::M_Chain>  &chain, int pl) {

	std::ofstream ofs(filename);
	int base_pl;
	int64_t count = 0;
	std::set<int>rawid;
	for (int64_t i = 0; i < chain.size(); i++) {
		if (count % 10000 == 0) {
			printf("\r output base rawid %lld/%lld(%4.1lf%%)", count, chain.size(), count*100. / chain.size());
		}
		count++;

		for (int j = 0; j < chain[i].basetracks.size(); j++) {
			base_pl = chain[i].basetracks[j].pos / 10;
			if (base_pl == pl) {
				if (rawid.count(chain[i].basetracks[j].rawid) == 1)continue;
				rawid.insert(chain[i].basetracks[j].rawid);

				ofs << std::right << std::fixed
					<< std::setw(5) << base_pl << " "
					<< std::setw(12) << chain[i].basetracks[j].rawid << std::endl;
			}
		}
	}
	printf("\r output base rawid %lld/%lld(%4.1lf%%)\n", count, chain.size(), count*100. / chain.size());
	printf("number of rawid = %d\n", rawid.size());
}