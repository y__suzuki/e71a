#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<vxx::base_track_t> read_base(std::string file_ECC, int pl);
std::map<std::pair< int, int>, std::vector<Sharing_file::Sharing_file>>SharingFile_spot_divide(std::vector<Sharing_file::Sharing_file> &sf);
void output_base(std::string file_out, std::map<std::pair< int, int>, std::vector<Sharing_file::Sharing_file>>&sf, std::vector<vxx::base_track_t>&base_3, std::vector<vxx::base_track_t>&base_4);
std::vector<netscan::base_track_t> extract_base(int pl, int spot, std::map<std::pair< int, int>, std::vector<Sharing_file::Sharing_file>>&sf, std::vector<vxx::base_track_t>&base);
std::vector<vxx::base_track_t> extract_bvxx(int pl, int spot, std::map<std::pair< int, int>, std::vector<Sharing_file::Sharing_file>>&sf, std::vector<vxx::base_track_t>&base);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-file-sf ECC-path output-path\n");
		exit(1);
	}
	std::string file_in_sf = argv[1];
	std::string file_ECC = argv[2];
	std::string file_out_path = argv[3];

	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_bin(file_in_sf);
	std::map<std::pair< int, int>, std::vector<Sharing_file::Sharing_file>>sf_map = SharingFile_spot_divide(sf);
	std::vector<vxx::base_track_t> base_3 = read_base(file_ECC, 3);
	std::vector<vxx::base_track_t> base_4 = read_base(file_ECC, 4);

	std::stringstream file_in_align;
	file_in_align << file_ECC << "\\Area0\\0\\align\\fine\\ali_003_004_interpolation.txt";
	std::vector <corrmap_3d::align_param > corr = corrmap_3d::read_ali_param(file_in_align.str(), false);
	std::vector <corrmap_3d::align_param2 >corr2 = DelaunayDivide(corr);
	std::vector < std::pair<vxx::base_track_t*, corrmap_3d::align_param2*>> track_param = track_affineparam_correspondence(base_4, corr2);
	corrmap_3d::trans_base_all(track_param);
	for (auto itr = base_4.begin(); itr != base_4.end(); itr++) {
		itr->m[0].z += itr->z;
		itr->m[1].z += itr->z;
	}
	output_base(file_out_path, sf_map, base_3, base_4);

}
std::map<std::pair< int, int>, std::vector<Sharing_file::Sharing_file>>SharingFile_spot_divide(std::vector<Sharing_file::Sharing_file> &sf) {

	std::multimap<std::pair<int, int>, Sharing_file::Sharing_file> sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(std::make_pair(itr->pl, itr->spotid), *itr));
	}
	std::map<std::pair< int, int>, std::vector<Sharing_file::Sharing_file>> ret;
	for (auto itr = sf_map.begin(); itr != sf_map.end(); itr++) {

		std::vector<Sharing_file::Sharing_file> sf_v;
		auto range = sf_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			sf_v.push_back(res->second);
		}
		ret.insert(std::make_pair(itr->first, sf_v));

		itr = std::next(itr, sf_map.count(itr->first) - 1);
	}
	return ret;

}

std::vector<vxx::base_track_t> read_base(std::string file_ECC, int pl) {
	std::stringstream file_in_base;
	file_in_base << file_ECC << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), pl, 0);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		itr->z = 0;
		itr->m[1].z = itr->m[1].z - itr->m[0].z;
		itr->m[0].z = 0;

		//itr->m[0].z = itr->m[0].z - itr->m[1].z;
		//itr->m[1].z = 0;
	}
	return base;
}

void output_base(std::string file_out,std::map<std::pair< int, int>, std::vector<Sharing_file::Sharing_file>>&sf, std::vector<vxx::base_track_t>&base_3, std::vector<vxx::base_track_t>&base_4) {
	std::set<int> spotid;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		spotid.insert(itr->first.second);
	}

	vxx::BvxxWriter bw;
	for (auto itr = spotid.begin(); itr != spotid.end(); itr++) {
		printf("spot %d\n", *itr);
		std::vector<netscan::base_track_t> out_base;
		std::stringstream file_out_base,file_out_bvxx[2];
		file_out_base << file_out << "\\base_" << std::setw(4) << std::setfill('0') << *itr << ".txt";
		file_out_bvxx[0] << file_out << "\\b003_" << std::setw(4) << std::setfill('0') << *itr << ".vxx";
		file_out_bvxx[1] << file_out << "\\b004_" << std::setw(4) << std::setfill('0') << *itr << ".vxx";
		//PL003;
		std::vector<netscan::base_track_t> base_sel3 = extract_base(3, *itr, sf, base_3);
		std::vector<netscan::base_track_t> base_sel4 = extract_base(4, *itr, sf, base_4);
		std::vector<vxx::base_track_t> bvxx_sel3 = extract_bvxx(3, *itr, sf, base_3);
		std::vector<vxx::base_track_t> bvxx_sel4 = extract_bvxx(4, *itr, sf, base_4);

		for (int i = 0; i < base_sel3.size(); i++) {
			out_base.push_back(base_sel3[i]);
		}
		for (int i = 0; i < base_sel4.size(); i++) {
			out_base.push_back(base_sel4[i]);
		}
		netscan::write_basetrack_txt(file_out_base.str(), out_base);
		bw.Write(file_out_bvxx[0].str(), 3, 0, bvxx_sel3);
		bw.Write(file_out_bvxx[1].str(), 4, 0, bvxx_sel4);

	}
}
std::vector<netscan::base_track_t> extract_base(int pl, int spot, std::map<std::pair< int, int>, std::vector<Sharing_file::Sharing_file>>&sf, std::vector<vxx::base_track_t>&base) {
	std::vector<netscan::base_track_t> ret;
	if (sf.count(std::make_pair(pl, spot)) == 0)return ret;

	std::vector<Sharing_file::Sharing_file> sf_v = sf.at(std::make_pair(pl, spot));


	std::map<std::tuple<int, int, int, int>, netscan::base_track_t> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::tuple<int, int, int, int> id;
		std::get<0>(id) = itr->m[0].zone;
		std::get<1>(id) = itr->m[0].rawid;
		std::get<2>(id) = itr->m[1].zone;
		std::get<3>(id) = itr->m[1].rawid;

		netscan::base_track_t b;
		b.ax = itr->ax;
		b.ay = itr->ay;
		b.x = itr->x;
		b.y = itr->y;
		b.z = itr->z;
		b.isg = itr->isg;
		b.pl = itr->pl;
		b.rawid = itr->rawid;
		b.zone = itr->zone;
		b.dmy = itr->dmy;
		for (int i = 0; i < 2; i++) {
			b.m[i].ax = itr->m[i].ax;
			b.m[i].ay = itr->m[i].ay;
			b.m[i].col = itr->m[i].col;
			b.m[i].row = itr->m[i].row;
			b.m[i].isg = itr->m[i].isg;
			b.m[i].ph = itr->m[i].ph;
			b.m[i].pos = itr->m[i].pos;
			b.m[i].rawid = itr->m[i].rawid;
			b.m[i].z = itr->m[i].z;
			b.m[i].zone = itr->m[i].zone;
		}
		base_map.insert(std::make_pair(id, b));

	}

	for (auto itr = sf_v.begin(); itr != sf_v.end(); itr++) {
		std::tuple<int, int, int, int> id;
		std::get<0>(id) = itr->zone[0];
		std::get<1>(id) = itr->rawid[0];
		std::get<2>(id) = itr->zone[1];
		std::get<3>(id) = itr->rawid[1];


		if (base_map.count(id) == 0) {
			fprintf(stderr, "basetrack not found PL%03d\n", pl);
			fprintf(stderr, "%d %d %d %d\n", itr->zone[0], itr->rawid[0], itr->zone[1], itr->rawid[1]);
			continue;
		}

		ret.push_back(base_map.at(id));
	}
	return ret;

}
std::vector<vxx::base_track_t> extract_bvxx(int pl, int spot, std::map<std::pair< int, int>, std::vector<Sharing_file::Sharing_file>>&sf, std::vector<vxx::base_track_t>&base) {
	std::vector<vxx::base_track_t> ret;
	if (sf.count(std::make_pair(pl, spot)) == 0)return ret;

	std::vector<Sharing_file::Sharing_file> sf_v = sf.at(std::make_pair(pl, spot));


	std::map<std::tuple<int, int, int, int>, vxx::base_track_t> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::tuple<int, int, int, int> id;
		std::get<0>(id) = itr->m[0].zone;
		std::get<1>(id) = itr->m[0].rawid;
		std::get<2>(id) = itr->m[1].zone;
		std::get<3>(id) = itr->m[1].rawid;

		base_map.insert(std::make_pair(id, *itr));

	}

	for (auto itr = sf_v.begin(); itr != sf_v.end(); itr++) {
		std::tuple<int, int, int, int> id;
		std::get<0>(id) = itr->zone[0];
		std::get<1>(id) = itr->rawid[0];
		std::get<2>(id) = itr->zone[1];
		std::get<3>(id) = itr->rawid[1];


		if (base_map.count(id) == 0) {
			fprintf(stderr, "basetrack not found PL%03d\n", pl);
			fprintf(stderr, "%d %d %d %d\n", itr->zone[0], itr->rawid[0], itr->zone[1], itr->rawid[1]);
			continue;
		}

		ret.push_back(base_map.at(id));
	}
	return ret;

}