//#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <filesystem>
#include <fstream>
#include <map>
class ConnectedList {
public:
	int ix, iy, label;
};
class GrainList {
public:
	int clusterid, pixelnum;
	double x, y, brightness_sum, brightness_sum9, brightness_max;
	bool flg;
	std::set<std::pair<int, int>> pixel_pos;

};
bool sort_ConnectedList(const ConnectedList &left, const ConnectedList &right) {
	return left.label < right.label;
}
class Grain3D {
public:
	int layer;
	double x, y, z;
	GrainList gr;
};

std::vector<cv::Mat> read_image(std::string file_path, int num);
std::vector<cv::Mat> image_triming(std::vector<cv::Mat> &image, std::string file_out_path, int index, bool output);
std::vector<cv::Mat> binaryzation(std::vector<cv::Mat>&image, std::string file_out_path, int index, bool output=false);
std::vector<cv::Mat> BW_inversion(std::vector<cv::Mat>&image, std::string file_out_path, int index, bool output=false);
std::vector<cv::Mat> blur(std::vector<cv::Mat>&image, std::string file_out_path, int index, bool output);
std::vector<cv::Mat> subtraction(std::vector<cv::Mat>&image, std::vector<cv::Mat>&bg, std::string file_out_path, int index, bool output);

std::vector<ConnectedList> GetConnectList(cv::Mat &image);
std::vector<GrainList> GetGrainList(cv::Mat &image, std::vector<ConnectedList>&connect_list, cv::Mat &image_bg);
std::vector<GrainList> GrainSeparation(cv::Mat &image, std::vector<GrainList>&grain, cv::Mat &image_br);
std::set<std::pair<int, int>> GetPeak(cv::Mat &image, GrainList &gr, cv::Mat &image_br);

std::vector<Grain3D>Grain_3D_clustering(std::map<int, std::vector<GrainList>>&grain_map);

void output_grain_2D(std::string filename, std::map<int, std::vector<GrainList>>&grain_map);
void output_grain_3D(std::string filename, std::vector<Grain3D>&grain);

void output_grain_2D_image(std::string filename, cv::Mat &image, std::vector<GrainList>&grain);
void output_grain_3D_image(std::string filename, cv::Mat &image, std::vector<GrainList>&grain_2d, std::vector<Grain3D>&grain_3d, int layer);

cv::Vec3b cluster_color(int id);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage: image-path image-num output-path\n");
		exit(1);
	}
	std::string file_in_image_path = argv[1];
	int image_num = std::stoi(argv[2]);
	std::string file_out_image_path = argv[3];

	std::vector<cv::Mat> image_gray = read_image(file_in_image_path, image_num);
	//image_gray = image_triming(image_gray, file_out_image_path,0, true);
	std::vector<cv::Mat> image_bin = binaryzation(image_gray, file_out_image_path, 1, false);
	std::vector<cv::Mat> image_bin_inv = BW_inversion(image_bin, file_out_image_path,2, false);
	std::vector<cv::Mat> image_bg = blur(image_gray, file_out_image_path,3, false);
	std::vector<cv::Mat> image_br = subtraction(image_gray, image_bg, file_out_image_path,4, false);

	int count = 0, all = 0;;
	std::map<int, std::vector<GrainList>> grain_inf;
	for (int i = 0; i < image_gray.size(); i++) {
		printf("\r Get Grain Cluster %d/%d", count, image_gray.size());
		count++;
		//輝度値平均
		//double bg_mean = cv::mean(image_gray[i])[0];
		//連結成分の抽出
		std::vector<ConnectedList> connect = GetConnectList(image_bin_inv[i]);
		//grain情報の抽出
		std::vector<GrainList> grain = GetGrainList(image_gray[i], connect, image_br[i]);
		//5pixel未満のcut & 連結grainの分離
		grain = GrainSeparation(image_gray[i], grain, image_br[i]);
		printf(" : Number of grain --> %5d", grain.size());
		//clustreの描画
		if (false) {
			std::stringstream filename;
			filename << file_out_image_path << "\\test5_" << std::setw(4) << std::setfill('0') << i << ".png";
			output_grain_2D_image(filename.str(), image_gray[i], grain);
		}


		all += grain.size();
		grain_inf.insert(std::make_pair(i, grain));
	}
	printf("\r Get Grain Cluster %d/%d : Number of grain --> %5d\n", count, image_gray.size(), all);

	std::vector<Grain3D> grain_3d = Grain_3D_clustering(grain_inf);

	std::stringstream output_2d, output_3d;
	output_2d << file_out_image_path << "\\grain_2d.txt";
	output_3d << file_out_image_path << "\\grain_3d.txt";
	output_grain_2D(output_2d.str(), grain_inf);
	output_grain_3D(output_3d.str(), grain_3d);

	if (false) {
		for (int i = 0; i < image_gray.size(); i++) {
			if (grain_inf.count(i) == 0)continue;
			auto res = grain_inf.find(i);
			std::stringstream filename;
			filename << file_out_image_path << "\\test6_" << std::setw(4) << std::setfill('0') << i << ".png";
			output_grain_3D_image(filename.str(), image_gray[i], res->second, grain_3d, i);
		}
	}

}
std::vector<cv::Mat> read_image(std::string file_path, int num) {
	std::vector<cv::Mat> ret;
	int count = 0;
	for (int i = 0; i < num; i++) {
		printf("\r read image %3d/%3d", count, num);
		count++;
		std::stringstream filename;
		filename << file_path << "\\" << std::setw(4) << std::setfill('0') << i << ".png";
		if (!std::filesystem::exists(filename.str())) {
			printf("\n file %s not found\n", filename.str().c_str());
			exit(1);
		}
		cv::Mat image = cv::imread(filename.str(), cv::IMREAD_GRAYSCALE);
		ret.push_back(image);
	}
	printf("\r read image %3d/%3d\n", count, num);
	return ret;
}
std::vector<cv::Mat> image_triming(std::vector<cv::Mat> &image, std::string file_out_path, int index,bool output) {
	std::vector<cv::Mat> ret;
	//平均的な像の広がりの
	
	for (int i = 0; i < image.size(); i++) {
		cv::Mat img = image[i].clone();
		img = img(cv::Rect(600,500,300,300));
		ret.push_back(img);
	}
	if (output) {
		int count = 0;
		for (int i = 0; i < ret.size(); i++) {
			printf("\r output binary image %d/%d", count, ret.size());
			count++;
			std::stringstream filename;
			filename << file_out_path << "\\test" << index << "_" << std::setw(4) << std::setfill('0') << i << ".png";
			cv::imwrite(filename.str(), ret[i]);
		}
		printf("\r output binary image %d/%d\n", count, ret.size());
	}
	return ret;

}
std::vector<cv::Mat> binaryzation(std::vector<cv::Mat>&image, std::string file_out_path, int index,bool output) {
	std::vector<cv::Mat> ret;
	//平均的な像の広がりの
	for (int i = 0; i < image.size(); i++) {
		cv::Mat img = image[i].clone();
		cv::adaptiveThreshold(img, img, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 11, 8);
		//original parameter
		//binimg = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 10)
		ret.push_back(img);
	}
	if (output) {
		int count = 0;
		for (int i = 0; i < ret.size(); i++) {
			printf("\r output binary image %d/%d", count, ret.size());
			count++;
			std::stringstream filename;
			filename << file_out_path << "\\test" << index << "_" << std::setw(4) << std::setfill('0') << i << ".png";
			cv::imwrite(filename.str(), ret[i]);
		}
		printf("\r output binary image %d/%d\n", count, ret.size());
	}
	return ret;
}

std::vector<cv::Mat> BW_inversion(std::vector<cv::Mat>&image, std::string file_out_path, int index,bool output) {
	std::vector<cv::Mat> ret;
	for (int i = 0; i < image.size(); i++) {
		cv::Mat img = image[i].clone();
		cv::bitwise_not(img, img);
		//original parameter
		//inv = cv2.bitwise_not(binimg)
		ret.push_back(img);
	}
	if (output) {
		int count = 0;
		for (int i = 0; i < ret.size(); i++) {
			printf("\r output binary image inversion %d/%d", count, ret.size());
			count++;
			std::stringstream filename;
			filename << file_out_path << "\\test" << index << "_" << std::setw(4) << std::setfill('0') << i << ".png";
			cv::imwrite(filename.str(), ret[i]);
		}
		printf("\r output binary image inversion %d/%d\n", count, ret.size());
	}
	return ret;
}

std::vector<cv::Mat> blur(std::vector<cv::Mat>&image, std::string file_out_path, int index,bool output) {
	std::vector<cv::Mat> ret;
	//平均的な像の広がりの
	for (int i = 0; i < image.size(); i++) {
		cv::Mat img = image[i].clone();
		cv::medianBlur(img, img, 51);
		//cv::adaptiveThreshold(img, img, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 11, 8);
		//original parameter
		//binimg = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 10)
		ret.push_back(img);
	}
	if (output) {
		int count = 0;
		for (int i = 0; i < ret.size(); i++) {
			printf("\r output blur image %d/%d", count, ret.size());
			count++;
			std::stringstream filename;
			filename << file_out_path << "\\test" << index << "_" << std::setw(4) << std::setfill('0') << i << ".png";
			cv::imwrite(filename.str(), ret[i]);
		}
		printf("\r output blur image %d/%d\n", count, ret.size());
	}
	return ret;
}

std::vector<cv::Mat> subtraction(std::vector<cv::Mat>&image, std::vector<cv::Mat>&bg, std::string file_out_path, int index,bool output) {
	std::vector<cv::Mat> ret;
	//平均的な像の広がりの
	int width, height;
	unsigned char br0, br1;
	int bright;
	for (int i = 0; i < image.size(); i++) {
		//x
		width = image[i].cols;
		//y
		height = image[i].rows;

		cv::Mat image_sub = cv::Mat::zeros(height, width, CV_8UC1);
		for (int ix = 0; ix < width; ix++) {
			for (int iy = 0; iy < height; iy++) {
				br0 = image[i].at<unsigned char>(iy, ix);//青
				br1 = bg[i].at<unsigned char>(iy, ix);//青
				bright = int(br1) - int(br0);
				if (bright > 0xff)image_sub.at<unsigned char>(iy, ix) = 0xff;
				else if (1 > bright)image_sub.at<unsigned char>(iy, ix) = 0x01;
				else image_sub.at<unsigned char>(iy, ix) = unsigned char(bright);
			}
		}
		ret.push_back(image_sub);
	}

	if (output) {
		int count = 0;
		for (int i = 0; i < ret.size(); i++) {
			printf("\r output blur image %d/%d", count, ret.size());
			count++;
			std::stringstream filename;
			filename << file_out_path << "\\test" << index << "_" << std::setw(4) << std::setfill('0') << i << ".png";
			cv::imwrite(filename.str(), ret[i]);
		}
		printf("\r output blur image %d/%d\n", count, ret.size());
	}
	return ret;
}


std::vector<ConnectedList> GetConnectList(cv::Mat &image) {
	//ラべリング処理
	cv::Mat labels;
	cv::Mat stats;
	cv::Mat centroids;
	int connectivity = 4;
	int ltype = CV_32S;
	int nlabels = cv::connectedComponentsWithStats(image, labels, stats, centroids, connectivity, ltype);

	int width, height;
	//x
	width = image.cols;
	//y
	height = image.rows;

	std::vector<ConnectedList>connected_list;
	for (int iy = 0; iy < height; iy++) {
		for (int ix = 0; ix < width; ix++) {
			if (labels.at<int>(iy, ix) == 0)continue;
			ConnectedList c_list;
			c_list.ix = ix;
			c_list.iy = iy;
			c_list.label = labels.at<int>(iy, ix);
			connected_list.push_back(c_list);
		}
	}

	sort(connected_list.begin(), connected_list.end(), sort_ConnectedList);

	return connected_list;
}

std::vector<GrainList> GetGrainList(cv::Mat &image, std::vector<ConnectedList>&connect_list, cv::Mat &image_br) {
	std::vector<GrainList> ret;

	int width, height;
	//x
	width = image.cols;
	//y
	height = image.rows;


	std::multimap<int, std::pair<int, int>> connect_map;
	for (auto itr = connect_list.begin(); itr != connect_list.end(); itr++) {
		connect_map.insert(std::make_pair(itr->label, std::make_pair(itr->ix, itr->iy)));
	}

	GrainList gr;
	int cluster_id = 1, brightness,ix,iy,ix_sum9,iy_sum9;
	for (auto itr = connect_map.begin(); itr != connect_map.end(); itr++) {
		int count = connect_map.count(itr->first);
		//初期化
		gr.clusterid = cluster_id*10;
		gr.pixel_pos.clear();
		gr.brightness_max = 0;
		gr.brightness_sum = 0;
		gr.brightness_sum9 = 0;
		gr.pixelnum = count;
		gr.x = 0;
		gr.y = 0;
		auto range = connect_map.equal_range(itr->first);
		//同一labelでloop
		for (auto connect_itr = range.first; connect_itr != range.second; connect_itr++) {
			//x,yのpixel座標
			ix = connect_itr->second.first;
			iy = connect_itr->second.second;
			brightness = image_br.at<unsigned char>(iy, ix);
			gr.pixel_pos.insert(std::make_pair(ix, iy));
			//輝度値平均
			gr.x +=  brightness*ix;
			gr.y += brightness *iy;
			gr.brightness_sum += brightness;
			if (gr.brightness_max < brightness) {
				gr.brightness_max = brightness;
				ix_sum9 = ix;
				iy_sum9 = iy;
			}
			//#grain_list[ID, number of pixel, centroid y, centroid x, brightness sum, max brightness, sum9 brightness]

		}

		gr.x /= gr.brightness_sum;
		gr.y /= gr.brightness_sum;
		//周囲のpixelの輝度値の和をとる
		for (int ix = -1; ix <= 1; ix++) {
			for (int iy = -1; iy <= 1; iy++) {
				int iix = ix_sum9 + ix;
				int iiy = iy_sum9 + iy;
				if (iix < 0)iix = 0;
				if (width <= iix)iix = width - 1;
				if (iiy < 0)iiy = 0;
				if (height <= iiy)iiy = height - 1;
				gr.brightness_sum9+= image_br.at<unsigned char>(iiy, iix);

			}
		}
		ret.push_back(gr);

		cluster_id++;
		itr = std::next(itr, count - 1);
	}

	return ret;
}

std::vector<GrainList> GrainSeparation(cv::Mat &image, std::vector<GrainList>&grain, cv::Mat &image_br) {
	int width, height;
	//x
	width = image.cols;
	//y
	height = image.rows;

	std::vector<GrainList> ret;
	for (int i = 0; i < grain.size(); i++) {
		if (grain[i].pixelnum < 4)continue;
		else if (grain[i].pixelnum <= 20)ret.push_back(grain[i]);
		else {
			std::set<std::pair<int, int>>peak_pos = GetPeak(image, grain[i], image_br);
			if (peak_pos.size() <= 1) {
				ret.push_back(grain[i]);
				continue;
			}
			//peakの位置を2値化画像に乗せてラベリング
			cv::Mat tmpimg = cv::Mat::zeros(height,width, CV_8UC1);
			for (auto itr = peak_pos.begin(); itr != peak_pos.end(); itr++) {
				tmpimg.at<unsigned char>(itr->second, itr->first) = 0xff;
			}
			//ラべリング処理
			cv::Mat labels;
			cv::Mat stats;
			cv::Mat centroids;
			int connectivity = 8;
			int ltype = CV_32S;
			int nlabels = cv::connectedComponentsWithStats(tmpimg, labels, stats, centroids, connectivity, ltype);
			if (nlabels < 3) {
				ret.push_back(grain[i]);
			}
			else {
				//peakの分離
				std::vector<GrainList> divide_gr;
				//重心の出力
				for (int j = 1; j < nlabels; j++) {
					double *param = centroids.ptr<double>(j);
					GrainList gr;
					gr.brightness_max = 0;
					gr.brightness_sum = 0;
					gr.brightness_sum9 = 0;
					gr.clusterid = grain[i].clusterid + j;
					gr.pixelnum = 0;
					gr.x = param[0];
					gr.y = param[1];
					gr.pixel_pos.clear();
					divide_gr.push_back(gr);
				}
					//pixelを最寄りのpeakに振り分ける
				for (auto itr = grain[i].pixel_pos.begin(); itr != grain[i].pixel_pos.end(); itr++) {
					double dist;
					int id;
					for (int j = 0; j < divide_gr.size(); j++) {
						if (j == 0 || dist > pow(itr->first - divide_gr[j].x, 2) + pow(itr->second - divide_gr[j].y, 2)) {
							dist = pow(itr->first - divide_gr[j].x, 2) + pow(itr->second - divide_gr[j].y, 2);
							id = j;
						}
					}
					divide_gr[id].pixel_pos.insert(*itr);
				}
				
				//輝度値重心などの計算
				int brightness, ix, iy, ix_sum9, iy_sum9;
				for (int j = 0; j < divide_gr.size(); j++) {
					divide_gr[j].x = 0;
					divide_gr[j].y = 0;
					ix_sum9 = 0;
					iy_sum9 = 0;
					for (auto itr = divide_gr[j].pixel_pos.begin(); itr != divide_gr[j].pixel_pos.end(); itr++) {
						//x,yのpixel座標
						ix = itr->first;
						iy = itr->second;
						brightness = image_br.at<unsigned char>(iy, ix);
						//輝度値平均
						divide_gr[j].x += brightness *ix;
						divide_gr[j].y += brightness *iy;
						divide_gr[j].brightness_sum += brightness;
						if (divide_gr[j].brightness_max < brightness) {
							divide_gr[j].brightness_max = brightness;
							ix_sum9 = ix;
							iy_sum9 = iy;
						}
					}
					divide_gr[j].pixelnum = divide_gr[j].pixel_pos.size();
					divide_gr[j].x /= divide_gr[j].brightness_sum;
					divide_gr[j].y /= divide_gr[j].brightness_sum;
					//周囲のpixelの輝度値の和をとる
					for (int ix = -1; ix <= 1; ix++) {
						for (int iy = -1; iy <= 1; iy++) {
							int iix = ix_sum9 + ix;
							int iiy = iy_sum9 + iy;
							if (iix < 0)iix = 0;
							if (width <= iix)iix = width - 1;
							if (iiy < 0)iiy = 0;
							if (height <= iiy)iiy = height - 1;
							divide_gr[j].brightness_sum9 += image_br.at<unsigned char>(iiy, iix);
						}
					}
					//printf("%d %g %g\n", j, divide_gr[j].x, divide_gr[j].y);
					ret.push_back(divide_gr[j]);				
				}
			}
		}
	}

	return ret;
}
std::set<std::pair<int, int>> GetPeak(cv::Mat &image, GrainList &gr, cv::Mat &image_br) {
	std::set<std::pair<int, int>>ret;
	
	int width, height;
	//x
	width = image.cols;
	//y
	height = image.rows;

	int brightness;
	bool flg = true;
	for (auto itr = gr.pixel_pos.begin(); itr != gr.pixel_pos.end(); itr++) {
		if (itr->first < 1)continue;
		if (itr->second < 1)continue;
		if (width - 1 <= itr->first)continue;
		if (height - 1 <= itr->second)continue;
		flg = true;
		brightness = image_br.at<unsigned char>(itr->second, itr->first);
		for (int ix = -1; ix <= 1; ix++) {
			for (int iy = -1; iy <= 1; iy++) {
				if (ix == 0 && iy == 0)continue;
				if (brightness < image_br.at<unsigned char>(itr->second + iy, itr->first + ix)) {
					flg=false;
				}

			}
		}
		//周囲のpixelより輝度値が低ければpeak判定
		if (flg) {
			ret.insert(*itr);
		}
	}
	return ret;
}

std::vector<Grain3D>Grain_3D_clustering(std::map<int, std::vector<GrainList>>&grain_map) {

	//flgの初期化
	for (auto itr = grain_map.begin(); itr != grain_map.end(); itr++) {
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			itr2->flg = true;
		}
	}

	double dist;
	int layer,all=0;
	std::pair<int, int> pixel_center;
	for (auto itr = grain_map.begin(); itr != grain_map.end(); itr++) {
		layer = itr->first;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (!itr2->flg)continue;
			//上下2枚を見て自分より輝度値が高いlayerがあればfalse
			for (int search_layer = -1; search_layer <= 1; search_layer++) {
				if (search_layer == 0)continue;
				if (grain_map.count(layer + search_layer) == 0)continue;
				auto res = grain_map.find(layer + search_layer);
				for (auto itr3 = res->second.begin(); itr3 != res->second.end(); itr3++) {
					dist = sqrt(pow(itr2->x - itr3->x, 2) + pow(itr2->y - itr3->y, 2));
					if (dist > 2)continue;
					if (itr3->brightness_sum9 >itr2->brightness_sum9)itr2->flg = false;
				}
			}
		}
		all += itr->second.size();
	}
	//printf("grain 3D clustering  %d --> %d\n", all, ret.size());
	std::vector<Grain3D> ret;

	double br[4] = {}, z[4] = {};

	for (auto itr = grain_map.begin(); itr != grain_map.end(); itr++) {
		layer = itr->first;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (!itr2->flg)continue;
			for (int i = 0; i < 4; i++) {
				br[i] = 0;
				z[i] = 0;
			}
			br[0] = itr2->brightness_sum9;
			z[0] = layer;
			//上下2枚を見て自分と同じ場所の輝度値を取得
						//上下2枚を見て自分より輝度値が高いlayerがあればfalse
			for (int search_layer = -1; search_layer <= 2; search_layer++) {
				if (search_layer == 0)continue;
				if (grain_map.count(layer + search_layer) == 0)continue;
				auto res = grain_map.find(layer + search_layer);
				for (auto itr3 = res->second.begin(); itr3 != res->second.end(); itr3++) {
					dist = sqrt(pow(itr2->x - itr3->x, 2) + pow(itr2->y - itr3->y, 2));
					if (dist > 2)continue;

					if (search_layer == -1) {
						br[1] = itr3->brightness_sum9;
						z[1] = layer + search_layer;
					}
					else if (search_layer == 1) {
						br[2] = itr3->brightness_sum9;
						z[2] = layer + search_layer;
					}
					else if (search_layer == 2) {
						br[3] = itr3->brightness_sum9;
						z[3] = layer + search_layer;
					}

					if (itr3->brightness_sum9 < itr2->brightness_sum9)itr2->flg = false;
				}
			}

			if (br[0] == br[1])continue;
			else if (br[0] == br[2]) {
				Grain3D gr;
				gr.x = itr2->x;
				gr.y = itr2->y;
				gr.z = (br[0] * z[0] + br[1] * z[1] + br[2] * z[2] + br[3] * z[3]) / (br[0] + br[1] + br[2] + br[3]);
				gr.layer = layer;
				gr.gr = *itr2;
				ret.push_back(gr);
			}
			else {
				Grain3D gr;
				gr.x = itr2->x;
				gr.y = itr2->y;
				gr.z = (br[0] * z[0] + br[1] * z[1] + br[2] * z[2]) / (br[0] + br[1] + br[2]);
				gr.layer = layer;
				gr.gr = *itr2;
				ret.push_back(gr);
			}
		}
	}


	printf("grain 3D clustering  %d --> %d\n", all, ret.size());
	return ret;
}

void output_grain_2D(std::string filename, std::map<int, std::vector<GrainList>>&grain_map) {

	std::ofstream ofs(filename);
	for (auto itr = grain_map.begin(); itr != grain_map.end(); itr++) {
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			ofs << std::right << std::fixed
				<< std::setw(10) << std::setprecision(0) << itr->first << " "
				<< std::setw(10) << std::setprecision(0) << itr2->clusterid << " "
				<< std::setw(10) << std::setprecision(2) << itr2->x << " "
				<< std::setw(10) << std::setprecision(2) << itr2->y << " "
				<< std::setw(10) << std::setprecision(0) << itr2->pixelnum << " "
				<< std::setw(10) << std::setprecision(0) << itr2->brightness_sum << " "
				<< std::setw(10) << std::setprecision(0) << itr2->brightness_sum9 << " "
				<< std::setw(10) << std::setprecision(0) << itr2->brightness_max << std::endl;
		}
	}
}
void output_grain_3D(std::string filename, std::vector<Grain3D>&grain) {
	std::ofstream ofs(filename);
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr->layer << " "
			<< std::setw(10) << std::setprecision(2) << itr->x << " "
			<< std::setw(10) << std::setprecision(2) << itr->y << " "
			<< std::setw(10) << std::setprecision(2) << itr->z << " "
			<< std::setw(10) << std::setprecision(0) << itr->gr.pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->gr.brightness_sum9 << std::endl;
	}
}

void output_grain_2D_image(std::string filename, cv::Mat &image, std::vector<GrainList>&grain) {

	int width, height;
	//x
	width = image.cols;
	//y
	height = image.rows;

	cv::Mat image_out = cv::Mat::zeros(height, width, CV_8UC3);
	for (int ix = 0; ix < width; ix++) {
		for (int iy = 0; iy < height; iy++) {
			image_out.at<cv::Vec3b>(iy, ix)[0] = image.at<unsigned char>(iy, ix);//青
			image_out.at<cv::Vec3b>(iy, ix)[1] = image.at<unsigned char>(iy, ix);//緑
			image_out.at<cv::Vec3b>(iy, ix)[2] = image.at<unsigned char>(iy, ix);//赤
		}
	}

	cv::Vec3b color;
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		color = cluster_color(itr->clusterid % 10);
		for (auto itr2 = itr->pixel_pos.begin(); itr2 != itr->pixel_pos.end(); itr2++) {
			image_out.at<cv::Vec3b>(itr2->second, itr2->first) = color;
		}
	}

	cv::imwrite(filename, image_out);

}

void output_grain_3D_image(std::string filename, cv::Mat &image, std::vector<GrainList>&grain_2d, std::vector<Grain3D>&grain_3d,int layer) {

	int width, height;
	//x
	width = image.cols;
	//y
	height = image.rows;

	cv::Mat image_out = cv::Mat::zeros(height, width, CV_8UC3);
	for (int ix = 0; ix < width; ix++) {
		for (int iy = 0; iy < height; iy++) {
			image_out.at<cv::Vec3b>(iy, ix)[0] = image.at<unsigned char>(iy, ix);//青
			image_out.at<cv::Vec3b>(iy, ix)[1] = image.at<unsigned char>(iy, ix);//緑
			image_out.at<cv::Vec3b>(iy, ix)[2] = image.at<unsigned char>(iy, ix);//赤
		}
	}

	cv::Vec3b color;
	for (auto itr = grain_2d.begin(); itr != grain_2d.end(); itr++) {
		color = cluster_color(0);
		for (auto itr2 = itr->pixel_pos.begin(); itr2 != itr->pixel_pos.end(); itr2++) {
			if (itr2->second >= height)continue;
			if (itr2->second < 0)continue;
			if (itr2->first >= width)continue;
			if (itr2->first < 0)continue;
			image_out.at<cv::Vec3b>(itr2->second, itr2->first) = color;
		}
	}
	for (auto itr = grain_3d.begin(); itr != grain_3d.end(); itr++) {
		color = cluster_color(1);
		if (itr->layer != layer)continue;
		for (auto itr2 = itr->gr.pixel_pos.begin(); itr2 != itr->gr.pixel_pos.end(); itr2++) {
			if (itr2->second >= height)continue;
			if (itr2->second < 0)continue;
			if (itr2->first >= width)continue;
			if (itr2->first < 0)continue;
			image_out.at<cv::Vec3b>(itr2->second, itr2->first) = color;
		}
		color = cluster_color(10);
		int ix, iy;
		ix = std::round(itr->x);
		iy = std::round(itr->y);
		if (iy>= height)continue;
		if (iy < 0)continue;
		if (ix >= width)continue;
		if (ix < 0)continue;

		image_out.at<cv::Vec3b>(iy, ix) = color;

	}


	cv::imwrite(filename, image_out);

}
cv::Vec3b cluster_color(int id) {
	//openmp-->BGR
	cv::Vec3b ret;
	//red
	if (id == 0) {
		ret[0] = 0x00;
		ret[1] = 0x00;
		ret[2] = 0xff;
	}
	//blue
	else if (id == 1) {
		ret[0] = 0xff;
		ret[1] = 0x00;
		ret[2] = 0x00;
	}
	//green
	else if (id == 2) {
		ret[0] = 0x00;
		ret[1] = 0xff;
		ret[2] = 0x00;
	}
	else if (id == 3) {
		ret[0] = 0x00;
		ret[1] = 0x00;
		ret[2] = 0x80;
	}
	else if (id == 4) {
		ret[0] = 0x00;
		ret[1] = 0x80;
		ret[2] = 0x00;
	}
	//green*0.5
	else if (id == 5) {
		ret[0] = 0x80;
		ret[1] = 0x00;
		ret[2] = 0x00;
	}
	else if (id == 6) {
		ret[0] = 0x00;
		ret[1] = 0xff;
		ret[2] = 0xff;
	}
	else if (id == 7) {
		ret[0] = 0xff;
		ret[1] = 0x00;
		ret[2] = 0xff;
	}
	else if (id == 8) {
		ret[0] = 0xff;
		ret[1] = 0xff;
		ret[2] = 0x00;
	}
	else if (id == 9) {
		ret[0] = 0x00;
		ret[1] = 0x80;
		ret[2] = 0x80;
	}
	//black
	else {
		ret[0] = 0x00;
		ret[1] = 0x00;
		ret[2] = 0x00;
	}
	return ret;
}