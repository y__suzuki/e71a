#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include <filesystem>
#include <omp.h>

class output_format_micro {
public:
	int pos, view, imager, zone, isg, ph, vph, px;
};
class output_format_base {
public:
	int pl, rawid;
	double ax, ay, x, y, z;
	output_format_micro m[2];
};

class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};
class output_connection_value {
public:
	int pl[2], rawid[2];
	double cv;
};

class link_file_header {
public:
	int  pl0, pl1;
	int64_t linknum,begin_row;
};

std::vector<link_file_header> header_read(std::vector<std::string> &input_file);
bool checkFileExistence(const std::string& str);
int64_t file_size(std::string filename);
void connection_value_output(std::string file_out, std::vector<std::string> &input_file, std::vector<link_file_header> &link_header);
void input_output_connection_value(FILE* fp_in, FILE* fp_out, int64_t link_num);
double Calc_cv(output_format_link &l);
double Calc_cv2(output_format_link &l);
double Calc_cv3(output_format_link &l);
void GaussJorden(double in[4][4], double b[4], double c[4]);

double Calc_cv_vph(output_format_link &l);
int judge_mip(double angle, double vph);

void output_header(std::string file_out, std::vector<link_file_header> &link_header);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-file-folder file-out-header(txt) file-out-cv(bin)\n");
		exit(1);
	}

	std::string file_in_link_path = argv[1];
	std::string file_out_header = argv[2];
	std::string file_out_link = argv[3];

	std::vector<std::string> input_file;
	for (const auto & file : std::filesystem::directory_iterator(file_in_link_path)) {
		const std::string filename = file.path().stem().string();
		const std::string exst = file.path().extension().string();
		if (exst == ".bin") {
			if (filename.find(".") == std::string::npos) {
				input_file.push_back(file.path().string());
			}
		}
	}
	std::vector<link_file_header> link_head = header_read(input_file);

	connection_value_output(file_out_link, input_file, link_head);

	output_header(file_out_header,link_head);

}
bool checkFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}
int64_t file_size(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	ifs.close();
	return size2;
}

std::vector<link_file_header> header_read(std::vector<std::string> &input_file) {

	std::vector<std::string> input_file2;
	for (auto &file : input_file) {
		if (!checkFileExistence(file)) {
			fprintf(stderr, "file [%s] not exist\n", file.c_str());
			continue;
		}
		input_file2.push_back(file);
	}
	input_file = input_file2;

	std::vector<link_file_header> ret;
	output_format_link l;
	int64_t all_tnum = 0;
	int count = 0, all = input_file.size();
	for (auto &file : input_file) {
		printf("\r read header %3d/%3d", count, all);
		count++;
		int64_t tnum = file_size(file) / sizeof(output_format_link);
		std::ifstream ifs(file, std::ios::binary);
		ifs.read((char*)& l, sizeof(output_format_link));
		link_file_header head;
		head.begin_row = all_tnum;
		head.linknum = tnum;
		head.pl0 = l.b[0].pl;
		head.pl1 = l.b[1].pl;
		ret.push_back(head);
		all_tnum += tnum;
		ifs.close();
	}
	printf("\r read header %3d/%3d\n", count, all);
	return ret;
}
void output_header(std::string file_out, std::vector<link_file_header> &link_header) {
	std::ofstream ofs(file_out);
	for (auto &head : link_header) {
		ofs
			<< std::setw(3) << head.pl0 << " "
			<< std::setw(3) << head.pl1 << " "
			<< std::setw(10) << head.begin_row << " "
			<< std::setw(15) << head.linknum
			<< std::endl;
	}
	ofs.close();
}

void connection_value_output(std::string file_out,std::vector<std::string> &input_file, std::vector<link_file_header> &link_header) {
	FILE*fp_in, *fp_out;
	if ((fp_out = fopen(file_out.c_str(), "w")) == NULL) {
		printf("file open error!!\n");
		exit(EXIT_FAILURE);	/* (3)エラーの場合は通常、異常終了する */
	}
	int count = 0, all = input_file.size();
	for (int i = 0; i < input_file.size(); i++) {
		printf("\r write connection value %3d/%3d", count, all);
		count++;
		//if (count < 100)continue;
		fp_in = fopen(input_file[i].c_str(), "rb");
		input_output_connection_value(fp_in, fp_out, link_header[i].linknum);
		fclose(fp_in);
	}
	printf("\r write connection value %3d/%3d\n", count, all);

}
void input_output_connection_value(FILE* fp_in, FILE* fp_out, int64_t link_num) {
	const int read_num_max = 1000;
	output_format_link link[read_num_max];
	output_connection_value out_cv[read_num_max];

	int64_t  now = 0;
	int read_num;
	bool flg = true;


	while (flg) {
		if (link_num - now == read_num_max) {
			read_num = link_num - now;
			flg = false;
		}
		else if (link_num - now < read_num_max) {
			read_num = link_num - now;
			flg = false;
		}
		else {
			read_num = read_num_max;
		}
		fread(&link, sizeof(output_format_link), read_num, fp_in);
#pragma omp parallel for num_threads(5)
		for (int i = 0; i < read_num; i++) {
			out_cv[i].pl[0] = link[i].b[0].pl;
			out_cv[i].pl[1] = link[i].b[1].pl;
			out_cv[i].rawid[0] = link[i].b[0].rawid;
			out_cv[i].rawid[1] = link[i].b[1].rawid;
			out_cv[i].cv = Calc_cv(link[i]) + Calc_cv_vph(link[i]);
			//out_cv[i].cv = Calc_cv2(link[i])*Calc_cv_vph(link[i]);
			//out_cv[i].cv = Calc_cv3(link[i])*Calc_cv_vph(link[i]);

		}
		fwrite(&out_cv, sizeof(output_connection_value), read_num, fp_out);
		now += read_num;
	}
	//printf("write fin %lld\n", now);
}
double Calc_cv(output_format_link &l) {
	double angle = sqrt(l.b[0].ax*l.b[0].ax + l.b[0].ay*l.b[0].ay);
	double d_lat_ang = ((l.b[1].ax - l.b[0].ax)*l.b[0].ay - (l.b[1].ay - l.b[0].ay)*l.b[0].ax) / angle;
	return fabs(d_lat_ang) / 0.0025;
}
double Calc_cv2(output_format_link &l) {
	double dz_inv = 1 / (l.b[0].z - l.b[1].z);
	double a[2], b[2], c[2], d[2];
	a[0] = -2 * (l.b[0].x - l.b[1].x)*pow(dz_inv, 3) + (l.b[0].ax + l.b[1].ax)*pow(dz_inv, 2);
	a[1] = -2 * (l.b[0].y - l.b[1].y)*pow(dz_inv, 3) + (l.b[0].ay + l.b[1].ay)*pow(dz_inv, 2);
	b[0] = 3 * (l.b[0].z + l.b[1].z)* (l.b[0].x - l.b[1].x)*pow(dz_inv, 3)
		- 3 / 2.*(l.b[0].z + l.b[1].z)*(l.b[0].ax + l.b[1].ax)*pow(dz_inv, 2)
		+ 1 / 2. * (l.b[0].ax - l.b[1].ax)*dz_inv;
	b[1] = 3 * (l.b[0].z + l.b[1].z)* (l.b[0].y - l.b[1].y)*pow(dz_inv, 3)
		- 3 / 2.*(l.b[0].z + l.b[1].z)*(l.b[0].ay + l.b[1].ay)*pow(dz_inv, 2)
		+ 1 / 2. * (l.b[0].ay - l.b[1].ay)*dz_inv;
	c[0] = -6 * (l.b[0].x - l.b[1].x)*l.b[0].z*l.b[1].z*pow(dz_inv, 3)
		+ 3 * (l.b[0].ax + l.b[1].ax)*l.b[0].z*l.b[1].z*pow(dz_inv, 2)
		- 1 / 2.*(l.b[0].z + l.b[1].z)*(l.b[0].ax - l.b[1].ax)*dz_inv
		+ 1 / 2.*(l.b[0].ax + l.b[1].ax);
	c[1] = -6 * (l.b[0].y - l.b[1].y)*l.b[0].z*l.b[1].z*pow(dz_inv, 3)
		+ 3 * (l.b[0].ay + l.b[1].ay)*l.b[0].z*l.b[1].z*pow(dz_inv, 2)
		- 1 / 2.*(l.b[0].z + l.b[1].z)*(l.b[0].ay - l.b[1].ay)*dz_inv
		+ 1 / 2.*(l.b[0].ay + l.b[1].ay);
	/*
	d[0] = -1 / 2.*(l.b[0].x - l.b[1].x)*(l.b[0].z + l.b[1].z)*(pow(l.b[0].z - l.b[1].z, 2) - 2 * l.b[0].z*l.b[1].z)*pow(dz_inv, 3)
		+ 1 / 4.*(l.b[0].z + l.b[1].z)*(l.b[0].ax + l.b[1].ax)*(pow(l.b[0].z - l.b[1].z, 2) - 2 * l.b[0].z*l.b[1].z)*pow(dz_inv, 2)
		+ 1 / 2.*l.b[0].z*l.b[1].z*(l.b[0].ax - l.b[1].ax)*dz_inv
		- 1 / 4.*(l.b[0].z + l.b[1].z)*(l.b[0].ax + l.b[1].ax) + 1 / 2.*(l.b[0].x + l.b[1].x);
	d[1] = -1 / 2.*(l.b[0].y - l.b[1].y)*(l.b[0].z + l.b[1].z)*(pow(l.b[0].z - l.b[1].z, 2) - 2 * l.b[0].z*l.b[1].z)*pow(dz_inv, 3)
		+ 1 / 4.*(l.b[0].z + l.b[1].z)*(l.b[0].ay + l.b[1].ay)*(pow(l.b[0].z - l.b[1].z, 2) - 2 * l.b[0].z*l.b[1].z)*pow(dz_inv, 2)
		+ 1 / 2.*l.b[0].z*l.b[1].z*(l.b[0].ay - l.b[1].ay)*dz_inv
		- 1 / 4.*(l.b[0].z + l.b[1].z)*(l.b[0].ay + l.b[1].ay) + 1 / 2.*(l.b[0].y + l.b[1].y);
	*/

	double path_length = 0;
	double dz, z;
	int pich = 1000000;
	dz = (l.b[0].z - l.b[1].z) / pich;
	for (int i = 1; i <= pich; i++) {
		z = l.b[1].z + dz * i;
		path_length += sqrt(
			9 * (a[0] * a[0] + a[1] * a[1])*pow(z, 4)
			+ 12 * (a[0] * b[0] + a[1] * b[1])*pow(z, 3)
			+ (4 * (b[0] * b[0] + b[1] * b[1]) + 6 * (a[0] * c[0] + a[1] * c[1]))*pow(z, 2)
			+ 4 * (b[0] * c[0] + b[1] * c[1])*z
			+ c[0] * c[0] + c[1] * c[1] + 1
		)*dz;
	}
	double distance = sqrt(pow(l.b[0].x - l.b[1].x, 2) + pow(l.b[0].y - l.b[1].y, 2) + pow(l.b[0].z - l.b[1].z, 2));
#pragma omp critical
	{
		printf("x:a = %g \ty:a = %g\n", a[0], a[1]);
		printf("x:b = %g \ty:b = %g\n", b[0], b[1]);
		printf("x:c = %g \ty:c = %g\n", c[0], c[1]);
		printf("ax0 = %g \t ay0 = %g\n", l.b[0].ax, l.b[0].ay);
		printf("ax1 = %g \t ay1 = %g\n", l.b[1].ax, l.b[1].ay);
		//printf("x:d=%g \ty:d=%g\n", d[0], d[1]);
		printf("dist = %g\n", distance);
		printf("path = %g\n", path_length);
		printf("divide=%g\n\n", path_length / distance);
	}
	return path_length / distance;
}
double Calc_cv_vph(output_format_link &l) {
	int vph0 = l.b[0].m[0].vph + l.b[0].m[1].vph;
	int vph1= l.b[1].m[0].vph + l.b[1].m[1].vph;
	int score0, score1;
	if (fabs(vph0 - vph1) < 50)return 0;
	else {
		score0 = judge_mip(sqrt(l.b[0].ax*l.b[0].ax + l.b[0].ay*l.b[0].ay), vph0);
		score1 = judge_mip(sqrt(l.b[1].ax*l.b[1].ax + l.b[1].ay*l.b[1].ay), vph1);
		return fabs(score0 - score1) ;
	}


}


int judge_mip(double angle, double vph) {
	if (vph > 250)return 2;

	if (angle < 0.4) {
		if (vph < -200 * angle + 200)return 0;
		else return 1;
	}
	else if (angle < 1.0) {
		if (vph < (-100 * angle + 400) / 3)return 0;
		else return 1;
	}
	return 0;


}


double Calc_cv3(output_format_link &l) {
	double x[4] = { l.b[0].x,l.b[1].x,l.b[0].ax,l.b[1].ax };
	double y[4] = { l.b[0].y,l.b[1].y,l.b[0].ay,l.b[1].ay };
	double in[4][4] = { {pow(l.b[0].z,3),pow(l.b[0].z,2),l.b[0].z,1},{pow(l.b[1].z,3),pow(l.b[1].z,2),l.b[1].z,1},{3 * pow(l.b[0].z,2),2 * l.b[0].z,1,0}, {3 * pow(l.b[1].z,2),2 * l.b[1].z,1,0} };
	double res[4];


	double a[2], b[2], c[2], d[2];
	GaussJorden(in, x, res);
	printf("a = %g\n", res[0]);
	printf("b = %g\n", res[1]);
	printf("c = %g\n", res[2]);
	printf("d = %g\n", res[3]);
	a[0] = res[0];
	b[0] = res[1];
	c[0] = res[2];
	d[0] = res[3];
	GaussJorden(in, y, res);
	printf("a = %g\n", res[0]);
	printf("b = %g\n", res[1]);
	printf("c = %g\n", res[2]);
	printf("d = %g\n", res[3]);
	a[1] = res[0];
	b[1] = res[1];
	c[1] = res[2];
	d[1] = res[3];

	double path_length = 0;
	double dz, z;
	int pich = 10000;
	dz = (l.b[0].z - l.b[1].z) / pich;
	for (int i = 1; i <= pich; i++) {
		z = l.b[1].z + dz * i;
		path_length += sqrt(
			9 * (a[0] * a[0] + a[1] * a[1])*pow(z, 4)
			+ 12 * (a[0] * b[0] + a[1] * b[1])*pow(z, 3)
			+ (4 * (b[0] * b[0] + b[1] * b[1]) + 6 * (a[0] * c[0] + a[1] * c[1]))*pow(z, 2)
			+ 4 * (b[0] * c[0] + b[1] * c[1])*z
			+ c[0] * c[0] + c[1] * c[1] + 1
		)*dz;
	}
	double distance = sqrt(pow(l.b[0].x - l.b[1].x, 2) + pow(l.b[0].y - l.b[1].y, 2) + pow(l.b[0].z - l.b[1].z, 2));
#pragma omp critical
	{
		printf("x:a = %g \ty:a = %g\n", a[0], a[1]);
		printf("x:b = %g \ty:b = %g\n", b[0], b[1]);
		printf("x:c = %g \ty:c = %g\n", c[0], c[1]);
		printf("ax0 = %g \t ay0 = %g\n", l.b[0].ax, l.b[0].ay);
		printf("ax1 = %g \t ay1 = %g\n", l.b[1].ax, l.b[1].ay);
		//printf("x:d=%g \ty:d=%g\n", d[0], d[1]);
		printf("dist = %g\n", distance);
		printf("path = %g\n", path_length);
		printf("divide=%g\n\n", path_length / distance);
	}
	return path_length / distance;
}

void GaussJorden(double in[4][4], double b[4], double c[4]) {


	double a[4][5];
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 5; j++) {
			if (j < 4) {
				a[i][j] = in[i][j];
			}
			else {
				a[i][j] = b[i];
			}
		}
	}
	int N = 4;
	double p, d;         // ピボット係数、ピボット行ｘ係数
	double max, dummy;   // 最大絶対値、入れ替え時ダミー
	int s;

	//元の連立方程式をコンソール出力
   //for (int i = 0; i < N; i++) {
   //	for (int j = 0; j < N; j++)
   //		printf("%+fx%d ", a[i][j], j + 1);
   //	printf("= %+f\n", a[i][N]);
   //}

	for (int k = 0; k < N; k++) {
		// 行入れ替え
		max = 0; s = k;
		for (int j = k; j < N; j++) {
			if (fabs(a[j][k]) > max) {
				max = fabs(a[j][k]);
				s = j;
			}
		}
		if (max == 0) {
			printf("解けない！");
			exit(1);
		}
		for (int j = 0; j <= N; j++) {
			dummy = a[k][j];
			a[k][j] = a[s][j];
			a[s][j] = dummy;
		}

		// ピボット係数
		p = a[k][k];

		// ピボット行を p で除算
		for (int j = k; j < N + 1; j++)
			a[k][j] /= p;

		// ピボット列の掃き出し
		for (int i = 0; i < N; i++) {
			if (i != k) {
				d = a[i][k];
				for (int j = k; j < N + 1; j++)
					a[i][j] -= d * a[k][j];
			}
		}
	}

	// 結果出力
	for (int k = 0; k < N; k++) {
		c[k] = a[k][N];
		//printf("x%d = %f\n", k + 1, a[k][N]);
	}
}
