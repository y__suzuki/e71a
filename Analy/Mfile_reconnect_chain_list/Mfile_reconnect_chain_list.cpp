#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>

void write_reconnect_list(std::string file_m, std::string file_out_start, std::string file_out_end);
class Chain_id{
public:
	int pl;
	uint64_t rawid;
};
int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mfile-path out-start-list(bin) stop-list(bin)\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_txt0 = argv[2];
	std::string file_out_txt1 = argv[3];

	write_reconnect_list(file_in_mfile, file_out_txt0, file_out_txt1);
}

void write_reconnect_list(std::string file_m, std::string file_out_start, std::string file_out_end) {
	std::ifstream ifs(file_m, std::ios::binary);
	std::ofstream ofs_start(file_out_start, std::ios::binary);
	std::ofstream ofs_end(file_out_end, std::ios::binary);

	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }
	
	Chain_id chain_id;
	std::vector<mfile1::MFileBase1> basetracks;
	basetracks.reserve(200);
	uint64_t count = 0, count_chain = 0, count_base = 0;
	for (uint64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		mfile1::MFileChain1 chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }
		count_chain++;
		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase1 base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			count_base++;
			basetracks.emplace_back(base);
		}
		//書き出し(binary)
		chain_id.pl = basetracks.begin()->pos / 10;
		chain_id.rawid = basetracks.begin()->rawid;
		ofs_start.write((char*)& chain_id, sizeof(Chain_id));

		chain_id.pl = basetracks.rbegin()->pos / 10;
		chain_id.rawid = basetracks.rbegin()->rawid;
		ofs_end.write((char*)& chain_id, sizeof(Chain_id));

		basetracks.clear();
	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

	if (count_base != mfile.info_header.Nbasetrack) { throw std::exception("Nbasetrack is wrong."); }
	if (count_chain != mfile.info_header.Nchain) { throw std::exception("Nchain is wrong."); }


}