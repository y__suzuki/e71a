#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>

class Chain_baselist {
public:
	int64_t groupid;
	std::set<std::pair<int32_t, int64_t>> btset;
	std::set<std::tuple<int, int, int, int>>ltlist;
	std::vector<int32_t> usepos;

	std::vector<l2c::Linklet> make_link_list();
	void set_usepos();
};

void l2c_all(std::vector<Chain_baselist> &chain_list);
void l2c_x(std::set<std::pair<int32_t, int64_t>> btset, std::vector<l2c::Linklet> ltlist, std::vector<int32_t> usepos);

void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs);
std::vector<Chain_baselist>read_group(std::string filename);
void make_mfile(std::vector<Chain_baselist> &g, mfile0::Mfile &m, std::multimap<int, mfile0::M_Base*> &base_map);
void input_basetrack_inf(std::multimap<int, mfile0::M_Base*> &base_map, std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, double> &z_map, std::string file_in_ECC);
void bvxx_to_mfilebase(mfile0::M_Base &m_base, vxx::base_track_t &base, double z, corrmap0::Corrmap &corr);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-file ECC_path out-mfile\n\n");
		exit(1);
	}

	std::string file_in_over = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_out_mfile = argv[3];

	std::string file_in_corrabs = file_in_ECC + "\\0\\align\\corrmap-abs.lst";
	//corrmap absの読み込み
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrabs, corr_abs);
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	z_map_correction(z_map, corr_abs);

	//overflow linkeltの読み込み
	std::vector<Chain_baselist> group = read_group(file_in_over);
	mfile0::Mfile m;
	std::multimap<int, mfile0::M_Base*> base_map;
	make_mfile(group, m, base_map);
	input_basetrack_inf(base_map, corr_abs, z_map, file_in_ECC);
	mfile0::write_mfile(file_out_mfile, m);




	//l2c_all(group);
	//for (int i = 0; i < group[0].link.size(); i++) {
	//	printf("%d %d %d %d %d\n", group[0].id, group[0].link[i].first.first, group[0].link[i].first.second, group[0].link[i].second.first, group[0].link[i].second.second);
	//}

}
void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs) {

	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		auto res = z_map.find(pl);
		if (res == z_map.end())continue;
		res->second += itr->dz;
	}

}

std::vector<Chain_baselist>read_group(std::string filename) {
	std::ifstream ifs(filename);

	std::vector<Chain_baselist> ret;
	Chain_baselist group;
	std::string str;
	int count = 0;
	while (std::getline(ifs, str)) {
		std::vector<std::string> str_v;
		str_v = StringSplit_with_tab(str);
		if (str_v[0] == "#") {
			if (count != 0) {
				ret.push_back(group);
			}
			group.groupid = std::stoi(str_v[1]);
			group.ltlist.clear();
			group.btset.clear();
			count++;
		}
		else {
			std::pair<int, int>base0, base1;
			base0.first = std::stoi(str_v[0]);
			base0.second = std::stoi(str_v[1]);
			base1.first = std::stoi(str_v[2]);
			base1.second = std::stoi(str_v[3]);
			group.btset.insert(std::make_pair(base0.first, base0.second));
			group.btset.insert(std::make_pair(base1.first, base1.second));
			group.ltlist.insert(std::make_tuple(base0.first, base1.first, base0.second, base1.second));
		}
	}
	ret.push_back(group);
	return ret;
}

void l2c_all(std::vector<Chain_baselist> &chain_list) {
	int count = 0;
	for (auto itr = chain_list.begin(); itr != chain_list.end(); itr++) {
		count++;
		itr->set_usepos();
		system("pause");
		printf("pos %d :", itr->usepos.size());
		for (auto itr2 = itr->usepos.begin(); itr2 != itr->usepos.end(); itr2++) {
			printf(" %4d", *itr2);
		}
		printf("\n");
		std::vector<l2c::Linklet> link = itr->make_link_list();
		for (auto itr2 = link.begin(); itr2 != link.end(); itr2++) {
			printf("%4d %4d %10d %10d\n", itr2->pos1, itr2->pos2, itr2->id1, itr2->id2);
		}
		printf("eventid = %d  %d/%d\n", itr->groupid, count, chain_list.size());
		l2c_x(itr->btset, link, itr->usepos);
	}


}
void l2c_x(std::set<std::pair<int32_t, int64_t>> btset, std::vector<l2c::Linklet> ltlist, std::vector<int32_t> usepos) {
	try
	{

		//std::vector<int32_t> usepos = { 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500 };
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		l2c::Cdat cdat = l2c::MakeCdat(ltlist, l2c::opt::usepos = usepos, l2c::opt::upperlim = 100000, l2c::opt::output_isolated_linklet = true);

		//中身を出力する。
		size_t grsize = cdat.GetNumOfGroups();
		for (size_t grid = 0; grid < grsize; ++grid)
		{
			const l2c::Group& gr = cdat.GetGroup(grid);
			size_t chsize = gr.GetNumOfChains();
			if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
			int32_t spl = gr.GetStartPL();
			int32_t epl = gr.GetEndPL();

			//l2c-xのPLは一般に使われるPLとは意味が異なり、useposの中で"何番目のposであるか"を意味する。
			//LibL2c-xもl2c-xの仕様に倣うことにした（ただしl2c-xが1始まりなのに対してこちらは0始まり）。
			//例えば今回は、pos==310〜500の20枚でChain Groupを作成しているが、
			//GetStartPL()の戻り値が"3"であったとすると、これはこのGroupがusepos[3]すなわち380から始まっているという意味になる。

			fprintf(stdout, "Group ID:%-9llu nchain:%-7llu start:%-4d end:%-4d\n", gr.GetID(), chsize, spl, epl);
			if (gr.IsOverUpperLim())
			{
				//upperlimを超過している場合、chainの情報はない。
				//ただしchainの本数はGetNumOfChainsで正しく取得できる。
				//また属すBaseTrackの情報は保持しているので、GetBaseTracksで全BaseTrackを取得できる。
				fprintf(stdout, "over upperlim. nchain:%-9lld\n", chsize);
				continue;
			}
			for (size_t ich = 0; ich < chsize; ++ich)
			{
				l2c::Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
				int64_t chid = ch.GetID();
				int32_t nseg = ch.GetNSeg();
				int32_t spl = ch.GetStartPL();
				int32_t epl = ch.GetEndPL();
				fprintf(stdout, "    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d\n", chid, nseg, spl, epl);

				for (size_t pl = 0; pl < possize; ++pl)
				{
					//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
					//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
					l2c::BaseTrackID bt = ch.GetBaseTrack(pl);
					if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
					int32_t btpl = bt.GetPL();
					int64_t btid = bt.GetRawID();
					//btplとplは厳密に一致しなければおかしい。
					if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
					if (btset.find(std::make_pair(usepos[btpl], btid)) == btset.end())
					{
						//エラーチェック。
						//Chain内の全てのBaseTrackはbtset（ファイルから読み込まれた全BaseTrack）に必ず含まれているはず。
						//含まれていなければエラー。
						throw std::exception("BaseTrack is not found in btset.");
					}
					fprintf(stdout, "        pl:%-3d pos:%-4d rawid:%-9lld\n", btpl, usepos[pl], btid);
				}
			}

		}
	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}
std::vector<l2c::Linklet> Chain_baselist::make_link_list() {
	std::vector<l2c::Linklet> ret;
	ret.reserve(ltlist.size());
	for (auto itr = ltlist.begin(); itr != ltlist.end(); itr++) {
		ret.emplace_back(std::get<0>(*itr), std::get<1>(*itr), std::get<2>(*itr), std::get<3>(*itr));
	}
	return ret;
}
void Chain_baselist::set_usepos() {
	std::set<int> pos_set;
	for (auto itr = btset.begin(); itr != btset.end(); itr++) {
		pos_set.insert(itr->first);
	}
	usepos.clear();
	for (auto itr = pos_set.begin(); itr != pos_set.end(); itr++) {
		usepos.push_back(*itr);
	}
}
void make_mfile(std::vector<Chain_baselist> &g, mfile0::Mfile &m,std::multimap<int, mfile0::M_Base*> &base_map) {

	m.header.head[0] = "% Created by mkmf";
	m.header.head[1] = "% on 2007 / 12 / 25 23:28 : 09 + 09:00 (JST)";
	m.header.head[2] = "	0       0   3   0      0.0   0.0000";
	std::set<int> pos;

	int count = 0;
	for (int i = 0; i < g.size(); i++) {
		for (auto itr = g[i].btset.begin(); itr != g[i].btset.end(); itr++) {
			mfile0::M_Chain c;

			mfile0::M_Base b;
			b.group_id = g[i].groupid;
			b.pos = itr->first;
			b.rawid = itr->second;
			c.basetracks.push_back(b);
			c.chain_id = count;
			c.nseg = c.basetracks.size();
			c.pos0 = c.basetracks.begin()->pos;
			c.pos1 = c.basetracks.rbegin()->pos;
			m.chains.push_back(c);
			count++;
			pos.insert(b.pos);
		}
	}
	m.header.num_all_plate = int(pos.size());

	for (auto itr = pos.begin(); itr != pos.end(); itr++) {
		m.header.all_pos.push_back(*itr);
	}
	base_map.clear();
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			base_map.insert(std::make_pair(itr2->pos / 10, &(*itr2)));
		}
	}
}
void input_basetrack_inf(std::multimap<int, mfile0::M_Base*> &base_map, std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, double> &z_map ,std::string file_in_ECC) {
	
	
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		int pl = itr->first;
		printf("PL%03d strat\n", pl);
		int count = base_map.count(pl);
		if (z_map.count(pl) == 0) {
			fprintf(stderr, "PL%0d zmap not found\n", pl);
			exit(1);
		}
		double z = z_map.at(pl);
		corrmap0::Corrmap param;
		bool abs_find = false;
		for (int i = 0; i < corr_abs.size(); i++) {
			if (corr_abs[i].pos[0] / 10 == pl) {
				param = corr_abs[i];
				abs_find = true;
				break;
			}
		}
		if (!abs_find) {
			fprintf(stderr, "PL%0d corrmap_abs not found\n", pl);
			exit(1);
		}
		std::map<int, mfile0::M_Base*> base_rawid;
		auto range = base_map.equal_range(pl);
		for (auto res = range.first; res != range.second; res++) {
			base_rawid.insert(std::make_pair(res->second->rawid, res->second));
		}
		std::stringstream base_file;
		base_file << file_in_ECC << "\\PL" << std::setw(3) << std::setfill('0') << pl
			<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
		if (!std::filesystem::exists(base_file.str())) {
			fprintf(stderr, "%s not exist\n", base_file.str().c_str());
			exit(1);
		}
		vxx::BvxxReader br;
		std::vector<vxx::base_track_t>base = br.ReadAll(base_file.str(), pl, 0);
		for (auto itr2 = base.begin(); itr2 != base.end(); itr2++) {
			if (base_rawid.count(itr2->rawid) == 1) {
				auto res = base_rawid.find(itr2->rawid);
				bvxx_to_mfilebase(*(res->second), *itr2, z, param);
			}
		}

		itr = std::next(itr, count - 1);
	}
}
void bvxx_to_mfilebase(mfile0::M_Base &m_base,vxx::base_track_t &base, double z, corrmap0::Corrmap &corr) {
	mfile0::M_Base ret;
	m_base.ax = base.ax*corr.angle[0] + base.ay*corr.angle[1] + corr.angle[4];
	m_base.ay = base.ax*corr.angle[2] + base.ay*corr.angle[3] + corr.angle[5];

	m_base.ph = base.m[0].ph + base.m[1].ph;
	m_base.x = base.x*corr.position[0] + base.y*corr.position[1] + corr.position[4];
	m_base.y = base.x*corr.position[2] + base.y*corr.position[3] + corr.position[5];
	m_base.z = z;

	m_base.flg_d[0] = 0;
	m_base.flg_d[1] = 0;
	m_base.flg_i[0] = 0;
	m_base.flg_i[1] = 0;
	m_base.flg_i[2] = 0;
	m_base.flg_i[3] = 0;

}