#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int judge_momch_edgeout(int direction, Momentum_recon::Mom_chain &chain, std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> &area, double edge_cut, int ex_pl_max);
bool judge_fiducial_area(std::vector<Fiducial_Area::Fiducial_Area>&area, Momentum_recon::Mom_basetrack&b);

std::vector<Momentum_recon::Event_information> cut_mip_inefficiency(std::vector<Momentum_recon::Event_information>&momch, std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> &area);
double Calc_nseg_npl_accumulate_probability(int nseg, int npl, double efficiency);
bool judge_black(Momentum_recon::Mom_chain &chain);
void Get_nseg_npl(int &nseg, int &npl, Momentum_recon::Mom_chain &chain, std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> &area);
void Get_nseg_npl(int &npl, int &nseg, int npl_max, Momentum_recon::Mom_chain &chain, std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> &area);

bool judge_unit_track(Momentum_recon::Mom_chain &chain);

int main(int argc, char**argv) {
	if (argc != 5) {
		printf("argc %d\n", argc);
		fprintf(stderr, "usage:file-in-momch  file-in-ECC fa.txt file_out_momch\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_in_area = argv[3];
	std::string file_out_momch = argv[4];

	//corrmap absの読み込み
	std::string file_in_corrmap = file_in_ECC + "\\Area0\\0\\align\\fine\\local\\corrmap-local-abs.lst";
	std::map<int, std::vector<corrmap_3d::align_param>>corrmap = corrmap_3d::read_ali_param_abs(file_in_corrmap, 1);
	std::map<int, std::vector<corrmap_3d::align_param2>>corrmap_dd = corrmap_3d::DelaunayDivide_map(corrmap);

	//fiducial areaの読み込み
	std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> area = Fiducial_Area::read_fiducial_Area(file_in_area);
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		if (corrmap_dd.count(itr->first) == 0) {
			fprintf(stderr, "corrmap local abs PL%03d not found\n", itr->first);
			exit(1);
		}
		std::vector<corrmap_3d::align_param2> param = corrmap_dd.at(itr->first);
		trans_mfile_cordinate(param, itr->second);
	}

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	momch = cut_mip_inefficiency(momch,area);



	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);
}

std::vector<Momentum_recon::Event_information> cut_mip_inefficiency(std::vector<Momentum_recon::Event_information>&momch, std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> &area) {
	std::vector<Momentum_recon::Event_information> ret;
	std::map<std::pair<int, int>, int>nseg_npl_count;
	std::map<std::pair<int, int>, int>nseg_npl_remain;
	for (auto &mom : momch) {
		Momentum_recon::Event_information ev = mom;
		ev.chains.clear();
		for (auto &c : mom.chains) {
			if (c.chainid == 0) {
				ev.chains.push_back(c);
				continue;
			}
			int nseg, npl;
			Get_nseg_npl(npl, nseg,8, c, area);
			auto res = nseg_npl_count.find(std::make_pair(npl, nseg));
			if (res == nseg_npl_count.end()) {
				nseg_npl_count.insert(std::make_pair(std::make_pair( npl, nseg), 1));
				nseg_npl_remain.insert(std::make_pair(std::make_pair(npl, nseg), 0));
			}
			else {
				res->second += 1;
			}

			if (judge_black(c)) {
				nseg_npl_remain.find(std::make_pair(npl, nseg))->second += 1;
				ev.chains.push_back(c);
				continue;
			}
			if (Calc_nseg_npl_accumulate_probability(npl, nseg, 0.98) > 0.01) {
				//if (!judge_unit_track(c)) {
					nseg_npl_remain.find(std::make_pair(npl, nseg))->second += 1;
					ev.chains.push_back(c);
				//}
			}


		}

		ret.push_back(ev);

	}


	for (int ipl = 0; ipl < 15; ipl++) {
		for (int iseg = 0; iseg < 15; iseg++) {
			auto res0 = nseg_npl_count.find(std::make_pair(ipl, iseg));
			auto res1 = nseg_npl_remain.find(std::make_pair(ipl, iseg));

			if (res0 != nseg_npl_count.end()) {
				printf("%3dPL %3dseg : %3d / %3d   %.14lf\n", ipl + 1, iseg + 1, res1->second, res0->second, Calc_nseg_npl_accumulate_probability(ipl, iseg, 0.98));
			}
		}
	}
	return ret;
}
bool judge_black(Momentum_recon::Mom_chain &chain) {
	double vph_mean = 0;
	int vph_count = 0;

	for (auto &b : chain.base) {
		vph_mean += b.m[0].ph % 10000;
		vph_mean += b.m[1].ph % 10000;
		vph_count += 2;
	}
	vph_mean /= vph_count;

	return vph_mean > 100;

}
bool judge_unit_track(Momentum_recon::Mom_chain &chain) {
	if (chain.base.size() != 2)return false;
	if (chain.base[1].pl - chain.base[0].pl != 1)return false;
	if (chain.base[0].pl <= 15)return false;
	if (chain.base[0].pl%2==1)return false;
	return true;
}

void Get_nseg_npl(int &npl, int &nseg, Momentum_recon::Mom_chain &chain, std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> &area) {
	nseg = chain.base.size();
	npl = chain.base.rbegin()->pl - chain.base.begin()->pl + 1;
	int edge_pl;
	int ex_pl_max;
	if (chain.direction == 1) {
		edge_pl = chain.base.begin()->pl;
		if (edge_pl == 3)ex_pl_max = 0;
		else if (edge_pl == 4)ex_pl_max = 1;
		else if (edge_pl == 5)ex_pl_max = 2;
		else if (edge_pl <= 17)ex_pl_max = 3;
		else if (edge_pl % 2 == 0)ex_pl_max = 2;
		else ex_pl_max = 3;
		for (int ex_pl = 0; ex_pl <= ex_pl_max; ex_pl++) {
			if (area.count(edge_pl - ex_pl) == 0)continue;

			if (judge_fiducial_area(area.at(edge_pl - ex_pl), *chain.base.begin())) {
				npl += 1;
			}
		}
	}
	else if (chain.direction == -1) {
		edge_pl = chain.base.rbegin()->pl;
		if (edge_pl == 133)ex_pl_max = 0;
		else if (edge_pl == 132)ex_pl_max = 1;
		else if (edge_pl <= 14)ex_pl_max = 3;
		else if (edge_pl == 15)ex_pl_max = 2;
		else if (edge_pl % 2 == 0)ex_pl_max = 3;
		else ex_pl_max = 2;
		for (int ex_pl = 0; ex_pl <= ex_pl_max; ex_pl++) {
			if (area.count(edge_pl + ex_pl) == 0)continue;
			//ex_z = z_map.at(up_pl + ex_pl);
			//ex_x = up_x + up_ax * (ex_z - up_z);
			//ex_y = up_y + up_ay * (ex_z - up_z);

			if (judge_fiducial_area(area.at(edge_pl + ex_pl), *chain.base.rbegin())) {
				npl += 1;
			}
		}

	}
	npl -= 1;
	nseg -= 1;

}

void Get_nseg_npl(int &npl, int &nseg, int npl_max,Momentum_recon::Mom_chain &chain, std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> &area) {
	nseg = 0;
	npl = 0;
	int edge_pl[2];
	int ex_pl_max;
	Momentum_recon::Mom_basetrack base;
	if (chain.direction == 1) {
		edge_pl[0] = chain.base.rbegin()->pl;
		edge_pl[1] = chain.base.begin()->pl;
		if (edge_pl[1] == 3)ex_pl_max = 0;
		else if (edge_pl[1] == 4)ex_pl_max = 1;
		else if (edge_pl[1]== 5)ex_pl_max = 2;
		else if (edge_pl[1] <= 17)ex_pl_max = 3;
		else if (edge_pl[1] % 2 == 0)ex_pl_max = 2;
		else ex_pl_max = 3;
		bool detect_flg = false;
		for (int pl = edge_pl[0]-1; pl >= edge_pl[1] -ex_pl_max && npl <= npl_max; pl--) {
			detect_flg = false;
			for (auto itr = chain.base.rbegin(); itr != chain.base.rend(); itr++) {
				if (itr->pl >= pl) {
					base = *itr;
				}
				if (itr->pl == pl) {
					nseg += 1;
					npl += 1;
					detect_flg = true;
				}
			}
			//飛跡がなかった場合範囲内か確認
			if (!detect_flg) {
				if (area.count(pl) == 0)continue;
				if (judge_fiducial_area(area.at(pl), base)) {
					npl += 1;
				}
			}
		}
	}
	else if (chain.direction == -1) {
		edge_pl[0] = chain.base.begin()->pl;
		edge_pl[1] = chain.base.rbegin()->pl;
		if (edge_pl[1] == 133)ex_pl_max = 0;
		else if (edge_pl[1] == 132)ex_pl_max = 1;
		else if (edge_pl[1] <= 14)ex_pl_max = 3;
		else if (edge_pl[1] == 15)ex_pl_max = 2;
		else if (edge_pl[1] % 2 == 0)ex_pl_max = 3;
		else ex_pl_max = 2;
		bool detect_flg = false;
		for (int pl = edge_pl[0] + 1; pl <= edge_pl[1] + ex_pl_max && npl <= npl_max; pl++) {
			detect_flg = false;
			for (auto itr = chain.base.begin(); itr != chain.base.end(); itr++) {
				if (itr->pl <= pl) {
					base = *itr;
				}
				if (itr->pl == pl) {
					nseg += 1;
					npl += 1;
					detect_flg = true;
				}
			}
			//飛跡がなかった場合範囲内か確認
			if (!detect_flg) {
				if (area.count(pl) == 0)continue;
				if (judge_fiducial_area(area.at(pl), base)) {
					npl += 1;
				}
			}
		}
	}
}

int judge_momch_edgeout(int direction, Momentum_recon::Mom_chain &chain, std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> &area, double edge_cut, int ex_pl_max) {

	int edge_pl;
	int return_flg = 0;
	if (direction == 1) {
		edge_pl = chain.base.begin()->pl;
		return_flg = 0;
		for (int ex_pl = 0; ex_pl <= ex_pl_max; ex_pl++) {
			if (area.count(edge_pl - ex_pl) == 0)continue;
			//ex_z = z_map.at(up_pl + ex_pl);
			//ex_x = up_x + up_ax * (ex_z - up_z);
			//ex_y = up_y + up_ay * (ex_z - up_z);

			if (!judge_fiducial_area(area.at(edge_pl - ex_pl), *chain.base.begin())) {
				return_flg = 2;
			}
		}
		if (edge_pl >= 132)return_flg = 1;
	}
	else if (direction == -1) {
		edge_pl = chain.base.rbegin()->pl;
		return_flg = 0;
		for (int ex_pl = 0; ex_pl <= ex_pl_max; ex_pl++) {
			if (area.count(edge_pl + ex_pl) == 0)continue;
			//ex_z = z_map.at(up_pl + ex_pl);
			//ex_x = up_x + up_ax * (ex_z - up_z);
			//ex_y = up_y + up_ay * (ex_z - up_z);

			if (!judge_fiducial_area(area.at(edge_pl + ex_pl), *chain.base.rbegin())) {
				return_flg = 2;
			}
		}
		if (edge_pl <= 4)return_flg = 1;

	}

	return return_flg;

}

bool judge_fiducial_area(std::vector<Fiducial_Area::Fiducial_Area>&area, Momentum_recon::Mom_basetrack&b) {

	std::map<double, Fiducial_Area::Point> point_map;
	double ex_x, ex_y, dist;
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		ex_x = b.x + b.ax*(itr->p[0].z - b.z);
		ex_y = b.y + b.ay*(itr->p[0].z - b.z);
		dist = pow(ex_x - itr->p[0].x, 2) + pow(ex_y - itr->p[0].y, 2);
		point_map.insert(std::make_pair(dist, itr->p[0]));
	}
	//外挿先から距離の一番近い点のz座標を使用
	double z = point_map.begin()->second.z;
	double x = b.x + b.ax*(z - b.z);
	double y = b.y + b.ay*(z - b.z);


	//true でArea内　falseでarea外

	//点(x,y)からx軸性の方向に直線を引き、その直線と多角形の辺が何回交わるか。
	//下から上に交わったときwn+1
	//上から下に交わったときwn-1
	int wn = 0;
	double vt;
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		// 上向きの辺、下向きの辺によって処理が分かれる。
	// 上向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、終点は含まない。(ルール1)
		if (itr->p[0].y <= y && itr->p[1].y > y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->p[0].y) / (itr->p[1].y - itr->p[0].y);
			if (x < itr->p[0].x + vt * (itr->p[1].x - itr->p[0].x)) {
				++wn;  //ここが重要。上向きの辺と交差した場合は+1
			}
		}
		// 下向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、始点は含まない。(ルール2)
		else if (itr->p[0].y > y && itr->p[1].y <= y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->p[0].y) / (itr->p[1].y - itr->p[0].y);
			if (x < itr->p[0].x + vt * (itr->p[1].x - itr->p[0].x)) {
				--wn;  //ここが重要。下向きの辺と交差した場合は-1
			}
		}
	}
	if (wn >= 1)return true;
	return false;
}

uint64_t Conbination(int n, int r) {
	//組み合わせの数nCrを計算
	uint64_t num = 1;
	for (int i = 1; i <= r; i++) {
		num = num * (n - i + 1) / i;
	}
	return num;

}

double Calc_nseg_npl_accumulate_probability(int npl, int nseg, double efficiency) {
	double eff_tmp;
	eff_tmp = 0;
	for (int iseg = 0; iseg <= nseg; iseg++) {
		eff_tmp += Conbination(npl, iseg)* pow(efficiency, iseg)*pow(1 - efficiency, npl - iseg);
	}
	return eff_tmp;
}
