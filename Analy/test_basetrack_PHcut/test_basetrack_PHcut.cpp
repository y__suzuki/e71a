#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>
#include <omp.h>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <string>
class microtrack_inf {
public:
	int zone, pos, col, row, isg, ph, pixelnum, hitnum;
	double angle;
};

void read_micro_inf(std::string file_path, std::string filename, int num, int pos, std::vector< microtrack_inf>&micro_inf, std::map<int, vxx::base_track_t>&micro_id, std::map<int, std::tuple<vxx::base_track_t, int, int>> &base_inf);
std::map<int, vxx::base_track_t> basetrack_microtrack_rawid_pickup(std::vector<vxx::base_track_t>&base, int face, int zone0, int zone1);
std::map<int, std::tuple<vxx::base_track_t, int, int>> basetrack_ph_inf_add(std::vector<vxx::base_track_t>&base);
std::vector<vxx::base_track_t> cut_ph(std::map<int, std::tuple<vxx::base_track_t, int, int>>&b, int cut);

int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:file-in-bvxx file_in_fvxx_path pl cut_ph out-file out_bvxx\n");
		exit(1);
	}

	std::string file_in_bvxx = argv[1];
	std::string path_in_fvxx = argv[2];
	int pl = std::stoi(argv[3]);
	int cut_ph_val = std::stoi(argv[4]);
	std::string file_out = argv[5];
	std::string file_out_bvxx = argv[6];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);
	std::map<int, std::tuple<vxx::base_track_t, int, int>> base_inf = basetrack_ph_inf_add(base);

	std::vector< microtrack_inf> micro_inf;
	std::map<int, vxx::base_track_t> microtrack_rawid;
	for (int i = 0; i < 8; i++) {
		printf("%d/8 start\n", i + 1);

		if (i == 0)microtrack_rawid = basetrack_microtrack_rawid_pickup(base, 0, 1, 7);
		else if (i == 1)microtrack_rawid = basetrack_microtrack_rawid_pickup(base, 1, 1, 13);
		else if (i == 2)microtrack_rawid = basetrack_microtrack_rawid_pickup(base, 0, 13, 19);
		else if (i == 3)microtrack_rawid = basetrack_microtrack_rawid_pickup(base, 1, 7, 19);
		else if (i == 4)microtrack_rawid = basetrack_microtrack_rawid_pickup(base, 0, 25, 31);
		else if (i == 5)microtrack_rawid = basetrack_microtrack_rawid_pickup(base, 1, 25, 37);
		else if (i == 6)microtrack_rawid = basetrack_microtrack_rawid_pickup(base, 0, 37, 43);
		else if (i == 7)microtrack_rawid = basetrack_microtrack_rawid_pickup(base, 1, 31, 43);

		if (i == 0)	read_micro_inf(path_in_fvxx, "thick", 0, pl * 10 + 1, micro_inf, microtrack_rawid,base_inf);
		else if (i == 1)read_micro_inf(path_in_fvxx, "thick", 0, pl * 10 + 2, micro_inf, microtrack_rawid, base_inf);
		else if (i == 2)read_micro_inf(path_in_fvxx, "thick", 1, pl * 10 + 1, micro_inf, microtrack_rawid, base_inf);
		else if (i == 3)read_micro_inf(path_in_fvxx, "thick", 1, pl * 10 + 2, micro_inf, microtrack_rawid, base_inf);
		else if (i == 4)read_micro_inf(path_in_fvxx, "thin", 0, pl * 10 + 1, micro_inf, microtrack_rawid, base_inf);
		else if (i == 5)read_micro_inf(path_in_fvxx, "thin", 0, pl * 10 + 2, micro_inf, microtrack_rawid, base_inf);
		else if (i == 6)read_micro_inf(path_in_fvxx, "thin", 1, pl * 10 + 1, micro_inf, microtrack_rawid, base_inf);
		else if (i == 7)read_micro_inf(path_in_fvxx, "thin", 1, pl * 10 + 2, micro_inf, microtrack_rawid, base_inf);


	}

	std::ofstream ofs(file_out);
	int all = base_inf.size();
	int count = 0;
	for (auto itr = base_inf.begin(); itr != base_inf.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r now file writing ... %d/%d", count, all);
		}
		count++;

		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << std::get<0>(itr->second).rawid << " "
			<< std::setw(7) << std::setprecision(4) << std::get<0>(itr->second).ax << " "
			<< std::setw(7) << std::setprecision(4) << std::get<0>(itr->second).ay << " "
			<< std::setw(4) << std::setprecision(0) << std::get<1>(itr->second) << " "
			<< std::setw(4) << std::setprecision(0) << std::get<2>(itr->second) << std::endl;
	}
	fprintf(stderr, "\r now file writing ... %d/%d\n", count, all);

	std::vector<vxx::base_track_t> out_base = cut_ph(base_inf, cut_ph_val);
	vxx::BvxxWriter bw;
	//bw.Write(file_out_bvxx,)
	bw.Write(file_out_bvxx, pl, 0, out_base);

}

std::map<int, std::tuple<vxx::base_track_t, int, int>> basetrack_ph_inf_add(std::vector<vxx::base_track_t>&base) {
	std::map<int, std::tuple<vxx::base_track_t, int, int>> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ret.insert(std::make_pair(itr->rawid, std::make_tuple(*itr, -1, -1)));
	}
	return ret;
}
std::map<int, vxx::base_track_t> basetrack_microtrack_rawid_pickup(std::vector<vxx::base_track_t>&base, int face, int zone0, int zone1) {
	std::map<int, vxx::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->m[face].zone != zone0 && itr->m[face].zone != zone1)continue;
		ret.insert(std::make_pair(itr->m[face].rawid, *itr));
	}
	return ret;
}

void read_micro_inf(std::string file_path, std::string filename, int num, int pos, std::vector< microtrack_inf>&micro_inf, std::map<int, vxx::base_track_t>&micro_id, std::map<int, std::tuple<vxx::base_track_t, int, int>> &base_inf) {
	int face = pos % 10;
	std::stringstream fvxxname;
	fvxxname << file_path << "\\f" << std::setw(4) << std::setfill('0') << pos << "_" << filename << "_" << std::setw(1) << num << ".vxx";

	//fvxx xx byte/track
	//1Mtrack=xxGB
	int read_track_num = (1 * 1000 * 1000);
	int id = 0;
	int t_num = -1;
	std::vector<vxx::micro_track_t> m;
	m.reserve(read_track_num);
	vxx::FvxxReader fr;
	microtrack_inf m_tmp;
	int num2, vola;
	while (t_num != 0) {
		std::array<int, 2> index = { id*read_track_num, (id + 1)*read_track_num };//1234<=rawid<=5678であるようなものだけを読む。
		m = fr.ReadAll(fvxxname.str(), pos, 0, vxx::opt::index = index);
		t_num = m.size();

		for (auto itr = m.begin(); itr != m.end(); itr++) {
			if (micro_id.count(itr->rawid) == 0)continue;
			auto res = micro_id.find(itr->rawid);
			auto b_inf = base_inf.find(res->second.rawid);
			//printf("%7.4lf %7.4lf %7d\n", res->second.m[0].ax, res->second.m[0].ay, res->second.m[0].ph);
			//printf("%7.4lf %7.4lf %7d\n", itr->ax, itr->ay, itr->ph);
			//printf("\n");
			//printf("zone:%d rawid:%d\n", itr->zone, itr->rawid);
			num2 = itr->px;
			vola = itr->py;

			m_tmp.zone = itr->zone;
			m_tmp.pos = itr->pos;
			m_tmp.col = itr->col;
			m_tmp.row = itr->row;
			m_tmp.isg = itr->isg;
			m_tmp.pixelnum = num2 >> 5;
			m_tmp.ph = num2 & 0x1f;
			m_tmp.hitnum = vola;
			m_tmp.angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
			if (face == 1)std::get<1>(b_inf->second) = m_tmp.ph;
			if (face == 2)std::get<2>(b_inf->second) = m_tmp.ph;

			micro_inf.push_back(m_tmp);
		}
		m.clear();
		id++;
	}
	//printf("microtrack num %d-->%d\n", micro_id.size(), micro_inf.size());
	m.clear();
	m.shrink_to_fit();

	return;
}
std::vector<vxx::base_track_t> cut_ph(std::map<int, std::tuple<vxx::base_track_t, int, int>>&b,int cut) {
	std::vector<vxx::base_track_t> ret;
	for (auto itr = b.begin(); itr != b.end(); itr++) {
		if (std::get<1>(itr->second) < cut)continue;
		if (std::get<2>(itr->second) < cut)continue;
		ret.push_back(std::get<0>(itr->second));
	}
	return ret;
}