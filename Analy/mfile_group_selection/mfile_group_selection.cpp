//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

std::vector<mfile0::M_Chain> group_selection(std::vector<mfile0::M_Chain>&c, int groupid);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg filename gid out-filename\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	int groupid = std::stoi(argv[2]);
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	m.chains = group_selection(m.chains, groupid);
	mfile0::write_mfile(file_out_mfile, m);
}
std::vector<mfile0::M_Chain> group_selection(std::vector<mfile0::M_Chain>&c, int groupid) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->basetracks.begin()->group_id != groupid)continue;
		ret.push_back(*itr);
	}
	return ret;

}
