#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>

class Chain_mom {
public:
	uint64_t chainid;
	int nseg;
	double mom, ax, ay, vph_ave;
};

std::vector<Chain_mom> read_chain(std::string filename);
std::map<uint64_t, double> read_chain_matsuo(std::string filename);
void add_mom(std::vector<Chain_mom>&chain, std::map<uint64_t, double>&mom);
void output_mom(std::string filename, std::vector<Chain_mom>&chain);

int main(int argc, char**argv) {

	if (argc != 4) {
		fprintf(stderr, "usage:prg file_in_mom file_in_mom_matsuo file-out\n");
		exit(1);
	}

	std::string file_in_mom = argv[1];
	std::string file_in_mom_matsuo = argv[2];
	std::string file_out = argv[3];

	std::vector<Chain_mom> chain=read_chain(file_in_mom);
	std::map<uint64_t, double> chain_mom = read_chain_matsuo(file_in_mom_matsuo);
	add_mom(chain, chain_mom);
	output_mom(file_out, chain);
}

std::vector<Chain_mom> read_chain(std::string filename) {
	std::ifstream ifs(filename.c_str());
	std::vector<Chain_mom> ret;
	Chain_mom chain;
	while (ifs >> chain.chainid >> chain.nseg >> chain.mom >> chain.ax >> chain.ay >> chain.vph_ave) {
		ret.push_back(chain);
	}
	return ret;
}
std::map<uint64_t, double> read_chain_matsuo(std::string filename) {
	//ID p pT a b Nsum Npl - 1 Kingang Chi ndf err_rate err_lowedge err_hiedge flags
	//	56509 0.258945 0 0.00187083 0.0565371 6 3 0 0.167111 3 0.371036 0.104005 0.419801 0 - 0 l
	std::ifstream ifs(filename.c_str());
	std::string str;
	getline(ifs,str);
	std::map<uint64_t, double> ret;
	double mom;
	uint64_t chainid;
	while (getline(ifs, str)) {
		sscanf(str.c_str(), "%llu %lf %*lf %*lf %*lf %*lf %*lf %*lf %*lf %*lf %*lf %*lf %*lf %*c", &chainid, &mom);
		if (isnan(mom))continue;
		mom = mom * 1000;
		ret.insert(std::make_pair(chainid, mom));
	}

	return ret;
}
void add_mom(std::vector<Chain_mom>&chain, std::map<uint64_t, double>&mom) {
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (mom.count(itr->chainid) == 0) {
			//itr->mom = -1;
		}
		else {
			itr->mom = mom.at(itr->chainid);
		}
	}
	return;
}
void output_mom(std::string filename, std::vector<Chain_mom>&chain) {
	std::ofstream ofs(filename);
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		//if (mom > 0) {
		ofs << std::right << std::fixed
			<< std::setw(20) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(4) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(12) << std::setprecision(4) << itr->mom << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(12) << std::setprecision(2) << itr->vph_ave << std::endl;
	}
	return;
}