#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

class Check_result {
public:
	int eventid, chainid;
	std::string str;
};
bool sort_chain_base(const mfile0::M_Base&left, const mfile0::M_Base&right) {
	return left.pos < right.pos;
}
bool sort_chain(const mfile0::M_Chain &left, const mfile0::M_Chain &right) {
	int gid_l = left.basetracks.begin()->group_id;
	int gid_r = right.basetracks.begin()->group_id;
	if (gid_l != gid_r)return gid_l < gid_r;

	int tid_l = left.chain_id;
	int tid_r = right.chain_id;
	if (tid_l != tid_r)return tid_l < tid_r;

	int tid2_l = left.basetracks.begin()->flg_i[1];
	int tid2_r = right.basetracks.begin()->flg_i[1];
	return tid2_l < tid2_r;
}


std::map<int, Check_result> read_file(std::string filename);
std::multimap<int, mfile0::M_Chain> chain_divide_event(std::vector<mfile0::M_Chain> &c);
mfile0::M_Chain chain_merge(mfile0::M_Chain&c0, mfile0::M_Chain&c1, std::map<int, std::tuple<int, int, int, int>>&connect_base);
void output_connect_base(std::string filename, std::map<int, std::tuple<int, int, int, int>>&connect_base);
void mfile_merge(std::vector<mfile0::M_Chain>&connect_chain, std::vector<mfile0::M_Chain>&all_chain);

int main(int argc, char** argv) {

	if (argc != 6) {
		fprintf(stderr, "usage in-mfile in-result-memo out-connect-inf out-mfile-connect out-mfile-all\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_result = argv[2];
	std::string file_out_connect = argv[3];
	std::string file_out_mfile_connect = argv[4];
	std::string file_out_mfile_all = argv[5];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::map<int, Check_result> check_res = read_file(file_in_result);

	std::multimap<int, mfile0::M_Chain> chains = chain_divide_event(m.chains);

	std::vector<mfile0::M_Chain>connect_chain;
	std::vector<mfile0::M_Chain>all_chain = m.chains;
	std::map<int, std::tuple<int, int, int, int>>connect_base;
	for (auto itr = check_res.begin(); itr != check_res.end(); itr++) {
		if (chains.count(itr->first) == 0) {
			fprintf(stderr, "%d %d %s not found 1\n", itr->second.eventid, itr->second.chainid, itr->second.str.c_str());
			continue;
		}
		if (itr->second.str != "connect") {
			printf("%d %d %s\n", itr->second.eventid, itr->second.chainid, itr->second.str.c_str());
			continue;
		}
		int count = 0;
		mfile0::M_Chain c0, c1;

		auto range = chains.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.chain_id == 0) {
				c0 = res->second;
				count++;
			}
			if (res->second.chain_id == itr->second.chainid) {
				c1 = res->second;
				count++;
			}
		}
		if (count != 2) {
			fprintf(stderr, "%d %d %s not found 2\n", itr->second.eventid, itr->second.chainid, itr->second.str.c_str());
			continue;
		}
		connect_chain.push_back(chain_merge(c0, c1, connect_base));

	}

	m.chains = connect_chain;
	mfile0::write_mfile(file_out_mfile_connect, m);
	output_connect_base(file_out_connect, connect_base);

	mfile_merge(connect_chain, all_chain);
	m.chains = connect_chain;
	mfile0::write_mfile(file_out_mfile_all, m);
}
std::map<int, Check_result> read_file(std::string filename) {
	std::ifstream ifs(filename);
	std::map<int, Check_result> ret;
	Check_result res;
	while (ifs >> res.eventid >> res.chainid >> res.str) {
		if (res.str != "connect"&&res.str != "kink"&&res.str != "divide"&&res.str != "proton") {
			printf("%d %d %s\n", res.eventid, res.chainid, res.str.c_str());
		}
		ret.insert(std::make_pair(res.eventid, res));
	}
	return ret;
}
std::multimap<int, mfile0::M_Chain> chain_divide_event(std::vector<mfile0::M_Chain> &c) {
	std::multimap<int, mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		ret.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}

	return ret;
}
mfile0::M_Chain chain_merge(mfile0::M_Chain&c0, mfile0::M_Chain&c1,std::map<int,std::tuple<int,int,int,int>>&connect_base) {


	mfile0::M_Chain ret;
	std::set<int> input_pl;
	std::tuple<int, int, int, int> id;
	std::get<0>(id) = c0.basetracks.rbegin()->pos / 10;
	std::get<1>(id) = c0.basetracks.rbegin()->rawid;

	for (auto itr = c0.basetracks.begin(); itr != c0.basetracks.end(); itr++) {
		input_pl.insert(itr->pos / 10);
		ret.basetracks.push_back(*itr);
	}

	bool flg = false;
	for (auto itr = c1.basetracks.begin(); itr != c1.basetracks.end(); itr++) {
		itr->flg_i[0] = 0;
		itr->flg_i[1] = 0;
		if (input_pl.count(itr->pos / 10) == 1)continue;
		if (!flg) {
			if (c0.basetracks.rbegin()->pos >= itr->pos)continue;
			std::get<2>(id) = itr->pos / 10;
			std::get<3>(id) = itr->rawid;
			flg = true;
		}
		ret.basetracks.push_back(*itr);
	}
	connect_base.insert(std::make_pair(c0.basetracks.begin()->group_id, id));

	sort(ret.basetracks.begin(), ret.basetracks.end(), sort_chain_base);
	ret.chain_id = c0.chain_id;
	ret.nseg = ret.basetracks.size();
	ret.pos0 = c0.basetracks.begin()->pos;
	ret.pos1 = c1.basetracks.rbegin()->pos;
	return ret;
}

void output_connect_base(std::string filename, std::map<int, std::tuple<int, int, int, int>>&connect_base) {
	std::ofstream ofs(filename);
	for (auto itr = connect_base.begin(); itr != connect_base.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(6) << std::setprecision(0) << itr->first << " "
			<< std::setw(3) << std::setprecision(0) << std::get<0>(itr->second) << " "
			<< std::setw(10) << std::setprecision(0) << std::get<1>(itr->second) << " "
			<< std::setw(3) << std::setprecision(0) << std::get<2>(itr->second) << " "
			<< std::setw(10) << std::setprecision(0) << std::get<3>(itr->second) << std::endl;
	}

}

void mfile_merge(std::vector<mfile0::M_Chain>&connect_chain, std::vector<mfile0::M_Chain>&all_chain) {
	std::set<int>eventid;
	for (auto itr = connect_chain.begin(); itr != connect_chain.end(); itr++) {
		eventid.insert(itr->basetracks.begin()->group_id);
	}
	for (auto itr = all_chain.begin(); itr != all_chain.end(); itr++) {
		if (eventid.count(itr->basetracks.begin()->group_id) == 1)continue;
		if (itr->chain_id != 0)continue;
		connect_chain.push_back(*itr);
	}

	std::sort(connect_chain.begin(), connect_chain.end(), sort_chain);
}
