#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class sfnt_file {
public:
	int id, entry_in_daily_file, unix_time, tracker_track_id, spotid;
	double bt_ax, bt_ay, bt_x, bt_y, nt_ax, nt_ay, nt_x, nt_y;
	double dx, dy, dax, day,dz_x,dz_y,chi_square;

};


std::vector<sfnt_file> Read_file(std::string filename);
std::vector<std::string> StringSplit_ast(std::string str);
bool isNumeric(std::string const &str);
void output_file(std::string filename, std::vector<sfnt_file> &link);
void output_file_bin(std::string filename, std::vector<sfnt_file> &link);


int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in \n");
		exit(1);
	}
	std::string file_in = argv[1];
	std::string file_out = argv[2];
	std::string file_out_bin = argv[3];

	std::vector<sfnt_file> sfnt = Read_file(file_in);
	printf("file size = %d\n", sfnt.size());
	output_file(file_out, sfnt);
	output_file_bin(file_out_bin, sfnt);

}

std::vector<std::string> StringSplit_ast(std::string str) {
	std::stringstream ss{ str };
	std::vector<std::string> v;
	std::string buf;
	while (std::getline(ss, buf, '*')) {
		if (buf != "") {
			v.push_back(buf);
		}
	}
	return v;
}

std::vector<sfnt_file> Read_file(std::string filename) {
	std::vector<sfnt_file> ret;
	std::ifstream ifs(filename);
	std::string str;
	int count = 0;
	while (std::getline(ifs, str)) {
		count++;
		//std::cout << (str[0] != '*') << " " << std::count(str.cbegin(), str.cend(), '*') << " " << std::endl;
		//if (count > 10)continue;
		if (str[0] != '*')continue;
		if (count % 10000 == 0) {
			printf("\r read file %d", count);
		}

		int count_ast = std::count(str.cbegin(), str.cend(), '*');
		if (count_ast != 23)continue;
		std::vector<std::string> str_v = StringSplit_ast(str);
		if (str_v.size() != 22)continue;
		if (!isNumeric(str_v[0]))continue;

		sfnt_file sfnt;
		sfnt.id = std::stoi(str_v[2]);
		sfnt.bt_ax = std::stod(str_v[3]);
		sfnt.bt_ay = std::stod(str_v[4]);
		sfnt.bt_x = std::stod(str_v[5]);
		sfnt.bt_y = std::stod(str_v[6]);
		sfnt.nt_ax = std::stod(str_v[7]);
		sfnt.nt_ay = std::stod(str_v[8]);
		sfnt.nt_x = std::stod(str_v[9]);
		sfnt.nt_y = std::stod(str_v[10]);
		sfnt.dx = std::stod(str_v[11]);
		sfnt.dy = std::stod(str_v[12]);
		sfnt.dax = std::stod(str_v[13]);
		sfnt.day = std::stod(str_v[14]);
		sfnt.entry_in_daily_file = std::stoi(str_v[15]);
		sfnt.unix_time = std::stoi(str_v[16]);
		sfnt.tracker_track_id = std::stoi(str_v[17]);
		sfnt.dz_x = std::stod(str_v[18]);
		sfnt.dz_y = std::stod(str_v[19]);
		sfnt.chi_square = std::stod(str_v[20]);
		sfnt.spotid = std::stoi(str_v[21]);


		ret.push_back(sfnt);
	}
	printf("\r read file %d\n", count);

	return ret;

}
bool isNumeric(std::string const &str)
{
	auto it = str.begin();
	while (it != str.end()) {
		if (*it == ' ') {
			it++;
			continue;
		}
		if (!isdigit(*it))break;
		it++;

	}
	return !str.empty() && it == str.end();
}

void output_file(std::string filename, std::vector<sfnt_file> &link) {
	std::ofstream ofs(filename);
	int count = 0, all = link.size();
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r write file %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr->id << " "
			<< std::setw(8) << std::setprecision(4) << itr->bt_ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->bt_ay << " "
			<< std::setw(8) << std::setprecision(4) << itr->bt_x << " "
			<< std::setw(8) << std::setprecision(4) << itr->bt_y << " "
			<< std::setw(8) << std::setprecision(4) << itr->nt_ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->nt_ay << " "
			<< std::setw(8) << std::setprecision(4) << itr->nt_x << " "
			<< std::setw(8) << std::setprecision(4) << itr->nt_y << " "
			<< std::setw(8) << std::setprecision(4) << itr->dx << " "
			<< std::setw(8) << std::setprecision(4) << itr->dy << " "
			<< std::setw(8) << std::setprecision(4) << itr->dax << " "
			<< std::setw(8) << std::setprecision(4) << itr->day << " "
			<< std::setw(8) << std::setprecision(4) << itr->dz_x << " "
			<< std::setw(8) << std::setprecision(4) << itr->dz_y << " "
			<< std::setw(8) << std::setprecision(4) << itr->chi_square << " "
			<< std::setw(5) << std::setprecision(0) << itr->spotid << " "
			<< std::setw(6) << std::setprecision(0) << itr->entry_in_daily_file << " "
			<< std::setw(11) << std::setprecision(0) << itr->unix_time << " "
			<< std::setw(3) << std::setprecision(0) << itr->tracker_track_id << std::endl;
	}
	printf("\r write file %d/%d(%4.1lf%%)\n", count, all, count*100. / all);


}
void output_file_bin(std::string filename, std::vector<sfnt_file> &link) {
	std::ofstream ofs(filename, std::ios::binary);
	int count = 0, all = link.size();
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r write file %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		ofs.write((char*)&*itr, sizeof(sfnt_file));
	}
	printf("\r write file %d/%d(%4.1lf%%)\n", count, all, count*100. / all);


}
