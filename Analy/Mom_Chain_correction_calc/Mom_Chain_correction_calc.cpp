#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class correction_val {
public:
	int pl, face, sensor_id,count;
	double angle_min, angle_max, angle, mean, sigma;
};
bool sort_corr(const correction_val&left, const correction_val&right) {
	if (left.pl != right.pl)return left.pl < right.pl;
	if (fabs(left.angle - right.angle) > 0.01)return left.angle < right.angle;
	if (left.sensor_id != right.sensor_id)return left.sensor_id < right.sensor_id;
	return left.face < right.face;
}

void Calc_mean_sigma_pixel(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma,int &count);
void Calc_correction_pixel(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v);
void Calc_mean_sigma_pixel_sensor(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma, int &count);
void Calc_correction_pixel_sensor(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v);
void Calc_mean_sigma_pixel_sensor_after(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma, int &count);
void Calc_correction_pixel_sensor_after(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v);

void Calc_mean_sigma_vph(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma, int &count);
void Calc_correction_vph(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v);
void Calc_mean_sigma_vph_sensor(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma, int &count);
void Calc_correction_vph_sensor(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v);
void output_correction(std::string filename, std::vector<correction_val> &corr_v);

int judege_sensor_id(int id);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-momch file-correction-file mode\n");
		fprintf(stderr, "mode=0:vph\n");
		fprintf(stderr, "mode=1:pixel\n");
		fprintf(stderr, "mode=10:vph   each sensor\n");
		fprintf(stderr, "mode=11:pixel each sensor\n");
		fprintf(stderr, "mode=21:pixel each sensor after\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_out_res = argv[2];
	int mode = std::stoi(argv[3]);

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::vector<correction_val> corr_v;
	if (mode == 0) {
		Calc_correction_vph(momch, corr_v);
	}
	else if (mode == 1) {
		Calc_correction_pixel(momch, corr_v);
	}
	else if (mode == 10) {
		Calc_correction_vph_sensor(momch, corr_v);
	}
	else if (mode == 11) {
		Calc_correction_pixel_sensor(momch, corr_v);
	}
	else if (mode == 21) {
		Calc_correction_pixel_sensor_after(momch, corr_v);
	}
	else {
		fprintf(stderr, "mode exception\n");
		fprintf(stderr, "mode=0:vph\n");
		fprintf(stderr, "mode=1:pixel\n");
		fprintf(stderr, "mode=10:vph   each sensor\n");
		fprintf(stderr, "mode=11:pixel each sensor\n");
	}

	output_correction(file_out_res, corr_v);

}
void Calc_correction_pixel(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v) {
	std::multimap<int, Momentum_recon::Mom_basetrack> base;
	for (auto itr = mom_ch.begin(); itr != mom_ch.end(); itr++) {
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			if (!isfinite(itr->ecc_mcs_mom))continue;
			if (itr->ecc_mcs_mom < 1000)continue;
			base.insert(std::make_pair(itr2->pl, *itr2));
		}
	}

	double angle_divide = 0.2;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr = std::next(itr, base.count(itr->first))) {
		std::multimap<int, Momentum_recon::Mom_basetrack> base_ang;
		auto range = base.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			angle = sqrt(res->second.ax*res->second.ax + res->second.ay*res->second.ay);
			base_ang.insert(std::make_pair(int(angle / angle_divide), res->second));
		}

		for (auto itr2 = base_ang.begin(); itr2 != base_ang.end(); itr2 = std::next(itr2, base_ang.count(itr2->first))) {
			double angle_min = itr2->first*angle_divide;
			double angle_max = (itr2->first + 1)*angle_divide;
			std::vector<Momentum_recon::Mom_basetrack> base_v;
			auto range = base_ang.equal_range(itr2->first);
			for (auto res = range.first; res != range.second; res++) {
				base_v.push_back(res->second);
			}
			correction_val corr;
			corr.angle_min = angle_min;
			corr.angle_max = angle_max;
			corr.angle = (angle_min + angle_max) / 2;
			corr.pl = itr->first;
			for (int face = 0; face <= 1; face++) {
				for (int sensor = 0; sensor <= 1; sensor++) {
					corr.face = face;
					corr.sensor_id = sensor;
					Calc_mean_sigma_pixel(base_v, corr.face, corr.sensor_id, corr.mean, corr.sigma, corr.count);
					if (corr.mean > 0)corr_v.push_back(corr);
				}
			}
		}
	}
}
void Calc_correction_pixel_sensor(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v) {
	std::multimap<int, Momentum_recon::Mom_basetrack> base;
	for (auto itr = mom_ch.begin(); itr != mom_ch.end(); itr++) {
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			if (!isfinite(itr->ecc_mcs_mom))continue;
			if (itr->ecc_mcs_mom < 1000)continue;
			base.insert(std::make_pair(itr2->pl, *itr2));
		}
	}

	double angle_divide = 0.2;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr = std::next(itr, base.count(itr->first))) {
		std::multimap<int, Momentum_recon::Mom_basetrack> base_ang;
		auto range = base.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			angle = sqrt(res->second.ax*res->second.ax + res->second.ay*res->second.ay);
			base_ang.insert(std::make_pair(int(angle / angle_divide), res->second));
		}

		for (auto itr2 = base_ang.begin(); itr2 != base_ang.end(); itr2 = std::next(itr2, base_ang.count(itr2->first))) {
			double angle_min = itr2->first*angle_divide;
			double angle_max = (itr2->first + 1)*angle_divide;
			std::vector<Momentum_recon::Mom_basetrack> base_v;
			auto range = base_ang.equal_range(itr2->first);
			for (auto res = range.first; res != range.second; res++) {
				base_v.push_back(res->second);
			}
			correction_val corr;
			corr.angle_min = angle_min;
			corr.angle_max = angle_max;
			corr.angle = (angle_min + angle_max) / 2;
			corr.pl = itr->first;
			for (int face = 0; face <= 1; face++) {
				for (int sensor = 0; sensor < 72; sensor++) {
					corr.face = face;
					corr.sensor_id = sensor;
					Calc_mean_sigma_pixel_sensor(base_v, corr.face, corr.sensor_id, corr.mean, corr.sigma, corr.count);
					if (corr.mean > 0)corr_v.push_back(corr);
				}
			}
		}
	}
}
void Calc_correction_pixel_sensor_after(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v) {
	std::multimap<int, Momentum_recon::Mom_basetrack> base;
	for (auto itr = mom_ch.begin(); itr != mom_ch.end(); itr++) {
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			if (!isfinite(itr->ecc_mcs_mom))continue;
			if (itr->ecc_mcs_mom < 1000)continue;
			base.insert(std::make_pair(itr2->pl, *itr2));
		}
	}

	double angle_divide = 0.2;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr = std::next(itr, base.count(itr->first))) {
		std::multimap<int, Momentum_recon::Mom_basetrack> base_ang;
		auto range = base.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			angle = sqrt(res->second.ax*res->second.ax + res->second.ay*res->second.ay);
			base_ang.insert(std::make_pair(int(angle / angle_divide), res->second));
		}

		for (auto itr2 = base_ang.begin(); itr2 != base_ang.end(); itr2 = std::next(itr2, base_ang.count(itr2->first))) {
			double angle_min = itr2->first*angle_divide;
			double angle_max = (itr2->first + 1)*angle_divide;
			std::vector<Momentum_recon::Mom_basetrack> base_v;
			auto range = base_ang.equal_range(itr2->first);
			for (auto res = range.first; res != range.second; res++) {
				base_v.push_back(res->second);
			}
			correction_val corr;
			corr.angle_min = angle_min;
			corr.angle_max = angle_max;
			corr.angle = (angle_min + angle_max) / 2;
			corr.pl = itr->first;
			for (int face = 0; face <= 1; face++) {
				for (int sensor = 0; sensor < 72; sensor++) {
					corr.face = face;
					corr.sensor_id = sensor;
					Calc_mean_sigma_pixel_sensor_after(base_v, corr.face, corr.sensor_id, corr.mean, corr.sigma, corr.count);
					if (corr.mean > 0)corr_v.push_back(corr);
				}
			}
		}
	}
}
void Calc_correction_vph(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v) {
	std::multimap<int, Momentum_recon::Mom_basetrack> base;
	for (auto itr = mom_ch.begin(); itr != mom_ch.end(); itr++) {
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			if (!isfinite(itr->ecc_mcs_mom))continue;
			if (itr->ecc_mcs_mom < 1000)continue;
			base.insert(std::make_pair(itr2->pl, *itr2));
		}
	}

	double angle_divide = 0.2;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr = std::next(itr, base.count(itr->first))) {
		std::multimap<int, Momentum_recon::Mom_basetrack> base_ang;
		auto range = base.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			angle = sqrt(res->second.ax*res->second.ax + res->second.ay*res->second.ay);
			base_ang.insert(std::make_pair(int(angle / angle_divide), res->second));
		}

		for (auto itr2 = base_ang.begin(); itr2 != base_ang.end(); itr2 = std::next(itr2, base_ang.count(itr2->first))) {
			double angle_min = itr2->first*angle_divide;
			double angle_max = (itr2->first + 1)*angle_divide;
			std::vector<Momentum_recon::Mom_basetrack> base_v;
			auto range = base_ang.equal_range(itr2->first);
			for (auto res = range.first; res != range.second; res++) {
				base_v.push_back(res->second);
			}
			correction_val corr;
			corr.angle_min = angle_min;
			corr.angle_max = angle_max;
			corr.angle = (angle_min + angle_max) / 2;
			corr.pl = itr->first;
			for (int face = 0; face <= 1; face++) {
				for (int sensor = 0; sensor <= 1; sensor++) {
					corr.face = face;
					corr.sensor_id = sensor;
					Calc_mean_sigma_vph(base_v, corr.face, corr.sensor_id, corr.mean, corr.sigma, corr.count);
					if (corr.mean > 0)corr_v.push_back(corr);
				}
			}
		}
	}
}
void Calc_correction_vph_sensor(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v) {
	std::multimap<int, Momentum_recon::Mom_basetrack> base;
	for (auto itr = mom_ch.begin(); itr != mom_ch.end(); itr++) {
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			if (!isfinite(itr->ecc_mcs_mom))continue;
			if (itr->ecc_mcs_mom < 1000)continue;
			base.insert(std::make_pair(itr2->pl, *itr2));
		}
	}

	double angle_divide = 0.2;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr = std::next(itr, base.count(itr->first))) {
		std::multimap<int, Momentum_recon::Mom_basetrack> base_ang;
		auto range = base.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			angle = sqrt(res->second.ax*res->second.ax + res->second.ay*res->second.ay);
			base_ang.insert(std::make_pair(int(angle / angle_divide), res->second));
		}

		for (auto itr2 = base_ang.begin(); itr2 != base_ang.end(); itr2 = std::next(itr2, base_ang.count(itr2->first))) {
			double angle_min = itr2->first*angle_divide;
			double angle_max = (itr2->first + 1)*angle_divide;
			std::vector<Momentum_recon::Mom_basetrack> base_v;
			auto range = base_ang.equal_range(itr2->first);
			for (auto res = range.first; res != range.second; res++) {
				base_v.push_back(res->second);
			}
			correction_val corr;
			corr.angle_min = angle_min;
			corr.angle_max = angle_max;
			corr.angle = (angle_min + angle_max) / 2;
			corr.pl = itr->first;
			for (int face = 0; face <= 1; face++) {
				for (int sensor = 0; sensor <72; sensor++) {
					corr.face = face;
					corr.sensor_id = sensor;
					Calc_mean_sigma_vph_sensor(base_v, corr.face, corr.sensor_id, corr.mean, corr.sigma,corr.count);
					if (corr.mean > 0)corr_v.push_back(corr);
				}
			}
		}
	}
}

void Calc_mean_sigma_pixel(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma, int &count) {
	double sum = 0, sum2 = 0;
	count = 0;
	double val = 0;
	for (auto itr = base_v.begin(); itr != base_v.end(); itr++) {
		if (judege_sensor_id(itr->m[face].imager) != sensor)continue;
		if (itr->m[face].hitnum < 0)continue;
		val = itr->m[face].hitnum / sqrt(1 + itr->ax*itr->ax + itr->ay*itr->ay);
		sum += val;
		sum2 += val * val;
		count += 1;
	}
	if (count > 0){
		mean = sum / count;
		sigma = sum2 / count - mean * mean;
		if (sigma>= 0) {
			sigma = sqrt(sigma);
		}
		else {
			sigma = 0;
		}
	}
	else {
		mean = -1;
		sigma = -1;
	}
}
void Calc_mean_sigma_pixel_sensor(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma, int &count) {
	double sum = 0, sum2 = 0;
	count = 0;
	double val = 0;
	for (auto itr = base_v.begin(); itr != base_v.end(); itr++) {
		if (itr->m[face].imager != sensor)continue;
		if (itr->m[face].hitnum < 0)continue;
		val = itr->m[face].hitnum / sqrt(1 + itr->ax*itr->ax + itr->ay*itr->ay);
		sum += val;
		sum2 += val * val;
		count += 1;
	}
	if (count > 0) {
		mean = sum / count;
		sigma = sum2 / count - mean * mean;
		if (sigma >= 0) {
			sigma = sqrt(sigma);
		}
		else {
			sigma = 0;
		}
	}
	else {
		mean = -1;
		sigma = -1;
	}
}
void Calc_mean_sigma_pixel_sensor_after(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma, int &count) {
	double sum = 0, sum2 = 0;
	count = 0;
	double val = 0;
	for (auto itr = base_v.begin(); itr != base_v.end(); itr++) {
		if (itr->m[face].imager != sensor)continue;
		if (itr->m[face].hitnum < 0)continue;
		val = itr->m[face].hitnum;
		sum += val;
		sum2 += val * val;
		count += 1;
	}
	if (count > 0) {
		mean = sum / count;
		sigma = sum2 / count - mean * mean;
		if (sigma >= 0) {
			sigma = sqrt(sigma);
		}
		else {
			sigma = 0;
		}
	}
	else {
		mean = -1;
		sigma = -1;
	}
}
void Calc_mean_sigma_vph(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma, int &count) {
	double sum = 0, sum2 = 0;
	count = 0;
	double val = 0;
	for (auto itr = base_v.begin(); itr != base_v.end(); itr++) {
		if (judege_sensor_id(itr->m[face].imager) != sensor)continue;
		val = itr->m[face].ph % 10000;
		sum += val;
		sum2 += val * val;
		count += 1;
	}
	if (count > 0) {
		mean = sum / count;
		sigma = sum2 / count - mean * mean;
		if (sigma >= 0) {
			sigma = sqrt(sigma);
		}
		else {
			sigma = 0;
		}
	}
	else {
		mean = -1;
		sigma = -1;
	}
}
void Calc_mean_sigma_vph_sensor(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma, int &count) {
	double sum = 0, sum2 = 0;
	count = 0;
	double val = 0;
	for (auto itr = base_v.begin(); itr != base_v.end(); itr++) {
		if (itr->m[face].imager != sensor)continue;
		val = itr->m[face].ph % 10000;
		sum += val;
		sum2 += val * val;
		count += 1;
	}
	if (count > 0) {
		mean = sum / count;
		sigma = sum2 / count - mean * mean;
		if (sigma >= 0) {
			sigma = sqrt(sigma);
		}
		else {
			sigma = 0;
		}
	}
	else {
		mean = -1;
		sigma = -1;
	}
}

void output_correction(std::string filename, std::vector<correction_val> &corr_v) {
	sort(corr_v.begin(), corr_v.end(), sort_corr);
	std::ofstream ofs(filename);


	for (auto itr = corr_v.begin(); itr != corr_v.end(); itr++) {
		ofs << std::fixed << std::right
			<< std::setw(5) << std::setprecision(0) << itr->pl << " "
			<< std::setw(3) << std::setprecision(0) << itr->face << " "
			<< std::setw(3) << std::setprecision(0) << itr->sensor_id << " "
			<< std::setw(7) << std::setprecision(4) << itr->angle << " "
			<< std::setw(7) << std::setprecision(4) << itr->angle_min << " "
			<< std::setw(7) << std::setprecision(4) << itr->angle_max << " "
			<< std::setw(7) << std::setprecision(0) << itr->count << " "
			<< std::setw(5) << std::setprecision(1) << itr->mean << " "
			<< std::setw(7) << std::setprecision(3) << itr->sigma << std::endl;
	}

}

int judege_sensor_id(int id) {
	if ((24 <= id && id <= 35) || id == 52)return 0;
	else return 1;
}