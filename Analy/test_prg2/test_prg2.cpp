#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <unordered_map>
#include <set>
#include <map>

vxx::micro_track_t search_fvxx(std::string filename, int pos);
int main(int argc, char **argv) {

	std::string file_in_fvxx_pos2[4];
	file_in_fvxx_pos2[0] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0322_thick_0.vxx";
	file_in_fvxx_pos2[1] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0322_thick_1.vxx";
	file_in_fvxx_pos2[2] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0322_thin_0.vxx";
	file_in_fvxx_pos2[3] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0322_thin_1.vxx";

	std::string file_in_fvxx_pos1[4];
	file_in_fvxx_pos1[0] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0321_thick_0.vxx";
	file_in_fvxx_pos1[1] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0321_thick_1.vxx";
	file_in_fvxx_pos1[2] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0321_thin_0.vxx";
	file_in_fvxx_pos1[3] = "I:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\eve_0251\\test\\f0321_thin_1.vxx";

	int pos = 322;
	vxx::micro_track_t mu = search_fvxx(file_in_fvxx_pos2[0], pos);

	system("pause");
}
vxx::micro_track_t search_fvxx(std::string filename, int pos) {
	vxx::FvxxReader fr;
	double x, y, ax, ay, pos_width, ang_width;
	x = 19253.8;
	y = 52289.4;
	ax = 0.0594;
	ay = 0.0008;
	x = x + (210 + 30)*ax;
	y = y + (210 + 30)*ay;
	pos_width = 1000;
	ang_width = 0.5;
	std::vector<vxx::CutArea> area;
	area.push_back(vxx::CutArea(x - pos_width, x + pos_width, y - pos_width, y + pos_width, ax - ang_width, ax + ang_width, ay - ang_width, ay + ang_width));
	auto res = fr.ReadAll(filename, pos, 0, vxx::opt::a = area);
	vxx::micro_track_t ret;
	for (auto itr = res.begin(); itr != res.end(); itr++) {
		double d_pos_x = (itr->x - x);
		double d_pos_y = (itr->y - y);
		double d_ang_x = (itr->ax - ax);
		double d_ang_y = (itr->ay - ay);
		if (fabs(d_pos_x) > 10)continue;
		if (fabs(d_pos_y) > 10)continue;
		if (fabs(d_ang_x) > 0.1)continue;
		if (fabs(d_ang_y) > 0.1)continue;
		printf("%d %d %8.1lf %8.1lf %5.4lf %5.4lf\n", itr->rawid, itr->ph, itr->x, itr->y, itr->ax, itr->ay);
		printf("%8.1lf %8.1lf %5.4lf %5.4lf\n", d_pos_x, d_pos_y, d_ang_x, d_ang_y);
		ret = *itr;
	}
	return ret;

}