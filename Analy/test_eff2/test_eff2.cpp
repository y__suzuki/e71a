#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
#include <set>

void calc_invers_corr_area(std::vector<corrmap0::Corrmap>&corr);
std::vector<std::pair<vxx::base_track_t, corrmap0::Corrmap>> correspond_corrmap(std::vector<vxx::base_track_t>&base, std::vector<corrmap0::Corrmap>&corr);
std::vector<vxx::base_track_t> base_trans(std::vector<std::pair<vxx::base_track_t, corrmap0::Corrmap>>&base_pair, double dz_nominal);
vxx::base_track_t base_trans_inv(vxx::base_track_t&base, corrmap0::Corrmap&corr, double nominal_gap);
std::vector<vxx::base_track_t> base_extrapolate(std::vector<vxx::base_track_t> &base, std::vector<corrmap0::Corrmap> &corr, double nominal_gap);
	;
int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg file-in-base file-in-corr pl noiminal-gap output-base\n");
		exit(1);
	}
	std::string file_in_base, file_in_corr;
	file_in_base = argv[1];
	file_in_corr = argv[2];
	int pl = std::stoi(argv[3]);
	double nominal_gap = std::stod(argv[4]);
	std::string file_out_base = argv[5];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base, pl, 0);

	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corr, corr);

	std::vector<vxx::base_track_t> base_ex = base_extrapolate(base, corr, nominal_gap);

	vxx::BvxxWriter bw;
	bw.Write(file_out_base, pl, 0, base_ex);

}


std::vector<vxx::base_track_t> base_extrapolate(std::vector<vxx::base_track_t> &base, std::vector<corrmap0::Corrmap> &corr, double nominal_gap) {
	double xmin, ymin;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		itr->notuse_d[0] = (itr->areax[0] + itr->areax[1]) / 2;
		itr->notuse_d[1] = (itr->areay[0] + itr->areay[1]) / 2;
		if (itr == corr.begin()) {
			xmin = itr->notuse_d[0];
			ymin = itr->notuse_d[1];
		}
		xmin = std::min(xmin, itr->notuse_d[0]);
		ymin = std::min(ymin, itr->notuse_d[1]);
	}
	double hash_size = 2000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	std::pair<int, int>id;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		id.first = (itr->notuse_d[0] - xmin) / hash_size;
		id.second = (itr->notuse_d[1] - ymin) / hash_size;
		corr_map.insert(std::make_pair(id, *itr));
	}

	std::vector<vxx::base_track_t> ret;
	corrmap0::Corrmap param;
	double ex_x, ex_y,dist;
	int ix, iy, loop_num = 0;
		std::vector<corrmap0::Corrmap> corr_buf;
		int count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r basetrack extrapolate %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
		}
		count++  ;
		ex_x = itr->x + itr->ax*nominal_gap;
		ex_y = itr->y + itr->ay*nominal_gap;
		ix = (ex_x - xmin) / hash_size;
		iy = (ex_y - ymin) / hash_size;
		loop_num = 1;
		while (corr_buf.size() < 9) {
			corr_buf.clear();
			for (int iix = -1 * loop_num; iix <= loop_num; iix++) {
				for (int iiy = -1 * loop_num; iiy <= loop_num; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (corr_map.count(id) == 0)continue;
					auto range = corr_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						corr_buf.push_back(res->second);
					}
				}
			}
			if (loop_num > 100)break;
			loop_num++;
		}
		//ここから
		if (corr_buf.size() == 0)continue;
		for (int j = 0; j < corr_buf.size(); j++) {
			if (j == 0) {
				dist = pow(ex_x - corr_buf[j].notuse_d[0], 2) + pow(ex_y - corr_buf[j].notuse_d[1], 2);
				param = corr_buf[j];
			}
			if (dist > pow(ex_x - corr_buf[j].notuse_d[0], 2) + pow(ex_y - corr_buf[j].notuse_d[1], 2)) {
				dist = pow(ex_x - corr_buf[j].notuse_d[0], 2) + pow(ex_y - corr_buf[j].notuse_d[1], 2);
				param = corr_buf[j];
			}
		}

		ex_x = itr->x + itr->ax*(nominal_gap+param.dz);
		ex_y = itr->y + itr->ay*(nominal_gap + param.dz);
		ix = (ex_x - xmin) / hash_size;
		iy = (ex_y - ymin) / hash_size;
		loop_num = 1;
		while (corr_buf.size() <9) {
			corr_buf.clear();
			for (int iix = -1 * loop_num; iix <= loop_num; iix++) {
				for (int iiy = -1 * loop_num; iiy <= loop_num; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (corr_map.count(id) == 0)continue;
					auto range = corr_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						corr_buf.push_back(res->second);
					}
				}
			}
			if (loop_num > 100)break;
			loop_num++;
		}
		//ここから
		if (corr_buf.size() == 0)continue;
		for (int j = 0; j < corr_buf.size(); j++) {
			if (j == 0) {
				dist = pow(ex_x - corr_buf[j].notuse_d[0], 2) + pow(ex_y - corr_buf[j].notuse_d[1], 2);
				param = corr_buf[j];
			}
			if (dist > pow(ex_x - corr_buf[j].notuse_d[0], 2) + pow(ex_y - corr_buf[j].notuse_d[1], 2)) {
				dist = pow(ex_x - corr_buf[j].notuse_d[0], 2) + pow(ex_y - corr_buf[j].notuse_d[1], 2);
				param = corr_buf[j];
			}
		}

		ret.push_back(base_trans_inv(*itr, param, nominal_gap));
	}
	fprintf(stderr, "\r basetrack extrapolate %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());
	return ret;
}



std::vector<std::pair<vxx::base_track_t, corrmap0::Corrmap>> correspond_corrmap(std::vector<vxx::base_track_t>&base, std::vector<corrmap0::Corrmap>&corr) {
	calc_invers_corr_area(corr);

	double x_min, y_min;
	for (int i = 0; i < corr.size(); i++) {
		if (i == 0) {
			x_min = corr[i].notuse_d[0];
			y_min = corr[i].notuse_d[1];
		}
		x_min = std::min(x_min, corr[i].notuse_d[0]);
		y_min = std::min(y_min, corr[i].notuse_d[1]);
	}

	double hash_size = 2000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_hash;
	std::pair<int, int> id;
	for (int i = 0; i < corr.size(); i++) {
		id.first = (corr[i].notuse_d[0] - x_min) / hash_size;
		id.second = (corr[i].notuse_d[1] - y_min) / hash_size;
		corr_hash.insert(std::make_pair(id, corr[i]));
	}

	int ix, iy, loop_num;
	std::vector<std::pair<vxx::base_track_t, corrmap0::Corrmap>> ret;
	std::vector<corrmap0::Corrmap> corr_v;
	//これ追加した
	double dist;
	corrmap0::Corrmap param;
	for (int i = 0; i < base.size(); i++) {
		corr_v.clear();
		loop_num = 0;
		ix = (base[i].x - x_min) / hash_size;
		iy = (base[i].y - y_min) / hash_size;

		while (corr_v.size() < 9) {

			for (int iix = -1 * loop_num; iix <= loop_num; iix++) {
				for (int iiy = -1 * loop_num; iiy <= loop_num; iiy++) {
					if (abs(iix) != loop_num && abs(iiy) != loop_num)continue;
					id.first = ix + iix;
					id.second = iy + iiy;
					if (corr_hash.count(id) == 0)continue;
					auto range = corr_hash.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						corr_v.push_back(res->second);
					}
				}
			}
			if (loop_num > 100)break;
			loop_num++;
		}
		//ここから
		if (corr_v.size() == 0)continue;
		for (int j = 0; j < corr_v.size(); j++) {
			if (j == 0) {
				dist = pow(base[i].x - corr_v[j].notuse_d[0], 2) + pow(base[i].y - corr_v[j].notuse_d[1], 2);
				param = corr_v[j];
			}
			if (dist > pow(base[i].x - corr_v[j].notuse_d[0], 2) + pow(base[i].y - corr_v[j].notuse_d[1], 2)) {
				dist = pow(base[i].x - corr_v[j].notuse_d[0], 2) + pow(base[i].y - corr_v[j].notuse_d[1], 2);
				param = corr_v[j];
			}
		}

		//ここから
		if (corr_v.size() == 0)continue;
		for (int j = 0; j < corr_v.size(); j++) {
			if (j == 0) {
				dist = pow(base[i].x - corr_v[j].notuse_d[0], 2) + pow(base[i].y - corr_v[j].notuse_d[1], 2);
				param = corr_v[j];
			}
			if (dist > pow(base[i].x - corr_v[j].notuse_d[0], 2) + pow(base[i].y - corr_v[j].notuse_d[1], 2)) {
				dist = pow(base[i].x - corr_v[j].notuse_d[0], 2) + pow(base[i].y - corr_v[j].notuse_d[1], 2);
				param = corr_v[j];
			}
		}
		ret.push_back(std::make_pair(base[i], param));
	}
	return ret;
}
void calc_invers_corr_area(std::vector<corrmap0::Corrmap>&corr) {

	double tmp_x, tmp_y, denominator;
	for (int i = 0; i < corr.size(); i++) {
		corr[i].notuse_d[0] = (corr[i].areax[0] + corr[i].areax[1]) / 2;
		corr[i].notuse_d[1] = (corr[i].areay[0] + corr[i].areay[1]) / 2;

		tmp_x = corr[i].notuse_d[0];
		tmp_y = corr[i].notuse_d[1];

		tmp_x = tmp_x - corr[i].position[4];
		tmp_y = tmp_y - corr[i].position[5];
		denominator = corr[i].position[0] * corr[i].position[3] - corr[i].position[1] * corr[i].position[2];

		corr[i].notuse_d[0] = (tmp_x*corr[i].position[3] - tmp_y * corr[i].position[1]) / denominator;
		corr[i].notuse_d[1] = (-1 * tmp_x*corr[i].position[2] + tmp_y * corr[i].position[0]) / denominator;
	}

}

vxx::base_track_t base_trans_inv(vxx::base_track_t&base, corrmap0::Corrmap&corr,double nominal_gap) {
	vxx::base_track_t ret;
	double rotation;
	ret = base;
	ret.x = base.x + base.ax*(nominal_gap + corr.dz);
	ret.y = base.y + base.ay*(nominal_gap + corr.dz);

	double tmp_x, tmp_y;
	tmp_x = ret.x - corr.position[4];
	tmp_y = ret.y - corr.position[5];
	double denominator;
	denominator = 1 / (corr.position[0] * corr.position[3] - corr.position[1] * corr.position[2]);

	ret.x = (tmp_x * corr.position[3] - tmp_y * corr.position[1])*denominator;
	ret.y = (-1 * tmp_x * corr.position[2] + tmp_y * corr.position[0])*denominator;


	tmp_x = ret.ax - corr.angle[4];
	tmp_y = ret.ay - corr.angle[5];

	//rotation = atan(corr.position[2] / corr.position[0]);
	//ret.ax = tmp_x * cos(-1 * rotation) - tmp_y * sin(-1 * rotation);
	//ret.ay = tmp_x * sin(-1 * rotation) + tmp_y * cos(-1 * rotation);

	denominator = 1 / (corr.angle[0] * corr.angle[3] - corr.angle[1] * corr.angle[2]);
	ret.ax = (tmp_x * corr.angle[3] - tmp_y * corr.angle[1])*denominator;
	ret.ay = (-1 * tmp_x * corr.angle[2] + tmp_y * corr.angle[0])*denominator;

	return ret;
}
