#include <VxxReader.h>
#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#define _USE_MATH_DEFINES
#include <math.h>

class GrainInformation {
public:
	int id,face, ix, iy, layer, pixelnum, brightness_sum9;
	double x, y, z;
};
class Grain_pair {
public:
	int id;
	double pos[3], dir[3];
	GrainInformation gr[2];
};
class Grain_track {
public:
	int id;
	double ax, ay;
	double sig_x, sig_y;
	double x0, y0, z0, x1, y1, z1;
	double x, y, z;
	int all_grain_num;
	double track_length;
	std::vector<GrainInformation> gr;
};
bool sort_grain_z(const GrainInformation& left, const GrainInformation& right) {
	return left.z < right.z;
}
bool sort_track_id(const Grain_track& left, const Grain_track& right) {
	return left.id < right.id;
}
bool sort_track_gnum(const Grain_track& left, const Grain_track& right) {
	return left.all_grain_num > right.all_grain_num;
}

std::vector<GrainInformation> read_grain_inf(std::string filename, int &face);
std::vector<std::pair<GrainInformation, GrainInformation>> search_pair(std::vector<GrainInformation>&grain);
bool search_nearest_grain(GrainInformation &gr, GrainInformation&res, std::vector<GrainInformation> &cand);
std::vector<GrainInformation> search_grain_pair(GrainInformation &gr, std::vector<GrainInformation> &cand);
void output_pair(std::string filename, std::vector<std::pair<GrainInformation, GrainInformation>>&pair);
void output_grain(std::string filename, std::vector<std::pair<GrainInformation, GrainInformation>>&pair);
void output_grain(std::string filename, std::vector<GrainInformation>&grain);

std::vector<std::pair<Grain_pair, Grain_pair>> track_connection(std::vector<Grain_pair>&tr);
std::vector<Grain_pair> pair2track(std::vector<std::pair<GrainInformation, GrainInformation>>&pair);
bool select_best_connection(Grain_pair&gr, std::vector < Grain_pair>&cand, Grain_pair&sel, double &md, double &oa);
std::vector < Grain_pair> select_best_connection(Grain_pair&gr, std::vector < Grain_pair>&cand);
std::vector <std::vector<GrainInformation>> make_chain(std::vector<std::pair<Grain_pair, Grain_pair>>&track_pair);
void output_grain(std::string filename, std::vector <std::vector<GrainInformation>>&group);
std::vector<Grain_track> make_track(std::vector <std::vector<GrainInformation>>&chains,int face);
void Calc_angle(Grain_track&track, int face);
void output_grain_each_chain(std::vector <std::vector<GrainInformation>>&group);
void Calc_grain_density(std::vector<Grain_track>&tr, std::vector<GrainInformation> &gr,int face);

void Cut_track(std::vector<Grain_track>&tr, std::vector<GrainInformation> &gr, int face, std::string file_out_sn);
void Calc_angle_grain_density(std::vector<Grain_track>&tr, std::vector<GrainInformation> &gr, double circle_radius, int face);
std::vector<Grain_track> track_clustering(std::vector<Grain_track>&tr);
void output_track(std::string filename, std::vector<Grain_track>&tr);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg filn-in-grain file-out-track file_out_sn\n");
		exit(1);
	}
	std::string file_in_grain = argv[1];
	std::string file_out_track = argv[2];
	std::string file_out_sn = argv[3];

	int face;
	std::vector<GrainInformation> grain_all = read_grain_inf(file_in_grain,face);
	std::vector<std::pair<GrainInformation, GrainInformation>> g_pair=search_pair(grain_all);
	std::vector<Grain_pair> track=pair2track(g_pair);
	printf("all link num =%d\n", track.size());

	std::vector<std::pair<Grain_pair, Grain_pair>> tr_pair = track_connection(track);
	std::vector <std::vector<GrainInformation>>group = make_chain(tr_pair);
	std::vector<Grain_track> track_all= make_track(group,face);
	Calc_grain_density(track_all, grain_all,face);
	Cut_track(track_all, grain_all,face, file_out_sn);
	track_all = track_clustering(track_all);
	output_track(file_out_track, track_all);
	//track_all = Cut_gd(track_all,20);

	//output_grain_each_chain(group);

	//output_pair(file_out, g_pair);
	//output_grain(file_out, g_pair);
	//output_grain(file_out, group);
}
std::vector<GrainInformation> read_grain_inf(std::string filename,int &face) {
		std::ifstream ifs(filename);
		std::vector<GrainInformation> ret;
		GrainInformation gr;
		int count = 0;
		while (ifs >> gr.face >> gr.ix >> gr.iy >> gr.layer >> gr.x >> gr.y >> gr.z >> gr.pixelnum >> gr.brightness_sum9) {
			if (count % 100000 == 0) {
				printf("\r grain read %d", count);
			}
			face = gr.face;
			gr.id = count;
			count++;
			ret.push_back(gr);
		}
		printf("\r grain read fin %d\n", ret.size());
		return ret;

	}
std::vector<std::pair<GrainInformation, GrainInformation>> search_pair(std::vector<GrainInformation>&grain) {


	double xmin, ymin, zmin;
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		if(itr==grain.begin()){
			xmin = itr->x;
			ymin = itr->y;
			zmin = itr->z;
		}
		xmin = std::min(xmin, itr->x);
		ymin = std::min(ymin, itr->y);
		zmin = std::min(zmin, itr->z);
	}

	std::multimap<std::tuple<int, int, int>, GrainInformation>grain_map;
	double x_bin = 4;
	double y_bin = 4;
	double z_bin = 4;
	std::tuple<int, int, int> id;
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		std::get<0>(id) = (itr->x - xmin) / x_bin;
		std::get<1>(id) = (itr->y - ymin) / y_bin;
		std::get<2>(id) = (itr->z - zmin) / z_bin;
		grain_map.insert(std::make_pair(id, *itr));
	}


	std::vector<std::pair<GrainInformation, GrainInformation>> ret;
	int ix, iy, iz;
	std::vector<GrainInformation> cand;

	GrainInformation res;
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		cand.clear();
		ix = (itr->x - xmin) / x_bin;
		iy= (itr->y - ymin) / y_bin;
		iz = (itr->z - zmin) / z_bin;
		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				for (int iiz = -1; iiz <= 1; iiz++) {
					std::get<0>(id) = ix + iix;
					std::get<1>(id) = iy + iiy;
					std::get<2>(id) = iz + iiz;
					if (grain_map.count(id) == 0)continue;
					auto range = grain_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						cand.push_back(res->second);
					}
				}
			}
		}
		cand = search_grain_pair(*itr, cand);
		if (cand.size() == 0)continue;
		for (auto itr2 = cand.begin(); itr2 != cand.end(); itr2++) {
			ret.push_back(std::make_pair(*itr, *itr2));
		}
		//if (search_nearest_grain(*itr, res, cand)) {
		//	ret.push_back(std::make_pair(*itr, res));
		//}
		
	}
	return ret;

}
bool search_nearest_grain(GrainInformation &gr, GrainInformation&res, std::vector<GrainInformation> &cand) {
	double dist;
	bool flg = false;
	double thr = 100;
	for (auto itr = cand.begin(); itr != cand.end(); itr++) {
		if (gr.id == itr->id)continue;
		if (gr.face != itr->face)continue;
		if (gr.ix != itr->ix)continue;
		if (gr.iy != itr->iy)continue;
		//自分よりzが小さいgrainのみ探索
		if (gr.z < itr->z)continue;
		if (gr.z == itr->z) {
			if (gr.id < itr->id)continue;
		}
		if (fabs(gr.z - itr->z) > -1.5*sqrt(pow(gr.x - itr->x, 2) + pow(gr.y - itr->y, 2)) + 3)continue;
		//if (pow(gr.x - itr->x, 2) + pow(gr.y - itr->y, 2) + pow(gr.z - itr->z, 2) > thr*thr)continue;
		if (!flg || dist > pow(gr.x - itr->x, 2) + pow(gr.y - itr->y, 2) + pow(gr.z - itr->z, 2)) {
			flg = true;
			res = *itr;
			dist = pow(gr.x - itr->x, 2) + pow(gr.y - itr->y, 2) + pow(gr.z - itr->z, 2);
		}
	}
	return flg;

}

std::vector<GrainInformation> search_grain_pair(GrainInformation &gr, std::vector<GrainInformation> &cand) {
	std::vector<GrainInformation> res;
	double dist;
	double thr = 100;
	for (auto itr = cand.begin(); itr != cand.end(); itr++) {
		if (gr.id == itr->id)continue;
		if (gr.face != itr->face)continue;
		if (gr.ix != itr->ix)continue;
		if (gr.iy != itr->iy)continue;
		//自分よりzが小さいgrainのみ探索
		if (gr.z < itr->z)continue;

		if (fabs(gr.z - itr->z) > -4/3.*sqrt(pow(gr.x - itr->x, 2) + pow(gr.y - itr->y, 2)) + 4)continue;
		//if (pow(gr.x - itr->x, 2) + pow(gr.y - itr->y, 2) + pow(gr.z - itr->z, 2) > thr*thr)continue;
		res.push_back(*itr);
	}
	return res;

}

void output_pair(std::string filename, std::vector<std::pair<GrainInformation, GrainInformation>>&pair) {
	std::ofstream ofs(filename);
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {

		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->first.face << " "
			<< std::setw(3) << std::setprecision(0) << itr->first.ix << " "
			<< std::setw(3) << std::setprecision(0) << itr->first.iy << " "
			<< std::setw(10) << std::setprecision(0) << itr->first.layer << " "
			<< std::setw(10) << std::setprecision(1) << itr->first.x << " "
			<< std::setw(10) << std::setprecision(1) << itr->first.y << " "
			<< std::setw(10) << std::setprecision(1) << itr->first.z << " "
			<< std::setw(10) << std::setprecision(0) << itr->first.pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->first.brightness_sum9 << " "
			<< std::setw(4) << std::setprecision(0) << itr->second.face << " "
			<< std::setw(3) << std::setprecision(0) << itr->second.ix << " "
			<< std::setw(3) << std::setprecision(0) << itr->second.iy << " "
			<< std::setw(10) << std::setprecision(0) << itr->second.layer << " "
			<< std::setw(10) << std::setprecision(1) << itr->second.x << " "
			<< std::setw(10) << std::setprecision(1) << itr->second.y << " "
			<< std::setw(10) << std::setprecision(1) << itr->second.z << " "
			<< std::setw(10) << std::setprecision(0) << itr->second.pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->second.brightness_sum9 << std::endl;


	}
}

void output_grain(std::string filename, std::vector<std::pair<GrainInformation, GrainInformation>>&pair) {
	std::ofstream ofs(filename);

	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		//if (itr->pixelnum < 10)continue;
		if (itr->first.face != 1)continue;
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->first.face << " "
			<< std::setw(3) << std::setprecision(0) << itr->first.ix << " "
			<< std::setw(3) << std::setprecision(0) << itr->first.iy << " "
			<< std::setw(10) << std::setprecision(0) << itr->first.layer << " "
			<< std::setw(10) << std::setprecision(1) << itr->first.x << " "
			<< std::setw(10) << std::setprecision(1) << itr->first.y << " "
			<< std::setw(10) << std::setprecision(1) << itr->first.z << " "
			<< std::setw(10) << std::setprecision(0) << itr->first.pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->first.brightness_sum9 << std::endl;
	}
}

void output_grain(std::string filename, std::vector <std::vector<GrainInformation>>&group) {
	std::ofstream ofs(filename);

	for (auto itr = group.begin(); itr != group.end(); itr++) {
		//if (itr->pixelnum < 10)continue;
		//if (itr->size() < 5)continue;
		for (auto itr2 = itr->begin(); itr2 != itr->end(); itr2++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr2->face << " "
				<< std::setw(3) << std::setprecision(0) << itr2->ix << " "
				<< std::setw(3) << std::setprecision(0) << itr2->iy << " "
				<< std::setw(10) << std::setprecision(0) << itr2->layer << " "
				<< std::setw(10) << std::setprecision(1) << itr2->x << " "
				<< std::setw(10) << std::setprecision(1) << itr2->y << " "
				<< std::setw(10) << std::setprecision(1) << itr2->z << " "
				<< std::setw(10) << std::setprecision(0) << itr2->pixelnum << " "
				<< std::setw(10) << std::setprecision(0) << itr2->brightness_sum9 << std::endl;
		}
	}
}

void output_grain(std::string filename, std::vector<GrainInformation>&grain) {
	std::ofstream ofs(filename);

	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->face << " "
			<< std::setw(3) << std::setprecision(0) << itr->ix << " "
			<< std::setw(3) << std::setprecision(0) << itr->iy << " "
			<< std::setw(10) << std::setprecision(0) << itr->layer << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(10) << std::setprecision(1) << itr->z << " "
			<< std::setw(10) << std::setprecision(0) << itr->pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->brightness_sum9 << std::endl;
	}

}

void output_grain_each_chain( std::vector <std::vector<GrainInformation>>&group) {
	
	int count = 0;
	for (auto itr = group.begin(); itr != group.end(); itr++) {
		//if (itr->pixelnum < 10)continue;
		std::stringstream filename;
		filename << "cluster" << std::setw(5) << std::setfill('0') << count << ".txt";
			count++;
			std::ofstream ofs(filename.str());

		for (auto itr2 = itr->begin(); itr2 != itr->end(); itr2++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr2->face << " "
				<< std::setw(3) << std::setprecision(0) << itr2->ix << " "
				<< std::setw(3) << std::setprecision(0) << itr2->iy << " "
				<< std::setw(10) << std::setprecision(0) << itr2->layer << " "
				<< std::setw(10) << std::setprecision(1) << itr2->x << " "
				<< std::setw(10) << std::setprecision(1) << itr2->y << " "
				<< std::setw(10) << std::setprecision(1) << itr2->z << " "
				<< std::setw(10) << std::setprecision(0) << itr2->pixelnum << " "
				<< std::setw(10) << std::setprecision(0) << itr2->brightness_sum9 << std::endl;
		}
	}
}

void output_track(std::string filename, std::vector<Grain_track>&tr) {
	std::ofstream ofs(filename);

	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(6) << std::setprecision(0) << itr->id << " "
			<< std::setw(8) << std::setprecision(4) << itr->ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(10) << std::setprecision(1) << itr->z << " "
			<< std::setw(10) << std::setprecision(1) << itr->z0 << " "
			<< std::setw(10) << std::setprecision(1) << itr->z1 << std::endl;
	}




}
std::vector<Grain_pair> pair2track(std::vector<std::pair<GrainInformation, GrainInformation>>&pair) {
	std::vector<Grain_pair> ret;
	int count = 0;
	double factor;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {

		Grain_pair tr;
		tr.id = count;
		count++;
		tr.gr[0] = itr->first;
		tr.gr[1] = itr->second;
		
		tr.pos[0] = (tr.gr[0].x + tr.gr[1].x) / 2;
		tr.pos[1] = (tr.gr[0].y + tr.gr[1].y) / 2;
		tr.pos[2] = (tr.gr[0].z + tr.gr[1].z) / 2;

		tr.dir[0] = (tr.gr[0].x - tr.gr[1].x);
		tr.dir[1] = (tr.gr[0].y - tr.gr[1].y);
		tr.dir[2] = (tr.gr[0].z - tr.gr[1].z);
		factor = sqrt(pow(tr.dir[0], 2) + pow(tr.dir[1], 2) + pow(tr.dir[2], 2));
		tr.dir[0] /= factor;
		tr.dir[1] /= factor;
		tr.dir[2] /= factor;

		ret.push_back(tr);
	}
	return ret;

}
std::vector<std::pair<Grain_pair, Grain_pair>> track_connection(std::vector<Grain_pair>&tr) {

	double xmin, ymin,zmin;
	for(auto itr=tr.begin();itr!=tr.end();itr++){
		if (itr == tr.begin()) {
			xmin = itr->pos[0];
			ymin = itr->pos[1];
			zmin = itr->pos[2];
		}
		xmin = std::min(xmin, itr->pos[0]);
		ymin = std::min(ymin, itr->pos[1]);
		zmin = std::min(zmin, itr->pos[2]);
	}
	std::multimap<std::tuple<int,int, int>, Grain_pair> track_map;
	std::tuple<int,int, int> id;
	double hash = 10;
	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		std::get<0>(id) = (itr->pos[0] - xmin) / hash;
		std::get<1>(id) = (itr->pos[1] - ymin) / hash;
		std::get<2>(id) = (itr->pos[2] - zmin) / hash;
		track_map.insert(std::make_pair(id, *itr));
	}

	std::vector<std::pair<Grain_pair, Grain_pair>> ret;
	std::vector < Grain_pair>cand;
	int64_t all = tr.size();
	int64_t count = 0;
	int ix, iy,iz;
	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		if (count % 100000 == 0) {
			printf("\r now track connection %12lld/%12lld(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		cand.clear();
		ix = (itr->pos[0] - xmin) / hash;
		iy = (itr->pos[1] - ymin) / hash;
		iz = (itr->pos[2] - zmin) / hash;

		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				for (int iiz = -1; iiz <= 1; iiz++) {
					std::get<0>(id) = ix + iix;
					std::get<1>(id) = iy + iiy;
					std::get<2>(id) = iz + iiz;
					if (track_map.count(id) == 0)continue;
					auto range = track_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						if (res->second.id == itr->id)continue;
						cand.push_back(res->second);
					}
				}
			}
		}
		
		cand = select_best_connection(*itr, cand);
		if (cand.size() == 0)continue;
		for (auto itr2 = cand.begin(); itr2 != cand.end(); itr2++) {
			ret.push_back(std::make_pair(*itr, *itr2));
		}
	}
	printf("\r now track connection %12lld/%12lld(%4.1lf%%)\n", count, all, count*100. / all);
	return ret;

}

bool select_best_connection(Grain_pair&gr, std::vector < Grain_pair>&cand, Grain_pair&sel, double &md, double &oa) {

	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	double z_range[2], extra[2];
	double md_tmp, oa_tmp;
	pos0.x = gr.pos[0];
	pos0.y = gr.pos[1];
	pos0.z = gr.pos[2];
	dir0.x = gr.dir[0];
	dir0.y = gr.dir[1];
	dir0.z = gr.dir[2];
	z_range[0] = pos0.z;
	bool flg = false;
	double dist;
	for (auto itr = cand.begin(); itr != cand.end(); itr++) {
		pos1.x = itr->pos[0];
		pos1.y = itr->pos[1];
		pos1.z = itr->pos[2];
		dir1.x = itr->dir[0];
		dir1.y = itr->dir[1];
		dir1.z = itr->dir[2];

		z_range[1] = pos1.z;
		oa_tmp = matrix_3D::opening_angle(dir0, dir1);
		md_tmp = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		//同一grain 逆方向
		if (!isfinite(md_tmp))continue;
		if (md_tmp > 5)continue;
		//45度以上ずれていたら飛ばす
		if (oa_tmp * 180 / M_PI > 45)continue;
		if (!flg || dist > pow(pos0.x - pos1.x, 2) + pow(pos0.y - pos1.y, 2) + pow(pos0.z - pos1.z, 2)) {
			flg = true;
			md = md_tmp;
			oa = oa_tmp;
			dist = pow(pos0.x - pos1.x, 2) + pow(pos0.y - pos1.y, 2) + pow(pos0.z - pos1.z, 2);
			sel = *itr;
		}
	}
	return flg;
}

std::vector < Grain_pair> select_best_connection(Grain_pair&gr, std::vector < Grain_pair>&cand) {
	std::vector < Grain_pair>ret;
	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	double z_range[2], extra[2];
	double md, oa;
	pos0.x = gr.pos[0];
	pos0.y = gr.pos[1];
	pos0.z = gr.pos[2];
	dir0.x = gr.dir[0];
	dir0.y = gr.dir[1];
	dir0.z = gr.dir[2];
	z_range[0] = pos0.z;
	bool flg = false;
	double dist;
	for (auto itr = cand.begin(); itr != cand.end(); itr++) {
		//同一idのgrainがいればpush back
		//if (
		//	gr.gr[0].id == itr->gr[0].id ||
		//	gr.gr[0].id == itr->gr[1].id ||
		//	gr.gr[1].id == itr->gr[0].id ||
		//	gr.gr[1].id == itr->gr[1].id
		//	) {
		//	ret.push_back(*itr);
		//	continue;
		//}

		pos1.x = itr->pos[0];
		pos1.y = itr->pos[1];
		pos1.z = itr->pos[2];
		dir1.x = itr->dir[0];
		dir1.y = itr->dir[1];
		dir1.z = itr->dir[2];

		z_range[1] = pos1.z;
		oa = matrix_3D::opening_angle(dir0, dir1);
		md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		//同一grain 逆方向
		if (!isfinite(md))continue;
		if (md > 3)continue;
		//45度以上ずれていたら飛ばす
		if (oa * 180 / M_PI > 10)continue;
		ret.push_back(*itr);
	}
	return ret;
}

std::vector <std::vector<GrainInformation>> make_chain(std::vector<std::pair<Grain_pair, Grain_pair>>&track_pair) {
	

	std::vector<GrainInformation> gr;
	std::multimap<int, int>gr_map_large;
	std::multimap<int, int>gr_map_small;
	std::map<int, Grain_pair>  tr_map;
	int id0, id1;
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		id0 = itr->first.id;
		id1 = itr->second.id;
		gr_map_large.insert(std::make_pair(std::min(id0, id1), std::max(id0, id1)));
		gr_map_small.insert(std::make_pair(std::max(id0, id1), std::min(id0, id1)));
		tr_map.insert(std::make_pair(itr->first.id, itr->first));
		tr_map.insert(std::make_pair(itr->second.id, itr->second));
	}

	std::set<int>add_id;
	std::set<int>search_id;
	std::set<int>finished_id_tmp;
	std::set<int>finished_id_all;
	std::vector<std::vector<int>>chains;
	std::vector<int> chain;
	//Grain pairでchainの生成
	int count = 0;
	int64_t all = tr_map.size(), now = 0;
	for (auto itr = tr_map.begin(); itr != tr_map.end(); itr++) {
		if (count % 1000 == 0) {
			printf("\r track clustering %12lld/%12lld(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		if (finished_id_all.count(itr->first) == 1)continue;

		
		search_id.clear();
		finished_id_tmp.clear();
		search_id.insert(itr->first);
		while (true) {
			add_id.clear();
			for (auto itr = search_id.begin(); itr != search_id.end(); itr++) {
				if (gr_map_large.count(*itr) != 0){
					auto range = gr_map_large.equal_range(*itr);
					for (auto res = range.first; res != range.second; res++) {
						if (finished_id_tmp.count(res->second) == 1)continue;
						if (search_id.count(res->second) == 1)continue;
						add_id.insert(res->second);
					}
				}
				if (gr_map_small.count(*itr) != 0){
					auto range = gr_map_small.equal_range(*itr);
					for (auto res = range.first; res != range.second; res++) {
						if (finished_id_tmp.count(res->second) == 1)continue;
						if (search_id.count(res->second) == 1)continue;
						add_id.insert(res->second);
					}
				}
				finished_id_tmp.insert(*itr);
			}
			if (add_id.size() == 0)break;
			search_id.swap(add_id);
		}

		chain.clear();
		for (auto itr = finished_id_tmp.begin(); itr != finished_id_tmp.end(); itr++) {
			finished_id_all.insert(*itr);
			chain.push_back(*itr);
		}
		chains.push_back(chain);

	}	
	printf("\r track clustering %12lld/%12lld(%4.1lf%%)\n", count, all, count*100. / all);

	std::vector <std::vector<GrainInformation>> ret;
	std::vector<GrainInformation> gr_v;
	std::map<int, GrainInformation> gr_map;
	for (int i = 0; i < chains.size(); i++) {
		gr_v.clear();
		gr_map.clear();
		//if (chains[i].size() >= 10)continue;
		for (auto j = 0; j < chains[i].size(); j++) {
			auto pair = tr_map.at(chains[i][j]);
			gr_map.insert(std::make_pair(pair.gr[0].id, pair.gr[0]));
			gr_map.insert(std::make_pair(pair.gr[1].id, pair.gr[1]));
		}
		for (auto itr = gr_map.begin(); itr != gr_map.end(); itr++) {
			gr_v.push_back(itr->second);
		}
		ret.push_back(gr_v);
	}

	printf("cluster num =%d\n", ret.size());


	return ret;
}

std::vector<Grain_track> make_track(std::vector <std::vector<GrainInformation>>&chains,int face) {
	std::vector<Grain_track> ret;
	for (auto itr = chains.begin(); itr != chains.end(); itr++) {
		Grain_track track;
		for (auto itr2 = itr->begin(); itr2 != itr->end(); itr2++) {
			track.gr.push_back(*itr2);
		}
		Calc_angle(track,face);
		ret.push_back(track);
	}
	//std::ofstream ofs("grain_track.txt");
	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	//if (itr->pixelnum < 10)continue;
	//	ofs << std::right << std::fixed
	//		<< std::setw(10) << std::setprecision(1) << itr->x + itr->ax*(itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y + itr->ay*(itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->z + (itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->x + itr->ax*(itr->z1 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y + itr->ay*(itr->z1 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->z + (itr->z1 - itr->z) << std::endl;
	//}



	return ret;
}
void Calc_angle(Grain_track&track,int face) {

	sort(track.gr.begin(), track.gr.end(), sort_grain_z);
	track.x0 = track.gr.begin()->x;
	track.y0 = track.gr.begin()->y;
	track.z0 = track.gr.begin()->z;
	track.x1 = track.gr.rbegin()->x;
	track.y1 = track.gr.rbegin()->y;
	track.z1 = track.gr.rbegin()->z;
	if (track.z1 - track.z0 == 0) {
		track.ax = (track.x1 - track.x0)/0.1;
		track.ay = (track.y1 - track.y0)/0.1;
		track.sig_x = -1;
		track.sig_y = -1;
		track.x = (track.x0 + track.x1) / 2;
		track.y = (track.y0 + track.y1) / 2;
		track.z = (track.z0 + track.z1) / 2;
		return;
	}
	if (face == 1) {
		track.z = track.z1;
	}
	else if (face == 2) {
		track.z = track.z0;
	}
	double x, y, n, xy, x2;
	x = 0;
	x2 = 0;
	xy = 0;
	y = 0;
	n = 0;
	for (auto itr = track.gr.begin(); itr != track.gr.end(); itr++) {
		x += itr->z;
		y += itr->x;
		xy += itr->z*itr->x;
		x2 += itr->z*itr->z;
		n += 1;
	}
	if (n*x2 - x * x == 0) {
		track.ax = 999999;
	}
	track.ax = (n*xy - x * y) / (n*x2 - x * x);
	track.x = (x2*y - xy * x) / (n*x2 - x * x);
	track.x = track.x + track.ax*(track.z-0);

	x = 0;
	x2 = 0;
	xy = 0;
	y = 0;
	n = 0;
	for (auto itr = track.gr.begin(); itr != track.gr.end(); itr++) {
		x += itr->z;
		y += itr->y;
		xy += itr->z*itr->y;
		x2 += itr->z*itr->z;
		n += 1;
	}
	if (n*x2 - x * x == 0) {
		track.ay = 999999;
	}
	track.ay = (n*xy - x * y) / (n*x2 - x * x);
	track.y = (x2*y - xy * x) / (n*x2 - x * x);
	track.y = track.y + track.ay*(track.z-0);

	if (track.gr.size() > 10) {

		x = 0;
		x2 = 0;
		xy = 0;
		y = 0;
		n = 0;
		double pred_pos = 0;
		for (auto itr = track.gr.begin(); itr != track.gr.end(); itr++) {
			pred_pos = track.x + track.ax*(itr->z - track.z);
			if (fabs(pred_pos - itr->x) > 10)continue;
			pred_pos = track.y + track.ay*(itr->z - track.z);
			if (fabs(pred_pos - itr->y) > 10)continue;

			x += itr->z;
			y += itr->x;
			xy += itr->z*itr->x;
			x2 += itr->z*itr->z;
			n += 1;
		}
		if (n>2&&n*x2 - x * x != 0) {
			track.ax = (n*xy - x * y) / (n*x2 - x * x);
			track.x = (x2*y - xy * x) / (n*x2 - x * x);
			track.x = track.x + track.ax*(track.z - 0);
		}

		x = 0;
		x2 = 0;
		xy = 0;
		y = 0;
		n = 0;
		for (auto itr = track.gr.begin(); itr != track.gr.end(); itr++) {
			pred_pos = track.x + track.ax*(itr->z - track.z);
			if (fabs(pred_pos - itr->x) > 10)continue;
			pred_pos = track.y + track.ay*(itr->z - track.z);
			if (fabs(pred_pos - itr->y) > 10)continue;

			x += itr->z;
			y += itr->y;
			xy += itr->z*itr->y;
			x2 += itr->z*itr->z;
			n += 1;
		}
		if (n > 2 && n*x2 - x * x != 0) {
			track.ay = (n*xy - x * y) / (n*x2 - x * x);
			track.y = (x2*y - xy * x) / (n*x2 - x * x);
			track.y = track.y + track.ay*(track.z - 0);
		}


	}
	//printf("%g %g %g %g %g %g %g %g\n", track.ax, track.ay, track.x, track.y, track.z, track.x0, track.y0, track.z0);
	return;
}
void Calc_angle_grain_density(std::vector<Grain_track>&tr, std::vector<GrainInformation> &gr, double circle_radius,int face ) {

	double z0, z1;

	double xmin, ymin, zmin;
	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		if (itr == gr.begin()) {
			xmin = itr->x;
			ymin = itr->y;
			zmin = itr->z;
			z0 = itr->z;
			z1 = itr->z;
		}
		xmin = std::min(xmin, itr->x);
		ymin = std::min(ymin, itr->y);
		zmin = std::min(zmin, itr->z);
		z0 = std::min(z0, itr->z);
		z1 = std::max(z1, itr->z);
	}

	std::multimap<std::tuple<int, int, int>, GrainInformation>grain_map;
	std::set<int> z_list;
	double x_bin = 4;
	double y_bin = 4;
	double z_bin = 1;
	std::tuple<int, int, int> id;
	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		std::get<0>(id) = (itr->x - xmin) / x_bin;
		std::get<1>(id) = (itr->y - ymin) / y_bin;
		std::get<2>(id) = (itr->z - zmin) / z_bin;
		z_list.insert((itr->z - zmin) / z_bin);
		grain_map.insert(std::make_pair(id, *itr));
	}

	double nom_x, nom_y, nom_z;
	double track_x, track_y;
	int ix, iy;
	int count = 0, all = tr.size();
	std::map<int, GrainInformation> hit_grain;
	for (auto itr = tr.begin(); itr != tr.end();) {
		if (count % 1000 == 0) {
			printf("\r grain density calculation %6d/%6d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		itr->gr.clear();
		itr->all_grain_num = 0;
		itr->track_length = fabs(z0 - z1)*sqrt(1 + itr->ax*itr->ax + itr->ay*itr->ay);

		for (auto itr2 = z_list.begin(); itr2 != z_list.end(); itr2++) {

			nom_z = ((*itr2) + 0.5)*z_bin + zmin;
			nom_x = itr->x + itr->ax*(nom_z - itr->z);
			nom_y = itr->y + itr->ay*(nom_z - itr->z);
			std::get<2>(id) = (nom_z - zmin) / z_bin;
			ix = (nom_x - xmin) / x_bin;
			iy = (nom_y - ymin) / y_bin;
			for (int iix = -1; iix <= 1; iix++) {
				for (int iiy = -1; iiy <= 1; iiy++) {
					std::get<0>(id) = ix + iix;
					std::get<1>(id) = iy + iiy;
					if (grain_map.count(id) == 0)continue;
					auto range = grain_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						track_x = itr->x + itr->ax*(res->second.z - itr->z);
						track_y = itr->y + itr->ay*(res->second.z - itr->z);
						if (pow(track_x - res->second.x, 2) + pow(track_y - res->second.y, 2) < circle_radius*circle_radius) {
							itr->all_grain_num += 1;
							itr->gr.push_back(res->second);
							hit_grain.insert(std::make_pair(res->second.id, res->second));
						}
					}
				}
			}
		}

		if (itr->gr.size() < 5) {
			itr = tr.erase(itr);
			continue;
		}

		itr++;
	}
	printf("\r grain density calculation %6d/%6d(%4.1lf%%)\n", count, all, count*100. / all);

	//拾ったgrainで位置角度の再計算
	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		Calc_angle(*itr,face);
	}

}

void Calc_grain_density(std::vector<Grain_track>&tr, std::vector<GrainInformation> &gr,int face) {
	Calc_angle_grain_density(tr, gr, 1.5,face);

	std::multimap<double, Grain_track* >track_sort;
	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		track_sort.insert(std::make_pair(itr->all_grain_num / itr->track_length, &(*itr)));
	}
	int track_id = 0;
	for (auto itr = track_sort.rbegin(); itr != track_sort.rend(); itr++) {
		itr->second->id = track_id;
		track_id++;
	}
	sort(tr.begin(), tr.end(), sort_track_id);


	//std::ofstream ofs("grain_track_comp.txt");
	//double tmp_x, tmp_y;
	//for (auto itr = tr.begin(); itr != tr.end(); itr++) {
	//	for (auto itr2 = itr->gr.begin(); itr2 != itr->gr.end(); itr2++) {
	//		tmp_x = itr->x + itr->ax*(itr2->z - itr->z);
	//		tmp_y = itr->y + itr->ay*(itr2->z - itr->z);

	//		ofs << std::right << std::fixed
	//			<< std::setw(8) << std::setprecision(0) << itr->id << " "
	//			<< std::setw(8) << std::setprecision(4) << itr->ax << " "
	//			<< std::setw(8) << std::setprecision(4) << itr->ay << " "
	//			<< std::setw(8) << std::setprecision(1) << itr->x << " "
	//			<< std::setw(8) << std::setprecision(1) << itr->y << " "
	//			<< std::setw(8) << std::setprecision(1) << itr->z << " "
	//			<< std::setw(8) << std::setprecision(1) << itr->z0 << " "
	//			<< std::setw(8) << std::setprecision(1) << itr->z1 << " "
	//			<< std::setw(8) << std::setprecision(1) << itr2->x << " "
	//			<< std::setw(8) << std::setprecision(1) << itr2->y << " "
	//			<< std::setw(8) << std::setprecision(1) << itr2->z << " "
	//			<< std::setw(8) << std::setprecision(2) << sqrt(pow(itr2->x - tmp_x, 2) + pow(itr2->y - tmp_y, 2)) << std::endl;
	//	}
	//}

	//出力部分
	/*
	std::ofstream ofs("grain_track_gd.txt");
	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		//if (itr->pixelnum < 10)continue;
		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(4) << itr->ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->z0 << " "
			<< std::setw(10) << std::setprecision(1) << itr->z1 << " "
			<< std::setw(5) << std::setprecision(0) << itr->gr.size() << " "
			<< std::setw(7) << std::setprecision(1) << itr->track_length << " "
			<< std::setw(6) << std::setprecision(0) << itr->all_grain_num << std::endl;
	}

	std::vector<GrainInformation> hit_gr;
	for (auto itr = hit_grain.begin(); itr != hit_grain.end(); itr++) {
		hit_gr.push_back(itr->second);
	}
	output_grain("track_hit_grain.txt", hit_gr);


	 std::vector<Grain_track> output_tr[5];
	 int id_gd;
	 for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		 id_gd = itr->all_grain_num / itr->track_length * 10;
		 if (id_gd > 4)id_gd = 4;
		 output_tr[id_gd] . push_back(*itr);
	}

	 for (int i = 0; i < 5; i++) {
		 std::stringstream file_out;
		 file_out << "grain_track_" << std::setw(1) << i << ".txt";
		 std::ofstream ofs(file_out.str());
		 for (auto itr = output_tr[i].begin(); itr != output_tr[i].end(); itr++) {
			 //if (itr->pixelnum < 10)continue;
			 ofs << std::right << std::fixed
				 << std::setw(10) << std::setprecision(1) << itr->x + itr->ax*(itr->z0 - itr->z) << " "
				 << std::setw(10) << std::setprecision(1) << itr->y + itr->ay*(itr->z0 - itr->z) << " "
				 << std::setw(10) << std::setprecision(1) << itr->z + (itr->z0 - itr->z) << " "
				 << std::setw(10) << std::setprecision(1) << itr->x + itr->ax*(itr->z1 - itr->z) << " "
				 << std::setw(10) << std::setprecision(1) << itr->y + itr->ay*(itr->z1 - itr->z) << " "
				 << std::setw(10) << std::setprecision(1) << itr->z + (itr->z1 - itr->z) << std::endl;
		 }

	 }
	 */
}

void Cut_track(std::vector<Grain_track>&tr, std::vector<GrainInformation> &gr,int face,std::string file_out_sn) {
	//半径1.0未満のみの抽出
	Calc_angle_grain_density(tr, gr, 1.0,face);
	const double cut_dz_max = 10;
	const double cut_dz_all = 40;
	std::vector<Grain_track> sel;
	double max_dist_z, z_dist, gd, z0, z1;
	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		//sort(itr->gr.begin(), itr->gr.end(), sort_grain_z);
		for (auto itr2 = itr->gr.begin(); itr2 != itr->gr.end(); itr2++) {
			if (std::next(itr2, 1) != itr->gr.end()) {
				if (itr2 == itr->gr.begin()) {
					max_dist_z = fabs(itr2->z - std::next(itr2, 1)->z);
				}
				max_dist_z = std::max(max_dist_z, fabs(itr2->z - std::next(itr2, 1)->z));
			}
		}
		z0 = itr->gr.begin()->z;
		z1 = itr->gr.rbegin()->z;
		z_dist = fabs(z0 - z1);
		if (z_dist < cut_dz_all)continue;
		if (max_dist_z > cut_dz_max)continue;
		sel.push_back(*itr);
	}

	//ofs.open("grain_track_comp2.txt");
	//std::ofstream ofs("grain_track_comp2.txt");
	//double tmp_x, tmp_y;
	//for (auto itr = tr.begin(); itr != tr.end(); itr++) {
	//	for (auto itr2 = itr->gr.begin(); itr2 != itr->gr.end(); itr2++) {
	//		tmp_x = itr->x + itr->ax*(itr2->z - itr->z);
	//		tmp_y = itr->y + itr->ay*(itr2->z - itr->z);

	//		ofs << std::right << std::fixed
	//			<< std::setw(8) << std::setprecision(0) << itr->id << " "
	//			<< std::setw(8) << std::setprecision(4) << itr->ax << " "
	//			<< std::setw(8) << std::setprecision(4) << itr->ay << " "
	//			<< std::setw(8) << std::setprecision(1) << itr->x << " "
	//			<< std::setw(8) << std::setprecision(1) << itr->y << " "
	//			<< std::setw(8) << std::setprecision(1) << itr->z << " "
	//			<< std::setw(8) << std::setprecision(1) << itr->z0 << " "
	//			<< std::setw(8) << std::setprecision(1) << itr->z1 << " "
	//			<< std::setw(8) << std::setprecision(1) << itr2->x << " "
	//			<< std::setw(8) << std::setprecision(1) << itr2->y << " "
	//			<< std::setw(8) << std::setprecision(1) << itr2->z << " "
	//			<< std::setw(8) << std::setprecision(2) << sqrt(pow(itr2->x - tmp_x, 2) + pow(itr2->y - tmp_y, 2)) << std::endl;
	//	}
	//}

	//ofs.close();
	//ofs.open("grain_track_sel.txt",std::ios::out);
	//for (auto itr = sel.begin(); itr != sel.end(); itr++) {
	//	//if (itr->pixelnum < 10)continue;
	//	ofs << std::right << std::fixed
	//		<< std::setw(10) << std::setprecision(1) << itr->x + itr->ax*(itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y + itr->ay*(itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->z + (itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->x + itr->ax*(itr->z1 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y + itr->ay*(itr->z1 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->z + (itr->z1 - itr->z) << std::endl;
	//}



	//ofs.close();
	//ofs.open("grain_track_signal_noise.txt", std::ios::out);
	//std::ofstream ofs("grain_track_signal_noise.txt");
	std::ofstream ofs(file_out_sn);
	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		for (auto itr2 = itr->gr.begin(); itr2 != itr->gr.end(); itr2++) {
			if (std::next(itr2, 1) != itr->gr.end()) {
				if (itr2 == itr->gr.begin()) {
					max_dist_z = fabs(itr2->z - std::next(itr2, 1)->z);
				}
				max_dist_z = std::max(max_dist_z, fabs(itr2->z - std::next(itr2, 1)->z));
			}
		}
		z0 = itr->gr.begin()->z;
		z1 = itr->gr.rbegin()->z;
		z_dist = fabs(z0 - z1);
		gd = itr->all_grain_num*100. / (z_dist*sqrt(1 + itr->ax*itr->ax + itr->ay*itr->ay));


		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(0) << itr->id << " "
			<< std::setw(8) << std::setprecision(2) << gd << " "
			<< std::setw(8) << std::setprecision(2) << z_dist << " "
			<< std::setw(8) << std::setprecision(2) << max_dist_z << " "
			<< std::setw(8) << std::setprecision(0) << itr->all_grain_num<< " "
			<< std::setw(8) << std::setprecision(2) << z0 << " "
			<< std::setw(8) << std::setprecision(2) << z1 << std::endl;
	}

	ofs.close();


	tr = sel;
}

std::vector<Grain_track> track_clustering(std::vector<Grain_track>&tr) {
	sort(tr.begin(), tr.end(), sort_track_gnum);
	double mean_z=0;
	int count = 0;

	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		mean_z += (itr->z0 + itr->z1) / 2;
		count++;
	}
	mean_z = mean_z / count;

	std::vector<std::pair<bool, Grain_track>> cluster;
	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		cluster.push_back(std::make_pair(true, *itr));
	}
	double x_tmp[2], y_tmp[2];
	double ax_tmp[2], ay_tmp[2];
	for (int i = 0; i < cluster.size(); i++) {
		x_tmp[0] = cluster[i].second.x + cluster[i].second.ax*(mean_z - cluster[i].second.z);
		y_tmp[0] = cluster[i].second.y + cluster[i].second.ay*(mean_z - cluster[i].second.z);
		ax_tmp[0] = cluster[i].second.ax;
		ay_tmp[0] = cluster[i].second.ay;
		for (int j = i + 1; j < cluster.size(); j++) {
			x_tmp[1] = cluster[j].second.x + cluster[j].second.ax*(mean_z - cluster[j].second.z);
			y_tmp[1] = cluster[j].second.y + cluster[j].second.ay*(mean_z - cluster[j].second.z);
			ax_tmp[1] = cluster[j].second.ax;
			ay_tmp[1] = cluster[j].second.ay;

			if (pow(x_tmp[1] - x_tmp[0], 2) + pow(y_tmp[1] - y_tmp[0], 2) > 20 * 20)continue;
			if (pow(ax_tmp[1] - ax_tmp[0], 2) + pow(ay_tmp[1] - ay_tmp[0], 2) > 0.2*0.2)continue;
			cluster[j].first = false;
		}
	}

	std::vector<Grain_track> ret;
	for (auto itr = cluster.begin(); itr != cluster.end(); itr++) {
		if (!itr->first)continue;
		ret.push_back(itr->second);
	}

	//std::ofstream ofs("grain_track_sel2.txt");
	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	//if (itr->pixelnum < 10)continue;
	//	ofs << std::right << std::fixed
	//		<< std::setw(10) << std::setprecision(1) << itr->x + itr->ax*(itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y + itr->ay*(itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->z + (itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->x + itr->ax*(itr->z1 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y + itr->ay*(itr->z1 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->z + (itr->z1 - itr->z) << std::endl;
	//}

	return ret;
}