#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>

std::set<int> eventid_pickup(std::vector<mfile0::M_Chain>&c);
std::set<int> read_shower(std::string filename);

int main(int argc, char**argv) {
	if (argc != 8) {
		fprintf(stderr, "usage:filename stop pene edgeout shower out-txt out-bin\n");
		exit(1);
	}
	std::string file_in_sf = argv[1];
	std::string file_in_stop = argv[2];
	std::string file_in_penetrate = argv[3];
	std::string file_in_edgeout = argv[4];
	std::string file_in_shower = argv[5];
	std::string file_out_sf_txt = argv[6];
	std::string file_out_sf_bin = argv[7];

	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_txt(file_in_sf);

	mfile0::Mfile stop, pene, edgeout;
	mfile1::read_mfile_extension(file_in_stop, stop);
	mfile1::read_mfile_extension(file_in_penetrate, pene);
	mfile1::read_mfile_extension(file_in_edgeout, edgeout);


	std::set<int> stop_id = eventid_pickup(stop.chains);
	std::set<int> pene_id = eventid_pickup(pene.chains);
	std::set<int> edgeout_id = eventid_pickup(edgeout.chains);
	std::set<int> shower_id = read_shower(file_in_shower);
	printf("all chain %5d\n", stop.chains.size()+ pene.chains.size()+ edgeout.chains.size());

	printf("stop      %5d\n", stop_id.size());
	printf("penetrat  %5d\n", pene_id.size());
	printf("edgeout   %5d\n", edgeout_id.size());
	printf("shower    %5d\n", shower_id.size());
	int count[4] = {};
	for (auto &f : sf) {
		if (stop_id.count(f.eventid) == 1) {
			f.ecc_track_type = 0;
			count[0] += 1;
		}
		else if (pene_id.count(f.eventid) == 1) {
			f.ecc_track_type = 1;
			count[1] += 1;
		}
		else if (edgeout_id.count(f.eventid) == 1) {
			f.ecc_track_type = 2;
			count[2] += 1;
		}
		else if (shower_id.count(f.eventid) == 1) {
			f.ecc_track_type = -2;
			count[3] += 1;
		}
	}
	printf("------------add flg-----------\n");
	printf("stop      %5d\n", count[0]);
	printf("penetrat  %5d\n", count[1]);
	printf("edgeout   %5d\n", count[2]);
	printf("shower    %5d\n", count[3]);

	Sharing_file::Write_sharing_file_txt(file_out_sf_txt, sf);
	Sharing_file::Write_sharing_file_bin(file_out_sf_bin, sf);
}
std::set<int> eventid_pickup(std::vector<mfile0::M_Chain>&c) {

	std::set<int> ret;
	for (auto &chain : c) {
		ret.insert(chain.basetracks.begin()->group_id);
	}
	return ret;

}
std::set<int> read_shower(std::string filename) {
	std::ifstream ifs(filename);
	int eventid, chainid, pl, rawid, linknum;
	std::set<int> ret;
	while (ifs >> eventid >> chainid >> pl >> rawid >> linknum) {
		ret.insert(eventid);
	}
	return ret;

}