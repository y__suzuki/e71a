#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>
#include <omp.h>

void microtrack_area(std::string filename, int pos, int zone, double &x_min, double &x_max, double &y_min, double &y_max);
void Read_fvxx_area(std::string filename, int pos, int zone, std::vector<vxx::micro_track_t> &micro, double x_min, double x_max, double y_min, double y_max);
void ghost_filter(std::vector<vxx::micro_track_t>&micro0, std::vector<vxx::micro_track_t>&micro1, std::map < std::pair<int, int>, std::pair<vxx::micro_track_t, vxx::micro_track_t> >&pair);
void fvxx_area(std::string filename, int pos, int zone, double &x_min, double &x_max, double &y_min, double &y_max);
void output_pair(std::string filename, std::map < std::pair<int, int>, std::pair<vxx::micro_track_t, vxx::micro_track_t> >&pair);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in_fvxx1 in_fvxx2 pos zone output\n");
		exit(1);
	}
	std::string file_in_fvxx0 = argv[1];
	std::string file_in_fvxx1 = argv[2];
	int pos = std::stoi(argv[3]);
	int zone = std::stoi(argv[4]);
	std::string file_out_txt = argv[5];
	std::map < std::pair<int, int>, std::pair<vxx::micro_track_t, vxx::micro_track_t> > pair;

	double x_min, x_max, y_min, y_max;
	fvxx_area(file_in_fvxx0, pos, zone, x_min, x_max, y_min, y_max);

	int divide = 5;
	int step = (x_max - x_min) / divide;
	int overrap = 500;
	double x, y;
	int count = 0;
	for (x = x_min; x <= x_max + overrap; x += step) {
		for (y = y_min; y <= y_max + overrap; y += step) {
			printf("count %d \n", count);
			//printf("x: %8.1lf %8.1lf  y: %8.1lf %8.1lf \n", x - overrap, x + step + overrap, y - overrap, y + step + overrap);
			count++;
			std::vector<vxx::micro_track_t> micro0;
			std::vector<vxx::micro_track_t> micro1;
			Read_fvxx_area(file_in_fvxx0, pos, zone, micro0, x - overrap, x + step + overrap, y - overrap, y + step + overrap);
			if (micro0.size() == 0)continue;
			Read_fvxx_area(file_in_fvxx1, pos, zone, micro1, x - overrap*2, x + step + overrap*2, y - overrap*2, y + step + overrap*2);

			ghost_filter(micro0, micro1, pair);
		}
	}
	fprintf(stderr, "all matched %d\n", pair.size());
	output_pair(file_out_txt, pair);

}
void fvxx_area(std::string filename, int pos, int zone, double &x_min, double &x_max, double &y_min, double &y_max) {
	int64_t count = 0;
	vxx::FvxxReader fr;
	std::vector<vxx::CutArea> area;
	if (fr.Begin(filename, pos, zone))
	{
		vxx::HashEntry h;
		vxx::micro_track_t m;
		while (fr.NextHashEntry(h))
		{
			while (fr.NextMicroTrack(m))
			{
				if (count % 1000000 == 0) {
					fprintf(stderr, "\r count %lld", count);
				}
				count++;
				if (count == 0) {
					x_min = m.x;
					x_max = m.x;
					y_min = m.y;
					y_max = m.y;
				}
				x_min = std::min(x_min, m.x);
				x_max = std::max(x_max, m.x);
				y_min = std::min(y_min, m.y);
				y_max = std::max(y_max, m.y);
			}
		}
		fr.End();
		fprintf(stderr, "\r count %lld\n", count);
	}
}
void Read_fvxx_area(std::string filename, int pos, int zone, std::vector<vxx::micro_track_t> &micro, double x_min, double x_max, double y_min, double y_max) {
	micro.clear();
	vxx::FvxxReader fr;
	std::vector<vxx::CutArea> area;
	area.push_back(vxx::CutArea(x_min, x_max, y_min, y_max));//xmin, xmax, ymin, ymax
	micro = fr.ReadAll(filename, pos, zone, vxx::opt::a = area);
}
void microtrack_area(std::string filename, int pos, int zone, double &x_min, double &x_max, double &y_min, double &y_max) {
	vxx::FvxxReader fr;
	int64_t count = 0;
	if (fr.Begin(filename, pos, zone))
	{
		vxx::HashEntry h;
		vxx::micro_track_t f;
		while (fr.NextHashEntry(h))
		{
			while (fr.NextMicroTrack(f))
			{
				if (count == 0) {
					x_min = f.x;
					x_max = f.x;
					y_min = f.y;
					y_max = f.y;
				}
				x_min = std::min(x_min, f.x);
				x_max = std::max(x_max, f.x);
				y_min = std::min(y_min, f.y);
				y_max = std::max(y_max, f.y);

				if (count % 1000000 == 0) {
					fprintf(stderr, "\r area search... %12lld", count);
				}
				count++;
			}
		}
		fr.End();
	}
	fprintf(stderr, "\r area search... %12lld fin\n", count);
}
void ghost_filter(std::vector<vxx::micro_track_t>&micro0, std::vector<vxx::micro_track_t>&micro1, std::map < std::pair<int,int> ,std::pair<vxx::micro_track_t, vxx::micro_track_t> >&pair) {
	int ini_size = pair.size();
	double x_min, y_min, ax_min, ay_min;
	double  hash_area = 50, hash_angle = 0.05;
	for (auto itr = micro1.begin(); itr != micro1.end(); itr++) {
		itr->px = 0;
		itr->py = 0;
		if (itr == micro1.begin()) {
			x_min = itr->x;
			y_min = itr->y;
			ax_min = itr->ax;
			ay_min = itr->ay;
		}
		x_min = std::min(x_min, itr->x);
		y_min = std::min(y_min, itr->y);
		ax_min = std::min(ax_min, itr->ax);
		ay_min = std::min(ay_min, itr->ay);
	}
	std::tuple<int, int, int, int> id;
	std::multimap < std::tuple<int, int, int, int>, vxx::micro_track_t* > micro_map;
	for (auto itr = micro1.begin(); itr != micro1.end(); itr++) {
		std::get<0>(id) = (itr->x - x_min) / hash_area;
		std::get<1>(id) = (itr->y - y_min) / hash_area;
		std::get<2>(id) = (itr->ax - ax_min) / hash_angle;
		std::get<3>(id) = (itr->ay - ay_min) / hash_angle;
		micro_map.insert(std::make_pair(id, &(*itr)));
	}

	//flg=0 ���T��
	//flg=1 signal(high PH)
	//flg=2 ghost (low PH)
	int64_t count = 0;
#pragma omp parallel for private(id)
	for (int i = 0; i < micro0.size(); i++) {
#pragma omp atomic
		count++;
		if (count % 100000 == 0) {
			fprintf(stderr, "\r ghost filter ...%d/%d(%4.1lf%%)", count, micro0.size(), count*100. / micro0.size());
		}
		int iix_range[2], iiy_range[2], iiax_range[2], iiay_range[2];
		double angle;
		angle = sqrt(micro0[i].ax*micro0[i].ax + micro0[i].ay*micro0[i].ay);
		double dr_pos = angle * 6 + 2;
		double dr_ang = angle * 0.1 + 0.1;
		double dl_pos = 0.8;
		double dl_ang = 0.04;

		iix_range[0] = ((micro0[i].x - x_min) - dr_pos) / hash_area;
		iix_range[1] = ((micro0[i].x - x_min) + dr_pos) / hash_area;
		iiy_range[0] = ((micro0[i].y - y_min) - dr_pos) / hash_area;
		iiy_range[1] = ((micro0[i].y - y_min) + dr_pos) / hash_area;
		iiax_range[0] = ((micro0[i].ax - ax_min) - dr_ang) / hash_angle;
		iiax_range[1] = ((micro0[i].ax - ax_min) + dr_ang) / hash_angle;
		iiay_range[0] = ((micro0[i].ay - ay_min) - dr_ang) / hash_angle;
		iiay_range[1] = ((micro0[i].ay - ay_min) + dr_ang) / hash_angle;
		//printf("%d %d %d %d %d %d %d %d\n", iix_range[0], iix_range[1], iiy_range[0], iiy_range[1], iiax_range[0], iiax_range[1], iiay_range[0], iiay_range[1]);
		for (int iix = iix_range[0]; iix <= iix_range[1]; iix++) {
			for (int iiy = iiy_range[0]; iiy <= iiy_range[1]; iiy++) {
				for (int iiax = iiax_range[0]; iiax <= iiax_range[1]; iiax++) {
					for (int iiay = iiay_range[0]; iiay <= iiay_range[1]; iiay++) {
						std::get<0>(id) = iix;
						std::get<1>(id) = iiy;
						std::get<2>(id) = iiax;
						std::get<3>(id) = iiay;

						if (micro_map.count(id) == 0)continue;
						else {
							auto range = micro_map.equal_range(id);
							for (auto res = range.first; res != range.second; res++) {
								if (fabs((res->second->ax - micro0[i].ax)*micro0[i].ay - (res->second->ay - micro0[i].ay)*micro0[i].ax) > dl_ang*angle)continue;
								if (fabs((res->second->x - micro0[i].x)*micro0[i].ay - (res->second->y - micro0[i].y)*micro0[i].ax) > dl_pos*angle)continue;
								if (fabs((res->second->ax - micro0[i].ax)*micro0[i].ax + (res->second->ay - micro0[i].ay)*micro0[i].ay) > dr_ang*angle)continue;
								if (fabs((res->second->x - micro0[i].x)*micro0[i].ax + (res->second->y - micro0[i].y)*micro0[i].ay) > dr_pos*angle)continue;
#pragma omp critical
								pair.insert(std::make_pair(std::make_pair(micro0[i].rawid, res->second->rawid), std::make_pair(micro0[i], *res->second)));

							}
						}
					}
				}
			}
		}
	}
	fprintf(stderr, "\r ghost filter ...%d/%d(%4.1lf%%)\n", count, micro0.size(), count*100. / micro0.size());

	fprintf(stderr, "all track %d %d -->matched %d\n", micro0.size(), micro1.size(), pair.size()- ini_size);
}
void output_pair(std::string filename, std::map < std::pair<int, int>, std::pair<vxx::micro_track_t, vxx::micro_track_t> >&pair) {
	std::ofstream ofs(filename);
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr->second.first.rawid << " "
			<< std::setw(7) << std::setprecision(0) << itr->second.first.ph << " "
			<< std::setw(7) << std::setprecision(4) << itr->second.first.ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->second.first.ay << " "
			<< std::setw(12) << std::setprecision(1) << itr->second.first.x << " "
			<< std::setw(12) << std::setprecision(1) << itr->second.first.y << " "
			<< std::setw(12) << std::setprecision(1) << itr->second.first.z1 << " "
			<< std::setw(12) << std::setprecision(1) << itr->second.first.z2 << " "
			<< std::setw(12) << std::setprecision(0) << itr->second.second.rawid << " "
			<< std::setw(7) << std::setprecision(0) << itr->second.second.ph << " "
			<< std::setw(7) << std::setprecision(4) << itr->second.second.ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->second.second.ay << " "
			<< std::setw(12) << std::setprecision(1) << itr->second.second.x << " "
			<< std::setw(12) << std::setprecision(1) << itr->second.second.y << " "
			<< std::setw(12) << std::setprecision(1) << itr->second.first.z1 << " "
			<< std::setw(12) << std::setprecision(1) << itr->second.first.z2 << std::endl;

	}
}