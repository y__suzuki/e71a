#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::set<int> eventid_pickup(std::vector<Sharing_file::Sharing_file> &sf, int eventid);
std::vector<mfile0::M_Base> track_pickup(std::vector<mfile0::M_Chain> &c, std::set<int> &eventid);
void output_track(std::string filename, std::vector<mfile0::M_Base> &b);


int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:input-mfile eventnum sharingfile output-file\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	int event_num = std::stoi(argv[2]);
	std::string file_in_sf = argv[3];
	std::string file_out_track = argv[4];

	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_extension(file_in_sf);
	std::set<int> event_list = eventid_pickup(sf, event_num);



	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);



	std::vector<mfile0::M_Base> base = track_pickup(m.chains, event_list);

	output_track(file_out_track, base);

	return 0;

}	
std::set<int> eventid_pickup(std::vector<Sharing_file::Sharing_file> &sf, int eventid) {
	int unix_time = -1;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		//printf("%d %d\n", itr->eventid, eventid);
		if (itr->eventid == eventid) {
			unix_time = itr->unix_time;
		}
	}
	printf("unix time %d\n", unix_time);
	std::set<int> ret;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		if (itr->unix_time== unix_time) {
			ret.insert(itr->eventid);
			printf("eventid = %d\n", itr->eventid);
		}
	}
	return ret;
}
std::vector<mfile0::M_Base> track_pickup(std::vector<mfile0::M_Chain> &c, std::set<int> &eventid) {
	std::vector<mfile0::M_Base> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (eventid.count(itr->basetracks.begin()->group_id) == 1) {
			ret.push_back(*itr->basetracks.rbegin());
		}
	}
	printf("track num = %d\n", ret.size());

	return ret;


}
void output_track(std::string filename, std::vector<mfile0::M_Base> &b) {
	std::ofstream ofs(filename);
	if (b.size() == 0)return;
	for (auto itr = b.begin(); itr != b.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(5) << std::setprecision(0) << itr->pos / 10 << " "
			<< std::setw(10) << std::setprecision(0) << itr->group_id << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->x<<" "
			<< std::setw(8) << std::setprecision(1) << itr->y << std::endl;
	}



}