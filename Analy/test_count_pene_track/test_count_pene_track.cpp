#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>

uint64_t chain_count(mfile1::MFile&m, std::map<int, std::tuple<double, double, double, double>>&area_range, std::map<int, double> &z_map);
void Mfile_Area(mfile1::MFile&m, std::map<int, std::tuple<double, double, double, double>>&area_range, std::map<int, double> &z_map);

int main(int argc, char**argv) {

	std::string file_in_mfile = "I:\\NINJA\\E71a\\work\\suzuki\\PID\\m1_fill.bmf";
	mfile1::MFile m;
	mfile1::read_mfile(file_in_mfile, m);
	std::map<int, std::tuple<double, double, double, double>>area_range;
	std::map<int, double> z_map;
	Mfile_Area(m, area_range, z_map);

	uint64_t num = chain_count(m, area_range, z_map);
	printf("PL004 track = %llu\n", num);
	printf("density = %.3lf/cm^2\n", num*1.0 / (25.*25.));

}
void Mfile_Area(mfile1::MFile&m, std::map<int, std::tuple<double, double, double, double>>&area_range, std::map<int, double> &z_map) {
	for (auto itr = m.all_basetracks.begin(); itr != m.all_basetracks.end(); itr++) {
		for (auto itr2 = itr->begin(); itr2 != itr->end(); itr2++) {
			auto res = area_range.insert(std::make_pair(itr2->pos / 10, std::make_tuple(itr2->x, itr2->x, itr2->y, itr2->y)));
			if (!res.second) {
				std::get<0>(res.first->second) = std::min(std::get<0>(res.first->second), itr2->x);
				std::get<1>(res.first->second) = std::max(std::get<1>(res.first->second), itr2->x);
				std::get<2>(res.first->second) = std::min(std::get<2>(res.first->second), itr2->y);
				std::get<3>(res.first->second) = std::max(std::get<3>(res.first->second), itr2->y);
			}
			auto res2 = z_map.insert(std::make_pair(itr2->pos / 10, itr2->z));
		}
	}
	//for (auto itr = area_range.begin(); itr != area_range.end(); itr++) {
	//	printf("%3d %.1lf %.1lf %.1lf %.1lf\n", itr->first, std::get<0>(itr->second), std::get<1>(itr->second), std::get<2>(itr->second), std::get<3>(itr->second));

	//}
}

uint64_t chain_count(mfile1::MFile&m, std::map<int, std::tuple<double, double, double, double>>&area_range, std::map<int, double> &z_map) {
	uint64_t count = 0;
	mfile1::MFileBase1 base_up, base_down;
	bool flg;
	double dz, ex_x, ex_y;
	std::tuple<double, double, double, double> up_range, down_range;

	for (int c = 0; c < m.chains.size(); c++) {
		if (m.chains[c].pos1 / 10 >= 130)continue;
		if (m.chains[c].pos0 / 10 != 4)continue;
		if (m.chains[c].pos1 / 10 <= 17)continue;
		////

		base_up = *(m.all_basetracks[c].rbegin());
		//上流edge付近ならcut
		flg = false;
		for (int ex_pl = 0; ex_pl <= 3; ex_pl++) {
			//ex_pl分上流に外挿
			if (z_map.count(base_up.pos / 10 + ex_pl) + z_map.count(base_up.pos / 10) != 2)continue;
			up_range = area_range.at(base_up.pos / 10 + ex_pl);
			dz = z_map.at(base_up.pos / 10 + ex_pl) - z_map.at(base_up.pos / 10);
			ex_x = base_up.ax*dz + base_up.x;
			ex_y = base_up.ay*dz + base_up.y;
			if (fabs(std::get<0>(up_range) - ex_x) < 5000)flg = true;
			if (fabs(std::get<1>(up_range) - ex_x) < 5000)flg = true;
			if (fabs(std::get<2>(up_range) - ex_y) < 5000)flg = true;
			if (fabs(std::get<3>(up_range) - ex_y) < 5000)flg = true;
		}
		if (flg)continue;
		////

		count++;
	}
	return count;
}