#include <fstream>
#include <iostream>
#include <ios>
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <map>

#include "DelaunayTriangulation.h"

class align_param {
public:
	int id, signal, ix, iy;
	//視野中心
	double x, y, z;
	//parameter(9);
	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;

};
std::vector<std::string> StringSplit(std::string str);
std::vector <align_param > read_ali(std::string filename, int output=0);

int main(int argc,char**argv) {
	if (argc != 2) {
		fprintf(stderr, "prg file-in-ali\n");
		exit(1);
	}

	std::string file_in_ali = argv[1];
	std::vector <align_param > ali_param=read_ali(file_in_ali, 1);

	std::vector<double> x, y;
	for (auto itr = ali_param.begin(); itr != ali_param.end(); itr++) {
		x.push_back(itr->x);
		y.push_back(itr->y);
	}

	delaunay::DelaunayTriangulation DT(x, y); // (std::vector<double> x, std::vector<double> y, uint32_t seed_)
	DT.execute(); // (double min_delta = 1e-6, double max_delta = 1e-5, int max_miss_count = 30)
	std::vector<delaunay::Edge> edge = DT.get_edges();
	{
		std::ofstream ofs("dt1.txt");
		for (auto e : edge) {
			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.first).x << " "
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.first).y << " "
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.first).z << " "
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.second).x << " "
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.second).y << " "
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.second).z << std::endl;
		}
	}
	{
		double cos_z[2], sin_z[2];
		align_param p[2];
		std::ofstream ofs("dt2.txt");
		for (auto e : edge) {
			p[0] = ali_param.at(e.first);
			p[1] = ali_param.at(e.second);
			cos_z[0] = cos(p[0].z_rot);
			cos_z[1] = cos(p[1].z_rot);
			sin_z[0] = sin(p[0].z_rot);
			sin_z[1] = sin(p[1].z_rot);

			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(1) << p[0].x_shrink*cos_z[0] * p[0].x - p[0].y_shrink*sin_z[0] * p[0].y + p[0].dx << " "
				<< std::setw(8) << std::setprecision(1) << p[0].x_shrink*sin_z[0] * p[0].x + p[0].y_shrink*cos_z[0] * p[0].y + p[0].dy << " "
				<< std::setw(8) << std::setprecision(1) << p[0].z + p[0].dz << " "
				<< std::setw(8) << std::setprecision(1) << p[1].x_shrink*cos_z[1] * p[1].x - p[1].y_shrink*sin_z[1] * p[1].y + p[1].dx << " "
				<< std::setw(8) << std::setprecision(1) << p[1].x_shrink*sin_z[1] * p[1].x + p[1].y_shrink*cos_z[1] * p[1].y + p[1].dy << " "
				<< std::setw(8) << std::setprecision(1) << p[1].z + p[1].dz << std::endl;
		}
	}
	//DT.dump(ofs);

}
std::vector <align_param > read_ali(std::string filename,int output) {
	std::vector <align_param >ret;

	std::ifstream ifs(filename);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (output == 1) {
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
	}

	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	align_param ali;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		//0   6576.4 - 2798.3      0.0 0.0000000 0.0000000 - 0.0072185 1.001882 1.001882 1.000000 0.000000 0.000000 0.000000 - 181.40   476.10 - 20.70

		ali.id = std::stoi(str_v[0]);
		ali.signal = std::stoi(str_v[1]);
		ali.ix = std::stoi(str_v[2]);
		ali.iy= std::stoi(str_v[3]);
		ali.x = std::stod(str_v[4]);
		ali.y = std::stod(str_v[5]);
		ali.z = std::stod(str_v[6]);
		ali.x_rot = std::stod(str_v[7]);
		ali.y_rot = std::stod(str_v[8]);
		ali.z_rot = std::stod(str_v[9]);
		ali.x_shrink = std::stod(str_v[10]);
		ali.y_shrink = std::stod(str_v[11]);
		ali.z_shrink = std::stod(str_v[12]);
		ali.yx_shear = std::stod(str_v[13]);
		ali.zx_shear = std::stod(str_v[14]);
		ali.zy_shear = std::stod(str_v[15]);
		ali.dx = std::stod(str_v[16]);
		ali.dy = std::stod(str_v[17]);
		ali.dz = std::stod(str_v[18]);
		ret.push_back(ali);
		if (cnt % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			if (output == 1) {
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
		}
		cnt++;

	}
	auto size1 = eofpos - begpos;
	if (output == 1) {
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;
	}
	if (cnt == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;


}
std::vector<std::string> StringSplit(std::string str) {
	std::stringstream ss{ str };
	std::vector<std::string> v;
	std::string buf;
	while (std::getline(ss, buf, ' ')) {
		if (buf != "") {
			v.push_back(buf);
		}
	}
	return v;
}
