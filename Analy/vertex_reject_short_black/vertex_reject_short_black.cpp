#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_format {
public:
	int eventid, trackid, pl, rawid, mu_pl, mu_rawid;
	double ax, ay, vph, md, oa, x, y;
};
bool sort_chain_base(const mfile0::M_Base&left, const mfile0::M_Base&right) {
	if (left.pos == right.pos)return left.rawid < right.rawid;
	return left.pos < right.pos;
}

std::vector<mfile0::M_Chain> reject_short_black(std::vector<mfile0::M_Chain>&chain);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage: prg in-mfile out-mfile\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);
	printf("mfile read fin\n");
	m.chains = reject_short_black(m.chains);

	mfile0::write_mfile(file_out_mfile, m);
}
bool judge_mip(double angle, double vph) {
	if (angle < 0.4) {
		return vph < -200 * angle + 200;
	}
	else if (angle < 1.0) {
		return vph < (-100 * angle + 400) / 3;
	}
	return vph < 100;


}
bool judge_mip(mfile0::M_Chain &c) {
	double ax_mean = 0, ay_mean = 0, angle, vph = 0;
	int count = 0;
	for (auto b : c.basetracks) {
		ax_mean += b.ax;
		ay_mean += b.ay;
		vph += b.ph % 10000;
		count++;
	}
	ax_mean = ax_mean / count;
	ay_mean = ay_mean / count;
	angle = sqrt(ax_mean*ax_mean + ay_mean * ay_mean);
	vph = vph / count;

	bool res = judge_mip(angle, vph);
	if (!res) {
		//printf("%d %.4lf %.1lf\n", c.chain_id, angle, vph);
	}
	return res;
}
int count_iron_plate(int pl0, int pl1) {
	int count = 0;
	for (int pl = pl0; pl < pl1; pl++) {
		//次のPLとの間にfeがあるか否か
		if (pl == 3)continue;
		else if (pl <= 14)count++;
		else if (pl == 15)continue;
		else if (pl % 2 == 0)count++;
		else continue;
	}
	return count;

}
bool judge_short_black(mfile0::M_Chain&c) {

	if (judge_mip(c)) {
		if (count_iron_plate(c.pos0 / 10, c.pos1 / 10) >= 5) {
			return true;
		}
	}
	//mipでも鉄板5枚以上なら許す
	else {
		if (count_iron_plate(c.pos0 / 10, c.pos1 / 10) >= 1) {
			return true;
		}
	}
	return false;

}
std::vector<mfile0::M_Chain> reject_short_black(std::vector<mfile0::M_Chain>&chain) {

	std::vector<mfile0::M_Chain> ret;


	int muon_pl = -1;
	for (auto &c : chain) {
		if (c.chain_id == 0) {
			ret.push_back(c);
			muon_pl = c.basetracks.rbegin()->pos / 10;
		}
		else {
			if (judge_short_black(c)) {
				ret.push_back(c);
			}
		}
	}
	return ret;

}
