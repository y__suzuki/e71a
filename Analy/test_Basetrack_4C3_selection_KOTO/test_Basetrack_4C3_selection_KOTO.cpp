#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <set>
#include <list>
struct base_inf {
	std::tuple<int, int, int, int> id;
	vxx::base_track_t b;
};
bool sort_id0(const base_inf &left, const base_inf &right) {
	if (std::get<0>(left.id) == std::get<0>(right.id)) {
		return std::get<1>(left.id) < std::get<1>(right.id);
	}
	return std::get<0>(left.id) < std::get<0>(right.id);
}
bool sort_id1(const base_inf &left, const base_inf &right) {
	if (std::get<2>(left.id) == std::get<2>(right.id)) {
		return std::get<3>(left.id) < std::get<3>(right.id);
	}
	return std::get<2>(left.id) < std::get<2>(right.id);
}
std::vector<vxx::base_track_t> CountTrack(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1, std::vector<vxx::base_track_t> &base2, std::vector<vxx::base_track_t> &base3);
void DeleteSameMicroTrk0(std::list<base_inf>&base);
void DeleteSameMicroTrk1(std::list<base_inf>&base);

int main(int argc, char** argv) {
	if (argc != 7) {
		fprintf(stderr, "prg input-bvxx0 input-bvxx1 input-bvxx2 input-bvxx3 pl out-bvxx\n");
		exit(1);
	}
	std::string file_in_bvxx[4];
	for (int i = 0; i < 4; i++) {
		file_in_bvxx[i] = argv[i + 1];
	}
	int pl = std::stoi(argv[5]);
	std::string file_out_bvxx = argv[6];

	std::vector<vxx::base_track_t> base[4];

	for (int i = 0; i < 4; i++) {
		base[i].reserve(100000000);
		vxx::BvxxReader br;
		base[i] = br.ReadAll(file_in_bvxx[i], pl, 0);
	}

	std::vector<vxx::base_track_t> out = CountTrack(base[0], base[1], base[2], base[3]);
	vxx::BvxxWriter bw;
	bw.Write(file_out_bvxx, pl, 0, out);

}
std::vector<vxx::base_track_t> CountTrack(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1, std::vector<vxx::base_track_t> &base2, std::vector<vxx::base_track_t> &base3) {

	std::set<int> base0_face0;
	std::set<int> base0_face1;
	std::set<int> base1_face0;
	std::set<int> base1_face1;
	std::set<int> base2_face0;
	std::set<int> base2_face1;
	std::set<int> base3_face0;
	std::set<int> base3_face1;
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		base0_face0.insert(itr->m[0].rawid);
		base0_face1.insert(itr->m[1].rawid);
	}
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		base1_face0.insert(itr->m[0].rawid);
		base1_face1.insert(itr->m[1].rawid);
	}
	for (auto itr = base2.begin(); itr != base2.end(); itr++) {
		base2_face0.insert(itr->m[0].rawid);
		base2_face1.insert(itr->m[1].rawid);
	}
	for (auto itr = base3.begin(); itr != base3.end(); itr++) {
		base3_face0.insert(itr->m[0].rawid);
		base3_face1.insert(itr->m[1].rawid);
	}

	std::list<base_inf> sel;

	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		if (base1_face0.count(itr->m[0].rawid) + base2_face1.count(itr->m[1].rawid) == 0)continue;
		base_inf tmp_b;
		tmp_b.id = std::make_tuple(0, itr->m[0].rawid, 0, itr->m[1].rawid);
		tmp_b.b = *itr;
		sel.push_back(tmp_b);
	}
	base0.clear();
	base0.shrink_to_fit();

	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (base0_face0.count(itr->m[0].rawid) + base3_face1.count(itr->m[1].rawid) == 0)continue;
		base_inf tmp_b;
		tmp_b.id = std::make_tuple(0, itr->m[0].rawid, 1, itr->m[1].rawid);
		tmp_b.b = *itr;
		sel.push_back(tmp_b);
	}
	base1.clear();
	base1.shrink_to_fit();

	for (auto itr = base2.begin(); itr != base2.end(); itr++) {
		if (base3_face0.count(itr->m[0].rawid) + base0_face1.count(itr->m[1].rawid) == 0)continue;
		base_inf tmp_b;
		tmp_b.id = std::make_tuple(1, itr->m[0].rawid, 0, itr->m[1].rawid);
		tmp_b.b = *itr;
		sel.push_back(tmp_b);
	}
	base2.clear();
	base2.shrink_to_fit();
	for (auto itr = base3.begin(); itr != base3.end(); itr++) {
		if (base2_face0.count(itr->m[0].rawid) + base1_face1.count(itr->m[1].rawid) == 0)continue;
		base_inf tmp_b;
		tmp_b.id = std::make_tuple(1, itr->m[0].rawid, 1, itr->m[1].rawid);
		tmp_b.b = *itr;
		sel.push_back(tmp_b);
	}
	base3.clear();
	base3.shrink_to_fit();

	DeleteSameMicroTrk0(sel);
	DeleteSameMicroTrk1(sel);



	std::vector<vxx::base_track_t> ret;
	for (auto itr = sel.begin(); itr != sel.end(); itr++) {
		ret.push_back(itr->b);
	}
	return ret;
	int trk_num = ret.size();
}
void DeleteSameMicroTrk0(std::list<base_inf>&base) {
	int all = base.size();
	base.sort(sort_id0);
	double da0, da1;
	for (auto itr = base.begin(); itr != base.end(); ) {
		if (std::next(itr, 1) == base.end())break;
		auto itr_next = std::next(itr, 1);
		if (std::get<0>(itr->id) == std::get<0>(itr_next->id) && std::get<1>(itr->id) == std::get<1>(itr_next->id)) {
			if (itr->b.m[1].ph < itr_next->b.m[1].ph) {
				itr = base.erase(itr);
			}
			else if (itr->b.m[1].ph > itr_next->b.m[1].ph) {
				base.erase(itr_next);
			}
			else {
				da0 = (itr->b.ax - itr->b.m[1].ax)*(itr->b.ax - itr->b.m[1].ax) + (itr->b.ay - itr->b.m[1].ay)*(itr->b.ay - itr->b.m[1].ay);
				da1 = (itr_next->b.ax - itr_next->b.m[1].ax)*(itr_next->b.ax - itr_next->b.m[1].ax) + (itr_next->b.ay - itr_next->b.m[1].ay)*(itr_next->b.ay - itr_next->b.m[1].ay);
				if (da0 < da1) {
					itr = base.erase(itr);
				}
				else {
					base.erase(itr_next);
				}
			}
		}
		else {
			itr++;
		}
	}
	int sel = base.size();
	fprintf(stderr, "Delete Same MicroTrk (isg2) %d/%d\n", sel, all);
}
void DeleteSameMicroTrk1(std::list<base_inf>&base) {
	int all = base.size();
	base.sort(sort_id1);
	double da0, da1;
	for (auto itr = base.begin(); itr != base.end(); ) {
		if (std::next(itr, 1) == base.end())break;
		auto itr_next = std::next(itr, 1);
		if (std::get<2>(itr->id) == std::get<2>(itr_next->id) && std::get<3>(itr->id) == std::get<3>(itr_next->id)) {
			if (itr->b.m[0].ph < itr_next->b.m[0].ph) {
				itr = base.erase(itr);
			}
			else if (itr->b.m[0].ph > itr_next->b.m[0].ph) {
				base.erase(itr_next);
			}
			else {
				da0 = (itr->b.ax - itr->b.m[0].ax)*(itr->b.ax - itr->b.m[0].ax) + (itr->b.ay - itr->b.m[0].ay)*(itr->b.ay - itr->b.m[0].ay);
				da1 = (itr_next->b.ax - itr_next->b.m[0].ax)*(itr_next->b.ax - itr_next->b.m[0].ax) + (itr_next->b.ay - itr_next->b.m[0].ay)*(itr_next->b.ay - itr_next->b.m[0].ay);
				if (da0 < da1) {
					itr = base.erase(itr);
				}
				else {
					base.erase(itr_next);
				}
			}
		}
		else {
			itr++;
		}
	}
	int sel = base.size();
	fprintf(stderr, "Delete Same MicroTrk (isg1) %d/%d\n", sel, all);
}
