#pragma comment(lib,"FILE_structure.lib")

#include <FILE_structure.hpp>


std::vector<mfile0::M_Chain> nseg_cut(std::vector<mfile0::M_Chain> &c);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg in-mfile out-mfile\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	m.chains = nseg_cut(m.chains);

	mfile1::write_mfile_extension(file_out_mfile, m);

}
std::vector<mfile0::M_Chain> nseg_cut(std::vector<mfile0::M_Chain> &c) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->nseg < 10)continue;
		ret.push_back(*itr);
	}
	return ret;
}
