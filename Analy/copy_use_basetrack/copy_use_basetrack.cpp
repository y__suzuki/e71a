#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};

int64_t Linklet_header_num(std::string filename);

void read_linklet_list(std::string filename, std::set<std::pair<int, int>> &base_id, std::set<int>&pl_list);
void read_base(std::string file_in_ECC, int pl, std::vector<netscan::base_track_t> &base, std::set<std::pair<int, int>> &base_id);

std::vector<netscan::base_track_t> base_convert(std::vector<vxx::base_track_t >&base);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg:file-in-link file-in-ECC file-out-base\n");
		exit(1);
	}

	std::string file_in_link = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_out_base = argv[3];

	std::set<std::pair<int, int>> base_id;
	std::set<int>pl_list;
	read_linklet_list(file_in_link, base_id, pl_list);
	std::vector<netscan::base_track_t> base;
	base.reserve(base_id.size());
	for (auto itr = pl_list.begin(); itr != pl_list.end(); itr++) {
		read_base(file_in_ECC, *itr, base, base_id);
	}
	printf("id   size =%lld\n", base_id.size());
	printf("base size =%lld\n", base.size());

	netscan::write_basetrack_bin(file_out_base, base);

}
int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}

void read_linklet_list(std::string filename, std::set<std::pair<int,int>> &base_id,std::set<int>&pl_list) {
	int64_t link_num = Linklet_header_num(filename);

	std::ifstream ifs(filename, std::ios::binary);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	linklet_header l;

	while (ifs.read((char*)& l, sizeof(linklet_header))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		base_id.insert(std::make_pair(l.pos0 / 10, l.raw0));
		base_id.insert(std::make_pair(l.pos1 / 10, l.raw1));
		pl_list.insert(l.pos0 / 10);
		pl_list.insert(l.pos1 / 10);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
}

void read_base(std::string file_in_ECC, int pl, std::vector<netscan::base_track_t> &base, std::set<std::pair<int, int>> &base_id) {
	std::stringstream file_in_base;
	file_in_base << file_in_ECC << "\\Area0\\PL"
		<< std::setw(3) << std::setfill('0') << pl << "\\b"
		<< std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	std::vector<vxx::base_track_t >base_in;
	vxx::BvxxReader br;
	base_in = br.ReadAll(file_in_base.str(), pl, 0);
	std::vector<netscan::base_track_t> base_in2 = base_convert(base_in);
	for (auto itr = base_in2.begin(); itr != base_in2.end(); itr++) {
		if (base_id.count(std::make_pair(itr->pl, itr->rawid)) == 1) {
			base.push_back(*itr);
		}
	}

}
std::vector<netscan::base_track_t> base_convert(std::vector<vxx::base_track_t >&base) {
	std::vector<netscan::base_track_t> ret;
	ret.reserve(base.size());
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		netscan::base_track_t b;
		b.ax = itr->ax;
		b.ay = itr->ay;
		b.dmy = itr->dmy;
		b.isg = itr->isg;
		b.pl = itr->pl;
		b.rawid = itr->rawid;
		b.x = itr->x;
		b.y = itr->y;
		b.z = itr->z;
		b.zone = itr->zone;
		for (int i = 0; i < 2; i++) {
			b.m[i].ax = itr->m[i].ax;
			b.m[i].ay = itr->m[i].ay;
			b.m[i].col = itr->m[i].col;
			b.m[i].isg = itr->m[i].isg;
			b.m[i].ph = itr->m[i].ph;
			b.m[i].pos = itr->m[i].pos;
			b.m[i].rawid = itr->m[i].rawid;
			b.m[i].row = itr->m[i].row;
			b.m[i].z = itr->m[i].z;
			b.m[i].zone = itr->m[i].zone;
		}
		ret.push_back(b);
	}
	return ret;
}

