#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <sstream>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <set>
#include <fstream>

#include <filesystem>

int main(int argc, char**argv) {
	printf("input pl event\n");
	int pl, event_id;
	std::cin >> pl >> event_id;

	std::string file_path = "K:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\image";

	std::stringstream file_path2;
	file_path2 << file_path << "\\ev"
		<< std::setw(5) << std::setfill('0') << event_id << "\\PL"
		<< std::setw(3) << std::setfill('0') << pl;

	bool flg = false;
	for (int face = 1; face <= 2; face++) {
		for (int ix = 0; ix < 5; ix++) {
			for (int iy = 0; iy < 5; iy++) {
				std::stringstream image_folder;
				image_folder << file_path2.str() << "\\face"
					<< std::setw(1) << face << "_"
					<< std::setw(1) << ix << "_"
					<< std::setw(1) << iy;
				flg = true;
				printf("\r check face%d_%d_%d", face, ix, iy);

				for (int num = 0; num < 100; num++) {
					std::stringstream filename;
					filename << image_folder.str() << "\\"
						<< std::setw(4) << std::setfill('0') << num << ".png";
					if (!std::filesystem::exists(filename.str())) {
						flg = false;
					}

				}
				if (!flg) {
					printf(" error\n");
				}
			}
		}
	}
	printf("\n");
	system("pause");
}