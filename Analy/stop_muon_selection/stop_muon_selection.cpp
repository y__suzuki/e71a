#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <filesystem>
#include <set>
class Fiducial_Area {
public:
	int pl;
	double x0, y0, z0, x1, y1, z1;
};

std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename);
void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map);
std::vector<mfile0::M_Chain> divide_penetrate(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&penetrate, int veto_pl);
std::vector<mfile0::M_Chain> divide_edge_out(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&edge_out, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map, double edge_cut, int ex_pl_max);


bool judge_fiducial_area(std::vector<Fiducial_Area>&area, double x, double y);


void output_upbase(std::string filename, std::vector<mfile0::M_Chain>&c);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage: prg file-in-mfile(txt) file-in-ECC fa.txt output-path\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_in_area = argv[3];
	std::string file_out_path = argv[4];

	//corrmap absの読み込み
	std::string file_in_corrabs = file_in_ECC + "\\0\\align\\corrmap-abs.lst";
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrabs, corr_abs);
	//fiducial areaの読み込み
	std::map<int, std::vector<Fiducial_Area>> area = read_fiducial_Area(file_in_area);
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);

	//corrmap absの適用
	trans_mfile_cordinate(corr_abs, area, z_map);
	//stop muon mfile読み込み
	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);
	std::vector<mfile0::M_Chain> all_chians = m.chains;

	std::string file_out_base_all = file_out_path + "\\stop_mu_all.txt";
	output_upbase(file_out_base_all, m.chains);

	//最上流取り出し

	//penetrate edgeout判定


		//penetrate check
	std::vector<mfile0::M_Chain> penetrate;
	//最上流がPL132以上
	all_chians = divide_penetrate(all_chians, penetrate, 132);

	//edgeout check
	std::vector<mfile0::M_Chain> edge_out;
	//最上流から4PL外挿,edgeから5mm以内に入ったらedge out
	all_chians = divide_edge_out(all_chians, edge_out, area, z_map, 0, 4);


	m.chains = all_chians;
	std::string file_out_mfile = file_out_path + "\\stop_mu.all";
	mfile0::write_mfile(file_out_mfile, m);

	std::string file_out_base= file_out_path + "\\stop_mu.txt";

	output_upbase(file_out_base, m.chains);



}

std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename) {

	std::ifstream ifs(filename);
	std::multimap<int, Fiducial_Area> fa_multi;
	std::map<int, std::vector<Fiducial_Area>> ret;
	Fiducial_Area fa;
	while (ifs >> fa.pl >> fa.x0 >> fa.y0 >> fa.z0 >> fa.x1 >> fa.y1 >> fa.z1) {
		fa_multi.insert(std::make_pair(fa.pl, fa));
	}

	int count = 0;
	for (auto itr = fa_multi.begin(); itr != fa_multi.end(); itr++) {
		count = fa_multi.count(itr->first);
		auto range = fa_multi.equal_range(itr->first);
		std::vector<Fiducial_Area> fa_vec;
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			fa_vec.push_back(itr2->second);
		}
		ret.insert(std::make_pair(itr->first, fa_vec));
	}

	return ret;

}

void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map) {
	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		if (area.count(pl) != 0) {
			auto vec = area.find(pl);
			double tmp_x, tmp_y;
			for (auto itr2 = vec->second.begin(); itr2 != vec->second.end(); itr2++) {
				tmp_x = itr2->x0;
				tmp_y = itr2->y0;
				itr2->x0 = tmp_x * itr->position[0] + tmp_y * itr->position[1] + itr->position[4];
				itr2->y0 = tmp_x * itr->position[2] + tmp_y * itr->position[3] + itr->position[5];
				tmp_x = itr2->x1;
				tmp_y = itr2->y1;
				itr2->x1 = tmp_x * itr->position[0] + tmp_y * itr->position[1] + itr->position[4];
				itr2->y1 = tmp_x * itr->position[2] + tmp_y * itr->position[3] + itr->position[5];
			}
		}
		if (z_map.count(pl) != 0) {
			auto z = z_map.find(pl);
			z->second = z->second + itr->dz;
			//printf("PL%03d z:%.1lf\n", pl, z->second);
		}
	}
}
std::vector<mfile0::M_Chain> divide_penetrate(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&penetrate, int veto_pl) {
	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		if (up_pl >= veto_pl) {
			penetrate.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("penetrate = %d\n", penetrate.size());
	return ret;
}
std::vector<mfile0::M_Chain> divide_edge_out(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&edge_out, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map, double edge_cut, int ex_pl_max) {


	double xmin, xmax, ymin, ymax;


	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	double up_z, up_x, up_y, up_ax, up_ay, ex_z, ex_x, ex_y;
	bool flg = false;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		flg = false;
		up_z = z_map.at(up_pl);
		up_x = itr->basetracks.rbegin()->x;
		up_y = itr->basetracks.rbegin()->y;
		up_ax = itr->basetracks.rbegin()->ax;
		up_ay = itr->basetracks.rbegin()->ay;
		for (int ex_pl = 0; ex_pl <= ex_pl_max; ex_pl++) {
			if (z_map.count(up_pl + ex_pl) == 0)continue;
			ex_z = z_map.at(up_pl + ex_pl);
			ex_x = up_x + up_ax * (ex_z - up_z);
			ex_y = up_y + up_ay * (ex_z - up_z);

			if (!judge_fiducial_area(area.at(up_pl + ex_pl), ex_x, ex_y)) {
				flg = true;
			}
		}
		if (flg) {
			edge_out.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("edge out = %d\n", edge_out.size());
	return ret;
}

bool judge_fiducial_area(std::vector<Fiducial_Area>&area, double x, double y) {
	//true でArea内　falseでarea外

	//点(x,y)からx軸性の方向に直線を引き、その直線と多角形の辺が何回交わるか。
	//下から上に交わったときwn+1
	//上から下に交わったときwn-1
	int wn = 0;
	double vt;
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		// 上向きの辺、下向きの辺によって処理が分かれる。
	// 上向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、終点は含まない。(ルール1)
		if (itr->y0 <= y && itr->y1 > y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->y0) / (itr->y1 - itr->y0);
			if (x < itr->x0 + vt * (itr->x1 - itr->x0)) {
				++wn;  //ここが重要。上向きの辺と交差した場合は+1
			}
		}
		// 下向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、始点は含まない。(ルール2)
		else if (itr->y0 > y && itr->y1 <= y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->y0) / (itr->y1 - itr->y0);
			if (x < itr->x0 + vt * (itr->x1 - itr->x0)) {
				--wn;  //ここが重要。下向きの辺と交差した場合は-1
			}
		}
	}
	if (wn >= 1)return true;
	return false;
}

void output_upbase(std::string filename, std::vector<mfile0::M_Chain>&c) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (c.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = c.begin(); itr != c.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)", count, int(c.size()), count*100. / c.size());
			}
			auto base = itr->basetracks.rbegin();
			count++;
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << base->pos / 10 << " "
				<< std::setw(10) << std::setprecision(1) << base->x << " "
				<< std::setw(10) << std::setprecision(1) << base->y << " "
				<< std::setw(10) << std::setprecision(1) << base->z << " "
				<< std::setw(7) << std::setprecision(4) << base->ax << " "
				<< std::setw(7) << std::setprecision(4) << base->ay << std::endl;
		}

	}
}