#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#include <omp.h>

class Track_Difference {
public:
	//pl1-->pl0へtrackを外挿した時のpl0上でのずれ
	int pl0,pl1;
	double ax,ay,x, y, dx, dy;
};
class align_track_inf {
public:
	mfile0::M_Base *base;
	mfile0::M_Chain *chain;
};

std::vector<int> Use_PL(std::vector<mfile0::M_Chain> &c);
int use_thread(double ratio, bool output);
void output_track_difference(std::string filename, std::multimap<int, Track_Difference> &track_diff);
void output_track_difference_bin(std::string filename, std::multimap<int, Track_Difference> &track_diff);

std::map<int, std::vector<align_track_inf>> chain_extract_pointer(std::vector<mfile0::M_Chain>*c);

int main(int argc, char**argv) {
	if (argc !=3) {
		fprintf(stderr, "usage:prg in-mfile out-file-name\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_name = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::vector<int> pl_all = Use_PL(m.chains);
	std::map<int, std::vector<align_track_inf>> track_inf_v = chain_extract_pointer(&(m.chains));

	int t_pl;
	std::multimap<int, Track_Difference> track_diff;
	for (int i = 0; i < pl_all.size(); i++) {
		t_pl = pl_all[i];
		printf("PL%03d start\n", t_pl);
		printf("initialization ...");
		track_diff.clear();
		printf(" fin\n");

		auto res = track_inf_v.find(t_pl);
		if (res == track_inf_v.end())continue;
		std::vector<align_track_inf>chains = res->second;

		int count = 0;
#pragma omp parallel for num_threads(use_thread(0.8,false)) schedule(guided)
		for (int j = 0; j < chains.size(); j++) {
			if (count % 1000 == 0) {
				printf("\r calc difference %8d/%8d(%4.1lf%%)", count, chains.size(), count*100. / chains.size());
			}
#pragma omp atomic
			count++;

			for (auto itr_b = chains[j].chain->basetracks.begin(); itr_b != chains[j].chain->basetracks.end(); itr_b++) {
				if (itr_b->pos / 10 == chains[j].base->pos / 10)continue;
				Track_Difference t_diff;
				t_diff.pl0 = chains[j].base->pos / 10;
				t_diff.pl1 = itr_b->pos / 10;
				t_diff.ax = mfile0::chain_ax(*chains[j].chain);
				t_diff.ay = mfile0::chain_ay(*chains[j].chain);
				t_diff.x = chains[j].base->x;
				t_diff.y = chains[j].base->y;
				t_diff.dx = itr_b->x - chains[j].base->x + t_diff.ax*(chains[j].base->z - itr_b->z);
				t_diff.dy = itr_b->y - chains[j].base->y + t_diff.ay*(chains[j].base->z - itr_b->z);
#pragma omp critical
				track_diff.insert(std::make_pair(t_diff.pl1, t_diff));
			}

		}

		printf("\r calc difference %8d/%8d(%4.1lf%%)\n", count, chains.size(), count*100. / chains.size());

		std::stringstream filename;
		filename << file_out_name << "_" << std::setw(3) << std::setfill('0') << t_pl << ".bin";
		output_track_difference_bin(filename.str(), track_diff);

	}

}
std::vector<int> Use_PL(std::vector<mfile0::M_Chain> &c) {
	std::set<int> pl_set;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			pl_set.insert(itr2->pos / 10);
		}
	}
	std::vector<int> ret;
	for (auto itr = pl_set.begin(); itr != pl_set.end(); itr++) {
		ret.push_back(*itr);
	}
	return ret;
}



std::map<int, std::vector<align_track_inf>> chain_extract_pointer(std::vector<mfile0::M_Chain>*c) {
	std::map<int, std::vector<align_track_inf>> ret;
	std::multimap<int, align_track_inf> t_inf_map;
	int pl;
	align_track_inf t_inf;
	int64_t count = 0, all = c->size();
	for (auto itr = c->begin(); itr != c->end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r chain extract inf %lld/%lld(%4.1lf)", count, all, count*100. / all);
		}
		count++;


		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			pl = itr2->pos / 10;
			t_inf.base = &(*itr2);
			t_inf.chain = &(*itr);
			t_inf_map.insert(std::make_pair(pl, t_inf));
		}
	}
	printf("\r chain extract inf %d/%d(%4.1lf)\n", count, all, count*100. / all);


	count = 0;
	all = t_inf_map.size();
	for (auto itr = t_inf_map.begin(); itr != t_inf_map.end(); itr++) {
		count = t_inf_map.count(itr->first);
		std::vector<align_track_inf> buf;
		buf.reserve(count);
		pl = itr->first;

		auto range = t_inf_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			buf.push_back(res->second);
		}
		ret.insert(std::make_pair(pl, buf));
		itr = std::next(itr, count - 1);
	}
	printf("chain extract inf fin\n");
	return ret;
}

void output_track_difference(std::string filename, std::multimap<int, Track_Difference> &track_diff) {
	std::ofstream ofs(filename);
	int count = 0;
	for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r Write Track difference ... %d/%d (%4.1lf%%)", count, int(track_diff.size()), count*100. / track_diff.size());
		}
		count++;

		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->second.pl0 << " "
			<< std::setw(4) << std::setprecision(0) << itr->second.pl1 << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->second.x << " "
			<< std::setw(10) << std::setprecision(1) << itr->second.y << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.dx << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.dy << std::endl;

	}
	fprintf(stderr, "\r Write Track difference ... %d/%d (%4.1lf%%)\n", count, int(track_diff.size()), count*100. / track_diff.size());

}

void output_track_difference_bin(std::string filename, std::multimap<int, Track_Difference> &track_diff) {
	std::ofstream ofs(filename, std::ios::binary);
	int count = 0;
	for (auto itr = track_diff.begin(); itr != track_diff.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r Write Track difference ... %d/%d (%4.1lf%%)", count, int(track_diff.size()), count*100. / track_diff.size());
		}
		count++;
		ofs.write((char*)&itr->second, sizeof(Track_Difference));
	}
	fprintf(stderr, "\r Write Track difference ... %d/%d (%4.1lf%%)\n", count, int(track_diff.size()), count*100. / track_diff.size());

}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
