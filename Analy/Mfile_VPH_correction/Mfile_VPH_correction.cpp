#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>

#pragma comment(lib, "VxxReader.lib")

#include "VxxReader.h"
class Microtrack_inf {
public:
	int pos, rawid, view, imager, ph, ph2, vph2, area;
	float ax, ay;
};
class Basetrack_inf {
public:
	uint64_t chain_id;
	int pl, zone, rawid;
	double ax, ay, mom;
	Microtrack_inf m[2];
};
void chain_nseg_selection(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, const int nseg);
void reject_Fe_ECC(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel);
void chain_dlat_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double threshold);
void chain_angle_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double thr_ax, double thr_ay);

void d_lat_VPH_output(std::string file_path, mfile1::MFile_minimum &m, std::vector<uint64_t> &all);

std::multimap<int, Basetrack_inf> chain2base(mfile1::MFile_minimum &m, std::vector<uint64_t> &all);
void read_basetrack(std::string file_path, std::multimap<int, Basetrack_inf> &base_map, int pl);
void output_inf(std::string file_path, std::multimap<int, Basetrack_inf> &base);
void output_inf_bin(std::string file_path, std::multimap<int, Basetrack_inf> &base);

mfile1::MFile_minimum mfile_chain_sel(mfile1::MFile_minimum &m, std::vector<uint64_t> &all);


int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mfile ECC_path out-file-path\n");
		exit(1);
	}
	//printf("%d\n", sizeof(Basetrack_inf));
	//input value
	std::string file_in_mfile = argv[1];
	std::string file_path = argv[2];
	std::string file_out_path = argv[3];

	mfile1::MFile_minimum m;
	mfile1::read_mfile(file_in_mfile, m);
	//zmap
	std::map<int, double> zmap;
	for (auto c : m.all_basetracks) {
		for (auto b : c) {
			zmap.insert(std::make_pair(b.pos / 10, b.z));
		}
	}
	std::vector<uint64_t> all, sel;
	for (int i = 0; i < m.chains.size(); i++) {
		all.push_back(i);
	}

	chain_nseg_selection(m, all, sel, 15);
	all = sel;
	sel.clear();

	reject_Fe_ECC(m, all, sel);
	all = sel;
	sel.clear();

	chain_angle_selection(m, all, sel, 4.0, 4.0);;
	all = sel;
	//sel.clear();
	mfile1::MFile_minimum m_sel = mfile_chain_sel(m, sel);
	for (auto c : m.all_basetracks) {
		c.clear();
		c.shrink_to_fit();
	}
	m.all_basetracks.clear();
	m.all_basetracks.shrink_to_fit();
	m.chains.clear();
	m.chains.shrink_to_fit();
	printf("mfile release fin\n");

	all.clear();
	for (int i = 0; i < m_sel.chains.size(); i++) {
		all.push_back(i);
	}

	d_lat_VPH_output(file_out_path, m_sel, all);

	std::multimap<int, Basetrack_inf> base_inf = chain2base(m_sel,all);

	for (auto c : m_sel.all_basetracks) {
		c.clear();
		c.shrink_to_fit();
	}
	m_sel.all_basetracks.clear();
	m_sel.all_basetracks.shrink_to_fit();
	m_sel.chains.clear();
	m_sel.chains.shrink_to_fit();
	printf("mfile release fin\n");

	for (int pl = 0; pl <= 133; pl++) {
		//printf("read　start PL%03d\n", pl);
		read_basetrack(file_path, base_inf, pl);
		//areaごとph2入れる部分も書きたい
	}
	output_inf(file_out_path, base_inf);
	output_inf_bin(file_out_path, base_inf);

}
void chain_nseg_selection(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, const int nseg) {
	for (auto i : all) {
		if (m.chains[i].nseg < nseg)continue;
		sel.push_back(i);
	}
	printf("chain nseg >= %lld: %lld --> %lld (%4.1lf%%)\n", nseg, all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}
void reject_Fe_ECC(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel) {
	for (auto i : all) {
		if (m.chains[i].pos0 / 10 > 15) {
			sel.push_back(i);
		}
		else {
			int flg = 0;
			for (auto b : m.all_basetracks[i]) {
				if (b.pos / 10 == 16)flg++;
				if (b.pos / 10 == 17)flg++;
				if (b.pos / 10 == 18)flg++;
				if (b.pos / 10 == 19)flg++;
				if (b.pos / 10 == 20)flg++;
				if (b.pos / 10 == 21)flg++;
				if (b.pos / 10 == 22)flg++;
				if (b.pos / 10 == 23)flg++;
			}
			if (flg >= 6 || m.chains[i].pos1 / 10 > 23) {
				sel.push_back(i);
			}
		}
	}
	printf("reject_Fe_ECC(PL1 >= 24): %lld --> %lld (%4.1lf%%)\n", all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}
void chain_dlat_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double threshold) {

	for (auto i : all) {
		if (mfile1::angle_diff_dev_lat(m.all_basetracks[i]) > threshold)continue;
		sel.push_back(i);
	}
	printf("chain lateral selection <= %5.4lf : %lld --> %lld (%4.1lf%%)\n", threshold, all.size(), sel.size(), sel.size()*100. / all.size());
	return;

}
void chain_angle_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double thr_ax, double thr_ay) {
	double ax, ay;
	for (auto i : all) {
		ax = mfile1::chain_ax(m.all_basetracks[i]);
		ay = mfile1::chain_ay(m.all_basetracks[i]);
		if (thr_ax - fabs(ax) < 0)continue;
		if (thr_ay - fabs(ay) < 0)continue;
		sel.push_back(i);

	}
	printf("chain average angle selection |ax|<=%3.1lf |ay|<=%3.1lf : %lld --> %lld (%4.1lf%%)\n", thr_ax, thr_ay, all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}
void d_lat_VPH_output(std::string file_path, mfile1::MFile_minimum &m, std::vector<uint64_t> &all) {
	std::stringstream filename;
	filename << file_path << "\\dlat.txt";
	std::ofstream ofs(filename.str());

	double d_lat, vph_ave, ph_ave,ax,ay,mom;
	for (auto num : all) {
		mom = mfile1::angle_diff_mom_iron(m.all_basetracks[num], 5);
		if (mom < 0)continue;

		ax = mfile1::chain_ax(m.all_basetracks[num]);
		ay = mfile1::chain_ay(m.all_basetracks[num]);
		d_lat = mfile1::angle_diff_dev_lat(m.all_basetracks[num]);
		vph_ave = mfile1::chain_vph(m.all_basetracks[num]);
		ph_ave = mfile1::chain_ph(m.all_basetracks[num]);
		ofs << std::right << std::fixed
			<< std::setw(20) << std::setprecision(0) << m.chains[num].chain_id << " "
			<< std::setw(7) << std::setprecision(4) << ax << " "
			<< std::setw(7) << std::setprecision(4) << ay << " "
			<< std::setw(6) << std::setprecision(2) << ph_ave << " "
			<< std::setw(6) << std::setprecision(2) << vph_ave << " "
			<< std::setw(7) << std::setprecision(1) << mom << " "
			<< std::setw(10) << std::setprecision(6) << d_lat << std::endl;

	}
}

mfile1::MFile_minimum mfile_chain_sel(mfile1::MFile_minimum &m, std::vector<uint64_t> &all) {
	printf("N Chain %lld\n", m.info_header.Nchain);
	printf("N Base  %lld\n", m.info_header.Nbasetrack);

	mfile1::MFile_minimum ret;
	ret.header = m.header;
	ret.info_header = m.info_header;
	for (auto num : all) {
		ret.chains.push_back(m.chains[num]);
		ret.all_basetracks.push_back(m.all_basetracks[num]);
	}

	ret.info_header.Nbasetrack = 0;
	ret.info_header.Nchain = 0;
	for (auto b : ret.all_basetracks) {
		ret.info_header.Nchain++;
		ret.info_header.Nbasetrack += b.size();
	}
	printf("N Chain %lld\n", ret.info_header.Nchain);
	printf("N Base  %lld\n", ret.info_header.Nbasetrack);
	return ret;

}
std::multimap<int, Basetrack_inf> chain2base(mfile1::MFile_minimum &m, std::vector<uint64_t> &all) {
	std::set<std::pair<int, int>> base_rawid;
	std::multimap<int, Basetrack_inf> base_inf;
	uint64_t all_c = 0, cnt = 0;
	all_c = all.size();
	double d_lat = 0;
	double mom;
	for (auto num : all) {

		if (cnt % 10000 == 0) {
			fprintf(stderr,"\r base inf fill %lld/%lld(%4.1lf%%)", cnt, all_c, cnt*100. / all_c);
		}
		cnt++;
		//d_lat = mfile1::angle_diff_dev_lat(m.all_basetracks[num]);
		//d_lat = 1 / d_lat;
		mom = mfile1::angle_diff_mom_iron(m.all_basetracks[num], 5);
		if (mom < 0)continue;
		if (mom > 2000)continue;

		for (auto t : m.all_basetracks[num]) {
			if (base_rawid.count(std::make_pair(t.pos, t.rawid)) == 1)continue;
			base_rawid.insert(std::make_pair(t.pos, t.rawid));

			Basetrack_inf b_tmp;
			b_tmp.pl = t.pos/10;
			b_tmp.chain_id = m.chains[num].chain_id;
			b_tmp.rawid = t.rawid;
			b_tmp.mom = mom;
			b_tmp.ax = 0;
			b_tmp.ay = 0;
			b_tmp.zone = 0;
			for (int i = 0; i < 2; i++) {
				b_tmp.m[i].area = 0;
				b_tmp.m[i].ax = 0;
				b_tmp.m[i].ay = 0;
				b_tmp.m[i].imager = 0;
				b_tmp.m[i].ph = 0;
				b_tmp.m[i].ph2 = 0;
				b_tmp.m[i].vph2 = 0;
				b_tmp.m[i].pos = 0;
				b_tmp.m[i].rawid = 0;
				b_tmp.m[i].view = 0;
			}
			base_inf.insert(std::make_pair(b_tmp.pl, b_tmp));
		}
		m.all_basetracks[num].clear();
		m.all_basetracks[num].shrink_to_fit();
	}
	fprintf(stderr, "\r base inf fill %lld/%lld(%4.1lf%%)\n", cnt, all_c, cnt*100. / all_c);

	return base_inf;
}

void read_basetrack(std::string file_path, std::multimap<int, Basetrack_inf> &base_map, int pl) {

	std::vector<vxx::base_track_t> ret;

	int count = 0;
	count = base_map.count(pl);
	if (count == 0)return;

	std::map<int, Basetrack_inf*> rawid;
	auto range = base_map.equal_range(pl);
	for (auto res = range.first; res != range.second; res++) {
		rawid.insert(std::make_pair(res->second.rawid, &(res->second)));
	}

	std::stringstream file_base;
	file_base << file_path << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_base.str(), pl, 0);
	uint32_t ShotID;
	int ViewID = 0, ImagerID = 0, NumberOfImager = 72;

	for (auto b : base) {
		if (rawid.count(b.rawid) == 0)continue;
		auto res = rawid.at(b.rawid);
		res->ax = b.ax;
		res->ay = b.ay;
		res->zone = b.m[0].zone;
		for (int i = 0; i < 2; i++) {
			res->m[i].ax = b.m[i].ax;
			res->m[i].ay = b.m[i].ay;
			res->m[i].ph = b.m[i].ph;
			res->m[i].ph2 = 0;
			res->m[i].vph2 = 0;
			res->m[i].area = 0;
			res->m[i].pos = b.m[i].pos;
			res->m[i].rawid = b.m[i].rawid;
			ShotID = ((uint32_t)(uint16_t)b.m[i].row << 16) | ((uint32_t)(uint16_t)b.m[i].col);
			ViewID = ShotID / NumberOfImager;
			ImagerID = ShotID % NumberOfImager;

			res->m[i].view = ViewID;
			res->m[i].imager = ImagerID;

		}
		ret.push_back(b);
	}
	printf("PL%03d basetrack pickup %d\n", pl, ret.size());
}
void output_inf(std::string file_path, std::multimap<int, Basetrack_inf> &base) {
	if (base.size() == 0)return;

	std::string file_out_name = file_path + "\\vph_corr.txt";
	std::ofstream ofs(file_out_name.c_str());
	//zone 入れ忘れた。入れる。thick/thinの分離
	for (auto b : base) {
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << b.second.pl << " "
			<< std::setw(20) << std::setprecision(0) << b.second.chain_id << " "
			<< std::setw(12) << std::setprecision(0) << b.second.rawid << " "
			<< std::setw(4) << std::setprecision(0) << b.second.zone << " "
			<< std::setw(10) << std::setprecision(3) << b.second.mom << " "
			<< std::setw(7) << std::setprecision(4) << b.second.ax << " "
			<< std::setw(7) << std::setprecision(4) << b.second.ay << " ";
		for (int i = 0; i < 2; i++) {
			ofs << std::setw(4) << std::setprecision(0) << b.second.m[i].pos << " "
				<< std::setw(12) << std::setprecision(0) << b.second.m[i].rawid << " "
				<< std::setw(7) << std::setprecision(4) << b.second.m[i].ax << " "
				<< std::setw(7) << std::setprecision(4) << b.second.m[i].ay << " "
				<< std::setw(5) << std::setprecision(0) << b.second.m[i].view << " "
				<< std::setw(3) << std::setprecision(0) << b.second.m[i].imager << " "
				<< std::setw(7) << std::setprecision(0) << b.second.m[i].ph << " "
				<< std::setw(4) << std::setprecision(0) << b.second.m[i].ph2 << " "
				<< std::setw(7) << std::setprecision(0) << b.second.m[i].vph2 << " "
				<< std::setw(7) << std::setprecision(0) << b.second.m[i].area << " ";
		}
		ofs << std::endl;
	}
}
void output_inf_bin(std::string file_path, std::multimap<int, Basetrack_inf> &base) {
	if (base.size() == 0)return;

	std::string file_out_name = file_path + "\\vph_corr_bin.dat";
	std::ofstream ofs(file_out_name, std::ios::binary);

	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", file_out_name.c_str());
		exit(1);
	}
	if (base.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", file_out_name.c_str());
	}
	int64_t count = 0;
	int64_t max = base.size();

	for (auto b : base) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& b.second, sizeof(Basetrack_inf));

	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();
}
