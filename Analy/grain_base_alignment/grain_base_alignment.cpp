#include <VxxReader.h>
#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class EmulsionTrack {
public:
//	2   1.0661 - 0.2955    66157.4   162655.3      194.7      194.7      255.2
	int id;
	double ax, ay, x, y, z, z0, z1;
};
class BaseTrack {
public:
	int rawid, ph, vph;
	double ax, ay, x, y, z;
};
class AffineParam {
public:
	double rotation, shrink, shift[2];
};
std::vector<EmulsionTrack> read_grain_track(std::string filename);
void track_area(std::vector<EmulsionTrack>&track, double &xmin, double &xmax, double &ymin, double &ymax);
std::vector<BaseTrack> read_base(std::string filename, int pl, double xmin, double xmax, double ymin, double ymax, double mergin, int face);
void output_base(std::string filename, std::vector<BaseTrack> &base, int face);
std::pair<double, double> Calc_alignment(std::vector<EmulsionTrack>&track, std::vector<BaseTrack>&base);
void Calc_dc(std::vector<EmulsionTrack>&track, std::vector<BaseTrack>&base, std::pair<double, double>diff, AffineParam&param, AffineParam&param_dc);
AffineParam Calc_param(std::vector<std::pair<EmulsionTrack, BaseTrack>> &pair_v);
void output_param(std::string filename, AffineParam&param, AffineParam &param_dc);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg filn-in-grain-track file-in-base pl face file-out-param\n");
		exit(1);
	}
	std::string file_in_grain = argv[1];
	std::string file_in_base = argv[2];
	int pl = std::stoi(argv[3]);
	int face= std::stoi(argv[4]);
	std::string file_out_param = argv[5];

	std::vector<EmulsionTrack> track = read_grain_track(file_in_grain);
	double xmin, xmax, ymin, ymax;
	track_area(track, xmin, xmax, ymin, ymax);
	//xmin-xmaxから余分に+-1mm読む
	double mergin = 1000;
	std::vector< BaseTrack> base=read_base(file_in_base, pl, xmin,  xmax,  ymin, ymax, mergin,face);
	std::pair<double,double> diff=Calc_alignment(track, base);

	AffineParam param, param_dc;
	Calc_dc(track, base,diff, param, param_dc);
	
	output_param(file_out_param, param, param_dc);

	//grain_trans(grain_all, base);

	//output_grain(file_out_grain, grain_all);
	//output_base(file_out_base, base,face);

}
std::vector<EmulsionTrack> read_grain_track(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<EmulsionTrack> ret;
	EmulsionTrack tr;
	int count = 0;
	while (ifs >> tr.id>>tr.ax>>tr.ay>>tr.x>>tr.y>>tr.z>>tr.z0>>tr.z1){
		if (count % 100000 == 0) {
			printf("\r track read %d", count);
		}
		count++;
		ret.push_back(tr);
	}
	printf("\r track read fin %d\n", ret.size());
	return ret;

}
void track_area(std::vector<EmulsionTrack>&track, double &xmin, double &xmax, double &ymin, double &ymax) {

	for (auto itr = track.begin(); itr != track.end(); itr++) {
		if (itr == track.begin()) {
			xmin = itr->x;
			xmax = itr->x;
			ymin = itr->y;
			ymax = itr->y;
		}
		xmin = std::min(xmin, itr->x);
		xmax = std::max(xmax, itr->x);
		ymin = std::min(ymin, itr->y);
		ymax = std::max(ymax, itr->y);
	}
	
}

std::vector<BaseTrack> read_base(std::string filename, int pl,double xmin, double xmax, double ymin, double ymax,double mergin,int face) {

	std::vector<vxx::base_track_t> base;
	vxx::BvxxReader br;
	std::vector<vxx::CutArea> area;
	area.push_back(vxx::CutArea(xmin - mergin-1000, xmax + mergin+1000, ymin - mergin - 1000, ymax + mergin + 1000,-2,2,-2,2));
	//std::array<int, 2> index = { rawid, rawid + 1 };//1234<=rawid<=5678であるようなものだけを読む。
	base = br.ReadAll(filename, pl, 0, vxx::opt::a = area);

	std::vector<BaseTrack> ret;
	double angle;
	int ph;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		BaseTrack base_tmp;
		base_tmp.rawid = itr->rawid;
		base_tmp.ax = itr->ax;
		base_tmp.ay = itr->ay;
		ph = itr->m[0].ph + itr->m[1].ph;
		base_tmp.ph = ph / 10000;
		base_tmp.vph = ph % 10000;
		if (face == 1) {
			base_tmp.x = itr->x;
			base_tmp.y = itr->y;
			base_tmp.z = 0;
		}
		else if (face == 2) {
			base_tmp.x = itr->x + itr-> ax*(itr->m[1].z - itr->m[0].z);
			base_tmp.y = itr->y + itr->ay*(itr->m[1].z - itr->m[0].z);
			base_tmp.z = itr->m[1].z - itr->m[0].z;
		}
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (base_tmp.x < xmin - mergin)continue;
		if (base_tmp.x > xmax + mergin)continue;
		if (base_tmp.y < ymin - mergin)continue;
		if (base_tmp.y > ymax + mergin)continue;
		if (angle < 0.5)ret.push_back(base_tmp);
		else if (angle < 1.0) {
			if (base_tmp.vph < 20)continue;
			ret.push_back(base_tmp);
		}
		else {
			if (base_tmp.vph < 10)continue;
			ret.push_back(base_tmp);
		}
	}

	return ret;

}

std::pair<double,double> Calc_alignment(std::vector<EmulsionTrack>&track, std::vector<BaseTrack>&base) {

	//std::ofstream ofs("track_pair.txt");
	std::vector<std::pair<double, double>> diff;
	double dx_min, dy_min, dx, dy;;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		for (auto itr2 = base.begin(); itr2 != base.end(); itr2++) {
			if (fabs(itr->ax - itr2->ax) > 0.2)continue;
			if (fabs(itr->ay - itr2->ay) > 0.2)continue;
			dx = itr2->x - itr->x;
			dy = itr2->y - itr->y;

			std::pair<double, double> diff_pair = std::make_pair(dx, dy);
			if (diff.size() == 0) {
				dx_min = dx;
				dy_min = dy;
			}
			dx_min = std::min(dx, dx_min);
			dy_min = std::min(dy, dy_min);
			diff.push_back(diff_pair);

			//ofs << std::right << std::fixed
			//	<< std::setw(8) << std::setprecision(4) << itr->ax - itr2->ax << " "
			//	<< std::setw(8) << std::setprecision(4) << itr->ay - itr2->ay << " "
			//	<< std::setw(8) << std::setprecision(1) << itr->x - itr2->x << " "
			//	<< std::setw(8) << std::setprecision(1) << itr->y - itr2->y << std::endl;
		}
	}

	//2次元histの作成
	double bin_size = 20;
	std::map < std::pair<int, int>, int> two_d_hsit;
	std::pair<int, int>id;
	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		id.first = (itr->first - dx_min) / bin_size;
		id.second = (itr->second - dy_min) / bin_size;
		auto res = two_d_hsit.insert(std::make_pair(id, 1));
		if (!res.second) {
			res.first->second++;
		}
	}

	//peak 探索
	int peak_trk_num = 0;
	for (auto itr = two_d_hsit.begin(); itr != two_d_hsit.end(); itr++) {
		if (peak_trk_num < itr->second) {
			peak_trk_num = itr->second;
			id = itr->first;
		}
	}
	double dx_peak, dy_peak;
	dx_peak = (id.first + 0.5)*bin_size + dx_min;
	dy_peak = (id.second + 0.5)*bin_size + dy_min;
	printf("peak(%.1lf,%.1lf)\n", dx_peak,dy_peak);
	printf("peak height = %d\n", peak_trk_num);
	double dx_sum = 0, dy_sum = 0;
	int count = 0;
	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		if (fabs(itr->first - dx_peak) > 60)continue;
		if (fabs(itr->second - dy_peak) > 60)continue;
		dx_sum += itr->first;
		dy_sum += itr->second;
		count++;
	}
	dx_peak = dx_sum / count;
	dy_peak = dy_sum / count;
	printf("peak refine(%.1lf,%.1lf)\n", dx_peak, dy_peak);
	std::pair<double, double> ret;
	ret.first = dx_peak;
	ret.second = dy_peak;
	return ret;
}

void Calc_dc(std::vector<EmulsionTrack>&track, std::vector<BaseTrack>&base, std::pair<double, double>diff, AffineParam&param, AffineParam&param_dc) {

	std::vector<EmulsionTrack>track_shift;
	for (auto tr:track){
		tr.x += diff.first;
		tr.y += diff.second;
		track_shift.push_back(tr);
	}

	std::ofstream ofs;
	//std::ofstream ofs("align_track_pair.txt");
	double dax, day, dx, dy,angle;
	std::vector<std::pair<EmulsionTrack, BaseTrack>> pair_v;
	std::map<int, bool> base_flg, track_flg;
	for (auto itr = track_shift.begin(); itr != track_shift.end(); itr++) {
		for (auto itr2 = base.begin(); itr2 != base.end(); itr2++) {
			dax = itr2->ax - itr->ax;
			day = itr2->ay - itr->ay;
			dx = itr2->x - itr->x;
			dy = itr2->y - itr->y;
			angle = sqrt(itr2->ax*itr2->ax + itr2->ay*itr2->ay);
			if (angle < 0.1) {
				if (fabs(dax) > 0.1)continue;
				if (fabs(day) > 0.1)continue;
			}
			else {
				//lateral角度差
				if (fabs(dax*itr2->ay - day * itr2->ax) > 0.15*angle)continue;
				//
				if (fabs(dax*itr2->ax + day * itr2->ay) > angle*(0.15+0.2*angle))continue;
			}
			if (fabs(dx) > 50)continue;
			if (fabs(dy) > 50)continue;
			pair_v.push_back(std::make_pair(*itr, *itr2));
			auto res = base_flg.insert(std::make_pair(itr2->rawid, true));
			if (!res.second)res.first->second = false;
			res = track_flg.insert(std::make_pair(itr->id, true));
			if (!res.second)res.first->second = false;
			//ofs << std::right << std::fixed
			//	<< std::setw(10) << std::setprecision(0) << itr->id << " "
			//	<< std::setw(10) << std::setprecision(4) << itr->ax << " "
			//	<< std::setw(10) << std::setprecision(4) << itr->ay << " "
			//	<< std::setw(10) << std::setprecision(1) << itr->x << " "
			//	<< std::setw(10) << std::setprecision(1) << itr->y << " "
			//	<< std::setw(10) << std::setprecision(0) << itr2->rawid << " "
			//	<< std::setw(10) << std::setprecision(4) << itr2->ax << " "
			//	<< std::setw(10) << std::setprecision(4) << itr2->ay << " "
			//	<< std::setw(10) << std::setprecision(1) << itr2->x << " "
			//	<< std::setw(10) << std::setprecision(1) << itr2->y << std::endl;
			//ofs << std::right << std::fixed
			//	<< std::setw(10) << std::setprecision(1) << itr->x << " "
			//	<< std::setw(10) << std::setprecision(1) << itr->y << " 0 "
			//	<< std::setw(10) << std::setprecision(1) << itr->x - 70 * itr->ax << " "
			//	<< std::setw(10) << std::setprecision(1) << itr->y - 70 * itr->ay << " -70 "
			//	<< std::setw(10) << std::setprecision(1) << itr2->x << " "
			//	<< std::setw(10) << std::setprecision(1) << itr2->y << " 0 "
			//	<< std::setw(10) << std::setprecision(1) << itr2->x + 70 * itr2->ax << " "
			//	<< std::setw(10) << std::setprecision(1) << itr2->y + 70 * itr2->ay << " 70 " << std::endl;

		}
	}
	//ofs.close();

	//multiは除外
	std::vector<std::pair<EmulsionTrack, BaseTrack>> pair_v_single;
	for (auto itr = pair_v.begin(); itr != pair_v.end(); itr++) {
		if (!track_flg.at(itr->first.id))continue;
		if (!base_flg.at(itr->second.rawid))continue;
		pair_v_single.push_back(*itr);
	}
	//これは系同士の座標の変換パラメータ
	param = Calc_param(pair_v_single);
	double tmp_x, tmp_y;
	for (auto itr2 = pair_v_single.begin(); itr2 != pair_v_single.end(); itr2++) {
		auto itr = &(itr2->first);
		tmp_x = itr->x;
		tmp_y = itr->y;
		//itr->x = tmp_x * cos(param.rotation) - tmp_y * sin(param.rotation);
		//itr->y = tmp_x * sin(param.rotation) + tmp_y * cos(param.rotation);
		itr->x = param.shrink*(tmp_x * cos(param.rotation) - tmp_y * sin(param.rotation)) + param.shift[0];
		itr->y = param.shrink*(tmp_x * sin(param.rotation) + tmp_y * cos(param.rotation)) + param.shift[1];
		tmp_x = itr->ax;
		tmp_y = itr->ay;
		itr->ax = tmp_x * cos(param.rotation) - tmp_y * sin(param.rotation);
		itr->ay = tmp_x * sin(param.rotation) + tmp_y * cos(param.rotation);
	}
	
	param.shift[0] = param.shrink*(diff.first* cos(param.rotation) - diff.second * sin(param.rotation)) + param.shift[0];
	param.shift[1] = param.shrink*(diff.first* sin(param.rotation) + diff.second * cos(param.rotation)) + param.shift[1];
	
	//ofs.open("track_pair2.txt");
	//for (auto itr3 = pair_v_single.begin(); itr3 != pair_v_single.end(); itr3++) {
	//	auto itr2 = &(itr3->second);
	//	auto itr = &(itr3->first);
	//	ofs << std::right << std::fixed
	//		<< std::setw(10) << std::setprecision(0) << itr->id << " "
	//		<< std::setw(10) << std::setprecision(4) << itr->ax << " "
	//		<< std::setw(10) << std::setprecision(4) << itr->ay << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->x << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y << " "
	//		<< std::setw(10) << std::setprecision(0) << itr2->rawid << " "
	//		<< std::setw(10) << std::setprecision(4) << itr2->ax << " "
	//		<< std::setw(10) << std::setprecision(4) << itr2->ay << " "
	//		<< std::setw(10) << std::setprecision(1) << itr2->x << " "
	//		<< std::setw(10) << std::setprecision(1) << itr2->y << std::endl;
	//}
	//ofs.close();

	param_dc.rotation = 0;
	param_dc.shrink = 1;
	param_dc.shift[0] = 0;
	param_dc.shift[1] = 0;
	double xa, xb, ya, yb, xaxb, yayb, xa2,ya2,num;
	xa = 0;
	xa2 = 0;
	xb = 0;
	ya = 0;
	ya2 = 0;
	yb = 0;
	xaxb = 0;
	yayb = 0;
	num = 0;

	for (auto itr = pair_v_single.begin(); itr != pair_v_single.end(); itr++) {
		xa += itr->first.ax;
		xa2 += itr->first.ax * itr->first.ax;
		ya += itr->first.ay;
		ya2 += itr->first.ay * itr->first.ay;
		xb += itr->second.ax;
		yb += itr->second.ay;

		xaxb += itr->first.ax*itr->second.ax;
		yayb += itr->first.ay*itr->second.ay;
		num++;
	}
	double denominator;
	denominator = xa2 + ya2 - (xa*xa + ya * ya) / num;
	if (denominator != 0) {
		param_dc.shrink = (xaxb + yayb) - (xa*xb + ya * yb) / num;
		param_dc.shrink /= denominator;
		param_dc.shift[0] = (xb - param_dc.shrink*xa) / num;
		param_dc.shift[1] = (yb - param_dc.shrink*ya) / num;
	}
	//printf("%g %g %g\n", param_dc.shrink, param_dc.shift[0], param_dc.shift[1]);












	////変換
	//for (auto itr2 = pair_v_single.begin(); itr2 != pair_v_single.end(); itr2++) {
	//	auto itr = &(itr2->first);
	//	tmp_x = itr->ax;
	//	tmp_y = itr->ay;
	//	itr->ax = param_dc.shrink*tmp_x + param_dc.shift[0];
	//	itr->ay = param_dc.shrink*tmp_y + param_dc.shift[1];
	//}
	//ofs.open("align_track_pair3.txt");
	//for (auto itr3 = pair_v_single.begin(); itr3 != pair_v_single.end(); itr3++) {
	//	auto itr2 = &(itr3->second);
	//	auto itr = &(itr3->first);

	//	ofs << std::right << std::fixed
	//		<< std::setw(10) << std::setprecision(1) << itr->x << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y << " 0 "
	//		<< std::setw(10) << std::setprecision(1) << itr->x - 70 * itr->ax << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y - 70 * itr->ay << " -70 "
	//		<< std::setw(10) << std::setprecision(1) << itr2->x << " "
	//		<< std::setw(10) << std::setprecision(1) << itr2->y << " 0 "
	//		<< std::setw(10) << std::setprecision(1) << itr2->x + 70 * itr2->ax << " "
	//		<< std::setw(10) << std::setprecision(1) << itr2->y + 70 * itr2->ay << " 70 " << std::endl;
	//}
	//ofs.close();


	//ofs.open("track_pair3.txt");
	//for (auto itr3 = pair_v_single.begin(); itr3 != pair_v_single.end(); itr3++) {
	//	auto itr2 = &(itr3->second);
	//	auto itr = &(itr3->first);
	//	ofs << std::right << std::fixed
	//		<< std::setw(10) << std::setprecision(0) << itr->id << " "
	//		<< std::setw(10) << std::setprecision(4) << itr->ax << " "
	//		<< std::setw(10) << std::setprecision(4) << itr->ay << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->x << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y << " "
	//		<< std::setw(10) << std::setprecision(0) << itr2->rawid << " "
	//		<< std::setw(10) << std::setprecision(4) << itr2->ax << " "
	//		<< std::setw(10) << std::setprecision(4) << itr2->ay << " "
	//		<< std::setw(10) << std::setprecision(1) << itr2->x << " "
	//		<< std::setw(10) << std::setprecision(1) << itr2->y << std::endl;
	//}
	//ofs.close();
	//std::ofstream ofs("align_track_pair.txt");
	//for (auto itr = pair_v_single.begin(); itr != pair_v_single.end(); itr++) {
	//	ofs << std::right << std::fixed
	//	<< std::setw(10) << std::setprecision(1) << itr->first.x << " "
	//	<< std::setw(10) << std::setprecision(1) << itr->first.y << " 0 "
	//	<< std::setw(10) << std::setprecision(1) << itr->first.x - 70 * itr->first.ax << " "
	//	<< std::setw(10) << std::setprecision(1) << itr->first.y - 70 * itr->first.ay << " -70 "
	//	<< std::setw(10) << std::setprecision(1) << itr->second.x << " "
	//	<< std::setw(10) << std::setprecision(1) << itr->second.y << " 0 "
	//	<< std::setw(10) << std::setprecision(1) << itr->second.x + 70 * itr->second.ax << " "
	//	<< std::setw(10) << std::setprecision(1) << itr->second.y + 70 * itr->second.ay << " 70 " << std::endl;
	//}
	//ofs.close();





	//ofs.open("grain_track_shift.txt");
	//for (auto itr = track_shift.begin(); itr != track_shift.end(); itr++) {
	//	ofs << std::right << std::fixed
	//		<< std::setw(10) << std::setprecision(1) << itr->x + itr->ax*(itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y + itr->ay*(itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->z + (itr->z0 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->x + itr->ax*(itr->z1 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y + itr->ay*(itr->z1 - itr->z) << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->z + (itr->z1 - itr->z) << std::endl;
	//}


}
AffineParam Calc_param(std::vector<std::pair<EmulsionTrack, BaseTrack>> &pair_v) {
	AffineParam ret;
	ret.rotation = 0;
	ret.shrink = 1;
	ret.shift[0] = 0;
	ret.shift[1] = 0;
	//emulsion xa,ya
	//base xb,yb
	double  xa, ya, xa2, ya2;
	double  xb, yb;
	double xaxb, yayb, xayb, xbya,num;
	num = 0;
	xa = 0;
	ya = 0;
	xa2 = 0;
	ya2 = 0;
	xb = 0;
	yb = 0;
	xaxb = 0;
	yayb = 0;
	xayb = 0;
	xbya = 0;
	for (auto &p : pair_v) {
		num += 1;
		xa += p.first.x;
		ya += p.first.y;
		xa2 += p.first.x*p.first.x;
		ya2 += p.first.y*p.first.y;
		xb += p.second.x;
		yb += p.second.y;
		xaxb += p.first.x*p.second.x;
		yayb += p.first.y*p.second.y;
		xayb += p.first.x*p.second.y;
		xbya += p.first.y*p.second.x;
	}
	if (num < 1) {

		printf("error\n");
		return ret;
	}
	double denominator = xa2 + ya2 - (xa*xa + ya * ya) / num;
	if (denominator == 0) {
		printf("error\n");
		return ret;
	}

	double a, b, p, q;
	a = xaxb + yayb - (xa*xb + ya * yb) / num;
	b = -1 * xbya + xayb - (-1 * ya*xb + xa * yb) / num;
	a /= denominator;
	b /= denominator;
	p = xb / num - (a*xa / num - b * ya / num);
	q = yb / num - (b*xa / num + a * ya / num);
	//printf("%g %g %g %g\n", a, b, p, q);

	ret.rotation = atan(b / a);
	ret.shrink = sqrt(a*a + b * b);
	ret.shift[0] = p;
	ret.shift[1] = q;
	//printf("%g %g %g %g\n", ret.rotation, ret.shrink, ret.shift[0], ret.shift[1]);

	return ret;
}

void output_base(std::string filename, std::vector<BaseTrack> &base,int face) {
	std::ofstream ofs(filename);

	double pos[2][3];
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		pos[0][0] = itr->x;
		pos[0][1] = itr->y;
		pos[0][2] = itr->z;
		if (face == 1) {
			pos[1][0] = itr->x + itr->ax*(70 - itr->z);
			pos[1][1] = itr->y + itr->ay*(70 - itr->z);
			pos[1][2] = itr->z + (70 - itr->z);
		}
		else if(face==2){
			pos[1][0] = itr->x + itr->ax*((itr->z+ 70) - itr->z);
			pos[1][1] = itr->y + itr->ay*((itr->z + 70) - itr->z);
			pos[1][2] = itr->z + ((itr->z + 70) - itr->z);
		}
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(1) << pos[0][0] << " "
			<< std::setw(10) << std::setprecision(1) << pos[0][1] << " "
			<< std::setw(10) << std::setprecision(1) << pos[0][2] << " "
			<< std::setw(10) << std::setprecision(1) << pos[1][0] << " "
			<< std::setw(10) << std::setprecision(1) << pos[1][1] << " "
			<< std::setw(10) << std::setprecision(1) << pos[1][2] << " "
			<< std::setw(10) << std::setprecision(0) << itr->ph << " "
			<< std::setw(10) << std::setprecision(0) << itr->vph << std::endl;
	}
}
void output_param(std::string filename, AffineParam&param, AffineParam &param_dc) {

	std::ofstream ofs(filename);


	ofs << std::right << std::fixed
		<< std::setw(10) << std::setprecision(5) << param.rotation << " "
		<< std::setw(10) << std::setprecision(5) << param.shrink << " "
		<< std::setw(10) << std::setprecision(2) << param.shift[0] << " "
		<< std::setw(10) << std::setprecision(2) << param.shift[1] << std::endl;
	ofs << std::right << std::fixed
		<< std::setw(10) << std::setprecision(5) << param_dc.rotation << " "
		<< std::setw(10) << std::setprecision(5) << param_dc.shrink << " "
		<< std::setw(10) << std::setprecision(5) << param_dc.shift[0] << " "
		<< std::setw(10) << std::setprecision(5) << param_dc.shift[1] << std::endl;
}
