#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf);
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, bool single_flg);
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, bool single_flg, int ECC_flg);
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, bool single_flg, int ECC_flg, int PM_flg);


int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "prg:sharing-file\n");
		exit(1);
	}

	std::string file_in_sf = argv[1];

	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_extension(file_in_sf);
	int all_prediction = Count_track_timing(sf);
	printf("all prediction:%d\n", all_prediction);
	printf("\t single:%d\n", Count_track_timing(sf, true));

	printf("\t\t ECC stop:%d\n", Count_track_timing(sf, true, 0));
	printf("\t\t\t PM stop:%d\n", Count_track_timing(sf, true, 0, 0));
	printf("\t\t\t PM penetrate:%d\n", Count_track_timing(sf, true, 0, 1));
	printf("\t\t\t PM interaction:%d\n", Count_track_timing(sf, true, 0, 2));

	printf("\t\t ECC penetrate:%d\n", Count_track_timing(sf, true, 1));
	printf("\t\t\t PM stop:%d\n", Count_track_timing(sf, true, 1, 0));
	printf("\t\t\t PM penetrate:%d\n", Count_track_timing(sf, true, 1, 1));
	printf("\t\t\t PM interaction:%d\n", Count_track_timing(sf, true, 1, 2));

	printf("\t\t ECC edge out:%d\n", Count_track_timing(sf, true, 2));
	printf("\t\t\t PM stop:%d\n", Count_track_timing(sf, true, 2, 0));
	printf("\t\t\t PM penetrate:%d\n", Count_track_timing(sf, true, 2, 1));
	printf("\t\t\t PM interaction:%d\n", Count_track_timing(sf, true, 2, 2));

	printf("\t multi :%d\n", Count_track_timing(sf, false));
	printf("\t\t ECC stop exist:%d\n", Count_track_timing(sf, false, 0));
	printf("\t\t\t PM stop:%d\n", Count_track_timing(sf, false, 0, 0));
	printf("\t\t\t PM penetrate:%d\n", Count_track_timing(sf, false, 0, 1));
	printf("\t\t\t PM interaction:%d\n", Count_track_timing(sf, false, 0, 2));

	printf("\t\t ECC stop not exist:%d\n", Count_track_timing(sf, false, 1));
	printf("\t\t\t PM stop:%d\n", Count_track_timing(sf, false, 1, 0));
	printf("\t\t\t PM penetrate:%d\n", Count_track_timing(sf, false, 1, 1));
	printf("\t\t\t PM interaction:%d\n", Count_track_timing(sf, false, 1, 2));

	//printf("\t\t single:%d\n", Count_track_timing(sf, 0, true));
	//printf("\t\t\t ECC stop:%d\n", Count_track_timing(sf, 0, true, 0));
	//printf("\t\t\t ECC penetrate:%d\n", Count_track_timing(sf, 0, true, 1));
	//printf("\t\t\t ECC edge out:%d\n", Count_track_timing(sf, 0, true, 2));
	//printf("\t\t multi:%d\n", Count_track_timing(sf, 0, false));
	//printf("\t\t\t ECC stop exist:%d\n", Count_track_timing(sf, 0, false, 0));
	//printf("\t\t\t ECC stop not exist:%d\n", Count_track_timing(sf, 0, false, 1));
	///////////////////////////////////////////////
	//printf("\t PM penetrate:%d\n", Count_track_timing(sf, 1));
	//printf("\t\t single:%d\n", Count_track_timing(sf, 1, true));
	//printf("\t\t\t ECC stop:%d\n", Count_track_timing(sf, 1, true, 0));
	//printf("\t\t\t ECC penetrate:%d\n", Count_track_timing(sf, 1, true, 1));
	//printf("\t\t\t ECC edge out:%d\n", Count_track_timing(sf, 1, true, 2));
	//printf("\t\t multi:%d\n", Count_track_timing(sf, 1, false));
	//printf("\t\t\t ECC stop exist:%d\n", Count_track_timing(sf, 1, false, 0));
	//printf("\t\t\t ECC stop not exist:%d\n", Count_track_timing(sf, 1, false, 1));
	///////////////////////////////////////////////

	//printf("\t PM interaction:%d\n", Count_track_timing(sf, 2));
	//printf("\t\t single:%d\n", Count_track_timing(sf, 2, true));
	//printf("\t\t\t ECC stop:%d\n", Count_track_timing(sf, 2, true, 0));
	//printf("\t\t\t ECC penetrate:%d\n", Count_track_timing(sf, 2, true, 1));
	//printf("\t\t\t ECC edge out:%d\n", Count_track_timing(sf, 2, true, 2));
	//printf("\t\t multi:%d\n", Count_track_timing(sf, 2, false));
	//printf("\t\t\t ECC stop exist:%d\n", Count_track_timing(sf, 2, false, 0));
	//printf("\t\t\t ECC stop not exist:%d\n", Count_track_timing(sf, 2, false, 1));
	///////////////////////////////////////////////

}
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf) {
	std::set<std::pair<int, int>> prediction_num;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		prediction_num.insert(std::make_pair(itr->unix_time, itr->tracker_track_id));
	}
	return prediction_num.size();
}
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, bool single_flg) {
	std::multimap<std::pair<int, int>, Sharing_file::Sharing_file>sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(std::make_pair(itr->unix_time, itr->tracker_track_id), *itr));
	}

	std::set<std::pair<int, int>> prediction_num;
	for (auto itr = sf_map.begin(); itr != sf_map.end(); itr = std::next(itr, sf_map.count(itr->first))) {
		std::vector<Sharing_file::Sharing_file> sf_v;
		auto range = sf_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			sf_v.push_back(res->second);
		}
		if (single_flg) {
			if (sf_v.size() != 1)continue;
			prediction_num.insert(itr->first);
		}
		else {
			if (sf_v.size() == 1)continue;
			prediction_num.insert(itr->first);
		}

	}
	return prediction_num.size();
}
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, bool single_flg, int ECC_flg) {
	std::multimap<std::pair<int, int>, Sharing_file::Sharing_file>sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(std::make_pair(itr->unix_time, itr->tracker_track_id), *itr));
	}

	std::set<std::pair<int, int>> prediction_num;
	for (auto itr = sf_map.begin(); itr != sf_map.end(); itr = std::next(itr, sf_map.count(itr->first))) {
		std::vector<Sharing_file::Sharing_file> sf_v;
		auto range = sf_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			sf_v.push_back(res->second);
		}
		if (single_flg) {
			if (sf_v.size() != 1)continue;
			if (sf_v[0].ecc_track_type != ECC_flg)continue;
			prediction_num.insert(itr->first);
		}
		else {
			if (sf_v.size() == 1)continue;
			bool stop_flg = false;
			for (auto itr2 = sf_v.begin(); itr2 != sf_v.end(); itr2++) {
				if (itr2->ecc_track_type == 0)stop_flg = true;
			}
			if (ECC_flg == 0 && stop_flg) {
				prediction_num.insert(itr->first);
			}
			else if (ECC_flg == 1 && !stop_flg) {
				prediction_num.insert(itr->first);
			}
		}

	}
	return prediction_num.size();
}

int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, bool single_flg, int ECC_flg,int PM_flg) {
	std::multimap<std::pair<int, int>, Sharing_file::Sharing_file>sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(std::make_pair(itr->unix_time, itr->tracker_track_id), *itr));
	}

	std::set<std::pair<int, int>> prediction_num;
	for (auto itr = sf_map.begin(); itr != sf_map.end(); itr = std::next(itr, sf_map.count(itr->first))) {
		std::vector<Sharing_file::Sharing_file> sf_v;
		auto range = sf_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			sf_v.push_back(res->second);
		}
		if (sf_v[0].track_type != PM_flg)continue;

		if (single_flg) {
			if (sf_v.size() != 1)continue;
			if (sf_v[0].ecc_track_type != ECC_flg)continue;
			prediction_num.insert(itr->first);
		}
		else {
			if (sf_v.size() == 1)continue;
			bool stop_flg = false;
			for (auto itr2 = sf_v.begin(); itr2 != sf_v.end(); itr2++) {
				if (itr2->ecc_track_type == 0)stop_flg = true;
			}
			if (ECC_flg == 0 && stop_flg) {
				prediction_num.insert(itr->first);
			}
			else if (ECC_flg == 1 && !stop_flg) {
				prediction_num.insert(itr->first);
			}
		}

	}
	return prediction_num.size();
}

