#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <omp.h>
#include <set>
struct Efficiency {
	int pl, ph, bin, zfilter, all, hit;
	double eff, eff_err, ang_min, ang_max;
};

void Basetrack_matching(std::vector<netscan::base_track_t> base_pred, std::vector<netscan::base_track_t>  &base_all, std::set<int>& base_rawid);
std::vector<std::vector<netscan::base_track_t>> Base_Area_Cut_Hash(std::vector<netscan::base_track_t> base);
std::vector <netscan::base_track_t> base_alignment(std::vector <netscan::base_track_t>&base0, std::vector <netscan::base_track_t> &base1);
void fit_line(std::vector<double>x, std::vector<double>y, double &slope, double &intercept);
void fit_line_intercept0(std::vector<double>x, std::vector<double>y, double &slope);
std::vector<netscan::base_track_t> id_search(std::vector<netscan::base_track_t> base, std::set<int> base_rawid);

int main(int argc, char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-bvxx(prediction) in-bvxx pl out-bvxx\n");
		exit(1);
	}
	std::string file_in_base_pred = argv[1];
	std::string file_in_base = argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_out_bvxx = argv[4];

	std::vector<netscan::base_track_t> base_pred;
	std::vector<netscan::base_track_t> base;

	netscan::read_basetrack_extension(file_in_base_pred, base_pred, pl, 0);
	netscan::read_basetrack_extension(file_in_base, base, pl, 0);
	std::vector<std::vector<netscan::base_track_t>>base_pred_hash = Base_Area_Cut_Hash(base_pred);

	std::set<int> base_rawid;
	int i = 0;
	for (i = 0; i < base_pred_hash.size(); i++) {
		printf("\r %d/%d(%4.1lf%%)", i, base_pred_hash.size(), i*100. / base_pred_hash.size());
		std::vector <netscan::base_track_t> base_ali = base_alignment(base_pred_hash[i], base);
		Basetrack_matching(base_pred_hash[i], base_ali, base_rawid);
	}
	printf("\r %d/%d(%4.1lf%%)\n", i, base_pred_hash.size(), i*100. / base_pred_hash.size());
	std::vector<netscan::base_track_t> base_out = id_search(base, base_rawid);
	printf("output base num=%d\n", base_out.size());
	netscan::write_basetrack_vxx(file_out_bvxx, base_out, pl, 0);
}
std::vector<std::vector<netscan::base_track_t>> Base_Area_Cut_Hash(std::vector<netscan::base_track_t> base) {
	std::vector<std::vector<netscan::base_track_t>> ret;
	double area[4];
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			area[0] = itr->x;
			area[1] = itr->x;
			area[2] = itr->y;
			area[3] = itr->y;
		}
		area[0] = std::min(area[0], itr->x);
		area[1] = std::max(area[1], itr->x);
		area[2] = std::min(area[2], itr->y);
		area[3] = std::max(area[3], itr->y);
	}
	double cut = 5000;
	area[0] += cut;
	area[1] -= cut;
	area[2] += cut;
	area[3] -= cut;

	std::multimap<std::pair<int, int>, netscan::base_track_t>  base_map;
	double hash = 7500;
	int ix, iy;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->x < area[0])continue;
		if (itr->x > area[1])continue;
		if (itr->y < area[2])continue;
		if (itr->y > area[3])continue;
		ix = (itr->x - area[0]) / hash;
		iy = (itr->y - area[2]) / hash;
		base_map.insert(std::make_pair(std::make_pair(ix, iy), *itr));

	}
	std::vector<netscan::base_track_t> base_vec;
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		if (itr->first == std::next(itr, 1)->first) {
			base_vec.push_back(itr->second);
		}
		else {
			base_vec.push_back(itr->second);
			ret.push_back(base_vec);
			base_vec.clear();
		}
	}
	return ret;
}
std::vector <netscan::base_track_t> base_alignment(std::vector <netscan::base_track_t>&base0, std::vector <netscan::base_track_t> &base1) {
	double x_min, x_max, y_min, y_max;
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		if (itr == base0.begin()) {
			x_min = itr->x;
			x_max = itr->x;
			y_min = itr->y;
			y_max = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		x_max = std::max(itr->x, x_max);
		y_min = std::min(itr->y, y_min);
		y_max = std::max(itr->y, y_max);
	}
	double overrap = 2000;
	x_min -= overrap;
	x_max += overrap;
	y_min -= overrap;
	y_max += overrap;
	std::vector<netscan::base_track_t> ret;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (itr->x < x_min)continue;
		if (itr->x > x_max)continue;
		if (itr->y < y_min)continue;
		if (itr->y > y_max)continue;
		ret.push_back(*itr);
	}



	//base0とretのaignmen-->retを変換
	double shift_x, shift_y, gap;
	std::vector<std::pair<netscan::base_track_t, netscan::base_track_t>> match;
	double diff_pos[2], diff_ang[2], angle;
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		for (auto itr2 = ret.begin(); itr2 != ret.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.05 * angle + 0.02)continue;
			if (fabs(diff_ang[1]) > 0.02)continue;
			if (fabs(diff_pos[0]) > 10 * angle + 15)continue;
			if (fabs(diff_pos[1]) > 15)continue;
			match.push_back(std::make_pair(*itr, *itr2));
		}
	}
	std::vector<double>dx, ax, dy, ay;
	std::vector<double>angle_v, dr;
	double tmp[2];

	for (auto itr = match.begin(); itr != match.end(); itr++) {
		ax.push_back(itr->first.ax);
		ay.push_back(itr->first.ay);
		dx.push_back(itr->first.x - itr->second.x);
		dy.push_back(itr->first.y - itr->second.y);
		tmp[0] = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);
		tmp[1] = ((itr->first.x - itr->second.x)*itr->first.ax + (itr->first.y - itr->second.y)*itr->first.ay) / tmp[0];
		angle_v.push_back(tmp[0]);
		dr.push_back(tmp[1]);

	}
	double slope;
	fit_line(ax, dx, slope, shift_x);
	fit_line(ay, dy, slope, shift_y);
	fit_line_intercept0(angle_v, dr, gap);

	//printf("shift x=%lf\n", shift_x);
	//printf("shift y=%lf\n", shift_y);
	//printf("gap=%lf\n", gap);
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		itr->x = itr->x + shift_x + itr->ax*gap;
		itr->y = itr->y + shift_y + itr->ay*gap;
	}


	//以下確認
	match.clear();
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		for (auto itr2 = ret.begin(); itr2 != ret.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.05 * angle + 0.02)continue;
			if (fabs(diff_ang[1]) > 0.02)continue;
			if (fabs(diff_pos[0]) > 10 * angle + 15)continue;
			if (fabs(diff_pos[1]) > 15)continue;
			match.push_back(std::make_pair(*itr, *itr2));
		}
	}
	//	printf("match num=%d\n", match.size());

	dx.clear();
	ax.clear();
	dy.clear();
	ay.clear();
	for (auto itr = match.begin(); itr != match.end(); itr++) {
		ax.push_back(itr->first.ax);
		ay.push_back(itr->first.ay);
		dx.push_back(itr->first.x - itr->second.x);
		dy.push_back(itr->first.y - itr->second.y);
		tmp[0] = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);
		tmp[1] = ((itr->first.x - itr->second.x)*itr->first.ax + (itr->first.y - itr->second.y)*itr->first.ay) / tmp[0];
		angle_v.push_back(tmp[0]);
		dr.push_back(tmp[1]);

	}
	fit_line(ax, dx, slope, shift_x);
	fit_line(ay, dy, slope, shift_y);
	fit_line_intercept0(angle_v, dr, gap);

	//printf("shift x=%lf\n", shift_x);
	//printf("shift y=%lf\n", shift_y);
	//printf("gap=%lf\n", gap);
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		itr->x = itr->x + itr->ax*gap;
		itr->y = itr->y + itr->ay*gap;
	}
	return ret;
}
void fit_line(std::vector<double>x, std::vector<double>y, double &slope, double &intercept) {
	if (x.size() != y.size()) {
		printf("exeption\n");
		exit(1);
	}
	double x1, y1, x2, y2, xy, n;
	x1 = 0;
	y1 = 0;
	x2 = 0;
	y2 = 0;
	xy = 0;
	n = 0;
	for (int i = 0; i < x.size(); i++) {
		x1 += x[i];
		y1 += y[i];
		x2 += x[i] * x[i];
		xy += x[i] * y[i];
		n += 1;
	}
	slope = (n*xy - x1 * y1) / (n*x2 - x1 * x1);
	intercept = (x2*y1 - xy * x1) / (n*x2 - x1 * x1);
}
void fit_line_intercept0(std::vector<double>x, std::vector<double>y, double &slope) {
	if (x.size() != y.size()) {
		printf("exeption\n");
		exit(1);
	}
	double x2, xy;
	x2 = 0;
	xy = 0;
	for (int i = 0; i < x.size(); i++) {
		x2 += x[i] * x[i];
		xy += x[i] * y[i];
	}
	slope = xy / x2;
}

void Basetrack_matching(std::vector<netscan::base_track_t> base_pred, std::vector<netscan::base_track_t>  &base_all, std::set<int>& base_rawid) {

	//int count = 0;

#pragma omp parallel for
	for (int i = 0; i < base_pred.size(); i++) {
		double diff_pos[2], diff_ang[2];
		auto itr = &(base_pred[i]);
		bool hit_flg = false;
		double angle;
		netscan::base_track_t base_hit;
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);

		for (auto itr2 = base_all.begin(); itr2 != base_all.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.05 * angle + 0.04)continue;
			if (fabs(diff_ang[1]) > 0.04)continue;
			if (fabs(diff_pos[0]) > 10 * angle + 15)continue;
			if (fabs(diff_pos[1]) > 15)continue;
			if (hit_flg == true) {
				if (base_hit.m[0].ph + base_hit.m[1].ph < itr2->m[0].ph + itr2->m[1].ph) {
					base_hit = *(itr2);
				}
			}
			else {
				base_hit = *(itr2);
				hit_flg = true;
			}
		}
		if (hit_flg) {
#pragma omp critical
			base_rawid.insert(base_hit.rawid);
		}
	}
}
std::vector<netscan::base_track_t> id_search (std::vector<netscan::base_track_t> base, std::set<int> base_rawid) {
	std::map<int, netscan::base_track_t> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_map.insert(std::make_pair(itr->rawid, *itr));
	}
	std::vector<netscan::base_track_t> ret;
	for (auto itr = base_rawid.begin(); itr != base_rawid.end(); itr++) {
		ret.push_back(base_map.find(*itr)->second);
	}
	return ret;
}
