#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

bool sort_chain(const mfile0::M_Chain &left, const mfile0::M_Chain &right) {
	return left.basetracks.begin()->group_id < right.basetracks.begin()->group_id;
}
int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-mfile1 file-in-mfile2 file-out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile1 = argv[1];
	std::string file_in_mfile2 = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m1,m2;
	mfile0::read_mfile(file_in_mfile1, m1);
	mfile0::read_mfile(file_in_mfile2, m2);
	for (auto itr = m1.chains.begin(); itr != m1.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			itr2->group_id = (itr2->group_id / 100000) * 100000;
		}
	}
	for (auto itr = m2.chains.begin(); itr != m2.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			itr2->group_id = (itr2->group_id / 100000) * 100000+1;
		}
		m1.chains.push_back(*itr);
	}


	sort(m1.chains.begin(), m1.chains.end(), sort_chain);

	mfile0::write_mfile(file_out_mfile, m1);
}
