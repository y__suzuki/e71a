#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>

class eventinf {
public:
	int chainid, pl, rawid,eventid;
	double ax, ay, x, y;
	Sharing_file::Sharing_file sf;
};

bool sort_chain(const mfile0::M_Chain &left,const mfile0::M_Chain &right) {
	if (left.basetracks[0].group_id == right.basetracks[0].group_id)return left.chain_id < right.chain_id;
	return left.basetracks[0].group_id < right.basetracks[0].group_id;
}
bool sort_eventinf(const eventinf &left, const eventinf &right) {
	if (left.eventid != right.eventid)return left.eventid < right.eventid;
	else if (left.chainid != right.chainid)return left.chainid < right.chainid;
	return left.pl < right.pl;
}
bool getFileNames(std::string folderPath, std::vector<std::string> &file_names);
std::vector<eventinf> Read_eventinf(std::string filename);
void add_eventid(std::vector<eventinf>&ev, int pl, int rawid, int eventid);
void Write_event_inf(std::string filename, std::vector<eventinf>&ev);
std::vector<eventinf> remove_eventid(std::vector<eventinf>&ev);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}

	std::string file_in_mfile_dir = argv[1];
	std::string file_in_eventinf = argv[2];
	std::string file_out_mfile = argv[3];
	std::string file_out_eventinf = argv[4];

	std::vector<eventinf> ev_inf = Read_eventinf(file_in_eventinf);

	std::vector < std::string > file_in_mfiles;
	getFileNames(file_in_mfile_dir, file_in_mfiles);

	mfile0::Mfile m;
	for (auto &filename : file_in_mfiles) {
		mfile0::Mfile m_tmp;
		mfile1::read_mfile_extension(filename, m_tmp);

		std::string pl_str = filename.substr(filename.size() - 16, 3);
		std::string ev_str = filename.substr(filename.size() - 12, 8);
		printf("%s %s\n", pl_str.c_str(), ev_str.c_str());

		int pl = std::stoi(pl_str.c_str());
		int ev= std::stoi(ev_str.c_str());

		for (auto &c : m_tmp.chains) {
			for (auto &b : c.basetracks) {
				b.group_id = pl * 10000000 + ev;
				add_eventid(ev_inf, b.pos / 10, b.rawid, pl * 10000000 + ev);

			}
			m.chains.push_back(c);
		}
	}
	ev_inf = remove_eventid(ev_inf);

	std::sort(ev_inf.begin(), ev_inf.end(), sort_eventinf);
	std::sort(m.chains.begin(), m.chains.end(), sort_chain);

	mfile1::write_mfile_extension(file_out_mfile, m);
	Write_event_inf(file_out_eventinf, ev_inf);
}

bool getFileNames(std::string folderPath, std::vector<std::string> &file_names)
{
	using namespace std::filesystem;
	directory_iterator iter(folderPath), end;
	std::error_code err;

	for (; iter != end && !err; iter.increment(err)) {
		const directory_entry entry = *iter;

		file_names.push_back(entry.path().string());
		//printf("%s\n", file_names.back().c_str());
	}

	/* エラー処理 */
	if (err) {
		std::cout << err.value() << std::endl;
		std::cout << err.message() << std::endl;
		return false;
	}
	return true;
}

std::vector<eventinf> Read_eventinf(std::string filename) {
	std::vector<eventinf> ret;
	eventinf ev;
	std::ifstream ifs(filename);
	while (ifs >> ev.chainid >> ev.pl >> ev.rawid >> ev.ax >> ev.ay >> ev.x >> ev.y
		>> ev.sf.pl >> ev.sf.ecc_id >> ev.sf.oss_id >> ev.sf.fixedwall_id >> ev.sf.trackerwall_id >> ev.sf.spotid >> ev.sf.zone[0] >> ev.sf.rawid[0] >> ev.sf.zone[1] >> ev.sf.rawid[1] >> ev.sf.unix_time >> ev.sf.tracker_track_id >> ev.sf.babymind_bunch >> ev.sf.entry_in_daily_file >> ev.sf.babymind_nplane >> ev.sf.charge >> ev.sf.babymind_momentum >> ev.sf.chi2_shifter[0] >> ev.sf.chi2_shifter[1] >> ev.sf.chi2_shifter[2] >> ev.sf.chi2_shifter[3] >> ev.sf.eventid >> ev.sf.track_type >> ev.sf.ecc_track_type) {
		ev.eventid = -1;
		ret.push_back(ev);
	}
	printf("eventinf size =%d\n", ret.size());
	return ret;
}
void add_eventid(std::vector<eventinf>&ev,int pl,int rawid,int eventid) {
	for (auto &e : ev) {
		if (e.pl == pl && e.rawid == rawid) {
			e.eventid = eventid;
		}
	}
}
void Write_event_inf(std::string filename, std::vector<eventinf>&ev) {
	std::ofstream ofs(filename);
	int64_t count = 0;
	int64_t max = ev.size();

	for (int i = 0; i < max; i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / ev.size() << "%%";
		}
		count++;
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << ev[i].eventid << " "
			<< std::setw(10) << std::setprecision(0) << ev[i].chainid << " "
			<< std::setw(3) << std::setprecision(0) << ev[i].pl << " "
			<< std::setw(10) << std::setprecision(0) << ev[i].rawid << " "
			<< std::setw(7) << std::setprecision(4) << ev[i].ax << " "
			<< std::setw(7) << std::setprecision(4) << ev[i].ay << " "
			<< std::setw(8) << std::setprecision(1) << ev[i].x << " "
			<< std::setw(8) << std::setprecision(1) << ev[i].y << " "
			<< std::setw(3) << std::setprecision(0) << ev[i].sf.pl << " "
			<< std::setw(10) << std::setprecision(0) << ev[i].sf.ecc_id << " "
			<< std::setw(10) << std::setprecision(0) << ev[i].sf.oss_id << " "
			<< std::setw(10) << std::setprecision(0) << ev[i].sf.fixedwall_id << " "
			<< std::setw(10) << std::setprecision(0) << ev[i].sf.trackerwall_id << " "
			<< std::setw(4) << std::setprecision(0) << ev[i].sf.spotid << " "
			<< std::setw(2) << std::setprecision(0) << ev[i].sf.zone[0] << " "
			<< std::setw(10) << std::setprecision(0) << ev[i].sf.rawid[0] << " "
			<< std::setw(2) << std::setprecision(0) << ev[i].sf.zone[1] << " "
			<< std::setw(10) << std::setprecision(0) << ev[i].sf.rawid[1] << " "
			<< std::setw(10) << std::setprecision(0) << ev[i].sf.unix_time << " "
			<< std::setw(3) << std::setprecision(0) << ev[i].sf.tracker_track_id << " "
			<< std::setw(3) << std::setprecision(0) << ev[i].sf.babymind_bunch << " "
			<< std::setw(6) << std::setprecision(0) << ev[i].sf.entry_in_daily_file << " "
			<< std::setw(3) << std::setprecision(0) << ev[i].sf.babymind_nplane << " "
			<< std::setw(3) << std::setprecision(0) << ev[i].sf.charge << " "
			<< std::setw(7) << std::setprecision(1) << ev[i].sf.babymind_momentum << " "
			<< std::setw(7) << std::setprecision(3) << ev[i].sf.chi2_shifter[0] << " "
			<< std::setw(7) << std::setprecision(3) << ev[i].sf.chi2_shifter[1] << " "
			<< std::setw(7) << std::setprecision(3) << ev[i].sf.chi2_shifter[2] << " "
			<< std::setw(7) << std::setprecision(3) << ev[i].sf.chi2_shifter[3] << " "
			<< std::setw(7) << std::setprecision(0) << ev[i].sf.eventid << " "
			<< std::setw(2) << std::setprecision(0) << ev[i].sf.track_type << " "
			<< std::setw(2) << std::setprecision(0) << ev[i].sf.ecc_track_type
			<< std::endl;
		std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / ev.size() << "%%" << std::endl;
	}


}

std::vector<eventinf> remove_eventid(std::vector<eventinf>&ev) {
	std::vector<eventinf> ret;
	for (auto &e : ev) {
		if (e.eventid < 0)continue;
		ret.push_back(e);
	}
	return ret;
}