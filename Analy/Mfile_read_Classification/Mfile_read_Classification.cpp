#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
class base_inf {
public:
	int pl;
	float ax, ay;
	double x, y, z;
};
class chain_inf {
public:
	int groupid, chainid, nseg;
	base_inf b[2];

	//groupid chainid nseg npl down:ax ay x y z up:ax ay x y z

};

void read_mfile_get_inf(std::string filename, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z_map, std::map<int, int> &group_num, std::vector<chain_inf> &chain);

void output_inf(std::string file_out_path, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z_map, std::map<int, int> &group_num, std::vector<chain_inf> &chain);
void output_range(std::string filename, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z_map);
void output_chain(std::string filename, std::vector<chain_inf> &chain);
void output_group(std::string filename, std::map<int, int> &group_num);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-filename out-path flg\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_path = argv[2];
	int flg = std::stoi(argv[3]);

	//分類わけして読む

	//必要な情報リストの出力
	//pl xmin xmax ymin ymax
	std::map<int, double> xmin, xmax, ymin, ymax,z_map;
	//groupに属するchain数
	std::map<int, int> group_num;
	//groupid chainid nseg npl down:ax ay x y z up:ax ay x y z
	std::vector<chain_inf> chain;

	read_mfile_get_inf(file_in_mfile, xmin, xmax, ymin, ymax, z_map, group_num, chain);

	output_inf(file_out_path, xmin, xmax, ymin, ymax, z_map, group_num, chain);
	//gorupの1本化

	//貫通/最上流track/stop

	//nseg4以上/以下


	//
	
}
void read_mfile_get_inf(std::string filename, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z_map, std::map<int, int> &group_num, std::vector<chain_inf> &chain) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }

	//	std::vector< mfile1::MFileChain1> chains;
	//	chains.reserve(mfile.info_header.Nchain);

	//	std::vector< std::vector< mfile1::MFileBase1>> all_basetracks;
	uint64_t count = 0;
	int chain_id, group_id, nseg, pl;
	double x, y, z;

	for (uint64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		mfile1::MFileChain1 r_chain;
		ifs.read((char*)& r_chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }
		std::vector< mfile1::MFileBase1> basetracks;
		basetracks.reserve(r_chain.nseg);
		for (int b = 0; b < r_chain.nseg; b++) {
			mfile1::MFileBase1 base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			basetracks.emplace_back(base);
		}
		chain_inf c_inf;
		base_inf base[2];


		c_inf.chainid = r_chain.chain_id;
		c_inf.nseg = r_chain.nseg;
		for (int i = 0; i < basetracks.size(); i++) {
			pl = basetracks[i].pos / 10;
			x = basetracks[i].x;
			y = basetracks[i].y;
			z = basetracks[i].z;
			group_id = basetracks[i].group_id;

			auto res0 = xmin.insert(std::make_pair(pl, x));
			if (!res0.second)res0.first->second = std::min(res0.first->second, x);
			auto res1 = xmax.insert(std::make_pair(pl, x));
			if (!res1.second)res1.first->second = std::max(res1.first->second, x);

			auto res2 = ymin.insert(std::make_pair(pl, y));
			if (!res2.second)res2.first->second = std::min(res2.first->second, y);
			auto res3 = ymax.insert(std::make_pair(pl, y));
			if (!res3.second)res3.first->second = std::max(res3.first->second, y);

			auto res4 = z_map.insert(std::make_pair(pl, z));

			if (i == 0) {
				base[0].pl = pl;
				base[0].ax = basetracks[i].ax;
				base[0].ay = basetracks[i].ay;
				base[0].x = x;
				base[0].y = y;
				base[0].z = z;
				c_inf.groupid = group_id;
			}
			else if (i == basetracks.size() - 1) {
				base[1].pl = pl;
				base[1].ax = basetracks[i].ax;
				base[1].ay = basetracks[i].ay;
				base[1].x = x;
				base[1].y = y;
				base[1].z = z;
			}
		}

		c_inf.b[0] = base[0];
		c_inf.b[1] = base[1];
		chain.push_back(c_inf);
		auto res = group_num.insert(std::make_pair(group_id, 1));
		if (!res.second) {
			res.first->second += 1;
		}

	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

}



void output_inf(std::string file_out_path, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z_map, std::map<int, int> &group_num, std::vector<chain_inf> &chain){
	std::stringstream file_out_folder;
	file_out_folder << file_out_path << "\\mfile_inf";

	std::filesystem::create_directories(file_out_folder.str());

	std::stringstream file_out_range,file_out_chain,file_out_group;
	file_out_range << file_out_folder.str() << "\\mfile_range.txt";
	file_out_chain << file_out_folder.str() << "\\mfile_chain.txt";
	file_out_group << file_out_folder.str() << "\\mfile_group.txt";

	output_range(file_out_range.str(), xmin, xmax, ymin, ymax,z_map);
	output_group(file_out_group.str(), group_num);
	output_chain(file_out_chain.str(), chain);

}
void output_range(std::string filename, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z_map) {
	std::ofstream ofs(filename);
	int key;
	for (auto itr = xmin.begin(); itr != xmin.end(); itr++) {
		key = itr->first;
		auto val0 = xmin.find(key);
		auto val1 = xmax.find(key);
		auto val2 = ymin.find(key);
		auto val3 = ymax.find(key);
		auto val4 = z_map.find(key);
		if (val0 == xmin.end())continue;
		if (val1 == xmax.end())continue;
		if (val2 == ymin.end())continue;
		if (val3 == ymax.end())continue;
		if (val4 == z_map.end())continue;
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << key << " "
			<< std::setw(10) << std::setprecision(1) << val0->second << " "
			<< std::setw(10) << std::setprecision(1) << val1->second << " "
			<< std::setw(10) << std::setprecision(1) << val2->second << " "
			<< std::setw(10) << std::setprecision(1) << val3->second << " "
			<< std::setw(10) << std::setprecision(1) << val4->second << std::endl;

	}
}
void output_chain(std::string filename, std::vector<chain_inf> &chain) {
	int count = 0;
	std::ofstream ofs(filename);
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r write chain %d/%d(%4.1lf%%)", count, chain.size(), count*100. / chain.size());
		}
		count++;

		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(12) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(4) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(4) << std::setprecision(0) << itr->b[0].pl << " "
			<< std::setw(7) << std::setprecision(4) << itr->b[0].ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->b[0].ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[0].x << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[0].y << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[0].z << " "
			<< std::setw(4) << std::setprecision(0) << itr->b[1].pl << " "
			<< std::setw(7) << std::setprecision(4) << itr->b[1].ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->b[1].ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[1].x << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[1].y << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[1].z << std::endl;
	}
	fprintf(stderr, "\r write chain %d/%d(%4.1lf%%)\n", count, chain.size(), count*100. / chain.size());

}
void output_group(std::string filename, std::map<int, int> &group_num) {

	std::map<int, int> group_num_out;
	for (auto itr = group_num.begin(); itr != group_num.end(); itr++) {
		auto res = group_num_out.insert(std::make_pair(itr->second, 1));
		if (!res.second) {
			res.first->second += 1;
		}
	}


	std::ofstream ofs(filename);
	for (auto itr = group_num_out.begin(); itr != group_num_out.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr->first << " "
			<< std::setw(12) << std::setprecision(0) << itr->second << " "
			<< std::endl;
	}
}

void divide_upstream_chain(std::vector<mfile0::M_Chain> &c, std::vector<mfile0::M_Chain> &up, std::vector<mfile0::M_Chain> &down, int PL_thr) {
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->pos1 / 10 >= PL_thr) {
			up.push_back(*itr);
		}
		else {
			down.push_back(*itr);
		}
	}
	printf("upstream track %lld\n", up.size());
	printf("downstream track %lld\n", down.size());
}
