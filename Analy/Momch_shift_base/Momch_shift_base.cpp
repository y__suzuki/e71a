#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <filesystem>
#include <set>
#include <omp.h>
void Set_read_list(std::vector<Momentum_recon::Event_information>&momch, std::map<int, std::map<int, vxx::base_track_t>> &read_list, std::set<std::pair<int, int>> &pl_pair);
void add_baseinf(std::string file_in_ECC, std::map<int, std::map<int, vxx::base_track_t>> &read_list);
std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_filename(std::string file_in_ECC);
int use_thread(double ratio, bool output);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-momch ECC-path fa.txt");
	}
	std::string file_in_momch = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_in_area = argv[3];

	//corrmap absの読み込み
	std::string file_in_corrmap = file_in_ECC + "\\Area0\\0\\align\\fine\\local\\corrmap-local-abs.lst";
	std::map<int, std::vector<corrmap_3d::align_param>>corrmap = corrmap_3d::read_ali_param_abs(file_in_corrmap, 1);
	std::map<int, std::vector<corrmap_3d::align_param2>>corrmap_dd = corrmap_3d::DelaunayDivide_map(corrmap);

	//fiducial areaの読み込み
	std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> area = Fiducial_Area::read_fiducial_Area(file_in_area);
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		if (corrmap_dd.count(itr->first) == 0) {
			fprintf(stderr, "corrmap local abs PL%03d not found\n", itr->first);
			exit(1);
		}
		std::vector<corrmap_3d::align_param2> param = corrmap_dd.at(itr->first);
		trans_mfile_cordinate(param, itr->second);
	}


	std::vector<Momentum_recon::Event_information>momch=Momentum_recon::Read_Event_information_extension(file_in_momch);
	std::map<int, std::map<int, vxx::base_track_t>> read_list;
	std::set<std::pair<int, int>> pl_pair;
	//basetrack情報、隣接PL情報の抽出
	Set_read_list(momch, read_list, pl_pair);
	//basetrack情報の抽出
	add_baseinf(file_in_ECC, read_list);
	
	//alignment paramerter read
	//これはポインタで参照されるためとっておく
	std::vector < std::pair<std::pair<int, int>, std::vector <corrmap_3d::align_param >>>all_align_param;
	//std::vector<std::pair< std::pair<int, int>, std::vector <corrmap_3d::align_param2 >>>all_align_param2;
	std::map< std::pair<int, int>, std::vector <corrmap_3d::align_param2 >>all_align_param2;
	std::vector<std::pair<std::pair<int, int>, std::string> >  files_in_align = Get_alignment_filename(file_in_ECC);

	//通常alinment pl0-->pl1 読みこみ
	int count = 0, all = files_in_align.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < files_in_align.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align read %d/%d", count, all);
		}
#pragma omp atomic
		count++;

		if (pl_pair.count(files_in_align[i].first) == 0)continue;

		std::vector <corrmap_3d::align_param > corr = corrmap_3d::read_ali_param(files_in_align[i].second, false);
#pragma omp critical
		all_align_param.push_back(std::make_pair(files_in_align[i].first, corr));
	}
	printf("\r corrmap align read %d/%d fin\n", count, all);


	//Delaunay3角形への分割
	count = 0, all = all_align_param.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < all_align_param.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align calc delaunay %d/%d", count, all);
		}
#pragma omp atomic
		count++;

		std::vector <corrmap_3d::align_param2 >corr2 = DelaunayDivide(all_align_param[i].second);
#pragma omp critical
		all_align_param2.insert(std::make_pair(all_align_param[i].first, corr2));

	}
	printf("\r corrmap align calc delaunay %d/%d fin\n", count, all);

	//
	double shift_x, shift_y;
	for (auto &ev : momch) {
		for (auto &chain : ev.chains) {
			Momentum_recon::Mom_chain c = chain;
			std::vector<vxx::base_track_t> chain_shift_base;
			for (int i_shift_x = -10; i_shift_x <= 10; i_shift_x++) {
				for (int i_shift_y = -10; i_shift_y <= 10; i_shift_y++) {
					if (i_shift_x == 0 && i_shift_y == 0)continue;
					shift_x = i_shift_x * 10000;
					shift_y = i_shift_y * 10000;

					//最下流をシフト

					//下流から元の位置ずれ角度ずれでbasetrackを配置

					//fiducial areaの確認

					//出力
				}
			}
		}
	}


}
void Set_read_list(std::vector<Momentum_recon::Event_information>&momch,std::map<int, std::map<int, vxx::base_track_t>> &read_list, std::set<std::pair<int, int>> &pl_pair)
{
	int pl;
	for (auto &ev_v : momch) {
		for (auto &c : ev_v.chains) {
			for (auto &b : c.base) {
				pl = b.pl;
				auto res = read_list.find(pl);
				if (res == read_list.end()) {
					std::map<int, vxx::base_track_t> base_map;
					vxx::base_track_t base;
					base_map.insert(std::make_pair(b.rawid, base));
					read_list.insert(std::make_pair(pl, base_map));
				}
				else {
					vxx::base_track_t base;
					res->second.insert(std::make_pair(b.rawid, base));

				}
			}
			for (auto &pair : c.base_pair) {
				pl_pair.insert(std::make_pair(pair.first.pl, pair.second.pl));
			}
		}
	}

}
void add_baseinf(std::string file_in_ECC, std::map<int, std::map<int, vxx::base_track_t>> &read_list) {
	for (auto itr = read_list.begin(); itr != read_list.end(); itr++) {
		std::stringstream file_in_base;
		file_in_base << file_in_ECC << "\\Area0\\PL"
			<< std::setw(3) << std::setfill('0') << itr->first << "\\b"
			<< std::setw(3) << std::setfill('0') << itr->first << ".sel.cor.vxx";
		int raw_min = itr->second.begin()->first;
		int raw_max = itr->second.rbegin()->first;
		std::vector<vxx::base_track_t >base;
		vxx::BvxxReader br;
		std::array<int, 2> index = { raw_min,raw_max + 1 };//1234<=rawid<=5678であるようなものだけを読む。
		printf("rawid %10d - %10d\n", raw_min, raw_max);
		base = br.ReadAll(file_in_base.str(), itr->first, 0, vxx::opt::index = index);
		for (auto &b:base){
			auto res = itr->second.find(b.rawid);
			if (res == itr->second.end())continue;
			res->second = b;
		}
	}
}
std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_filename(std::string file_in_ECC) {
	std::vector<std::pair<std::pair<int, int>, std::string> > ret;
	std::map<std::pair<int, int>, std::string> ret_map;

	std::string file_in_align_path = file_in_ECC + "\\Area0\\0\\align\\fine";
	std::filesystem::directory_iterator iter(file_in_align_path), end;
	std::error_code err;

	std::vector<std::string > file_names;
	for (; iter != end && !err; iter.increment(err)) {
		const std::filesystem::directory_entry entry = *iter;

		file_names.push_back(entry.path().string());
		//printf("%s\n", file_names.back().c_str());
	}
	for (int i = 0; i < file_names.size(); i++) {
		int length = file_names[i].size();
		if (file_names[i].substr(length - 4, 4) != ".txt")continue;
		if (file_names[i].substr(length - 18, 14) != "_interpolation")continue;

		int pl0 = std::stoi(file_names[i].substr(length - 25, 3));
		int pl1 = std::stoi(file_names[i].substr(length - 21, 3));

		ret_map.insert(std::make_pair(std::make_pair(pl0, pl1), file_names[i]));
	}
	for (auto itr = ret_map.begin(); itr != ret_map.end(); itr++) {
		ret.push_back(*itr);
	}
	return ret;
}


int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
