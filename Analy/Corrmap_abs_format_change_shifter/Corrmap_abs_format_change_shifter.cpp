#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>

void read_ali_param(std::string filename, std::multimap<int, corrmap_3d::align_param>&corr);
void output_corrmap(std::string filename, std::multimap<int, corrmap_3d::align_param>&corr);

void change_format(std::vector<corrmap0::Corrmap>&old_param, std::multimap<int, corrmap_3d::align_param>&new_param, int ix, int iy, std::map<int, double> &z_map);
std::map<int, double> Read_stfile(std::string filename);


bool sort_param(const corrmap_3d::align_param&left, const corrmap_3d::align_param&right) {
	if (left.ix == right.ix)return left.iy < right.iy;
	return left.ix < right.ix;
}
int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:prg file-in-align ix iy file-in-ECC file-out mode\n");
		fprintf(stderr, "mode=0:create/overwrite file\n");
		fprintf(stderr, "mode=1:add(sort) file\n");

		exit(1);
	}

	std::string file_in_align = argv[1];
	int ix = std::stoi(argv[2]);
	int iy = std::stoi(argv[3]);
	std::string file_in_ECC = argv[4];
	std::string file_out = argv[5];
	int mode = std::stoi(argv[6]);
	if (mode != 0 && mode != 1) {
		fprintf(stderr, "mode exception\n");
		fprintf(stderr, "mode=0:create/overwrite file\n");
		fprintf(stderr, "mode=1:add(sort) file\n");
		exit(1);
	}

	if (!std::filesystem::exists(file_in_align)) {
		fprintf(stderr, "file[%s]not exist\n", file_in_align.c_str());
		return 0;
	}
	std::vector<corrmap0::Corrmap> corr_origin;
	corrmap0::read_cormap(file_in_align, corr_origin);

	std::multimap<int, corrmap_3d::align_param> corr_align;

	if (mode == 1) {
		read_ali_param(file_out, corr_align);
	}
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\..\\st\\st.dat";
	std::map<int, double>z_map= Read_stfile(structure_path.str());


	change_format(corr_origin, corr_align, ix, iy, z_map);

	output_corrmap(file_out, corr_align);

}
std::map<int, double> Read_stfile(std::string filename) {
	chamber1::Chamber chamber;
	chamber1::read_structure(filename, chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	double base_z = z_map.at(1) + 280;

	for (auto itr = z_map.begin(); itr != z_map.end(); itr++) {
		itr->second -= base_z;
	}

	return z_map;
}

void change_format(std::vector<corrmap0::Corrmap>&old_param, std::multimap<int, corrmap_3d::align_param>&new_param, int ix, int iy, std::map<int, double> &z_map) {
	int min_pl = 99999999;
	for (auto itr = old_param.begin(); itr != old_param.end(); itr++) {
		min_pl = std::min(min_pl, itr->pos[0] / 10);
	}
	if (min_pl == 3) {
		int pl = 0;
		for (auto itr = old_param.begin(); itr != old_param.end(); itr++) {
			pl = itr->pos[0] / 10;
			if (z_map.count(pl) == 0) {
				printf("PL%03d z not found\n", pl);
				exit(1);
			}

			corrmap_3d::align_param param;
			param.id = 0;
			param.ix = ix;
			param.iy = iy;
			param.signal = 0;
			param.dx = itr->position[4];
			param.dy = itr->position[5];
			param.dz = itr->dz;

			double factor = itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2];
			param.x_shrink = sqrt(factor);
			param.y_shrink = sqrt(factor);
			param.z_shrink = 1;
			//param.z_shrink = itr->notuse_d[0];

			param.x_rot = 0;
			param.y_rot = 0;
			param.z_rot = atan((itr->position[2] / itr->position[0]));

			//�K���̈�͕␳�O�̌n?
			//double area[4];
			//area[0] = itr->areax[0] - itr->position[4];
			//area[1] = itr->areax[1] - itr->position[4];
			//area[2] = itr->areay[0] - itr->position[5];
			//area[3] = itr->areay[1] - itr->position[5];

			//itr->areax[0] = 1. / factor * (area[0] * itr->position[3] - area[2] * itr->position[1]);
			//itr->areay[0] = 1. / factor * (area[2] * itr->position[0] - area[0] * itr->position[2]);
			//itr->areax[1] = 1. / factor * (area[1] * itr->position[3] - area[3] * itr->position[1]);
			//itr->areay[1] = 1. / factor * (area[3] * itr->position[0] - area[1] * itr->position[2]);

			param.x = (itr->areax[0] + itr->areax[1]) / 2;
			param.y = (itr->areay[0] + itr->areay[1]) / 2;
			param.z = z_map.at(pl);


			param.yx_shear = 0;
			param.zx_shear = 0;
			param.zy_shear = 0;

			new_param.insert(std::make_pair(pl, param));

		}
	}

	std::multimap<int, corrmap_3d::align_param> ret;
	for (auto itr = new_param.begin(); itr != new_param.end(); itr++) {
		std::vector<corrmap_3d::align_param> corr_v;
		int pl = itr->first;
		auto range = new_param.equal_range(pl);
		for (auto res = range.first; res != range.second; res++) {
			corr_v.push_back(res->second);
		}
		sort(corr_v.begin(), corr_v.end(), sort_param);
		for (int i = 0; i < corr_v.size(); i++) {
			corr_v[i].id = i;
			ret.insert(std::make_pair(pl, corr_v[i]));
		}

		itr = std::next(itr, new_param.count(pl) - 1);
	}

	std::swap(new_param, ret);
}

void read_ali_param(std::string filename, std::multimap<int, corrmap_3d::align_param>&corr) {

	corrmap_3d::align_param param_tmp;
	int pl;
	std::ifstream ifs(filename);

	while (ifs >> pl >> param_tmp.id >> param_tmp.ix >> param_tmp.iy >> param_tmp.signal
		>> param_tmp.x >> param_tmp.y >> param_tmp.z
		>> param_tmp.x_rot >> param_tmp.y_rot >> param_tmp.z_rot
		>> param_tmp.x_shrink >> param_tmp.y_shrink >> param_tmp.z_shrink
		>> param_tmp.yx_shear >> param_tmp.zx_shear >> param_tmp.zy_shear
		>> param_tmp.dx >> param_tmp.dy >> param_tmp.dz) {
		corr.insert(std::make_pair(pl, param_tmp));
		//printf("ix %d iy%d\n", param_tmp.ix, param_tmp.iy);
	}
	fprintf(stderr, "%s input finish\n", filename.c_str());

}

void output_corrmap(std::string filename, std::multimap<int, corrmap_3d::align_param>&corr) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open ���s
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (corr.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		if (corr.size() != 0) {
			for (auto itr = corr.begin(); itr != corr.end(); itr++) {
				if (count % 10000 == 0) {
					fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)", count, int(corr.size()), count*100. / corr.size());
				}
				count++;
				ofs << std::right << std::fixed
					<< std::setw(8) << std::setprecision(0) << itr->first << " "
					<< std::setw(8) << std::setprecision(0) << itr->second.id << " "
					<< std::setw(4) << std::setprecision(0) << itr->second.ix << " "
					<< std::setw(4) << std::setprecision(0) << itr->second.iy << " "
					<< std::setw(4) << std::setprecision(0) << itr->second.signal << " "
					<< std::setw(8) << std::setprecision(1) << itr->second.x << " "
					<< std::setw(8) << std::setprecision(1) << itr->second.y << " "
					<< std::setw(8) << std::setprecision(1) << itr->second.z << " "
					<< std::setw(9) << std::setprecision(7) << itr->second.x_rot << " "
					<< std::setw(9) << std::setprecision(7) << itr->second.y_rot << " "
					<< std::setw(9) << std::setprecision(7) << itr->second.z_rot << " "
					<< std::setw(8) << std::setprecision(6) << itr->second.x_shrink << " "
					<< std::setw(8) << std::setprecision(6) << itr->second.y_shrink << " "
					<< std::setw(8) << std::setprecision(6) << itr->second.z_shrink << " "
					<< std::setw(8) << std::setprecision(6) << itr->second.yx_shear << " "
					<< std::setw(8) << std::setprecision(6) << itr->second.zx_shear << " "
					<< std::setw(8) << std::setprecision(6) << itr->second.zy_shear << " "
					<< std::setw(8) << std::setprecision(2) << itr->second.dx << " "
					<< std::setw(8) << std::setprecision(2) << itr->second.dy << " "
					<< std::setw(8) << std::setprecision(2) << itr->second.dz << std::endl;
			}
			fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)\n", count, int(corr.size()), count*100. / corr.size());
		}
	}



}

