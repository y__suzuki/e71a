#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include <functions.hpp>
#include "VxxReader.h"

#include <set>
#include <picojson.h>

class seach_tracks {
public:
	std::vector<int> black_ph_threshold, black_vph_threshold;
	int pl, black_npl_threshold, mip_npl_threshold;
	double  md_threshld,search_depth;
	std::string file_corrmap, file_basetrack;
};

class attach_track {
public:
	std::vector<mfile0::M_Chain> c;
	vxx::base_track_t b;
	double md, depth;

};

void read_param_json(std::string filename, std::string &file_in_mfile, std::string &file_in_corrmap, std::string &file_out_mfile, std::string &file_out_inf, std::vector<seach_tracks> &param);
vxx::base_track_t read_muon(std::string filename);
void Print_param(std::string file_in_mfile, std::string file_in_corrmap, std::string file_out_mfile, std::string file_out_inf, std::vector<seach_tracks>param);
std::map<int, double> SetZmap(std::vector<mfile0::M_Chain>&c);

std::vector<attach_track> attach_search(vxx::base_track_t muon, seach_tracks param, std::map<int, double> z_map, std::vector<mfile0::M_Chain> &all, std::vector<corrmap0::Corrmap> &corr_abs);
std::vector<vxx::base_track_t> read_base(vxx::base_track_t &muon, std::string filename, int pl, int alignment_flg, std::string file_corrmap);

void Apply_alignment(vxx::base_track_t &base, double nominal_gap, std::vector<corrmap0::Corrmap> &corr);
void Apply_alignment(std::vector<vxx::base_track_t> &base, double nominal_gap, std::vector<corrmap0::Corrmap> &corr);
void basetrack_trans(std::vector<vxx::base_track_t>&base, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size, double nominal_gap);
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size);
void basetrack_tans_affin(vxx::base_track_t &base, corrmap0::Corrmap corr, double nominal_z);

std::vector<attach_track> attach_cand(vxx::base_track_t muon, std::vector<vxx::base_track_t> &base, seach_tracks param);
std::vector<attach_track> chain_check(std::vector<attach_track>&attach_cand, std::vector<mfile0::M_Chain> &c, seach_tracks param, std::map<int, double> z_map, std::vector<corrmap0::Corrmap> &corr_abs, int mu_pl);
void basetrack2chain(attach_track &attach, std::map<int, double> z, std::vector < corrmap0::Corrmap> &abs, int64_t &chain_num, seach_tracks param);
std::vector<attach_track>  chain_threshold_judge(std::vector<attach_track> &attach_cand, seach_tracks param, int mu_pl);

void wrtie_attach_inf(std::string filename, std::vector<attach_track> &attach_cand_all);

int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "prg param.json\n");
		exit(1);
	}
	std::string file_in_json = argv[1];
	std::string file_in_mfile;
	std::string file_in_corrmap;
	std::string file_out_mfile;
	std::string file_out_inf;

	std::vector<seach_tracks> param;

	read_param_json(file_in_json, file_in_mfile, file_in_corrmap, file_out_mfile, file_out_inf, param);
	Print_param(file_in_mfile, file_in_corrmap, file_out_mfile, file_out_inf, param);

	vxx::base_track_t muon = read_muon(file_in_json);
	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrmap, corr_abs);

	std::map<int, double> z_map = SetZmap(m.chains);

	mfile0::Mfile m_out;
	m_out.header = m.header;
	std::vector<attach_track> attach_cand_all;
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		printf("search PL%03d\n", itr->pl);
		std::vector<attach_track> attach_chain = attach_search(muon, *itr, z_map, m.chains, corr_abs);
		for (auto itr2 = attach_chain.begin(); itr2 != attach_chain.end(); itr2++) {
			for (auto itr3 = itr2->c.begin(); itr3 != itr2->c.end(); itr3++) {
				m_out.chains.push_back(*itr3);
			}
			attach_cand_all.push_back(*itr2);
		}
	}
	if (file_out_mfile != "") {
		mfile0::write_mfile(file_out_mfile, m_out);
	}
	if (file_out_inf != "") {
		wrtie_attach_inf(file_out_inf, attach_cand_all);
	}

}


void read_param_json(std::string filename, std::string &file_in_mfile, std::string &file_in_corrmap,std::string &file_out_mfile,std::string &file_out_inf,std::vector<seach_tracks> &param) {

	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}
	picojson::object &all = v.get<picojson::object>();
	file_in_mfile = all["mfile_all"].get<std::string>();
	file_in_corrmap = all["corrmap_abs"].get<std::string>();
	file_out_mfile= all["otuput_mfile"].get<std::string>();
	file_out_inf= all["otuput_inf"].get<std::string>();

	picojson::object &search_condition = all["search_condition"].get<picojson::object>();
	std::vector<int> black_ph_threshold, black_vph_threshold;
	int black_npl_threshold, mip_npl_threshold;
	double search_depth;

	picojson::array &array_tmp = search_condition["black_PH_threshold"].get<picojson::array>();
	for (auto itr = array_tmp.begin(); itr != array_tmp.end(); itr++) {
		black_ph_threshold.push_back((int)itr->get<double>());
	}
	array_tmp.clear();
	array_tmp = search_condition["black_VPH_threshold"].get<picojson::array>();
	for (auto itr = array_tmp.begin(); itr != array_tmp.end(); itr++) {
		black_vph_threshold.push_back((int)itr->get<double>());
	}
	black_npl_threshold = (int)search_condition["black_npl_threshold"].get<double>();
	mip_npl_threshold = (int)search_condition["mip_npl_threshold"].get<double>();
	search_depth = search_condition["search_depth"].get<double>();
	array_tmp.clear();

	picojson::array &search_pl_inf = all["search_pl_inf"].get<picojson::array>();
	for (auto itr = search_pl_inf.begin(); itr != search_pl_inf.end(); itr++) {
		seach_tracks s_t;
		picojson::object& obj = itr->get<picojson::object>();

		s_t.file_basetrack = obj["basetrack"].get < std::string >();
		s_t.pl = (int)obj["pl"].get < double  >();
		s_t.file_corrmap = obj["corrmap_align"].get < std::string  >();
		s_t.md_threshld = obj["md_threshold"].get < double  >();

		s_t.black_npl_threshold = black_npl_threshold;
		s_t.black_ph_threshold = black_ph_threshold;
		s_t.black_vph_threshold = black_vph_threshold;
		s_t.mip_npl_threshold = mip_npl_threshold;
		s_t.search_depth = search_depth;

		param.push_back(s_t);
	}
}
vxx::base_track_t read_muon(std::string filename) {
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}
	picojson::object &all = v.get<picojson::object>();
	picojson::object &muon_information = all["muon_information"].get<picojson::object>();


	std::string file_in_bvxx = muon_information["basetrack"].get<std::string>();
	int pl = (int)muon_information["pl"].get<double >();
	int rawid = (int)muon_information["rawid"].get<double>();

	vxx::BvxxReader br;
	std::array<int, 2> index = {rawid, rawid+1 };//1234<=rawid<=5678であるようなものだけを読む。
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0, vxx::opt::index = index);

	vxx::base_track_t ret;
	bool flg = true;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (rawid == itr->rawid) {
			ret = *itr;
			flg = false;
			break;
		}
	}
	if (flg) {
		fprintf(stderr, "muon track not found\n");
		exit(1);
	}
	return ret;
}

void Print_param(std::string file_in_mfile, std::string file_in_corrmap, std::string file_out_mfile, std::string file_out_inf, std::vector<seach_tracks>param) {
	printf("----------------------input parameter--------------------------\n");
	printf("mfile all:%s\n", file_in_mfile.c_str());
	printf("corr(bas):%s\n", file_in_corrmap.c_str());
	printf("mfile out:%s\n", file_out_mfile.c_str());
	printf("inf out  :%s\n", file_out_inf.c_str());
	printf("\n");
	auto itr = param.begin();
	printf("nPL(black)   :%d\n", itr->black_npl_threshold);
	printf("PL(mip)      :%d\n", itr->mip_npl_threshold);
	printf("depth        :%.1lf\n", itr->search_depth);
	printf("black thr PH :[");
	for (auto itr2 = itr->black_ph_threshold.begin(); itr2 != itr->black_ph_threshold.end(); itr2++) {
		if (itr2 + 1 == itr->black_ph_threshold.end()) {
			printf("%d", *itr2);
		}
		else {
			printf("%d, ", *itr2);
		}
	}
	printf("]\n");
	printf("black thr VPH:[");
	for (auto itr2 = itr->black_vph_threshold.begin(); itr2 != itr->black_vph_threshold.end(); itr2++) {
		if (itr2 + 1 == itr->black_vph_threshold.end()) {
			printf("%d", *itr2);
		}
		else {
			printf("%d, ", *itr2);
		}
	}
	printf("]\n");
	printf("\n");

	for (itr = param.begin(); itr != param.end(); itr++) {
		printf("\tbasetrack  :%s\n", itr->file_basetrack.c_str());
		printf("\tPL         :%d\n", itr->pl);
		printf("\tcorr(align):%s\n", itr->file_corrmap.c_str());
		printf("\tmd thr     :%.1lf\n", itr->md_threshld);
		printf("\n");

	}
	printf("-------------------------------------------------------------------------\n");
}
std::map<int, double> SetZmap(std::vector<mfile0::M_Chain>&c) {
	std::map<int, double> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			ret.insert(std::make_pair(itr2->pos / 10, itr2->z));
		}
	}
	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	printf("%d : %lf\n", itr->first, itr->second);
	//}

	return ret;
}

//attachを探す
std::vector<attach_track> attach_search(vxx::base_track_t muon, seach_tracks param, std::map<int, double> z_map, std::vector<mfile0::M_Chain> &all, std::vector<corrmap0::Corrmap> &corr_abs) {
	std::vector<mfile0::M_Chain> ret;

	//0-->そのまま、1-->attach searchのほうが上流、そちらを変換、-1-->muonのほうが上流、そちらを変換
	//
	int aligment_flg = 0;
	if (muon.pl < param.pl) {
		if (param.file_corrmap != "") {
			aligment_flg = 1;
		}
	}
	else if (muon.pl > param.pl) {
		if (param.file_corrmap != "") {
			aligment_flg = -1;
		}
	}

	//basetrack or muonを変換。座標系を合わせる。
	std::vector<vxx::base_track_t> base = read_base(muon, param.file_basetrack, param.pl, aligment_flg, param.file_corrmap);

	//MDでattachするbasetrackの探索
	std::vector<attach_track> cand = attach_cand(muon, base, param);
	printf("attach basetrack :%d\n", cand.size());
	//Chainの有無,balck/mipのカットなど
	cand = chain_check(cand, all, param, z_map, corr_abs, muon.pl);
	printf("after chain mip/black cut:%d\n", cand.size());
	return cand;

}
std::vector<vxx::base_track_t> read_base(vxx::base_track_t &muon, std::string filename, int pl, int alignment_flg, std::string file_corrmap) {
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(filename, pl, 0);

	int pl0 = std::min(muon.pl, pl);
	int pl1 = std::max(muon.pl, pl);
	double iron = 500;
	double emulsion = 350;
	double water = 2000;
	double acryl = 2000;
	double pack = 116;
	double nominal_gap;
	if (pl1 - pl0 == 0) {
		nominal_gap = 0;
		return base;
	}
	else if (pl1 - pl0 == 1) {
		if (pl1 <= 1) {
			fprintf(stderr, "PL number exception PL%0d PL%0d\n", pl0, pl1);
			exit(1);
		}
		else if (pl1 == 2)nominal_gap = emulsion;
		else if (pl1 == 3)nominal_gap = emulsion + acryl;
		else if (pl1 == 4)nominal_gap = emulsion;
		else if (pl1 <= 15)nominal_gap = iron + emulsion;
		else if (pl1 == 16)nominal_gap = emulsion + pack * 2;
		else if (pl1 % 2 == 0)nominal_gap = water + pack * 2 + emulsion;
		else nominal_gap = iron + emulsion;
	}
	else if (pl1 - pl0 == 2) {
		if (pl1 <= 2) {
			fprintf(stderr, "PL number exception PL%0d PL%0d\n", pl0, pl1);
			exit(1);
		}
		else if (pl1 == 3)nominal_gap = emulsion * 2 + acryl;
		else if (pl1 == 4)nominal_gap = emulsion * 2 + acryl;
		if (pl1 <= 15)nominal_gap = iron * 2 + emulsion * 2;
		else if (pl1 == 16 || pl1 == 17)nominal_gap = emulsion * 2 + pack * 2 + iron;
		else  nominal_gap = water + pack * 2 + emulsion * 2 + iron;
	}
	else {
		fprintf(stderr, "now only 0peke and 1peke are supported\n");
		fprintf(stderr, "Please contact Suzuki\n");
		exit(1);
	}

	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_corrmap, corr);
	if (alignment_flg == 1) {
		Apply_alignment(base, nominal_gap, corr);
	}
	else if (alignment_flg == -1) {
		Apply_alignment(muon, nominal_gap, corr);
	}
	return base;
}
void Apply_alignment(std::vector<vxx::base_track_t> &base, double nominal_gap, std::vector<corrmap0::Corrmap> &corr) {
	//corrmapの範囲を逆変換
	double hash_size = 4000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	corrmap_area_inverse(corr, corr_map, hash_size);

	basetrack_trans(base, corr_map, hash_size, nominal_gap);

}
void basetrack_trans(std::vector<vxx::base_track_t>&base, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size, double nominal_gap) {
		int ix, iy;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			std::pair<int, int> id;
			ix = itr->x / hash_size;
			iy = itr->y / hash_size;
			std::vector<corrmap0::Corrmap> corr_list;
			int loop = 0;
			while (corr_list.size() <= 3) {
				loop++;
				for (int iix = -1 * loop; iix <= 1 * loop; iix++) {
					for (int iiy = -1 * loop; iiy <= 1 * loop; iiy++) {
						id.first = ix + iix;
						id.second = iy + iiy;
						if (corr_map.count(id) == 0)continue;
						auto range = corr_map.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							corr_list.push_back(res->second);
						}
					}
				}
			}
			double dist;
			corrmap0::Corrmap param;
			for (auto itr2 = corr_list.begin(); itr2 != corr_list.end(); itr2++) {
				double cx, cy;
				cx = (itr2->areax[0] + itr2->areax[1]) / 2;
				cy = (itr2->areay[0] + itr2->areay[1]) / 2;
				if (itr2 == corr_list.begin()) {
					dist = (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy);
					param = *itr2;
				}
				if (dist > (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy)) {
					dist = (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy);
					param = *itr2;
				}

			}
			basetrack_tans_affin(*itr, param, nominal_gap);

		}


	}
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size) {

		//hash_sizeはcorrmapのサイズより大きくする
		double cx, cy;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			double area[4], factor;
			area[0] = itr->areax[0] - itr->position[4];
			area[1] = itr->areax[1] - itr->position[4];
			area[2] = itr->areay[0] - itr->position[5];
			area[3] = itr->areay[1] - itr->position[5];

			factor = 1 / (itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);
			itr->areax[0] = factor * (area[0] * itr->position[3] - area[2] * itr->position[1]);
			itr->areay[0] = factor * (area[2] * itr->position[0] - area[0] * itr->position[2]);
			itr->areax[1] = factor * (area[1] * itr->position[3] - area[3] * itr->position[1]);
			itr->areay[1] = factor * (area[3] * itr->position[0] - area[1] * itr->position[2]);

			cx = (itr->areax[0] + itr->areax[1]) / 2;
			cy = (itr->areay[0] + itr->areay[1]) / 2;

			std::pair<int, int>id;
			id.first = cx / hash_size;
			id.second = cy / hash_size;
			corr_map.insert(std::make_pair(id, *itr));
		}

	}
void basetrack_tans_affin(vxx::base_track_t &base, corrmap0::Corrmap corr, double nominal_z) {
		double x_tmp, y_tmp;
		x_tmp = base.x;
		y_tmp = base.y;
		base.x = x_tmp * corr.position[0] + y_tmp * corr.position[1] + corr.position[4];
		base.y = x_tmp * corr.position[2] + y_tmp * corr.position[3] + corr.position[5];

		x_tmp = base.ax;
		y_tmp = base.ay;
		base.ax = x_tmp * corr.angle[0] + y_tmp * corr.angle[1] + corr.angle[4];
		base.ay = x_tmp * corr.angle[2] + y_tmp * corr.angle[3] + corr.angle[5];

		base.z = -1 * nominal_z + corr.dz;
	}

void Apply_alignment(vxx::base_track_t &base, double nominal_gap, std::vector<corrmap0::Corrmap> &corr) {
	//corrmapの範囲を逆変換
	double hash_size = 4000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	corrmap_area_inverse(corr, corr_map, hash_size);

	int ix, iy;
	std::pair<int, int> id;
	ix = base.x / hash_size;
	iy = base.y / hash_size;
	std::vector<corrmap0::Corrmap> corr_list;
	int loop = 0;
	while (corr_list.size() <= 3) {
		loop++;
		for (int iix = -1 * loop; iix <= 1 * loop; iix++) {
			for (int iiy = -1 * loop; iiy <= 1 * loop; iiy++) {
				id.first = ix + iix;
				id.second = iy + iiy;
				if (corr_map.count(id) == 0)continue;
				auto range = corr_map.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					corr_list.push_back(res->second);
				}
			}
		}
	}
	double dist;
	corrmap0::Corrmap param;
	for (auto itr2 = corr_list.begin(); itr2 != corr_list.end(); itr2++) {
		double cx, cy;
		cx = (itr2->areax[0] + itr2->areax[1]) / 2;
		cy = (itr2->areay[0] + itr2->areay[1]) / 2;
		if (itr2 == corr_list.begin()) {
			dist = (base.x - cx)*(base.x - cx) + (base.y - cy)*(base.y - cy);
			param = *itr2;
		}
		if (dist > (base.x - cx)*(base.x - cx) + (base.y - cy)*(base.y - cy)) {
			dist = (base.x - cx)*(base.x - cx) + (base.y - cy)*(base.y - cy);
			param = *itr2;
		}

	}
	basetrack_tans_affin(base, param, nominal_gap);

}

std::vector<attach_track> attach_cand(vxx::base_track_t muon, std::vector<vxx::base_track_t> &base, seach_tracks param) {
	std::vector<attach_track> ret;

	matrix_3D::vector_3D pos0, pos1, dir0,dir1;
	pos0.x = muon.x;
	pos0.y = muon.y;
	pos0.z = muon.z;
	dir0.x = muon.ax;
	dir0.y = muon.ay;
	dir0.z =1;
	int num = 0, all = base.size();
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (num % 10000 == 0) {
			printf("\r vertex search %d/%d(%4.1lf%%)", num, all, num*100. / all);
		}
		num++;

		pos1.x = itr->x;
		pos1.y = itr->y;
		pos1.z = itr->z;
		dir1.x = itr->ax;
		dir1.y = itr->ay;
		dir1.z = 1;

		double extra[2];
		double z_range[2] = { pos0.z - param.search_depth,pos0.z };
		double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		double depth0, depth1, depth_ave;
		depth0 = (pos0.z + extra[0]);
		depth1 = (pos1.z + extra[1]);
		depth_ave = (depth0 + depth1) / 2;
		if (md > param.md_threshld)continue;
		attach_track attach;
		attach.b = *itr;
		attach.depth = depth_ave;
		attach.md = md;
		ret.push_back(attach);
	}
	printf("\r vertex search %d/%d(%4.1lf%%) fin\n", num, all, num*100. / all);

	return ret;
}

std::vector<attach_track> chain_check(std::vector<attach_track>&attach_cand, std::vector<mfile0::M_Chain> &c, seach_tracks param, std::map<int, double> z_map, std::vector<corrmap0::Corrmap> &corr_abs,int mu_pl) {
	int64_t chain_num = 0;
	std::map<std::pair<int, int>, int> base2group;
	std::multimap<int, mfile0::M_Chain*> group2chain;
	for (auto itr = c.begin(); itr!= c.end(); itr++) {
		chain_num = std::max(chain_num, itr->chain_id);
		group2chain.insert(std::make_pair(itr->basetracks.begin()->group_id, &(*itr)));
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			base2group.insert(std::make_pair(std::make_pair(itr2->pos / 10, itr2->rawid), itr2->group_id));
		}
	}

	chain_num = (chain_num / 10000000 + 1) * 10000000;
	for (auto itr = attach_cand.begin(); itr != attach_cand.end(); itr++) {
		if (base2group.count(std::make_pair(itr->b.pl, itr->b.rawid)) == 0) {
			//chain(linklet)になっていない-->1segのchain作成
			basetrack2chain(*itr, z_map, corr_abs, chain_num, param);
		}
		else {
			int groupid = base2group.find(std::make_pair(itr->b.pl, itr->b.rawid))->second;
			if (group2chain.count(groupid) == 0) {
				fprintf(stderr, "groupID=%d not found\n", groupid);
				exit(1);
			}
			auto range = group2chain.equal_range(groupid);
			for (auto res = range.first; res != range.second; res++) {
				itr->c.push_back(*res->second);
			}
		}
	}

	std::vector<attach_track> ret = chain_threshold_judge(attach_cand, param,mu_pl);
	return ret;
}

void basetrack2chain(attach_track &attach, std::map<int, double> z, std::vector < corrmap0::Corrmap> &abs, int64_t &chain_num, seach_tracks param) {

	//basetrack座標系のbasetrackの取得
	vxx::BvxxReader br;
	std::array<int, 2> index = { attach.b.rawid, attach.b.rawid + 1 };//1234<=rawid<=5678であるようなものだけを読む。
	std::vector<vxx::base_track_t> base = br.ReadAll(param.file_basetrack, attach.b.pl, 0, vxx::opt::index = index);

	vxx::base_track_t base_ori;
	bool flg = true;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (attach.b.rawid == itr->rawid) {
			base_ori = *itr;
			flg = false;
			break;
		}
	}
	if (flg) {
		fprintf(stderr, "PL%03d rawid=%d track not found\n", attach.b.pl, attach.b.rawid);
		exit(1);
	}


	int pl = base_ori.pl;
	flg = false;
	mfile0::M_Chain c_tmp;
	mfile0::M_Base b;
	for (auto itr = abs.begin(); itr != abs.end(); itr++) {
		if (itr->pos[0] / 10 != pl)continue;
		if (flg)continue;
		b.ax = base_ori.ax*itr->angle[0] + base_ori.ay*itr->angle[1] + itr->angle[4];
		b.ay = base_ori.ax*itr->angle[2] + base_ori.ay*itr->angle[3] + itr->angle[5];
		b.x = base_ori.x*itr->position[0] + base_ori.y*itr->position[1] + itr->position[4];
		b.y = base_ori.x*itr->position[2] + base_ori.y*itr->position[3] + itr->position[5];
		b.ph = base_ori.m[0].ph + base_ori.m[1].ph;
		b.rawid = base_ori.rawid;
		b.pos = pl * 10 + 1;
		b.group_id = chain_num;
		auto z_val = z.find(pl);
		if (z_val == z.end()) {
			printf("PL%03d z not found\n", pl);
			exit(1);
		}
		b.z = z_val->second;

		flg = true;

	}
	if (!flg) {
		printf("PL%03d align abs not found\n", pl);
		exit(1);
	}
	c_tmp.basetracks.push_back(b);
	c_tmp.chain_id = chain_num;
	c_tmp.nseg = c_tmp.basetracks.size();
	c_tmp.pos0 = c_tmp.basetracks.begin()->pos;
	c_tmp.pos1 = c_tmp.basetracks.rbegin()->pos;
	attach.c.push_back(c_tmp);

	chain_num++;

}

std::vector<attach_track>  chain_threshold_judge(std::vector<attach_track> &attach_cand, seach_tracks param,int mu_pl) {
	std::vector<attach_track> ret;

	for (auto itr = attach_cand.begin(); itr != attach_cand.end(); itr++) {
		bool flg = false;
		for (auto itr2 = itr->c.begin(); itr2 != itr->c.end(); itr2++) {
			//plで貫通チェック
			int pl0, pl1;
			pl0 = itr2->pos0 / 10;
			pl1 = itr2->pos1 / 10;
			if (pl0<mu_pl - 1 && pl1>mu_pl + 1)continue;

			int npl = pl1 - pl0 + 1;
			double ph_ave = 0, vph_ave = 0;
			int num = 0;
			for (auto itr3 = itr2->basetracks.begin(); itr3 != itr2->basetracks.end(); itr3++) {
				num++;
				ph_ave += itr3->ph / 10000;
				vph_ave += itr3->ph % 10000;
			}
			ph_ave = ph_ave / num;
			vph_ave = vph_ave / num;
			double angle = pow(mfile0::chain_ax(*itr2), 2) + pow(mfile0::chain_ay(*itr2), 2);
			int i_ang = (int)sqrt(angle) * 10;
			if (ph_ave > param.black_ph_threshold[std::min(i_ang, (int)param.black_ph_threshold.size() - 1)] && vph_ave > param.black_vph_threshold[std::min(i_ang, (int)param.black_vph_threshold.size() - 1)]) {
				//balckの場合
				if (npl < param.black_npl_threshold)continue;
			}
			else {
				//mipの場合
				if (npl < param.mip_npl_threshold)continue;
			}
			flg = true;
		}
		if (flg) {
			ret.push_back(*itr);
		}
	}
	return ret;
}
void wrtie_attach_inf(std::string filename, std::vector<attach_track> &attach_cand_all) {
	std::ofstream ofs(filename);
	for (auto itr = attach_cand_all.begin(); itr != attach_cand_all.end(); itr++) {
		for (auto itr2 = itr->c.begin(); itr2 != itr->c.end(); itr2++) {
			ofs << std::right << std::fixed
				<< std::setw(3) << std::setprecision(0) << itr->b.pl << " "
				<< std::setw(8) << std::setprecision(0) << itr->b.rawid << " "
				<< std::setw(5) << std::setprecision(1) << itr->md << " "
				<< std::setw(7) << std::setprecision(1) << itr->depth << " "
				<< std::setw(12) << std::setprecision(0) << itr2->chain_id << " "
				<< std::setw(3) << std::setprecision(0) << itr2->pos0 / 10 << " "
				<< std::setw(3) << std::setprecision(0) << itr2->pos1 / 10 << " "
				<< std::setw(3) << std::setprecision(0) << itr2->nseg << std::endl;
		}
	}
}
