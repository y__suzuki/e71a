#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include <functions.hpp>
#include "VxxReader.h"

#include <set>

std::map<int, double> SetZmap(std::vector<mfile0::M_Chain>&c);

std::vector<vxx::base_track_t> search_attach_cand_down(mfile0::M_Base base, std::string file_path, vxx::base_track_t &base_mu);
std::vector<vxx::base_track_t> judge_attach_down(vxx::base_track_t base_mu, std::vector<vxx::base_track_t> &base);

std::vector<vxx::base_track_t> search_attach_cand_up(vxx::base_track_t base_mu, std::string file_path);
void basetrack_trans(std::vector<vxx::base_track_t>&base, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size, double nominal_gap);
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size);
void basetrack_tans_affin(vxx::base_track_t &base, corrmap0::Corrmap corr, double nominal_z);
std::vector<vxx::base_track_t> judge_attach_up(vxx::base_track_t base_mu, std::vector<vxx::base_track_t> &base);

void basetrack_chain_check_down(std::vector<mfile0::M_Chain> &c, std::vector<vxx::base_track_t> &base_all, std::vector<mfile0::M_Chain> &add, std::vector<vxx::base_track_t> &remain, int interaction_pl);
void basetrack_chain_check_up(std::vector<mfile0::M_Chain> &c, std::vector<vxx::base_track_t> &base_all, std::vector<mfile0::M_Chain> &add, std::vector<vxx::base_track_t> &remain, int interaction_pl);
void basetrack2chain(std::vector<mfile0::M_Chain> &c, std::vector<vxx::base_track_t> &base_all, std::map<int, double> z, std::vector < corrmap0::Corrmap> abs);

int maina(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "prg muon-mfile all-mfile base-folder \n");
		exit(1);
	}

	std::string file_in_mfile_muon = argv[1];
	std::string file_in_mfile_all = argv[2];
	std::string file_in_path = argv[3];

	std::vector<corrmap0::Corrmap> abs;
	{
		std::stringstream filename;
		filename << file_in_path << "\\0\\align\\corrmap-abs.lst";
		corrmap0::read_cormap(filename.str(), abs);
	}

	mfile0::Mfile mu, m;
	mfile1::read_mfile_extension(file_in_mfile_muon, mu);
	mfile1::read_mfile_extension(file_in_mfile_all, m);

	std::map<int, double> z_map = SetZmap(m.chains);
	std::vector<mfile0::M_Chain> attach_cand;

	//mfileでattach trackを探索
	mfile0::M_Base up_base = *(mu.chains.begin()->basetracks.rbegin());
	vxx::base_track_t base_mu;
	//basetrack-->attach上流
	std::vector<vxx::base_track_t> attach_cand_down = search_attach_cand_down(up_base, file_in_path, base_mu);
	std::vector<vxx::base_track_t> attach_cand_up = search_attach_cand_up(base_mu, file_in_path);
	//attachに対しchainの探索
	mfile0::Mfile m_down, m_up;
	m_down.header = m.header;
	m_up.header = m.header;
	std::vector<vxx::base_track_t> only_base_down;
	std::vector<vxx::base_track_t> only_base_up;
	basetrack_chain_check_down(m.chains, attach_cand_down, m_down.chains, only_base_down,base_mu.pl);
	basetrack_chain_check_up(m.chains, attach_cand_up, m_up.chains, only_base_up, base_mu.pl);
	printf("%d %d\n", m_down.chains.size(),  m_up.chains.size());
	basetrack2chain(m_down.chains, only_base_down, z_map, abs);
	basetrack2chain(m_up.chains, only_base_up, z_map, abs);
	printf("%d %d\n", m_down.chains.size(), m_up.chains.size());

	mfile0::write_mfile(file_in_mfile_muon+".down.all", m_down);
	mfile0::write_mfile(file_in_mfile_muon + ".up.all", m_up);
}
std::map<int, double> SetZmap(std::vector<mfile0::M_Chain>&c) {
	std::map<int, double> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			ret.insert(std::make_pair(itr2->pos / 10, itr2->z));
		}
	}
	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	printf("%d : %lf\n", itr->first, itr->second);
	//}

	return ret;
}


std::vector<vxx::base_track_t> search_attach_cand_down(mfile0::M_Base base, std::string file_path,vxx::base_track_t &base_mu) {
	//直下のbasetrack
	int pl = base.pos / 10;
	int rawid = base.rawid;

	std::stringstream file_in_bvxx;
	file_in_bvxx << file_path << "\\PL" << std::setw(3) << std::setfill('0') << pl << "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base_all = br.ReadAll(file_in_bvxx.str(), pl, 0);

	bool flg = true;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		if (itr->rawid == base.rawid) {
			base_mu = *itr;
			flg = false;
			break;
		}
	}
	if (flg) {
		fprintf(stderr, "not found id=%d\n", base.rawid);
		exit(1);
	}
	std::vector<vxx::base_track_t> attach_cand = judge_attach_down(base_mu, base_all);

	return attach_cand;
}

std::vector<vxx::base_track_t> judge_attach_down(vxx::base_track_t base_mu, std::vector<vxx::base_track_t> &base) {

	std::vector<vxx::base_track_t> ret;

	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	pos0.x = base_mu.x;
	pos0.y = base_mu.y;
	pos0.z = 0;

	dir0.x = base_mu.ax;
	dir0.y = base_mu.ay;
	dir0.z = 1;

	double nominal_gap;
	if (base_mu.pl > 15 && base_mu.pl % 2 == 0) {
		nominal_gap = 850;
	}
	else if (base_mu.pl > 15 && base_mu.pl % 2 == 1) {
		//水2mmの設定
		nominal_gap = 2582;
	}

	double md_thr = 200;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->rawid == base_mu.rawid)continue;
		pos1.x = itr->x;
		pos1.y = itr->y;
		pos1.z = 0;
		dir1.x = itr->ax;
		dir1.y = itr->ay;
		dir1.z = 1;

		double z_range[2] = { -1 * nominal_gap - 500,0 };
		double extra[2];
		double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		double oa = matrix_3D::opening_angle(dir0, dir1);
		if (md < md_thr) {
			ret.push_back(*itr);
		}

	}
	printf("down attach cand %d\n", ret.size());
	return ret;
}

std::vector<vxx::base_track_t> search_attach_cand_up(vxx::base_track_t base_mu, std::string file_path) {
	//直上のbasetrack
	int pl0 = base_mu.pl;
	int pl1 = base_mu.pl + 1;
	double nominal_gap;
	if (pl0 > 15 && pl0 % 2 == 0) {
		nominal_gap = 850;
	}
	else if (pl0 > 15 && pl0 % 2 == 1) {
		//水2mmの設定
		nominal_gap = 2582;
	}
	else {
		nominal_gap = -1;
	}
	std::stringstream file_in_bvxx;
	file_in_bvxx << file_path << "\\PL" << std::setw(3) << std::setfill('0') << pl1 << "\\b" << std::setw(3) << std::setfill('0') << pl1 << ".sel.cor.vxx";

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base_all = br.ReadAll(file_in_bvxx.str(), pl1, 0);

	std::stringstream file_in_align;
	file_in_align << file_path << "\\0\\align\\corrmap-align-" << std::setw(3) << std::setfill('0') << pl0 << "-" << std::setw(3) << std::setfill('0') << pl1 << ".lst";
	
	printf("%s\n", file_in_align.str().c_str());
	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_align.str(), corr);

	//corrmapの範囲を逆変換
	double hash_size = 4000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	corrmap_area_inverse(corr, corr_map, hash_size);

	std::vector<vxx::base_track_t> base_all_tmp = base_all;
	basetrack_trans(base_all_tmp, corr_map, hash_size, nominal_gap);
	std::vector<vxx::base_track_t> attach_cand = judge_attach_up(base_mu, base_all_tmp);

	std::set<int> rawid_list;
	for (auto itr = attach_cand.begin(); itr != attach_cand.end(); itr++) {
		rawid_list.insert(itr->rawid);
	}

	attach_cand.clear();
	for (auto itr = base_all.begin(); itr!= base_all.end(); itr++) {
		if (rawid_list.count(itr->rawid) == 1) {
			attach_cand.push_back(*itr);
		}
	}

	return attach_cand;


}
void basetrack_trans(std::vector<vxx::base_track_t>&base, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size, double nominal_gap) {
	int ix, iy;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::pair<int, int> id;
		ix = itr->x / hash_size;
		iy = itr->y / hash_size;
		std::vector<corrmap0::Corrmap> corr_list;
		int loop = 0;
		while (corr_list.size() <= 3) {
			loop++;
			for (int iix = -1*loop; iix <= 1*loop; iix++) {
				for (int iiy = -1 * loop; iiy <= 1 * loop; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (corr_map.count(id) == 0)continue;
					auto range = corr_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						corr_list.push_back(res->second);
					}
				}
			}
		}
		double dist;
		corrmap0::Corrmap param;
		for (auto itr2 = corr_list.begin(); itr2 != corr_list.end(); itr2++) {
			double cx, cy;
			cx = (itr2->areax[0] + itr2->areax[1]) / 2;
			cy = (itr2->areay[0] + itr2->areay[1]) / 2;
			if (itr2 == corr_list.begin()) {
				dist = (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy);
				param = *itr2;
			}
			if (dist > (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy)) {
				dist = (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy);
				param = *itr2;
			}

		}
		basetrack_tans_affin(*itr, param, nominal_gap);

	}


}
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map,double hash_size) {

	//hash_sizeはcorrmapのサイズより大きくする
	double cx, cy;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		double area[4], factor;
		area[0] = itr->areax[0] - itr->position[4];
		area[1] = itr->areax[1] - itr->position[4];
		area[2] = itr->areay[0] - itr->position[5];
		area[3] = itr->areay[1] - itr->position[5];

		factor = 1 / (itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);
		itr->areax[0] = factor * (area[0] * itr->position[3] - area[2] * itr->position[1]);
		itr->areay[0] = factor * (area[2] * itr->position[0] - area[0] * itr->position[2]);
		itr->areax[1] = factor * (area[1] * itr->position[3] - area[3] * itr->position[1]);
		itr->areay[1] = factor * (area[3] * itr->position[0] - area[1] * itr->position[2]);

		cx = (itr->areax[0] + itr->areax[1]) / 2;
		cy = (itr->areay[0] + itr->areay[1]) / 2;

		std::pair<int, int>id;
		id.first = cx / hash_size;
		id.second= cy / hash_size;
		corr_map.insert(std::make_pair(id, *itr));
	}

}
void basetrack_tans_affin(vxx::base_track_t &base, corrmap0::Corrmap corr, double nominal_z) {
	double x_tmp, y_tmp;
	x_tmp = base.x;
	y_tmp = base.y;
	base.x = x_tmp * corr.position[0] + y_tmp * corr.position[1] + corr.position[4];
	base.y = x_tmp * corr.position[2] + y_tmp * corr.position[3] + corr.position[5];

	x_tmp = base.ax;
	y_tmp = base.ay;
	base.ax = x_tmp * corr.angle[0] + y_tmp * corr.angle[1] + corr.angle[4];
	base.ay = x_tmp * corr.angle[2] + y_tmp * corr.angle[3] + corr.angle[5];

	base.z = -1 * nominal_z + corr.dz;
}
std::vector<vxx::base_track_t> judge_attach_up(vxx::base_track_t base_mu, std::vector<vxx::base_track_t> &base) {

	std::vector<vxx::base_track_t> ret;

	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	pos0.x = base_mu.x;
	pos0.y = base_mu.y;
	pos0.z = 0;

	dir0.x = base_mu.ax;
	dir0.y = base_mu.ay;
	dir0.z = 1;

	double md_thr = 200;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->rawid == base_mu.rawid)continue;
		pos1.x = itr->x;
		pos1.y = itr->y;
		pos1.z = itr->z;
		dir1.x = itr->ax;
		dir1.y = itr->ay;
		dir1.z = 1;

		double z_range[2] = { pos1.z-350,pos0.z };
		double extra[2];
		double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		double oa = matrix_3D::opening_angle(dir0, dir1);
		//if (itr->rawid == 65489) {
		//	double depth = (extra[0] + extra[1]) / 2;
		//	printf("md      =%lf\n", md);
		//	printf("depth0  =%lf\n", extra[0]);
		//	printf("depth1  =%lf\n", extra[1]);
		//	printf("depth   =%lf\n", depth);
		//	printf("zrange  =%lf\n", z_range[0]);
		//	printf("zrange  =%lf\n", z_range[1]);
		//}

		if (md < md_thr) {
			ret.push_back(*itr);
		}

	}

	printf("up attach cand %d\n", ret.size());

	return ret;
}

void basetrack_chain_check_down(std::vector<mfile0::M_Chain> &c, std::vector<vxx::base_track_t> &base_all, std::vector<mfile0::M_Chain> &add, std::vector<vxx::base_track_t> &remain, int interaction_pl) {

	std::set<std::pair<int, int>> base_id;

	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		base_id.insert(std::make_pair(itr->pl, itr->rawid));
	}

	std::set<std::pair<int, int>> exist_base_id;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		bool flg = false;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (base_id.count(std::make_pair(itr2->pos / 10, itr2->rawid)) == 1) {
				flg = true;
				exist_base_id.insert(std::make_pair(itr2->pos / 10, itr2->rawid));
			}
		}
		if (flg) {
			//+-5pl貫通-->reject
			if (itr->pos1 / 10 > interaction_pl + 1)continue;
			if (itr->nseg <= 4) {
				double vph = 0;
				for (int i = 0; i < itr->basetracks.size(); i++) {
					vph += itr->basetracks[i].ph%10000;
				}
				if (vph < 100 * itr->nseg)continue;
			}
			add.push_back(*itr);
		}
	}

	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		if (exist_base_id.count(std::make_pair(itr->pl, itr->rawid)) == 0) {
			remain.push_back(*itr);
		}
	}
}
void basetrack_chain_check_up(std::vector<mfile0::M_Chain> &c, std::vector<vxx::base_track_t> &base_all, std::vector<mfile0::M_Chain> &add, std::vector<vxx::base_track_t> &remain, int interaction_pl) {

	std::set<std::pair<int, int>> base_id;

	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		base_id.insert(std::make_pair(itr->pl, itr->rawid));
	}

	std::set<std::pair<int, int>> exist_base_id;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		bool flg = false;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (base_id.count(std::make_pair(itr2->pos / 10, itr2->rawid)) == 1) {
				flg = true;
				exist_base_id.insert(std::make_pair(itr2->pos / 10, itr2->rawid));
			}
		}
		if (flg) {
			//+-5pl貫通-->reject
			if (itr->pos0 / 10 < interaction_pl - 1)continue;
			if (itr->nseg <= 4) {
				double vph = 0;
				for (int i = 0; i < itr->basetracks.size(); i++) {
					vph += itr->basetracks[i].ph % 10000;
				}
				if (vph < 100 * itr->nseg)continue;
			}

			add.push_back(*itr);
		}
	}

	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		if (exist_base_id.count(std::make_pair(itr->pl, itr->rawid)) == 0) {
			remain.push_back(*itr);
		}
	}
}

void basetrack2chain(std::vector<mfile0::M_Chain> &c, std::vector<vxx::base_track_t> &base_all, std::map<int, double> z, std::vector < corrmap0::Corrmap> abs) {

	int chain_id = 10000000;
	int group_id = 10000000;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {

		int pl = itr->pl;
		bool flg = false;
		mfile0::M_Chain c_tmp;
		mfile0::M_Base b;
		for (auto itr2 = abs.begin(); itr2 != abs.end(); itr2++) {
			if (itr2->pos[0] / 10 != pl)continue;
			if (flg)continue;
			b.ax = itr->ax*itr2->angle[0] + itr->ay*itr2->angle[1] + itr2->angle[4];
			b.ay = itr->ax*itr2->angle[2] + itr->ay*itr2->angle[3] + itr2->angle[5];
			b.x = itr->x*itr2->position[0] + itr->y*itr2->position[1] + itr2->position[4];
			b.y = itr->x*itr2->position[2] + itr->y*itr2->position[3] + itr2->position[5];
			b.ph = itr->m[0].ph + itr->m[1].ph;
			b.rawid = itr->rawid;
			b.pos = pl * 10+1;
			b.group_id = group_id;
			auto z_val = z.find(pl);
			if (z_val == z.end()) {
				printf("PL%03d z not found\n", pl);
				exit(1);
			}
			b.z = z_val->second;

			flg = true;

		}
		if (!flg) {
			printf("PL%03d align abs not found\n", pl);
			exit(1);
		}
		if (b.ph % 10000 < 100)continue;
		c_tmp.basetracks.push_back(b);
		c_tmp.chain_id = chain_id;
		c_tmp.nseg = c_tmp.basetracks.size();
		c_tmp.pos0 = c_tmp.basetracks.begin()->pos;
		c_tmp.pos1 = c_tmp.basetracks.rbegin()->pos;
		c.push_back(c_tmp);
		chain_id--;
		group_id--;
	}


}