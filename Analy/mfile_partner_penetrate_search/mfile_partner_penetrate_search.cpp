#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <omp.h>
#include <set>
std::vector<mfile0::M_Chain> penetrate_check(mfile0::M_Chain c, mfile1::MFile_minimum&m);
mfile0::M_Base base_converter(mfile1::MFileBase &base);
int use_thread(double ratio, bool output);


int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-mfile file-in-mfile-all out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile_sel = argv[1];
	std::string file_in_mfile_all = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m;
	mfile1::MFile_minimum  m_all;
	mfile0::read_mfile(file_in_mfile_sel, m);
	mfile1::read_mfile(file_in_mfile_all, m_all);

	mfile0::Mfile m_out;
	m_out.header = m.header;
	int count = 0, all = m.chains.size();
	for (int64_t i = 0; i < m.chains.size(); i++) {
		printf("\r now ... %5d/%5d", count, all);
		count++;

		std::vector<mfile0::M_Chain> connect_cand = penetrate_check(m.chains[i], m_all);
		m_out.chains.push_back(m.chains[i]);
		for (auto itr2 = connect_cand.begin(); itr2 != connect_cand.end(); itr2++) {
			m_out.chains.push_back(*itr2);
		}
	}
	
	printf("\r now ... %5d/%5d\n", count, all);

	mfile0::write_mfile(file_out_mfile, m_out);
}
std::vector<mfile0::M_Chain> penetrate_check(mfile0::M_Chain c, mfile1::MFile_minimum&m) {
	int64_t gid = c.basetracks.begin()->group_id;
	std::vector<mfile0::M_Chain> ret;

	std::set<int64_t> cand;
	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	int pl0, pl1;
	pl0 = c.basetracks.begin()->pos / 10;
	pos0.x = c.basetracks.begin()->x;
	pos0.y = c.basetracks.begin()->y;
	pos0.z = c.basetracks.begin()->z;
	dir0.x = c.basetracks.begin()->ax;
	dir0.y = c.basetracks.begin()->ay;
	dir0.z = 1;

	pl1 = c.basetracks.rbegin()->pos / 10;
	pos1.x = c.basetracks.rbegin()->x;
	pos1.y = c.basetracks.rbegin()->y;
	pos1.z = c.basetracks.rbegin()->z;
	dir1.x = c.basetracks.rbegin()->ax;
	dir1.y = c.basetracks.rbegin()->ay;
	dir1.z = 1;

#pragma omp parallel for num_threads(use_thread(0.8,false)) schedule(guided)
	for (int64_t i = 0; i < m.all_basetracks.size(); i++) {
		double oa, md;
		double extra[2], z_range[2];
		int t_pl0, t_pl1;
		matrix_3D::vector_3D t_pos0, t_pos1, t_dir0, t_dir1;
		t_pl0 = m.all_basetracks[i].begin()->pos / 10;
		t_pl1 = m.all_basetracks[i].rbegin()->pos / 10;
		if (pl1 < t_pl0&&t_pl0 - pl1 <= 10) {
			t_pos0.x = m.all_basetracks[i].begin()->x;
			t_pos0.y = m.all_basetracks[i].begin()->y;
			t_pos0.z = m.all_basetracks[i].begin()->z;
			t_dir0.x = m.all_basetracks[i].begin()->ax;
			t_dir0.y = m.all_basetracks[i].begin()->ay;
			t_dir0.z = 1;
			z_range[1] = pos1.z;
			z_range[0] = t_pos0.z;
			oa = matrix_3D::opening_angle(dir1, t_dir0);
			md = matrix_3D::minimum_distance(pos1, t_pos0, dir1, t_dir0, z_range, extra);
			if (oa > 0.1)continue;
			if (md > 100)continue;
#pragma omp critical
			cand.insert(i);
		}
		if (pl0 > t_pl1&&pl0 - t_pl1 <= 10) {
			t_pos1.x = m.all_basetracks[i].rbegin()->x;
			t_pos1.y = m.all_basetracks[i].rbegin()->y;
			t_pos1.z = m.all_basetracks[i].rbegin()->z;
			t_dir1.x = m.all_basetracks[i].rbegin()->ax;
			t_dir1.y = m.all_basetracks[i].rbegin()->ay;
			t_dir1.z = 1;
			z_range[0] = pos0.z;
			z_range[1] = t_pos1.z;
			oa = matrix_3D::opening_angle(dir0, t_dir1);
			md = matrix_3D::minimum_distance(pos0, t_pos1, dir0, t_dir1, z_range, extra);
			if (oa > 0.1)continue;
			if (md > 100)continue;
#pragma omp critical
			cand.insert(i);
		}
	}

	
	for (auto itr = cand.begin(); itr != cand.end(); itr++) {
		mfile0::M_Chain chain;
		for (auto itr2 = m.all_basetracks[*itr].begin(); itr2 != m.all_basetracks[*itr].end(); itr2++) {
			mfile0::M_Base base = base_converter(*itr2);
			base.group_id = gid;
			chain.basetracks.push_back(base);
		}
		chain.nseg = chain.basetracks.size();
		chain.pos0 = chain.basetracks.begin()->pos;
		chain.pos1 = chain.basetracks.rbegin()->pos;
		chain.chain_id = *itr;
		ret.push_back(chain);
	}
	return ret;



}
mfile0::M_Base base_converter(mfile1::MFileBase &base) {
	mfile0::M_Base ret;
	ret.pos = base.pos;
	ret.rawid = base.rawid;
	ret.ph = base.ph;
	ret.ax = base.ax;
	ret.ay = base.ay;
	ret.x = base.x;
	ret.y = base.y;
	ret.z = base.z;
	ret.group_id = 0;
	ret.flg_d[0] = 0;
	ret.flg_d[1] = 0;
	ret.flg_i[0] = 0;
	ret.flg_i[1] = 0;
	ret.flg_i[2] = 0;
	ret.flg_i[3] = 0;
	return ret;
}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
