#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class Efficiency {
public:
	int pl, all, detect;
	double angle_min, angle_max;
};
void output_eff_file(std::string filename, std::vector<Efficiency>&eff);
std::vector<Efficiency> Calc_fill_factor(std::vector<mfile0::M_Chain>&c);


int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-mfile file-our\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_txt = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::vector<Efficiency> eff = Calc_fill_factor(m.chains);
	output_eff_file(file_out_txt, eff);

}
std::vector<Efficiency> Calc_fill_factor(std::vector<mfile0::M_Chain>&c) {
	std::map<int,std::map<double,Efficiency> >ret;
	for (int pl = 3; pl <= 133; pl++) {
		for (int i_ang = 0; i_ang < 20; i_ang++) {
			Efficiency eff;
			eff.angle_min = i_ang * 0.1;
			eff.angle_max = (i_ang + 1)*0.1;
			eff.pl = pl;
			eff.all = 0;
			eff.detect = 0;
			if (ret.count(eff.pl) == 0) {
				std::map<double, Efficiency> eff_map;
				eff_map.insert(std::make_pair(eff.angle_max, eff));
				ret.insert(std::make_pair(eff.pl, eff_map));
			}
			else {
				auto res = ret.find(eff.pl);
				res->second.insert(std::make_pair(eff.angle_max, eff));
			}
		}
	}
	
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		double ax = mfile0::chain_ax(*itr);
		double ay = mfile0::chain_ay(*itr);
		double angle = sqrt(ax*ax + ay * ay);
		int pl_min = itr->basetracks.begin()->pos / 10;
		int pl_max = itr->basetracks.rbegin()->pos / 10;
		pl_min = 3;
		pl_max = 133;
		//printf("angle %.1lf\n", angle);
		for (int pl = pl_min; pl <= pl_max; pl++) {
			if (ret.count(pl) == 0) {
				fprintf(stderr,"PL%03d efficiency not found\n", pl);
				exit(1);
			}
			auto eff_map = ret.find(pl);
			auto eff_file = eff_map->second.upper_bound(angle);
			if (eff_file == eff_map->second.end())continue;
			eff_file->second.all += 1;
			for (auto &b : itr->basetracks) {
				if (b.pos / 10 == pl) {
					eff_file->second.detect += 1;
					//printf("all %d / hit %d\n",eff_file->second.all,eff_file->second.detect);
				}
			}
		}
	}


	std::vector<Efficiency> eff_v;
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			eff_v.push_back(itr2->second);
		}
	}
	return eff_v;
}

void output_eff_file(std::string filename, std::vector<Efficiency>&eff) {
	std::ofstream ofs(filename);
	for (auto itr = eff.begin(); itr != eff.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->pl << " "
			<< std::setw(3) << std::setprecision(2) << itr->angle_min << " "
			<< std::setw(3) << std::setprecision(2) << itr->angle_max << " "
			<< std::setw(8) << std::setprecision(0) << itr->all << " "
			<< std::setw(8) << std::setprecision(0) << itr->detect << std::endl;

	}
}