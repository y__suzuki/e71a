#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include<vector>

void read_write_mfile(std::string in_file, std::string out_file);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg file-in-mfile file-out-group\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	read_write_mfile(file_in_mfile, file_out_mfile);
}
void mfile_read_write_bin(std::string file_in_mfile, std::string file_out_mfile) {

	std::ofstream ofs(file_out_mfile);
	std::ifstream ifs(file_in_mfile, std::ios::binary);


	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}


	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }


	std::vector< mfile1::MFileChain> group;
	std::vector< std::vector< mfile1::MFileBase>> all_basetracks;

	uint64_t count = 0, r_base_num = 0, r_chain_num = 0, w_base_num = 0, w_chain_num = 0, r_group_num = 0;
	int64_t gid = -1;
	for (int64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		//一旦chainを読む
		mfile1::MFileChain chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }
		r_chain_num++;

		std::vector< mfile1::MFileBase> basetracks;
		basetracks.reserve(chain.nseg);

		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			basetracks.emplace_back(base);
			r_base_num++;

		}

		//読んだchainが前のchainと同じgroupか確認
		if (c + 1 == mfile.info_header.Nchain) {
			if (gid != basetracks.begin()->group_id) {
				////////////////////////////
				//今までのgroupの書き出し
				r_group_num++;
				gid = basetracks.begin()->group_id;
				//gorupからbestなchainを選択
				if (group.size() == 1) {
					w_chain = group[0];
					w_base = all_basetracks[0];
				}
				else {
					select_best_chain(group, all_basetracks, w_chain, w_base);
				}
				//書き出し
				ofs.write((char*)& w_chain, sizeof(mfile1::MFileChain));
				w_chain_num++;
				assert(w_chain.nseg == w_base.size());
				for (int b = 0; b < w_chain.nseg; b++) {
					ofs.write((char*)& w_base[b], sizeof(mfile1::MFileBase));
					w_base_num++;
				}
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();
				////////////////////////////
				//最後のgroupの書き出し
				w_chain = chain;
				w_base = basetracks;
				r_group_num++;
				gid = basetracks.begin()->group_id;
				//書き出し
				ofs.write((char*)& w_chain, sizeof(mfile1::MFileChain));
				w_chain_num++;
				assert(w_chain.nseg == w_base.size());
				for (int b = 0; b < w_chain.nseg; b++) {
					ofs.write((char*)& w_base[b], sizeof(mfile1::MFileBase));
					w_base_num++;
				}
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();
			}
			else {
				//今のchainをpush back
				all_basetracks.emplace_back(basetracks);
				group.emplace_back(chain);
				r_group_num++;
				gid = basetracks.begin()->group_id;
				//gorupからbestなchainを選択
				if (group.size() == 1) {
					w_chain = group[0];
					w_base = all_basetracks[0];
				}
				else {
					select_best_chain(group, all_basetracks, w_chain, w_base);
				}
				//書き出し
				ofs.write((char*)& w_chain, sizeof(mfile1::MFileChain));
				w_chain_num++;
				assert(w_chain.nseg == w_base.size());
				for (int b = 0; b < w_chain.nseg; b++) {
					ofs.write((char*)& w_base[b], sizeof(mfile1::MFileBase));
					w_base_num++;
				}
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();

			}
		}
		else if (group.size() != 0 && gid != basetracks.begin()->group_id) {
			r_group_num++;
			gid = basetracks.begin()->group_id;
			//gorupからbestなchainを選択
			if (group.size() == 1) {
				w_chain = group[0];
				w_base = all_basetracks[0];
			}
			else {
				select_best_chain(group, all_basetracks, w_chain, w_base);
			}
			//書き出し
			ofs.write((char*)& w_chain, sizeof(mfile1::MFileChain));
			w_chain_num++;
			assert(w_chain.nseg == w_base.size());
			for (int b = 0; b < w_chain.nseg; b++) {
				ofs.write((char*)& w_base[b], sizeof(mfile1::MFileBase));
				w_base_num++;
			}
			group.clear();
			for (int64_t i = 0; i < all_basetracks.size(); i++) {
				all_basetracks[i].clear();
			}
			all_basetracks.clear();
		}
		else if (c == 0) {
			gid = basetracks.begin()->group_id;
		}
		all_basetracks.emplace_back(basetracks);
		group.emplace_back(chain);

	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

	if (r_chain_num != mfile.info_header.Nchain) { throw std::exception("Nchain is wrong."); }
	if (r_base_num != mfile.info_header.Nbasetrack) { throw std::exception("Nbasetrack is wrong."); }
	mfile.info_header.Nchain = w_chain_num;
	mfile.info_header.Nbasetrack = w_base_num;
	//file pointerを最初に戻してヘッダを書く
	ofs.clear();
	ofs.seekp(0, std::ios::beg);

	ofs.write((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	ofs.write((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));

	printf("mfile info\n");
	printf("group num =%lld\n", r_group_num);
	printf("chain num =%lld --> %lld\n", r_chain_num, w_chain_num);
	printf("base  num =%lld --> %lld\n", r_base_num, w_base_num);
}

void Write_group(std::ofstream &ofs, std::vector< mfile1::MFileChain> &group, std::vector< std::vector< mfile1::MFileBase>> &all_basetracks) {

	std::set<std::tuple<int, int, int, int>>all_linklet;
	for(int64_t )

}
