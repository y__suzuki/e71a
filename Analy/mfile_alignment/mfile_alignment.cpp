#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <omp.h>
#include <set>

class align_param {
public:
	int pl,id, signal, ix, iy;
	//視野中心
	double x, y, z;
	//parameter(9);
	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;

};
class track_difference {
public:
	double ax0, ax1, ay0, ay1, x0, x1, y0, y1, z0, z1;
	double sigma_ax, sigma_ay, sigma_px, sigma_py;
};

std::vector<track_difference> track_diff_angle_correction(std::vector<mfile0::M_Chain>&c, int pl);
std::multimap<std::pair<int, int>, track_difference> select_track(std::vector<track_difference>&diff, double align_step);
align_param calc_align_angle(std::pair<int, int>id, double hash_size, std::vector<track_difference> &diff_local);
std::vector<track_difference> calc_track_diff(std::vector<track_difference>&diff, align_param &param);

void output_diff(std::string filename, std::vector<track_difference> &diff);
double angle_acc(double angle);


void align_2plate(std::vector<mfile0::M_Chain>&c, int pl[2]);
void calc_angle_align(std::vector<track_difference> &pair, double &shrink, double &shift_x, double &shift_y);
void calc_angle_align_sigma(std::vector<track_difference> &pair, double &shrink, double &shift_x, double &shift_y);


void calc_align(align_param &param, std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> &base_pair);
void calc_position_align(std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> &base_pair, int mode, double &rotation, double &shrink, double &shift_x, double &shift_y);
std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> extra_center(std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> base_pair);
void calc_z_align(std::vector<track_difference> &pair, double &shift_z);
void calc_position_align(std::vector<track_difference> &pair, double &rotation, double &shrink, double &shift_x, double &shift_y);
void make_track_diff(std::vector<track_difference>&diff, std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> &base_pair, align_param &param);
align_param align_param_ini(std::pair<int, int>id, double hash_size, double z);

void output_corrmap(std::string filename, std::vector<align_param> &corr);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "prg in-mfile out-txt\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_txt = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	//5mm pichでlocal align
	double align_step = 5000;

	std::map<int, std::vector<align_param>> align_map;

	int pl[2] = { 3,133 };
	//angleのcorrection
	for (int pl_num = pl[0]; pl_num <= pl[1]; pl_num++) {
		printf("\r angle correction PL%03d", pl_num);
		//debug
		//if (pl_num!= 20)continue;
		std::vector<track_difference> diff_global_before;
		std::vector<track_difference> diff_global_after;

		//parameter
		std::vector<align_param> param_v;

		//hitのあるbasetrack/chain angleの抽出
		std::vector<track_difference> diff = track_diff_angle_correction(m.chains, pl_num);

		//stepでtrackを分割
		std::multimap<std::pair<int, int>, track_difference> diff_hash = select_track(diff, align_step);
		//各視野でparameterの出力
		for (auto itr = diff_hash.begin(); itr != diff_hash.end(); itr++) {
			//printf("ix:%d iy:%d\n", itr->first.first, itr->first.second);

			int count = diff_hash.count(itr->first);
			if (count < 100) {
				itr = std::next(itr, count - 1);
				continue;
			}

			auto range = diff_hash.equal_range(itr->first);
			std::vector<track_difference> diff_local;
			for (auto res = range.first; res != range.second; res++) {
				diff_local.push_back(res->second);
			}
			param_v.push_back(calc_align_angle(itr->first, align_step, diff_local));
			//debug
			//////////////////////
			for (auto itr2 = diff_local.begin(); itr2 != diff_local.end(); itr2++) {
				diff_global_before.push_back(*itr2);
			}
			diff_local = calc_track_diff(diff_local, *param_v.rbegin());
			for (auto itr2 = diff_local.begin(); itr2 != diff_local.end(); itr2++) {
				diff_global_after.push_back(*itr2);
			}
			/////////////////////

			itr = std::next(itr, count - 1);
		}
		for (int i = 0; i < param_v.size();i++){
			param_v[i].pl = pl_num;
			param_v[i].id = i;
		}
		//sigma ax,ayの付与

		//debug
		////////////////////
		//output_diff("out_diff0_all_noshift.txt", diff_global_before);
		//output_diff("out_diff1_all_noshift.txt", diff_global_after);
		////////////////////

		align_map.insert(std::make_pair(pl_num, param_v));
	}
	printf("\r angle correction PL%03d\n", pl[1]);

	std::vector<align_param> out_corr;
	for (auto itr = align_map.begin(); itr != align_map.end(); itr++) {
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			out_corr.push_back(*itr2);
		}
	}

	output_corrmap(file_out_txt, out_corr);

}
std::vector<track_difference> track_diff_angle_correction(std::vector<mfile0::M_Chain>&c, int pl) {
	std::vector<track_difference> ret;
	track_difference diff;
	bool flg = false;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		flg = false;
		diff.ax0 = mfile0::chain_ax(*itr);
		diff.ay0 = mfile0::chain_ay(*itr);
		diff.sigma_ax = angle_acc(diff.ax0);
		diff.sigma_ay = angle_acc(diff.ay0);

		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (itr2->pos / 10 == pl) {
				flg = true;
				diff.ax1 = itr2->ax;
				diff.ay1 = itr2->ay;
				diff.x1 = itr2->x;
				diff.y1 = itr2->y;
				diff.z1 = itr2->z;
			}
		}
		if (flg) {
			ret.push_back(diff);
		}
	}
	return ret;
}

std::multimap<std::pair<int, int>, track_difference> select_track(std::vector<track_difference>&diff, double align_step) {
	double x_min, y_min;
	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		if (itr == diff.begin()) {
			x_min = itr->x1;
			y_min = itr->y1;
		}
		x_min = std::min(itr->x1, x_min);
		y_min = std::min(itr->y1, y_min);
	}
	std::multimap<std::pair<int, int>, track_difference> ret;
	int ix, iy;
	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		if (itr->x1 < 0)ix = itr->x1 / align_step + 1;
		else ix = itr->x1 / align_step;

		if (itr->y1 < 0)iy = itr->y1 / align_step + 1;
		else iy = itr->y1 / align_step;

		ret.insert(std::make_pair(std::make_pair(ix, iy), *itr));

	}
	return ret;
}

align_param calc_align_angle(std::pair<int, int>id, double hash_size, std::vector<track_difference> &diff_local) {
	align_param param;
	param.ix = id.first;
	param.iy = id.second;
	param.x = id.first*hash_size + hash_size / 2;
	param.y = id.second*hash_size + hash_size / 2;
	param.z = 0;

	param.dx = 0;
	param.dy = 0;
	param.dz = 0;
	param.x_rot = 0;
	param.y_rot = 0;
	param.z_rot = 0;
	param.x_shrink = 1;
	param.y_shrink = 1;
	param.z_shrink = 1;
	param.yx_shear = 0;
	param.zx_shear = 0;
	param.zy_shear = 0;
	param.signal = 0;

	double shrink, shift_x, shift_y;
	//calc_angle_align(diff_local, shrink, shift_x, shift_y);
	calc_angle_align_sigma(diff_local, shrink, shift_x, shift_y);
	param.z_shrink *= shrink;
	//param.zx_shear += shift_x;
	//param.zy_shear += shift_y;
	std::vector<track_difference> tmp_diff = calc_track_diff(diff_local, param);
	//sigmaの測定

	//sigmaで割った値でalign

	//calc_angle_align(tmp_diff, shrink, shift_x, shift_y);
	//calc_angle_align_sigma(tmp_diff, shrink, shift_x, shift_y);
	//if (param.ix == 20 && param.iy == 20) {
	//	output_diff("out_diff0.txt", diff_local);
	//	output_diff("out_diff1.txt", tmp_diff);
	//}
	return param;

}

std::vector<track_difference> calc_track_diff(std::vector<track_difference>&diff, align_param &param) {
	std::vector<track_difference> ret;
	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		track_difference diff_tmp;
		diff_tmp.ax0 = itr->ax0;
		diff_tmp.ay0 = itr->ay0;
		diff_tmp.x0 = itr->x0;
		diff_tmp.y0 = itr->y0;
		diff_tmp.z0 = itr->z0;

		diff_tmp.ax1 = param.z_shrink*cos(param.z_rot)*itr->ax1 - param.z_shrink*sin(param.z_rot)*itr->ay1 + param.zx_shear;
		diff_tmp.ay1 = param.z_shrink*sin(param.z_rot)*itr->ax1 + param.z_shrink*cos(param.z_rot)*itr->ay1 + param.zy_shear;
		diff_tmp.x1 = param.x_shrink*cos(param.z_rot)*itr->x1 - param.y_shrink*sin(param.z_rot)*itr->y1 + param.dx;
		diff_tmp.y1 = param.x_shrink*sin(param.z_rot)*itr->x1 + param.y_shrink*cos(param.z_rot)*itr->y1 + param.dy;
		diff_tmp.z1 = itr->z1 + param.dz;

		diff_tmp.sigma_ax = itr->sigma_ax;
		diff_tmp.sigma_ay = itr->sigma_ay;
		diff_tmp.sigma_px = itr->sigma_px;
		diff_tmp.sigma_py = itr->sigma_py;

		ret.push_back(diff_tmp);
	}
	return ret;
}

double angle_acc(double angle) {
	double dx = 0.32229;
	double dz = 2.29754;
	double acc_single = 1 / 210.*sqrt(dx*dx + angle * angle*dz*dz);

	return acc_single;
}
void align_2plate(std::vector<mfile0::M_Chain>&c, int pl[2]) {
	std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> base_pair;

	std::pair < mfile0::M_Base, mfile0::M_Base >pair_tmp;
	bool flg[2] = { false, false };

	for (auto itr = c.begin(); itr != c.end(); itr++) {
		flg[0] = false;
		flg[1] = false;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (pl[0] == itr2->pos / 10) {
				pair_tmp.first = *itr2;
				flg[0] = true;
			}
			if (pl[1] == itr2->pos / 10) {
				pair_tmp.second = *itr2;
				flg[1] = true;
			}
		}
		if (flg[0] && flg[1]) {
			base_pair.push_back(pair_tmp);
		}
	}

	std::multimap<std::pair<int, int>, std::pair<mfile0::M_Base, mfile0::M_Base>> hash_pair;
	//1.5cm pich
	double hash_size = 300000;
	double x_min, y_min;
	std::pair<int, int> id;
	for (auto itr = base_pair.begin(); itr != base_pair.end(); itr++) {
		if (itr->second.x < 0) {
			id.first = itr->second.x / hash_size - 1;
		}
		else {
			id.first = itr->second.x / hash_size;
		}

		if (itr->second.y < 0) {
			id.second = itr->second.y / hash_size - 1;
		}
		else {
			id.second = itr->second.y / hash_size;
		}

		hash_pair.insert(std::make_pair(id, *itr));
	}

	std::ofstream ofs;
	int count = 0;
	int out_count = 0;
	std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> pair;
	for (auto itr = hash_pair.begin(); itr != hash_pair.end(); itr++) {
		printf("\r now ix:%4d iy:%4d", itr->first.first, itr->first.second);

		count = hash_pair.count(itr->first);
		std::stringstream file_out,file_out_new;
		file_out << "base_pair_"
			<< std::setw(5) << std::setfill('0') << itr->first.first
			<< "_"
			<< std::setw(5) << std::setfill('0') << itr->first.second
			<< ".txt";
		file_out_new << "base_pair_"
			<< std::setw(5) << std::setfill('0') << itr->first.first
			<< "_"
			<< std::setw(5) << std::setfill('0') << itr->first.second
			<< ".new.txt";

		ofs.open(file_out.str());
		auto range = hash_pair.equal_range(itr->first);
		pair.clear();
		for (auto res = range.first; res != range.second; res++) {
			pair.push_back(res->second);
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << res->second.first.pos / 10 << " "
				<< std::setw(8) << std::setprecision(4) << res->second.first.ax << " "
				<< std::setw(8) << std::setprecision(4) << res->second.first.ay << " "
				<< std::setw(8) << std::setprecision(1) << res->second.first.x << " "
				<< std::setw(8) << std::setprecision(1) << res->second.first.y << " "
				<< std::setw(8) << std::setprecision(1) << res->second.first.z << " "

				<< std::setw(4) << std::setprecision(0) << res->second.second.pos / 10 << " "
				<< std::setw(8) << std::setprecision(4) << res->second.second.ax << " "
				<< std::setw(8) << std::setprecision(4) << res->second.second.ay << " "
				<< std::setw(8) << std::setprecision(1) << res->second.second.x << " "
				<< std::setw(8) << std::setprecision(1) << res->second.second.y << " "
				<< std::setw(8) << std::setprecision(1) << res->second.second.z << std::endl;
		}
		ofs.close();


		align_param param = align_param_ini(itr->first, hash_size, pair.begin()->second.z);
		calc_align(param,pair);

		std::vector<track_difference>diff;
		make_track_diff(diff, pair, param);

		ofs.open(file_out_new.str());
		for (int i = 0; i < diff.size();i++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << pair[i].first.pos / 10 << " "
				<< std::setw(8) << std::setprecision(4) << diff[i].ax0 << " "
				<< std::setw(8) << std::setprecision(4) << diff[i].ay0 << " "
				<< std::setw(8) << std::setprecision(1) << diff[i].x0 << " "
				<< std::setw(8) << std::setprecision(1) << diff[i].y0 << " "
				<< std::setw(8) << std::setprecision(1) << diff[i].z0 << " "

				<< std::setw(4) << std::setprecision(0) << pair[i].second.pos / 10 << " "
				<< std::setw(8) << std::setprecision(4) << diff[i].ax1 << " "
				<< std::setw(8) << std::setprecision(4) << diff[i].ay1 << " "
				<< std::setw(8) << std::setprecision(1) << diff[i].x1 << " "
				<< std::setw(8) << std::setprecision(1) << diff[i].y1 << " "
				<< std::setw(8) << std::setprecision(1) << diff[i].z1 << std::endl;
		}
		ofs.close();


		itr = std::next(itr, count - 1);
	}
	printf("\r now ix:%4d iy:%4d", hash_pair.end()->first.first, hash_pair.end()->first.second);

	std::stringstream file_out;
	file_out << "base_pair.txt";
	ofs.open(file_out.str());

	for (auto itr = hash_pair.begin(); itr != hash_pair.end(); itr++) {


		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->second.first.pos / 10 << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.first.ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.first.ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.first.x << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.first.y << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.first.z << " "

			<< std::setw(4) << std::setprecision(0) << itr->second.second.pos / 10 << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.second.ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.second.ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.second.x << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.second.y << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.second.z << std::endl;
	}
	ofs.close();



}
align_param align_param_ini(std::pair<int, int>id, double hash_size,double z) {
	align_param param;
	param.ix = id.first;
	param.iy = id.second;
	param.x = id.first*hash_size + hash_size / 2;
	param.y = id.second*hash_size + hash_size / 2;
	param.z = z;

	param.dx = 0;
	param.dy = 0;
	param.dz = 0;
	param.x_rot = 0;
	param.y_rot = 0;
	param.z_rot = 0;
	param.x_shrink = 1;
	param.y_shrink = 1;
	param.z_shrink = 1;
	param.yx_shear = 0;
	param.zx_shear = 0;
	param.zy_shear = 0;

	param.signal = 0;
	return param;

}
void calc_align(align_param &param, std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> &base_pair) {

	std::vector<track_difference> diff;
	double shift_x, shift_y, shift_z, rotation, shrink;

	/////gap tune//////
	make_track_diff(diff, base_pair, param);
	calc_z_align(diff, shift_z);
	param.dz += shift_z;

	//////rot and shrink and shift tune ////
	make_track_diff(diff, base_pair, param);
	calc_position_align(diff, rotation, shrink, shift_x, shift_y);
	param.z_rot += rotation;
	make_track_diff(diff, base_pair, param);
	calc_position_align(diff, rotation, shrink, shift_x, shift_y);
	param.dx += shift_x;
	param.dy += shift_y;

	////angle shrink z/////
	make_track_diff(diff, base_pair, param);
	calc_angle_align(diff, shrink, shift_x, shift_y);
	param.z_shrink *= shrink;
	param.zx_shear += shift_x;
	param.zy_shear += shift_y;

	/////gap tune//////
	make_track_diff(diff, base_pair, param);
	calc_z_align(diff, shift_z);
	param.dz += shift_z;
	make_track_diff(diff, base_pair, param);
	calc_z_align(diff, shift_z);
	param.dz += shift_z;

}

void make_track_diff(std::vector<track_difference>&diff, std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> &base_pair,align_param &param) {
	diff.clear();
	for (auto itr = base_pair.begin(); itr != base_pair.end(); itr++) {
		track_difference diff_tmp;
		diff_tmp.ax0 = itr->first.ax;
		diff_tmp.ay0 = itr->first.ay;
		diff_tmp.x0 = itr->first.x;
		diff_tmp.y0 = itr->first.y;
		diff_tmp.z0 = itr->first.z;

		diff_tmp.ax1 = param.z_shrink*cos(param.z_rot)*itr->second.ax - param.z_shrink*sin(param.z_rot)*itr->second.ay + param.zx_shear;
		diff_tmp.ay1 = param.z_shrink*sin(param.z_rot)*itr->second.ax + param.z_shrink*cos(param.z_rot)*itr->second.ay + param.zy_shear;
		diff_tmp.x1 = param.x_shrink*cos(param.z_rot)*itr->second.x - param.y_shrink*sin(param.z_rot)*itr->second.y + param.dx;
		diff_tmp.y1 = param.x_shrink*sin(param.z_rot)*itr->second.x + param.y_shrink*cos(param.z_rot)*itr->second.y + param.dy;
		diff_tmp.z1 = itr->second.z+param.dz;

		diff.push_back(diff_tmp);
	}

}

//回転無し
void calc_angle_align(std::vector<track_difference> &pair, double &shrink, double &shift_x, double &shift_y) {

	double ax0ax1 = 0;
	double ay0ay1 = 0;
	double ax1_2 = 0;
	double ay1_2 = 0;
	double ax0 = 0;
	double ax1 = 0;
	double ay0 = 0;
	double ay1 = 0;

	int num = pair.size();
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		ax0ax1 += itr->ax0*itr->ax1;
		ay0ay1 += itr->ay0*itr->ay1;
		ax1_2 += itr->ax1*itr->ax1;
		ay1_2 += itr->ay1*itr->ay1;
		ax0 += itr->ax0;
		ax1 += itr->ax1;
		ay0 += itr->ay0;
		ay1 += itr->ay1;
	}
	shrink = (ax0ax1 - ax0 / num * ax1 + ay0ay1 - ay0 / num * ay1) / ((ax1_2)-(ax1*ax1 / num) + (ay1_2)-(ay1*ay1 / num));
	shift_x = (ax0 - shrink * ax1) / num;
	shift_y = (ay0 - shrink * ay1) / num;

	printf("shrink %.4lf shift-x %.6lf shift-y=%.6lf\n", shrink, shift_x, shift_y);

}
//回転無し
void calc_angle_align_sigma(std::vector<track_difference> &pair, double &shrink, double &shift_x, double &shift_y) {

	double ax0ax1 = 0;
	double ay0ay1 = 0;
	double ax1_2 = 0;
	double ay1_2 = 0;
	double ax0 = 0;
	double ax1 = 0;
	double ay0 = 0;
	double ay1 = 0;

	double sig_x = 0;
	double sig_y = 0;

	int num = pair.size();
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		ax0ax1 += itr->ax0 / itr->sigma_ax*itr->ax1 / itr->sigma_ax;
		ay0ay1 += itr->ay0 / itr->sigma_ay*itr->ay1 / itr->sigma_ay;
		ax1_2 += itr->ax1 / itr->sigma_ax*itr->ax1 / itr->sigma_ax;
		ay1_2 += itr->ay1 / itr->sigma_ay*itr->ay1 / itr->sigma_ay;
		ax0 += itr->ax0 / (itr->sigma_ax*itr->sigma_ax);
		ax1 += itr->ax1 / (itr->sigma_ax*itr->sigma_ax);
		ay0 += itr->ay0 / (itr->sigma_ay*itr->sigma_ay);
		ay1 += itr->ay1 / (itr->sigma_ay*itr->sigma_ay);

		sig_x += 1 / (itr->sigma_ax*itr->sigma_ax);
		sig_y += 1 / (itr->sigma_ay*itr->sigma_ay);
	}
	shrink = (ax0ax1 - ax0 / sig_x * ax1 + ay0ay1 - ay0 / sig_y * ay1) / ((ax1_2)-(ax1*ax1 / sig_x) + (ay1_2)-(ay1*ay1 / sig_y));
	shift_x = (ax0 - shrink * ax1) / sig_x;
	shift_y = (ay0 - shrink * ay1) / sig_y;

	//printf("shrink %.4lf shift-x %.6lf shift-y=%.6lf\n", shrink, shift_x, shift_y);

}
//回転無し,平行移動無し
void calc_z_align(std::vector<track_difference> &pair,   double &shift_z) {

	int num = pair.size();
	

	std::vector<double> ex_x[2], ex_y[2];
	double dx, dy;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		ex_x[0].push_back(itr->x0 + itr->ax0 * (itr->z1 - itr->z0) / 2);
		ex_x[1].push_back(itr->x1 + itr->ax1 * (itr->z0 - itr->z1) / 2);
		ex_y[0].push_back(itr->y0 + itr->ay0 * (itr->z1 - itr->z0) / 2);
		ex_y[1].push_back(itr->y1 + itr->ay1 * (itr->z0 - itr->z1) / 2);
	}

	double x0tanx = 0;
	double x1tanx = 0;
	double y0tany = 0;
	double y1tany = 0;
	double tanx2 = 0;
	double tany2 = 0;

	for (int i = 0; i < num; i++) {
		x0tanx += ex_x[0][i] * pair[i].ax1;
		x1tanx += ex_x[1][i] * pair[i].ax1;
		y0tany += ex_y[0][i] * pair[i].ay1;
		y1tany += ex_y[1][i] * pair[i].ay1;
		tanx2 += pair[i].ax1*pair[i].ax1;
		tany2 += pair[i].ay1*pair[i].ay1;

	}
	shift_z = -1*(x0tanx- x1tanx+ y0tany- y1tany) / (tanx2+ tany2);
	//printf("shift z: %g\n", shift_z);
}

void calc_position_align(std::vector<track_difference> &pair, double &rotation, double &shrink, double &shift_x, double &shift_y) {

	int num = pair.size();
	std::vector<double> ex_x[2], ex_y[2];
	double dx, dy;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		ex_x[0].push_back(itr->x0 + itr->ax0 * (itr->z1 - itr->z0) / 2);
		ex_x[1].push_back(itr->x1 + itr->ax1 * (itr->z0 - itr->z1) / 2);
		ex_y[0].push_back(itr->y0 + itr->ay0 * (itr->z1 - itr->z0) / 2);
		ex_y[1].push_back(itr->y1 + itr->ay1 * (itr->z0 - itr->z1) / 2);
	}

	double x0 = 0;
	double x1 = 0;
	double y0 = 0;
	double y1 = 0;
	double x2y2 = 0;
	double x0x1_y0y1 = 0;
	double y0x1_x0y1 = 0;
	for (int i = 0; i < num; i++) {
		x0 += ex_x[0][i];
		x1 += ex_x[1][i];
		y0 += ex_y[0][i];
		y1 += ex_y[1][i];

		x2y2 += ex_x[0][i] * ex_x[0][i] + ex_y[0][i] * ex_y[0][i];
		x0x1_y0y1 += ex_x[0][i] * ex_x[1][i] + ex_y[0][i] * ex_y[1][i];
		y0x1_x0y1 += ex_y[0][i] * ex_x[1][i] - ex_x[0][i] * ex_y[1][i];
	}

	double a, b, c, d;
	a = (x0*x1 + y0 * y1 - num * x0x1_y0y1) / (x0*x0 + y0 * y0 - num * x2y2);
	b = -1*(y0*x1 - x0 * y1 - num * y0x1_x0y1) / (x0*x0 + y0 * y0 - num * x2y2);
	c = (x1 -( a * x0 - b * y0)) / num;
	d = (y1 -( b * x0 + a * y0)) / num;

	rotation = atan(-1*b / a);
	shrink = sqrt(a*a + b * b);
	shift_x = -1*c;
	shift_y = -1*d;

	//printf("rot: %.4lf shrink %.4lf shift-x %.6lf shift-y=%.6lf\n", rotation, shrink, shift_x, shift_y);

}

std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> extra_center(std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> base_pair) {
	std::vector<std::pair<mfile0::M_Base, mfile0::M_Base>> ret;
	for (auto itr = base_pair.begin(); itr != base_pair.end(); itr++) {
		itr->first.x = itr->first.x + itr->first.ax*(itr->second.z - itr->first.z) / 2;
		itr->first.y = itr->first.y + itr->first.ay*(itr->second.z - itr->first.z) / 2;
		itr->second.x = itr->second.x + itr->second.ax*(itr->first.z - itr->second.z) / 2;
		itr->second.y = itr->second.y + itr->second.ay*(itr->first.z - itr->second.z) / 2;
		ret.push_back(*itr);
	}
	return ret;
}


void output_diff(std::string filename, std::vector<track_difference> &diff) {
	std::ofstream ofs(filename);

	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(4) << itr->ax0 << " "
			<< std::setw(8) << std::setprecision(4) << itr->ay0 << " "
			<< std::setw(8) << std::setprecision(1) << itr->x0 << " "
			<< std::setw(8) << std::setprecision(1) << itr->y0 << " "
			<< std::setw(8) << std::setprecision(1) << itr->z0 << " "
			<< std::setw(8) << std::setprecision(4) << itr->ax1 << " "
			<< std::setw(8) << std::setprecision(4) << itr->ay1 << " "
			<< std::setw(8) << std::setprecision(1) << itr->x1 << " "
			<< std::setw(8) << std::setprecision(1) << itr->y1 << " "
			<< std::setw(8) << std::setprecision(1) << itr->z1 << " "
			<< std::setw(8) << std::setprecision(6) << itr->sigma_ax << " "
			<< std::setw(8) << std::setprecision(6) << itr->sigma_ay << " "
			<< std::setw(8) << std::setprecision(3) << itr->sigma_px << " "
			<< std::setw(8) << std::setprecision(3) << itr->sigma_py << std::endl;
	}
}
void output_corrmap(std::string filename, std::vector<align_param> &corr) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (corr.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)", count, int(corr.size()), count*100. / corr.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(0) << itr->pl << " "
				<< std::setw(8) << std::setprecision(0) << itr->id << " "
				<< std::setw(4) << std::setprecision(0) << itr->ix << " "
				<< std::setw(4) << std::setprecision(0) << itr->iy << " "
				<< std::setw(4) << std::setprecision(0) << itr->signal << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << " "
				<< std::setw(8) << std::setprecision(1) << itr->z << " "
				<< std::setw(9) << std::setprecision(7) << itr->x_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->y_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->z_rot << " "
				<< std::setw(8) << std::setprecision(6) << itr->x_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->y_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->z_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->yx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zy_shear << " "
				<< std::setw(8) << std::setprecision(2) << itr->dx << " "
				<< std::setw(8) << std::setprecision(2) << itr->dy << " "
				<< std::setw(8) << std::setprecision(2) << itr->dz << std::endl;
		}
		fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)\n", count, int(corr.size()), count*100. / corr.size());

	}



}

