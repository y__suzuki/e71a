#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>
#include <filesystem>
#include <set>
#include <omp.h>


class output_format_micro {
public:
	int pos, view, imager, zone, isg, ph, vph, px;
};
class output_format_base {
public:
	int pl, rawid;
	double ax, ay, x, y, z;
	output_format_micro m[2];
};

class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};

std::map<int, int> read_viwer_check(std::string filename);
std::multimap<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>> connect_base_pair(std::vector<mfile0::M_Chain>&chains, std::map<int, int> &connect_map, std::multimap<int, vxx::base_track_t*> &read_base_list);
void read_base(std::string file_in_ECC, std::multimap<int, vxx::base_track_t*> &read_base_list);
std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_filename(std::string file_in_ECC);
std::vector<output_format_link> basetrack_pair_to_linket(std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&track_pair);
void output_pair_bin(std::string filename, std::vector<output_format_link>&link);

int use_thread(double ratio, bool output);


int main(int argc,char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file-in-mfile file-in-viwercheck file-in-ECC file_out\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_check = argv[2];
	std::string file_in_ECC = argv[3];
	std::string file_out = argv[4];


	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	std::map<int, int> connect_map = read_viwer_check(file_in_check);
	std::multimap<int, vxx::base_track_t*> read_base_list;

	std::multimap<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>>base_pair = connect_base_pair(m.chains, connect_map, read_base_list);
	read_base(file_in_ECC, read_base_list);

	//alignment paramerter read
	//これはポインタで参照されるためとっておく
	std::vector < std::pair<std::pair<int, int>, std::vector <corrmap_3d::align_param >>>all_align_param;
	//std::vector<std::pair< std::pair<int, int>, std::vector <corrmap_3d::align_param2 >>>all_align_param2;
	std::map< std::pair<int, int>, std::vector <corrmap_3d::align_param2 >>all_align_param2;
	std::vector<std::pair<std::pair<int, int>, std::string> >  files_in_align = Get_alignment_filename(file_in_ECC);

	//通常alinment pl0-->pl1 読みこみ
	int count = 0, all = files_in_align.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < files_in_align.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align read %d/%d", count, all);
		}
#pragma omp atomic
		count++;
		//隣接+1pekeだけ読む
		if (base_pair.count(files_in_align[i].first)==0)continue;

		std::vector <corrmap_3d::align_param > corr = corrmap_3d::read_ali_param(files_in_align[i].second, false);
#pragma omp critical
		all_align_param.push_back(std::make_pair(files_in_align[i].first, corr));
	}
	printf("\r corrmap align read %d/%d fin\n", count, all);

	//Delaunay3角形への分割
	count = 0, all = all_align_param.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < all_align_param.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align calc delaunay %d/%d", count, all);
		}
#pragma omp atomic
		count++;

		std::vector <corrmap_3d::align_param2 >corr2 = DelaunayDivide(all_align_param[i].second);
#pragma omp critical
		all_align_param2.insert(std::make_pair(all_align_param[i].first, corr2));

	}
	printf("\r corrmap align calc delaunay %d/%d fin\n", count, all);

	std::vector<output_format_link> link_all;

	for (auto itr = base_pair.begin(); itr != base_pair.end(); itr++) {
		std::vector<vxx::base_track_t> b0, b1;
		int count = base_pair.count(itr->first);
		auto range = base_pair.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			b0.push_back(res->second.first);
			b1.push_back(res->second.second);
		}
		if (all_align_param2.count(itr->first) == 0) {
			itr = std::next(itr, count - 1);
			continue;
		}
		std::vector <corrmap_3d::align_param2 >corr2 = all_align_param2.at(itr->first);
		////trackとdelaunay3角形の対応
		std::vector < std::pair<vxx::base_track_t*, corrmap_3d::align_param2*>> track_param = track_affineparam_correspondence(b1, corr2);
		////basetrackを変換
		corrmap_3d::trans_base_all(track_param);

		std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> link_tmp;
		for (int i = 0; i < b0.size(); i++) {
			link_tmp.push_back(std::make_pair(&b0[i], &b1[i]));
		}
		std::vector<output_format_link> link = basetrack_pair_to_linket(link_tmp);
		for (auto itr = link.begin(); itr != link.end(); itr++) {
			link_all.push_back(*itr);
		}

		itr = std::next(itr, count - 1);
	}

	output_pair_bin(file_out, link_all);


}
std::map<int, int> read_viwer_check(std::string filename) {
	std::map < int, int> ret;
	std::ifstream ifs(filename);
	int group, chain;
	while (ifs >> group >> chain) {
		ret.insert(std::make_pair(group, chain));
	}
	return ret;
}

std::multimap<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>> connect_base_pair(std::vector<mfile0::M_Chain>&chains,	std::map<int, int> &connect_map,std::multimap<int,vxx::base_track_t*> &read_base_list){
	std::multimap<int, mfile0::M_Chain> group;
	std::multimap<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>> ret;
	for (auto &c : chains) {
		group.insert(std::make_pair(c.basetracks.begin()->group_id, c));
	}

	for (auto itr = group.begin(); itr != group.end(); itr++) {
		int count = group.count(itr->first);
		if (connect_map.count(itr->first) == 0) {
			itr = std::next(itr, count - 1);
			continue;
		}
		int chainid = connect_map.at(itr->first);
		mfile0::M_Chain c1, c2;
		int muon_pl;
		auto range = group.equal_range(itr->first);
		int flg = 0;
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.chain_id == 0) {
				c1 = res->second;
				flg++;
			}
			if (res->second.chain_id == chainid) {
				c2 = res->second;
				flg++;
			}
			if (res->second.basetracks.begin()->flg_i[1] == 0) {
				muon_pl = res->second.pos1 / 10;
				flg++;
			}
		}
		if (flg != 3) {
			itr = std::next(itr, count - 1);
			continue;
		}

		vxx::base_track_t b1, b2;
		b1.pl = -1;
		b2.pl = -1;
		if (c1.pos1 / 10 <= muon_pl) {
			//forward
			b1.pl = c1.pos1 / 10;
			b1.rawid = c1.basetracks.rbegin()->rawid;
			for (auto&b : c2.basetracks) {
				if (b.pos / 10 > b1.pl){
					b2.pl = b.pos / 10;
					b2.rawid = b.rawid;
					break;
				}
			}
		}
		else if (c1.pos0 / 10 > muon_pl) {
			//backward
			b1.pl = c1.pos0 / 10;
			b1.rawid = c1.basetracks.begin()->rawid;
			for (auto&b : c2.basetracks) {
				if (b.pos / 10 >= b1.pl) {
					break;
				}
				b2.pl = b.pos / 10;
				b2.rawid = b.rawid;
			}
		}
		if (b1.pl < 0 || b2.pl < 0) {
			itr = std::next(itr, count - 1);
			continue;
		}

		if (b1.pl > b2.pl)std::swap(b1, b2);
		
		ret.insert(std::make_pair(std::make_pair(b1.pl, b2.pl), std::make_pair(b1, b2)));

		itr = std::next(itr, count - 1);
	}

	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		read_base_list.insert(std::make_pair(itr->second.first.pl, &itr->second.first));
		read_base_list.insert(std::make_pair(itr->second.second.pl, &itr->second.second));
		printf("%d %d\n", itr->first.first, itr->first.second);
	}
	return ret;

}


void read_base(std::string file_in_ECC, std::multimap<int, vxx::base_track_t*> &read_base_list) {

	vxx::BvxxReader br;
	int count = 0, all = read_base_list.size();
	for (auto itr = read_base_list.begin(); itr != read_base_list.end(); itr++) {
		printf("read %d/%d\n", count, all);
		count++;
		std::stringstream file_in_base;
		int pl = itr->first;
		int count = read_base_list.count(itr->first);
		file_in_base << file_in_ECC << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl
			<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";

		std::array<int, 2> index = {itr->second->rawid,itr->second->rawid+1 };//1234<=rawid<=5678であるようなものだけを読む。
		std::vector<vxx::base_track_t> base_v = br.ReadAll(file_in_base.str(), pl, 0, vxx::opt::index = index);
		base_v[0].z = 0;
		(*itr->second) = base_v[0];

	}
	printf("read %d/%d\n", count, all);

}


std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_filename(std::string file_in_ECC) {
	std::vector<std::pair<std::pair<int, int>, std::string> > ret;
	std::map<std::pair<int, int>, std::string> ret_map;

	std::string file_in_align_path = file_in_ECC + "\\Area0\\0\\align\\fine";
	std::filesystem::directory_iterator iter(file_in_align_path), end;
	std::error_code err;

	std::vector<std::string > file_names;
	for (; iter != end && !err; iter.increment(err)) {
		const std::filesystem::directory_entry entry = *iter;

		file_names.push_back(entry.path().string());
		//printf("%s\n", file_names.back().c_str());
	}
	for (int i = 0; i < file_names.size(); i++) {
		int length = file_names[i].size();
		if (file_names[i].substr(length - 4, 4) != ".txt")continue;
		if (file_names[i].substr(length - 18, 14) != "_interpolation")continue;

		int pl0 = std::stoi(file_names[i].substr(length - 25, 3));
		int pl1 = std::stoi(file_names[i].substr(length - 21, 3));

		ret_map.insert(std::make_pair(std::make_pair(pl0, pl1), file_names[i]));
	}
	for (auto itr = ret_map.begin(); itr != ret_map.end(); itr++) {
		ret.push_back(*itr);
	}
	return ret;
}



output_format_link output_format(vxx::base_track_t &t1, vxx::base_track_t &t2) {
	int NumberOfImager = 72;
	uint32_t ShotID;
	output_format_link l;
	l.b[0].pl = t1.pl;
	l.b[0].rawid = t1.rawid;
	l.b[0].ax = t1.ax;
	l.b[0].ay = t1.ay;
	l.b[0].x = t1.x;
	l.b[0].y = t1.y;
	l.b[0].z = t1.z;

	l.b[0].m[0].pos = t1.m[0].pos;
	l.b[0].m[0].zone = t1.m[0].zone;
	l.b[0].m[0].isg = t1.m[0].isg;
	l.b[0].m[0].ph = t1.m[0].ph / 10000;
	l.b[0].m[0].vph = t1.m[0].ph % 10000;
	ShotID = ((uint32_t)(uint16_t)t1.m[0].row << 16) | ((uint32_t)(uint16_t)t1.m[0].col);
	l.b[0].m[0].view = ShotID / NumberOfImager;
	l.b[0].m[0].imager = ShotID % NumberOfImager;
	l.b[0].m[0].px = -1;

	l.b[0].m[1].pos = t1.m[1].pos;
	l.b[0].m[1].zone = t1.m[1].zone;
	l.b[0].m[1].isg = t1.m[1].isg;
	l.b[0].m[1].ph = t1.m[1].ph / 10000;
	l.b[0].m[1].vph = t1.m[1].ph % 10000;
	ShotID = ((uint32_t)(uint16_t)t1.m[1].row << 16) | ((uint32_t)(uint16_t)t1.m[1].col);
	l.b[0].m[1].view = ShotID / NumberOfImager;
	l.b[0].m[1].imager = ShotID % NumberOfImager;
	l.b[0].m[1].px = -1;

	l.b[1].pl = t2.pl;
	l.b[1].rawid = t2.rawid;
	l.b[1].ax = t2.ax;
	l.b[1].ay = t2.ay;
	l.b[1].x = t2.x;
	l.b[1].y = t2.y;
	l.b[1].z = t2.z;

	l.b[1].m[0].pos = t2.m[0].pos;
	l.b[1].m[0].zone = t2.m[0].zone;
	l.b[1].m[0].isg = t2.m[0].isg;
	l.b[1].m[0].ph = t2.m[0].ph / 10000;
	l.b[1].m[0].vph = t2.m[0].ph % 10000;
	ShotID = ((uint32_t)(uint16_t)t2.m[0].row << 16) | ((uint32_t)(uint16_t)t2.m[0].col);
	l.b[1].m[0].view = ShotID / NumberOfImager;
	l.b[1].m[0].imager = ShotID % NumberOfImager;
	l.b[1].m[0].px = -1;

	l.b[1].m[1].pos = t2.m[1].pos;
	l.b[1].m[1].zone = t2.m[1].zone;
	l.b[1].m[1].isg = t2.m[1].isg;
	l.b[1].m[1].ph = t2.m[1].ph / 10000;
	l.b[1].m[1].vph = t2.m[1].ph % 10000;
	ShotID = ((uint32_t)(uint16_t)t2.m[1].row << 16) | ((uint32_t)(uint16_t)t2.m[1].col);
	l.b[1].m[1].view = ShotID / NumberOfImager;
	l.b[1].m[1].imager = ShotID % NumberOfImager;
	l.b[1].m[1].px = -1;

	l.Calc_difference();

	return l;
}

std::vector<output_format_link> basetrack_pair_to_linket(std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&track_pair) {
	std::vector<output_format_link> link;

	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		link.push_back(output_format(*(itr->first), *(itr->second)));
	}

	return link;
}
void output_format_link::Calc_difference() {

	dax = b[1].ax - b[0].ax;
	day = b[1].ay - b[0].ay;
	dx = b[1].x - b[0].x - (b[0].ax + b[1].ax) / 2 * (b[1].z - b[0].z);
	dy = b[1].y - b[0].y - (b[0].ay + b[1].ay) / 2 * (b[1].z - b[0].z);
	dar = (dax*b[0].ax + day * b[0].ay) / sqrt(b[0].ax*b[0].ax + b[1].ay*b[1].ay);
	dal = (dax*b[0].ay - day * b[0].ax) / sqrt(b[0].ax*b[0].ax + b[1].ay*b[1].ay);


	//dr,dlの計算
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = b[0].x;
	pos0.y = b[0].y;
	pos0.z = b[0].z;
	dir0.x = b[0].ax;
	dir0.y = b[0].ay;
	dir0.z = 1;
	pos1.x = b[1].x;
	pos1.y = b[1].y;
	pos1.z = b[1].z;
	dir1.x = b[1].ax;
	dir1.y = b[1].ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);


}
void output_pair_bin(std::string filename, std::vector<output_format_link>&link) {

	std::ofstream ofs(filename, std::ios::binary);
	int count = 0;
	for (auto l : link) {
		if (count % 10000 == 0) {
			printf("\r write linklet %10d/%10d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;
		ofs.write((char*)& l, sizeof(output_format_link));
	}

	printf("\r write linklet %10d/%10d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

}


int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
