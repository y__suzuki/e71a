#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>

class microtrack_inf {
public:
	int zone, pos, col, row, isg, ph, pixelnum, hitnum;
};

std::multimap<std::pair<int, int>, std::tuple<int, int, int>>read_base_id(std::string file_path, std::string filename, int num, int pl);
void read_micro(std::string file_path, std::string filename, int num, int pos, int zone0, int zone1, int area, std::multimap<std::pair<int, int>, std::tuple<int, int, int>>base_id[2][4], std::vector<vxx::micro_track_t>micro[2][4]);
void write_fvxx_inf(std::string filename, std::vector<vxx::micro_track_t>&m);

int main(int argc, char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-bvxx-folder pl area fvxx-folder out-file-path\n");
		fprintf(stderr, "*************[[[ caution ]]]***********\n");
		fprintf(stderr, "this program depend on zone structure\n");
		exit(1);
	}

	std::string file_in_bvxx_path = argv[1];
	int pl = std::stoi(argv[2]);
	int area = std::stoi(argv[3]);
	std::string path_in_fvxx = argv[4];
	std::string file_out_path = argv[5];

	//microtrackはbasetrack中のpos,zone / col,row,isgでuniqueになる。
	std::multimap<std::pair<int, int>, std::tuple<int, int, int>>base_id[2][4];
	for (int i = 0; i < 4; i++) {
		base_id[0][i] = read_base_id(file_in_bvxx_path, "thick", i, pl);
		base_id[1][i] = read_base_id(file_in_bvxx_path, "thin", i, pl);
	}

	//zone構造に依存する部分
	//fvxxのzoneは小さいほうを割り当てる
	std::vector<vxx::micro_track_t> microtrack[2][4];

	read_micro(path_in_fvxx, "thick", 0, pl * 10 + 1, area + 6 * 0, area + 6 * 1, area, base_id, microtrack);
	read_micro(path_in_fvxx, "thick", 0, pl * 10 + 2, area + 6 * 0, area + 6 * 2, area, base_id, microtrack);
	read_micro(path_in_fvxx, "thick", 1, pl * 10 + 1, area + 6 * 2, area + 6 * 3, area, base_id, microtrack);
	read_micro(path_in_fvxx, "thick", 1, pl * 10 + 2, area + 6 * 1, area + 6 * 3, area, base_id, microtrack);

	read_micro(path_in_fvxx, "thin", 0, pl * 10 + 1, area + 6 * 4, area + 6 * 5, area, base_id, microtrack);
	read_micro(path_in_fvxx, "thin", 0, pl * 10 + 2, area + 6 * 4, area + 6 * 6, area, base_id, microtrack);
	read_micro(path_in_fvxx, "thin", 1, pl * 10 + 1, area + 6 * 6, area + 6 * 7, area, base_id, microtrack);
	read_micro(path_in_fvxx, "thin", 1, pl * 10 + 2, area + 6 * 5, area + 6 * 7, area, base_id, microtrack);

	for (int i = 0; i < 4; i++) {
		std::stringstream file_out_file[2];
		file_out_file[0] << file_out_path << "\\micro_inf_thick_" << std::setw(1) << i;
		file_out_file[1] << file_out_path << "\\micro_inf_thin_" << std::setw(1) << i;
		write_fvxx_inf(file_out_file[0].str(), microtrack[0][i]);
		write_fvxx_inf(file_out_file[1].str(), microtrack[1][i]);
	}
}

std::multimap<std::pair<int, int>, std::tuple<int, int, int>>read_base_id(std::string file_path, std::string filename, int num, int pl) {
	//b020_thick_0
	std::stringstream bvxxname;
	bvxxname << file_path << "\\b" << std::setw(3) << std::setfill('0') << pl << "_" << filename << "_" << std::setw(1) << num << ".sel.vxx";

	
	std::multimap<std::pair<int, int>, std::tuple<int, int, int>> ret;

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(bvxxname.str(), pl, 0);
	std::map<int, vxx::micro_track_subset_t> pos0, pos1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		pos0.insert(std::make_pair(itr->m[0].rawid, itr->m[0]));
		pos1.insert(std::make_pair(itr->m[1].rawid, itr->m[1]));
	}
	for (auto itr = pos0.begin(); itr != pos0.end(); itr++) {
		ret.insert(std::make_pair(std::make_pair(itr->second.pos, itr->second.zone), std::make_tuple(itr->second.col, itr->second.row, itr->second.isg)));
	}
	for (auto itr = pos1.begin(); itr != pos1.end(); itr++) {
		ret.insert(std::make_pair(std::make_pair(itr->second.pos, itr->second.zone), std::make_tuple(itr->second.col, itr->second.row, itr->second.isg)));
	}

	return ret;
}
void read_micro(std::string file_path, std::string filename, int num, int pos, int zone0, int zone1, int area, std::multimap<std::pair<int, int>, std::tuple<int, int, int>>base_id[2][4], std::vector<vxx::micro_track_t>micro[2][4]) {
	//fxxx1_thick_0.vxx zone0=area,zone1=area+6

	std::set<std::tuple<int, int, int>> micro_id[2][4];
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 4; j++) {
			if (base_id[i][j].count(std::make_pair(pos, zone0)) != 0) {
				auto range = base_id[i][j].equal_range(std::make_pair(pos, zone0));
				for (auto itr = range.first; itr != range.second; itr++) {
					micro_id[i][j].insert(itr->second);
				}
			}
			if (base_id[i][j].count(std::make_pair(pos, zone1)) != 0) {
				auto range = base_id[i][j].equal_range(std::make_pair(pos, zone1));
				for (auto itr = range.first; itr != range.second; itr++) {
					micro_id[i][j].insert(itr->second);
				}
			}
		}
	}

	std::stringstream fvxxname;
	fvxxname << file_path << "\\f" << std::setw(4) << std::setfill('0') << pos << "_" << filename << "_" << std::setw(1) << num << ".vxx";

	//fvxx 96 byte/track
	//100Mtrack=10GB
	int read_track_num = 100 * 1000 * 1000;
	int microtrack_num = -1;
	int id_range = 0;

	std::tuple<int, int, int> id;

	while (microtrack_num != 0) {
		microtrack_num = 0;
		vxx::FvxxReader fr;

		std::array<int, 2> index = { id_range*read_track_num, (id_range + 1)*read_track_num };//1234<=rawid<=5678であるようなものだけを読む。
		//std::array<int, 2> index = { 0,1000000};//1234<=rawid<=5678であるようなものだけを読む。

		std::vector<vxx::micro_track_t> m;
		m.reserve(microtrack_num);
		m = fr.ReadAll(fvxxname.str(), pos, 0, vxx::opt::index = index);
		microtrack_num = m.size();
		id_range++;
		for (auto itr = m.begin(); itr != m.end(); itr++) {
			std::get<0>(id) = itr->col;
			std::get<1>(id) = itr->row;
			std::get<2>(id) = itr->isg;
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 4; j++) {
					if (micro_id[i][j].count(id) == 1) {
						itr->zone = (i * 4 + j) * 6 + area;
						micro[i][j].push_back(*itr);
					}
				}
			}
		}
		m.clear();
	}

	return;
}
void write_fvxx_inf(std::string filename, std::vector<vxx::micro_track_t>&m) {
	//suzuki code<--保存されていない
	//ret.px = m.ph2 + m.pixelnum * 100;
	//ret.py = m.hitnum;


	//yoshimoto code
	//int pc_ph()const { return num & 0x1f; }
	//int pc_vol()const { return vola; }
	//int pc_area()const { return num >> 5; }
	std::vector<microtrack_inf> m_inf;
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		int num = itr->px;
		int vola = itr->py;

		microtrack_inf m_tmp;
		m_tmp.zone = itr->zone;
		m_tmp.pos = itr->pos;
		m_tmp.col = itr->col;
		m_tmp.row = itr->row;
		m_tmp.isg = itr->isg;
		m_tmp.pixelnum = num >> 5;
		m_tmp.ph = num & 0x1f;
		m_tmp.hitnum = vola;
		m_inf.push_back(m_tmp);
	}

	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (m_inf.size() == 0) {
		fprintf(stderr, "target file ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = m_inf.size();
	for (int i = 0; i < m_inf.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& m_inf[i], sizeof(microtrack_inf));
		//printf("%d %d %d %d %d %d\n", m_inf[i].col, m_inf[i].row, m_inf[i].isg, m_inf[i].ph, m_inf[i].pixelnum, m_inf[i].hitnum);
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();
}


