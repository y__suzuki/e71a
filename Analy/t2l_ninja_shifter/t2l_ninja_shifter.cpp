#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "functions.lib")
#include <functions.hpp>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#include <picojson.h>


#include <filesystem>
class t2l_param {
public:
	std::string file_in_shifter_path, file_in_structure, file_in_align_path;
	std::string file_out_linklet_path;
	int pl0, pl1;
	double intercept_ax, intercept_ay, intercept_px, intercept_py;
	double slope_px, slope_py, slope_ax, slope_ay;
	double intercept_ar, intercept_al, intercept_pr, intercept_pl;
	double slope_pr, slope_pl, slope_ar, slope_al;
	double position_hash, angle_hash;
	double angle_max;
	void Print_all();
	void Print_sample();
};
class linklet_t {
public:
	int pos[2];
	double zproj;
	double dx, dy;
	double xc, yc;
	vxx::base_track_t b[2];

	void Calc_difference();
	// Below member is commented-out ( i.e. de-activated ) by default action.  
	// To access them, you need un-comment and give it to dump_linklet like --format 0 your-netscan-data-types-ui.h 
	//micro_track_t m[4];

};


double nominal_gap(std::string file_in_structure, int pl[2]);
std::string Set_file_read_bvxx_path(std::string file_ECC_path, int pl, int area);
std::string Set_file_read_ali_path(std::string file_align_path, int pl[2]);

std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>> Make_base_corrmap_pair(std::vector<vxx::base_track_t>&base, std::vector<corrmap0::Corrmap> &corr);
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> &corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap*> &corr_map, double hash_size);
void basetrack_trans_affine(std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>>&base_pairs, double nominal_z);

std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> connect_track_all(std::vector<vxx::base_track_t>&base0, std::vector<vxx::base_track_t>&base1, t2l_param &param);
std::vector<vxx::base_track_t*> connect_track(vxx::base_track_t &t, std::vector<vxx::base_track_t*> &connect_cand, t2l_param &param);
bool judge_connect_xy(vxx::base_track_t &t1, vxx::base_track_t &t2, t2l_param &param);
bool judge_connect_rl(vxx::base_track_t &t1, vxx::base_track_t &t2, t2l_param &param);
void Calc_position_difference(vxx::base_track_t &t1, vxx::base_track_t &t2, double &dr, double &dl);

std::vector<linklet_t> linklet_format_change(std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&base_pairs);
void write_linklet_txt(std::string filename, std::vector<linklet_t> &l);
void write_linklet_bin(std::string filename, std::vector<linklet_t> &l);
std::string Set_file_write_link_path(std::string file_link_path, int pl[2]);
t2l_param read_param_json(std::string filename);


int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg param-json pl0 pl1\n");
		fprintf(stderr, "json file smaeple\n");
		fprintf(stderr, "-----------------\n");
		t2l_param param;
		param.Print_sample();
		fprintf(stderr, "-----------------\n");
		exit(1);
	}
	int area = 0;
	std::string file_in_json = argv[1];

	int pl0 = std::stoi(argv[2]);
	int pl1 = std::stoi(argv[3]);

	int pl[2];
	pl[0] = std::min(pl0, pl1);
	pl[1] = std::max(pl0, pl1);

	t2l_param param = read_param_json(file_in_json);
	param.pl0 = pl[0];
	param.pl1 = pl[1];
	param.Print_all();
	//出力linkletファイル名
	std::string file_out_link = Set_file_write_link_path(param.file_out_linklet_path, pl);


	double gap = nominal_gap(param.file_in_structure, pl);
	//読み込みfile名設定
	std::string file_in_base[2];
	for (int i = 0; i < 2; i++) {
		file_in_base[i] = Set_file_read_bvxx_path(param.file_in_shifter_path, pl[i], area);
	}
	std::string file_in_ali = Set_file_read_ali_path(param.file_in_align_path, pl);


	//fileの存在確認・読み込み
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base[2];
	for (int i = 0; i < 2; i++) {
		if (!std::filesystem::exists(file_in_base[i])) {
			fprintf(stderr, "%s not exist\n", file_in_base[i].c_str());
			exit(1);
		}
		base[i] = br.ReadAll(file_in_base[i], pl[i], 0);
	}
	std::vector<corrmap0::Corrmap> corr;
	if (!std::filesystem::exists(file_in_ali)) {
		fprintf(stderr, "%s not exist\n", file_in_ali.c_str());
		exit(1);
	}
	corrmap0::read_cormap(file_in_ali, corr);

	//for (int i = 0; i < 10; i++) {
	//	printf("%d %5.4lf %5.4lf %8.1lf %8.1lf %8.1lf\n", base[1][i].rawid, base[1][i].ax, base[1][i].ay, base[1][i].x, base[1][i].y, base[1][i].z);
	//}

	//
	std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>> base_pairs = Make_base_corrmap_pair(base[1], corr);
	basetrack_trans_affine(base_pairs, gap);

	//for (int i = 0; i < 10; i++) {
	//	printf("%d %5.4lf %5.4lf %8.1lf %8.1lf %8.1lf\n", base[1][i].rawid, base[1][i].ax, base[1][i].ay, base[1][i].x, base[1][i].y, base[1][i].z);
	//}
	std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> connect_track_pair = connect_track_all(base[0], base[1], param);

	std::vector<linklet_t> link = linklet_format_change(connect_track_pair);
	write_linklet_bin(file_out_link, link);

}

//json fileでのparameter読み込み
t2l_param read_param_json(std::string filename) {

	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	t2l_param ret;
	picojson::object &all = v.get<picojson::object>();
	ret.file_in_shifter_path = all["file_in_shifter_path"].get<std::string>();
	ret.file_in_align_path = all["file_in_align_path"].get<std::string>();
	ret.file_in_structure = all["file_in_structure"].get<std::string>();
	ret.file_out_linklet_path = all["file_out_linklet_path"].get<std::string>();
	picojson::object &connect_param_angle = all["connect_param_angle"].get<picojson::object>();

	picojson::object &connect_param_position = all["connect_param_position"].get<picojson::object>();

	ret.intercept_ax = connect_param_angle["intercept_x"].get<double>();
	ret.intercept_ay = connect_param_angle["intercept_y"].get<double>();
	ret.intercept_ar = connect_param_angle["intercept_r"].get<double>();
	ret.intercept_al = connect_param_angle["intercept_l"].get<double>();
	ret.slope_ax = connect_param_angle["slope_x"].get<double>();
	ret.slope_ay = connect_param_angle["slope_y"].get<double>();
	ret.slope_ar = connect_param_angle["slope_r"].get<double>();
	ret.slope_al = connect_param_angle["slope_l"].get<double>();


	ret.intercept_px = connect_param_position["intercept_x"].get<double>();
	ret.intercept_py = connect_param_position["intercept_y"].get<double>();
	ret.intercept_pr = connect_param_position["intercept_r"].get<double>();
	ret.intercept_pl = connect_param_position["intercept_l"].get<double>();
	ret.slope_px = connect_param_position["slope_x"].get<double>();
	ret.slope_py = connect_param_position["slope_y"].get<double>();
	ret.slope_pr = connect_param_position["slope_r"].get<double>();
	ret.slope_pl = connect_param_position["slope_l"].get<double>();

	ret.position_hash = all["position_hash"].get<double>();
	ret.angle_hash = all["angle_hash"].get<double>();
	ret.angle_max = all["angle_max"].get<double>();

	return ret;
}
void t2l_param::Print_all() {

	printf("file_in_shifter_path : %s\n", file_in_shifter_path.c_str());
	printf("file_in_align_path : %s\n", file_in_align_path.c_str());
	printf("file_in_structure : %s\n", file_in_structure.c_str());
	printf("file_out_linklet_path : %s\n", file_out_linklet_path.c_str());


	printf("PL0 : %03d \n", pl0);
	printf("PL1 : %03d \n", pl1);

	printf("position hash : %g[um]\n", position_hash);
	printf("angle hash : %\g\n", angle_hash);
	printf("angle max : %\g\n", angle_max);

	printf("dax: %.5lf * tan\u03B8x + %.5lf\n", slope_ax, intercept_ax);
	printf("day: %.5lf * tan\u03B8y + %.5lf\n", slope_ay, intercept_ay);
	printf("dar: %.5lf * tan\u03B8 + %.5lf\n", slope_ar, intercept_ar);
	printf("dal: %.5lf * tan\u03B8 + %.5lf\n", slope_al, intercept_al);

	printf("dpx: %.1lf * tan\u03B8x + %.1lf\n", slope_px, intercept_px);
	printf("dpy: %.1lf * tan\u03B8y + %.1lf\n", slope_py, intercept_py);
	printf("dpr: %.1lf * tan\u03B8 + %.1lf\n", slope_pr, intercept_pr);
	printf("dpl: %.1lf * tan\u03B8 + %.1lf\n", slope_pl, intercept_pl);

}
void t2l_param::Print_sample() {
	printf("{\n");

	printf("\t\"file_in_shfiter_path\":\"I:/NINJA/E71a/ECC5\",\n");
	printf("\t\"file_in_align_path\":\"I:/NINJA/E71a/ECC5/Area0/0/align/fine\",\n");
	printf("\t\"file_in_structure\":\"I:/NINJA/E71a/ECC5/Area0/st/st.dat\",\n");

	printf("\t\"file_out_linklet_path\":\"I:/NINJA/E71a/work/suzuki/Linklet/t2l_3D\",\n");
	printf("\t\"position_hash\":2000,\n");
	printf("\t\"angle_hash\":0.2,\n");
	printf("\t\"angle_max\":10.0,\n");

	printf("\t\"connect_param_angle\":{\n");
	printf("\t\t\"slope_x\":0.15,\n");
	printf("\t\t\"intercept_x\":0.05,\n");
	printf("\t\t\"slope_y\":0.15,\n");
	printf("\t\t\"intercept_y\":0.05,\n");
	printf("\t\t\"slope_r\":0.15,\n");
	printf("\t\t\"intercept_r\":0.05,\n");
	printf("\t\t\"slope_l\":0,\n");
	printf("\t\t\"intercept_l\":0.05\n");
	printf("\t\}\n");

	printf("\t\"connect_param_position\":{\n");
	printf("\t\t\"slope_x\":150,\n");
	printf("\t\t\"intercept_x\":50,\n");
	printf("\t\t\"slope_y\":150,\n");
	printf("\t\t\"intercept_y\":50,\n");
	printf("\t\t\"slope_r\":150,\n");
	printf("\t\t\"intercept_r\":50,\n");
	printf("\t\t\"slope_l\":0,\n");
	printf("\t\t\"intercept_l\":50\n");
	printf("\t\}\n");

	printf("}\n");


}


std::string Set_file_read_bvxx_path(std::string file_ECC_path, int pl, int area) {
	std::stringstream file_in_base;
	file_in_base << file_ECC_path << "\\Area" << area << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	return file_in_base.str();

}
std::string Set_file_read_ali_path(std::string file_align_path, int pl[2]) {
	std::stringstream file_in_ali;
	file_in_ali << file_align_path << "\\ali_"
		<< std::setw(3) << std::setfill('0') << pl[0] << "_"
		<< std::setw(3) << std::setfill('0') << pl[1] << "_interpolation.txt";
	return file_in_ali.str();
}
double nominal_gap(std::string file_in_structure, int pl[2]) {

	//gap nominal read
	chamber1::Chamber chamber;
	chamber1::read_structure(file_in_structure, chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	if (z_map.count(pl[0]) + z_map.count(pl[1]) != 2) {
		fprintf(stderr, "exception nominal gap not found\n");
		exit(1);
	}
	//pl[0]のz=0の座標系
	return z_map[pl[1]] - z_map[pl[0]];
}

std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>> Make_base_corrmap_pair(std::vector<vxx::base_track_t>&base, std::vector<corrmap0::Corrmap> &corr) {
	double hash_size = 4000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap*> corr_map;
	corrmap_area_inverse(corr, corr_map, hash_size);

	std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>> base_pairs;

	int ix, iy;
	int count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (count % 100000 == 0) {
			printf("\r search corrmap %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
		}
		count++;
		std::pair<int, int> id;
		ix = (itr)->x / hash_size;
		iy = (itr)->y / hash_size;
		std::vector<corrmap0::Corrmap*> corr_list;
		int loop = 0;
		while (corr_list.size() <= 3) {
			loop++;
			for (int iix = -1 * loop; iix <= 1 * loop; iix++) {
				for (int iiy = -1 * loop; iiy <= 1 * loop; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (corr_map.count(id) == 0)continue;
					auto range = corr_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						corr_list.push_back(res->second);
					}
				}
			}
		}
		double dist;
		corrmap0::Corrmap* param;
		for (auto itr2 = corr_list.begin(); itr2 != corr_list.end(); itr2++) {
			double cx, cy;
			cx = ((*itr2)->areax[0] + (*itr2)->areax[1]) / 2;
			cy = ((*itr2)->areay[0] + (*itr2)->areay[1]) / 2;
			if (itr2 == corr_list.begin()) {
				dist = (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy);
				param = (*itr2);
			}
			if (dist > (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy)) {
				dist = (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy);
				param = (*itr2);
			}

		}
		base_pairs.push_back(std::make_pair(&(*itr), param));
	}
	printf("\r search corrmap %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

	return base_pairs;
}
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> &corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap*> &corr_map, double hash_size) {

	//hash_sizeはcorrmapのサイズより大きくする
	double cx, cy;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		double area[4], factor;
		area[0] = itr->areax[0] - itr->position[4];
		area[1] = itr->areax[1] - itr->position[4];
		area[2] = itr->areay[0] - itr->position[5];
		area[3] = itr->areay[1] - itr->position[5];

		factor = 1 / (itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);
		itr->areax[0] = factor * (area[0] * itr->position[3] - area[2] * itr->position[1]);
		itr->areay[0] = factor * (area[2] * itr->position[0] - area[0] * itr->position[2]);
		itr->areax[1] = factor * (area[1] * itr->position[3] - area[3] * itr->position[1]);
		itr->areay[1] = factor * (area[3] * itr->position[0] - area[1] * itr->position[2]);

		cx = (itr->areax[0] + itr->areax[1]) / 2;
		cy = (itr->areay[0] + itr->areay[1]) / 2;

		std::pair<int, int>id;
		id.first = cx / hash_size;
		id.second = cy / hash_size;
		corr_map.insert(std::make_pair(id, &(*itr)));
	}

}
void basetrack_trans_affine(std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>>&base_pairs, double nominal_z) {
	//position rot+shift+拡大
	//angle rot
	double x_tmp, y_tmp;
	double sqrt_det;
	double cos, sin;
	for (auto p : base_pairs) {
		sqrt_det = sqrt(p.second->position[0] * p.second->position[3] - p.second->position[1] * p.second->position[2]);
		cos = p.second->position[0] / sqrt_det;
		sin = -1 * p.second->position[1] / sqrt_det;
		x_tmp = p.first->x;
		y_tmp = p.first->y;
		p.first->x = x_tmp * p.second->position[0] + y_tmp * p.second->position[1] + p.second->position[4];
		p.first->y = x_tmp * p.second->position[2] + y_tmp * p.second->position[3] + p.second->position[5];
		x_tmp = p.first->ax;
		y_tmp = p.first->ay;
		p.first->ax = x_tmp * cos + y_tmp * -1 * sin + p.second->angle[4];
		p.first->ay = x_tmp * sin + y_tmp * cos + p.second->angle[5];

		p.first->z = nominal_z + p.second->dz;
	}
}


//接続関連
std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> connect_track_all(std::vector<vxx::base_track_t>&base0, std::vector<vxx::base_track_t>&base1, t2l_param &param) {

	std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> connected;
	const double hash_pos_size = param.position_hash;
	const double hash_ang_size = param.angle_hash;
	double min[4];
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		if (itr == base0.begin()) {
			min[0] = itr->x;
			min[1] = itr->y;
			min[2] = itr->ax;
			min[3] = itr->ay;
		}
		min[0] = std::min(min[0], itr->x);
		min[1] = std::min(min[1], itr->y);
		min[2] = std::min(min[2], itr->ax);
		min[3] = std::min(min[3], itr->ay);
		itr->z = 0;
	}

	std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t*>base_map;
	std::tuple<int, int, int, int> id;
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		std::get<0>(id) = (itr->x - min[0]) / hash_pos_size;
		std::get<1>(id) = (itr->y - min[1]) / hash_pos_size;
		std::get<2>(id) = (itr->ax - min[2]) / hash_ang_size;
		std::get<3>(id) = (itr->ay - min[3]) / hash_ang_size;
		base_map.insert(std::make_pair(id, &(*itr)));
	}

	//base1
	double ex_x, ex_y;
	double x_min, x_max, y_min, y_max;
	double ax_min, ax_max, ay_min, ay_max;
	double angle_acc_x, angle_acc_y;
	double position_acc_x, position_acc_y;
	int i_ax_min, i_ax_max, i_ay_min, i_ay_max;
	int i_x_min, i_x_max, i_y_min, i_y_max;
	std::vector<vxx::base_track_t*> connect_cand;
	int count = 0;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r connect track %10d/%10d (%4.1lf%%)", count, base1.size(), count*100. / base1.size());
		}
		count++;
		if (fabs(itr->ax) > param.angle_max)continue;
		if (fabs(itr->ay) > param.angle_max)continue;
		connect_cand.clear();
		//printf("%g %g %g %g\n", itr->ax, itr->ay, itr->x, itr->y);
		//allowance 
		angle_acc_x = param.intercept_ax + param.slope_ax*fabs(itr->ax);
		angle_acc_y = param.intercept_ay + param.slope_ay*fabs(itr->ay);
		position_acc_x = param.intercept_px + param.slope_px*fabs(itr->ax);
		position_acc_y = param.intercept_py + param.slope_py*fabs(itr->ay);
		//z=z1 --> (z0+z1)/2に外挿
		ex_x = itr->ax*(-1)*itr->z / 2 + itr->x;
		ex_y = itr->ay*(-1)*itr->z / 2 + itr->y;
		//中点に外挿-->位置ずれallowanceずらす-->角度ずれallowanceずらしてもう半分外挿
		x_min = ex_x - position_acc_x - (itr->ax - angle_acc_x)*itr->z / 2;
		x_max = ex_x + position_acc_x - (itr->ax + angle_acc_x)*itr->z / 2;
		y_min = ex_y - position_acc_y - (itr->ay - angle_acc_y)*itr->z / 2;
		y_max = ex_y + position_acc_y - (itr->ay + angle_acc_y)*itr->z / 2;
		//ang_center+-(angle_acc)*sigma
		ax_min = itr->ax - angle_acc_x;
		ax_max = itr->ax + angle_acc_x;
		ay_min = itr->ay - angle_acc_y;
		ay_max = itr->ay + angle_acc_y;

		//hash idに変換
		i_x_min = (x_min - min[0]) / hash_pos_size;
		i_x_max = (x_max - min[0]) / hash_pos_size;
		i_y_min = (y_min - min[1]) / hash_pos_size;
		i_y_max = (y_max - min[1]) / hash_pos_size;
		i_ax_min = (ax_min - min[2]) / hash_ang_size;
		i_ax_max = (ax_max - min[2]) / hash_ang_size;
		i_ay_min = (ay_min - min[3]) / hash_ang_size;
		i_ay_max = (ay_max - min[3]) / hash_ang_size;
		//printf("%d %d %d %d\n", i_x_min, i_x_max, i_y_min, i_y_max);
		//printf("%d %d %d %d\n", i_ax_min, i_ax_max, i_ay_min, i_ay_max);
		//hash mapの中から該当trackを探す
		for (int ix = i_x_min; ix <= i_x_max; ix++) {
			for (int iy = i_y_min; iy <= i_y_max; iy++) {
				for (int iax = i_ax_min; iax <= i_ax_max; iax++) {
					for (int iay = i_ay_min; iay <= i_ay_max; iay++) {
						std::get<0>(id) = ix;
						std::get<1>(id) = iy;
						std::get<2>(id) = iax;
						std::get<3>(id) = iay;
						if (base_map.count(id) == 0)continue;
						auto range = base_map.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							connect_cand.push_back(res->second);
						}
					}
				}
			}
		}

		connect_cand = connect_track(*itr, connect_cand, param);

		if (connect_cand.size() == 0)continue;
		for (auto itr2 = connect_cand.begin(); itr2 != connect_cand.end(); itr2++) {
			connected.push_back(std::make_pair((*itr2), &(*itr)));
		}
	}
	printf("\r connect track %10d/%10d (%4.1lf%%)\n", count, base1.size(), count*100. / base1.size());

	return connected;

}
std::vector<vxx::base_track_t*> connect_track(vxx::base_track_t &t, std::vector<vxx::base_track_t*> &connect_cand, t2l_param &param) {
	std::vector<vxx::base_track_t*> ret;
	for (auto itr = connect_cand.begin(); itr != connect_cand.end(); itr++) {
		if (judge_connect_xy(t, *(*itr), param) && judge_connect_rl(t, *(*itr), param)) {
			ret.push_back(*itr);
		}
	}
	return ret;
}
bool judge_connect_xy(vxx::base_track_t &t1, vxx::base_track_t &t2, t2l_param &param) {
	double angle, d_pos_x, d_pos_y, d_ang_x, d_ang_y;
	double all_pos_x, all_pos_y, all_ang_x, all_ang_y;

	all_ang_x = param.intercept_ax + param.slope_ax*fabs(t1.ax);
	all_ang_y = param.intercept_ay + param.slope_ay*fabs(t1.ay);

	all_pos_x = param.intercept_px + param.slope_px*fabs(t1.ax);
	all_pos_y = param.intercept_py + param.slope_py*fabs(t1.ay);

	d_pos_x = t2.x - t1.x - (t1.ax + t2.ax) / 2 * (t2.z - t1.z);
	d_pos_y = t2.y - t1.y - (t1.ay + t2.ay) / 2 * (t2.z - t1.z);

	d_ang_x = (t2.ax - t1.ax);
	d_ang_y = (t2.ay - t1.ay);

	if (fabs(d_ang_x) > fabs(all_ang_x))return false;
	if (fabs(d_ang_y) > fabs(all_ang_y))return false;

	if (fabs(d_pos_x) > fabs(all_pos_x))return false;
	if (fabs(d_pos_y) > fabs(all_pos_y))return false;

	return true;
}
bool judge_connect_rl(vxx::base_track_t &t1, vxx::base_track_t &t2, t2l_param &param) {
	double angle, d_pos_r, d_pos_l, d_ang_r, d_ang_l;
	double all_pos_r, all_pos_l, all_ang_r, all_ang_l;
	angle = sqrt(t1.ax*t1.ax + t1.ay*t1.ay);
	if (angle < 0.1)return true;

	all_ang_r = param.intercept_ar + param.slope_ar*angle;
	all_ang_l = param.intercept_al + param.slope_al*angle;
	all_pos_r = param.intercept_pr + param.slope_pr*angle;
	all_pos_l = param.intercept_pl + param.slope_pl*angle;


	d_ang_r = ((t2.ax - t1.ax)*t1.ax + (t2.ay - t1.ay)*t1.ay);
	d_ang_l = ((t2.ax - t1.ax)*t1.ay - (t2.ay - t1.ay)*t1.ax);
	if (fabs(d_ang_r) > fabs(all_ang_r)*angle)return false;
	if (fabs(d_ang_l) > fabs(all_ang_l)*angle)return false;

	Calc_position_difference(t1, t2, d_pos_r, d_pos_l);
	if (fabs(d_pos_r) > fabs(all_pos_r))return false;
	if (fabs(d_pos_l) > fabs(all_pos_l))return false;

	return true;
}
void Calc_position_difference(vxx::base_track_t &t1, vxx::base_track_t &t2, double &dr, double &dl) {
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = t1.x;
	pos0.y = t1.y;
	pos0.z = t1.z;
	dir0.x = t1.ax;
	dir0.y = t1.ay;
	dir0.z = 1;
	pos1.x = t2.x;
	pos1.y = t2.y;
	pos1.z = t2.z;
	dir1.x = t2.ax;
	dir1.y = t2.ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);

}

std::vector<linklet_t> linklet_format_change(std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&base_pairs) {
	std::vector<linklet_t> link;

	for (auto itr = base_pairs.begin(); itr != base_pairs.end(); itr++) {
		linklet_t l;
		l.b[0] = *(itr->first);
		l.b[1] = *(itr->second);
		l.Calc_difference();
		link.push_back(l);
	}
	return link;
}
void linklet_t::Calc_difference() {
	pos[0] = b[0].pl * 10;
	pos[1] = b[1].pl * 10;
	zproj = (b[0].z + b[1].z) / 2;
	dx = b[0].x - b[1].x - (b[0].ax + b[1].ax) / 2 * (b[1].z - b[0].z);
	dy = b[0].y - b[1].y - (b[0].ay + b[1].ay) / 2 * (b[1].z - b[0].z);
	Calc_position_difference(b[0], b[1], xc, yc);
}
void write_linklet_txt(std::string filename, std::vector<linklet_t> &l) {
	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (l.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = l.begin(); itr != l.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)", count, int(l.size()), count*100. / l.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(6) << itr->pos[0] << " "
				<< std::setw(15) << itr->b[0].rawid << " "
				<< std::setw(6) << itr->pos[1] << " "
				<< std::setw(15) << itr->b[1].rawid
				<< std::setw(8) << itr->b[0].m[0].ph + itr->b[0].m[1].ph << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[0].ax << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[0].ay << " "
				<< std::setw(10) << std::setprecision(1) << itr->b[0].x << " "
				<< std::setw(10) << std::setprecision(1) << itr->b[0].y << " "
				<< std::setw(8) << itr->b[1].m[0].ph + itr->b[1].m[1].ph << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[1].ax << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[1].ay << " "
				<< std::setw(10) << std::setprecision(1) << itr->b[1].x << " "
				<< std::setw(10) << std::setprecision(1) << itr->b[1].y << " "
				<< std::setw(11) << std::setprecision(1) << itr->b[0].z << " "
				<< std::setw(11) << std::setprecision(1) << itr->b[1].z << " "
				<< std::setw(11) << std::setprecision(1) << itr->zproj << " "
				<< std::setw(10) << std::setprecision(2) << itr->xc << " "
				<< std::setw(10) << std::setprecision(2) << itr->yc << " "
				<< std::setw(6) << itr->b[0].m[0].pos << " "
				<< std::setw(8) << itr->b[0].m[0].col << " "
				<< std::setw(10) << itr->b[0].m[0].row << " "
				<< std::setw(3) << itr->b[0].m[0].zone << " "
				<< std::setw(7) << itr->b[0].m[0].isg << " "
				<< std::setw(6) << itr->b[0].m[1].pos << " "
				<< std::setw(8) << itr->b[0].m[1].col << " "
				<< std::setw(10) << itr->b[0].m[1].row << " "
				<< std::setw(3) << itr->b[0].m[1].zone << " "
				<< std::setw(7) << itr->b[0].m[1].isg << " "
				<< std::setw(6) << itr->b[1].m[0].pos << " "
				<< std::setw(8) << itr->b[1].m[0].col << " "
				<< std::setw(10) << itr->b[1].m[0].row << " "
				<< std::setw(3) << itr->b[1].m[0].zone << " "
				<< std::setw(7) << itr->b[1].m[0].isg << " "
				<< std::setw(6) << itr->b[1].m[1].pos << " "
				<< std::setw(8) << itr->b[1].m[1].col << " "
				<< std::setw(10) << itr->b[1].m[1].row << " "
				<< std::setw(3) << itr->b[1].m[1].zone << " "
				<< std::setw(7) << itr->b[1].m[1].isg << " "
				<< "0" << " " << "0.0" << " "
				<< std::setw(15) << itr->b[0].m[0].rawid << " "
				<< std::setw(8) << itr->b[0].m[0].ph << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[0].m[0].ax << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[0].m[0].ay << " "
				<< std::setw(15) << itr->b[0].m[1].rawid << " "
				<< std::setw(8) << itr->b[0].m[1].ph << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[0].m[1].ax << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[0].m[1].ay << " "
				<< std::setw(15) << itr->b[1].m[0].rawid << " "
				<< std::setw(8) << itr->b[1].m[0].ph << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[1].m[0].ax << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[1].m[0].ay << " "
				<< std::setw(15) << itr->b[1].m[1].rawid << " "
				<< std::setw(8) << itr->b[1].m[1].ph << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[1].m[1].ax << " "
				<< std::setw(8) << std::setprecision(4) << itr->b[1].m[1].ay << " "
				<< std::setw(10) << std::setprecision(1) << itr->dx << " "
				<< std::setw(10) << std::setprecision(1) << itr->dy << std::endl;
		}
		fprintf(stderr, "\r Write Linklet ... %d/%d (%4.1lf%%)\n", count, int(l.size()), count*100. / l.size());
	}
	ofs.close();

}
void write_linklet_bin(std::string filename, std::vector<linklet_t> &l) {
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (l.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = l.size();
	for (int i = 0; i < l.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& l[i], sizeof(linklet_t));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();
}
std::string Set_file_write_link_path(std::string file_link_path, int pl[2]) {
	std::stringstream file_out_link;
	file_out_link << file_link_path << "\\l_"
		<< std::setw(3) << std::setfill('0') << pl[0] << "_"
		<< std::setw(3) << std::setfill('0') << pl[1] << ".bll";
	return file_out_link.str();
}
