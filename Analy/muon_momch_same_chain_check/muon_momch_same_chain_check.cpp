#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void check_same_chain(std::vector<Momentum_recon::Mom_chain>&momch);

int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:file_in_momch file_out_momch \n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	check_same_chain(momch);
}
void check_same_chain(std::vector<Momentum_recon::Mom_chain>&momch) {
	std::map<int, std::set<std::pair<int, int>>> base_list;
	std::set<std::pair<int, int>> pair_fin;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		int eventid = itr->groupid;
		std::set<std::pair<int, int>> b_map;
		std::pair<int, int>id;
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			id.first = itr2->pl;
			id.second = itr2->rawid;
			b_map.insert(id);
			//printf("%d %d %d\n", eventid, id.first, id.second);

			for (auto itr3 = base_list.begin(); itr3 != base_list.end(); itr3++) {
				if (itr3->second.count(id) == 1) {
					if(pair_fin.count(std::make_pair(eventid, itr3->first)) == 0) {
						printf("%d - %d\n", eventid, itr3->first);
						pair_fin.insert(std::make_pair(eventid, itr3->first));
					}
				}
			}
		}
		base_list.insert(std::make_pair(eventid, b_map));
	}


}