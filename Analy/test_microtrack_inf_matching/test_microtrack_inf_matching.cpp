#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <ios>
#include <map>
#include <iomanip> 

class microtrack_inf {
public:
	int zone, pos, col, row, isg, ph, pixelnum, hitnum;
};
std::vector<microtrack_inf> read_microtrack_inf(std::string filename);
void matching(std::vector<microtrack_inf>&m0, std::vector<microtrack_inf>&m1);

int main(int argc, char **argv) {

	if (argc != 3) {
		fprintf(stderr, "usage:\n");
		exit(1);
	}

	std::string file_in_fvxx_inf[2];
	file_in_fvxx_inf[0] = argv[1];
	file_in_fvxx_inf[1] = argv[2];

	std::vector<microtrack_inf> m0 = read_microtrack_inf(file_in_fvxx_inf[0]);
	std::vector<microtrack_inf> m1 = read_microtrack_inf(file_in_fvxx_inf[1]);
	matching(m0, m1);

}

std::vector<microtrack_inf> read_microtrack_inf(std::string filename) {
	std::vector<microtrack_inf> ret;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	microtrack_inf m;
	while (ifs.read((char*)& m, sizeof(microtrack_inf))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(m);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;
}

void matching(std::vector<microtrack_inf>&m0, std::vector<microtrack_inf>&m1) {

	std::map<std::tuple<int, int, int>, microtrack_inf*> m1_map;
	for (auto itr = m1.begin(); itr != m1.end(); itr++) {
		m1_map.insert(std::make_pair(std::make_tuple(itr->col, itr->row, itr->isg), &(*itr)));
	}

	int all = m0.size();
	int count = 0;
	for (auto itr = m0.begin(); itr != m0.end(); itr++) {
		auto res = m1_map.find(std::make_tuple(itr->col, itr->row, itr->isg));
		if (res == m1_map.end()) {
			printf("error fvxx not found [%d,%d,%d]\n", itr->col, itr->row, itr->isg);
		}

		if (itr->hitnum != res->second->hitnum) printf("hitnum %d %d\n", itr->hitnum, res->second->hitnum);
		if (itr->ph != res->second->ph) printf("ph %d %d\n", itr->ph, res->second->ph);
		if (itr->pixelnum != res->second->pixelnum) printf("pixelnum %d %d\n", itr->pixelnum, res->second->pixelnum);
		if (itr->pos != res->second->pos) printf("pos %d %d\n", itr->pos, res->second->pos);
		//if (itr->zone != res->second->zone) printf("zone %d %d\n", itr->zone, res->second->zone);

		count++;
	}

	printf("matching %d %d\n", count, all);
}