#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>
double Calc_mom_Chain(mfile0::M_Chain &c,bool output=false);
double angle_accuracy(double angle);
std::vector<double> FeECC_angle_diff(mfile0::M_Chain &c, int fe);
std::vector<double> waterECC_angle_diff(mfile0::M_Chain &c, int water, int fe);
double calc_angle_diff_lat(mfile0::M_Base b0, mfile0::M_Base b1);
double calc_sqrt_radiation_length_ratio(int fe, int water, double angle);
void calc_RMS_error(std::vector<double> data, double&rms, double &error);
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold);
void Fit_Linear(std::vector<std::tuple<double, double, double>> angle_diff_rms, double ang_acc, double &slope);
double Calc_VPH_average(mfile0::M_Chain &c);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "udage:prg input-mfile output-file\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_name = argv[2];
	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	//m.chains = chain_dlat_selection(m.chains, 0.01);
	int count = 0;
	double mom, vph_ave;
	std::ofstream ofs(file_out_name);
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r calc mom %d/%d(%4.1lf%%)", count, m.chains.size(), count*100. / m.chains.size());
		}
		count++;
		mom = Calc_mom_Chain(*itr);
		vph_ave = Calc_VPH_average(*itr);
		if (mom > 0) {
			ofs << std::right << std::fixed
				<< std::setw(12) << std::setprecision(0) << itr->chain_id << " "
				<< std::setw(12) << std::setprecision(4) << mom << " "
				<< std::setw(12) << std::setprecision(2) << vph_ave << std::endl;
		}
	}
	fprintf(stderr, "\r calc mom %d/%d(%4.1lf%%)\n", count, m.chains.size(), count*100. / m.chains.size());
}
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (mfile0::angle_diff_dev_lat(*itr, ax, ay) < threshold)continue;
		ret.push_back(*itr);
	}
	printf("chain lateral selection >= %5.4lf : %d --> %d (%4.1lf%%)\n", threshold, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;

}
double Calc_mom_Chain(mfile0::M_Chain &c,bool output) {
	//pl�Ԃ̊p�x�����v�Z
	double angle = sqrt(pow(mfile0::chain_ax(c), 2) + pow(mfile0::chain_ay(c), 2));
	//Fe,water
	std::map<std::tuple<int, int>, std::vector<double>> angle_diff;
	//���v���̍ŏ�
	int stat_min = 4;

	//Fe ecc�̊p�x��
	angle_diff.insert(std::make_pair(std::make_tuple(1, 0), FeECC_angle_diff(c, 1)));
	for (int i = 2; i <= 5; i++) {
		std::vector<double> each_angle_diff = FeECC_angle_diff(c, i);
		if (each_angle_diff.size() < stat_min)continue;
		angle_diff.insert(std::make_pair(std::make_tuple(i, 0), each_angle_diff));
	}
	//water ECC�̊p�x��
	std::vector<double> each_angle_diff;
	for (int i = 1; i < c.basetracks.size() / 2; i++) {
		each_angle_diff = waterECC_angle_diff(c, i, i - 1);
		if (i == 1) {
			auto res = angle_diff.insert(std::make_pair(std::make_tuple(i, i - 1), each_angle_diff));
			if (!res.second) {
				for (auto itr = each_angle_diff.begin(); itr != each_angle_diff.end(); itr++) {
					res.first->second.push_back(*itr);
				}
			}
			if (res.first->second.size() < stat_min) {
				res.first->second.clear();
				angle_diff.erase(res.first->first);
			}
		}
		else {
			if (each_angle_diff.size() >= stat_min) {
				angle_diff.insert(std::make_pair(std::make_tuple(i, i - 1), each_angle_diff));
			}
		}
		each_angle_diff = waterECC_angle_diff(c, i, i);
		if (each_angle_diff.size() >= stat_min) {
			angle_diff.insert(std::make_pair(std::make_tuple(i, i), each_angle_diff));
		}
		each_angle_diff = waterECC_angle_diff(c, i - 1, i);
		if (each_angle_diff.size() >= stat_min) {
			angle_diff.insert(std::make_pair(std::make_tuple(i - 1, i), each_angle_diff));
		}
	}

	std::vector<std::tuple<double,double,double>> angle_diff_rms;
	double rms, error,length;
	for (auto itr = angle_diff.begin(); itr != angle_diff.end(); itr++) {
		calc_RMS_error(itr->second, rms, error);
		length = calc_sqrt_radiation_length_ratio(std::get<0>(itr->first), std::get<1>(itr->first), angle);
		angle_diff_rms.push_back(std::make_tuple(length, rms, error));
	}
	double slope;
	Fit_Linear(angle_diff_rms, angle_accuracy(angle), slope);

	if (output) {
		std::ofstream ofs("out.txt", std::ios::app);
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << c.chain_id << " "
			<< std::setw(3) << std::setprecision(0) << angle_diff_rms.size() << " "
			<< std::setw(8) << std::setprecision(6) << angle_accuracy(angle) << " "
			<< std::setw(8) << std::setprecision(6) << slope << std::endl;

		for (auto itr = angle_diff_rms.begin(); itr != angle_diff_rms.end(); itr++) {
			ofs << std::right << std::fixed
				<< std::setw(12) << std::setprecision(6) << std::get<0>(*itr) << " "
				<< std::setw(12) << std::setprecision(6) << std::get<1>(*itr) << " "
				<< std::setw(12) << std::setprecision(6) << std::get<2>(*itr) << std::endl;
		}
	}
	//slope=(13.6/pb)^2
	double pb = 0;
	if (slope < 0) {
		pb = -1;
	}
	else if (angle_diff_rms.size() < 3) {
		pb = -2;
	}
	else {
		pb = 13.6 / sqrt(slope);
	}
	return pb;
}
double angle_accuracy(double angle) {
	//�p�x���x��Ԃ�
	if (angle < 1.5) {
		return 0.002;
	}
	return 0.0038;
}
std::vector<double> FeECC_angle_diff(mfile0::M_Chain &c, int fe) {
	std::vector<double> ret;
	int pl;
	bool flg = false;

	for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); ) {
		flg = false;
		if (16 <= itr->pos / 10)break;
		if (3 >= itr->pos / 10) {
			itr++;
			continue;
		}
		for (int skip = 1; skip <= fe; skip++) {
			if ((itr + skip) == c.basetracks.end())break;
			auto next = std::next(itr, skip);
			if (next->pos / 10 >= 16)break;
			if (((next)->pos - itr->pos) / 10 == fe) {
				ret.push_back(calc_angle_diff_lat(*itr, *next));
				flg = true;
				itr = next;
				break;
			}
		}
		if (!flg) {
			itr++;
		}
	}
	return ret;


}
std::vector<double> waterECC_angle_diff(mfile0::M_Chain &c, int fe, int water) {
	std::vector<double> ret;
	int pl;
	bool flg = false;

	for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); ) {
		flg = false;
		for (int skip = 1; skip <= water+fe; skip++) {
			if ((itr + skip) == c.basetracks.end())break;
			auto next = std::next(itr, skip);
			if (((next)->pos - itr->pos) / 10 == fe+water) {
				pl = itr->pos / 10;
				if (water > fe) {
					if (16 <= pl && pl % 2 == 1) flg = true;
				}
				else if (water < fe) {
					if (16 <= pl && pl % 2 == 0) flg = true;
				}
				else {
					if (16 <= pl)flg = true;
				}
				if (flg) {
					ret.push_back(calc_angle_diff_lat(*itr, *next));
					itr = next;
					break;
				}
			}
		}
		if (!flg) {
			itr++;
		}
	}
	return ret;

}
double calc_angle_diff_lat(mfile0::M_Base b0, mfile0::M_Base b1) {
	double angle, d_lat;
	angle = sqrt(b0.ax*b0.ax + b0.ay*b0.ay);
	d_lat = ((b1.ax - b0.ax)*b0.ay - (b1.ay - b0.ay)*b0.ax) / angle;
	return d_lat;
}
void calc_RMS_error(std::vector<double> data, double&rms, double &error) {
	double sum2 = 0, sum = 0;
	int count = 0;
	for (auto itr = data.begin(); itr != data.end(); itr++) {
		sum2 += (*itr)*(*itr);
		sum += (*itr);
		count++;
	}
	rms = sqrt(sum2 / count - pow((sum / count), 2));
	error = rms / sqrt(2 * (count - 1));
}
double calc_sqrt_radiation_length_ratio(int fe, int water, double angle) {
	//�e������radiation length(cm)
	double water_rad = 36.08;
	double iron_rad = 1.758;
	double emulsion_rad = 3.03;
	double base_rad = 41.31;

	//pass length(cm)
	double pass_length = sqrt(1 + angle * angle);
	double water_length = 0.23*water*pass_length;
	double iron_length = 0.05*fe*pass_length;
	double emulsion_length = 0.007*(fe + water) * 2 * pass_length;
	double base_length = 0.021*(fe + water)  * pass_length;

	//radiation length ratio
	double rad_length = water_length / water_rad + iron_length / iron_rad + emulsion_length / emulsion_rad + base_length / base_rad;
	return sqrt(rad_length);
}
void Fit_Linear(std::vector<std::tuple<double, double, double>> angle_diff_rms, double ang_acc, double &slope) {
	double x = 0, xy = 0, xx = 0;
	double x_fix, y_fix, y_fix_err;
	//rad_length ^ 2 * (1 + 0.038*log(rad_length ^ 2)) ^ 2
	for (auto itr = angle_diff_rms.begin(); itr != angle_diff_rms.end(); itr++) {
		x_fix = pow(std::get<0>(*itr)*(1 + 0.038*log(pow(std::get<0>(*itr), 2))), 2);
		y_fix = pow(std::get<1>(*itr), 2);
		y_fix_err = 2*std::get<2>(*itr)*std::get<2>(*itr);
		x += x_fix / y_fix_err;
		xy+=x_fix*y_fix/ y_fix_err;
		xx += x_fix * x_fix / y_fix_err;
	}
	//slope=(13.6/pb)^2
	slope = (xy - ang_acc*ang_acc * x) / xx;
}
double Calc_VPH_average(mfile0::M_Chain &c) {
	double vph = 0;
	for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
		vph += itr->ph % 10000;
	}
	vph = vph / c.basetracks.size();
	return vph;
}