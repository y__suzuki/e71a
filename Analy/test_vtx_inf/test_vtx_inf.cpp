#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
# include <iostream>
# include <random>
#include <set>


class stop_track {
public:
	uint64_t chainid, groupid;
	int  nseg, npl, pl0, pl1, ph, rawid;
	double ax, ay, x, y, z;
};
class track_pair {
public:
	int pl0, pl1, raw0, raw1;
	double x, y, z, md, oa;
};
class track_multi {
public:
	int eventid, pl;
	double x, y, z;
	std::vector< std::pair<double, stop_track>> trk;
	std::vector<track_pair>pair;
};
void read_vtx_file(std::string filename, std::vector<track_multi>&multi);
std::vector<track_multi> bwd_attach(std::vector<track_multi>&multi);
	void output_vtx(std::string filename, std::vector<track_multi> vtx);

int main(int argc, char** argv) {
	if (argc != 3) {
		fprintf(stderr, "usage\n");
		exit(1);
	}

	std::string in_file_vtx = argv[1];
	std::string out_file_vtx = argv[2];

	std::vector<track_multi> multi;
	read_vtx_file(in_file_vtx, multi);
	std::vector<track_multi> multi_add = bwd_attach(multi);
	output_vtx(out_file_vtx, multi_add);
}

void read_vtx_file(std::string filename, std::vector<track_multi>&multi) {
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0, trk_num;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);

		track_multi m;
		m.eventid = std::stoi(str_v[0]);
		m.pl = std::stoi(str_v[1]);
		m.x = std::stod(str_v[3]);
		m.y = std::stod(str_v[4]);
		m.z = std::stod(str_v[5]);
		trk_num = std::stoi(str_v[2]);

		for (int i = 0; i < trk_num*(trk_num - 1) / 2; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			track_pair p;
			p.pl0 = std::stoi(str_v[0]);
			p.raw0 = std::stoi(str_v[1]);
			p.pl1 = std::stoi(str_v[2]);
			p.raw1 = std::stoi(str_v[3]);
			p.x = std::stod(str_v[4]);
			p.y = std::stod(str_v[5]);
			p.z = std::stod(str_v[6]);
			p.oa = std::stod(str_v[7]);
			p.md = std::stod(str_v[8]);
			m.pair.push_back(p);
		}
		for (int i = 0; i < trk_num; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			stop_track s;
			s.pl0 = std::stoi(str_v[0]);
			s.pl1 = std::stoi(str_v[1]);
			s.rawid = std::stoi(str_v[2]);
			s.chainid = std::stoll(str_v[3]);
			s.groupid = std::stoll(str_v[4]);
			s.nseg = std::stoi(str_v[5]);
			s.npl = std::stoi(str_v[6]);
			s.ph = std::stoi(str_v[7]);
			s.ax = std::stod(str_v[8]);
			s.ay = std::stod(str_v[9]);
			s.x = std::stod(str_v[10]);
			s.y = std::stod(str_v[11]);
			s.z = 0;
			double ip = std::stod(str_v[12]);
			m.pl = s.pl1;

			m.trk.push_back(std::make_pair(ip, s));
		}

		multi.push_back(m);
	}

}
std::vector<track_multi> bwd_attach(std::vector<track_multi>&multi) {
	std::vector<track_multi>ret;
	bool flg = false;
	int pl = 0;
	for (auto itr = multi.begin(); itr != multi.end(); itr++) {
		pl = itr->pl;
		flg = false;
		for (auto itr2 = itr->trk.begin(); itr2 != itr->trk.end(); itr2++) {
			if (itr2->second.pl1 != pl)flg = true;
		}
		if (flg) {
			ret.push_back(*itr);
		}

	}
	printf("%d --> %d\n", multi.size(),ret.size());
	return ret;
}
void output_vtx(std::string filename, std::vector<track_multi> vtx) {
	std::ofstream ofs(filename);
	for (auto itr0 = vtx.begin(); itr0 != vtx.end(); itr0++) {
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr0->eventid << " "
			<< std::setw(4) << std::setprecision(0) << itr0->pl << " "
			<< std::setw(4) << std::setprecision(0) << itr0->trk.size() << " "
			<< std::setw(8) << std::setprecision(1) << itr0->x << " "
			<< std::setw(8) << std::setprecision(1) << itr0->y << " "
			<< std::setw(8) << std::setprecision(1) << itr0->z << std::endl;
		for (auto itr1 = itr0->pair.begin(); itr1 != itr0->pair.end(); itr1++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr1->pl0 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->raw0 << " "
				<< std::setw(4) << std::setprecision(0) << itr1->pl1 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->raw1 << " "
				<< std::setw(8) << std::setprecision(1) << itr1->x << " "
				<< std::setw(8) << std::setprecision(1) << itr1->y << " "
				<< std::setw(8) << std::setprecision(1) << itr1->z << " "
				<< std::setw(6) << std::setprecision(4) << itr1->oa << " "
				<< std::setw(4) << std::setprecision(1) << itr1->md << std::endl;
		}
		for (auto itr1 = itr0->trk.begin(); itr1 != itr0->trk.end(); itr1++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr1->second.pl0 << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.pl1 << " "
				<< std::setw(12) << std::setprecision(0) << itr1->second.rawid << " "
				<< std::setw(12) << std::setprecision(0) << itr1->second.chainid << " "
				<< std::setw(12) << std::setprecision(0) << itr1->second.groupid << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.nseg << " "
				<< std::setw(4) << std::setprecision(0) << itr1->second.npl << " "
				<< std::setw(6) << std::setprecision(0) << itr1->second.ph << " "
				<< std::setw(7) << std::setprecision(4) << itr1->second.ax << " "
				<< std::setw(7) << std::setprecision(4) << itr1->second.ay << " "
				<< std::setw(8) << std::setprecision(1) << itr1->second.x << " "
				<< std::setw(8) << std::setprecision(1) << itr1->second.y << " "
				<< std::setw(4) << std::setprecision(1) << itr1->first << std::endl;
		}
	}
}
