#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

bool Divide_Chain_VPH(Momentum_recon::Mom_chain&c, std::pair<int, double>&diff_max);
bool Divide_Chain_pixel(Momentum_recon::Mom_chain&c, std::pair<int, double>&diff_max);

bool Calc_divide_vph(Momentum_recon::Mom_chain  &c, int pl, double &val);
bool Calc_divide_pixel(Momentum_recon::Mom_chain  &c, int pl, double &val);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg file-in-momch file-out\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_out = argv[2];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::ofstream ofs(file_out);

	int pl0, pl1;
	double vph0, vph1;
	std::pair<int, double>VPH_diff_max, pixel_diff_max;
	double val_VPH, val_pixel;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		if (!Divide_Chain_VPH(*itr, VPH_diff_max))continue;
		if (!Divide_Chain_pixel(*itr, pixel_diff_max))continue;
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(10) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(4) << std::setprecision(0) << VPH_diff_max.first << " "
			<< std::setw(8) << std::setprecision(2) << VPH_diff_max.second << " "
			<< std::setw(4) << std::setprecision(0) << pixel_diff_max.first << " "
			<< std::setw(8) << std::setprecision(2) << pixel_diff_max.second << std::endl;
	}

}
bool Divide_Chain_VPH(Momentum_recon::Mom_chain&c, std::pair<int, double>&diff_max){
	std::set<int> pl_set;
	for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
		pl_set.insert(itr->pl);
	}

	int pl_min = *pl_set.begin();
	int pl_max = *pl_set.rbegin();
	//printf("PL range %d %d\n", pl_min, pl_max);
	double val;
	diff_max.second = DBL_MIN;
	bool return_flg = false;
	for (int pl = pl_min+6; pl <= pl_max-5; pl++) {
		if (Calc_divide_vph(c, pl, val)) {
			if (diff_max.second < val) {
				diff_max.second = val;
				diff_max.first = pl;
				return_flg = true;
			}
		}
	}
	return return_flg;
}
bool Divide_Chain_pixel(Momentum_recon::Mom_chain&c, std::pair<int, double>&diff_max) {
	std::set<int> pl_set;
	for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
		pl_set.insert(itr->pl);
	}

	int pl_min = *pl_set.begin();
	int pl_max = *pl_set.rbegin();
	//printf("PL range %d %d\n", pl_min, pl_max);
	double val;
	diff_max.second = DBL_MIN;
	bool return_flg = false;
	for (int pl = pl_min + 6; pl <= pl_max - 5; pl++) {
		if (Calc_divide_pixel(c, pl, val)) {
			if (diff_max.second < val) {
				diff_max.second = val;
				diff_max.first = pl;
				return_flg = true;
			}
		}
	}
	return return_flg;
}

bool Calc_divide_vph(Momentum_recon::Mom_chain  &c, int pl, double &val) {
	double mean[2], mean_err[2], sd[2];
	int count[2];
	count[0] = 0;
	count[1] = 0;
	int sum2[2] = {}, sum1[2] = {};

	for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
		if (itr->pl < pl) {
			for (int i = 0; i < 2; i++) {
				count[0] += 1;
				sum1[0] += itr->m[i].ph;
				sum2[0] += pow(itr->m[i].ph, 2);
			}
		}
		else {
			for (int i = 0; i < 2; i++) {
				count[1] += 1;
				sum1[1] += itr->m[i].ph;
				sum2[1] += pow(itr->m[i].ph, 2);
			}
		}
	}

	for (int i = 0; i < 2; i++) {

		if (count[i] < 2) return false;
		mean[i] = sum1[i] / count[i];
		sd[i] = sum2[i] / count[i] - mean[i] * mean[i];
		if (sd[i] <= 0)return false;
		else {
			sd[i] = sqrt(sd[i]) * sqrt(count[i] - 1) / sqrt(count[i]);
			mean_err[i] = sd[i] / sqrt(count[i]);
		}
	}

	double ratio = mean[1] / mean[0];
	double ratio_error = sqrt(pow(mean_err[1] / mean[0], 2) + pow(mean[1] / (mean[0] * mean[0])*mean_err[0], 2));

	val = (ratio - 1) / ratio_error;
	return true;
}
bool Calc_divide_pixel(Momentum_recon::Mom_chain  &c, int pl, double &val) {
	double mean[2], mean_err[2], sd[2];
	int count[2];
	count[0] = 0;
	count[1] = 0;
	int sum2[2] = {}, sum1[2] = {};

	for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
		if (itr->pl == 39)continue;
		if (itr->pl < pl) {
			for (int i = 0; i < 2; i++) {
				if (itr->m[i].hitnum <= 0)continue;
				count[0] += 1;
				sum1[0] += itr->m[i].hitnum;
				sum2[0] += pow(itr->m[i].hitnum, 2);
			}
		}
		else {
			for (int i = 0; i < 2; i++) {
				if (itr->m[i].hitnum <= 0)continue;
				count[1] += 1;
				sum1[1] += itr->m[i].hitnum;
				sum2[1] += pow(itr->m[i].hitnum, 2);
			}
		}
	}

	for (int i = 0; i < 2; i++) {

		if (count[i] < 2) return false;
		mean[i] = sum1[i] / count[i];
		sd[i] = sum2[i] / count[i] - mean[i] * mean[i];
		if (sd[i] <= 0)return false;
		else {
			sd[i] = sqrt(sd[i]) * sqrt(count[i] - 1) / sqrt(count[i]);
			mean_err[i] = sd[i] / sqrt(count[i]);
		}
	}

	double ratio = mean[1] / mean[0];
	double ratio_error = sqrt(pow(mean_err[1] / mean[0], 2) + pow(mean[1] / (mean[0] * mean[0])*mean_err[0], 2));

	val = (ratio - 1) / ratio_error;
	return true;

}

