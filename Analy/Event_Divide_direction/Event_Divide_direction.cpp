#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Event_information> Divide_momch(std::vector<Momentum_recon::Event_information> &momch, int mode);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-momch file-out-momch mode\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];
	int mode = std::stoi(argv[3]);

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);

	std::vector<Momentum_recon::Event_information> momch_divide = Divide_momch(momch,mode);

	Momentum_recon::Write_Event_information_extension(file_out_momch, momch_divide);
}
std::vector<Momentum_recon::Event_information> Divide_momch(std::vector<Momentum_recon::Event_information> &momch,int mode){
	std::vector<Momentum_recon::Event_information> ret;
	for (auto &ev : momch) {
		Momentum_recon::Event_information ev_tmp = ev;
		ev_tmp.chains.clear();
		Momentum_recon::Mom_chain muon;
		muon.chainid = -1;
		for (auto &ch : ev.chains) {
			if (ch.particle_flg == 13||ch.chainid==0) {
				ch.particle_flg == 13;
				ch.direction = 1;
				muon = ch;
			}
		}
		if (muon.chainid == -1) {
			fprintf(stderr, "event %d muon not found\n", ev.groupid);
		}

		ev_tmp.chains.push_back(muon);
		int vertex_pl = muon.base.rbegin()->pl;
		for (auto &ch : ev.chains) {
			if (muon.chainid == ch.chainid)continue;
			int start_pl = ch.base.begin()->pl;
			int stop_pl = ch.base.rbegin()->pl;
			if (mode == 0) {
				ch.chainid = ch.chainid * 10;
				ch.direction = 1;
				ev_tmp.chains.push_back(ch);

				ch.chainid = ch.chainid + 1;
				ch.direction = -1;
				ev_tmp.chains.push_back(ch);
			}
			else if (mode == 1) {
				//�ђʏ���
				if (start_pl <= vertex_pl && vertex_pl < stop_pl) {
					Momentum_recon::Mom_chain ch_div[4];
					for (int i = 0; i < 4; i++) {
						ch_div[i] = ch;
						ch_div[i].chainid = ch_div[i].chainid * 10 + i + 2;
						ch_div[i].base.clear();
						ch_div[i].base_pair.clear();
					}
					for (auto &base : ch.base) {
						if (base.pl <= vertex_pl) {
							ch_div[0].base.push_back(base);
							ch_div[1].base.push_back(base);
						}
						else {
							ch_div[2].base.push_back(base);
							ch_div[3].base.push_back(base);
						}
					}
					for (auto &b_pair : ch.base_pair) {
						if (b_pair.second.pl <= vertex_pl) {
							ch_div[0].base_pair.push_back(b_pair);
							ch_div[1].base_pair.push_back(b_pair);
						}
						else if (vertex_pl < b_pair.first.pl) {
							ch_div[2].base_pair.push_back(b_pair);
							ch_div[3].base_pair.push_back(b_pair);
						}
					}
					ch_div[0].direction = 1;
					ch_div[1].direction = -1;
					ch_div[2].direction = 1;
					ch_div[3].direction = -1;

					ev_tmp.chains.push_back(ch_div[0]);
					ev_tmp.chains.push_back(ch_div[1]);
					ev_tmp.chains.push_back(ch_div[2]);
					ev_tmp.chains.push_back(ch_div[3]);

				}
				else {
					ch.chainid = ch.chainid * 10;
					ch.direction = 1;
					ev_tmp.chains.push_back(ch);

					ch.chainid = ch.chainid + 1;
					ch.direction = -1;
					ev_tmp.chains.push_back(ch);
				}
			}

		}
		ret.push_back(ev_tmp);
	}
	return ret;
}
