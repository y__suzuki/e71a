//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>
using namespace l2c;

class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};

l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output);
void read_linklet_list(std::string filename, std::vector<Linklet> &ltlist);
void Linklet_reduction(std::vector<Linklet> &all_ltlist, int pl0, int pl1);
void write_linklet_list(std::string filename, std::vector<Linklet> &ltlist);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg file-in-linklet-list file-out-linklet-list\n");
		exit(1);
	}
	std::string file_in_link = argv[1];
	std::string file_out_link = argv[2];

	std::vector<Linklet> ltlist;
	read_linklet_list(file_in_link, ltlist);
	printf("all linklet num = %lld\n", ltlist.size());

	Linklet_reduction(ltlist, 1, 40);
	Linklet_reduction(ltlist, 30, 70);
	Linklet_reduction(ltlist, 60, 100);
	Linklet_reduction(ltlist, 90, 150);
	//Linklet_reduction(ltlist, 1, 20);
	//Linklet_reduction(ltlist, 10, 50);
	//Linklet_reduction(ltlist, 40, 80);
	//Linklet_reduction(ltlist, 70, 110);
	//Linklet_reduction(ltlist, 100, 150);
	printf("all linklet num = %lld\n", ltlist.size());

	write_linklet_list(file_out_link, ltlist);

}

int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}

void read_linklet_list(std::string filename, std::vector<Linklet> &ltlist){
	int64_t link_num = Linklet_header_num(filename);
	ltlist.reserve(link_num);

	std::ifstream ifs(filename,std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	linklet_header l;
	while (ifs.read((char*)& l, sizeof(linklet_header))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		ltlist.emplace_back(l.pos0, l.pos1, l.raw0, l.raw1);
		//printf("%d %d %d %d\n", l.pos0, l.pos1, l.raw0, l.raw1);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
}

l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output) {
	try
	{
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		if (output) {
			l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
			return cdat;
		}
		l2c::Cdat cdat = l2c::MakeCdat_no_out(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
		return cdat;

	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}

void Linklet_reduction( std::vector<Linklet> &all_ltlist, int pl0, int pl1) {
	//pl0<= PL <=pl1 の範囲でlinkletの畳み込み
	std::set<std::pair<int32_t, int64_t>> btset;
	std::vector<Linklet> ltlist,not_use_ltlist;
	std::set<int32_t> pos_set;
	for (auto itr = all_ltlist.begin(); itr != all_ltlist.end(); itr++) {
		if (itr->pos1 / 10 < pl0 || itr->pos2 / 10 > pl1) {
			not_use_ltlist.push_back(*itr);
			continue;
		}
		ltlist.push_back(*itr);
		btset.insert(std::make_pair(itr->pos1, itr->id1));
		btset.insert(std::make_pair(itr->pos2, itr->id2));
		pos_set.insert(itr->pos1);
		pos_set.insert(itr->pos2);

	}
	all_ltlist.clear();
	all_ltlist.shrink_to_fit();

	printf("PL%03d<= PL <=PL%03d linklet num =%lld(not use %lld)\n", pl0, pl1, ltlist.size(), not_use_ltlist.size());
	std::vector<int32_t> usepos(pos_set.begin(), pos_set.end());

	l2c::Cdat cdat=l2c_x(btset, ltlist, usepos, 1, true);
	ltlist.clear();

	size_t grsize = cdat.GetNumOfGroups();
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
		auto link_list = gr.GetLinklets();
		for (auto itr = link_list.begin(); itr != link_list.end(); itr++) {
			int32_t btpl0 = usepos[itr->first.GetPL()];
			int64_t btid0 = itr->first.GetRawID();
			int32_t btpl1 = usepos[itr->second.GetPL()];
			int64_t btid1 = itr->second.GetRawID();
			ltlist.emplace_back(btpl0, btpl1, btid0, btid1);
		}
	}
	printf("after reduction linklet num =%lld\n", ltlist.size());

	for (auto itr = ltlist.begin(); itr != ltlist.end(); itr++) {
		not_use_ltlist.push_back(*itr);
	}
	printf("all linklet num =%lld\n", not_use_ltlist.size());

	std::swap(not_use_ltlist, all_ltlist);

}

void write_linklet_list(std::string filename, std::vector<Linklet> &ltlist){
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (ltlist.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = ltlist.size();
	linklet_header l;

	for (int i = 0; i < ltlist.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		l.pos0 = ltlist[i].pos1;
		l.pos1 = ltlist[i].pos2;
		l.raw0 = ltlist[i].id1;
		l.raw1 = ltlist[i].id2;
		ofs.write((char*)& l, sizeof(linklet_header));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}
