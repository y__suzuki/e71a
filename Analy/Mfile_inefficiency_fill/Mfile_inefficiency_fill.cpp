#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include "VxxReader.h"
#include <FILE_structure.hpp>
#include <set>

//mfile-->prediction作成(local ali)-->basetrackから探索

class Basetrack {
public:
	int pl, rawid, ph;
	double ax, ay, x, y, z;
};
class Prediction {
public:
	uint64_t chainid;
	int pl, rawid;
	double ax, ay, x, y, z;
	Basetrack base[2];
	Basetrack pred[2];
};
class Prediction_hit :public Prediction {
public:
	int hit[2];
	Basetrack hit_base[2];
};
std::map<int, double> Get_zmap(mfile1::MFile_minimum &m);

void chain_nseg_selection(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, const int nseg);
void reject_Fe_ECC(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel);
void chain_dlat_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double threshold);
void chain_angle_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double thr_ax, double thr_ay);

mfile1::MFile_minimum chain_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &sel);
std::vector<Prediction> pick_ineff_track(mfile1::MFile_minimum &m);
std::vector<Prediction> debug_pick_ineff_track(mfile1::MFile_minimum &m);
void Add_base_inf(std::vector<Prediction>&pred, std::string file_path, std::map<int, double> &z_nominal_map);
void debug_Add_base_inf(std::vector<Prediction>&pred, std::string file_path, std::map<int, double> &z_nominal_map);
void Make_prediction(std::vector<Prediction>&pred, std::string file_path, std::map<int, double> &z_nominal_map);

void Apply_alignment0(std::vector<Basetrack*> pred, std::vector<Basetrack*> base, std::vector<corrmap0::Corrmap> &corr);
void corrmap_area(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size);
void basetrack_extra(std::vector<Basetrack*> pred, std::vector<Basetrack*> base, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size);
void basetrack_tans_affin_invers(Basetrack* base, corrmap0::Corrmap corr);

void Apply_alignment1(std::vector<Basetrack*> pred, std::vector<Basetrack*> base, std::vector<corrmap0::Corrmap> &corr);
void basetrack_trans(std::vector<Basetrack*> pred, std::vector<Basetrack*> base, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size);
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size);
void basetrack_tans_affin_extra(Basetrack* pred, Basetrack* base, corrmap0::Corrmap corr);

void base_matching(std::vector<Prediction_hit*> &pred_v, std::vector<vxx::base_track_t> &base);
std::vector<Prediction_hit> search_basetrack(std::vector<Prediction>&pred, std::string file_path);
void base_matching(std::vector<Prediction_hit*> &pred_v, std::vector<vxx::base_track_t> &base);

void output_pred(std::string filename, std::vector<Prediction_hit > &pred_v);
void out_detected(std::vector<Prediction_hit > &pred_v);
void output_pred_inf(std::string filename, std::vector<Prediction_hit > &pred_v);
void output_pred_base_diff(std::string filename, std::vector<Prediction_hit > &pred_v);

void Fill_detected_track(mfile1::MFile_minimum &m, std::vector<Prediction_hit> &pred_hit, std::map<int, double> z_map, std::vector<corrmap0::Corrmap> &corr_abs);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg mfile-in ECC_path output_path\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_path = argv[2];
	std::string file_out_path = argv[3];

	mfile1::MFile_minimum m;
	mfile1::read_mfile(file_in_mfile, m);

	std::vector<uint64_t> all, sel;
	for (int i = 0; i < m.chains.size(); i++) {
		all.push_back(i);
	}

	chain_nseg_selection(m, all, sel, 15);
	//all = sel;
	//sel.clear();

	//reject_Fe_ECC(m, all, sel);
	//all = sel;
	//sel.clear();

	//chain_angle_selection(m, all, sel, 4.0, 4.0);
	//chain_angle_selection(m, all, sel, 0.5, 0.5);
	//all = sel;
	//sel.clear();

	//chain_dlat_selection(m, all, sel, 0.005);

	mfile1::MFile_minimum m_sel= chain_selection(m, sel);
	std::map<int, double> z_map = Get_zmap(m_sel);
	std::vector<Prediction> pred = pick_ineff_track(m_sel);
	//std::vector<Prediction> pred = debug_pick_ineff_track(m);

	std::stringstream structure_path;
	structure_path << file_path << "\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_nominal_map = chamber1::base_z_convert(chamber);

	Add_base_inf(pred, file_path, z_nominal_map);
	//debug_Add_base_inf(pred, file_path, z_nominal_map);
	Make_prediction(pred, file_path, z_nominal_map);
	std::vector<Prediction_hit> pred_hit = search_basetrack(pred, file_path);

	std::stringstream corrmap_abs_path;
	corrmap_abs_path << file_path << "\\Area0\\0\\align\\corrmap-abs.lst";
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(corrmap_abs_path.str(), corr_abs);


	Fill_detected_track(m, pred_hit, z_map, corr_abs);

	mfile1::write_mfile(file_out_path, m);
	//output_pred(file_out_path, pred_hit);
	//out_detected(pred_hit);

}

std::map<int, double> Get_zmap(mfile1::MFile_minimum &m) {
	std::map<int, double> ret;
	for (auto c : m.all_basetracks) {
		for (auto b : c) {
			ret.insert(std::make_pair(b.pos / 10, b.z));
		}
	}
	return ret;
}


void chain_nseg_selection(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, const int nseg) {
	for (auto i : all) {
		if (m.chains[i].nseg < nseg)continue;
		sel.push_back(i);
	}
	printf("chain nseg >= %lld: %lld --> %lld (%4.1lf%%)\n", nseg, all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}
void reject_Fe_ECC(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel) {
	for (auto i : all) {
		if (m.chains[i].pos0 / 10 > 15) {
			sel.push_back(i);
		}
		else {
			int flg = 0;
			for (auto b : m.all_basetracks[i]) {
				if (b.pos / 10 == 16)flg++;
				if (b.pos / 10 == 17)flg++;
				if (b.pos / 10 == 18)flg++;
				if (b.pos / 10 == 19)flg++;
				if (b.pos / 10 == 20)flg++;
				if (b.pos / 10 == 21)flg++;
				if (b.pos / 10 == 22)flg++;
				if (b.pos / 10 == 23)flg++;
			}
			if (flg >= 6 || m.chains[i].pos1 / 10 > 23) {
				sel.push_back(i);
			}
		}
	}
	printf("reject_Fe_ECC(PL1 >= 24): %lld --> %lld (%4.1lf%%)\n", all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}
void chain_dlat_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double threshold) {

	for (auto i : all) {
		if (mfile1::angle_diff_dev_lat(m.all_basetracks[i]) > threshold)continue;
		sel.push_back(i);
	}
	printf("chain lateral selection <= %5.4lf : %lld --> %lld (%4.1lf%%)\n", threshold, all.size(), sel.size(), sel.size()*100. / all.size());
	return;

}
void chain_angle_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double thr_ax, double thr_ay) {
	double ax, ay;
	for (auto i : all) {
		ax = mfile1::chain_ax(m.all_basetracks[i]);
		ay = mfile1::chain_ay(m.all_basetracks[i]);
		if (thr_ax - fabs(ax) < 0)continue;
		if (thr_ay - fabs(ay) < 0)continue;
		sel.push_back(i);

	}
	printf("chain average angle selection |ax|<=%3.1lf |ay|<=%3.1lf : %lld --> %lld (%4.1lf%%)\n", thr_ax, thr_ay, all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}
mfile1::MFile_minimum chain_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &sel) {

	mfile1::MFile_minimum ret;
	ret.header = m.header;
	ret.info_header = m.info_header;

	for (auto itr = sel.begin(); itr != sel.end(); itr++) {
		ret.chains.push_back(m.chains.at(*itr));
		ret.all_basetracks.push_back(m.all_basetracks.at(*itr));
	}
	uint64_t N_chain = 0, N_base = 0;
	for (auto c : ret.all_basetracks) {
		N_chain++;
		N_base += c.size();
	}
	if (N_chain != ret.chains.size()) { throw std::exception("N_Chain is wrong."); }
	ret.info_header.Nchain = N_chain;
	ret.info_header.Nbasetrack = N_base;

	return ret;
}

std::vector<Prediction> debug_pick_ineff_track(mfile1::MFile_minimum &m) {
	std::vector<Prediction> ret;
	uint64_t all = 0;
	for (auto c : m.all_basetracks) {
		for (auto b = c.begin(); b != c.end(); b++) {
			all++;
			//inefficiencyのtrackを抽出
			if (b == c.begin())continue;
			if (b + 1 == c.end())continue;
			if ((b + 1)->pos / 10 - b->pos / 10 != 1)continue;
			if (b->pos / 10 - (b - 1)->pos / 10 != 1)continue;
			if (b->pos / 10 < 44)continue;
			if (45 < b->pos / 10)continue;
			Prediction pred;
			Basetrack base[2];
			base[0].pl = (b - 1)->pos / 10;
			base[0].rawid = (b - 1)->rawid;
			base[0].ph = (b - 1)->ph;
			base[0].ax = (b - 1)->ax;
			base[0].ay = (b - 1)->ay;
			base[0].x = (b - 1)->x;
			base[0].y = (b - 1)->y;
			base[0].z = 0;

			base[1].pl = (b + 1)->pos / 10;
			base[1].rawid = (b + 1)->rawid;
			base[1].ph = (b + 1)->ph;
			base[1].ax = (b + 1)->ax;
			base[1].ay = (b + 1)->ay;
			base[1].x = (b + 1)->x;
			base[1].y = (b + 1)->y;
			base[1].z = 0;

			pred.pl = b->pos / 10;
			pred.rawid = b->rawid;
			pred.pred[0].pl = b->pos / 10;
			pred.pred[1].pl = b->pos / 10;
			pred.base[0] = base[0];
			pred.base[1] = base[1];
			ret.push_back(pred);
		}
	}
	return ret;

}
void debug_Add_base_inf(std::vector<Prediction>&pred, std::string file_path, std::map<int, double> &z_nominal_map) {
	std::set<int> pl;
	std::multimap<int, int> pred_rawid;
	std::map<std::pair<int, int>, Basetrack*> pred_base;
	std::map<std::pair<int, int>, Prediction*> pred_base2;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		pl.insert(itr->base[0].pl);
		pl.insert(itr->base[1].pl);
		pl.insert(itr->pl);

		pred_rawid.insert(std::make_pair(itr->base[0].pl, itr->base[0].rawid));
		pred_rawid.insert(std::make_pair(itr->base[1].pl, itr->base[1].rawid));
		pred_rawid.insert(std::make_pair(itr->pl, itr->rawid));

		pred_base.insert(std::make_pair(std::make_pair(itr->base[0].pl, itr->base[0].rawid), &(itr->base[0])));
		pred_base.insert(std::make_pair(std::make_pair(itr->base[1].pl, itr->base[1].rawid), &(itr->base[1])));
		pred_base2.insert(std::make_pair(std::make_pair(itr->pl, itr->rawid), &(*itr)));
	}

	std::vector<vxx::base_track_t> base;
	vxx::BvxxReader br;
	double z;
	int all = 0, count = 0;
	for (auto num = pl.begin(); num != pl.end(); num++) {
		std::stringstream file_base;
		file_base << file_path << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << *num
			<< "\\b" << std::setw(3) << std::setfill('0') << *num << ".sel.cor.vxx";
		base.clear();
		base = br.ReadAll(file_base.str(), *num, 0);
		std::map<int, vxx::base_track_t*> base_map;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_map.insert(std::make_pair(itr->rawid, &(*itr)));
		}

		z = z_nominal_map.at(*num);

		all = pred_rawid.count(*num);
		count = 0;
		if (all == 0)continue;
		auto range = pred_rawid.equal_range(*num);
		for (auto itr = range.first; itr != range.second; itr++) {
			if (count % 1000 == 0) {
				fprintf(stderr, "\r PL%03d fill predction <-- basetrack inf %d/%d(%4.1lf%%)", *num, count, all, count*100. / all);
			}
			count++;

			auto res_base = base_map.find(itr->second);
			if (res_base == base_map.end()) {
				fprintf(stderr, "basetrack rawid=%d not found\n", itr->second);
				exit(1);
			}
			auto res_pred = pred_base.find(std::make_pair(itr->first, itr->second));
			auto res_pred2 = pred_base2.find(std::make_pair(itr->first, itr->second));

			if (res_pred != pred_base.end()) {
				res_pred->second->ax = res_base->second->ax;
				res_pred->second->ay = res_base->second->ay;
				res_pred->second->x = res_base->second->x;
				res_pred->second->y = res_base->second->y;
				res_pred->second->z = z;
			}
			if (res_pred2 != pred_base2.end()) {
				res_pred2->second->ax = res_base->second->ax;
				res_pred2->second->ay = res_base->second->ay;
				res_pred2->second->x = res_base->second->x;
				res_pred2->second->y = res_base->second->y;
				res_pred2->second->z = z;
			}
		}
		fprintf(stderr, "\r PL%03d fill predction <-- basetrack inf %d/%d(%4.1lf%%)\n", *num, count, all, count*100. / all);
	}
}

std::vector<Prediction> pick_ineff_track(mfile1::MFile_minimum &m) {
	std::vector<Prediction> ret;
	uint64_t all = 0, ich = 0;
	int id = 0;
	for (auto c : m.all_basetracks) {
		for (auto b = c.begin(); b != c.end(); b++) {
			all++;
			//inefficiencyのtrackを抽出
			if (b == c.begin())continue;
			if (b + 1 == c.end())continue;
			if ((b + 1)->pos / 10 - b->pos / 10 == 1)continue;
			for (int pl = b->pos / 10 + 1; pl < (b + 1)->pos / 10; pl++) {
				//if (pl < 45)continue;
				//if (49 < pl)continue;
				all++;
				Prediction pred;
				Basetrack base[2];
				base[0].pl = b->pos / 10;
				base[0].rawid = b->rawid;
				base[0].ph = b->ph;
				base[0].ax = b->ax;
				base[0].ay = b->ay;
				base[0].x = b->x;
				base[0].y = b->y;
				base[0].z = 0;

				base[1].pl = (b + 1)->pos / 10;
				base[1].rawid = (b + 1)->rawid;
				base[1].ph = (b + 1)->ph;
				base[1].ax = (b + 1)->ax;
				base[1].ay = (b + 1)->ay;
				base[1].x = (b + 1)->x;
				base[1].y = (b + 1)->y;
				base[1].z = 0;

				pred.pl = pl;
				pred.rawid = id;
				pred.chainid = m.chains[ich].chain_id;
				id++;
				pred.pred[0].pl = pl;
				pred.pred[1].pl = pl;
				pred.base[0] = base[0];
				pred.base[1] = base[1];
				ret.push_back(pred);

			}
		}
		ich++;
	}
	printf("ineff base %d --> %d(%4.1lf%%)\n", all, ret.size(), ret.size()*100. / all);
	return ret;
}
void Add_base_inf(std::vector<Prediction>&pred, std::string file_path, std::map<int, double> &z_nominal_map) {
	std::set<int> pl;
	std::multimap<int, int> pred_rawid;
	std::map<std::pair<int, int>, Basetrack*> pred_base;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		pl.insert(itr->base[0].pl);
		pl.insert(itr->base[1].pl);

		pred_rawid.insert(std::make_pair(itr->base[0].pl, itr->base[0].rawid));
		pred_rawid.insert(std::make_pair(itr->base[1].pl, itr->base[1].rawid));

		pred_base.insert(std::make_pair(std::make_pair(itr->base[0].pl, itr->base[0].rawid), &(itr->base[0])));
		pred_base.insert(std::make_pair(std::make_pair(itr->base[1].pl, itr->base[1].rawid), &(itr->base[1])));
	}

	std::vector<vxx::base_track_t> base;
	vxx::BvxxReader br;
	double z;
	int all = 0, count = 0;
	for (auto num = pl.begin(); num != pl.end(); num++) {
		std::stringstream file_base;
		file_base << file_path << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << *num
			<< "\\b" << std::setw(3) << std::setfill('0') << *num << ".sel.cor.vxx";
		base.clear();
		base = br.ReadAll(file_base.str(), *num, 0);
		std::map<int, vxx::base_track_t*> base_map;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_map.insert(std::make_pair(itr->rawid, &(*itr)));
		}

		z = z_nominal_map.at(*num);

		all = pred_rawid.count(*num);
		count = 0;
		if (all == 0)continue;
		auto range = pred_rawid.equal_range(*num);
		for (auto itr = range.first; itr != range.second; itr++) {
			if (count % 1000 == 0) {
				fprintf(stderr, "\r PL%03d fill predction <-- basetrack inf %d/%d(%4.1lf%%)", *num, count, all, count*100. / all);
			}
			count++;

			auto res_base = base_map.find(itr->second);
			if (res_base == base_map.end()) {
				fprintf(stderr, "basetrack rawid=%d not found\n", itr->second);
				exit(1);
			}
			auto res_pred = pred_base.find(std::make_pair(itr->first, itr->second));
			if (res_pred == pred_base.end()) {
				fprintf(stderr, "prediction pl=%d rawid=%d not found\n", itr->first, itr->second);
				exit(1);
			}
			if (res_pred->second->ph != res_base->second->m[0].ph + res_base->second->m[1].ph) {
				fprintf(stderr, "\nprediction PH:%d pl:%d rawid:%d\n", res_pred->second->ph, res_pred->second->pl, res_pred->second->rawid);
				fprintf(stderr, "basetrack  PH:%d pl:%d rawid:%d\n", res_base->second->m[0].ph + res_base->second->m[1].ph, res_base->second->pl, res_base->second->rawid);
				fprintf(stderr, "mis match\n");
				exit(1);
			}
			res_pred->second->ax = res_base->second->ax;
			res_pred->second->ay = res_base->second->ay;
			res_pred->second->x = res_base->second->x;
			res_pred->second->y = res_base->second->y;
			res_pred->second->z = z;
		}
		fprintf(stderr, "\r PL%03d fill predction <-- basetrack inf %d/%d(%4.1lf%%)\n", *num, count, all, count*100. / all);
	}
}

void Make_prediction(std::vector<Prediction>&pred, std::string file_path, std::map<int, double> &z_nominal_map) {

	std::multimap<std::pair<int, int>, Prediction*> pred_extra0, pred_extra1;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		pred_extra0.insert(std::make_pair(std::make_pair(itr->base[0].pl, itr->pl), &(*itr)));
		pred_extra1.insert(std::make_pair(std::make_pair(itr->base[1].pl, itr->pl), &(*itr)));
	}
	double z;
	for (auto itr = pred_extra0.begin(); itr != pred_extra0.end(); itr++) {
		auto key = itr->first;
		auto range = pred_extra0.equal_range(key);
		std::stringstream file_corrmap;
		file_corrmap << file_path << "\\Area0\\0\\align\\corrmap-align-"
			<< std::setw(3) << std::setfill('0') << key.first << "-"
			<< std::setw(3) << std::setfill('0') << key.second << ".lst";
		std::vector<corrmap0::Corrmap> corr;
		corrmap0::read_cormap(file_corrmap.str(), corr);
		z = z_nominal_map.at(key.second);
		std::vector<Basetrack*> pred;
		std::vector<Basetrack*> base;
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			itr2->second->pred[0].ax = itr2->second->base[0].ax;
			itr2->second->pred[0].ay = itr2->second->base[0].ay;
			itr2->second->pred[0].x = itr2->second->base[0].x;
			itr2->second->pred[0].y = itr2->second->base[0].y;
			itr2->second->pred[0].z = z;
			pred.push_back(&(itr2->second->pred[0]));
			base.push_back(&(itr2->second->base[0]));
		}
		//下流から上流に外挿(nominalgap+corr_dz)
		//外挿先のbasetrackを逆変換
		Apply_alignment0(pred, base, corr);

		itr = std::next(itr, pred_extra0.count(key) - 1);
	}
	for (auto itr = pred_extra1.begin(); itr != pred_extra1.end(); itr++) {
		auto key = itr->first;
		auto range = pred_extra1.equal_range(key);
		std::stringstream file_corrmap;
		file_corrmap << file_path << "\\Area0\\0\\align\\corrmap-align-"
			<< std::setw(3) << std::setfill('0') << key.second << "-"
			<< std::setw(3) << std::setfill('0') << key.first << ".lst";
		std::vector<corrmap0::Corrmap> corr;
		corrmap0::read_cormap(file_corrmap.str(), corr);
		z = z_nominal_map.at(key.second);
		std::vector<Basetrack*> pred;
		std::vector<Basetrack*> base;
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			itr2->second->pred[1].ax = itr2->second->base[1].ax;
			itr2->second->pred[1].ay = itr2->second->base[1].ay;
			itr2->second->pred[1].x = itr2->second->base[1].x;
			itr2->second->pred[1].y = itr2->second->base[1].y;
			itr2->second->pred[1].z = z;
			pred.push_back(&(itr2->second->pred[1]));
			base.push_back(&(itr2->second->base[1]));
		}
		//上流から下流に外挿(nominalgap+corr_dz)
		Apply_alignment1(pred, base, corr);
		itr = std::next(itr, pred_extra1.count(key) - 1);
	}

	//for (auto itr = pred.begin(); itr != pred.end(); itr++) {
	//	printf("%3d %5.4lf %5.4lf %8.1lf %8.1lf %8.1lf\n", itr->base[0].pl, itr->base[0].ax, itr->base[0].ay, itr->base[0].x, itr->base[0].y, itr->base[0].z);
	//	printf("%3d %5.4lf %5.4lf %8.1lf %8.1lf %8.1lf\n", itr->pred[0].pl, itr->pred[0].ax, itr->pred[0].ay, itr->pred[0].x, itr->pred[0].y, itr->pred[0].z);
	//	printf("%3d %5.4lf %5.4lf %8.1lf %8.1lf %8.1lf\n", itr->pl, itr->ax, itr->ay, itr->x, itr->y, itr->z);
	//	printf("%3d %5.4lf %5.4lf %8.1lf %8.1lf %8.1lf\n", itr->pred[1].pl, itr->pred[1].ax, itr->pred[1].ay, itr->pred[1].x, itr->pred[1].y, itr->pred[1].z);
	//	printf("%3d %5.4lf %5.4lf %8.1lf %8.1lf %8.1lf\n", itr->base[1].pl, itr->base[1].ax, itr->base[1].ay, itr->base[1].x, itr->base[1].y, itr->base[1].z);
	//	printf("\n");
	//}

}
//下流-->上流に外挿する際の下流basetrackの変換
void Apply_alignment0(std::vector<Basetrack*> pred, std::vector<Basetrack*> base, std::vector<corrmap0::Corrmap> &corr) {
	//corrmapの範囲を逆変換
	double hash_size = 4000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	corrmap_area(corr, corr_map, hash_size);

	basetrack_extra(pred, base, corr_map, hash_size);

}
void corrmap_area(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size) {

	//hash_sizeはcorrmapのサイズより大きくする
	double cx, cy;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {

		cx = (itr->areax[0] + itr->areax[1]) / 2;
		cy = (itr->areay[0] + itr->areay[1]) / 2;

		std::pair<int, int>id;
		id.first = cx / hash_size;
		id.second = cy / hash_size;
		corr_map.insert(std::make_pair(id, *itr));
	}

}
void basetrack_extra(std::vector<Basetrack*> pred, std::vector<Basetrack*> base, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size) {
	int ix, iy;
	double extra_x, extra_y;
	int count = 0;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		auto itr_base = std::next(base.begin(), count);
		count++;
		extra_x = (*itr_base)->x + (*itr_base)->ax*((*itr)->z - (*itr_base)->z);
		extra_y = (*itr_base)->y + (*itr_base)->ay*((*itr)->z - (*itr_base)->z);
		corrmap0::Corrmap param;
		std::pair<int, int> id;
		{
			ix = extra_x / hash_size;
			iy = extra_y / hash_size;
			std::vector<corrmap0::Corrmap> corr_list;
			int loop = 0;
			while (corr_list.size() <= 3) {
				loop++;
				for (int iix = -1 * loop; iix <= 1 * loop; iix++) {
					for (int iiy = -1 * loop; iiy <= 1 * loop; iiy++) {
						id.first = ix + iix;
						id.second = iy + iiy;
						if (corr_map.count(id) == 0)continue;
						auto range = corr_map.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							corr_list.push_back(res->second);
						}
					}
				}
			}
			double dist;
			for (auto itr2 = corr_list.begin(); itr2 != corr_list.end(); itr2++) {
				double cx, cy;
				cx = (itr2->areax[0] + itr2->areax[1]) / 2;
				cy = (itr2->areay[0] + itr2->areay[1]) / 2;
				if (itr2 == corr_list.begin()) {
					dist = (extra_x - cx)*(extra_x - cx) + (extra_y - cy)*(extra_y - cy);
					param = *itr2;
				}
				if (dist > (extra_x - cx)*(extra_x - cx) + (extra_y - cy)*(extra_y - cy)) {
					dist = (extra_x - cx)*(extra_x - cx) + (extra_y - cy)*(extra_y - cy);
					param = *itr2;
				}

			}
		}
		extra_x = (*itr_base)->x + (*itr_base)->ax*((*itr)->z + param.dz - (*itr_base)->z);
		extra_y = (*itr_base)->y + (*itr_base)->ay*((*itr)->z + param.dz - (*itr_base)->z);
		{
			ix = extra_x / hash_size;
			iy = extra_y / hash_size;
			std::vector<corrmap0::Corrmap> corr_list;
			int loop = 0;
			while (corr_list.size() <= 3) {
				loop++;
				for (int iix = -1 * loop; iix <= 1 * loop; iix++) {
					for (int iiy = -1 * loop; iiy <= 1 * loop; iiy++) {
						id.first = ix + iix;
						id.second = iy + iiy;
						if (corr_map.count(id) == 0)continue;
						auto range = corr_map.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							corr_list.push_back(res->second);
						}
					}
				}
			}
			double dist;
			for (auto itr2 = corr_list.begin(); itr2 != corr_list.end(); itr2++) {
				double cx, cy;
				cx = (itr2->areax[0] + itr2->areax[1]) / 2;
				cy = (itr2->areay[0] + itr2->areay[1]) / 2;
				if (itr2 == corr_list.begin()) {
					dist = (extra_x - cx)*(extra_x - cx) + (extra_y - cy)*(extra_y - cy);
					param = *itr2;
				}
				if (dist > (extra_x - cx)*(extra_x - cx) + (extra_y - cy)*(extra_y - cy)) {
					dist = (extra_x - cx)*(extra_x - cx) + (extra_y - cy)*(extra_y - cy);
					param = *itr2;
				}

			}
		}
		(*itr)->x = (*itr_base)->x + (*itr_base)->ax*((*itr)->z + param.dz - (*itr_base)->z);
		(*itr)->y = (*itr_base)->y + (*itr_base)->ay*((*itr)->z + param.dz - (*itr_base)->z);
		(*itr_base)->z = (*itr_base)->z - param.dz;
		basetrack_tans_affin_invers(*itr, param);
	}


}
void basetrack_tans_affin_invers(Basetrack* base, corrmap0::Corrmap corr) {
	double x_tmp, y_tmp, factor;
	x_tmp = base->x - corr.position[4];
	y_tmp = base->y - corr.position[5];
	factor = 1 / (corr.position[0] * corr.position[3] - corr.position[1] * corr.position[2]);

	base->x = factor * (x_tmp * corr.position[3] - y_tmp * corr.position[1]);
	base->y = factor * (y_tmp *corr.position[0] - x_tmp * corr.position[2]);

	x_tmp = base->ax - corr.angle[4];
	y_tmp = base->ay - corr.angle[5];
	factor = 1 / (corr.angle[0] * corr.angle[3] - corr.angle[1] * corr.angle[2]);

	base->ax = factor * (x_tmp * corr.angle[3] - y_tmp * corr.angle[1]);
	base->ay = factor * (y_tmp *corr.angle[0] - x_tmp * corr.angle[2]);

}

//上流-->下流に外挿する際の上流basetrackの変換
void Apply_alignment1(std::vector<Basetrack*> pred, std::vector<Basetrack*> base, std::vector<corrmap0::Corrmap> &corr) {
	//corrmapの範囲を逆変換
	double hash_size = 4000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map;
	corrmap_area_inverse(corr, corr_map, hash_size);
	basetrack_trans(pred, base, corr_map, hash_size);

}
void basetrack_trans(std::vector<Basetrack*> pred, std::vector<Basetrack*> base, std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_map, double hash_size) {
	int ix, iy;
	int count = 0;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		auto itr_base = std::next(base.begin(), count);
		count++;
		std::pair<int, int> id;
		ix = (*itr)->x / hash_size;
		iy = (*itr)->y / hash_size;
		std::vector<corrmap0::Corrmap> corr_list;
		int loop = 0;
		while (corr_list.size() <= 3) {
			loop++;
			for (int iix = -1 * loop; iix <= 1 * loop; iix++) {
				for (int iiy = -1 * loop; iiy <= 1 * loop; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (corr_map.count(id) == 0)continue;
					auto range = corr_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						corr_list.push_back(res->second);
					}
				}
			}
		}
		double dist;
		corrmap0::Corrmap param;
		for (auto itr2 = corr_list.begin(); itr2 != corr_list.end(); itr2++) {
			double cx, cy;
			cx = (itr2->areax[0] + itr2->areax[1]) / 2;
			cy = (itr2->areay[0] + itr2->areay[1]) / 2;
			if (itr2 == corr_list.begin()) {
				dist = ((*itr)->x - cx)*((*itr)->x - cx) + ((*itr)->y - cy)*((*itr)->y - cy);
				param = *itr2;
			}
			if (dist > ((*itr)->x - cx)*((*itr)->x - cx) + ((*itr)->y - cy)*((*itr)->y - cy)) {
				dist = ((*itr)->x - cx)*((*itr)->x - cx) + ((*itr)->y - cy)*((*itr)->y - cy);
				param = *itr2;
			}

		}
		basetrack_tans_affin_extra(*itr, *itr_base, param);

	}


}
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap> &corr_map, double hash_size) {

	//hash_sizeはcorrmapのサイズより大きくする
	double cx, cy;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		double area[4], factor;
		area[0] = itr->areax[0] - itr->position[4];
		area[1] = itr->areax[1] - itr->position[4];
		area[2] = itr->areay[0] - itr->position[5];
		area[3] = itr->areay[1] - itr->position[5];

		factor = 1 / (itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);
		itr->areax[0] = factor * (area[0] * itr->position[3] - area[2] * itr->position[1]);
		itr->areay[0] = factor * (area[2] * itr->position[0] - area[0] * itr->position[2]);
		itr->areax[1] = factor * (area[1] * itr->position[3] - area[3] * itr->position[1]);
		itr->areay[1] = factor * (area[3] * itr->position[0] - area[1] * itr->position[2]);

		cx = (itr->areax[0] + itr->areax[1]) / 2;
		cy = (itr->areay[0] + itr->areay[1]) / 2;

		std::pair<int, int>id;
		id.first = cx / hash_size;
		id.second = cy / hash_size;
		corr_map.insert(std::make_pair(id, *itr));
	}

}
void basetrack_tans_affin_extra(Basetrack* pred, Basetrack* base, corrmap0::Corrmap corr) {
	double x_tmp, y_tmp;
	x_tmp = pred->x;
	y_tmp = pred->y;
	pred->x = x_tmp * corr.position[0] + y_tmp * corr.position[1] + corr.position[4];
	pred->y = x_tmp * corr.position[2] + y_tmp * corr.position[3] + corr.position[5];

	x_tmp = pred->ax;
	y_tmp = pred->ay;
	pred->ax = x_tmp * corr.angle[0] + y_tmp * corr.angle[1] + corr.angle[4];
	pred->ay = x_tmp * corr.angle[2] + y_tmp * corr.angle[3] + corr.angle[5];

	base->z = base->z + corr.dz;

	pred->x = pred->x + pred->ax*(pred->z - base->z);
	pred->y = pred->y + pred->ay*(pred->z - base->z);
}

std::vector<Prediction_hit> search_basetrack(std::vector<Prediction>&pred, std::string file_path) {
	std::vector<Prediction_hit> pred_hit;
	std::multimap<int, Prediction_hit*> pred_hit_map;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		itr->ax = (itr->pred[0].ax + itr->pred[1].ax) / 2;
		itr->ay = (itr->pred[0].ay + itr->pred[1].ay) / 2;
		itr->x = (itr->pred[0].x + itr->pred[1].x) / 2;
		itr->y = (itr->pred[0].y + itr->pred[1].y) / 2;
		itr->x = (itr->pred[0].z + itr->pred[1].z) / 2;
		Prediction_hit pred_hit_tmp;
		pred_hit_tmp.pl = itr->pl;
		pred_hit_tmp.rawid = itr->rawid;
		pred_hit_tmp.chainid = itr->chainid;
		pred_hit_tmp.base[0] = itr->base[0];
		pred_hit_tmp.base[1] = itr->base[1];
		pred_hit_tmp.pred[0] = itr->pred[0];
		pred_hit_tmp.pred[1] = itr->pred[1];
		pred_hit_tmp.ax = itr->ax;
		pred_hit_tmp.ay = itr->ay;
		pred_hit_tmp.x = itr->x;
		pred_hit_tmp.y = itr->y;
		pred_hit_tmp.z = itr->z;

		pred_hit.push_back(pred_hit_tmp);
	}
	for (auto itr = pred_hit.begin(); itr != pred_hit.end(); itr++) {
		pred_hit_map.insert(std::make_pair(itr->pl, &(*itr)));
	}

	std::vector<vxx::base_track_t> base;
	vxx::BvxxReader br;
	int pl, count = 0;
	for (auto itr = pred_hit_map.begin(); itr != pred_hit_map.end(); itr++) {
		base.clear();

		pl = itr->first;
		count = pred_hit_map.count(pl);
		auto range = pred_hit_map.equal_range(pl);
		std::vector<Prediction_hit*> pred_v;
		for (auto res = range.first; res != range.second; res++) {
			pred_v.push_back(res->second);
		}
		std::stringstream file_base;
		file_base << file_path << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl
			<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
		base = br.ReadAll(file_base.str(), pl, 0);
		base_matching(pred_v, base);

		itr = std::next(itr, count - 1);

	}

	return pred_hit;
}
void base_matching(std::vector<Prediction_hit*> &pred_v, std::vector<vxx::base_track_t> &base) {
	std::multimap<std::pair<int, int>, vxx::base_track_t*> base_map;
	double hash = 1000;
	double x_min, y_min;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		y_min = std::min(itr->y, y_min);
	}
	std::pair<int, int> id;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		id.first = (itr->x - x_min) / hash;
		id.second = (itr->y - y_min) / hash;
		base_map.insert(std::make_pair(id, &(*itr)));
	}

	int ix, iy;
	double angle, all_pos[2], all_ang[2], diff_pos[2], diff_ang[2];

	for (auto itr = pred_v.begin(); itr != pred_v.end(); itr++) {
		for (int i = 0; i < 2; i++) {
			ix = ((*itr)->pred[i].x - x_min) / hash;
			iy = ((*itr)->pred[i].y - y_min) / hash;
			angle = sqrt((*itr)->pred[i].ax*(*itr)->pred[i].ax + (*itr)->pred[i].ay*(*itr)->pred[i].ay);
			all_pos[0] = 10 + angle * 10;
			all_pos[1] = 10;
			all_ang[0] = 0.025 + angle * 0.025;
			all_ang[1] = 0.025;
			(*itr)->hit[i] = 0;

			for (int iix = -1; iix <= 1; iix++) {
				for (int iiy = -1; iiy <= 1; iiy++) {

					id.first = ix + iix;
					id.second = iy + iiy;
					if (base_map.count(id) == 0)continue;
					auto range = base_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						if (angle > 0.01) {
							diff_pos[0] = (res->second->x - (*itr)->pred[i].x)*(*itr)->pred[i].ax + (res->second->y - (*itr)->pred[i].y)*(*itr)->pred[i].ay;
							diff_pos[1] = (res->second->x - (*itr)->pred[i].x)*(*itr)->pred[i].ay - (res->second->y - (*itr)->pred[i].y)*(*itr)->pred[i].ax;

							if (fabs(diff_pos[0]) > all_pos[0] * angle)continue;
							if (fabs(diff_pos[1]) > all_pos[1] * angle)continue;

							diff_ang[0] = (res->second->ax - (*itr)->pred[i].ax)*(*itr)->pred[i].ax + (res->second->ay - (*itr)->pred[i].ay)*(*itr)->pred[i].ay;
							diff_ang[1] = (res->second->ax - (*itr)->pred[i].ax)*(*itr)->pred[i].ay - (res->second->ay - (*itr)->pred[i].ay)*(*itr)->pred[i].ax;

							if (fabs(diff_ang[0]) > all_ang[0] * angle)continue;
							if (fabs(diff_ang[1]) > all_ang[1] * angle)continue;

							(*itr)->hit_base[i].pl = res->second->pl;
							(*itr)->hit_base[i].rawid = res->second->rawid;
							(*itr)->hit_base[i].ph = res->second->m[0].ph + res->second->m[1].ph;
							(*itr)->hit_base[i].ax = res->second->ax;
							(*itr)->hit_base[i].ay = res->second->ay;
							(*itr)->hit_base[i].x = res->second->x;
							(*itr)->hit_base[i].y = res->second->y;
							(*itr)->hit[i] = 1;
						}
						else {
							diff_pos[0] = (res->second->x - (*itr)->pred[i].x);
							diff_pos[1] = (res->second->y - (*itr)->pred[i].y);
							if (fabs(diff_pos[0]) > all_pos[1])continue;
							if (fabs(diff_pos[1]) > all_pos[1])continue;

							diff_ang[0] = (res->second->ax - (*itr)->pred[i].ax);
							diff_ang[1] = (res->second->ay - (*itr)->pred[i].ay);
							if (fabs(diff_ang[0]) > all_ang[1])continue;
							if (fabs(diff_ang[1]) > all_ang[1])continue;

							(*itr)->hit_base[i].pl = res->second->pl;
							(*itr)->hit_base[i].rawid = res->second->rawid;
							(*itr)->hit_base[i].ph = res->second->m[0].ph + res->second->m[1].ph;
							(*itr)->hit_base[i].ax = res->second->ax;
							(*itr)->hit_base[i].ay = res->second->ay;
							(*itr)->hit_base[i].x = res->second->x;
							(*itr)->hit_base[i].y = res->second->y;
							(*itr)->hit[i] = 1;

						}

					}

				}
			}
		}
	}
}

void output_pred(std::string file_path, std::vector<Prediction_hit > &pred_v) {

	std::stringstream pred_inf, base_diff;
	pred_inf << file_path << "\\pred_inf.txt";
	base_diff << file_path << "\\pred_hitbase_diff.txt";
	output_pred_inf(pred_inf.str(), pred_v);
	output_pred_base_diff(base_diff.str(), pred_v);
}
void output_pred_inf(std::string filename, std::vector<Prediction_hit > &pred_v) {
	std::ofstream ofs(filename);
	int count = 0;
	for (auto &p : pred_v) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r Write file ... %d/%d (%4.1lf%%)", count, int(pred_v.size()), count*100. / pred_v.size());
		}
		count++;
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << p.pl << " "
			<< std::setw(12) << std::setprecision(0) << p.rawid << " "
			<< std::setw(1) << std::setprecision(0) << p.hit[0] << " "
			<< std::setw(7) << std::setprecision(4) << p.pred[0].ax << " "
			<< std::setw(7) << std::setprecision(4) << p.pred[0].ay << " "
			<< std::setw(8) << std::setprecision(1) << p.pred[0].x << " "
			<< std::setw(8) << std::setprecision(1) << p.pred[0].y << " "
			<< std::setw(8) << std::setprecision(1) << p.pred[0].z << " "
			<< std::setw(3) << std::setprecision(0) << p.base[0].pl << " "
			<< std::setw(8) << std::setprecision(1) << p.base[0].z << " "

			<< std::setw(1) << std::setprecision(0) << p.hit[1] << " "
			<< std::setw(7) << std::setprecision(4) << p.pred[1].ax << " "
			<< std::setw(7) << std::setprecision(4) << p.pred[1].ay << " "
			<< std::setw(8) << std::setprecision(1) << p.pred[1].x << " "
			<< std::setw(8) << std::setprecision(1) << p.pred[1].y << " "
			<< std::setw(8) << std::setprecision(1) << p.pred[1].z << " "
			<< std::setw(3) << std::setprecision(0) << p.base[1].pl << " "
			<< std::setw(8) << std::setprecision(1) << p.base[1].z << std::endl;
	}
	fprintf(stderr, "\r Write file ... %d/%d (%4.1lf%%)\n", count, int(pred_v.size()), count*100. / pred_v.size());

}
void output_pred_base_diff(std::string filename, std::vector<Prediction_hit > &pred_v) {
	std::ofstream ofs(filename);
	int count = 0;
	for (auto &p : pred_v) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r Write file ... %d/%d (%4.1lf%%)", count, int(pred_v.size()), count*100. / pred_v.size());
		}
		count++;
		for (int i = 0; i < 2; i++) {
			if (p.hit[i] == 0)continue;
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << p.pl << " "
				<< std::setw(12) << std::setprecision(0) << p.rawid << " "
				<< std::setw(2) << std::setprecision(0) << i << " "
				<< std::setw(7) << std::setprecision(4) << p.pred[i].ax << " "
				<< std::setw(7) << std::setprecision(4) << p.pred[i].ay << " "
				<< std::setw(8) << std::setprecision(1) << p.pred[i].x << " "
				<< std::setw(8) << std::setprecision(1) << p.pred[i].y << " "
				<< std::setw(8) << std::setprecision(1) << p.pred[i].z << " "
				<< std::setw(3) << std::setprecision(0) << p.base[i].pl << " "
				<< std::setw(8) << std::setprecision(1) << p.base[i].z << " "

				<< std::setw(4) << std::setprecision(0) << p.hit_base[i].pl << " "
				<< std::setw(12) << std::setprecision(0) << p.hit_base[i].rawid << " "
				<< std::setw(7) << std::setprecision(4) << p.hit_base[i].ax << " "
				<< std::setw(7) << std::setprecision(4) << p.hit_base[i].ay << " "
				<< std::setw(8) << std::setprecision(1) << p.hit_base[i].x << " "
				<< std::setw(8) << std::setprecision(1) << p.hit_base[i].y << std::endl;
		}
	}
	fprintf(stderr, "\r Write file ... %d/%d (%4.1lf%%)\n", count, int(pred_v.size()), count*100. / pred_v.size());

}
void out_detected(std::vector<Prediction_hit > &pred_v) {
	uint64_t all = 0, hit = 0;
	for (auto p : pred_v) {
		all++;
		if (p.hit[0] == 1 || p.hit[1] == 1) {
			hit++;
		}
	}
	printf("all %lld\n", all);
	printf("hit %lld\n", hit);
	//all 429526
	//hit 153569
}

void Fill_detected_track(mfile1::MFile_minimum &m, std::vector<Prediction_hit> &pred_hit, std::map<int, double> z_map, std::vector<corrmap0::Corrmap> &corr_abs) {

	printf("N Chain %lld\n", m.info_header.Nchain);
	printf("N Base %lld\n", m.info_header.Nbasetrack);

	std::map<int, corrmap0::Corrmap> corr;
	for (auto p : corr_abs) {
		corr.insert(std::make_pair(p.pos[0] / 10, p));
	}
	std::multimap<uint64_t, mfile1::MFileBase> fill_base;
	int i;
	for (auto itr = pred_hit.begin(); itr != pred_hit.end(); itr++) {

		if (itr->hit[0] + itr->hit[1] == 0)continue;
		else if (itr->hit[0] + itr->hit[1] == 1) {
			if (itr->hit[0] == 1)i = 0;
			else i = 1;
		}
		else {
			//gap小さいほう使う
			if (fabs(itr->pred[0].z - itr->base[0].z) < fabs(itr->pred[1].z - itr->base[1].z))i = 0;
			else i = 1;
		}

		auto param = corr.at(itr->pred[i].pl);
		auto z = z_map.at(itr->pred[i].pl);
		mfile1::MFileBase b;

		b.pos = itr->pl * 10;
		b.group_id = 0;
		b.ph = itr->hit_base[i].ph;
		b.rawid = itr->hit_base[i].rawid;
		b.z = z;

		b.x = itr->hit_base[i].x*param.position[0] + itr->hit_base[i].y*param.position[1] + param.position[4];
		b.y = itr->hit_base[i].x*param.position[2] + itr->hit_base[i].y*param.position[3] + param.position[5];
		b.ax = itr->hit_base[i].ax*param.angle[0] + itr->hit_base[i].ay*param.angle[1] + param.angle[4];
		b.ay = itr->hit_base[i].ax*param.angle[2] + itr->hit_base[i].ay*param.angle[3] + param.angle[5];

		fill_base.insert(std::make_pair(itr->chainid, b));

	}
	printf("fill base num=%d\n", fill_base.size());

	int count = 0,loop_c=0;
	for (int c = 0; c < m.all_basetracks.size(); c++) {
		if (loop_c % 10000 == 0) {
			fprintf(stderr, "\r inefficiency base fill %d/%d(%4.1lf%%)", loop_c, m.all_basetracks.size(), loop_c*100. / m.all_basetracks.size());
		}
		loop_c++;

		mfile1::MFileChain* c_p = &(m.chains[c]);
		count = fill_base.count(c_p->chain_id);
		if (count == 0)continue;
		std::vector<mfile1::MFileBase> chain;
		auto range = fill_base.equal_range(c_p->chain_id);
		for (auto b = m.all_basetracks[c].begin(); b != m.all_basetracks[c].end(); b++) {
			//inefficiencyのtrackを抽出
			chain.push_back(*b);
			if (b == m.all_basetracks[c].begin()) continue;
			else if (b + 1 == m.all_basetracks[c].end()) continue;
			else if ((b + 1)->pos / 10 - b->pos / 10 == 1) continue;
			else {
				for (int pl = b->pos / 10 + 1; pl < (b + 1)->pos / 10; pl++) {
					for (auto itr = range.first; itr != range.second; itr++) {
						if (itr->second.pos / 10 == pl) {
							itr->second.group_id = b->group_id;
							chain.push_back(itr->second);
							break;
						}
					}
				}
			}
		}
		c_p->nseg = chain.size();
		m.all_basetracks[c].swap(chain);
	}
	fprintf(stderr, "\r inefficiency base fill %d/%d(%4.1lf%%)\n", loop_c, m.all_basetracks.size(), loop_c*100. / m.all_basetracks.size());



	uint64_t N_base = 0, N_chain = 0;
	for (auto c : m.all_basetracks) {
		N_base += c.size();
		N_chain++;
	}
	m.info_header.Nbasetrack = N_base;
	m.info_header.Nchain = N_chain;

	printf("N Chain %lld\n", m.info_header.Nchain);
	printf("N Base %lld\n", m.info_header.Nbasetrack);

}
