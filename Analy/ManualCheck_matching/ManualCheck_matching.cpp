#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include <functions.hpp>
#include <VxxReader.h>
class UTS_track {
public:
	int view, rawid, ph;
	double ax, ay, x, y;
//	0         1 110015  0.0056 - 0.0002 - 87472.5 - 147154.3

};
class Matched_track {
	vxx::micro_track_t h;
	UTS_track u;
	double dx, dy;
};

std::vector<UTS_track> read_UTS_track(std::string filename);
void uts_track_area(std::vector<UTS_track>&t, double &xmin, double &xmax, double &ymin, double &ymax);

bool judge_matching(UTS_track &t_u, vxx::micro_track_t&t_h);
bool judge_matching(UTS_track t_u, vxx::micro_track_t&t_h, double dx, double dy);
vxx::base_track_t output_base(UTS_track &t, int pl, int &rawid);
vxx::base_track_t output_base(vxx::micro_track_t &t);
std::vector<UTS_track> PH_cut(std::vector<UTS_track>&t);


int main(int argc, char **argv) {
	if (argc != 7) {
		fprintf(stderr, "prg input-UTS-track input-fvxx pos x y width\n");
		exit(1);
		//x-center,y-center,widthも後から入れる
	}
	std::string file_in_uts = argv[1];
	std::string file_in_fvxx = argv[2];
	int pos = std::stoi(argv[3]);
	double x = std::stod(argv[4]) * 1000;
	double y = std::stod(argv[5]) * 1000;
	double width = std::stod(argv[6]) * 1000;

	//uts track読み込み
	std::vector<UTS_track> uts_track = read_UTS_track(file_in_uts);
	//uts area測定
	double uts_xmin, uts_xmax, uts_ymin, uts_ymax;
	uts_track_area(uts_track, uts_xmin, uts_xmax, uts_ymin, uts_ymax);
	printf("%.1lf %.1lf %.1lf %.1lf\n", uts_xmin, uts_xmax, uts_ymin, uts_ymax);
	printf("%.1lf  %.1lf\n", uts_xmax-uts_xmin, uts_ymax - uts_ymin);
	//hts area決定
	double hts_xmin = x - width;
	double hts_xmax = x + width;
	double hts_ymin = y - width;
	double hts_ymax = y + width;
	uts_track = PH_cut(uts_track);

	//hts track読み込み
	vxx::FvxxReader fr;
	std::vector<vxx::CutArea> area;
	area.push_back(vxx::CutArea(hts_xmin, hts_xmax, hts_ymin, hts_ymax, -0.6, 0.6, -0.6, 0.6));//xmin, xmax, ymin, ymax, axmin, axmax, aymin, aymax
	std::vector<vxx::micro_track_t> micro = fr.ReadAll(file_in_fvxx, pos, 0, vxx::opt::a = area);

	std::ofstream ofs("output.txt");
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		for (auto itr2 = uts_track.begin(); itr2 != uts_track.end(); itr2++) {
			if (judge_matching(*itr2, *itr)) {
				ofs << std::right << std::fixed
					<< std::setw(8) << std::setprecision(0) << itr2->rawid << " "
					<< std::setw(8) << std::setprecision(0) << itr2->ph << " "
					<< std::setw(8) << std::setprecision(4) << itr2->ax << " "
					<< std::setw(8) << std::setprecision(4) << itr2->ay << " "
					<< std::setw(8) << std::setprecision(1) << itr2->x << " "
					<< std::setw(8) << std::setprecision(1) << itr2->y << " "
					<< std::setw(8) << std::setprecision(0) << itr->ph << " "
					<< std::setw(8) << std::setprecision(4) << itr->ax << " "
					<< std::setw(8) << std::setprecision(4) << itr->ay << " "
					<< std::setw(8) << std::setprecision(1) << itr->x << " "
					<< std::setw(8) << std::setprecision(1) << itr->y << std::endl;

			}
		}
	}


}

std::vector<UTS_track> read_UTS_track(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<UTS_track> ret;
	UTS_track t;
	while (ifs >> t.view >> t.rawid >> t.ph >> t.ax >> t.ay >> t.x >> t.y) {
		if (t.ph % 10000 < 2)continue;
		ret.push_back(t);
	}
	return ret;
}
std::vector<UTS_track> PH_cut(std::vector<UTS_track>&t) {
	std::vector<UTS_track> ret;
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		//if (itr->ph / 10000 < 10)continue;
		ret.push_back(*itr);
	}
	printf("uts track %d\n", ret.size());
	return ret;
}
void uts_track_area(std::vector<UTS_track>&t,double &xmin, double &xmax, double &ymin, double &ymax) {

	for (auto itr = t.begin(); itr != t.end(); itr++) {
		if (itr == t.begin()) {
			xmin = itr->x;
			xmax = itr->x;
			ymin = itr->y;
			ymax = itr->y;
		}
		xmin = std::min(xmin, itr->x);
		xmax = std::max(xmax, itr->x);
		ymin = std::min(ymin, itr->y);
		ymax = std::max(ymax, itr->y);
	}

}
bool judge_matching(UTS_track &t_u, vxx::micro_track_t&t_h) {
	if (fabs(t_u.ax - t_h.ax) > 0.3)return false;
	if (fabs(t_u.ay - t_h.ay) > 0.3)return false;
	return true;
}
bool judge_matching(UTS_track t_u, vxx::micro_track_t&t_h,double dx,double dy) {
	if (fabs(t_u.ax - t_h.ax) > 0.3)return false;
	if (fabs(t_u.ay - t_h.ay) > 0.3)return false;
	t_u.x = t_u.x + dx;
	t_u.y = t_u.y + dy;
	if (fabs(t_u.x - t_h.x) > 100)return false;
	if (fabs(t_u.y - t_h.y) > 100)return false;

	return true;
}



vxx::base_track_t output_base(UTS_track &t, int pl,int &rawid) {
	vxx::base_track_t ret;
	ret.ax = t.ax;
	ret.ay = t.ay;
	ret.pl = pl;
	ret.rawid = rawid;
	ret.isg = 0;
	ret.x = t.x;
	ret.y = t.y;
	ret.z = 0;
	ret.zone = 0;
	for (int i = 0; i < 2; i++) {
		ret.m[i].ax = ret.ax;
		ret.m[i].ay = ret.ay;
		ret.m[i].col = 0;
		ret.m[i].isg = rawid;
		ret.m[i].ph = t.ph;
		ret.m[i].pos = pl * 10 + i + 1;
		ret.m[i].rawid = rawid;
		ret.m[i].row = 0;
		ret.m[i].z = 210 * i;
		ret.m[i].zone = 0;
	}
	rawid++;

	return ret;
}
vxx::base_track_t output_base(vxx::micro_track_t &t) {
	vxx::base_track_t ret;
	ret.ax = t.ax;
	ret.ay = t.ay;
	ret.pl = t.pos/10;
	ret.rawid = t.rawid;
	ret.isg = 0;
	ret.x = t.x;
	ret.y = t.y;
	ret.z = 0;
	ret.zone = 0;
	for (int i = 0; i < 2; i++) {
		ret.m[i].ax = ret.ax;
		ret.m[i].ay = ret.ay;
		ret.m[i].col = 0;
		ret.m[i].isg = t.rawid;
		ret.m[i].ph = t.ph;
		ret.m[i].pos = ret.pl * 10 + i + 1;
		ret.m[i].rawid = t.rawid;
		ret.m[i].row = 0;
		ret.m[i].z = 210 * i;
		ret.m[i].zone = 0;
	}

	return ret;

}
