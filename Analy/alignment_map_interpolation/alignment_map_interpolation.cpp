#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "functions.lib")
#include <functions.hpp>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <chrono>
#include <filesystem>
#include <set>
#include <omp.h>
#include <thread>


class align_param {
public:
	int id, signal, ix, iy;
	//視野中心
	double x, y, z;
	//parameter(9);
	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;

};
bool sort_ali_param(const align_param&left, const align_param&right) {
	if (left.ix == right.ix)return left.iy < right.iy;
	return left.ix < right.ix;
}

std::vector<align_param> read_ali_param(std::string filename, bool output);
void add_corrmap_edge(std::vector<align_param> &corr, std::vector<vxx::base_track_t> &base, double extra_range);
align_param make_param_bilinear(std::tuple<int, int, double, double> &edge_point, std::map<std::pair<int, int>, align_param> &near_param, double nominal_gap);
void GaussJorden(double in[3][3], double b[3], double c[3]);
void output_corrmap(std::string filename, std::vector<align_param> &corr);
double Calc_bilinear(std::pair<double, double> point, std::map<std::pair<double, double>, double> &val_map, bool output_flg);
double Calc_average_dist(std::pair<double, double> point, std::map<std::pair<double, double>, double> &val_map, bool output_flg);

int main(int argc, char**argv) {

	if (argc != 6) {
		fprintf(stderr, "usage:prg input-ali in-bvxx-pl1 pl extra_range out-ali\n");
		exit(1);
	}
	std::string file_in_ali = argv[1];
	std::string file_in_bvxx = argv[2];
	std::string file_out_ali = argv[5];
	int pl = std::stoi(argv[3]);
	double extra_range = std::stod(argv[4]);

	std::vector<align_param>ali_param = read_ali_param(file_in_ali,false);
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);

	add_corrmap_edge(ali_param, base, extra_range);

	output_corrmap(file_out_ali, ali_param);

}

std::vector<align_param> read_ali_param(std::string filename, bool output) {

	std::vector<align_param> ret;
	align_param param_tmp;
	std::ifstream ifs(filename);

	while (ifs >> param_tmp.id >> param_tmp.ix >> param_tmp.iy >> param_tmp.signal
		>> param_tmp.x >> param_tmp.y >> param_tmp.z
		>> param_tmp.x_rot >> param_tmp.y_rot >> param_tmp.z_rot
		>> param_tmp.x_shrink >> param_tmp.y_shrink >> param_tmp.z_shrink
		>> param_tmp.yx_shear >> param_tmp.zx_shear >> param_tmp.zy_shear
		>> param_tmp.dx >> param_tmp.dy >> param_tmp.dz) {
		ret.push_back(param_tmp);
		//printf("ix %d iy%d\n", param_tmp.ix, param_tmp.iy);

	}
	if (output == 1) {
		fprintf(stderr, "%s input finish\n", filename.c_str());
	}
	if (ret.size() == 0) {
		fprintf(stderr, "%s alignment miss!\n", filename.c_str());
		exit(1);
	}
	return ret;

}

void add_corrmap_edge(std::vector<align_param> &corr, std::vector<vxx::base_track_t> &base,double extra_range) {

	std::map<std::pair<int, int>, align_param> ali_map;
	double xmin, ymin, xmax, ymax;
	int corr_ix_min, corr_ix_max, corr_iy_min, corr_iy_max;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		ali_map.insert(std::make_pair(std::make_pair(itr->ix, itr->iy), *itr));

		if (itr == corr.begin()) {
			xmin = itr->x;
			ymin = itr->y;
			xmax = itr->x;
			ymax = itr->y;
			corr_ix_min = itr->ix;
			corr_iy_min = itr->iy;
			corr_ix_max = itr->ix;
			corr_iy_max = itr->iy;

		}
		if (xmin > itr->x) {
			xmin = itr->x;
			corr_ix_min = itr->ix;
		}
		if (ymin > itr->y) {
			ymin = itr->y;
			corr_iy_min = itr->iy;
		}
		if (xmax < itr->x) {
			xmax = itr->x;
			corr_ix_max = itr->ix;
		}
		if (ymax < itr->y) {
			ymax = itr->y;
			corr_iy_max = itr->iy;
		}
	}
	printf("corrmap size\n");
	printf("%g %g %g %g\n", xmin, xmax, ymin, ymax);
	printf("%4d %4d %4d %4d\n", corr_ix_min, corr_ix_max, corr_iy_min, corr_iy_max);





	double corr_xmin, corr_xmax, corr_ymin, corr_ymax;
	corr_xmin = xmin;
	corr_xmax = xmax;
	corr_ymin = ymin;
	corr_ymax = ymax;


	for (auto itr = base.begin(); itr != base.end(); itr++) {

		xmin = std::min(itr->x, xmin);
		ymin = std::min(itr->y, ymin);
		xmax = std::max(itr->x, xmax);
		ymax = std::max(itr->y, ymax);
	}

	printf("corrmap size+base size \n");
	printf("%g %g %g %g\n", xmin, xmax, ymin, ymax);

	xmin -= extra_range;
	ymin -= extra_range;
	xmax += extra_range;
	ymax += extra_range;

	double step = 2000;
	int ix_min, ix_max, iy_min, iy_max;
	ix_min = corr_ix_min-(int((corr_xmin - xmin) / step) + 1);
	iy_min = corr_iy_min -( int((corr_ymin - ymin) / step) + 1);
	ix_max = corr_ix_max+(int((xmax - corr_xmax) / step) + 1 );
	iy_max = corr_iy_max +(int((ymax - corr_ymax) / step) + 1);
	printf("add id(x,y)\n");
	printf("%4d %4d %4d %4d\n", ix_min, ix_max, iy_min, iy_max);

	std::vector<std::tuple<int, int, double, double>> edge_point;
	for (int ix = ix_min; ix <= ix_max; ix++) {
		for (int iy = iy_min; iy <= iy_max; iy++) {
			if (ali_map.count(std::make_pair(ix, iy)) == 1)continue;

			std::tuple<int, int, double, double> add_edge;
			std::get<0>(add_edge) = ix;
			std::get<1>(add_edge) = iy;

			//追加点の内外判定
			bool ix_up_flg = false;
			bool ix_down_flg = false;
			bool iy_up_flg = false;
			bool iy_down_flg = false;

			for (auto itr = corr.begin(); itr != corr.end(); itr++) {
				if (itr->ix == ix) {
					if (itr->iy < iy)iy_down_flg = true;
					if (itr->iy > iy)iy_up_flg = true;
				}
				if (itr->iy == iy) {
					if (itr->ix < ix)ix_down_flg = true;
					if (itr->ix > ix)ix_up_flg = true;
				}
			}
			//内部の点
			if (ix_down_flg&&ix_up_flg&&iy_down_flg&&iy_up_flg)continue;

			bool  nearest_neighbor_flg = false;
			int loop_num = 1;
			while (true) {
				for (int ix2 = ix - loop_num; ix2 <= ix + loop_num; ix2++) {
					for (int iy2 = iy - loop_num; iy2 <= iy + loop_num; iy2++) {
						auto res = ali_map.find(std::make_pair(ix2, iy2));
						if (res == ali_map.end())continue;

						std::get<2>(add_edge) = res->second.x + (ix - res->first.first)*step;
						std::get<3>(add_edge) = res->second.y + (iy - res->first.second)*step;
						nearest_neighbor_flg = true;
						if (nearest_neighbor_flg)break;
					}
					if (nearest_neighbor_flg)break;
				}
				if (nearest_neighbor_flg)break;
				loop_num++;
			}
			edge_point.push_back(add_edge);
		}
	}

	std::map<std::pair<int, int>, align_param> near_param;
	std::vector<align_param> add_cand;
	double nominal_gap = corr.begin()->z;

	int calc_num = 0;
	int count = 0;
	for (auto itr = edge_point.begin(); itr != edge_point.end(); itr++) {
		printf("\r add align param %d/%d", count, edge_point.size());
		count++;

		//if (count > 2)continue;
		//printf("\n");

		near_param.clear();
		int loop_num = 1;
		while (true) {
			for (int ix = std::get<0>(*itr) - loop_num; ix <= std::get<0>(*itr) + loop_num; ix++) {
				for (int iy = std::get<1>(*itr) - loop_num; iy <= std::get<1>(*itr) + loop_num; iy++) {
					if (ali_map.count(std::make_pair(ix, iy)) == 0)continue;
					auto res = ali_map.find(std::make_pair(ix, iy));
					near_param.insert(*res);
				}
			}

			if (near_param.size() < 3) {
				loop_num++;
				continue;
			}


			bool ix_check = false;
			bool iy_check = false;

			int ix_ini = near_param.begin()->first.first;
			int iy_ini = near_param.begin()->first.second;

			int dx_ini = std::next(near_param.begin(), 1)->first.first - ix_ini;
			int dy_ini = std::next(near_param.begin(), 1)->first.second - iy_ini;

			bool linearly_independent_flg = false;
			int dx, dy;
			for (auto itr = std::next(near_param.begin(), 1); itr != near_param.end(); itr++) {
				dx = itr->first.first - ix_ini;
				dy = itr->first.second - iy_ini;

				if (dx_ini == 0) {
					if (dx != 0) linearly_independent_flg = true;
				}
				else if (dy_ini == 0) {
					if (dy != 0)linearly_independent_flg = true;
				}
				else {
					if (dx*dy_ini != dy * dx_ini)linearly_independent_flg = true;
				}
			}
			if (!linearly_independent_flg) {
				loop_num++;
				continue;
			}
			else {

				add_cand.push_back(make_param_bilinear(*itr, near_param, nominal_gap));
				break;
			}
		}
	}
	printf("\r add align param %d/%d\n", count, edge_point.size());

	for (auto itr = add_cand.begin(); itr != add_cand.end(); itr++) {
		corr.push_back(*itr);
	}
	sort(corr.begin(), corr.end(), sort_ali_param);


	for (int i = 0; i < corr.size(); i++) {
		corr[i].id = i;
	}


}
align_param make_param_bilinear(std::tuple<int, int, double, double> &edge_point,std::map<std::pair<int, int>, align_param> &near_param,double nominal_gap) {

	align_param ret;
	//near paramを使って bilinearで補間
	//edge_point ix,iy,x,yの組
	ret.ix = std::get<0>(edge_point);
	ret.iy = std::get<1>(edge_point);
	ret.x = std::get<2>(edge_point);
	ret.y = std::get<3>(edge_point);
	ret.z = nominal_gap;


	std::map<std::pair<double, double>, double> dx, dy, dz, x_shrink, y_shrink, z_shrink, x_rot, y_rot, z_rot, yx_shear, zx_shear, zy_shear;
	for (auto itr = near_param.begin(); itr != near_param.end(); itr++) {
		x_rot.insert(std::make_pair(std::make_pair(itr->second.x, itr->second.y), itr->second.x_rot));
		y_rot.insert(std::make_pair(std::make_pair(itr->second.x, itr->second.y), itr->second.y_rot));
		z_rot.insert(std::make_pair(std::make_pair(itr->second.x, itr->second.y), itr->second.z_rot));
		x_shrink.insert(std::make_pair(std::make_pair(itr->second.x, itr->second.y), itr->second.x_shrink));
		y_shrink.insert(std::make_pair(std::make_pair(itr->second.x, itr->second.y), itr->second.y_shrink));
		z_shrink.insert(std::make_pair(std::make_pair(itr->second.x, itr->second.y), itr->second.z_shrink));
		yx_shear.insert(std::make_pair(std::make_pair(itr->second.x, itr->second.y), itr->second.yx_shear));
		zx_shear.insert(std::make_pair(std::make_pair(itr->second.x, itr->second.y), itr->second.zx_shear));
		zy_shear.insert(std::make_pair(std::make_pair(itr->second.x, itr->second.y), itr->second.zy_shear));
	}

	std::pair<double, double> point;
	point.first = std::get<2>(edge_point);
	point.second = std::get<3>(edge_point);

	ret.x_rot = Calc_average_dist(point, x_rot, false);
	ret.y_rot = Calc_average_dist(point, y_rot, false);
	ret.z_rot = Calc_average_dist(point, z_rot, false);
	ret.x_shrink = Calc_average_dist(point, x_shrink, false);
	ret.y_shrink = Calc_average_dist(point, y_shrink, false);
	ret.z_shrink = Calc_average_dist(point, z_shrink, false);
	ret.yx_shear = Calc_average_dist(point, yx_shear, false);
	ret.zx_shear = Calc_average_dist(point, yx_shear, false);
	ret.zy_shear = Calc_average_dist(point, yx_shear, false);

	double tmpx, tmpy, rotx,roty;
	for (auto itr = near_param.begin(); itr != near_param.end(); itr++) {
		tmpx = itr->second.x*cos(itr->second.z_rot) - itr->second.y*sin(itr->second.z_rot)*itr->second.x_shrink;
		tmpy = (itr->second.x*sin(itr->second.z_rot) + itr->second.y*cos(itr->second.z_rot))*itr->second.y_shrink;

		dx.insert(std::make_pair(std::make_pair(tmpx, tmpy), itr->second.dx));
		dy.insert(std::make_pair(std::make_pair(tmpx, tmpy), itr->second.dy));
		dz.insert(std::make_pair(std::make_pair(tmpx, tmpy), itr->second.dz));
	}
	std::pair<double, double> point2 = point;
	point2.first = point.first*cos(ret.z_rot) - point.second*sin(ret.z_rot);
	point2.second = point.first*sin(ret.z_rot) + point.second*cos(ret.z_rot);
	ret.dz = Calc_bilinear(point2, dz, false);
	ret.dx = Calc_average_dist(point, dx, false);
	ret.dy = Calc_average_dist(point, dy, false);

	ret.signal = -1;
	
	return ret;
}
double Calc_bilinear(std::pair<double, double> point, std::map<std::pair<double, double>, double> &val_map,bool output_flg) {

	//計算
	double x2, y2, z2, x, y, z, xy, yz, zx,n;
	x2 = 0;
	y2 = 0;
	z2 = 0;
	x = 0; 
	y = 0;
	z = 0;
	xy = 0;
	yz = 0;
	zx = 0;
	n = 0;
	for (auto itr = val_map.begin(); itr != val_map.end(); itr++) {
		if (output_flg) {
			printf("%g %g %g\n", itr->first.first, itr->first.second, itr->second);
		}

		x += itr->first.first;
		y += itr->first.second;
		z += itr->second;

		x2+= itr->first.first*itr->first.first;
		y2+= itr->first.second*itr->first.second;
		z2 += itr->second*itr->second;

		xy += itr->first.first*itr->first.second;
		yz += itr->first.second*itr->second;
		zx += itr->second*itr->first.first;

		n++;
	}
	double in[3][3] = { {x2,xy,x},{xy,y2,y},{x,y,n} }, vec[3] = { zx,yz,z }, out[3] = {};
	GaussJorden(in, vec, out);


	double ret;
	ret = out[0] * point.first + out[1] * point.second + out[2];
	if (output_flg) {
		printf("%g %g %g\n", point.first, point.second, ret);
	}
	return ret;

}
double Calc_average_dist(std::pair<double, double> point, std::map<std::pair<double, double>, double> &val_map, bool output_flg) {
	//計算
	double val_sum, dist, dist_sum ;
	val_sum = 0;
	dist_sum = 0;
	for (auto itr = val_map.begin(); itr != val_map.end(); itr++) {
		if (output_flg) {
			printf("%g %g %g\n", itr->first.first, itr->first.second, itr->second);
		}
		dist = sqrt(pow(itr->first.first - point.first, 2) + pow(itr->first.second - point.second, 2));
		val_sum += itr->second / dist;
		dist_sum += 1 / dist;
	}


	double ret = val_sum / dist_sum;
	return ret;

}

void GaussJorden(double in[3][3], double b[3], double c[3]) {


	double a[3][4];
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			if (j < 3) {
				a[i][j] = in[i][j];
			}
			else {
				a[i][j] = b[i];
			}
		}
	}
	int N = 3;
	double p, d;         // ピボット係数、ピボット行ｘ係数
	double max, dummy;   // 最大絶対値、入れ替え時ダミー
	int s;

	//元の連立方程式をコンソール出力
   //for (int i = 0; i < N; i++) {
   //	for (int j = 0; j < N; j++)
   //		printf("%+fx%d ", a[i][j], j + 1);
   //	printf("= %+f\n", a[i][N]);
   //}

	for (int k = 0; k < N; k++) {
		// 行入れ替え
		max = 0; s = k;
		for (int j = k; j < N; j++) {
			if (fabs(a[j][k]) > max) {
				max = fabs(a[j][k]);
				s = j;
			}
		}
		if (max == 0) {
			printf("解けない！");
			exit(1);
		}
		for (int j = 0; j <= N; j++) {
			dummy = a[k][j];
			a[k][j] = a[s][j];
			a[s][j] = dummy;
		}

		// ピボット係数
		p = a[k][k];

		// ピボット行を p で除算
		for (int j = k; j < N + 1; j++)
			a[k][j] /= p;

		// ピボット列の掃き出し
		for (int i = 0; i < N; i++) {
			if (i != k) {
				d = a[i][k];
				for (int j = k; j < N + 1; j++)
					a[i][j] -= d * a[k][j];
			}
		}
	}

	// 結果出力
	for (int k = 0; k < N; k++) {
		c[k] = a[k][N];
		//printf("x%d = %f\n", k + 1, a[k][N]);
	}
}

void output_corrmap(std::string filename, std::vector<align_param> &corr) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (corr.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)", count, int(corr.size()), count*100. / corr.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(0) << itr->id << " "
				<< std::setw(4) << std::setprecision(0) << itr->ix << " "
				<< std::setw(4) << std::setprecision(0) << itr->iy << " "
				<< std::setw(4) << std::setprecision(0) << itr->signal << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << " "
				<< std::setw(8) << std::setprecision(1) << itr->z << " "
				<< std::setw(9) << std::setprecision(7) << itr->x_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->y_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->z_rot << " "
				<< std::setw(8) << std::setprecision(6) << itr->x_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->y_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->z_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->yx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zy_shear << " "
				<< std::setw(8) << std::setprecision(2) << itr->dx << " "
				<< std::setw(8) << std::setprecision(2) << itr->dy << " "
				<< std::setw(8) << std::setprecision(2) << itr->dz << std::endl;
		}
		fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)\n", count, int(corr.size()), count*100. / corr.size());

	}



}

