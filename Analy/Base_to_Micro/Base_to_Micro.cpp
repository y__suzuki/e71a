#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include <functions.hpp>
#include <VxxReader.h>

std::vector<std::pair<vxx::base_track_t, corrmap0::CorrmapDC>> corrmap_base_correspond(std::vector<corrmap0::CorrmapDC> &corr, std::vector<vxx::base_track_t>&base, int pos);
bool select_corrmap(std::vector<corrmap0::CorrmapDC> &corr, corrmap0::CorrmapDC &sel, double mx, double my, double dist_thr);
std::vector<vxx::micro_track_t> convert_base2micro(std::vector<std::pair<vxx::base_track_t, corrmap0::CorrmapDC>>&base, int pos);


int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage bvxx pos dc-map out-fvxx\n");
	}
	std::string file_in_bvxx = argv[1];
	int pos = std::stoi(argv[2]);
	int pl = pos / 10;
	std::string file_in_dc = argv[3];
	std::string file_out_fvxx = argv[4];

	std::vector<corrmap0::CorrmapDC> corr;
	corrmap0::read_cormap_dc(file_in_dc, corr);

	std::vector<vxx::base_track_t>base;
	vxx::BvxxReader br;
	base=br.ReadAll(file_in_bvxx, pl, 0);

	std::vector<std::pair<vxx::base_track_t, corrmap0::CorrmapDC>> base_corr = corrmap_base_correspond(corr, base, pos);
	std::vector<vxx::micro_track_t> micro=convert_base2micro(base_corr,pos);

	vxx::FvxxWriter fw;
	fw.Write(file_out_fvxx, pos, 0, micro);



}
std::vector<std::pair<vxx::base_track_t, corrmap0::CorrmapDC>> corrmap_base_correspond(std::vector<corrmap0::CorrmapDC> &corr, std::vector<vxx::base_track_t>&base, int pos) {
	double corr_x_min, corr_y_min;
	double hash_size = 5000;
	int count = 0;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (pos != itr->pos[0])continue;
		if (count == 0) {
			corr_x_min = itr->areax[0];
			corr_y_min = itr->areay[0];
			count++;
		}
		corr_x_min = std::min(corr_x_min, itr->areax[0]);
		corr_y_min = std::min(corr_y_min, itr->areay[0]);
	}

	std::multimap<std::pair<int, int>, corrmap0::CorrmapDC> corr_hash;
	std::pair<int, int> id;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (pos != itr->pos[0])continue;
		id.first = ((itr->areax[0] + itr->areax[1]) / 2 - corr_x_min) / hash_size;
		id.second = ((itr->areay[0] + itr->areay[1]) / 2 - corr_x_min) / hash_size;
		corr_hash.insert(std::make_pair(id, *itr));
	}

	std::vector<std::pair<vxx::base_track_t, corrmap0::CorrmapDC>> ret;
	std::vector<corrmap0::CorrmapDC> corr_buf;
	corrmap0::CorrmapDC sel;
	double mx, my;
	int ix, iy, loop;
	count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r corr-base correspond %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
		}
		count++;



		if (pos % 10 == 2) {
			mx = itr->x + itr->ax*(itr->m[1].z - itr->m[0].z);
			my = itr->y + itr->ay*(itr->m[1].z - itr->m[0].z);
		}
		else if (pos % 10 == 1) {
			mx = itr->x;
			my = itr->y;
		}
		else {
			fprintf(stderr, "pos errror : %d\n", pos);
		}

		ix = (mx - corr_x_min) / hash_size;
		iy = (my - corr_y_min) / hash_size;

		loop = 1;
		while (loop < 10) {
			corr_buf.clear();

			for (int iix = -1 * loop; iix <= loop; iix++) {
				for (int iiy = -1 * loop; iiy <= loop; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (corr_hash.count(id) == 0)continue;
					auto range = corr_hash.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						corr_buf.push_back(res->second);
					}

				}
			}
			if (select_corrmap(corr_buf, sel, mx, my, hash_size)) {
				ret.push_back(std::make_pair(*itr, sel));
				break;
			}

			loop++;
		}
		if (loop > 9) {
			fprintf(stderr, "corrmap not found\n");
			fprintf(stderr, "pos=%d rawid=%d\n", pos, itr->rawid);
		}
	}
	fprintf(stderr, "\r corr-base correspond %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

	return ret;
}
bool select_corrmap(std::vector<corrmap0::CorrmapDC> &corr, corrmap0::CorrmapDC &sel, double mx, double my, double dist_thr) {
	double dist = pow(dist_thr, 2);
	bool flg = false;
	double x, y;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		x = (itr->areax[0] + itr->areax[1]) / 2;
		y = (itr->areay[0] + itr->areay[1]) / 2;
		if (dist > pow(x - mx, 2) + pow(y - my, 2)) {
			flg = true;
			sel = *itr;
			dist = pow(x - mx, 2) + pow(y - my, 2);
		}
	}
	return flg;
}

std::vector<vxx::micro_track_t> convert_base2micro(std::vector<std::pair<vxx::base_track_t, corrmap0::CorrmapDC>>&base,int pos) {

	std::map<std::tuple<int,int,int>,vxx::micro_track_t> micro_map;

	vxx::micro_track_t m;
	vxx::micro_track_subset_t ms;
	int id = pos % 10 - 1;
	
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ms = itr->first.m[id];
		m.col = ms.col;
		m.row = ms.row;
		m.isg = ms.isg;
		m.ph = ms.ph;
		m.pos = ms.pos;
		m.rawid = ms.rawid;
		m.z = 0;
		m.z1 = 0;
		m.z2 = 60;
		m.zone = ms.zone;
		m.px = 0;
		m.py = 0;
		m.ax = (ms.ax - itr->second.dax) / itr->second.shr;
		m.ay = (ms.ay - itr->second.day) / itr->second.shr;
		if (id == 0) {
			m.x = itr->first.x - 30 * m.ax;
			m.y = itr->first.y - 30 * m.ay;
		}
		else {
			m.x = itr->first.x + itr->first.ax*(itr->first.m[1].z - itr->first.m[0].z) + 30 * m.ax;
			m.y = itr->first.y + itr->first.ay*(itr->first.m[1].z - itr->first.m[0].z) + 30 * m.ay;
		}
		micro_map.insert(std::make_pair(std::make_tuple(m.col, m.row, m.isg), m));
	}

	std::vector<vxx::micro_track_t> ret;
	for (auto itr = micro_map.begin(); itr != micro_map.end(); itr++) {
		ret.push_back(itr->second);
	}
	return ret;
}