#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"


int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in_fvxx pos zone out_fvxx\n");
		exit(1);
	}
	std::string file_in_fvxx = argv[1];
	int pos = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_fvxx = argv[4];
	std::vector<vxx::micro_track_t> micro;

	vxx::FvxxReader fr;
	int64_t count = 0;
	if (fr.Begin(file_in_fvxx, pos, zone))
	{
		vxx::HashEntry h;
		vxx::micro_track_t f;
		while (fr.NextHashEntry(h))
		{
			while (fr.NextMicroTrack(f))
			{
				if (count % 1000000 == 0) {
					fprintf(stderr, "\r fvxx filtering... %12lld", count);
				}
				count++;

				if (f.ph % 10000 == 2 && f.ph / 10000 == 6)continue;
				micro.push_back(f);

			}
		}
		fr.End();
	}
	fprintf(stderr, "\r fvxx filtering... %12lld fin\n", count);
	fprintf(stderr, "\r %12lld --> %lld (%4.1lf%%)\n", count, micro.size(), micro.size()*100. / count);

	vxx::FvxxWriter w;
	w.Write(file_out_fvxx, pos, zone, micro);
}
