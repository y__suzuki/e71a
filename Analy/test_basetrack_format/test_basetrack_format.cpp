#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>

void output_basetrack(std::string filename, std::vector<netscan::base_track_t> &base);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-bvxx pl zone out-txt\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_base = argv[4];

	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_base, base, pl, zone);

	output_basetrack(file_out_base, base);
	printf("fin\n");
}

void output_basetrack(std::string filename, std::vector<netscan::base_track_t> &base) {

	std::ofstream ofs(filename);
	int count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)", count, int(base.size()), count*100. / base.size());
		}
		count++;
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(7) << std::setprecision(4) << sqrt(itr->ax*itr->ax + itr->ay*itr->ay) << " "
			<< std::setw(3) << std::setprecision(0) << (itr->m[0].ph + itr->m[1].ph) / 10000 << " "
			<< std::setw(5) << std::setprecision(0) << (itr->m[0].ph + itr->m[1].ph) % 10000 << " "
			<< std::setw(8) << std::setprecision(0) << itr->m[0].ph << " "
			<< std::setw(8) << std::setprecision(0) << itr->m[1].ph << " "
			<< std::setw(7) << std::setprecision(4) << ((itr->ax - itr->m[0].ax)*itr->ax + (itr->ay - itr->m[0].ay)*itr->ay) / sqrt(itr->ax*itr->ax + itr->ay*itr->ay) << " "
			<< std::setw(7) << std::setprecision(4) << ((itr->ax - itr->m[1].ax)*itr->ax + (itr->ay - itr->m[1].ay)*itr->ay) / sqrt(itr->ax*itr->ax + itr->ay*itr->ay) << " "
			<< std::setw(7) << std::setprecision(4) << ((itr->ax - itr->m[0].ax)*itr->ay - (itr->ay - itr->m[0].ay)*itr->ax) / sqrt(itr->ax*itr->ax + itr->ay*itr->ay) << " "
			<< std::setw(7) << std::setprecision(4) << ((itr->ax - itr->m[1].ax)*itr->ay - (itr->ay - itr->m[1].ay)*itr->ax) / sqrt(itr->ax*itr->ax + itr->ay*itr->ay) << " "
			<< std::setw(7) << std::setprecision(4) << ((itr->m[0].ax - itr->m[1].ax)*itr->m[1].ay - (itr->m[0].ay - itr->m[1].ay)*itr->m[1].ax) / sqrt(itr->m[1].ax*itr->m[1].ax + itr->m[1].ay*itr->m[1].ay) << " ";

		matrix_3D::vector_3D dir1, pos1, dir2, pos2;
		pos1.x = itr->x;
		pos1.y = itr->y;
		pos1.z = itr->m[0].z;
		pos2.x = itr->x + (itr->m[1].z - itr->m[0].z)*itr->ax;
		pos2.y = itr->y + (itr->m[1].z - itr->m[0].z)*itr->ay;
		pos2.z = itr->m[1].z;
		dir1.x = itr->m[0].ax;
		dir1.y = itr->m[0].ay;
		dir1.z = 1;
		dir2.x = itr->m[1].ax;
		dir2.y = itr->m[1].ay;
		dir2.z = 1;
		double md, z_range[2], extra[2];
		z_range[0] = pos1.z;
		z_range[1] = pos2.z;
		md = matrix_3D::minimum_distance(pos1, pos2, dir1, dir2, z_range[2], extra[2]);
		ofs << std::setw(4) << std::setprecision(1) << md << std::endl;


	}
	fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)\n", count, int(base.size()), count*100. / base.size());
	return;
}