#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <filesystem>
#include <set>

class IP_data {
public:
	int vertex_pl,event_id;
	std::pair<int, int> muon_base;
	std::set<std::pair<int, int>> partner_base;
};


std::vector<IP_data> read_muon(std::vector<mfile0::M_Chain> &c);
void read_partner(std::vector<mfile0::M_Chain> &c, std::vector<IP_data>&ip);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg filename ECC_path\n");
		exit(1);
	}
	std::string file_in_event = argv[1];
	std::string file_in_ecc = argv[2];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_ecc, m);

	std::vector<IP_data> ip_data=read_muon(m.chains);
	read_partner(m.chains, ip_data);

	for (auto itr = ip_data.begin(); itr != ip_data.end(); itr++) {
		std::set<int> pl_list;
		pl_list.insert(itr->muon_base.first);
		for (auto itr2 = itr->partner_base.begin(); itr2 != itr->partner_base.end(); itr2++) {
			pl_list.insert(itr2->first);
		}
		std::map<int, std::vector<vxx::base_track_t>> all_base;
		for (auto itr2 = pl_list.begin(); itr2 != pl_list.end(); itr2++) {
			std::stringstream file_in_base;
			file_in_base << file_in_ecc << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << *itr2
				<< "\\b" << std::setw(3) << std::setfill('0') << *itr2
				<< ".sel.cor.vxx";
			std::vector<vxx::base_track_t> base;
			vxx::BvxxReader br;
			base = br.ReadAll(file_in_base.str(), *itr2, 0);

			if (*itr2 != itr->vertex_pl) {
				std::stringstream file_in_corrmap;
				file_in_base << file_in_ecc << "\\Area0\\0\\align\\fine\\ali_"
					<< std::setw(3) << std::setfill('0') << std::min(*itr2, itr->vertex_pl) << "_"
					<< std::setw(3) << std::setfill('0') << std::max(*itr2, itr->vertex_pl)
					<< "_interpolation.txt";

				std::vector<corrmap_3d::align_param> corr = corrmap_3d::read_ali_param(file_in_corrmap.str(), 0);



			}



			all_base.insert(std::make_pair(*itr2, base));
		}




	}
}
std::vector<IP_data> read_muon(std::vector<mfile0::M_Chain> &c) {
	std::vector<IP_data> ret;
	std::set<int>eventid;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		auto res = eventid.insert(itr->basetracks.begin()->group_id / 100000);
		if (!res.second) {
			IP_data ip;
			ip.event_id = itr->basetracks.begin()->group_id / 100000;
			ip.muon_base.first = itr->basetracks.rbegin()->pos / 10;
			ip.muon_base.second = itr->basetracks.rbegin()->rawid;
			ip.vertex_pl = ip.muon_base.first;
			ret.push_back(ip);
		}
	}
	return ret;
}

void read_partner(std::vector<mfile0::M_Chain> &c, std::vector<IP_data>&ip) {
	int eventid,pl0,pl1,rawid;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		eventid = itr->basetracks.begin()->group_id / 100000;
		pl0 = itr->basetracks.begin()->pos / 10;
		pl1 = itr->basetracks.rbegin()->pos / 10;

		for (auto &ip_sel : ip) {
			if (ip_sel.event_id != eventid)continue;
			//downstream(forward)
			if (ip_sel.vertex_pl == pl1) {
				rawid = itr->basetracks.rbegin()->rawid;
				if (ip_sel.muon_base.second == rawid)continue;
				ip_sel.partner_base.insert(std::make_pair(pl1, rawid));
			}
			//upstream(backward)
			else if (ip_sel.vertex_pl + 1 == pl0) {
				rawid = itr->basetracks.begin()->rawid;
				ip_sel.partner_base.insert(std::make_pair(pl0, rawid));
			}
		}
	}

}

std::vector<align_param> read_ali_param(std::string filename, bool output) {

	std::vector<align_param> ret;
	align_param param_tmp;
	std::ifstream ifs(filename);

	while (ifs >> param_tmp.id >> param_tmp.ix >> param_tmp.iy >> param_tmp.signal
		>> param_tmp.x >> param_tmp.y >> param_tmp.z
		>> param_tmp.x_rot >> param_tmp.y_rot >> param_tmp.z_rot
		>> param_tmp.x_shrink >> param_tmp.y_shrink >> param_tmp.z_shrink
		>> param_tmp.yx_shear >> param_tmp.zx_shear >> param_tmp.zy_shear
		>> param_tmp.dx >> param_tmp.dy >> param_tmp.dz) {
		ret.push_back(param_tmp);
		//printf("ix %d iy%d\n", param_tmp.ix, param_tmp.iy);

	}
	if (output == 1) {
		fprintf(stderr, "%s input finish\n", filename.c_str());
	}
	if (ret.size() == 0) {
		fprintf(stderr, "%s alignment miss!\n", filename.c_str());
		exit(1);
	}
	return ret;

}


//basetrack-alignment mapの対応
std::vector <std::pair<vxx::base_track_t*, align_param2*>>track_affineparam_correspondence(std::vector<vxx::base_track_t>&base, std::vector <align_param2> &param) {

	//local alignの視野中心を取り出して、位置でhash
	//local alignの視野中心の作るdelaunay三角形をmapで対応

	std::map<int, align_param*> view_center;
	std::multimap<int, align_param2*>triangles;
	double xmin = 999999, ymin = 999999, hash = 2000;
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		for (int i = 0; i < 3; i++) {
			view_center.insert(std::make_pair(itr->corr_p[i]->id, (itr->corr_p[i])));
			triangles.insert(std::make_pair(itr->corr_p[i]->id, &(*itr)));
			xmin = std::min(itr->corr_p[i]->x, xmin);
			ymin = std::min(itr->corr_p[i]->y, ymin);
		}
	}
	std::multimap<std::pair<int, int>, align_param*> view_center_hash;
	std::pair<int, int>id;
	for (auto itr = view_center.begin(); itr != view_center.end(); itr++) {
		id.first = int((itr->second->x - xmin) / hash);
		id.second = int((itr->second->y - ymin) / hash);
		view_center_hash.insert(std::make_pair(id, itr->second));
	}

	std::vector < std::pair<vxx::base_track_t*, align_param2*>> ret;
	std::vector<align_param*> param_cand;
	int loop = 0, ix, iy, count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (count % 100000 == 0) {
			printf("\r search correspond triangles %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
		}
		count++;
		ix = (itr->x - xmin) / hash;
		iy = (itr->y - ymin) / hash;
		loop = 1;
		while (true) {
			param_cand.clear();
			for (int iix = ix - loop; iix <= ix + loop; iix++) {
				for (int iiy = iy - loop; iiy <= iy + loop; iiy++) {
					id.first = iix;
					id.second = iiy;
					if (view_center_hash.count(id) != 0) {
						auto range = view_center_hash.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							param_cand.push_back(res->second);
						}
					}
				}
			}
			if (param_cand.size() > 2)break;
			loop++;
		}
		align_param2* param2 = search_param(param_cand, *itr, triangles);
		ret.push_back(std::make_pair(&(*itr), param2));
	}
	printf("\r search correspond triangles %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

	return ret;
}
align_param2* search_param(std::vector<align_param*> &param, vxx::base_track_t&base, std::multimap<int, align_param2*>&triangles) {
	//三角形内部
	//最近接三角形
	double dist = 0;
	std::map<double, align_param* > dist_map;
	//align_paramを近い順にsort
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		dist = ((*itr)->x - base.x)*((*itr)->x - base.x) + ((*itr)->y - base.y)*((*itr)->y - base.y);
		dist_map.insert(std::make_pair(dist, (*itr)));
	}

	double sign[3];
	bool flg = false;
	int id;

	align_param2* ret = triangles.begin()->second;
	for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
		if (itr != dist_map.begin())continue;


		//corrmapのID
		id = itr->second->id;
		if (triangles.count(id) == 0) {
			fprintf(stderr, "alignment triangle ID=%d not found\n", id);
			exit(1);
		}
		//idの属する三角形を探索
		auto range = triangles.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			sign[0] = (itr2->second->corr_p[1]->x - itr2->second->corr_p[0]->x)*(base.y - itr2->second->corr_p[1]->y) - (itr2->second->corr_p[1]->y - itr2->second->corr_p[0]->y)*(base.x - itr2->second->corr_p[1]->x);
			sign[1] = (itr2->second->corr_p[2]->x - itr2->second->corr_p[1]->x)*(base.y - itr2->second->corr_p[2]->y) - (itr2->second->corr_p[2]->y - itr2->second->corr_p[1]->y)*(base.x - itr2->second->corr_p[2]->x);
			sign[2] = (itr2->second->corr_p[0]->x - itr2->second->corr_p[2]->x)*(base.y - itr2->second->corr_p[0]->y) - (itr2->second->corr_p[0]->y - itr2->second->corr_p[2]->y)*(base.x - itr2->second->corr_p[0]->x);
			//printf("point %.lf,%.1lf\n", base.x, base.y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[0]->x, itr2->second->corr_p[0]->y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[1]->x, itr2->second->corr_p[1]->y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[2]->x, itr2->second->corr_p[2]->y);
			//printf("sign %.1lf %1.lf %.1lf\n", sign[0], sign[1], sign[2]);
			//printf("  signbit %d %d %d\n", std::signbit(sign[0]), std::signbit(sign[1]), std::signbit(sign[2]));
			//printf("n signbit %d %d %d\n", !std::signbit(sign[0]), !std::signbit(sign[1]), !std::signbit(sign[2]));
			//printf("judge %d\n", (std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2])));
			//printf("\n");

			//符号が3つとも一致でtrue
			if ((std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2]))) {
				ret = itr2->second;
				flg = true;
				break;
			}
		}
		if (flg)break;
	}
	if (flg) {
		//printf("point in trianlge\n");
		return ret;
	}

	//distが最小になるcorrmapをとってくる
	dist = -1;
	for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
		//corrmapのID
		id = itr->second->id;
		if (triangles.count(id) == 0) {
			fprintf(stderr, "alignment triangle ID=%d not found\n", id);
			exit(1);
		}
		//idの属する三角形を探索
		auto range = triangles.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			if (dist<0 || dist>select_triangle_vale(itr2->second, base)) {
				dist = select_triangle_vale(itr2->second, base);
				ret = itr2->second;
			}
		}
	}
	//printf("point not in trianlge\n");
	return ret;
}
double select_triangle_vale(align_param2* param, vxx::base_track_t&base) {
	double x, y;
	double dist = 0;
	x = (param->corr_p[0]->x + param->corr_p[1]->x + param->corr_p[2]->x) / 3;
	y = (param->corr_p[0]->y + param->corr_p[1]->y + param->corr_p[2]->y) / 3;
	dist = (base.x - x)*(base.x - x) + (base.y - y)*(base.y - y);
	return dist;
}

//変換 zshrink補正-->9para変換
void trans_base_all(std::vector < std::pair<vxx::base_track_t*, align_param2*>>&track_pair) {
	std::map<std::tuple<int, int, int>, align_param2*> param_map;
	std::multimap<std::tuple<int, int, int>, vxx::base_track_t*>base_map;
	std::tuple<int, int, int>id;
	//三角形ごとにbasetrackをまとめる
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		std::get<0>(id) = itr->second->corr_p[0]->id;
		std::get<1>(id) = itr->second->corr_p[1]->id;
		std::get<2>(id) = itr->second->corr_p[2]->id;
		param_map.insert(std::make_pair(id, itr->second));
		base_map.insert(std::make_pair(id, itr->first));
	}


	//ここで三角形ごとに変換
	int count = 0;
	std::vector<vxx::base_track_t*> t_base;
	for (auto itr = param_map.begin(); itr != param_map.end(); itr++) {
		if (count % 1000 == 0) {
			printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)", count, param_map.size(), count*100. / param_map.size());
		}
		count++;

		t_base.clear();

		if (base_map.count(itr->first) == 0)continue;
		auto range = base_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			t_base.push_back(res->second);
		}
		trans_base(t_base, itr->second);

	}
	printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)\n", count, param_map.size(), count*100. / param_map.size());

}
void trans_base(std::vector<vxx::base_track_t*>&base, align_param2 *param) {

	matrix_3D::matrix_33 x_rot_mat(0, param->x_rot), y_rot_mat(1, param->y_rot), z_rot_mat(2, param->z_rot), all_trans(0, 0), shear_mat(0, 0), shrink_mat(0, 0);

	shrink_mat.val[0][0] *= param->x_shrink;
	shrink_mat.val[1][1] *= param->y_shrink;
	//shrink_mat.val[2][2] *= param->z_shrink;
	shear_mat.val[0][1] = param->yx_shear;
	shear_mat.val[0][2] = param->zx_shear;
	shear_mat.val[1][2] = param->zy_shear;

	matrix_3D::vector_3D shift, center;
	center.x = param->x;
	center.y = param->y;
	center.z = param->z;
	shift.x = param->dx;
	shift.y = param->dy;
	shift.z = param->dz;

	all_trans.matrix_multiplication(shear_mat);
	all_trans.matrix_multiplication(shrink_mat);
	all_trans.matrix_multiplication(z_rot_mat);
	all_trans.matrix_multiplication(y_rot_mat);
	all_trans.matrix_multiplication(x_rot_mat);

	//all_trans.Print();
	matrix_3D::vector_3D base_p0, base_p1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_p0.x = (*itr)->x;
		base_p0.y = (*itr)->y;
		base_p0.z = param->z;

		base_p1.x = (*itr)->x + (*itr)->ax*((*itr)->m[1].z - (*itr)->m[0].z);
		base_p1.y = (*itr)->y + (*itr)->ay*((*itr)->m[1].z - (*itr)->m[0].z);
		//角度shrink分はここでかける
		base_p1.z = param->z + ((*itr)->m[1].z - (*itr)->m[0].z) / param->z_shrink;

		//視野中心を原点に移動
		//base_p0 = matrix_3D::addition(base_p0, matrix_3D::const_multiple(center, -1));
		//base_p1 = matrix_3D::addition(base_p1, matrix_3D::const_multiple(center, -1));

		//変換の実行
		base_p0.matrix_multiplication(all_trans);
		base_p0 = matrix_3D::addition(base_p0, shift);
		base_p1.matrix_multiplication(all_trans);
		base_p1 = matrix_3D::addition(base_p1, shift);

		//原点をもとに戻す
		//base_p0 = matrix_3D::addition(base_p0, center);
		//base_p1 = matrix_3D::addition(base_p1, center);

		(*itr)->x = base_p0.x;
		(*itr)->y = base_p0.y;
		(*itr)->z = base_p0.z;

		//printf("ax:%.4lf --> %.4lf\n", (*itr)->ax, (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z));
		//printf("ay:%.4lf --> %.4lf\n", (*itr)->ay, (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z));

		(*itr)->ax = (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z);
		(*itr)->ay = (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z);

	}
}

