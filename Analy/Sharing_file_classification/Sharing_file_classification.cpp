#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf);
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, int pm_flg);
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, int pm_flg, bool single_flg);
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, int pm_flg, bool single_flg, int ecc_flg);

int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "prg:sharing-file\n");
		exit(1);
	}

	std::string file_in_sf = argv[1];
	
	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_extension(file_in_sf);
	int all_prediction = Count_track_timing(sf);
	printf("all prediction:%d\n", all_prediction);
	printf("\t PM stop:%d\n", Count_track_timing(sf, 0));
	printf("\t\t single:%d\n", Count_track_timing(sf, 0, true));
	printf("\t\t\t ECC stop:%d\n", Count_track_timing(sf, 0, true, 0));
	printf("\t\t\t ECC penetrate:%d\n", Count_track_timing(sf, 0, true, 1));
	printf("\t\t\t ECC edge out:%d\n", Count_track_timing(sf, 0, true, 2));
	printf("\t\t multi:%d\n", Count_track_timing(sf, 0, false));
	printf("\t\t\t ECC stop exist:%d\n", Count_track_timing(sf, 0, false, 0));
	printf("\t\t\t ECC stop not exist:%d\n", Count_track_timing(sf, 0, false, 1));
	/////////////////////////////////////////////
	printf("\t PM penetrate:%d\n", Count_track_timing(sf, 1));
	printf("\t\t single:%d\n", Count_track_timing(sf, 1, true));
	printf("\t\t\t ECC stop:%d\n", Count_track_timing(sf, 1, true, 0));
	printf("\t\t\t ECC penetrate:%d\n", Count_track_timing(sf, 1, true, 1));
	printf("\t\t\t ECC edge out:%d\n", Count_track_timing(sf, 1, true, 2));
	printf("\t\t multi:%d\n", Count_track_timing(sf, 1, false));
	printf("\t\t\t ECC stop exist:%d\n", Count_track_timing(sf, 1, false, 0));
	printf("\t\t\t ECC stop not exist:%d\n", Count_track_timing(sf, 1, false, 1));
	/////////////////////////////////////////////

	printf("\t PM interaction:%d\n", Count_track_timing(sf, 2));
	printf("\t\t single:%d\n", Count_track_timing(sf, 2, true));
	printf("\t\t\t ECC stop:%d\n", Count_track_timing(sf, 2, true, 0));
	printf("\t\t\t ECC penetrate:%d\n", Count_track_timing(sf, 2, true, 1));
	printf("\t\t\t ECC edge out:%d\n", Count_track_timing(sf, 2, true, 2));
	printf("\t\t multi:%d\n", Count_track_timing(sf, 2, false));
	printf("\t\t\t ECC stop exist:%d\n", Count_track_timing(sf, 2, false, 0));
	printf("\t\t\t ECC stop not exist:%d\n", Count_track_timing(sf, 2, false, 1));
	/////////////////////////////////////////////

}
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf) {
	std::set<int> unix_time;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		unix_time.insert(itr->unix_time);
	}
	return unix_time.size();
}
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, int pm_flg) {
	std::set<int> unix_time;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		if (itr->track_type != pm_flg)continue;
		unix_time.insert(itr->unix_time);
	}
	return unix_time.size();
}
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, int pm_flg, bool single_flg) {
	std::multimap<int, Sharing_file::Sharing_file>sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(itr->unix_time, *itr));
	}

	std::set<int> unix_time;
	for (auto itr = sf_map.begin(); itr != sf_map.end(); itr = std::next(itr, sf_map.count(itr->first))) {
		std::vector<Sharing_file::Sharing_file> sf_v;
		auto range = sf_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			sf_v.push_back(res->second);
		}
		if (single_flg) {
			if (sf_v.size() != 1)continue;
			if (sf_v[0].track_type != pm_flg)continue;
			unix_time.insert(itr->first);
		}
		else {
			if (sf_v.size() == 1)continue;
			if (sf_v[0].track_type != pm_flg)continue;
			unix_time.insert(itr->first);
		}

	}
	return unix_time.size();
}
int Count_track_timing(std::vector<Sharing_file::Sharing_file>&sf, int pm_flg, bool single_flg, int ecc_flg) {
	std::multimap<int, Sharing_file::Sharing_file>sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(itr->unix_time, *itr));
	}

	std::set<int> unix_time;
	for (auto itr = sf_map.begin(); itr != sf_map.end(); itr = std::next(itr, sf_map.count(itr->first))) {
		std::vector<Sharing_file::Sharing_file> sf_v;
		auto range = sf_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			sf_v.push_back(res->second);
		}
		if (single_flg) {
			if (sf_v.size() != 1)continue;
			if (sf_v[0].track_type != pm_flg)continue;
			if (sf_v[0].ecc_track_type != ecc_flg)continue;
			unix_time.insert(itr->first);
		}
		else {
			if (sf_v.size() == 1)continue;
			if (sf_v[0].track_type != pm_flg)continue;
			bool stop_flg = false;
			for (auto &p : sf_v) {
				if (p.ecc_track_type == 0)stop_flg = true;
			}
			if (ecc_flg == 0 && stop_flg) {
				unix_time.insert(itr->first);
			}
			else if (ecc_flg == 1 && !stop_flg) {
				unix_time.insert(itr->first);
			}
		}

	}
	return unix_time.size();
}
