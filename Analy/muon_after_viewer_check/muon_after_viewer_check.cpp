#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#include <chrono>
#include<filesystem>

#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<std::string> mfile_path(std::string file_path);
void Read_mfile(std::string file_in_mfile, mfile0::Mfile&m);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg filepath file-out-mfile\n");
		fprintf(stderr, "folder name = 0,1,2,...\n");
		fprintf(stderr, "mfile name=track_sel2.all\n");
		exit(1);
	}

	std::string file_in_path = argv[1];
	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;
	std::vector<std::string> file_in_mfile=mfile_path(file_in_path);
	for (int i = 0; i < file_in_mfile.size(); i++) {
		printf("%s\n", file_in_mfile[i].c_str());
		Read_mfile(file_in_mfile[i], m);
	}

	printf("chain num = %d\n", m.chains.size());
	mfile0::write_mfile(file_out_mfile, m);

}


std::vector<std::string> mfile_path(std::string file_path) {
	std::vector<std::string> ret;
	for (int i = 100; i >= 0; i--) {
		std::stringstream file_path_ss;
		file_path_ss << file_path << "\\" << i;
		if (!std::filesystem::exists(file_path_ss.str()))continue;

		std::stringstream file_path_mfile;
		file_path_mfile << file_path << "\\" << i << "\\track_sel2.all";
		if (!std::filesystem::exists(file_path_mfile.str()))continue;

		ret.push_back(file_path_mfile.str());
	}
	return ret;
}

void Read_mfile(std::string file_in_mfile, mfile0::Mfile&m) {
	std::set<int> eventid;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		eventid.insert(itr->basetracks.begin()->group_id);
	}

	mfile0::Mfile m2;
	mfile0::read_mfile(file_in_mfile, m2);
	//最初の読み込み
	if (m.chains.size() == 0) {
		m.header = m2.header;
	}

	for (auto itr = m2.chains.begin(); itr != m2.chains.end(); itr++) {
		if (itr->chain_id!=0)continue;
		if (eventid.count(itr->basetracks.begin()->group_id) == 1)continue;
		m.chains.push_back(*itr);
	}

}