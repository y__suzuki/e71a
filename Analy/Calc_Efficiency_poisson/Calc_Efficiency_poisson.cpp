#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>


void chain_nseg_selection(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, const int nseg);
void reject_Fe_ECC(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel);
void chain_dlat_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double threshold);
void chain_angle_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double thr_ax, double thr_ay);

int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:prg filename\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	struct Prediction {
		int PL, flg, rawid;
		double ax, ay, x, y;
	};
	mfile1::MFile_minimum m;
	mfile1::read_mfile(file_in_mfile, m,15);
	//zmap
	std::map<int, double> zmap;
	for (auto c : m.all_basetracks) {
		for (auto b : c) {
			zmap.insert(std::make_pair(b.pos / 10, b.z));
		}
	}
	std::vector<uint64_t> all, sel;
	for (int i = 0; i < m.chains.size(); i++) {
		all.push_back(i);
	}

	reject_Fe_ECC(m, all, sel);
	all = sel;
	sel.clear();

	chain_angle_selection(m, all, sel, 4.0, 4.0);;
	all = sel;
	sel.clear();

	chain_dlat_selection(m, all, sel, 0.005);

}
void chain_nseg_selection(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, const int nseg) {
	for (auto i : all) {
		if (m.chains[i].nseg < nseg)continue;
		sel.push_back(i);
	}
	printf("chain nseg >= %lld: %lld --> %lld (%4.1lf%%)\n", nseg, all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}
void reject_Fe_ECC(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel) {
	for (auto i : all) {
		if (m.chains[i].pos0 / 10 > 15) {
			sel.push_back(i);
		}
		else {
			int flg = 0;
			for (auto b : m.all_basetracks[i]) {
				if (b.pos / 10 == 16)flg++;
				if (b.pos / 10 == 17)flg++;
				if (b.pos / 10 == 18)flg++;
				if (b.pos / 10 == 19)flg++;
				if (b.pos / 10 == 20)flg++;
				if (b.pos / 10 == 21)flg++;
				if (b.pos / 10 == 22)flg++;
				if (b.pos / 10 == 23)flg++;
			}
			if (flg >= 6 || m.chains[i].pos1 / 10 > 23) {
				sel.push_back(i);
			}
		}
	}
	printf("reject_Fe_ECC(PL1 >= 24): %lld --> %lld (%4.1lf%%)\n", all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}
void chain_dlat_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double threshold) {
	double ax, ay, div_rad, div_lat;
	std::ofstream ofs("out.txt");

	for (auto i : all) {
		ax = mfile1::chain_ax(m.all_basetracks[i]);
		ay = mfile1::chain_ay(m.all_basetracks[i]);
		mfile1::angle_diff_dev_theta(m.all_basetracks[i], ax, ay, div_rad, div_lat);
		ofs << std::right << std::fixed
			<< std::setw(5) << std::setprecision(0) << m.all_basetracks[i].size()
			<< std::setw(10) << std::setprecision(6) << div_rad
			<< std::setw(10) << std::setprecision(6) << div_lat << std::endl;
			if (mfile1::angle_diff_dev_lat(m.all_basetracks[i]) > threshold)continue;
		sel.push_back(i);
	}
	printf("chain lateral selection <= %5.4lf : %lld --> %lld (%4.1lf%%)\n", threshold, all.size(), sel.size(), sel.size()*100. / all.size());
	return;

}
void chain_angle_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double thr_ax, double thr_ay) {
	double ax, ay;
	for (auto i : all) {
		ax = mfile1::chain_ax(m.all_basetracks[i]);
		ay = mfile1::chain_ay(m.all_basetracks[i]);
		if (thr_ax - fabs(ax) < 0)continue;
		if (thr_ay - fabs(ay) < 0)continue;
		sel.push_back(i);

	}
	printf("chain average angle selection |ax|<=%3.1lf |ay|<=%3.1lf : %lld --> %lld (%4.1lf%%)\n", thr_ax, thr_ay, all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}

