#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "functions.lib")
#include <functions.hpp>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <chrono>
#include <filesystem>
#include <set>

class align_param {
public:
	int id;
	//視野中心
	double x, y, z;
	//parameter(9);
	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;

};

std::string Set_file_read_ali_path(std::string file_ECC_path, int pl0, int pl1, int area);
std::vector<corrmap0::Corrmap> select_edge(std::vector<corrmap0::Corrmap> &corr);
std::vector<align_param> align_change_format(std::vector<corrmap0::Corrmap> &corr);

void output_corrmap(std::string filename, std::vector<align_param> &corr);
void output_Fiducial_Area_line(std::ofstream &ofs, std::vector<align_param> &corr, int PL);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "prg ECC_path plmin plmax output-file\n");
		exit(1);
	}
	int area = 0;
	int plmin = std::stoi(argv[2]);
	int plmax = std::stoi(argv[3]);

	std::string file_ECC_path = argv[1];
	std::string file_output = argv[4];


	std::map<int, std::vector<align_param>> all_PL_area;
	for (int pl = plmin; pl <= plmax; pl++) {
		std::vector<corrmap0::Corrmap> corr;

		//fileの存在確認・読み込み
		std::string file_in_ali = Set_file_read_ali_path(file_ECC_path, pl-1, pl , area);
		if (!std::filesystem::exists(file_in_ali)) {
			fprintf(stderr, "%s not exist\n", file_in_ali.c_str());
			continue;
		}
		printf("start PL%03d\n", pl);
		corrmap0::read_cormap(file_in_ali, corr);

		//edgeの抽出
		corr = select_edge(corr);

		//視野中心を逆変換
		std::vector<align_param> ali_params = align_change_format(corr);

		all_PL_area.insert(std::make_pair(pl, ali_params));
	}
	std::ofstream ofs(file_output);

	for (auto itr = all_PL_area.begin(); itr != all_PL_area.end(); itr++) {
		output_Fiducial_Area_line(ofs, itr->second,itr->first);
	}

}
std::string Set_file_read_ali_path(std::string file_ECC_path, int pl0, int pl1, int area) {
	std::stringstream file_in_ali;
	file_in_ali << file_ECC_path << "\\Area" << area << "\\0\\align\\corrmap-align-"
		<< std::setw(3) << std::setfill('0') << pl0 << "-"
		<< std::setw(3) << std::setfill('0') << pl1 << ".lst";
	return file_in_ali.str();

}
/*
std::vector<corrmap0::Corrmap> select_edge(std::vector<corrmap0::Corrmap> &corr) {
	std::multimap<int, corrmap0::Corrmap> map_x, map_y;
	std::vector<corrmap0::Corrmap> edge_xmin, edge_xmax, edge_ymin, edge_ymax;

	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		map_x.insert(std::make_pair(itr->notuse_i[1], *itr));
		map_y.insert(std::make_pair(itr->notuse_i[2], *itr));
	}
	corrmap0::Corrmap edge_min, edge_max;
	//ixについてloop iyのmin/maxを抽出
	for (auto itr = map_x.begin(); itr != map_x.end(); itr++) {
		auto range = map_x.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res == range.first) {
				edge_min = res->second;
				edge_max = res->second;
			}
			if (edge_min.notuse_i[2] > res->second.notuse_i[2])edge_min = res->second;
			if (edge_max.notuse_i[2] < res->second.notuse_i[2])edge_max = res->second;
		}
		edge_ymin.push_back(edge_min);
		edge_ymax.push_back(edge_max);
	}
	//iyについてloop ixのmin/maxを抽出
	for (auto itr = map_y.begin(); itr != map_y.end(); itr++) {
		auto range = map_y.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res == range.first) {
				edge_min = res->second;
				edge_max = res->second;
			}
			if (edge_min.notuse_i[1] > res->second.notuse_i[1])edge_min = res->second;
			if (edge_max.notuse_i[1] < res->second.notuse_i[1])edge_max = res->second;
		}
		edge_xmin.push_back(edge_min);
		edge_xmax.push_back(edge_max);
	}

	std::vector<corrmap0::Corrmap> ret;
	std::vector<corrmap0::Corrmap> path_xmin, path_xmax, path_ymin, path_ymax;
	for (auto itr = edge_xmin.begin(); itr != edge_xmin.end(); itr++) {
		path_xmin.push_back(*itr);
	}
	for (auto itr = edge_ymax.begin(); itr != edge_ymax.end(); itr++) {
		path_ymax.push_back(*itr);
	}
	for (auto itr = edge_xmax.rbegin(); itr != edge_xmax.rend(); itr++) {
		path_xmax.push_back(*itr);
	}
	for (auto itr = edge_ymin.rbegin(); itr != edge_ymin.rend(); itr++) {
		path_ymin.push_back(*itr);
	}


	return ret;

}
*/

std::vector<corrmap0::Corrmap> select_edge(std::vector<corrmap0::Corrmap> &corr) {
	std::multimap<int, corrmap0::Corrmap> map_x, map_y;
	std::map<std::pair<int, int>, corrmap0::Corrmap> edge_all;

	double x_ave = 0, y_ave = 0;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		map_x.insert(std::make_pair(itr->notuse_i[1], *itr));
		map_y.insert(std::make_pair(itr->notuse_i[2], *itr));
		x_ave += (itr->areax[0] + itr->areax[1]) / 2;
		y_ave += (itr->areay[0] + itr->areay[1]) / 2;
	}
	x_ave = x_ave / corr.size();
	y_ave = y_ave / corr.size();

	corrmap0::Corrmap edge_min, edge_max;
	//ixについてloop iyのmin/maxを抽出
	for (auto itr = map_x.begin(); itr != map_x.end(); itr++) {
		auto range = map_x.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res == range.first) {
				edge_min = res->second;
				edge_max = res->second;
			}
			if (edge_min.notuse_i[2] > res->second.notuse_i[2])edge_min = res->second;
			if (edge_max.notuse_i[2] < res->second.notuse_i[2])edge_max = res->second;
		}
		edge_all.insert(std::make_pair(std::make_pair(edge_min.notuse_i[1], edge_min.notuse_i[2]), edge_min));
		edge_all.insert(std::make_pair(std::make_pair(edge_max.notuse_i[1], edge_max.notuse_i[2]), edge_max));
	}
	//iyについてloop ixのmin/maxを抽出
	for (auto itr = map_y.begin(); itr != map_y.end(); itr++) {
		auto range = map_y.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res == range.first) {
				edge_min = res->second;
				edge_max = res->second;
			}
			if (edge_min.notuse_i[1] > res->second.notuse_i[1])edge_min = res->second;
			if (edge_max.notuse_i[1] < res->second.notuse_i[1])edge_max = res->second;
		}
		edge_all.insert(std::make_pair(std::make_pair(edge_min.notuse_i[1], edge_min.notuse_i[2]), edge_min));
		edge_all.insert(std::make_pair(std::make_pair(edge_max.notuse_i[1], edge_max.notuse_i[2]), edge_max));
	}

	//edgeの点を中心から回して順番につなげる

	double base_x, base_y;
	base_x = (edge_all.begin()->second.areax[0] + edge_all.begin()->second.areax[1]) / 2;
	base_y = (edge_all.begin()->second.areay[0] + edge_all.begin()->second.areay[1]) / 2;
	double base_vec[2],dir_vec[2],base_length;
	base_vec[0] = base_x - x_ave;
	base_vec[1] = base_y - y_ave;
	base_length = sqrt(base_vec[0] * base_vec[0] + base_vec[1] * base_vec[1]);

	int count = 0;
	std::map<double, corrmap0::Corrmap> edge_rot;
	double dir_length,theta,c_product;
	for (auto itr = edge_all.begin(); itr != edge_all.end(); itr++) {
		dir_vec[0] = (itr->second.areax[0] + itr->second.areax[1]) / 2 - x_ave;
		dir_vec[1] = (itr->second.areay[0] + itr->second.areay[1]) / 2 - y_ave;
		dir_length = sqrt(dir_vec[0] * dir_vec[0] + dir_vec[1] * dir_vec[1]);
		theta = (base_vec[0] * dir_vec[0] + base_vec[1] * dir_vec[1]) / (base_length*dir_length);
		if (1-fabs(theta) < 0.000000001) {
			//一直線上の場合
			//始点との距離を測る
			double dist = sqrt(pow(base_vec[0] - dir_vec[0], 2) + pow(base_vec[1] - dir_vec[1], 2));
			//中心からの距離<始点からの距離 -->0度
			if (dist < dir_length) {
				theta = 0;
			}
			else {
				theta= acos(-1);
			}
		}
		else {
			theta = acos(theta);
			c_product = base_vec[0] * dir_vec[1] - base_vec[1] * dir_vec[0];
			if (signbit(c_product)) {
				theta = 2*acos(-1) - theta;
			}
		}
		count++;
		edge_rot.insert(std::make_pair(theta, itr->second));
	}




	std::vector<corrmap0::Corrmap> ret;
	for (auto itr = edge_rot.begin(); itr != edge_rot.end(); itr++) {
		//printf("%5.4lf\n", itr->first);
		ret.push_back(itr->second);
	}

	return ret;

}
std::vector<align_param> align_change_format(std::vector<corrmap0::Corrmap> &corr) {
	std::vector<align_param> ret;
	for (int i = 0; i < corr.size(); i++) {
		align_param param;
		param.id = i;
		param.dx = corr[i].position[4];
		param.dy = corr[i].position[5];
		param.dz = corr[i].dz;
		double factor = corr[i].position[0] * corr[i].position[3] - corr[i].position[1] * corr[i].position[2];
		param.x_shrink = sqrt(factor);
		param.y_shrink = sqrt(factor);
		param.z_shrink = 1;

		param.z_rot = atan((corr[i].position[2] / corr[i].position[0]));

		double area[4];
		area[0] = corr[i].areax[0] - corr[i].position[4];
		area[1] = corr[i].areax[1] - corr[i].position[4];
		area[2] = corr[i].areay[0] - corr[i].position[5];
		area[3] = corr[i].areay[1] - corr[i].position[5];

		corr[i].areax[0] = 1. / factor * (area[0] * corr[i].position[3] - area[2] * corr[i].position[1]);
		corr[i].areay[0] = 1. / factor * (area[2] * corr[i].position[0] - area[0] * corr[i].position[2]);
		corr[i].areax[1] = 1. / factor * (area[1] * corr[i].position[3] - area[3] * corr[i].position[1]);
		corr[i].areay[1] = 1. / factor * (area[3] * corr[i].position[0] - area[1] * corr[i].position[2]);

		param.x = (corr[i].areax[0] + corr[i].areax[1]) / 2;
		param.y = (corr[i].areay[0] + corr[i].areay[1]) / 2;
		param.z = 0;

		param.x_rot = 0;
		param.y_rot = 0;
		param.yx_shear = 0;
		param.zx_shear = 0;
		param.zy_shear = 0;

		ret.push_back(param);
	}
	return ret;
}
void output_corrmap(std::string filename, std::vector<align_param> &corr) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (corr.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)", count, int(corr.size()), count*100. / corr.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(0) << itr->id << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << " "
				<< std::setw(8) << std::setprecision(1) << itr->z << " "
				<< std::setw(9) << std::setprecision(7) << itr->x_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->y_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->z_rot << " "
				<< std::setw(8) << std::setprecision(6) << itr->x_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->y_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->z_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->yx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zy_shear << " "
				<< std::setw(8) << std::setprecision(2) << itr->dx << " "
				<< std::setw(8) << std::setprecision(2) << itr->dy << " "
				<< std::setw(8) << std::setprecision(2) << itr->dz << std::endl;
		}
		fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)\n", count, int(corr.size()), count*100. / corr.size());

	}



}
void output_Fiducial_Area_line(std::ofstream &ofs, std::vector<align_param> &corr, int PL) {

	if (corr.size() == 0) {
		fprintf(stderr, "target corrmap ... null\n");
	}
	else {
		int count = 0;
		int n = corr.size();
		std::cout << std::right << std::fixed;
		for (int i = 0; i < n; i++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)", count, int(corr.size()), count*100. / corr.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(5) << std::setprecision(0) << PL << " "
				<< std::setw(8) << std::setprecision(1) << corr[i%n].x << " "
				<< std::setw(8) << std::setprecision(1) << corr[i%n].y << " "
				<< std::setw(8) << std::setprecision(1) << corr[i%n].dz << " "
				<< std::setw(8) << std::setprecision(1) << corr[(i + 1) % n].x << " "
				<< std::setw(8) << std::setprecision(1) << corr[(i + 1) % n].y << " "
				<< std::setw(8) << std::setprecision(1) << corr[(i + 1) % n].dz << std::endl;
		}
		fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)\n", count, int(corr.size()), count*100. / corr.size());

	}
}