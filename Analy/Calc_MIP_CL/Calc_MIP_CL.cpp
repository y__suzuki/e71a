#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"PID_correct.lib")
#include <PID_correct.h>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <iostream>
#include <set>
#include <omp.h>
#include <filesystem>

class VPH_Dist {
public:
	double angle_min, angle_max, mean, sigma;
	int pos, trackingid, sensorid;
};
class VPH_CL {
public:
	double angle_min, angle_max;
	int pos, trackingid, sensorid;
	std::map<int, double> cl_map;
};
std::vector<PID_track> pb_cut(std::vector<PID_track>&track, double threshold);

void read_VPH_Dist(std::string filename, std::vector<VPH_Dist> &dist, int pl);
void Add_CL_Value(std::vector<PID_track> &track, std::vector<VPH_Dist> &dist, std::vector<VPH_CL> &cl_map, int max);
int sensor_id(int sensor);
int angle_id(std::set<double >angle_range, double angle);
int tracking_id(int track);
std::map<int, double> Calc_CL_Value_fit(VPH_Dist &dist, int vph_cut, int max);
std::map<int, double> Calc_CL_Value_bin(std::vector<PID_track*> &track);

double Calc_gaus(double mean, double sigma, double val);
void write_CL(std::string filename, std::vector <VPH_CL>&cl);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg file-in-micro file-in-sigma pl file-out file_out_CL\n");
		exit(1);
	}
	//最終的な入力は考える
	std::string file_in_base = argv[1];
	std::string file_in_distrbution = argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_out = argv[4];
	std::string file_out_CL = argv[5];

	const int vph_max = 11 * 11 * (16 - 6 + 1);

	std::vector<PID_track> track;
	PID_track::read_pid_track(file_in_base, track);

	std::vector<VPH_Dist> dist;
	read_VPH_Dist(file_in_distrbution, dist, pl);
	std::vector<VPH_CL>cl;
	Add_CL_Value(track, dist,cl, vph_max);


	//trackのwrite
	PID_track::wrtie_pid_track(file_out, track);
	//CL分布のwrite
	write_CL(file_out_CL, cl);
}
void read_VPH_Dist(std::string filename, std::vector<VPH_Dist> &dist,int pl) {
	std::ifstream ifs(filename);

	VPH_Dist dist_t;
	while (ifs >> dist_t.pos >> dist_t.trackingid >> dist_t.sensorid >> dist_t.angle_min >> dist_t.angle_max >> dist_t.mean >> dist_t.sigma) {
		if (dist_t.pos / 10 != pl)continue;
		dist.push_back(dist_t);
	}
	if (dist.size() == 0) {
		fprintf(stderr, "PL%03d not found\n", pl);
		exit(1);
	}
	return;
}
std::vector<PID_track> pb_cut(std::vector<PID_track>&track, double threshold) {
	std::vector<PID_track> ret;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		if (itr->pb < threshold)continue;
		ret.push_back(*itr);
	}
	return ret;
}

void Add_CL_Value(std::vector<PID_track> &track, std::vector<VPH_Dist> &dist, std::vector<VPH_CL> &cl_map, int max) {

	//pos,tracking,sensor
	std::multimap<std::tuple<int, int, int>, PID_track*>track_map;
	std::multimap<std::tuple<int, int, int>, VPH_Dist>dist_map;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		track_map.insert(std::make_pair(std::make_tuple(itr->pos, tracking_id(itr->trackingid), sensor_id(itr->sensorid)), &(*itr)));
	}
	for (auto itr = dist.begin(); itr != dist.end(); itr++) {
		dist_map.insert(std::make_pair(std::make_tuple(itr->pos, itr->trackingid, itr->sensorid), *itr));
	}

	int vph_cut = 0;
	int64_t all_track = track.size(), count_track = 0;
	std::tuple<int, int, int> id;
	for (auto itr = track_map.begin(); itr != track_map.end(); itr++) {
		id = itr->first;

		int count = track_map.count(id);

		if (dist_map.count(id) == 0) {
			fprintf(stderr, "Appropriate distribution not found\n");
			fprintf(stderr, "pos=%d trackingid=%d sensorid=%d\n", std::get<0>(id), std::get<1>(id), std::get<2>(id));
			exit(1);
		}
		auto dist_map_tmp = dist_map.equal_range(id);
		auto track_map_tmp = track_map.equal_range(id);

		//agnle id決定用のsetを用意
		std::set<double> angle_range;
		for (auto itr2 = dist_map_tmp.first; itr2 != dist_map_tmp.second; itr2++) {
			angle_range.insert(itr2->second.angle_max);
		}
		//trackを角度で分ける
		std::multimap<int, PID_track*> track_map_angleid;
		for (auto res = track_map_tmp.first; res != track_map_tmp.second; res++) {
			track_map_angleid.insert(std::make_pair(angle_id(angle_range, res->second->angle), res->second));
		}
		//角度ごとにCLの計算
		for (auto itr2 = dist_map_tmp.first; itr2 != dist_map_tmp.second; itr2++) {
			int a_id = angle_id(angle_range, itr2->second.angle_min + 0.001);
			if (track_map_angleid.count(a_id) == 0)continue;
			auto track_range = track_map_angleid.equal_range(a_id);
			//vph cutをtrack集団の最小にする
			for (auto itr3 = track_range.first; itr3 != track_range.second; itr3++) {
				if (itr3 == track_range.first)vph_cut = int(itr3->second->vph + 0.01);
				vph_cut = std::min((int)(itr3->second->vph + 0.01), vph_cut);
			}

			//CL listの用意
			//gauss fit ver
			//std::map<int, double> CL_Value_map = Calc_CL_Value_fit(itr2->second, vph_cut, max);
			//bin count ver
			std::vector<PID_track*> track_sel;
			for (auto itr3 = track_range.first; itr3 != track_range.second; itr3++) {
				//pb<1GeVのカット
				if (itr3->second->pb < 1000)continue;
				track_sel.push_back(itr3->second);
			}
			std::map<int, double> CL_Value_map = Calc_CL_Value_bin(track_sel);
			VPH_CL cl_tmp;
			cl_tmp.angle_min = itr2->second.angle_min;
			cl_tmp.angle_max = itr2->second.angle_max;
			cl_tmp.pos = itr2->second.pos;
			cl_tmp.sensorid = itr2->second.sensorid;
			cl_tmp.trackingid = itr2->second.trackingid;
			cl_tmp.cl_map = CL_Value_map;
			if (track_sel.size() > 10)cl_map.push_back(cl_tmp);
			//debug用出力
			//printf("\n pos=%5d trackingid=%2d sensorid=%2d angleid=%d\n", std::get<0>(id), std::get<1>(id), std::get<2>(id), a_id);
			//for (auto itr = CL_Value_map.begin(); itr != CL_Value_map.end(); itr++) {
			//	if (itr->second < 0.0001)continue;
			//	printf("%d %.6lf\n", itr->first, itr->second);
			//}
			//system("pause");


			//trackに対してloop CLの計算
			int val_min, val_max, size = track_sel.size();
			val_min = CL_Value_map.begin()->first;
			val_max = CL_Value_map.rbegin()->first;
			for (auto itr3 = track_range.first; itr3 != track_range.second; itr3++) {
				if (count_track % 10000 == 0) {
					printf("\r now ... pos=%5d trackingid=%2d sensorid=%2d %lld/%lld(%4.1lf%%)", std::get<0>(id), std::get<1>(id), std::get<2>(id), count_track, all_track, count_track*100. / all_track);
				}
				count_track++;
				if (size < 10) {
					itr3->second->vph = -1;
				}
				else if (itr3->second->vph > val_max) {
					itr3->second->vph = CL_Value_map.rbegin()->second;
				}
				else if (itr3->second->vph < val_min) {
					itr3->second->vph = CL_Value_map.begin()->second;
				}
				else {
					itr3->second->vph = CL_Value_map.at(itr3->second->vph);
				}
			}
		}

		itr = std::next(itr, count - 1);
	}
	printf("\r now ... pos=%5d trackingid=%2d sensorid=%2d %lld/%lld(%4.1lf%%)\n", std::get<0>(id), std::get<1>(id), std::get<2>(id), count_track, all_track, count_track*100. / all_track);
}

int sensor_id(int sensor) {
	if ((24 <= sensor && sensor < 36) || sensor == 52)return 1;
	return 0;
}
int angle_id (std::set<double >angle_range, double angle) {
	int i = 0;
	for (auto itr = angle_range.begin(); itr != angle_range.end(); itr++) {
		if (angle < *itr)return i;
		i++;
	}
	return i - 1;
}
int tracking_id(int track) {
	if (track==0||track==1)return 0;
	else if (track == 2)return 1;
	return 2;
}
std::map<int, double> Calc_CL_Value_fit(VPH_Dist &dist, int vph_cut, int max) {

	double all = 0;
	//面積の計算
	for (int vph = vph_cut; vph <= max; vph++) {
		all += Calc_gaus(dist.mean, dist.sigma, vph);
	}
	//相対度数の計算
	std::map<int, double> Relative_frequency;
	for (int vph = vph_cut; vph <= max; vph++) {
		Relative_frequency.insert(std::make_pair(vph, Calc_gaus(dist.mean, dist.sigma, vph) / all));
	}
	//期待値の計算
	all = 0;
	for (int vph = vph_cut; vph <= max; vph++) {
		all += Relative_frequency.at(vph)*vph;
	}
	double Expecte_value = all;
	int Expecte_value_int = int(std::round(Expecte_value) + 0.01);
	//CLの計算
	std::map<int, double> ret;
	for (int vph = vph_cut; vph <= max; vph++) {
		int dist = vph - Expecte_value_int;
		all = 0;
		if (dist < 0) {
			for (int i = vph_cut; i <= vph; i++) {
				all += Relative_frequency.at(i);
			}
			for (int i = max; i > Expecte_value_int - dist; i--) {
				all += Relative_frequency.at(i);
			}
			ret.insert(std::make_pair(vph, all));
		}
		else if (dist > 0) {
			for (int i = vph_cut; i < Expecte_value_int - dist; i++) {
				all += Relative_frequency.at(i);
			}
			for (int i = max; i >= vph; i--) {
				all += Relative_frequency.at(i);
			}
			ret.insert(std::make_pair(vph, all));
		}
		else {
			for (int i = vph_cut; i <= max; i++) {
				all += Relative_frequency.at(i);
			}
			ret.insert(std::make_pair(vph, all));
		}
	}
	return ret;
}
std::map<int, double> Calc_CL_Value_bin(std::vector<PID_track*> &track) {

	double all = 0;
	std::map<int, int> vph_bin;
	int bin_min, bin_max;
	//面積の計算
	for (auto itr = track.begin(); itr != track.end();itr++) {
		if (itr == track.begin()) {
			bin_min = int(round((*itr)->vph));
			bin_max = int(round((*itr)->vph));
		}
		bin_min = std::min(bin_min, int(round((*itr)->vph)));
		bin_max = std::max(bin_max, int(round((*itr)->vph)));

		all++;
		auto res = vph_bin.insert(std::make_pair(int(round((*itr)->vph) + 0.0001), 1));
		if (!res.second)res.first->second++;
	}
	//統計0の部分0埋め
	//bin_minからでもいい?でもどうせ1になる
	for (int i = 1; i <= bin_max; i++) {
		auto res = vph_bin.insert(std::make_pair(i, 0));
	}
	//for (auto itr = vph_bin.begin(); itr != vph_bin.end(); itr++) {	
	//	printf("%d %d\n", itr->first, itr->second);
	//}

	//CLの計算
	std::map<int, double> ret;
	int sum = 0;
	for (auto itr = vph_bin.rbegin(); itr != vph_bin.rend(); itr++) {
		sum += itr->second;
		ret.insert(std::make_pair(itr->first, sum*1.0 / all));
	}

	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	printf("%d %.5lf\n", itr->first, itr->second);
	//}

	return ret;
}
double Calc_gaus(double mean, double sigma, double val) {
	return 1. / sigma * exp(-1 * pow(val - mean, 2) / (2 * sigma*sigma));
}

void write_CL(std::string filename, std::vector <VPH_CL>&cl) {
	std::ofstream ofs(filename, std::ios::app);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (cl.size() == 0) {
		fprintf(stderr, "target track ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = cl.size();
	for (auto itr = cl.begin(); itr != cl.end();itr++) {
		std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		count++;
		for (auto itr2 = itr->cl_map.begin(); itr2!=itr->cl_map.end(); itr2++) {
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << itr->pos << " "
				<< std::setw(2) << std::setprecision(0) << itr->trackingid << " "
				<< std::setw(2) << std::setprecision(0) << itr->sensorid << " "
				<< std::setw(3) << std::setprecision(1) << itr->angle_min << " "
				<< std::setw(3) << std::setprecision(1) << itr->angle_max << " "
				<< std::setw(4) << std::setprecision(0) << itr2->first << " "
				<< std::setw(15) << std::setprecision(14) << itr2->second << std::endl;
		}
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();
}
