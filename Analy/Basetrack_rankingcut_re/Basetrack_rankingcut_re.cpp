#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <set>

std::set<std::tuple<int, int, int, int>>read_Area0_base(std::string file_path, int pl);
std::vector<vxx::base_track_t> read_Areax_base(std::string file_path, int pl, int area, std::set<std::tuple<int, int, int, int>>&base0_id);
vxx::base_track_t base_best_selection(vxx::base_track_t&signal, std::vector<vxx::base_track_t>&base);
void read_all_tracking_base(std::string file_path, int pl, int area, std::vector<vxx::base_track_t> &base, std::vector<vxx::base_track_t> base_all[8]);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg ECC-path pl output-path(tmp)\n");
		exit(1);
	}
	std::string file_path = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_out_path = argv[3];
	//Area0 file読み込み
	std::set<std::tuple<int, int, int, int>> base0_id = read_Area0_base(file_path, pl);
	
	//Area1-6 .sel.vxx読み込み
	for (int area = 1; area <= 6; area++) {
		//Areax .sel.vxx area0とmatchする飛跡のみ読み込み
		std::vector<vxx::base_track_t> base = read_Areax_base(file_path, pl, area, base0_id);
		////thick0-4,thin0-4で対応するtrack抽出
		std::vector<vxx::base_track_t> base_all[8];
		read_all_tracking_base(file_path, pl, area, base, base_all);

		//確認用output
		vxx::BvxxWriter bw;
		for (int i = 0; i < 8; i++) {
			std::stringstream file_out;
			if (i < 4) {
				file_out << file_out_path << "\\Area" << std::setw(1) << area << "\\b" << std::setw(3) << std::setfill('0') << pl
					<< "_thick_" << std::setw(1) << i << ".sig.vxx";
			}
			else {
				file_out << file_out_path << "\\Area" << std::setw(1) << area << "\\b" << std::setw(3) << std::setfill('0') << pl
					<< "_thin_" << std::setw(1) << i % 4 << ".sig.vxx";
			}
			bw.Write(file_out.str(), pl, 0, base_all[i]);
		}
		////signal領域決定


	}

}
std::set<std::tuple<int, int, int, int>>read_Area0_base(std::string file_path, int pl) {
	std::stringstream file_in_bvxx;
	file_in_bvxx << file_path << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl<<".sel.cor.connect.vxx";

	std::vector<vxx::base_track_t> base;
	vxx::BvxxReader br;
	base = br.ReadAll(file_in_bvxx.str(), pl, 0);

	std::set<std::tuple<int, int, int, int>> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ret.insert(std::make_tuple(itr->m[0].zone, itr->m[0].rawid, itr->m[1].zone, itr->m[1].rawid));
	}
	return ret;
}
std::vector<vxx::base_track_t> read_Areax_base(std::string file_path, int pl, int area, std::set<std::tuple<int, int, int, int>>&base0_id) {
	std::stringstream file_in_bvxx;
	file_in_bvxx << file_path << "\\Area" << std::setw(1) << area
		<< "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.vxx";

	std::vector<vxx::base_track_t> base;
	vxx::BvxxReader br;
	base = br.ReadAll(file_in_bvxx.str(), pl, 0);

	std::vector<vxx::base_track_t> ret;
	std::tuple<int, int, int, int>id;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::get<0>(id) = itr->m[0].zone;
		std::get<1>(id) = itr->m[0].rawid;
		std::get<2>(id) = itr->m[1].zone;
		std::get<3>(id) = itr->m[1].rawid;
		if (base0_id.count(id) == 0)continue;
		ret.push_back(*itr);
	}


	return ret;
}
void read_all_tracking_base(std::string file_path, int pl, int area, std::vector<vxx::base_track_t> &base, std::vector<vxx::base_track_t> base_all[8]) {
	std::stringstream file_path2;
	file_path2 << file_path << "\\Area" << std::setw(1) << area
		<< "\\PL" << std::setw(3) << std::setfill('0') << pl;
	std::string filename[8];
	for (int i = 0; i < 4; i++) {
		std::stringstream filename0, filename1;
		filename0 << file_path2.str() << "\\b" << std::setw(3) << std::setfill('0') << pl << "_thick_" << std::setw(1) << i << ".sel.vxx";
		filename1 << file_path2.str() << "\\b" << std::setw(3) << std::setfill('0') << pl << "_thin_" << std::setw(1) << i << ".sel.vxx";
		filename[i] = filename0.str();
		filename[i+4] = filename0.str();
	}
	//filename[0 - 3]:thick0 - 3
	//filename[4 - 7]:thin0 - 3
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base_tracking;
	for (int i = 0; i < 8; i++) {
		base_all[i].clear();
		base_tracking.clear();
		base_tracking= br.ReadAll(filename[i], pl, 0);
		//hash
		double xmin = 999999, ymin = 999999;
		for (auto itr = base_tracking.begin(); itr != base_tracking.end(); itr++) {
			if (itr == base_tracking.begin()) {
				xmin = itr->x;
				ymin = itr->y;
			}
			xmin = std::min(itr->x, xmin);
			ymin = std::min(itr->y, ymin);
		}
		double hash_size = 1000;
		std::multimap<std::pair<int, int>, vxx::base_track_t*> base_hash;
		std::pair<int, int> id;
		for (auto itr = base_tracking.begin(); itr != base_tracking.end(); itr++) {
			id.first = (itr->x - xmin) / hash_size;
			id.second = (itr->y - ymin) / hash_size;
			base_hash.insert(std::make_pair(id, &(*itr)));
		}
		//同一track 探索
		std::vector <vxx::base_track_t>base_tmp;
		int ix, iy;
		double angle, all_pos[2], all_ang[2], diff_pos[2], diff_ang[2];
		int count = 0;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			//if (count % 1000 == 0) {
			//	fprintf(stderr, "\r track matching %d/%d(%4.1lf%%))", count, base.size(), count*100. / base.size());
			//}
			count++;

			ix = (itr->x - xmin) / hash_size;
			iy = (itr->y - ymin) / hash_size;
			angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
			//一致のallowance
			all_pos[0] = 5;
			all_pos[1] = 5 * angle + 5;
			all_ang[0] = 0.01;
			all_ang[1] = 0.03*angle + 0.01;

			base_tmp.clear();
			for (int iix = -1; iix <= 1; iix++) {
				for (int iiy = -1; iiy <= 1; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (base_hash.count(id) == 0)continue;
					auto range = base_hash.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						diff_pos[0] = (res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax;
						if (fabs(diff_pos[0]) > all_pos[0] * angle)continue;
						diff_pos[1] = (res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay;
						if (fabs(diff_pos[1]) > all_pos[1] * angle)continue;

						diff_ang[0] = (res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax;
						if (fabs(diff_ang[0]) > all_ang[0] * angle)continue;
						diff_ang[1] = (res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay;
						if (fabs(diff_ang[1]) > all_ang[1] * angle)continue;

						base_tmp.push_back(*(res->second));
					}

				}
			}
			if (base_tmp.size() == 0)continue;
			base_all[i].push_back(base_best_selection(*itr, base_tmp));
		}
	}

}
vxx::base_track_t base_best_selection(vxx::base_track_t&signal,std::vector<vxx::base_track_t>&base) {

	vxx::base_track_t sel;
	if (base.size() == 1)sel = *base.begin();
	else {
		int vph;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			if (itr->m[0].zone == signal.m[0].zone&&itr->m[0].rawid == signal.m[0].rawid&&itr->m[1].zone == signal.m[1].zone&&itr->m[1].rawid == signal.m[1].rawid) {
				sel = *itr;
				break;
			}
			if (itr == base.begin()) {
				vph = itr->m[0].ph + itr->m[1].ph;
				sel = *itr;
			}
			if (vph < itr->m[0].ph + itr->m[1].ph) {
				vph = itr->m[0].ph + itr->m[1].ph;
				sel = *itr;
			}
		}
	}
	return sel;
}
