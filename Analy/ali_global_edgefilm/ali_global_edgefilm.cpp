#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#include <chrono>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

void chain_nseg_selection(std::vector<mfile0::M_Chain>&c);
void chain_PL_selection(std::vector<mfile0::M_Chain>&c, int pl);
void chain_mom_selection(std::vector<mfile0::M_Chain>&c, int pl);

std::set<int> prediction_base(std::vector<mfile0::M_Chain>&c, int pl);

void chain_penetrate_selection(std::vector<mfile0::M_Chain>&c);
void chain_mom_selection(std::vector<mfile0::M_Chain>&c);
void pickup_baseinf(std::string filename, int pl, std::multimap<int, vxx::base_track_t*>&base_map, double &nominal_z);

void make_link(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&ret, std::vector<mfile0::M_Chain>&c, std::multimap<int, vxx::base_track_t*> &base_map);
std::vector<netscan::linklet_t> make_link(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> base_pair, corrmap0::Corrmap &param);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file_in_mfile file-in-ECC file-out-link\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_out_link = argv[3];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	chain_penetrate_selection(m.chains);
	chain_mom_selection(m.chains);

	//mfile1::write_mfile_extension("tmp.bmf", m);

	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);


	//linklet�̍쐬
	std::multimap<int, vxx::base_track_t*> base_map;
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>base_pair;
	make_link(base_pair,m.chains, base_map);
	vxx::BvxxReader br;
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		printf("PL%03d\n", itr->first);
		int pl = itr->first;
		int count = base_map.count(itr->first);
		double nominal_z = z_map.at(pl);

		std::multimap<int, vxx::base_track_t*>base_map2;
		auto range = base_map.equal_range(pl);
		for (auto res = range.first; res != range.second; res++) {
			//printf("%d %d\n", res->second->pl, res->second->rawid);
			base_map2.insert(std::make_pair(res->second->rawid, res->second));
		}
		
		std::stringstream file_in_base;
		file_in_base << file_in_ECC << "\\Area0\\PL"
			<< std::setw(3) << std::setfill('0') << pl << "\\b"
			<< std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";

		//std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), pl, 0);
		//for (auto itr = base.begin(); itr != base.end(); itr++) {
		//	if (base_map.count(itr->rawid) == 0)continue;
		//	auto range = base_map.equal_range(itr->rawid);
		//	for (auto res = range.first; res != range.second; res++) {
		//		vxx::base_track_t b = *itr;
		//		*(res->second) = b;
		//		res->second->z = nominal_z;
		//	}
		//}

		pickup_baseinf(file_in_base.str(), pl, base_map2, nominal_z);

		itr = std::next(itr, count - 1);
	}

	corrmap0::Corrmap param;
	param.position[0] = 1;
	param.position[1] = 0;
	param.position[2] = 0;
	param.position[3] = 1;
	param.position[4] = 0;
	param.position[5] = 0;

	param.angle[0] = 1;
	param.angle[1] = 0;
	param.angle[2] = 0;
	param.angle[3] = 1;
	param.angle[4] = 0;
	param.angle[5] = 0;

	std::vector<netscan::linklet_t> link = make_link(base_pair, param);

	netscan::write_linklet_bin(file_out_link, link);


}
void chain_penetrate_selection(std::vector<mfile0::M_Chain>&c) {
	std::vector<mfile0::M_Chain> pene;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->pos0 / 10 != 3)continue;
		if (itr->pos1 / 10 != 133)continue;

		pene.push_back(*itr);
	}

	printf("penetrate rejection %d --> %d(%4.1lf%%)\n", c.size(), pene.size(), pene.size()*100. / c.size());
	std::swap(c, pene);

}
void chain_mom_selection(std::vector<mfile0::M_Chain>&c) {
	std::vector<mfile0::M_Chain> ret;

	std::ofstream ofs("out_diff_theta.txt");
	for (auto &chain : c) {

		double diff1 = 0, diff2 = 0;
		int count = 0;
		for (int i = 0; i < chain.basetracks.size(); i++) {
			if (i + 1 == chain.basetracks.size())continue;
			if (chain.basetracks[i].pos / 10 % 2 == 1)continue;
			if (chain.basetracks[i + 1].pos / 10 - chain.basetracks[i].pos / 10 != 1)continue;

			double ax0, ax1, ay0, ay1, diff;
			ax0 = chain.basetracks[i].ax;
			ax1 = chain.basetracks[i + 1].ax;
			ay0 = chain.basetracks[i].ay;
			ay1 = chain.basetracks[i + 1].ay;
			if (sqrt(ax0*ax0 + ay0 * ay0) > 0.01) {
				double denominator;
				denominator = 1 / sqrt(ax0*ax0 + ay0 * ay0 + 1);
				double t[3] = {
					ax0 *denominator,
				ay0 * denominator,
				1 * denominator
				};
				denominator = 1 / sqrt(ax0*ax0 + ay0 * ay0 + pow(ax0*ax0 + ay0 * ay0, 2));
				double r[3] = {
				-1 * ax0 * denominator,
				-1 * ay0 * denominator,
					(ax0*ax0 + ay0 * ay0)*denominator
				};
				denominator = 1 / sqrt(ax0*ax0 + ay0 * ay0);
				double l[3] = {
					-1 * ay0*denominator,
					ax0*denominator,
					0
				};

				denominator = 1 / (ax1*t[0] + ay1 * t[1] + t[2]);
				diff = (ax1*l[0] + ay1 * l[1] + l[2])*denominator;
			}
			else {
				diff = ay1 - ay0;
			}
			diff1 += diff;
			diff2 += diff * diff;
			count++;
		}

		if (count < 5)continue;

		double diff_sig = sqrt(diff2 / count - pow(diff1 / count, 2));
		double ax = mfile0::chain_ax(chain);
		double ay = mfile0::chain_ay(chain);
		ofs << std::right << std::fixed
			<< std::setw(7) << std::setprecision(5) << ax << " "
			<< std::setw(7) << std::setprecision(5) << ay << " "
			<< std::setw(7) << std::setprecision(5) << diff_sig << std::endl;
		//if (diff_sig < 0.01) {
		if (diff_sig < 0.002) {
			//if (13.6 / diff_sig * sqrt(0.5*sqrt(1 + ax * ax + ay * ay) / 17.57) > 1000) {
			ret.push_back(chain);
		}

	}

	printf("mom selection fin %lld --> %lld\n", c.size(), ret.size());
	std::swap(c, ret);

}
void make_link(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>>&ret,std::vector<mfile0::M_Chain>&c,std::multimap<int,vxx::base_track_t*> &base_map) {
	//std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		std::pair<vxx::base_track_t, vxx::base_track_t> b_pair;
		b_pair.first.pl = itr->basetracks.begin()->pos / 10;
		b_pair.first.rawid = itr->basetracks.begin()->rawid;
		b_pair.second.pl = itr->basetracks.rbegin()->pos / 10;
		b_pair.second.rawid = itr->basetracks.rbegin()->rawid;
		ret.push_back(b_pair);
	}

	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		base_map.insert(std::make_pair(itr->first.pl, &(itr->first)));
		base_map.insert(std::make_pair(itr->second.pl, &(itr->second)));
	}
}


void pickup_baseinf(std::string filename,int pl, std::multimap<int, vxx::base_track_t*>&base_map,double &nominal_z) {

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(filename, pl, 0);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (base_map.count(itr->rawid) == 0)continue;
		auto range = base_map.equal_range(itr->rawid);
		for (auto res = range.first; res != range.second; res++) {
			res->second->ax = itr->ax;
			res->second->ay = itr->ay;
			res->second->x = itr->x;
			res->second->y = itr->y;
			res->second->z = nominal_z;
			res->second->m[0].ph = itr->m[0].ph;
			res->second->m[1].ph = itr->m[1].ph;
		}
	}



}

netscan::base_track_t format_change(vxx::base_track_t &base) {
	netscan::base_track_t ret;
	ret.ax = base.ax;
	ret.ay = base.ay;
	ret.x = base.x;
	ret.y = base.y;
	ret.z = base.z;
	ret.pl = base.pl;
	ret.rawid = base.rawid;
	ret.zone = base.zone;
	return ret;

}

netscan::base_track_t format_change(vxx::base_track_t &base, corrmap0::Corrmap &param) {
	netscan::base_track_t ret;
	ret.ax = base.ax*param.angle[0] + base.ay*param.angle[1] + param.angle[4];
	ret.ay = base.ax*param.angle[2] + base.ay*param.angle[3] + param.angle[5];
	ret.x = base.x*param.position[0] + base.y*param.position[1] + param.position[4];
	ret.y = base.x*param.position[2] + base.y*param.position[3] + param.position[5];
	ret.z = base.z + param.dz;
	ret.pl = base.pl;
	ret.rawid = base.rawid;
	ret.zone = base.zone;
	return ret;

}
std::vector<netscan::linklet_t> make_link(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> base_pair, corrmap0::Corrmap &param) {

	std::vector<netscan::linklet_t> ret;
	for (auto &p : base_pair) {
		netscan::linklet_t link;
		link.b[0] = format_change(p.first);
		link.b[1] = format_change(p.second,param);
		link.pos[0] = link.b[0].pl * 10;
		link.pos[1] = link.b[1].pl * 10;
		link.xc = 0;
		link.yc = 0;
		link.dx = link.b[1].x - link.b[0].x - (link.b[0].ax + link.b[1].ax) / 2 * (link.b[1].z - link.b[0].z);
		link.dy = link.b[1].y - link.b[0].y - (link.b[0].ay + link.b[1].ay) / 2 * (link.b[1].z - link.b[0].z);

		ret.push_back(link);
	}
	return ret;

}
