#pragma comment(lib, "VxxReader.lib")
#include <VxxReader.h>
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <set>
std::vector<vxx::base_track_t> linklet_selection(std::vector<vxx::base_track_t>&base, std::vector<netscan::linklet_t> &link);

int main(int argc, char** argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg input-bvxx pl output-bvxx input-linklet0 input-linklet1");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_out_bvxx = argv[3];
	std::string file_in_linklet0 = argv[4];
	std::string file_in_linklet1 = argv[5];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);

	std::vector<netscan::linklet_t> link0, link1;
	netscan::read_linklet_bin(file_in_linklet0, link0);
	netscan::read_linklet_bin(file_in_linklet1, link1);
	base = linklet_selection(base, link0);
	base = linklet_selection(base, link1);
	vxx::BvxxWriter bw;
	bw.Write(file_out_bvxx, pl, 0, base);

}
std::vector<vxx::base_track_t> linklet_selection(std::vector<vxx::base_track_t>&base, std::vector<netscan::linklet_t> &link) {
	int pl = base.begin()->pl;
	std::set<int> rawid;
	int face;
	if (link.begin()->pos[0] / 10 == pl) {
		face = 0;
	}
	else if (link.begin()->pos[1] / 10 == pl) {
		face = 1;
	}
	else {
		fprintf(stderr, "not same pl\n");
		fprintf(stderr, "basetrack %d\n", pl);
		fprintf(stderr, "linklet %d-%d\n", link.begin()->pos[0] / 10, link.begin()->pos[1] / 10);
		exit(1);
	}
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		rawid.insert(itr->b[face].rawid);
	}

	std::vector<vxx::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (rawid.count(itr->rawid) == 1) {
			ret.push_back(*itr);
		}
	}
	fprintf(stderr, "linklet %d -%d selection: %d --> %d\n", link.begin()->pos[0] / 10, link.begin()->pos[1] / 10,base.size(), ret.size());
	return ret;
}