#include <filesystem>
#include <fstream>
#include <map>
#include <sstream>
#include <picojson.h>
#include <set>
class GrainInformation {
public:
	int id, face, ix, iy, layer, pixelnum, brightness_sum9;
	double x, y, z;
};
class AffineParam {
public:
	double rotation, shrink, shift[2];
};

std::vector<GrainInformation> read_grain_inf(std::string filename, int &face);
void read_param_dc(std::string filename, AffineParam&param, AffineParam&param_dc);
void apply_param(std::vector<GrainInformation>&grain, AffineParam&param, AffineParam&param_dc, int face);
void output_grain(std::string filename, std::vector< GrainInformation > &grain);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg file-in-grain file-param output-file-face\n");
		exit(1);
	}
	std::string file_in_grain = argv[1];
	std::string file_in_param = argv[2];
	std::string file_out_grain = argv[3];

	int face;
	std::vector<GrainInformation> grain_all = read_grain_inf(file_in_grain, face);
	AffineParam param, param_dc;
	read_param_dc(file_in_param, param, param_dc);
	apply_param(grain_all, param, param_dc, face);
	output_grain(file_out_grain, grain_all);
	






}
std::vector<GrainInformation> read_grain_inf(std::string filename, int &face) {
	std::ifstream ifs(filename);
	std::vector<GrainInformation> ret;
	GrainInformation gr;
	int count = 0;
	while (ifs >> gr.face >> gr.ix >> gr.iy >> gr.layer >> gr.x >> gr.y >> gr.z >> gr.pixelnum >> gr.brightness_sum9) {
		if (count % 100000 == 0) {
			printf("\r grain read %d", count);
		}
		face = gr.face;
		gr.id = count;
		count++;
		ret.push_back(gr);
	}
	printf("\r grain read fin %d\n", ret.size());
	return ret;

}
void read_param_dc(std::string filename, AffineParam&param, AffineParam&param_dc) {

	std::ifstream ifs(filename);
	ifs >> param.rotation >> param.shrink >> param.shift[0] >> param.shift[1];
	ifs >> param_dc.rotation >> param_dc.shrink >> param_dc.shift[0] >> param_dc.shift[1];



}

void apply_param(std::vector<GrainInformation>&grain, AffineParam&param, AffineParam&param_dc,int face) {
	double tmp_x, tmp_y;
	double z0, z1;
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		if (itr == grain.begin()) {
			z0 = itr->z;
			z1 = itr->z;
		}
		z0 = std::min(itr->z, z0);
		z1 = std::max(itr->z, z1);
		tmp_x = itr->x;
		tmp_y = itr->y;
		itr->x = param.shrink*(tmp_x*cos(param.rotation) - tmp_y * sin(param.rotation)) + param.shift[0];
		itr->y = param.shrink*(tmp_x*sin(param.rotation) + tmp_y * cos(param.rotation)) + param.shift[1];

	}
	double z_border=0;
	if (face == 1) {
		z_border = z1;
	}
	else if (face == 2) {
		z_border = z0;
	}
	double dz;
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		dz = itr->z - z_border;
		dz /= param_dc.shrink;

		itr->z = dz+z_border;
		itr->x += dz *param_dc.shift[0];
		itr->y += dz *param_dc.shift[1];

	}




}

void output_grain(std::string filename, std::vector< GrainInformation > &grain) {
	std::ofstream ofs(filename);
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {

		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->face << " "
			<< std::setw(3) << std::setprecision(0) << itr->ix << " "
			<< std::setw(3) << std::setprecision(0) << itr->iy << " "
			<< std::setw(10) << std::setprecision(0) << itr->layer << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(10) << std::setprecision(1) << itr->z << " "
			<< std::setw(10) << std::setprecision(0) << itr->pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->brightness_sum9 << std::endl;


	}
}