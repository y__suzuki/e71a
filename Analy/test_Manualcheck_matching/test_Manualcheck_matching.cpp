#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <fstream>
#include <map>
#include <ios>     // std::left, std::right
#include <iomanip> 

class uts_track {
public:
	int viewid, rawid, ph;
	double ax, ay, x, y;
};

std::vector<uts_track> ReadUTStrack(std::string filename);
std::vector<vxx::micro_track_t> PHcut(std::vector<vxx::micro_track_t>&micro);
std::vector<std::pair<vxx::micro_track_t, uts_track>> track_matching(std::vector<vxx::micro_track_t> &micro, std::vector<uts_track>&uts);
bool matching_condition_0(vxx::micro_track_t micro, uts_track uts);
void output_result(std::string filename, std::vector<std::pair<vxx::micro_track_t, uts_track>> match);
void output_fvxx_txt(std::string filename, std::vector<vxx::micro_track_t> micro);


int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "prg input-vxx(fvxx) input-txt(UTS) pl output fvxx-txt-out\n");
		exit(1);
	}
	std::string file_in_fvxx = argv[1];
	std::string file_in_txt = argv[2];
	int pl = std::stoi(argv[3]);
	int pos = pl * 10 + 2;
	std::string file_out_txt = argv[4];
	std::string file_out_fvxx_txt = argv[5];

	//fvxx読み込み
	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> fvxx = fr.ReadAll(file_in_fvxx, pos, 0);
	//phcut
	fvxx = PHcut(fvxx);
	//txt読み込み
	std::vector<uts_track> uts = ReadUTStrack(file_in_txt);
	//track matching
	std::vector<std::pair<vxx::micro_track_t, uts_track>> match = track_matching(fvxx, uts);

	//matching結果の出力
	output_result(file_out_txt, match);
	//使ったHTSのtrackを出力
	output_fvxx_txt(file_out_fvxx_txt, fvxx);
}
std::vector<uts_track> ReadUTStrack(std::string filename) {
	std::vector<uts_track> ret;

	std::ifstream ifs(filename);

	uts_track uts_tmp;
	while (ifs >> uts_tmp.viewid >> uts_tmp.rawid >> uts_tmp.ph >> uts_tmp.ax >> uts_tmp.ay >> uts_tmp.x >> uts_tmp.y) {
		ret.push_back(uts_tmp);
	}

	fprintf(stderr, "read uts track %d\n", ret.size());
	return ret;
}
std::vector<vxx::micro_track_t> PHcut(std::vector<vxx::micro_track_t>&micro) {
	std::vector<vxx::micro_track_t> ret;
	double angle;
	int ang_id;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		ang_id = int(angle * 10);
		switch (ang_id) {
		case 0:
			if (itr->ph % 10000 >= 10)ret.push_back(*itr);
			break;
		case 1:
			if (itr->ph % 10000 >= 9)ret.push_back(*itr);
			break;
		case 2:
			if (itr->ph % 10000 >= 8)ret.push_back(*itr);
			break;
		case 3:
			if (itr->ph % 10000 >= 8)ret.push_back(*itr);
			break;
		case 4:
			if (itr->ph % 10000 >= 8)ret.push_back(*itr);
			break;
		case 5:
			if (itr->ph % 10000 >= 8)ret.push_back(*itr);
			break;
		default:
			ret.push_back(*itr);
			break;
		}
	}

	fprintf(stderr, "mcirotrack PH cut %d --> %d\n", micro.size(), ret.size());
	return ret;
}

std::vector<std::pair<vxx::micro_track_t, uts_track>> track_matching(std::vector<vxx::micro_track_t> &micro, std::vector<uts_track>&uts) {
	std::vector<std::pair<vxx::micro_track_t, uts_track>> match;

	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		for (auto itr2 = uts.begin(); itr2 != uts.end(); itr2++) {
			//matching
			if (matching_condition_0(*itr, *itr2)) {
				match.push_back(std::make_pair(*itr, *itr2));
			}
		}
	}
	return match;
}
inline bool matching_condition_0(vxx::micro_track_t micro, uts_track uts) {
	if (fabs(micro.ax - uts.ax) > 0.1)return false;
	if (fabs(micro.ay - uts.ay) > 0.1)return false;
	return true;
}
void output_result(std::string filename,std::vector<std::pair<vxx::micro_track_t, uts_track>> match) {
	std::vector<std::pair<double, double>> position_diff;
	for (auto itr = match.begin(); itr != match.end(); itr++) {
		position_diff.push_back(std::make_pair(itr->first.x - itr->second.x, itr->first.y - itr->second.y));
	}
	//何mu pichで出力するか
	double hash = 10;
	int dx, dy;
	std::map<std::pair<int, int>, int> count;
	for (auto itr = position_diff.begin(); itr != position_diff.end(); itr++) {
		dx = itr->first / 10;
		dy = itr->second/ 10;
		auto res = count.insert(std::make_pair(std::make_pair(dx, dy), 1));
		if (!res.second) {
			res.first->second++;
		}
	}

	std::ofstream  ofs(filename);
	for (auto itr = count.begin(); itr != count.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(1) << (int)(itr->first.first - hash / 2) << " "
			<< std::setw(8) << std::setprecision(1) << (int)(itr->first.first + hash / 2) << " "
			<< std::setw(8) << std::setprecision(1) << (int)(itr->first.second - hash / 2) << " "
			<< std::setw(8) << std::setprecision(1) << (int)(itr->first.second + hash / 2) << " "
			<< std::setw(8) << std::setprecision(0) << (int)(itr->second) << std::endl;
	}

}
void output_fvxx_txt(std::string filename, std::vector<vxx::micro_track_t> micro) {
	std::ofstream  ofs(filename);
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(8) << std::setprecision(0) << itr->ph << " "
			<< std::setw(8) << std::setprecision(4) << itr->ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->x << " "
			<< std::setw(8) << std::setprecision(1) << itr->y << std::endl;
	}
	
}