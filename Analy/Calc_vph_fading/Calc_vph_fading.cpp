#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_format {
public:
	int groupid, chainid, nseg, npl, vph_count,spot;
	float vph_mean, vph_error, ecc_mcs_mom, angle;
};

std::map<int, vxx::base_track_t> read_base_map(std::string file_in_ECC, int pl);
void sharingfile_pl_divide(std::vector<Sharing_file::Sharing_file>&sf, std::map<std::tuple<int, int, int, int>, Sharing_file::Sharing_file>&sf_pl3, std::map<std::tuple<int, int, int, int>, Sharing_file::Sharing_file>&sf_pl4);
std::vector<output_format> Calc_average(std::vector<Momentum_recon::Mom_chain> &momch
	, std::map<int, vxx::base_track_t> &base_pl3, std::map<int, vxx::base_track_t> &base_pl4
	, std::map<std::tuple<int, int, int, int>, Sharing_file::Sharing_file>&sf_pl3, std::map<std::tuple<int, int, int, int>, Sharing_file::Sharing_file>&sf_pl4);

void Calc_vph_mean(std::vector<Momentum_recon::Mom_basetrack>&base, output_format &out);
double Calc_angle(Momentum_recon::Mom_chain&c);
void output_file_bin(std::string filename, std::vector<output_format>&out);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_in_sf = argv[2];
	std::string file_in_ECC = argv[3];
	std::string file_out_bin = argv[4];


	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_extension(file_in_sf);
	std::map<std::tuple<int, int, int, int>, Sharing_file::Sharing_file> sf_pl3, sf_pl4;
	sharingfile_pl_divide(sf, sf_pl3, sf_pl4);

	std::map<int, vxx::base_track_t> base_pl3 = read_base_map(file_in_ECC, 3);
	std::map<int, vxx::base_track_t> base_pl4 = read_base_map(file_in_ECC, 4);

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);

	std::vector<output_format> out = Calc_average(momch
		, base_pl3, base_pl4
		, sf_pl3, sf_pl4);
	output_file_bin(file_out_bin, out);
	
}
std::map<int, vxx::base_track_t> read_base_map(std::string file_in_ECC, int pl) {
	std::stringstream file_in_base;
	file_in_base << file_in_ECC << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl << "\\b"
		<< std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t>base = br.ReadAll(file_in_base.str(), pl, 0);

	std::map<int, vxx::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ret.insert(std::make_pair(itr->rawid, *itr));
	}
	return ret;
}

void sharingfile_pl_divide(std::vector<Sharing_file::Sharing_file>&sf, std::map<std::tuple<int,int,int,int>, Sharing_file::Sharing_file>&sf_pl3, std::map<std::tuple<int, int, int, int>, Sharing_file::Sharing_file>&sf_pl4) {
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		if (itr->pl == 3) {
			sf_pl3.insert(std::make_pair(std::make_tuple(itr->zone[0], itr->rawid[0], itr->zone[1], itr->rawid[1]), *itr));
		}
		else if (itr->pl == 4) {
			sf_pl4.insert(std::make_pair(std::make_tuple(itr->zone[0], itr->rawid[0], itr->zone[1], itr->rawid[1]), *itr));
		}
	}
} 

std::vector<output_format> Calc_average(std::vector<Momentum_recon::Mom_chain> &momch
	, std::map<int, vxx::base_track_t> &base_pl3, std::map<int, vxx::base_track_t> &base_pl4
	, std::map<std::tuple<int, int, int, int>, Sharing_file::Sharing_file>&sf_pl3, std::map<std::tuple<int, int, int, int>, Sharing_file::Sharing_file>&sf_pl4) {
	
	std::vector<output_format>ret;

	int pl_min;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		pl_min = itr->base.begin()->pl;
		if (pl_min != 3 && pl_min != 4)continue;
		output_format out;
		out.angle = Calc_angle(*itr);
		out.chainid = itr->chainid;
		out.groupid = itr->groupid;
		out.ecc_mcs_mom = itr->ecc_mcs_mom;
		out.npl = itr->base.rbegin()->pl - itr->base.begin()->pl + 1;
		out.nseg = itr->base.size();

		Calc_vph_mean(itr->base, out);
		if (pl_min == 3) {
			auto res = base_pl3.find(itr->base.begin()->rawid);
			if (res == base_pl3.end()) {
				fprintf(stderr, "PL%03d rawid=%d not found\n", pl_min, itr->base.begin()->rawid);
				exit(2);
			}
			auto res2 = sf_pl3.find(std::make_tuple(res->second.m[0].zone, res->second.m[0].rawid, res->second.m[1].zone, res->second.m[1].rawid));
			if (res2 == sf_pl3.end())continue;
			out.spot = res2->second.spotid;
		}
		else if (pl_min == 4) {
			auto res = base_pl4.find(itr->base.begin()->rawid);
			if (res == base_pl4.end()) {
				fprintf(stderr, "PL%03d rawid=%d not found\n", pl_min, itr->base.begin()->rawid);
				exit(2);
			}
			auto res2 = sf_pl4.find(std::make_tuple(res->second.m[0].zone, res->second.m[0].rawid, res->second.m[1].zone, res->second.m[1].rawid));
			if (res2 == sf_pl4.end())continue;
			out.spot = res2->second.spotid;
		}
		ret.push_back(out);
	}
	return ret;
}

void Calc_vph_mean(std::vector<Momentum_recon::Mom_basetrack>&base, output_format &out) {
	double sum = 0, sum2 = 0;
	int count = 0, vph;
	for (auto &b : base) {
		for (int i = 0; i < 2; i++) {
			vph = b.m[i].ph % 10000;
			sum += vph;
			sum2 += vph * vph;
			count++;
		}
	}
	out.vph_count = count;
	if (count < 1) {
		out.vph_mean = -1;
		out.vph_error = -1;
		return;
	}
	out.vph_mean = sum / count;
	double sd = sum2 / count - pow(sum / count, 2);
	if (sd <= 0.001) {
		out.vph_error = sqrt(out.vph_mean);
	}
	else {
		out.vph_error = sqrt(sd)*sqrt(count) / sqrt(count - 1);
	}
}
double Calc_angle(Momentum_recon::Mom_chain&c) {
	double ax = 0;
	double ay = 0;
	int count = 0;
	for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
		ax += itr->ax;
		ay += itr->ay;
		count += 1;
	}
	ax = ax / count;
	ay = ay / count;
	return sqrt(ax*ax + ay * ay);


}
void output_file_bin(std::string filename, std::vector<output_format>&out) {
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open ���s
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (out.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = out.size();
	for (int i = 0; i < out.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& out[i], sizeof(output_format));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}

