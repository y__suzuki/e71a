#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"PID_correct.lib")
#include <PID_correct.h>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <iostream>
#include <set>
#include <omp.h>

class Chain_PID {
public:
	uint64_t chainid;
	int nseg;
	double ax, ay, angle, vph, pb, mip_vph, mip_pixel;
};
class class_tmp {
	uint64_t chainid;
	uint64_t nseg;
	double ax, ay, angle, vph, pb, mip_vph, mip_pixel;
};
int main(int argc, char**argv) {

	printf("%.4lf\n", log(DBL_MIN));
	exit(1);




	std::string file_in = "I:\\NINJA\\E71a\\work\\suzuki\\PID\\out_tcl.bin";
	Chain_PID t;

	std::ifstream ifs(file_in, std::ios::binary);
	int64_t count = 0;
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t size1 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	printf("%d\n", sizeof(class_tmp));
	printf("%d\n", sizeof(Chain_PID));
	exit(1);
	while (ifs.read((char*)& t, sizeof(Chain_PID))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			size1 = nowpos - begpos;
			printf("\r Fill ... %4.1lf%%", size1 * 100. / size2);
		}
		printf("%llu %d %5.4lf %.2lf\n", t.chainid, t.nseg, t.ax, t.vph);
		count++;
	}
}