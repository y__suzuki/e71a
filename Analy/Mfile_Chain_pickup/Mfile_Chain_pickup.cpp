#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>
#include <sstream>
std::vector<mfile0::M_Chain> chain_nseg_selection(std::vector<mfile0::M_Chain> &chain, int nseg);
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold);
std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>  &chain);
std::vector<mfile0::M_Chain> chain_angle_selection(std::vector<mfile0::M_Chain>  &chain, double thr_ax, double thr_ay);
std::vector<mfile0::M_Chain> chain_area_selection(std::vector<mfile0::M_Chain>  &chain, double xmin, double xmax, double ymin, double ymax);
std::vector<mfile0::M_Chain> inefficiency_pickup(std::vector<mfile0::M_Chain>  &chain, int pl);
std::vector<mfile0::M_Chain> all_group_select(std::vector<mfile0::M_Chain>  &chain_all, std::vector<mfile0::M_Chain>  &chain_sel);
std::vector<mfile0::M_Chain> chain_angle_selection2(std::vector<mfile0::M_Chain>  &chain, double angmin, double angmax);
std::vector<mfile0::M_Chain> chain_angle_selection3(std::vector<mfile0::M_Chain>  &chain, int x_sign, int y_sign);
std::vector<mfile0::Mfile> chain_divide(mfile0::Mfile m, int max_chain);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg in-mfile out-mfile\n");
		exit(1);
	}

	//input value
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];
	//mfile
	mfile0::Mfile m;

	//read mfile
	mfile1::read_mfile_extension(file_in_mfile, m);
	mfile0::Mfile m_all = m;

	m.chains = chain_nseg_selection(m.chains, 10);
	m.chains = chain_angle_selection(m.chains, 4.0, 4.0);
	m.chains = chain_dlat_selection(m.chains, 0.005);
	//m.chains = chain_area_selection(m.chains, 40000, 50000, 40000, 50000);
	m.chains = group_clustering(m.chains);
	int pl = 9;
	m.chains = inefficiency_pickup(m.chains, pl);
	m.chains = chain_angle_selection2(m.chains, 1.5, 2.5);
	m.chains = chain_angle_selection3(m.chains, 1, 1);

	std::vector<mfile0::Mfile> m_div = chain_divide(m, 100);
	for (int i = 0; i < m_div.size(); i++) {
		m.chains = all_group_select(m_all.chains, m_div[i].chains);
		std::stringstream ss;
		ss << file_out_mfile << "_" << std::setfill('0') << std::setw(4) << i << ".all";
			mfile0::write_mfile(ss.str(), m);
	}
}
std::vector<mfile0::M_Chain> chain_nseg_selection(std::vector<mfile0::M_Chain> &chain, int nseg) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr->nseg < nseg)continue;
		ret.push_back(*itr);
	}
	printf("chain nseg >= %d: %d --> %d (%4.1lf%%)\n", nseg, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>  &chain) {

	std::vector<mfile0::M_Chain> ret;
	std::set<int>gid;
	std::multimap<int, mfile0::M_Chain*>group;
	int nseg_max;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		gid.insert(itr->basetracks[0].group_id);
		group.insert(std::make_pair(itr->basetracks[0].group_id, &(*itr)));
	}
	for (auto itr = gid.begin(); itr != gid.end(); itr++) {
		if (group.count(*itr) == 1) {
			ret.push_back(*(group.find(*itr)->second));
		}
		else {
			auto range = group.equal_range(*itr);
			nseg_max = 1;
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				nseg_max = std::max(itr2->second->nseg, nseg_max);
			}
			for (auto itr2 = range.first; itr2 != range.second; itr2++) {
				if (itr2->second->nseg == nseg_max) {
					ret.push_back(*(itr2->second));
					break;
				}
			}
		}
	}
	printf("chain group clustering: %d --> %d (%4.1lf%%)\n", chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;

}
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (mfile0::angle_diff_dev_lat(*itr, ax, ay) > threshold)continue;
		ret.push_back(*itr);
	}
	printf("chain lateral selection <= %5.4lf : %d --> %d (%4.1lf%%)\n", threshold, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;

}
std::vector<mfile0::M_Chain> chain_angle_selection(std::vector<mfile0::M_Chain>  &chain, double thr_ax, double thr_ay) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (thr_ax - fabs(ax) < 0)continue;
		if (thr_ay - fabs(ay) < 0)continue;
		ret.push_back(*itr);
	}
	printf("chain average angle selection |ax|<=%3.1lf |ay|<=%3.1lf : %d --> %d (%4.1lf%%)\n", thr_ax, thr_ay, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::M_Chain> chain_area_selection(std::vector<mfile0::M_Chain>  &chain, double xmin, double xmax, double ymin, double ymax) {
	std::vector<mfile0::M_Chain> ret;
	bool flg;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		flg = false;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (xmin > itr2->x)continue;
			if (xmax < itr2->x)continue;
			if (ymin > itr2->y)continue;
			if (ymax < itr2->y)continue;
			flg = true;
		}
		if (flg) {
			ret.push_back(*itr);
		}
	}
	printf("chain area selection %8.1lf < x < %8.1lf, %8.1lf < y < %8.1lf : %d --> %d (%4.1lf%%)\n", xmin, xmax, ymin, ymax, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::M_Chain> inefficiency_pickup(std::vector<mfile0::M_Chain>  &chain, int PL) {
	std::vector<mfile0::M_Chain> chain_sel;
	int flg = 0;
	double dz;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		flg = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (PL == 1) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 6) {
					flg++;
				}
			}
			else if (PL == 2) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 5) {
					flg++;
				}
			}
			else if (PL == 3) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 4) {
					flg++;
				}
			}
			else if (4 <= PL && PL <= 14) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 3) {
					flg++;
				}
			}
			else if (PL == 15) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 4) {
					flg++;
				}
			}
			else if (PL == 16) {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 5) {
					flg++;
				}
			}
			else {
				if (abs(itr2->pos / 10 - PL) != 0 && abs(itr2->pos / 10 - PL) <= 6) {
					flg++;
				}
			}
			if (itr2->pos / 10 == 16 || itr2->pos / 10 == 17) {
				flg++;
			}
		}
		if (flg == 8) {
			chain_sel.push_back(*itr);
		}
	}

	std::vector<mfile0::M_Chain> ret;
	for (auto itr = chain_sel.begin(); itr != chain_sel.end(); itr++) {
		flg = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (PL == itr2->pos / 10) {
				flg = 1;
			}
		}
		if (flg == 0) {
			ret.push_back(*itr);
		}
	}
	return ret;
}
std::vector<mfile0::M_Chain> all_group_select(std::vector<mfile0::M_Chain>  &chain_all, std::vector<mfile0::M_Chain>  &chain_sel) {
	std::multimap<int, mfile0::M_Chain*> group_map;
	for (auto itr = chain_all.begin(); itr != chain_all.end(); itr++) {
		group_map.insert(std::make_pair(itr->basetracks.begin()->group_id, &(*itr)));
	}
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = chain_sel.begin(); itr != chain_sel.end(); itr++) {
		if (group_map.count(itr->basetracks.begin()->group_id) == 0) {
			fprintf(stderr, "not found this ID :%d\n", itr->basetracks.begin()->group_id);
		}
		else {
			auto res = group_map.equal_range(itr->basetracks.begin()->group_id);
			for (auto range = res.first; range != res.second; range++) {
				ret.push_back(*range->second);
			}
		}
	}
	return ret;
}
std::vector<mfile0::M_Chain> chain_angle_selection2(std::vector<mfile0::M_Chain>  &chain, double angmin, double angmax) {
	std::vector<mfile0::M_Chain> ret;
	double angle;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		angle = sqrt(pow(mfile0::chain_ax(*itr), 2) + pow(mfile0::chain_ay(*itr), 2));
		if (angmin <= angle && angle < angmax) {
			ret.push_back(*itr);
		}
	}
	fprintf(stderr, "anglecut2 %.1lf <= angle < %.1lf: %d --> %d(%4.1lf%%)\n", angmin, angmax, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}std::vector<mfile0::M_Chain> chain_angle_selection3(std::vector<mfile0::M_Chain>  &chain, int x_sign, int y_sign){
	std::vector<mfile0::M_Chain> ret;
	double angle_x,angle_y;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		angle_x = mfile0::chain_ax(*itr);
		angle_y = mfile0::chain_ay(*itr);
		if (x_sign*angle_x < 0)continue;
		if (y_sign*angle_y < 0)continue;
			ret.push_back(*itr);
	}
	fprintf(stderr, "anglecut3 angle x ,angle y sign : %d --> %d(%4.1lf%%)\n",chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::Mfile> chain_divide(mfile0::Mfile m, int max_chain) {
	int denominator = m.chains.size()/max_chain;
	std::vector<mfile0::Mfile> ret;
	for (int i = 0; i < denominator; i++) {
		mfile0::Mfile m_tmp;
		m_tmp.header = m.header;
		m_tmp.chains.reserve(max_chain + 10);
		ret.push_back(m_tmp);
	}
	int num;
	for (int i = 0; i < m.chains.size(); i++) {
		num = i % denominator;
		ret[num].chains.push_back(m.chains[i]);
	}
	printf("chain divide maxtrack = %d\n", max_chain);
	for (int i = 0; i < ret.size(); i++) {
		printf("mfile %d: %d\n", i, ret[i].chains.size());
	}
	return ret;
}