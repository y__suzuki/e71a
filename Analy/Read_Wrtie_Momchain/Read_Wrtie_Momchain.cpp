
#define _CRT_SECURE_NO_WARNINGS
//#pragma comment(lib, "VxxReader.lib")
//#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <filesystem>
#include <omp.h>

int main(int argc,char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-momchain file-out-momchain\n");
		exit(1);
	}

	std::string file_in_mom = argv[1];
	std::string file_out_mom = argv[2];

	std::vector<Momentum_recon::Event_information> mom = Momentum_recon::Read_Event_information_extension(file_in_mom);
	Momentum_recon::Write_Event_information_extension(file_out_mom, mom);

}