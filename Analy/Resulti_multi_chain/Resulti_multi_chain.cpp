#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <map>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <stack>
#include <unordered_set>
namespace mfile0 {
	bool operator<(const M_Base&left, const M_Base&right) {
		if (left.pos == right.pos) {
			return left.rawid < right.rawid;
		}
		return left.pos < right.pos;
	}
	bool operator==(const M_Base&left, const M_Base&right) {
		return left.pos == right.pos&&left.rawid == right.rawid;
	}
}
bool sort_chain(mfile0::M_Chain &left,mfile0::M_Chain &right) {
	if (left.basetracks.begin()->group_id != right.basetracks.begin()->group_id) {
		return left.basetracks.begin()->group_id < right.basetracks.begin()->group_id;
	}
	else if (left.basetracks.begin()->flg_i[1] != right.basetracks.begin()->flg_i[1]) {
		return left.basetracks.begin()->flg_i[1] < right.basetracks.begin()->flg_i[1];
	}
	else {
		return left.chain_id < right.chain_id;
	}
	return left.chain_id < right.chain_id;
}
void select_multi_chain(std::vector<mfile0::M_Chain> &all, std::vector<mfile0::M_Chain> &single, std::vector<std::vector<mfile0::M_Chain>> &multi);
mfile0::M_Chain select_best_chain(std::vector<mfile0::M_Chain>&multi);
mfile0::M_Chain select_better_chain(mfile0::M_Chain &c1, mfile0::M_Chain&c2);
void chain_extrapolate_x(mfile0::M_Chain&c, double z, double &x, double &angle);
void chain_extrapolate_y(mfile0::M_Chain&c, double z, double &y, double &angle);
double Calc_chain_rms(mfile0::M_Chain&c);


int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "prg file-in-mfile file-out-mfile\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];
	mfile0::Mfile m, out;
	mfile0::read_mfile(file_in_mfile, m);
	out.header = m.header;

	std::vector<std::vector<mfile0::M_Chain>> multi;
	std::vector<mfile0::M_Chain> single;
	select_multi_chain(m.chains, single, multi);
	for (auto itr = single.begin(); itr != single.end(); itr++) {
		out.chains.push_back(*itr);
	}

	for (int i = 0; i < multi.size(); i++) {
		//printf("size = %d\n", multi[i].size());
		//printf("gid=%d\n", multi[i][0].basetracks[0].group_id);
		out.chains.push_back(select_best_chain(multi[i]));
	}

	std::sort(out.chains.begin(), out.chains.end(), sort_chain);
	mfile0::write_mfile(file_out_mfile, out);

}
void select_multi_chain(std::vector<mfile0::M_Chain> &all, std::vector<mfile0::M_Chain> &single,std::vector<std::vector<mfile0::M_Chain>> &multi) {

	std::multimap<int, mfile0::M_Chain> group;
	for (auto &c : all) {
		group.insert(std::make_pair(c.basetracks.begin()->group_id, c));
	}

	for (auto itr = group.begin(); itr != group.end(); itr++) {
		int count = group.count(itr->first);
		if (count == 1) {
			single.push_back(itr->second);
		}
		else {
			std::vector<mfile0::M_Chain> g;
			auto range = group.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				g.push_back(res->second);
			}
			multi.push_back(g);
		}

		itr = std::next(itr, count - 1);
	}
}
mfile0::M_Chain select_best_chain(std::vector<mfile0::M_Chain>&multi) {
	mfile0::M_Chain ret = multi[0];
	for (int i = 1; i < multi.size(); i++) {
		//ret - multi[i]の比較

		ret = select_better_chain(ret, multi[i]);

	}

	return ret;
}

mfile0::M_Chain select_better_chain(mfile0::M_Chain &c1, mfile0::M_Chain&c2) {

	std::set<mfile0::M_Base> b1, b2;
	mfile0::M_Base ori;
	ori.pos = 0;
	ori.rawid = -1;
	for (auto itr = c1.basetracks.begin(); itr != c1.basetracks.end(); itr++) {
		b1.insert(*itr);
		if (itr->flg_i[0] == 1)ori = *itr;
	}
	for (auto itr = c2.basetracks.begin(); itr != c2.basetracks.end(); itr++) {
		b2.insert(*itr);
	}

	std::set<mfile0::M_Base> b1_only, b2_only,share;
	for (auto itr = b1.begin(); itr != b1.end(); itr++) {
		if (b2.count(*itr) == 1) {
			share.insert(*itr);
		}
		else {
			b1_only.insert(*itr);
		}
	}
	for (auto itr = b2.begin(); itr != b2.end(); itr++) {
		if (b1.count(*itr) == 1) {
			share.insert(*itr);
		}
		else {
			b2_only.insert(*itr);
		}
	}


	if (share.count(ori) == 0) {
		fprintf(stderr, "origin basetrack not found\n");
		fprintf(stderr, "gid = %d PL%03d rawid=%d\n",ori.group_id,ori.pos/10,ori.rawid);
	}

	//track 1本 or 2本
	//同じPLの異なる2本が500um以上離れていたら違う飛跡
	bool same_track_flg = true;
	for (auto itr = b1_only.begin(); itr != b1_only.end(); itr++) {
		for (auto itr2 = b2_only.begin(); itr2 != b2_only.end(); itr2++) {
			if (itr->pos / 10 == itr2->pos / 10) {
				double dist;
				dist = sqrt(pow(itr->x - itr2->x, 2) + pow(itr->y - itr2->y, 2));
				if (dist > 500)same_track_flg = false;
			}
		}
	}

	mfile0::M_Chain ret;
	std::map<int, mfile0::M_Base> base;
	if (same_track_flg) {

		for (auto itr = share.begin(); itr != share.end(); itr++) {
			base.insert(std::make_pair(itr->pos / 10, *itr));
			ret.basetracks.push_back(*itr);
		}
		std::sort(ret.basetracks.begin(), ret.basetracks.end());
		//b1とb2で同一PLに異なるtrackあり

		std::multimap<double, mfile0::M_Base> b1_dist;
		std::multimap<double, mfile0::M_Base> b2_dist;
		//距離でsort
		for (auto itr = b1_only.begin(); itr != b1_only.end(); itr++) {
			b1_dist.insert(std::make_pair(fabs(itr->z - ori.z), *itr));
		}
		for (auto itr = b2_only.begin(); itr != b2_only.end(); itr++) {
			b2_dist.insert(std::make_pair(fabs(itr->z - ori.z), *itr));
		}

		for (auto itr = b1_dist.begin(); itr != b1_dist.end(); itr++) {
			if (base.count(itr->second.pos / 10) == 1)continue;
			for (auto itr2 = b2_only.begin(); itr2 != b2_only.end(); itr2++) {
				if (itr->second.pos / 10 == itr2->pos / 10) {
					double x_chain, y_chain,ax_chain,ay_chain;
					chain_extrapolate_x(ret, itr->second.z, x_chain, ax_chain);
					chain_extrapolate_y(ret, itr->second.z, y_chain, ay_chain);
					double dist1 = pow(itr->second.ax - ax_chain, 2) + pow(itr->second.ay - ay_chain, 2);
					double dist2 = pow(itr2->ax - ax_chain, 2) + pow(itr2->ay - ay_chain, 2);
					if (dist1 < dist2) {
						base.insert(std::make_pair(itr->second.pos / 10, itr->second));
						ret.basetracks.push_back(itr->second);
					}
					else {
						base.insert(std::make_pair(itr2->pos / 10, *itr2));
						ret.basetracks.push_back(*itr2);
					}
				}
			}
		}

		for (auto itr = b2_dist.begin(); itr != b2_dist.end(); itr++) {
			if (base.count(itr->second.pos / 10) == 1)continue;
			for (auto itr2 = b1_only.begin(); itr2 != b1_only.end(); itr2++) {
				if (itr->second.pos / 10 == itr2->pos / 10) {
					double x_chain, y_chain, ax_chain, ay_chain;
					chain_extrapolate_x(ret, itr->second.z, x_chain, ax_chain);
					chain_extrapolate_y(ret, itr->second.z, y_chain, ay_chain);
					double dist1 = pow(itr->second.ax - ax_chain, 2) + pow(itr->second.ay - ay_chain, 2);
					double dist2 = pow(itr2->ax - ax_chain, 2) + pow(itr2->ay - ay_chain, 2);
					if (dist1 < dist2) {
						base.insert(std::make_pair(itr->second.pos / 10, itr->second));
						ret.basetracks.push_back(itr->second);
					}
					else {
						base.insert(std::make_pair(itr2->pos / 10, *itr2));
						ret.basetracks.push_back(*itr2);
					}
				}
			}
		}

		for (auto itr = b1_only.begin(); itr != b1_only.end(); itr++) {
			if (base.count(itr->pos / 10) == 1)continue;
			base.insert(std::make_pair(itr->pos / 10, *itr));
			ret.basetracks.push_back(*itr);
		}
		for (auto itr = b2_only.begin(); itr != b2_only.end(); itr++) {
			if (base.count(itr->pos / 10) == 1)continue;
			base.insert(std::make_pair(itr->pos / 10, *itr));
			ret.basetracks.push_back(*itr);
		}

		std::sort(ret.basetracks.begin(), ret.basetracks.end());
		ret.chain_id = c1.chain_id;
		ret.pos0 = ret.basetracks.begin()->pos;
		ret.pos1 = ret.basetracks.rbegin()->pos;
		ret.nseg = ret.basetracks.size();
		return ret;

	}

	//track 2本-->良いほうを選ぶ
	//共通部分trackとそれぞれのtrack部分で比較
	//高運動量なら長さ/角度差
	//低運動量なら

	//share部分は入れる
	for (auto itr = share.begin(); itr != share.end(); itr++) {
		ret.basetracks.push_back(*itr);
	}
	mfile0::M_Chain c_m1,c_m2;
	for (auto itr = b1_only.begin(); itr != b1_only.end(); itr++) {
		c_m1.basetracks.push_back(*itr);
	}
	for (auto itr = b2_only.begin(); itr != b2_only.end(); itr++) {
		c_m2.basetracks.push_back(*itr);
	}
	//両方ともshortの場合
	if (c_m1.basetracks.size() < 3 && c_m2.basetracks.size() < 3) {
		//rmsの小さいほう選ぶ
		double diff_lat1 = Calc_chain_rms(c1);
		double diff_lat2 = Calc_chain_rms(c2);

		if (diff_lat1 < diff_lat2)return c1;
		else c2;
	}
	else if (c_m1.basetracks.size() < 3)return c2;
	else if (c_m2.basetracks.size() < 3)return c1;

	double share_ax = mfile0::chain_ax(ret);
	double share_ay = mfile0::chain_ay(ret);
	double c1_ax = mfile0::chain_ax(c_m1);
	double c1_ay = mfile0::chain_ay(c_m1);
	double c2_ax = mfile0::chain_ax(c_m2);
	double c2_ay = mfile0::chain_ay(c_m2);

	double diff_c1 = pow(share_ax - c1_ax, 2) + pow(share_ay - c1_ay, 2);
	double diff_c2 = pow(share_ax - c2_ax, 2) + pow(share_ay - c2_ay, 2);
	if (diff_c1 < diff_c2)return c1;
	return c2;

}

void chain_extrapolate_x(mfile0::M_Chain&c, double z, double &x,double &angle) {

	double x_chain = 0;
	double z_chain = 0;
	double ax_chain = 0;

	if (c.basetracks.size() == 0) {
		fprintf(stderr, "basetrack not exist\n");
		exit(1);
	}
	else if (c.basetracks.size() == 1) {
		mfile0::M_Base b = c.basetracks.at(0);
		ax_chain = b.ax;
		x_chain = b.x;
		z_chain = b.z;
	}
	else if (c.basetracks.size() == 2) {
		mfile0::M_Base b0 = c.basetracks.at(0);
		mfile0::M_Base b1 = c.basetracks.at(1);
		x_chain = (b0.x + b1.x) / 2;
		z_chain = (b0.z + b1.z) / 2;
		ax_chain = (b0.x - b1.x) / (b0.z - b1.z);
	}
	else {
		std::map<double, mfile0::M_Base > base_list;
		for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
			base_list.insert(std::make_pair(fabs(itr->z - z), *itr));
		}
		std::vector<mfile0::M_Base> use_base;
		for (auto itr = base_list.begin(); itr != base_list.end(); itr++) {
			use_base.push_back(itr->second);
			if (use_base.size() > 6)break;
		}

		double x1, y1, xy, xx, n;
		x1 = 0;
		y1 = 0;
		xy = 0;
		xx = 0;
		n = 0;
		for (auto itr = use_base.begin(); itr != use_base.end(); itr++) {
			x1 += itr->z;
			y1 += itr->x;
			xy += itr->z*itr->x;
			xx += itr->z*itr->z;
			n += 1;

			//printf("%.1lf %.1lf\n", itr->z, itr->x);
		}
		x_chain = 0;
		z_chain = (xx*y1 - x1 * xy) / (n*xx - x1 * x1);
		ax_chain = (n*xy - x1 * y1) / (n*xx - x1 * x1);

		//printf("slope:%.4lf intercept:%.1lf\n", ax_chain, z_chain);
	}
	x = ax_chain * (z - z_chain) + x_chain;
	angle = ax_chain;
}
void chain_extrapolate_y(mfile0::M_Chain&c, double z, double &y,double &angle) {

	double y_chain = 0;
	double z_chain = 0;
	double ay_chain = 0;

	if (c.basetracks.size() == 0) {
		fprintf(stderr, "basetrack not exist\n");
		exit(1);
	}
	else if (c.basetracks.size() == 1) {
		mfile0::M_Base b = c.basetracks.at(0);
		ay_chain = b.ay;
		y_chain = b.y;
		z_chain = b.z;
	}
	else if (c.basetracks.size() == 2) {
		mfile0::M_Base b0 = c.basetracks.at(0);
		mfile0::M_Base b1 = c.basetracks.at(1);
		y_chain = (b0.y + b1.y) / 2;
		z_chain = (b0.z + b1.z) / 2;
		ay_chain = (b0.y - b1.y) / (b0.z - b1.z);
	}
	else {
		std::map<double, mfile0::M_Base > base_list;
		for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
			base_list.insert(std::make_pair(fabs(itr->z - z), *itr));
		}
		std::vector<mfile0::M_Base> use_base;
		for (auto itr = base_list.begin(); itr != base_list.end(); itr++) {
			use_base.push_back(itr->second);
			if (use_base.size() > 6)break;
		}

		double x1, y1, xy, xx, n;
		x1 = 0;
		y1 = 0;
		xy = 0;
		xx = 0;
		n = 0;
		for (auto itr = use_base.begin(); itr != use_base.end(); itr++) {
			x1 += itr->z;
			y1 += itr->y;
			xy += itr->z*itr->y;
			xx += itr->z*itr->z;
			n += 1;

			//printf("%.1lf %.1lf\n", itr->z, itr->y);
		}
		y_chain = 0;
		z_chain = (xx*y1 - x1 * xy) / (n*xx - x1 * x1);
		ay_chain = (n*xy - x1 * y1) / (n*xx - x1 * x1);

		//printf("slope:%.4lf intercept:%.1lf\n", ay_chain, z_chain);
	}
	y = ay_chain * (z - z_chain) + y_chain;
	angle = ay_chain;

}


double Calc_chain_rms(mfile0::M_Chain&c) {
	double ax = mfile0::chain_ax(c);
	double ay = mfile0::chain_ay(c);
	double rms_rad, rms_lat;

	mfile0::angle_diff_dev_theta(c, ax, ay, rms_rad, rms_lat);
	return rms_lat;
}