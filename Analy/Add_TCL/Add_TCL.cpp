#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"PID_correct.lib")
#include <PID_correct.h>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <iostream>
#include <set>
#include <omp.h>
#include <filesystem>
#include <sstream>

class Chain_PID {
public:
	uint64_t chainid, nseg;
	double ax, ay, angle, vph, pb, mip_vph, mip_pixel;
};
std::vector<Chain_PID> read_chain(std::string filename);
void read_CL_file(std::string filename, std::map<uint64_t, double>&chain);
void Add_CL(std::vector<Chain_PID>&chain, std::map<uint64_t, double>&vph, std::map<uint64_t, double>&pixel);

int main(int argc, char**argv) {

	if (argc != 5) {
		fprintf(stderr, "usage:prg file-in-mom-pid file-in-VPHTCL file-in-PixelTCL out-file\n");
		exit(1);
	}
	std::string file_in_mom = argv[1];
	std::string file_in_vph = argv[2];
	std::string file_in_pixel = argv[3];
	std::string file_out = argv[4];
	std::map<uint64_t, double> vph_cl, pixel_cl;
	read_CL_file(file_in_vph, vph_cl);
	read_CL_file(file_in_pixel,pixel_cl);
	std::vector<Chain_PID> chain = read_chain(file_in_mom);
	Add_CL(chain, vph_cl, pixel_cl);

	std::ofstream ofs_txt(file_out + ".txt");
	std::ofstream ofs_bin(file_out + ".bin",std::ios::binary);
	for (int i = 0; i < chain.size();i++) {
		ofs_txt << std::right << std::fixed
			<< std::setw(20) << std::setprecision(0) << chain[i].chainid << " "
			<< std::setw(4) << std::setprecision(0) << chain[i].nseg << " "
			<< std::setw(12) << std::setprecision(4) << chain[i].pb << " "
			<< std::setw(7) << std::setprecision(4) << chain[i].ax << " "
			<< std::setw(7) << std::setprecision(4) << chain[i].ay << " "
			<< std::setw(12) << std::setprecision(2) << chain[i].vph << " "
			<< std::setw(20) << std::setprecision(18) << chain[i].mip_vph << " "
			<< std::setw(20) << std::setprecision(18) << chain[i].mip_pixel << std::endl;
		ofs_bin.write((char*)&chain[i], sizeof(Chain_PID));
	}


}
std::vector<Chain_PID> read_chain(std::string filename) {
	std::ifstream ifs(filename.c_str());
	std::vector<Chain_PID> ret;
	Chain_PID chain;
	double tmp_d[3];
	while (ifs >> chain.chainid >> chain.nseg >> chain.pb >> chain.ax >> chain.ay >> chain.vph>>tmp_d[0]>>tmp_d[1]>>tmp_d[2]) {
		chain.mip_pixel = -1;
		chain.mip_vph = -1;
		chain.angle = sqrt(chain.ax*chain.ax + chain.ay*chain.ay);
		ret.push_back(chain);
	}
	return ret;
}
void read_CL_file(std::string filename, std::map<uint64_t, double>&chain) {
	std::ifstream ifs(filename, std::ios::binary);
	int64_t count = 0;
	uint64_t chainid;
	double CL;

	while (ifs.read((char*)& chainid, sizeof(uint64_t))) {
		ifs.read((char*)& CL, sizeof(double));
		chain.insert(std::make_pair(chainid, CL));
		count++;
	}
	printf("read chain=%llu\n", count);

	return;
}

void Add_CL(std::vector<Chain_PID>&chain, std::map<uint64_t, double>&vph, std::map<uint64_t, double>&pixel) {

	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (vph.count(itr->chainid) != 0) {
			itr->mip_vph = vph.at(itr->chainid);
		}
		if (pixel.count(itr->chainid) != 0) {
			itr->mip_pixel = pixel.at(itr->chainid);
		}
	}
}