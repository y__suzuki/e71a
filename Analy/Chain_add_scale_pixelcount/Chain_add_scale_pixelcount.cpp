#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"PID_correct.lib")
#include <PID_correct.h>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <iostream>
#include <set>
#include <omp.h>
#include <filesystem>
#include <sstream>
class Fit_result {
public:
	double angle_max;
	std::pair<double,double> fit_param;
};
class CL_map {
public:
	double angle_max;
	std::map<int, double> cl_map;
};
std::set<uint64_t> read_chain(std::string filename);
void pickup_chain(std::vector<PID_track> &track, std::multimap<uint64_t, PID_track> &chains, std::set<uint64_t> &chainid);
std::multimap < std::tuple<int, int, int>, Fit_result> read_fit_result(std::string filename);
std::vector<PID_track> pixel_scale(std::vector<PID_track> &chain, std::multimap < std::tuple<int, int, int>, Fit_result>&fit_res);
std::vector<PID_track> reject_track(std::vector<PID_track>&track);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg Pixel-fit-file base_path chain_dat output\n");
		exit(1);
	}


	std::string file_in_fit = argv[1];
	std::string file_in_base_path = argv[2];
	std::string file_in_chain = argv[3];
	std::string file_out = argv[4];

	std::multimap < std::tuple<int, int, int>, Fit_result> fit_res = read_fit_result(file_in_fit);
	std::set<uint64_t> chainid = read_chain(file_in_chain);

	//std::multimap<uint64_t, double> chain_CL;
	//std::multimap <uint64_t, std::pair<short, float>> chain_CL;
	std::vector<PID_track> track_buf;
	std::multimap<uint64_t, PID_track> chains;
	for (int pl = 1; pl <= 133; pl++) {
		printf("read CL file PL%03d\n", pl);
		std::stringstream filename;
		filename << file_in_base_path << "\\base_" << std::setw(3) << std::setfill('0') << pl << ".bin";
		if (!std::filesystem::exists(filename.str())) {
			printf("%s no track\n", filename.str().c_str());
			continue;
		}
		PID_track::read_pid_track(filename.str(), track_buf);
		track_buf = reject_track(track_buf);
		pickup_chain(track_buf, chains, chainid);
		track_buf.clear();
	}

	std::ofstream ofs(file_out + ".bin", std::ios::binary);
	std::ofstream ofs_txt(file_out + ".txt");
	for (auto itr = chains.begin(); itr != chains.end(); itr++) {
		int count = chains.count(itr->first);
		std::vector<PID_track> chain;
		auto range = chains.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			chain.push_back(res->second);
		}
		chain = pixel_scale(chain, fit_res);
		for (auto itr2 = chain.begin(); itr2 != chain.end(); itr2++) {
			ofs_txt << std::right << std::fixed
				<< std::setw(20) << std::setprecision(0) << itr2->chainid << " "
				<< std::setw(12) << std::setprecision(0) << itr2->rawid << " "
				<< std::setw(4) << std::setprecision(2) << itr2->pid << " "
				<< std::setw(8) << std::setprecision(1) << itr2->pb << " "
				<< std::setw(7) << std::setprecision(4) << itr2->angle << " "
				<< std::setw(4) << std::setprecision(0) << itr2->pos << " "
				<< std::setw(3) << std::setprecision(0) << itr2->trackingid << " "
				<< std::setw(10) << std::setprecision(4) << itr2->vph << " "
				<< std::setw(6) << std::setprecision(1) << itr2->pixelnum << " "
				<< std::setw(3) << std::setprecision(0) << itr2->sensorid << std::endl;
			ofs.write((char *)&(*itr), sizeof(PID_track));
		}
		itr = std::next(itr, count - 1);
	}
}
std::set<uint64_t> read_chain(std::string filename) {
	std::ifstream ifs(filename);
	std::set<uint64_t> ret;
	uint64_t cid;
	while (ifs >> cid) {
		ret.insert(cid);
	}
	return ret;
}
std::vector<PID_track> reject_track(std::vector<PID_track>&track) {
	std::vector<PID_track> ret;
	ret.reserve(track.size());
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		//vphにCLが入っている
		//CL<0は評価失敗(-1が入っている)
		if (itr->vph < -0.1)continue;
		ret.push_back(*itr);
	}
	return ret;
}
void pickup_chain(std::vector<PID_track> &track, std::multimap<uint64_t, PID_track> &chains, std::set<uint64_t> &chainid) {
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		if (chainid.count(itr->chainid) == 0)continue;
		chains.insert(std::make_pair(itr->chainid, *itr));
	}
	return;
}

int sensor_id(int sensor) {
	if ((24 <= sensor && sensor < 36) || sensor == 52)return 1;
	return 0;
}
int angle_id(std::set<double >angle_range, double angle) {
	int i = 0;
	for (auto itr = angle_range.begin(); itr != angle_range.end(); itr++) {
		if (angle < *itr)return i;
		i++;
	}
	return i - 1;
}
int tracking_id(int track) {
	if (track == 0 || track == 1)return 0;
	else if (track == 2)return 1;
	return 2;
}

std::vector<PID_track> pixel_scale(std::vector<PID_track> &chain, std::multimap < std::tuple<int, int, int>, Fit_result>&fit_res) {

	std::vector<PID_track> ret;

	std::multimap<int, PID_track> pos_divide;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		pos_divide.insert(std::make_pair(itr->pos, *itr));
	}

	for (auto itr = pos_divide.begin(); itr != pos_divide.end(); itr++) {
		int count = pos_divide.count(itr->first);
		ret.push_back(pos_divide.find(itr->first)->second);
		itr = std::next(itr, count - 1);
	}
	std::tuple<int, int, int> id;
	Fit_result param;
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		std::get<0>(id) = itr->pos;
		std::get<1>(id) = tracking_id(itr->trackingid);
		std::get<2>(id) = sensor_id(itr->sensorid);
		if (fit_res.count(id) == 0) {
			//parameterがなかったら負の大きい値
			itr->vph = -1000;
			continue;
		}
		auto range = fit_res.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			if (itr->angle < itr2->second.angle_max) {
				param = itr2->second;
				break;
			}
			if (std::next(itr2, 1) == range.second) {
				param = itr2->second;
			}
		}
		//meanから何sigma離れているか。
		itr->vph = (itr->pixelnum - param.fit_param.first) / param.fit_param.second;
	}
	return ret;
}
std::multimap < std::tuple<int, int, int>, Fit_result> read_fit_result(std::string filename) {
	int pos, trackingid, sensorid;
	double angle_min, angle_max, mean, sigma;
	std::multimap < std::tuple<int, int, int>, Fit_result> ret;
	Fit_result res;
	std::ifstream ifs(filename.c_str());
	std::string str;
	int count = 0;
	while (std::getline(ifs, str))
	{
		sscanf(str.c_str(), "%d %d %d %lf %lf %lf %lf", &pos, &trackingid, &sensorid, &angle_min, &angle_max, &mean, &sigma);
		res.angle_max = angle_max;
		res.fit_param.first = mean;
		res.fit_param.second = sigma;
		ret.insert(std::make_pair(std::make_tuple(pos, trackingid, sensorid), res));
		count++;
		if (count % 10000 == 0) {
			fprintf(stderr, "\r Fill ---> %8d", count);
		}
	}
	fprintf(stderr, "\r Fill ---> %8d   finished\n", count);
	return ret;
}