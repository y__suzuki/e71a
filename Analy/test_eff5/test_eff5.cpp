#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
#include <set>
#include <picojson.h>

class micro_sensor_inf {
public:
	int area, sensor, view;
	int px, py;
};
class EachImager_Param {
public:
	//CamareaID * 5 + SensorID = ImagerID
	//Width,Height 縦横のpixel数
	double Aff_coef[6], Aff_coef_offset[6], DZ;
	int CameraID, GridX, GridY, Height, ImagerID, SensorID, Width;
	std::string LastReportFilePath;
};
class EachShot_Param {
public:
	int View, Imager, StartAnalysisPicNo, GridX, GridY;
	double Z_begin, X_center, Y_center;
	std::string TrackFilePath;
	//GridX,Y intでダイジョブ?
};
class EachView_Param {
public:
	int  LayerID, NPicThickOfLayer;
	double Stage_x, Stage_y, ThickOfLayer, Z_begin, Z_end;
};

vxx::base_track_t convert(netscan::base_track_t&base);
std::vector<vxx::base_track_t> select_base(std::vector<netscan::linklet_t>&link);
void micro_inf_pick_up(std::vector<vxx::base_track_t>&base
	, std::vector<vxx::micro_track_subset_t> thick0_v[6]
	, std::vector<vxx::micro_track_subset_t> thick1_v[6]
	, std::vector<vxx::micro_track_subset_t> thin0_v[6]
	, std::vector<vxx::micro_track_subset_t> thin1_v[6]);
std::vector<vxx::micro_track_t> get_micro_inf(std::string ECC_path, int pl, int area, std::vector<vxx::micro_track_subset_t>&m);
std::map<int, EachImager_Param> read_EachImager(std::string filename);
std::vector<EachShot_Param> read_EachShot(std::string filename);
std::map<int, EachView_Param> read_EachView(std::string filename);
std::vector<micro_sensor_inf> get_sensor_inf(std::string ECC_path, int pl, int area, std::vector<vxx::micro_track_t>&m);
void microtrack_transformation(EachImager_Param imager, EachView_Param view, vxx::micro_track_t &m, micro_sensor_inf &m_inf);
void output(std::ofstream &ofs, std::vector<micro_sensor_inf>&m);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-base pl in-link ECC_path output-filename\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_link = argv[3];
	std::string ECC_path = argv[4];
	std::string file_out = argv[5];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base, pl, 0);
	printf("prediction %d\n", base.size());
	std::vector<netscan::linklet_t>link;
	netscan::read_linklet_bin(file_in_link, link);
	std::vector<vxx::base_track_t> base_detect = select_base(link);
	std::vector<vxx::micro_track_subset_t> thick0_v[6];
	std::vector<vxx::micro_track_subset_t> thick1_v[6];
	std::vector<vxx::micro_track_subset_t> thin0_v[6];
	std::vector<vxx::micro_track_subset_t> thin1_v[6];
	std::vector<vxx::micro_track_t> thick0[6];
	std::vector<vxx::micro_track_t> thick1[6];
	std::vector<vxx::micro_track_t> thin0[6];
	std::vector<vxx::micro_track_t> thin1[6];
	micro_inf_pick_up(base_detect, thick0_v, thick1_v, thin0_v, thin1_v);
	for (int area = 1; area <= 6; area++) {
		printf("get micro inf Area%d\n", area);
		thick0[area - 1] = get_micro_inf(ECC_path, pl + 1, area, thick0_v[area - 1]);
		thick1[area - 1] = get_micro_inf(ECC_path, pl + 1, area, thick1_v[area - 1]);
		thin0[area - 1] = get_micro_inf(ECC_path, pl + 1, area, thin0_v[area - 1]);
		thin1[area - 1] = get_micro_inf(ECC_path, pl + 1, area, thin1_v[area - 1]);
	}
	std::vector<micro_sensor_inf> thick0_pix[6];
	std::vector<micro_sensor_inf> thick1_pix[6];
	std::vector<micro_sensor_inf> thin0_pix[6];
	std::vector<micro_sensor_inf> thin1_pix[6];
	for (int area = 1; area <= 6; area++) {
		printf("get picel inf Area%d\n", area);
		thick0_pix[area - 1] = get_sensor_inf(ECC_path, pl + 1, area, thick0[area - 1]);
		thick1_pix[area - 1] = get_sensor_inf(ECC_path, pl + 1, area, thick1[area - 1]);
		thin0_pix[area - 1] = get_sensor_inf(ECC_path, pl + 1, area, thin0[area - 1]);
		thin1_pix[area - 1] = get_sensor_inf(ECC_path, pl + 1, area, thin1[area - 1]);
	}
	std::ofstream ofs(file_out);
	for (int area = 1; area <= 6; area++) {
		printf("output Area%d\n", area);
		output(ofs, thick0_pix[area - 1]);
		output(ofs, thick1_pix[area - 1]);
		output(ofs, thin0_pix[area - 1]);
		output(ofs, thin1_pix[area - 1]);
	}

	
}
std::vector<vxx::base_track_t> select_base(std::vector<netscan::linklet_t>&link) {
	std::vector<vxx::base_track_t> ret;
	std::map<int, vxx::base_track_t> base_map;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		base_map.insert(std::make_pair(itr->b[1].rawid, convert(itr->b[1])));
	}
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		ret.push_back(itr->second);
	}
	return ret;

}
vxx::base_track_t convert(netscan::base_track_t&base) {
	vxx::base_track_t ret;
	ret.ax = base.ax;
	ret.ay = base.ay;
	ret.x = base.x;
	ret.y = base.y;
	ret.isg = base.isg;
	ret.pl= base.pl;
	ret.rawid = base.rawid;
	ret.z = base.z;
	ret.zone = base.zone;
	ret.dmy = base.dmy;
	for (int i = 0; i < 2; i++) {
		ret.m[i].ax = base.m[i].ax;
		ret.m[i].ay = base.m[i].ay;
		ret.m[i].col = base.m[i].col;
		ret.m[i].isg= base.m[i].isg;
		ret.m[i].ph = base.m[i].ph;
		ret.m[i].pos = base.m[i].pos;
		ret.m[i].rawid = base.m[i].rawid;
		ret.m[i].row = base.m[i].row;
		ret.m[i].z = base.m[i].z;
		ret.m[i].zone = base.m[i].zone;
	}
	return ret;
}
void micro_inf_pick_up(std::vector<vxx::base_track_t>&base
	,std::vector<vxx::micro_track_subset_t> thick0_v[6]
	, std::vector<vxx::micro_track_subset_t> thick1_v[6]
	, std::vector<vxx::micro_track_subset_t> thin0_v[6]
	, std::vector<vxx::micro_track_subset_t> thin1_v[6]) {
	std::map<int,vxx::micro_track_subset_t> thick0[6],thick1[6],thin0[6],thin1[6];
	int quotient,remain;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		quotient = (itr->m[0].zone - 1) / 6;
		remain = (itr->m[0].zone - 1) % 6 + 1;
		if (quotient == 0 || quotient == 1) {
			thick0[remain - 1].insert(std::make_pair(itr->m[0].rawid, itr->m[0]));
		}
		else if (quotient == 2 || quotient == 3) {
			thick1[remain - 1].insert(std::make_pair(itr->m[0].rawid, itr->m[0]));
		}
		else if (quotient == 4 || quotient == 5) {
			thin0[remain - 1].insert(std::make_pair(itr->m[0].rawid, itr->m[0]));
		}
		else if (quotient == 6 || quotient == 7) {
			thin1[remain - 1].insert(std::make_pair(itr->m[0].rawid, itr->m[0]));
		}
	}
	for (int i = 0; i < 6; i++) {
		for (auto itr = thick0[i].begin(); itr != thick0[i].end(); itr++) {
			thick0_v[i].push_back(itr->second);
		}
		for (auto itr = thick1[i].begin(); itr != thick1[i].end(); itr++) {
			thick1_v[i].push_back(itr->second);
		}
		for (auto itr = thin0[i].begin(); itr != thin0[i].end(); itr++) {
			thin0_v[i].push_back(itr->second);
		}
		for (auto itr = thin1[i].begin(); itr != thin1[i].end(); itr++) {
			thin1_v[i].push_back(itr->second);
		}
	}
}

std::vector<vxx::micro_track_t> get_micro_inf(std::string ECC_path, int pl, int area, std::vector<vxx::micro_track_subset_t>&m) {
	std::stringstream file_in_base;
	file_in_base << ECC_path << "\\Area"<< area 
		<< "\\PL" << std::setw(3) << std::setfill('0') << pl 
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.vxx";

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), pl, 0);
	std::map<std::pair<int, int>, vxx::base_track_t> base_micro;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_micro.insert(std::make_pair(std::make_pair(itr->m[0].zone, itr->m[0].rawid), *itr));
	}
	std::vector<vxx::micro_track_t> ret;
	for(auto itr=m.begin();itr!=m.end();itr++){
		if (base_micro.count(std::make_pair(itr->zone, itr->rawid)) == 0) {
			printf("zone %d rawid=%d not found\n", itr->zone, itr->rawid);
			continue;
		}
		vxx::micro_track_t micro;
		auto b= base_micro.at(std::make_pair(itr->zone, itr->rawid));
		micro.ax = b.m[0].ax;
		micro.ay = b.m[0].ay;
		micro.x = b.x;
		micro.y = b.y;
		micro.col = b.m[0].col;
		micro.row = b.m[0].row;
		micro.isg = b.m[0].isg;
		micro.ph = b.m[0].ph;
		micro.pos = b.m[0].pos;
		micro.px = 0;
		micro.py = 0;
		micro.rawid= b.m[0].rawid;
		micro.z = b.m[0].z;
		micro.z1 = b.m[0].z;
		micro.z2 = b.m[0].z+60;
		micro.zone = b.m[0].zone;
		ret.push_back(micro);
	}
	return ret;
}

std::vector<micro_sensor_inf> get_sensor_inf(std::string ECC_path, int pl, int area, std::vector<vxx::micro_track_t>&m) {
	std::stringstream file_path_beta;
	file_path_beta << ECC_path << "\\Area" << area
		<< "\\PL" << std::setw(3) << std::setfill('0') << pl;

	std::string file_in_Beta_EachImagerParam = file_path_beta.str() + "\\Beta_EachImagerParam.json";
	std::string file_in_Beta_EachShotParam = file_path_beta.str() + "\\Beta_EachShotParam.json";
	std::string file_in_Beta_EachViewParam = file_path_beta.str() + "\\Beta_EachViewParam.json";

	std::map<int, EachImager_Param> imager_map = read_EachImager(file_in_Beta_EachImagerParam);
	std::vector<EachShot_Param> shot_vec = read_EachShot(file_in_Beta_EachShotParam);
	std::map<int, EachView_Param> view_map = read_EachView(file_in_Beta_EachViewParam);


	uint32_t ShotID;
	int ViewID = 0, ImagerID = 0;
	const int NumberOfImager = 72;
	std::vector<micro_sensor_inf> ret;
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		ShotID = ((uint32_t)(uint16_t)itr->row << 16) | ((uint32_t)(uint16_t)itr->col);
		ViewID = ShotID / NumberOfImager;
		ImagerID = ShotID % NumberOfImager;
		micro_sensor_inf m_inf;
		m_inf.area = (itr->zone - 1) % 6 + 1;
		m_inf.sensor = ImagerID;
		m_inf.view = ViewID;

		if (imager_map.count(ImagerID) == 0) {
			printf("imager %d param not found\n", ImagerID);
		}
		if (view_map.count(ViewID) == 0) {
			printf("view %d param not found\n", ViewID);
		}
		microtrack_transformation(imager_map.at(ImagerID), view_map.at(ViewID), *itr, m_inf);
		ret.push_back(m_inf);
	}

	return ret;
}


//Implementation
std::map<int, EachImager_Param> read_EachImager(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	std::map<int, EachImager_Param> ret;
	//mapのkey=imagerID
	int ImagerID = 0;
	picojson::array &all = v.get<picojson::array>();
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachImager_Param param;
		picojson::array &Aff_coef = obj["Aff_coef"].get<picojson::array>();
		picojson::array &Aff_coef_offset = obj["Aff_coef_offset"].get<picojson::array>();
		int i = 0;
		for (int i = 0; i < 6; i++) {
			param.Aff_coef[i] = Aff_coef[i].get<double>();
			param.Aff_coef_offset[i] = Aff_coef_offset[i].get<double>();
		}
		param.DZ = obj["DZ"].get<double>();
		param.CameraID = (int)obj["CameraID"].get<double>();
		param.GridX = (int)obj["GridX"].get<double>();
		param.GridY = (int)obj["GridY"].get<double>();
		param.Height = (int)obj["Height"].get<double>();
		param.ImagerID = (int)obj["ImagerID"].get<double>();
		param.SensorID = (int)obj["SensorID"].get<double>();
		param.Width = (int)obj["Width"].get<double>();
		param.LastReportFilePath = obj["LastReportFilePath"].get<std::string>();
		ret.insert(std::make_pair(ImagerID, param));
		ImagerID++;

		//printf("imagerID %d factor %.10lf\n", param.ImagerID, param.Aff_coef[0] * param.Aff_coef[3] - param.Aff_coef[1] * param.Aff_coef[2]);
	}
	printf("number of imager = %d\n", ImagerID);
	return ret;
}
std::vector<EachShot_Param> read_EachShot(std::string filename) {
	//JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}

	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	//位置-->shot paramに行けるようにしたい-->全beta.json読み込んだ後でやる
	std::vector<EachShot_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachShot_Param param;
		param.Z_begin = obj["Z_begin"].get<double>();
		param.View = (int)obj["View"].get<double>();
		param.Imager = (int)obj["Imager"].get<double>();
		param.StartAnalysisPicNo = (int)obj["StartAnalysisPicNo"].get<double>();
		param.GridX = (int)obj["GridX"].get<double>();
		param.GridY = (int)obj["GridY"].get<double>();
		param.TrackFilePath = obj["TrackFilePath"].get<std::string>();
		param.X_center = -1;
		param.Y_center = -1;
		ret.push_back(param);
	}
	printf("number of shot = %zd\n", ret.size());

	return ret;
}
std::map<int, EachView_Param> read_EachView(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}
	std::map<int, EachView_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	int ViewID = 0;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachView_Param param;
		param.Stage_x = obj["Stage_x"].get<double>();
		param.Stage_y = obj["Stage_y"].get<double>();
		param.ThickOfLayer = obj["ThickOfLayer"].get<double>();
		param.Z_begin = obj["Z_begin"].get<double>();
		param.Z_end = obj["Z_end"].get<double>();
		param.LayerID = (int)obj["LayerID"].get<double>();
		param.NPicThickOfLayer = (int)obj["NPicThickOfLayer"].get<double>();
		ret.insert(std::make_pair(ViewID, param));
		ViewID++;
	}
	printf("number of view = %d\n", ViewID);
	return ret;
}
void microtrack_transformation(EachImager_Param imager, EachView_Param view, vxx::micro_track_t &m, micro_sensor_inf &m_inf) {
	//stage-->pixel 座標へ
	//角度は回転のみ
	double px, py, pax, pay, x_tmp, y_tmp;
	//1層の厚み*層数*角度/(pixel length)+dz補正
	x_tmp = m.x;
	y_tmp = m.y;

	//um --> mm & shift成分の計算
	x_tmp = x_tmp / 1000 - imager.Aff_coef[4] - imager.Aff_coef_offset[4] - view.Stage_x;
	y_tmp = y_tmp / 1000 - imager.Aff_coef[5] - imager.Aff_coef_offset[5] - view.Stage_y;
	double factor = 1.0 / (imager.Aff_coef[0] * imager.Aff_coef[3] - imager.Aff_coef[1] * imager.Aff_coef[2]);
	px = factor * (imager.Aff_coef[3] * x_tmp - imager.Aff_coef[1] * y_tmp);
	py = factor * (imager.Aff_coef[0] * y_tmp - imager.Aff_coef[2] * x_tmp);
	px = px + 2048 / 2;
	py = py + 1088 / 2;
	//角度の変換 回転のみ

	//m.ax-->1umで何umシフトするか。
		//15layer毎のシフトピクセル量
	//60=15layerの厚み

	x_tmp = m.ax * 60 / 1000;
	y_tmp = m.ay * 60 / 1000;
	//fabsとっていい?-->signの適用
	pax = factor * (imager.Aff_coef[3] * x_tmp - imager.Aff_coef[1] * y_tmp);
	pay = factor * (imager.Aff_coef[0] * y_tmp - imager.Aff_coef[2] * x_tmp);

	m_inf.px = px;
	m_inf.py = py;
}

void output(std::ofstream &ofs, std::vector<micro_sensor_inf>&m) {
	int count = 0;
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r write inf %d/%d(%4.1lf%%)", count, m.size(), count*100. / m.size());
		}
		count++;

		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << itr->area << " "
			<< std::setw(4) << std::setprecision(0) << itr->sensor << " "
			<< std::setw(4) << std::setprecision(0) << itr->view << " "
			<< std::setw(6) << std::setprecision(0) << itr->px << " "
			<< std::setw(6) << std::setprecision(0) << itr->py << std::endl;
	}
	printf("\r write inf %d/%d(%4.1lf%%)\n", count, m.size(), count*100. / m.size());

}