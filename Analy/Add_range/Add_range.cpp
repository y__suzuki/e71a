﻿#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>

class Chain_mom {
public:
	uint64_t chainid;
	int nseg;
	double mom, ax, ay, vph_ave,range;
};
std::vector<Chain_mom> read_chain(std::string filename);
void add_range(Chain_mom &chain, std::vector<mfile1::MFileBase1>&base);
void material_ratio(double &iron, double &film, double &water);
void material_count(int pl, double &iron, double &film, double &water);
void output_mom(std::string filename, std::vector<Chain_mom>&chain);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-chain in-mfile outfile\n");
		exit(1);
	}
	std::string file_in_chain = argv[1];
	std::string file_in_mfile = argv[2];
	std::string file_out = argv[3];
	std::vector < Chain_mom >chain= read_chain(file_in_chain);
	mfile1::MFile m;
	mfile1::read_mfile(file_in_mfile, m);

	std::map<uint64_t, std::vector<mfile1::MFileBase1>*> chain_id_map;
	for (int i = 0; i < m.chains.size(); i++) {
		chain_id_map.insert(std::make_pair(m.chains[i].chain_id, &(m.all_basetracks[i])));
	}
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		add_range(*itr, *chain_id_map.at(itr->chainid));	
	}
	output_mom(file_out, chain);

}
std::vector<Chain_mom> read_chain(std::string filename) {
	std::ifstream ifs(filename.c_str());
	std::vector<Chain_mom> ret;
	Chain_mom chain;
	while (ifs >> chain.chainid >> chain.nseg >> chain.mom >> chain.ax >> chain.ay >> chain.vph_ave) {
		chain.range = -1;
		ret.push_back(chain);
	}
	return ret;
}
void add_range(Chain_mom &chain, std::vector<mfile1::MFileBase1>&base) {
	mfile1::MFileBase1 base_up = *base.rbegin();
	mfile1::MFileBase1 base_down = *base.begin();
	double film = 0, iron = 0, water = 0;
	for (int pl = base_down.pos / 10; pl < base_up.pos / 10; pl++) {
		material_count(pl, iron, film, water);
	}
	double dist = sqrt(pow(base_up.x - base_down.x, 2) + pow(base_up.y - base_down.y, 2) + pow(base_up.y - base_down.y, 2));
	material_ratio(iron, film, water);
	double range = dist * iron + dist * water * 1 / 7.874 + dist * film*2.076 / 7.874;
	chain.range = range;
}
void material_count(int pl,double &iron,double &film,double &water) {
	film++;
	if (pl >= 16 && pl % 2 == 1) {
		water++;;
	}
	if (pl >= 16 && pl % 2 == 0) {
		iron++;
	}
	if (4<=pl&&pl<15) {
		iron++;
	}
}
void material_ratio(double &iron, double &film, double &water) {
	//iron 500um 密度 7.874 g/cm³
	//water 2.3,, 密度1.0 g/cm³
	//emulsion gel 3.6 g/cm³
	//polystyrene 1.060g/cm³
	//em:pol=2:3-->2.076

	//all-->g/cm^2
	double all = 0.05*7.874*iron + 0.23*1.0*water + (0.021*1.06 + 0.007 * 2 * 3.6)*film;
	iron = 0.05*7.874*iron / all;
	water = 0.23*1.0*water / all;
	film = (0.021*1.06 + 0.007 * 2 * 3.6)*film/all;
}


void output_mom(std::string filename, std::vector<Chain_mom>&chain) {
	std::ofstream ofs(filename);
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		//if (mom > 0) {
		ofs << std::right << std::fixed
			<< std::setw(20) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(4) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(12) << std::setprecision(4) << itr->mom << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(12) << std::setprecision(2) << itr->vph_ave << " "
			<< std::setw(12) << std::setprecision(1) << itr->range << std::endl;
	}
	return;
}