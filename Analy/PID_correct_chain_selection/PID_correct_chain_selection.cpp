//#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <set>
#include<vector>

std::vector<uint64_t> nseg_selection(mfile1::MFile &m, std::vector<uint64_t> &all, int threshold);
mfile1::MFile mfile_selection(mfile1::MFile &m, std::vector<uint64_t> &all);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage mfile mfile-out(bin) mfile-out(txt)\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile_bin = argv[2];
	std::string file_out_mfile_txt = argv[3];

	mfile1::MFile m;
	mfile1::read_mfile(file_in_mfile, m);
	std::vector<uint64_t> all, sel;
	all.reserve(m.chains.size());
	for (uint64_t i = 0; i < m.chains.size(); i++) {
		all.push_back(i);
	}
	all = nseg_selection(m, all, 10);
	m = mfile_selection(m, all);

	mfile1::write_mfile(file_out_mfile_bin, m);

	mfile0::Mfile m_out;
	mfile1::converter(m, m_out);

	mfile0::write_mfile(file_out_mfile_txt, m_out);

}
std::vector<uint64_t> nseg_selection(mfile1::MFile &m, std::vector<uint64_t> &all, int threshold) {
	std::vector<uint64_t> ret;
	uint64_t all_size = all.size();
	ret.reserve(all_size);

	for (auto c : all) {
		if (m.chains[c].nseg >= threshold) {
			ret.push_back(c);
		}
	}
	printf("nseg >= %d: %lld --> %lld(%4.1lf%%)", threshold, all.size(), ret.size(), ret.size()*100. / all.size());
	return ret;
}
mfile1::MFile mfile_selection(mfile1::MFile &m, std::vector<uint64_t> &all) {
	mfile1::MFile ret;
	ret.header = m.header;
	ret.info_header = m.info_header;
	for (auto c : all) {
		ret.chains.push_back(m.chains[c]);
		std::vector<mfile1::MFileBase1> base;
		for (auto b : m.all_basetracks[c]) {
			base.push_back(b);
		}
		ret.all_basetracks.push_back(base);
	}
	uint64_t N_base = 0;
	uint64_t N_chain = 0;
	for (auto c : ret.chains) {
		N_chain++;
		N_base += c.nseg;
	}
	ret.info_header.Nbasetrack = N_base;
	ret.info_header.Nchain = N_chain;

	return ret;
}

