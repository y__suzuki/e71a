#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <omp.h>
#include <set>

void rename_mfile(mfile0::Mfile &m);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr,"usage:prg in-file out-file\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];
	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	rename_mfile(m);
	mfile0::write_mfile(file_out_mfile, m);





}
void rename_mfile(mfile0::Mfile &m) {
	int gid;

	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		gid = itr->basetracks.begin()->group_id / 100000;
		itr->chain_id = itr->basetracks.begin()->group_id;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			itr2->group_id = gid;
		}
	}

	std::map<int, int> gid_count;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		auto res = gid_count.insert(std::make_pair(itr->basetracks.begin()->group_id , 1));
		if (!res.second)res.first->second++;
	}

	std::map<int, int> multi_count;

	for (auto itr = gid_count.begin(); itr != gid_count.end(); itr++) {
		printf("gid %d chain:%d\n", itr->first, itr->second);
		auto res = multi_count.insert(std::make_pair(itr->second, 1));
		if (!res.second)res.first->second++;
	}	
	for (auto itr = multi_count.begin(); itr != multi_count.end(); itr++) {
		printf("multi %d --> %d\n", itr->first, itr->second);

	}



}