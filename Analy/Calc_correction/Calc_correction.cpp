#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Mom_chain> nseg_cut(std::vector<Momentum_recon::Mom_chain>&momch, int nseg);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg file-in-momch file-out\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_out = argv[2];
	//nseg>=nseg cutを読みこみ
	int nseg_cut = 20;
	//std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_bin(file_in_momch, nseg_cut);

	Momentum_recon::Write_mom_chain_bin_root(file_out, momch);
}
