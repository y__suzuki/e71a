#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class Sharing_file2 :public Sharing_file::Sharing_file {
public:
	double tracker_position_x, tracker_position_y;
	double tracker_angle_x, tracker_angle_y;
	double bm_angle_x, bm_angle_y;
	double tss_angle_x, tss_angle_y;
};

std::vector< Sharing_file2 > read_sf2(std::string filename);
void Write_sharing_file2_txt(std::string filename, std::vector<Sharing_file2>& sharing_file_v);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file_in_sf file_in_sf_add file_out_sf_add\n");
		exit(1);
	}
	std::string file_in_sf = argv[1];
	std::string file_in_sf_add = argv[2];
	std::string file_out_sf = argv[3];

	std::vector< Sharing_file2 > sf_add = read_sf2(file_in_sf_add);
	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_extension(file_in_sf);

	std::map<int, Sharing_file::Sharing_file> sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(itr->eventid, *itr));
	}

	std::vector< Sharing_file2 > save_sf;
	for (auto itr = sf_add.begin(); itr != sf_add.end(); itr++) {
		if (sf_map.count(itr->eventid) == 0)continue;
		itr->ecc_track_type = sf_map.at(itr->eventid).ecc_track_type;
		save_sf.push_back(*itr);
	}

	Write_sharing_file2_txt(file_out_sf, save_sf);


}

std::vector< Sharing_file2 > read_sf2(std::string filename) {
	std::vector<Sharing_file2> ret;
	std::ifstream ifs(filename);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	Sharing_file2 t;
	//while (ifs.read((char*)& t, sizeof(Sharing_file))) {
	while (ifs >> t.pl >> t.ecc_id >> t.oss_id >> t.fixedwall_id >> t.trackerwall_id >> t.spotid >> t.zone[0] >> t.rawid[0] >> t.zone[1] >> t.rawid[1] >> t.unix_time >> t.tracker_track_id >> t.babymind_bunch >> t.entry_in_daily_file >> t.babymind_nplane >> t.charge >> t.babymind_momentum >> t.chi2_shifter[0] >> t.chi2_shifter[1] >> t.chi2_shifter[2] >> t.chi2_shifter[3] >> t.eventid >> t.track_type >> t.ecc_track_type
		>>t.tracker_position_x>>t.tracker_position_y>>t.tracker_angle_x>>t.tracker_angle_y>>t.bm_angle_x>>t.bm_angle_y>>t.tss_angle_x>>t.tss_angle_y) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(t);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no data!\n", filename.c_str());
		exit(1);
	}
	return ret;



}

void Write_sharing_file2_txt(std::string filename, std::vector<Sharing_file2>& sharing_file_v)
{
	std::ofstream ofs(filename);
	if (!ofs) {
		//file open s
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (sharing_file_v.size() == 0) {
		fprintf(stderr, "target data ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int64_t count = 0;
		int64_t max = sharing_file_v.size();

		for (int i = 0; i < max; i++) {
			if (count % 10000 == 0) {
				std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / sharing_file_v.size() << "%%";
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(3) << std::setprecision(0) << sharing_file_v[i].pl << " "
				<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].ecc_id << " "
				<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].oss_id << " "
				<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].fixedwall_id << " "
				<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].trackerwall_id << " "
				<< std::setw(4) << std::setprecision(0) << sharing_file_v[i].spotid << " "
				<< std::setw(2) << std::setprecision(0) << sharing_file_v[i].zone[0] << " "
				<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].rawid[0] << " "
				<< std::setw(2) << std::setprecision(0) << sharing_file_v[i].zone[1] << " "
				<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].rawid[1] << " "
				<< std::setw(10) << std::setprecision(0) << sharing_file_v[i].unix_time << " "
				<< std::setw(3) << std::setprecision(0) << sharing_file_v[i].tracker_track_id << " "
				<< std::setw(3) << std::setprecision(0) << sharing_file_v[i].babymind_bunch << " "
				<< std::setw(6) << std::setprecision(0) << sharing_file_v[i].entry_in_daily_file << " "
				<< std::setw(3) << std::setprecision(0) << sharing_file_v[i].babymind_nplane << " "
				<< std::setw(3) << std::setprecision(0) << sharing_file_v[i].charge << " "
				<< std::setw(7) << std::setprecision(1) << sharing_file_v[i].babymind_momentum << " "
				<< std::setw(7) << std::setprecision(3) << sharing_file_v[i].chi2_shifter[0] << " "
				<< std::setw(7) << std::setprecision(3) << sharing_file_v[i].chi2_shifter[1] << " "
				<< std::setw(7) << std::setprecision(3) << sharing_file_v[i].chi2_shifter[2] << " "
				<< std::setw(7) << std::setprecision(3) << sharing_file_v[i].chi2_shifter[3] << " "
				<< std::setw(7) << std::setprecision(0) << sharing_file_v[i].eventid << " "
				<< std::setw(2) << std::setprecision(0) << sharing_file_v[i].track_type << " "
				<< std::setw(2) << std::setprecision(0) << sharing_file_v[i].ecc_track_type<<" "
				<< std::setw(8) << std::setprecision(3) << sharing_file_v[i].tracker_position_x << " "
				<< std::setw(8) << std::setprecision(3) << sharing_file_v[i].tracker_position_y << " "
				<< std::setw(6) << std::setprecision(3) << sharing_file_v[i].tracker_angle_x << " "
				<< std::setw(6) << std::setprecision(3) << sharing_file_v[i].tracker_angle_y << " "
				<< std::setw(6) << std::setprecision(3) << sharing_file_v[i].bm_angle_x << " "
				<< std::setw(6) << std::setprecision(3) << sharing_file_v[i].bm_angle_y << " "
				<< std::setw(6) << std::setprecision(3) << sharing_file_v[i].tss_angle_x << " "
				<< std::setw(6) << std::setprecision(3) << sharing_file_v[i].tss_angle_y 
				<< std::endl;

		}
		std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / sharing_file_v.size() << "%%" << std::endl;
	}
}
