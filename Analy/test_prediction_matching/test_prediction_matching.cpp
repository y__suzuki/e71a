#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include "VxxReader.h"
#include <FILE_structure.hpp>
struct Prediction {
	int PL, flg, rawid;
	double ax, ay, x, y;
};
std::vector<Prediction> read_prediction(std::string filename, int pl);
	void Basetrack_trans(std::vector<vxx::base_track_t> &base, std::vector<corrmap0::Corrmap> corr, int pl);
	void search_prediction(std::vector<Prediction> &pred, std::vector<vxx::base_track_t> &base);
		void output_prediction(std::string filename, std::vector<Prediction> pred);

int main(int argc, char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg input-pred input-bvxx pl corrmap-abs output-pred\n");
		exit(1);
	}
	std::string file_in_pred = argv[1];
	std::string file_in_bvxx = argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_in_corr = argv[4];
	std::string file_out_pred = argv[5];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);
	std::vector<Prediction> pred = read_prediction(file_in_pred, pl);
	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corr, corr);

	Basetrack_trans(base, corr, pl);

	search_prediction(pred, base);
	output_prediction( file_out_pred, pred);
}
std::vector<Prediction> read_prediction(std::string filename, int pl) {
	std::ifstream ifs(filename);
	std::vector<Prediction> ret;
	Prediction pred_buf;
	int count = 0;
	//1  0.7841  0.5916  39304.3  39042.1  1
	while (ifs >> pred_buf.PL >> pred_buf.ax >> pred_buf.ay >> pred_buf.x >> pred_buf.y >> pred_buf.flg) {
		count++;
		if (count % 10000 == 0) {
			fprintf(stderr, "\r read prediction ...%d", count);
		}
		if (pred_buf.PL != pl)continue;
		ret.push_back(pred_buf);


	}
	fprintf(stderr, "\r read prediction ...%d\n", count);

	fprintf(stderr, "input prediction(PL%03d) %d\n", pl, ret.size());
	return ret;
}
void Basetrack_trans(std::vector<vxx::base_track_t> &base, std::vector<corrmap0::Corrmap> corr, int pl) {
	corrmap0::Corrmap param;
	bool flg = false;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (itr->pos[0] / 10 == pl) {
			param = *itr;
			flg = true;
		}
	}
	if (!flg) {
		fprintf(stderr, "corrmap abs not found PL=%03d\n", pl);
		exit(1);
	}

	double tmp_x, tmp_y;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		tmp_x = itr->x;
		tmp_y = itr->y;
		itr->x = tmp_x * param.position[0] + tmp_y * param.position[1] + param.position[4];
		itr->y = tmp_x * param.position[2] + tmp_y * param.position[3] + param.position[5];
		tmp_x = itr->ax;
		tmp_y = itr->ay;
		itr->ax = tmp_x * param.angle[0] + tmp_y * param.angle[1] + param.angle[4];
		itr->ay = tmp_x * param.angle[2] + tmp_y * param.angle[3] + param.angle[5];
	}
	fprintf(stderr, "basetrack trans fin\n");
}
void search_prediction(std::vector<Prediction> &pred, std::vector<vxx::base_track_t> &base) {
	double area_hash = 2000;
	double x_min, y_min;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(x_min, itr->x);
		y_min = std::min(y_min, itr->y);
	}

	std::pair<int, int>id;
	std::multimap<std::pair<int, int>, vxx::base_track_t*> base_map;
	for (auto itr=base.begin(); itr != base.end(); itr++) {
		id.first = (itr->x - x_min) / area_hash;
		id.second = (itr->y - y_min) / area_hash;
		base_map.insert(std::make_pair(id, &(*itr)));
	}


	std::ofstream ofs("diff.txt");
	bool flg = false;
	double angle;
	double pos_thr[2],ang_thr[2];
	double d_pos[2], d_ang[2];
	double id_x[2], id_y[2];
	int count = 0;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		count++;
		if (count % 10000 == 0) {
			fprintf(stderr, "\r now calc %d/%d(%4.1lf%%)", count, pred.size(), count*100. / pred.size());
		}
		flg = false;
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		pos_thr[0] = 15;
		pos_thr[1] = 15 + 30 * angle;
		ang_thr[0] = 0.02;
		ang_thr[1] = 0.02 + 0.15 * angle;
		id_x[0] = (itr->x - pos_thr[1] - x_min) / area_hash;
		id_x[1] = (itr->x + pos_thr[1] - x_min) / area_hash;
		id_y[0] = (itr->y - pos_thr[1] - y_min) / area_hash;
		id_y[1] = (itr->y + pos_thr[1] - y_min) / area_hash;
		for (int ix = id_x[0]; ix <= id_x[1]; ix++) {
			if (flg)break;
			for (int iy = id_y[0]; iy <= id_y[1]; iy++) {
				if (flg)break;
				id.first = ix;
				id.second = iy;
				if (base_map.count(id) == 0)continue;
				auto range = base_map.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					if (flg)break;
					d_pos[0] = ((res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax) / angle;
					d_pos[1] = ((res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay) / angle;
					d_ang[0] = ((res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax) / angle;
					d_ang[1] = ((res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay) / angle;
					if (fabs(d_pos[0]) > pos_thr[0])continue;
					if (fabs(d_pos[1]) > pos_thr[1])continue;
					if (fabs(d_ang[0]) > ang_thr[0])continue;
					if (fabs(d_ang[1]) > ang_thr[1])continue;
					ofs << std::right << std::fixed << std::setfill(' ')
						<< std::setw(8) << std::setprecision(1) << itr->x << " "
						<< std::setw(8) << std::setprecision(1) << itr->y << " "
						<< std::setw(7) << std::setprecision(4) << itr->ax << " "
						<< std::setw(7) << std::setprecision(4) << itr->ay << " "
						<< std::setw(6) << std::setprecision(2) << d_pos[0] << " "
						<< std::setw(6) << std::setprecision(2) << d_pos[1] << " "
						<< std::setw(6) << std::setprecision(4) << d_ang[0] << " "
						<< std::setw(6) << std::setprecision(4) << d_ang[1] << std::endl;
					itr->flg = 1;
					flg = true;
				}
			}
		}
		if (!flg) {
			itr->flg = 0;
		}
	}
	fprintf(stderr, "\r now calc %d/%d(%4.1lf%%)\n", count, pred.size(), count*100. / pred.size());

}
void output_prediction(std::string filename, std::vector<Prediction> pred) {

	std::ofstream ofs(filename);
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		ofs << std::right << std::fixed << std::setfill(' ')
			<< std::setw(3) << std::setprecision(0) << itr->PL << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->x << " "
			<< std::setw(8) << std::setprecision(1) << itr->y << " "
			<< std::setw(2) << std::setprecision(0) << itr->flg << std::endl;
	}

}