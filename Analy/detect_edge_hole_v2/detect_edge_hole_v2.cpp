#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

//#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <filesystem>
//typedef cv::Vec<float, 2> Vec2f;

class track_image_pixel {
public:
	int ix, iy, hit;
	//view center
	double x, y;
};
static void findSquares(const cv::Mat& image, std::vector<std::vector<cv::Point> >& squares);
static void drawSquares(cv::Mat& image, const std::vector<std::vector<cv::Point> >& squares);

std::vector<track_image_pixel> calc_pixel_inf(std::vector<vxx::micro_track_t> &micro, double size, std::pair<int, int>&edge, std::pair<double, double>&min);
cv::Mat Make_image_bin(std::vector<track_image_pixel>& pixel_inf, std::pair<int, int> edge, int bin_thr);
cv::Mat Make_image_gray(std::vector<track_image_pixel>& pixel_inf, std::pair<int, int> edge);

void wrtie_picture(std::vector<track_image_pixel>& pixel_inf, std::pair<int, int> edge, int bin_thr);
cv::Mat Make_smooth_image(std::vector<track_image_pixel>& pixel_inf, std::pair<int, int> edge);
void shiftImage(cv::Mat& img);
cv::Mat Make_Ft(cv::Mat image);
void Mask_Line(cv::Mat mask, int x_center, int x_width, int y_center, int y_width);
cv::Mat edge_detect(cv::Mat image);

std::vector<vxx::micro_track_t> read_all_microtrack(std::string ECC_path, int pl);

int main(int argc, char** argv) {
	if (argc != 4) {
		fprintf(stderr, "usgae:file-in-ECC pl pic_folder output-data\n");
		exit(1);
	}
	std::string file_in_ECC = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_out_floder = argv[3];

	std::vector<vxx::micro_track_t> micro = read_all_microtrack(file_in_ECC, pl);
	std::pair<int, int> edge;
	std::pair<double, double> min;
	double size = 50;
	std::vector<track_image_pixel> pixel_inf = calc_pixel_inf(micro, size, edge, min);

	//gray scale
	cv::Mat image_bin = Make_image_bin(pixel_inf, edge, 1);
	cv::Mat image_gray = Make_image_gray(pixel_inf, edge);

	std::stringstream file_out_bin, file_out_gray;
	file_out_bin << file_out_floder << "\\PL" << std::setw(3) << std::setfill('0') << pl << "_bin.png";
	file_out_gray << file_out_floder << "\\PL" << std::setw(3) << std::setfill('0') << pl << "_gray.png";
	cv::imwrite(file_out_bin.str(), image_bin);
	cv::imwrite(file_out_gray.str(), image_gray);

	std::stringstream file_out_trans_param;
	file_out_trans_param << file_out_floder << "\\PL" << std::setw(3) << std::setfill('0') << pl << "_param.txt";
	std::ofstream ofs(file_out_trans_param.str());
	ofs << size << " " << min.first << " " << min.second << std::endl;;

	//pixel --> basetrack
	//(ix+0.5)*size+x_min
	//(iy+0.5)*size+y_min




		//cv::Mat image2 = edge_detect(image);


		//cv::Mat image_ft = Make_Ft(image);

		//cv::Mat image_bg =  Make_smooth_image(pixel_inf, edge);
		//cv::Mat image_bin;
		//cv::threshold(image_bg, image_bin, 50, 255, cv::THRESH_BINARY); //二値化
		//
		//std::string filename = "image_test2.png";
		//cv::imwrite(filename, image_bin);

		//std::vector<std::vector<cv::Point> > squares;
		//findSquares(image, squares);
		//drawSquares(image, squares);
		//wrtie_picture(pixel_inf, edge,1);
}

std::vector<vxx::micro_track_t> read_all_microtrack(std::string ECC_path, int pl) {
	std::vector<vxx::micro_track_t> ret;
	for (int area = 1; area <= 6; area++) {
		std::stringstream file_corr;
		file_corr << ECC_path << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl
			<< "\\corrmap-connect-1-" << area << ".lst";
		if (!std::filesystem::exists(file_corr.str())) {
			fprintf(stderr, "file %s not found\n", file_corr.str().c_str());
		}
		corrmap0::Corrmap corr;
		corrmap0::read_cormap(file_corr.str(), corr);

		for (int i = 0; i <= 1; i++) {
			std::stringstream  file_micro;
			file_micro << ECC_path << "\\Area" << area << "\\PL"
				<< std::setw(3) << std::setfill('0') << pl << "\\f"
				<< std::setw(3) << std::setfill('0') << pl << "1_thick_"
				<< i << ".vxx";

			if (!std::filesystem::exists(file_micro.str())) {
				fprintf(stderr, "file %s not found\n", file_micro.str().c_str());
			}


			std::vector<vxx::micro_track_t> micro;
			vxx::FvxxReader fr;
			micro = fr.ReadAll(file_micro.str(), pl*10+1, 0);
			double tmp_x, tmp_y;
			for (auto itr = micro.begin(); itr != micro.end(); itr++) {
				tmp_x = itr->x + itr->ax*(itr->z2 - itr->z1) / 2;
				tmp_y = itr->y + itr->ay*(itr->z2 - itr->z1) / 2;
				itr->x = tmp_x * corr.position[0] + tmp_y * corr.position[1] + corr.position[4];
				itr->y = tmp_x * corr.position[2] + tmp_y * corr.position[3] + corr.position[5];
				itr->y = itr->y*-1;
				ret.push_back(*itr);
			}

		}
	}
	return ret;

}


std::vector<track_image_pixel> calc_pixel_inf(std::vector<vxx::micro_track_t> &micro, double size, std::pair<int, int>&edge, std::pair<double, double>&min) {
	std::vector<track_image_pixel> ret;
	double x_min, y_min;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		if (itr == micro.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		y_min = std::min(itr->y, y_min);
	}
	x_min = x_min - 10 * size;
	y_min = y_min - 10 * size;
	min.first = x_min;
	min.second = y_min;
	//pixel --> basetrack
	//(ix+0.5)*size+x_min
	//(iy+0.5)*size+y_min

	int ix, iy, ix_max, iy_max;
	std::map<std::pair<int, int>, int> pixel_inf_map;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		ix = (itr->x - x_min) / size;
		iy = (itr->y - y_min) / size;
		auto res = pixel_inf_map.insert(std::make_pair(std::make_pair(ix, iy), 1));
		if (!res.second) {
			res.first->second++;
		}
	}

	track_image_pixel pixel_inf;
	for (auto itr = pixel_inf_map.begin(); itr != pixel_inf_map.end(); itr++) {
		if (itr == pixel_inf_map.begin()) {
			ix_max = itr->first.first;
			iy_max = itr->first.second;
		}
		ix_max = std::max(ix_max, itr->first.first);
		iy_max = std::max(iy_max, itr->first.second);
		pixel_inf.hit = itr->second;
		pixel_inf.ix = itr->first.first;
		pixel_inf.iy = itr->first.second;
		pixel_inf.x = (pixel_inf.ix + 0.5)*size + x_min;
		pixel_inf.y = (pixel_inf.iy + 0.5)*size + y_min;

		ret.push_back(pixel_inf);
	}
	edge.first = ix_max;
	edge.second = iy_max;
	printf("xmax %d ymax %d\n", ix_max, iy_max);
	return ret;
}
cv::Mat Make_image_bin(std::vector<track_image_pixel>& pixel_inf, std::pair<int, int> edge, int bin_thr) {
	int max_pixel = std::max(edge.first, edge.second) + 10;
	//std::map<std::pair<int, int>, track_image_pixel> pixel_map;
	//for (auto itr = pixel_inf.begin(); itr != pixel_inf.end(); itr++) {
	//	pixel_map.insert(std::make_pair(std::make_pair(itr->ix, itr->iy), *itr));
	//}
	cv::Mat image = cv::Mat::zeros(max_pixel, max_pixel, CV_8UC3);

	for (auto itr = pixel_inf.begin(); itr != pixel_inf.end(); itr++) {
		//printf("%5d %5d %5d %5d\n", max_pixel, max_pixel, itr->ix, itr->iy);
		if (itr->hit < bin_thr)continue;
		image.at<cv::Vec3b>(itr->iy, itr->ix)[0] = 0xff; //青
		image.at<cv::Vec3b>(itr->iy, itr->ix)[1] = 0xff; //緑
		image.at<cv::Vec3b>(itr->iy, itr->ix)[2] = 0xff; //赤
	}
	return image;

}
cv::Mat Make_image_gray(std::vector<track_image_pixel>& pixel_inf, std::pair<int, int> edge) {
	int bin_max;
	int mean = 0;
	int max_pixel = std::max(edge.first, edge.second) + 10;
	for (auto itr = pixel_inf.begin(); itr != pixel_inf.end(); itr++) {
		mean += itr->hit;
	}
	mean = mean / pixel_inf.size();
	bin_max = mean * 2.5;
	cv::Mat image = cv::Mat::zeros(max_pixel, max_pixel, CV_8UC3);
	double ratio;
	int val;
	for (auto itr = pixel_inf.begin(); itr != pixel_inf.end(); itr++) {
		//printf("%5d %5d %5d %5d\n", max_pixel, max_pixel, itr->ix, itr->iy);
		ratio = std::min(1., itr->hit*1. / bin_max);
		val = int(ratio * 255);
		//if (itr->hit < bin_max)continue;
		image.at<cv::Vec3b>(itr->iy, itr->ix)[0] = val; //青
		image.at<cv::Vec3b>(itr->iy, itr->ix)[1] = val; //緑
		image.at<cv::Vec3b>(itr->iy, itr->ix)[2] = val; //赤
	}
	return image;

}


cv::Mat edge_detect(cv::Mat image) {
	cv::Mat im1 = image, clone();
	cv::Mat im2 = cv::Mat::zeros(image.size(), CV_8UC1);
	int width = im2.cols;
	int height = im2.rows;
	for (int iy = 0; iy < height; iy++) {
		for (int ix = 0; ix < width; ix++) {
			im2.data[iy * width + ix] = image.at<cv::Vec3b>(iy, ix)[0];
		}
	}


	std::vector< std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	cv::findContours(im2, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

	cv::Mat drawing = image.clone();
	cv::RNG rng(12345);

	for (size_t i = 0; i < contours.size(); i++) {
		cv::Scalar color = cv::Scalar(rng.uniform(0, 256), rng.uniform(0, 256), rng.uniform(0, 256));
		cv::drawContours(drawing, contours, (int)i, color);
	}
	std::string filename = "image_test_edge.png";
	cv::imwrite(filename, im2);
	return im2;
}
cv::Mat Make_smooth_image(std::vector<track_image_pixel>& pixel_inf, std::pair<int, int> edge) {
	int bin_max;
	int mean = 0;
	int max_pixel = std::max(edge.first, edge.second);
	for (auto itr = pixel_inf.begin(); itr != pixel_inf.end(); itr++) {
		mean += itr->hit;
	}
	mean = mean / pixel_inf.size();
	bin_max = mean * 2.5;
	cv::Mat image = cv::Mat::zeros(max_pixel + 20, max_pixel + 20, CV_8UC3);
	double ratio;
	int val;
	for (auto itr = pixel_inf.begin(); itr != pixel_inf.end(); itr++) {
		//printf("%5d %5d %5d %5d\n", max_pixel, max_pixel, itr->ix, itr->iy);
		ratio = std::min(1., itr->hit*1. / bin_max);
		val = int(ratio * 255);
		//if (itr->hit < bin_max)continue;
		image.at<cv::Vec3b>(itr->iy + 10, itr->ix + 10)[0] = val; //青
		image.at<cv::Vec3b>(itr->iy + 10, itr->ix + 10)[1] = val; //緑
		image.at<cv::Vec3b>(itr->iy + 10, itr->ix + 10)[2] = val; //赤
	}
	std::string filename = "image_test0.png";
	cv::imwrite(filename, image);

	cv::Mat bilateral;
	const int searchWindowSize = 50;
	cv::bilateralFilter(image, bilateral, searchWindowSize, 200, 50);
	filename = "image_test1.png";
	cv::imwrite(filename, bilateral);

	return  bilateral;
}

cv::Mat Make_Ft(cv::Mat image) {
	std::string filename = "image_test.png";
	cv::imwrite(filename, image);

	cv::Mat srcW, freqW, dstW;

	// DFT用最適サイズ取得
	int oW = cv::getOptimalDFTSize((int)image.cols);
	int oH = cv::getOptimalDFTSize((int)image.rows);
	cv::Mat mask(cv::Size(oW, oH), CV_32FC2, cv::Scalar(1.0, 1.0));
	// 入力画像を DFT 入力形式に変換
	// 入力画像のサイズをFFT処理に適切なサイズに拡張
	cv::copyMakeBorder(image, image,
		0, oH - image.rows, 0, oW - image.cols,
		cv::BORDER_CONSTANT, cv::Scalar::all(0));
	cv::cvtColor(image, image, cv::COLOR_BGR2GRAY);
	image.convertTo(image, CV_32FC1, 1.0 / 255.0);

	filename = "image_test_ori.png";
	cv::imwrite(filename, image * 255);
	//imshow("Input Image", src);


	std::vector<cv::Mat> chW{ image, cv::Mat::zeros(image.size(), CV_32FC1) };
	merge(chW, srcW); // チャンネル数2の入力画像srcWを合成

	// DFT
	dft(srcW, freqW); // DFT計算
	shiftImage(freqW); // 画像入れ替え
	//circle(mask, cv::Point(oW / 2, oH / 2), 32, cv::Scalar(0.0), -1); //低周波成分を0に
	//circle(mask, cv::Point(oW / 2, oH / 2), 100, cv::Scalar(0.0), 100); //高周波成分を0に
	Mask_Line(mask, 0, 20, 0, 20);

	split(freqW, chW);
	filename = "image_test_ori_fd.png";
	cv::imwrite(filename, chW[0] / 10);

	freqW = freqW.mul(mask); // フィルタリング（マスクとの積）
	split(freqW, chW);
	//imshow("周波数分布", chW[0] / 10);
	//Frequency distribution
	filename = "image_test_cut_fd.png";
	cv::imwrite(filename, chW[0] / 10);

	// 逆 DFT
	shiftImage(freqW); // 画像入れ替え（元に戻す）
	idft(freqW, dstW); // 逆DFT計算
	split(dstW, chW);
	normalize(chW[0], chW[0], 0.0, 1.0, cv::NORM_MINMAX);

	//image.convertTo(image, CV_32FC1, 1.0 / 255.0);

	filename = "image_test_cut.png";
	cv::imwrite(filename, chW[0] * 255);
	//imshow("逆DFT映像", chW[0]);
	//waitKey();
	return dstW;
}
void Mask_Line(cv::Mat mask, int x_center, int x_width, int y_center, int y_width) {
	int width = mask.cols;
	int height = mask.rows;
	for (int iy = 0; iy < height; iy++) {
		for (int ix = 0; ix < width; ix++) {
			if (abs(ix - x_center) < x_width)continue;
			if (abs(iy - y_center) < y_width)continue;

			mask.at<cv::Vec2f>(iy, ix)[0] = 0xffff;
			mask.at<cv::Vec2f>(iy, ix)[1] = 0xffff;
		}
	}
	//std::string filename;
	//filename = "image_test_mask0.png";
	//cv::imwrite(filename, mask[0] / 10);
	//filename = "image_test_mask1.png";
	//cv::imwrite(filename, mask[0] / 10);


	//std::vector<cv::Mat> chW{ image, cv::Mat::zeros(image.size(), CV_32FC1) };
	//merge(chW, srcW); // チャンネル数2の入力画像srcWを合成

	//// DFT
	//dft(srcW, freqW); // DFT計算
	//shiftImage(freqW); // 画像入れ替え
	////circle(mask, cv::Point(oW / 2, oH / 2), 32, cv::Scalar(0.0), -1); //低周波成分を0に
	////circle(mask, cv::Point(oW / 2, oH / 2), 100, cv::Scalar(0.0), 100); //高周波成分を0に
	//Mask_Line(mask, 0, 20, 0, 20);

	//split(freqW, chW);
	//filename = "image_test_ori_fd.png";
	//cv::imwrite(filename, chW[0] / 10);



}
// 画像入れ替え関数
void shiftImage(cv::Mat& img)
{
	int cx = img.cols / 2, cy = img.rows / 2;
	cv::Mat q0(img, cv::Rect(0, 0, cx, cy)), q1(img, cv::Rect(cx, 0, cx, cy));
	cv::Mat q2(img, cv::Rect(0, cy, cx, cy)), q3(img, cv::Rect(cx, cy, cx, cy)), tmp;
	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);
	q1.copyTo(tmp);
	q2.copyTo(q1);
	tmp.copyTo(q2);
}

void wrtie_picture(std::vector<track_image_pixel> &pixel_inf, std::pair<int, int> edge, int bin_thr) {

	int max_pixel = std::max(edge.first, edge.second);
	//std::map<std::pair<int, int>, track_image_pixel> pixel_map;
	//for (auto itr = pixel_inf.begin(); itr != pixel_inf.end(); itr++) {
	//	pixel_map.insert(std::make_pair(std::make_pair(itr->ix, itr->iy), *itr));
	//}

	cv::Mat image = cv::Mat::zeros(max_pixel + 20, max_pixel + 20, CV_8UC3);

	for (auto itr = pixel_inf.begin(); itr != pixel_inf.end(); itr++) {
		//printf("%5d %5d %5d %5d\n", max_pixel, max_pixel, itr->ix, itr->iy);
		if (itr->hit < bin_thr)continue;
		image.at<cv::Vec3b>(itr->iy + 10, itr->ix + 10)[0] = 0xff; //青
		image.at<cv::Vec3b>(itr->iy + 10, itr->ix + 10)[1] = 0xff; //緑
		image.at<cv::Vec3b>(itr->iy + 10, itr->ix + 10)[2] = 0xff; //赤
	}
	std::string filename = "output_base.png";
	cv::imwrite(filename, image);

}


static void help(const char* programName)
{
	std::cout <<
		"\nA program using pyramid scaling, Canny, contours and contour simplification\n"
		"to find squares in a list of images (pic1-6.png)\n"
		"Returns sequence of squares detected on the image.\n"
		"Call:\n"
		"./" << programName << " [file_name (optional)]\n"
		"Using OpenCV version " << CV_VERSION << "\n" << std::endl;
}
int thresh = 50, N = 11;
const char* wndname = "Square Detection Demo";
// helper function:
// finds a cosine of angle between vectors
// from pt0->pt1 and from pt0->pt2
static double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;
	return (dx1*dx2 + dy1 * dy2) / sqrt((dx1*dx1 + dy1 * dy1)*(dx2*dx2 + dy2 * dy2) + 1e-10);
}
// returns sequence of squares detected on the image.
static void findSquares(const cv::Mat& image, std::vector<std::vector<cv::Point> >& squares)
{
	squares.clear();
	cv::Mat pyr, timg, gray0(image.size(), CV_8U), gray;
	// down-scale and upscale the image to filter out the noise
	pyrDown(image, pyr, cv::Size(image.cols / 2, image.rows / 2));
	pyrUp(pyr, timg, image.size());
	std::vector<std::vector<cv::Point> > contours;
	// find squares in every color plane of the image
	for (int c = 0; c < 3; c++)
	{
		int ch[] = { c, 0 };
		mixChannels(&timg, 1, &gray0, 1, ch, 1);
		// try several threshold levels
		for (int l = 0; l < N; l++)
		{
			// hack: use Canny instead of zero threshold level.
			// Canny helps to catch squares with gradient shading
			if (l == 0)
			{
				// apply Canny. Take the upper threshold from slider
				// and set the lower to 0 (which forces edges merging)
				Canny(gray0, gray, 0, thresh, 5);
				// dilate canny output to remove potential
				// holes between edge segments
				dilate(gray, gray, cv::Mat(), cv::Point(-1, -1));
			}
			else
			{
				// apply threshold if l!=0:
				//     tgray(x,y) = gray(x,y) < (l+1)*255/N ? 255 : 0
				gray = gray0 >= (l + 1) * 255 / N;
			}
			// find contours and store them all as a list
			findContours(gray, contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);
			std::vector<cv::Point> approx;
			// test each contour
			for (size_t i = 0; i < contours.size(); i++)
			{
				// approximate contour with accuracy proportional
				// to the contour perimeter
				approxPolyDP(contours[i], approx, arcLength(contours[i], true)*0.02, true);
				// square contours should have 4 vertices after approximation
				// relatively large area (to filter out noisy contours)
				// and be convex.
				// Note: absolute value of an area is used because
				// area may be positive or negative - in accordance with the
				// contour orientation
				if (approx.size() == 4 &&
					fabs(contourArea(approx)) > 1000 &&
					isContourConvex(approx))
				{
					double maxCosine = 0;
					for (int j = 2; j < 5; j++)
					{
						// find the maximum cosine of the angle between joint edges
						double cosine = fabs(angle(approx[j % 4], approx[j - 2], approx[j - 1]));
						maxCosine = MAX(maxCosine, cosine);
					}
					// if cosines of all angles are small
					// (all angles are ~90 degree) then write quandrange
					// vertices to resultant sequence
					if (maxCosine < 0.3)
						squares.push_back(approx);
				}
			}
		}
	}
}
// the function draws all the squares in the image
static void drawSquares(cv::Mat& image, const std::vector<std::vector<cv::Point> >& squares)
{
	cv::Mat image_tmp;
	for (size_t i = 0; i < squares.size(); i++)
	{
		image_tmp = image.clone();
		const cv::Point* p = &squares[i][0];
		int n = (int)squares[i].size();
		polylines(image_tmp, &p, &n, 1, true, cv::Scalar(0, 255, 0), 3, cv::LINE_AA);
		std::stringstream filename;
		filename << "image_test_squares_" << std::setw(3) << std::setfill('0') << i << ".png";
		cv::imwrite(filename.str(), image_tmp);

	}
}
