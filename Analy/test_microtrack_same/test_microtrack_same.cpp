#pragma comment(lib, "VxxReader.lib")
#include <VxxReader.h>
#include <fstream>
#include <iostream>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <map>
void file_output(std::string filename, std::vector<vxx::micro_track_t> &micro_sel, std::vector<vxx::micro_track_t> &micro_all);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg file-in-fvxx-sel file-in-fvxx-all pos zone output-file\n");
		exit(1);
	}
	std::string file_in_fvxx_sel = argv[1];
	std::string file_in_fvxx_all = argv[2];
	int pos = std::stoi(argv[3]);
	int zone = std::stoi(argv[4]);
	std::string file_out_list = argv[5];

	std::vector<vxx::micro_track_t> micro_sel, micro_all;
	vxx::FvxxReader fr;
	micro_sel = fr.ReadAll(file_in_fvxx_sel, pos, zone);
	micro_all = fr.ReadAll(file_in_fvxx_all, pos, zone);

	file_output(file_out_list, micro_sel, micro_all);

}
void file_output(std::string filename, std::vector<vxx::micro_track_t> &micro_sel, std::vector<vxx::micro_track_t> &micro_all) {

	std::map<std::tuple<int, int, int>, vxx::micro_track_t*> micro_map;
	for (auto itr = micro_all.begin(); itr != micro_all.end(); itr++) {
		micro_map.insert(std::make_pair(std::make_tuple(itr->col, itr->row, itr->isg), &(*itr)));
	}
	std::ofstream ofs(filename);
	std::tuple<int, int, int> id;
	for (auto itr = micro_sel.begin(); itr != micro_sel.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (micro_map.count(id) == 0) {
			fprintf(stderr, "not found %d %d %d\n", std::get<0>(id), std::get<1>(id), std::get<2>(id));
		}
		else {
			auto res = micro_map.find(id);
			ofs << std::right << std::fixed
				<< std::setw(12) << std::setprecision(0) << itr->rawid << " "
				<< std::setw(8) << std::setprecision(0) << itr->col << " "
				<< std::setw(8) << std::setprecision(0) << itr->row << " "
				<< std::setw(8) << std::setprecision(0) << itr->isg << " "
				<< std::setw(7) << std::setprecision(0) << itr->ph << " "
				<< std::setw(7) << std::setprecision(4) << itr->ax << " "
				<< std::setw(7) << std::setprecision(4) << itr->ay << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << " "
				<< std::setw(7) << std::setprecision(0) << res->second->ph << " "
				<< std::setw(7) << std::setprecision(4) << res->second->ax << " "
				<< std::setw(7) << std::setprecision(4) << res->second->ay << " "
				<< std::setw(8) << std::setprecision(1) << res->second->x << " "
				<< std::setw(8) << std::setprecision(1) << res->second->y << std::endl;

		}
	}


}
