#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::set<int> event_list(mfile0::Mfile &m);
std::vector<Sharing_file::Sharing_file> sharing_file_selection(std::vector<Sharing_file::Sharing_file>&sf, std::set<int>&eventid);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-mfile file-in-sf file-out-sf\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_sf = argv[2];
	std::string file_out_sf = argv[3];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::vector<Sharing_file::Sharing_file>sf = Sharing_file::Read_sharing_file_extension(file_in_sf);

	std::set<int> ev = event_list(m);
	sf = sharing_file_selection(sf, ev);

	Sharing_file::Write_sharing_file_txt(file_out_sf, sf);

}
std::set<int> event_list(mfile0::Mfile &m) {
	std::set<int> ret;

	for (auto &c : m.chains) {
		ret.insert(c.basetracks.begin()->group_id);
	}
	return ret;
}
	
std::vector<Sharing_file::Sharing_file> sharing_file_selection(std::vector<Sharing_file::Sharing_file>&sf, std::set<int>&eventid) {
	std::vector<Sharing_file::Sharing_file> ret;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		if (eventid.count(itr->eventid) == 0)continue;
		ret.push_back(*itr);
	}
	return ret;
}