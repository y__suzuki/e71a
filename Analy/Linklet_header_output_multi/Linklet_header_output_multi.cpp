#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <chrono>

#include <FILE_structure.hpp>


void output_header(std::ofstream &ofs, std::vector<netscan::linklet_t> &link);
bool checkFileExistence(const std::string& str);
void input_output_header(std::string file_in, std::string file_out);

int main(int argc, char **argv) {
	if (argc != 7) {
		fprintf(stderr, "prg linklet-path output-name pl-start pl-end mode multi\n");
		exit(1);
	}
	std::string file_path_linklet = argv[1];
	std::string file_out_txt = argv[2];
	int pl_start = std::stoi(argv[3]);
	int pl_end = std::stoi(argv[4]);
	int mode = std::stoi(argv[5]);
	int multi = std::stoi(argv[6]);

	std::vector<std::string>filename_v;
	if (mode == 0) {
		for (int pl = pl_start; pl < pl_end; pl++) {
			for (int peke = 0; peke <= 2; peke++) {
				if (peke == 2) {
					//PL015-018はつながない
					if (pl + peke + 1 == 18)continue;
					//水2層はつながない
					if (pl >= 16 && pl % 2 == 1)continue;
				}
				std::stringstream file_link;
				file_link << file_path_linklet << "\\l-" << std::setw(3) << std::setfill('0') << pl << "-"
					<< std::setw(3) << std::setfill('0') << pl + peke + 1 << ".sel.bll";

				if (checkFileExistence(file_link.str()) == false) {
					fprintf(stderr, "file [%s] not exist\n", file_link.str().c_str());
					continue;
				}
				filename_v.push_back(file_link.str());

			}
		}
	}
	else {
		fprintf(stderr,"mode = 0:1st chain\n");
		fprintf(stderr,"mode = 1:2nd chain\n");
		exit(1);
	}

	FILE *fp;	/* (1)ファイルポインタの宣言 */
	char s[256];

	/* (2)ファイルのオープン */
	/*  ここで、ファイルポインタを取得する */
	if ((fp = fopen("smpl.txt", "r")) == NULL) {
		printf("file open error!!\n");
		exit(EXIT_FAILURE);	/* (3)エラーの場合は通常、異常終了する */
	}

	/* (4)ファイルの読み（書き）*/
	while (fgets(s, 256, fp) != NULL) {

		/* ここではfgets()により１行単位で読み出し */
		printf("%s", s);
	}
	fclose(fp);	/* (5)ファイルのクローズ */

	return 0;
	std::ofstream ofs(file_out_txt, std::ios::app);
#pragma omp parallel for num_threads(multi) schedule(dynamic,1)
	for (int i = 0; i < filename_v.size(); i++) {
		std::vector<netscan::linklet_t> link;
		netscan::read_linklet_bin(filename_v[i], link);
#pragma omp critical
		output_header(ofs, link);
	}

}
bool checkFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}
void output_header(std::ofstream &ofs, std::vector<netscan::linklet_t> &link) {
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		ofs << itr->pos[0] << " " << itr->b[0].rawid << " " << itr->pos[1] << " " << itr->b[1].rawid << std::endl;
	}
}
void input_output_header(std::string file_in, std::string file_out) {
	std::ifstream ifs(file_in, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;

	std::ofstream ofs(file_out, std::ios::app);
	int64_t count = 0;
	netscan::linklet_t l;
	while (ifs.read((char*)& l, sizeof(netscan::linklet_t))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading writing ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		ofs << l.pos[0] << " " << l.b[0].rawid << " " << l.pos[1] << " " << l.b[1].rawid << std::endl;
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading writing ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

}
