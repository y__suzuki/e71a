#include <filesystem>
#include <fstream>
#include <map>
#include <sstream>
#include <picojson.h>
#include <set>
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class GrainInformation {
public:
	int id, face, ix, iy, layer, pixelnum, brightness_sum9;
	double x, y, z;
};
class AffineParam {
public:
	double rotation, shrink, shift[2];
};

std::vector<GrainInformation> read_grain_inf(std::string filename, int &face);
void read_param_dc(std::string filename, AffineParam&param, AffineParam&param_dc);
void apply_param(std::vector<GrainInformation>&grain, corrmap0::Corrmap &param, double nom_z);
void output_grain(std::string filename, std::vector< GrainInformation > &grain);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "prg file-in-grain pl file-in-ECC file-in-enevt_track output-file-face\n");
		exit(1);
	}


	std::string file_in_grain = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_ECC = argv[3];
	std::string file_in_mfile = argv[4];
	std::string file_out_grain = argv[5];
	std::string file_out_track = argv[5];


	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);

	double nominal_z = z_map.at(pl);

	std::stringstream corr_abs_path;
	corr_abs_path << file_in_ECC << "\\Area0\\0\\align\\corrmap-abs.lst";
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::Corrmap param;
	corrmap0::read_cormap(corr_abs_path.str(), corr_abs);
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		if (itr->pos[0] / 10 == pl) {
			param = *itr;
		}
	}


	int face;
	std::vector<GrainInformation> grain_all = read_grain_inf(file_in_grain, face);


	apply_param(grain_all, param, nominal_z);
	output_grain(file_out_grain, grain_all);



}
std::vector<GrainInformation> read_grain_inf(std::string filename, int &face) {
	std::ifstream ifs(filename);
	std::vector<GrainInformation> ret;
	GrainInformation gr;
	int count = 0;
	while (ifs >> gr.face >> gr.ix >> gr.iy >> gr.layer >> gr.x >> gr.y >> gr.z >> gr.pixelnum >> gr.brightness_sum9) {
		if (count % 100000 == 0) {
			printf("\r grain read %d", count);
		}
		face = gr.face;
		gr.id = count;
		count++;
		ret.push_back(gr);
	}
	printf("\r grain read fin %d\n", ret.size());
	return ret;

}

void apply_param(std::vector<GrainInformation>&grain, corrmap0::Corrmap &param, double nom_z) {

	double dz = nom_z + param.dz;
	double x, y;
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		x = itr->x;
		y = itr->y;
		itr->z += dz;
		itr->x = param.position[0] * x + param.position[1] * y + param.position[4];
		itr->y = param.position[2] * x + param.position[3] * y + param.position[5];
	}



}

void output_grain(std::string filename, std::vector< GrainInformation > &grain) {
	std::ofstream ofs(filename);
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {

		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->face << " "
			<< std::setw(3) << std::setprecision(0) << itr->ix << " "
			<< std::setw(3) << std::setprecision(0) << itr->iy << " "
			<< std::setw(10) << std::setprecision(0) << itr->layer << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(10) << std::setprecision(1) << itr->z << " "
			<< std::setw(10) << std::setprecision(0) << itr->pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->brightness_sum9 << std::endl;


	}
}