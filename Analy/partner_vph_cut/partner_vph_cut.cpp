#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_inf {
public:
	int groupid, chainid, nseg, npl,edge_out_flg;
	std::vector<int>pl_all;
	double ax, ay, vph_ave, vph_sigma;

};
class Point {
public:
	double x, y, z;
};
class Fiducial_Area {
public:
	int pl;
	Point p[2];
	//double x0, y0, z0, x1, y1, z1;
};

std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch);
std::map<double, std::pair<double, double>> vph_mip_distribution();
std::vector<output_inf> Calc_nseg_npl(std::vector<Momentum_recon::Event_information> &ev_v, std::map<double, std::pair<double, double>> &vph_mip, std::map<int, std::vector<Fiducial_Area>>&area);
void output_file(std::string filename, std::vector<output_inf>&output);
std::vector<Momentum_recon::Event_information> Cut_vph(std::vector<Momentum_recon::Event_information> &ev_v, std::map<double, std::pair<double, double>> &vph_mip, std::map<int, std::vector<Fiducial_Area>>&area);
void trans_base_all(std::vector < std::pair<Point*, corrmap_3d::align_param2*>>&track_pair);
std::vector <std::pair<Point*, corrmap_3d::align_param2*>>track_affineparam_correspondence(std::vector<Point*>&p, std::vector <corrmap_3d::align_param2> &param);
void trans_mfile_cordinate(std::vector<corrmap_3d::align_param2> &param, std::vector<Fiducial_Area>&area);
bool judge_fiducial_area(std::vector<Fiducial_Area>&area, Momentum_recon::Mom_basetrack&b);
void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map);
std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename);
int judge_momch_edgeout(int direction, Momentum_recon::Mom_chain &chain, std::map<int, std::vector<Fiducial_Area>> &area, double edge_cut, int ex_pl_max);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg file-in-momch file-in-ECC fa.txt file-out-momch file-out-inf\n");
	}
	std::string file_in_momch = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_in_area = argv[3];
	std::string file_out_momch = argv[4];
	std::string file_out = argv[5];

	//corrmap absの読み込み
	std::string file_in_corrmap = file_in_ECC + "\\Area0\\0\\align\\fine\\local\\corrmap-local-abs.lst";
	std::map<int, std::vector<corrmap_3d::align_param>>corrmap = corrmap_3d::read_ali_param_abs(file_in_corrmap, 1);
	std::map<int, std::vector<corrmap_3d::align_param2>>corrmap_dd = corrmap_3d::DelaunayDivide_map(corrmap);

	//fiducial areaの読み込み
	std::map<int, std::vector<Fiducial_Area>> area = read_fiducial_Area(file_in_area);
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		if (corrmap_dd.count(itr->first) == 0) {
			fprintf(stderr, "corrmap local abs PL%03d not found\n", itr->first);
			exit(1);
		}
		std::vector<corrmap_3d::align_param2> param = corrmap_dd.at(itr->first);
		trans_mfile_cordinate(param, itr->second);
	}


	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);

	std::map<double, std::pair<double, double>> vph_mip = vph_mip_distribution();

	std::vector<output_inf> out = Calc_nseg_npl(momch, vph_mip, area);

	output_file(file_out, out);
	momch = Cut_vph(momch, vph_mip,area);
	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);

}

std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename) {

	std::ifstream ifs(filename);
	std::multimap<int, Fiducial_Area> fa_multi;
	std::map<int, std::vector<Fiducial_Area>> ret;
	Fiducial_Area fa;
	while (ifs >> fa.pl >> fa.p[0].x >> fa.p[0].y >> fa.p[0].z >> fa.p[1].x >> fa.p[1].y >> fa.p[1].z) {
		fa_multi.insert(std::make_pair(fa.pl, fa));
	}

	int count = 0;
	for (auto itr = fa_multi.begin(); itr != fa_multi.end(); itr++) {
		count = fa_multi.count(itr->first);
		auto range = fa_multi.equal_range(itr->first);
		std::vector<Fiducial_Area> fa_vec;
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			fa_vec.push_back(itr2->second);
		}
		ret.insert(std::make_pair(itr->first, fa_vec));
	}

	return ret;

}

void trans_mfile_cordinate(std::vector<corrmap_3d::align_param2> &param, std::vector<Fiducial_Area>&area) {

	std::vector< Point*> p_trans;
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		p_trans.push_back(&(itr->p[0]));
		p_trans.push_back(&(itr->p[1]));
	}
	std::vector <std::pair<Point*, corrmap_3d::align_param2*>> p_trans_map = track_affineparam_correspondence(p_trans, param);
	trans_base_all(p_trans_map);
}


std::map<double, std::pair<double, double>> vph_mip_distribution() {
	std::map<double, std::pair<double, double>> ret;
	ret.insert(std::make_pair(0.05, std::make_pair(61.86, 6.8)));
	ret.insert(std::make_pair(0.15, std::make_pair(49.68, 7.6)));
	ret.insert(std::make_pair(0.25, std::make_pair(36.38, 5.0)));
	ret.insert(std::make_pair(0.35, std::make_pair(30.85, 4.0)));
	ret.insert(std::make_pair(0.45, std::make_pair(29.20, 3.8)));
	ret.insert(std::make_pair(0.55, std::make_pair(27.17, 3.9)));
	ret.insert(std::make_pair(0.65, std::make_pair(25.14, 3.8)));
	ret.insert(std::make_pair(0.80, std::make_pair(22.26, 3.6)));
	ret.insert(std::make_pair(1.00, std::make_pair(19.83, 3.3)));
	ret.insert(std::make_pair(1.20, std::make_pair(18.39, 3.2)));
	ret.insert(std::make_pair(1.40, std::make_pair(17.24, 3.1)));
	ret.insert(std::make_pair(1.70, std::make_pair(16.02, 3.0)));
	ret.insert(std::make_pair(2.10, std::make_pair(15.29, 3.0)));
	ret.insert(std::make_pair(2.50, std::make_pair(14.19, 3.0)));
	return ret;
}

std::vector<output_inf> Calc_nseg_npl(std::vector<Momentum_recon::Event_information> &ev_v, std::map<double, std::pair<double, double>> &vph_mip, std::map<int, std::vector<Fiducial_Area>>&area) {
	std::vector<output_inf> ret;
	output_inf out;
	int vertex_pl, direction;
	
	for (auto &ev : ev_v) {
		for (auto itr = ev.chains.begin(); itr != ev.chains.end(); itr++) {
			if (itr->chainid == 0)continue;

			vertex_pl = ev.vertex_pl;
			direction = itr->direction;
			Momentum_recon::Mom_chain chain;
			chain = *itr;
			chain.base.clear();
			chain.base_pair.clear();
			if (direction == 1) {
				for (auto &b : itr->base) {
					if (b.pl > vertex_pl)continue;
					chain.base.push_back(b);
				}
				for (auto &pair : itr->base_pair) {
					if (pair.second.pl > vertex_pl)continue;
					chain.base_pair.push_back(pair);
				}
			}
			else if (direction == -1) {
				for (auto &b : itr->base) {
					if (b.pl <= vertex_pl)continue;
					chain.base.push_back(b);
				}
				for (auto &pair : itr->base_pair) {
					if (pair.first.pl <= vertex_pl)continue;
					chain.base_pair.push_back(pair);
				}
			}

			out.groupid = ev.groupid;
			out.chainid = chain.chainid;
			out.edge_out_flg = judge_momch_edgeout(itr->direction, *itr, area, 0, 4);

			out.npl = chain.base.rbegin()->pl - chain.base.begin()->pl + 1;
			out.nseg = chain.base.size();
			out.ax = 0;
			out.ay = 0;
			out.vph_ave = 0;
			out.pl_all.clear();
			int count_angle = 0;
			int count_vph = 0;
			for (auto &b : chain.base) {
				out.ax += b.ax;
				out.ay += b.ay;
				out.vph_ave += b.m[0].ph % 10000;
				out.vph_ave += b.m[1].ph % 10000;
				count_angle += 1;
				count_vph += 2;
				out.pl_all.push_back(b.pl);
			}
			out.ax /= count_angle;
			out.ay /= count_angle;
			out.vph_ave /= count_vph;
			double angle = sqrt(out.ax*out.ax + out.ay * out.ay);

			double sigma = 0;
			auto vph_angle = vph_mip.upper_bound(angle);
			if (vph_angle == vph_mip.begin()) {
				sigma = (out.vph_ave - vph_angle->second.first) / vph_angle->second.second;
			}
			else if (vph_angle == vph_mip.end()) {
				sigma = (out.vph_ave - vph_mip.rbegin()->second.first) / vph_mip.rbegin()->second.second;
			}
			else {
				double vph_mean, vph_sigma;
				auto val0 = std::next(vph_angle, -1);
				auto val1 = vph_angle;
				vph_mean = (val1->second.first - val0->second.first) / (val1->first - val0->first)*(angle - val0->first) + val0->second.first;
				vph_sigma = (val1->second.second - val0->second.second) / (val1->first - val0->first)*(angle - val0->first) + val0->second.second;
				sigma = (out.vph_ave - vph_mean) / vph_sigma;
			}
			out.vph_sigma = sigma;
			ret.push_back(out);
		}
	}
	return ret;

}

std::vector<Momentum_recon::Event_information> Cut_vph(std::vector<Momentum_recon::Event_information> &ev_v, std::map<double, std::pair<double, double>> &vph_mip, std::map<int, std::vector<Fiducial_Area>>&area) {
	std::vector<Momentum_recon::Event_information> ret;
	output_inf out;
	int vertex_pl, direction;
	for (auto &ev : ev_v) {
		Momentum_recon::Event_information cut_ev = ev;
		cut_ev.chains.clear();
		for (auto itr = ev.chains.begin(); itr != ev.chains.end(); itr++) {
			if (itr->chainid == 0) {
				cut_ev.chains.push_back(*itr);
				continue;
			}

			vertex_pl = ev.vertex_pl;
			direction = itr->direction;
			Momentum_recon::Mom_chain chain;
			chain = *itr;
			chain.base.clear();
			chain.base_pair.clear();
			if (direction == 1) {
				for (auto &b : itr->base) {
					if (b.pl > vertex_pl)continue;
					chain.base.push_back(b);
				}
				for (auto &pair : itr->base_pair) {
					if (pair.second.pl > vertex_pl)continue;
					chain.base_pair.push_back(pair);
				}
			}
			else if (direction == -1) {
				for (auto &b : itr->base) {
					if (b.pl <= vertex_pl)continue;
					chain.base.push_back(b);
				}
				for (auto &pair : itr->base_pair) {
					if (pair.first.pl <= vertex_pl)continue;
					chain.base_pair.push_back(pair);
				}
			}

			out.groupid = ev.groupid;
			out.chainid = chain.chainid;
			out.edge_out_flg = judge_momch_edgeout(itr->direction, *itr, area, 0, 4);
			out.npl = chain.base.rbegin()->pl - chain.base.begin()->pl + 1;
			out.nseg = chain.base.size();
			out.ax = 0;
			out.ay = 0;
			out.vph_ave = 0;
			out.pl_all.clear();
			int count_angle = 0;
			int count_vph = 0;
			for (auto &b : chain.base) {
				out.ax += b.ax;
				out.ay += b.ay;
				out.vph_ave += b.m[0].ph % 10000;
				out.vph_ave += b.m[1].ph % 10000;
				count_angle += 1;
				count_vph += 2;
				out.pl_all.push_back(b.pl);
			}
			out.ax /= count_angle;
			out.ay /= count_angle;
			out.vph_ave /= count_vph;
			double angle = sqrt(out.ax*out.ax + out.ay * out.ay);

			double sigma = 0;
			auto vph_angle = vph_mip.upper_bound(angle);
			if (vph_angle == vph_mip.begin()) {
				sigma = (out.vph_ave - vph_angle->second.first) / vph_angle->second.second;
			}
			else if (vph_angle == vph_mip.end()) {
				sigma = (out.vph_ave - vph_mip.rbegin()->second.first) / vph_mip.rbegin()->second.second;
			}
			else {
				double vph_mean, vph_sigma;
				auto val0 = std::next(vph_angle, -1);
				auto val1 = vph_angle;
				vph_mean = (val1->second.first - val0->second.first) / (val1->first - val0->first)*(angle - val0->first) + val0->second.first;
				vph_sigma = (val1->second.second - val0->second.second) / (val1->first - val0->first)*(angle - val0->first) + val0->second.second;
				sigma = (out.vph_ave - vph_mean) / vph_sigma;
			}
			out.vph_sigma = sigma;
			if (out.npl == 1) {
				if (out.vph_sigma < 20)continue;
			}
			else if (out.npl <= 10) {
				if (out.vph_sigma < 10 && out.edge_out_flg == 0)continue;
			}
			cut_ev.chains.push_back(*itr);
		}
		ret.push_back(cut_ev);
	}
	return ret;

}
void output_file(std::string filename, std::vector<output_inf>&output) {
	std::ofstream ofs(filename);
	for (auto itr = output.begin(); itr != output.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(6) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(6) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(2) << std::setprecision(0) << itr->edge_out_flg << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(6) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(6) << std::setprecision(0) << itr->npl << " "
			<< std::setw(6) << std::setprecision(1) << itr->vph_ave << " "
			<< std::setw(6) << std::setprecision(4) << itr->vph_sigma << std::endl;
		for (int i = 0; i < itr->pl_all.size(); i++) {
			if (i + 1 == itr->pl_all.size()) {
				ofs << std::setw(3) << std::setprecision(0) << itr->pl_all[i] << std::endl;
			}
			else {
				ofs << std::setw(3) << std::setprecision(0) << itr->pl_all[i] << " ";
			}
		}
	}
}




int judge_momch_edgeout(int direction, Momentum_recon::Mom_chain &chain, std::map<int, std::vector<Fiducial_Area>> &area, double edge_cut, int ex_pl_max) {

	int edge_pl;
	int return_flg = 0;
	if (direction == 1) {
		edge_pl = chain.base.begin()->pl;
		return_flg = 0;
		for (int ex_pl = 0; ex_pl <= ex_pl_max; ex_pl++) {
			if (area.count(edge_pl - ex_pl) == 0)continue;
			//ex_z = z_map.at(up_pl + ex_pl);
			//ex_x = up_x + up_ax * (ex_z - up_z);
			//ex_y = up_y + up_ay * (ex_z - up_z);

			if (!judge_fiducial_area(area.at(edge_pl - ex_pl), *chain.base.begin())) {
				return_flg = 2;
			}
		}
		if (edge_pl >= 132)return_flg = 1;
	}
	else if (direction == -1) {
		edge_pl = chain.base.rbegin()->pl;
		return_flg = 0;
		for (int ex_pl = 0; ex_pl <= ex_pl_max; ex_pl++) {
			if (area.count(edge_pl + ex_pl) == 0)continue;
			//ex_z = z_map.at(up_pl + ex_pl);
			//ex_x = up_x + up_ax * (ex_z - up_z);
			//ex_y = up_y + up_ay * (ex_z - up_z);

			if (!judge_fiducial_area(area.at(edge_pl + ex_pl), *chain.base.rbegin())) {
				return_flg = 2;
			}
		}
		if (edge_pl <= 4)return_flg = 1;

	}

	return return_flg;

}

bool judge_fiducial_area(std::vector<Fiducial_Area>&area, Momentum_recon::Mom_basetrack&b) {

	std::map<double, Point> point_map;
	double ex_x, ex_y, dist;
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		ex_x = b.x + b.ax*(itr->p[0].z - b.z);
		ex_y = b.y + b.ay*(itr->p[0].z - b.z);
		dist = pow(ex_x - itr->p[0].x, 2) + pow(ex_y - itr->p[0].y, 2);
		point_map.insert(std::make_pair(dist, itr->p[0]));
	}
	//外挿先から距離の一番近い点のz座標を使用
	double z = point_map.begin()->second.z;
	double x = b.x + b.ax*(z - b.z);
	double y = b.y + b.ay*(z - b.z);


	//true でArea内　falseでarea外

	//点(x,y)からx軸性の方向に直線を引き、その直線と多角形の辺が何回交わるか。
	//下から上に交わったときwn+1
	//上から下に交わったときwn-1
	int wn = 0;
	double vt;
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		// 上向きの辺、下向きの辺によって処理が分かれる。
	// 上向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、終点は含まない。(ルール1)
		if (itr->p[0].y <= y && itr->p[1].y > y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->p[0].y) / (itr->p[1].y - itr->p[0].y);
			if (x < itr->p[0].x + vt * (itr->p[1].x - itr->p[0].x)) {
				++wn;  //ここが重要。上向きの辺と交差した場合は+1
			}
		}
		// 下向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、始点は含まない。(ルール2)
		else if (itr->p[0].y > y && itr->p[1].y <= y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->p[0].y) / (itr->p[1].y - itr->p[0].y);
			if (x < itr->p[0].x + vt * (itr->p[1].x - itr->p[0].x)) {
				--wn;  //ここが重要。下向きの辺と交差した場合は-1
			}
		}
	}
	if (wn >= 1)return true;
	return false;
}

//mfile0::M_Base
//basetrack-alignment mapの対応
double select_triangle_vale(corrmap_3d::align_param2* param, Point*p) {
	double x, y;
	double dist = 0;
	x = (param->corr_p[0]->x + param->corr_p[1]->x + param->corr_p[2]->x) / 3;
	y = (param->corr_p[0]->y + param->corr_p[1]->y + param->corr_p[2]->y) / 3;
	dist = (p->x - x)*(p->x - x) + (p->y - y)*(p->y - y);
	return dist;
}
corrmap_3d::align_param2* search_param(std::vector<corrmap_3d::align_param*> &param, Point*p, std::multimap<int, corrmap_3d::align_param2*>&triangles) {
	//三角形内部
	//最近接三角形
	double dist = 0;
	std::map<double, corrmap_3d::align_param* > dist_map;
	//align_paramを近い順にsort
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		dist = ((*itr)->x - p->x)*((*itr)->x - p->x) + ((*itr)->y - p->y)*((*itr)->y - p->y);
		dist_map.insert(std::make_pair(dist, (*itr)));
	}

	double sign[3];
	bool flg = false;
	int id;

	corrmap_3d::align_param2* ret = triangles.begin()->second;
	for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
		if (itr != dist_map.begin())continue;


		//corrmapのID
		id = itr->second->id;
		if (triangles.count(id) == 0) {
			fprintf(stderr, "alignment triangle ID=%d not found\n", id);
			exit(1);
		}
		//idの属する三角形を探索
		auto range = triangles.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			sign[0] = (itr2->second->corr_p[1]->x - itr2->second->corr_p[0]->x)*(p->y - itr2->second->corr_p[1]->y) - (itr2->second->corr_p[1]->y - itr2->second->corr_p[0]->y)*(p->x - itr2->second->corr_p[1]->x);
			sign[1] = (itr2->second->corr_p[2]->x - itr2->second->corr_p[1]->x)*(p->y - itr2->second->corr_p[2]->y) - (itr2->second->corr_p[2]->y - itr2->second->corr_p[1]->y)*(p->x - itr2->second->corr_p[2]->x);
			sign[2] = (itr2->second->corr_p[0]->x - itr2->second->corr_p[2]->x)*(p->y - itr2->second->corr_p[0]->y) - (itr2->second->corr_p[0]->y - itr2->second->corr_p[2]->y)*(p->x - itr2->second->corr_p[0]->x);
			//printf("point %.lf,%.1lf\n", base.x, base.y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[0]->x, itr2->second->corr_p[0]->y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[1]->x, itr2->second->corr_p[1]->y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[2]->x, itr2->second->corr_p[2]->y);
			//printf("sign %.1lf %1.lf %.1lf\n", sign[0], sign[1], sign[2]);
			//printf("  signbit %d %d %d\n", std::signbit(sign[0]), std::signbit(sign[1]), std::signbit(sign[2]));
			//printf("n signbit %d %d %d\n", !std::signbit(sign[0]), !std::signbit(sign[1]), !std::signbit(sign[2]));
			//printf("judge %d\n", (std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2])));
			//printf("\n");

			//符号が3つとも一致でtrue
			if ((std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2]))) {
				ret = itr2->second;
				flg = true;
				break;
			}
		}
		if (flg)break;
	}
	if (flg) {
		//printf("point in trianlge\n");
		return ret;
	}

	//distが最小になるcorrmapをとってくる
	dist = -1;
	for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
		//corrmapのID
		id = itr->second->id;
		if (triangles.count(id) == 0) {
			fprintf(stderr, "alignment triangle ID=%d not found\n", id);
			exit(1);
		}
		//idの属する三角形を探索
		auto range = triangles.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			if (dist<0 || dist>select_triangle_vale(itr2->second, p)) {
				dist = select_triangle_vale(itr2->second, p);
				ret = itr2->second;
			}
		}
	}
	//printf("point not in trianlge\n");
	return ret;
}
std::vector <std::pair<Point*, corrmap_3d::align_param2*>>track_affineparam_correspondence(std::vector<Point*>&p, std::vector <corrmap_3d::align_param2> &param) {

	//local alignの視野中心を取り出して、位置でhash
	//local alignの視野中心の作るdelaunay三角形をmapで対応

	std::map<int, corrmap_3d::align_param*> view_center;
	std::multimap<int, corrmap_3d::align_param2*>triangles;
	double xmin = 999999, ymin = 999999, hash = 2000;
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		for (int i = 0; i < 3; i++) {
			view_center.insert(std::make_pair(itr->corr_p[i]->id, (itr->corr_p[i])));
			triangles.insert(std::make_pair(itr->corr_p[i]->id, &(*itr)));
			xmin = std::min(itr->corr_p[i]->x, xmin);
			ymin = std::min(itr->corr_p[i]->y, ymin);
		}
	}
	std::multimap<std::pair<int, int>, corrmap_3d::align_param*> view_center_hash;
	std::pair<int, int>id;
	for (auto itr = view_center.begin(); itr != view_center.end(); itr++) {
		id.first = int((itr->second->x - xmin) / hash);
		id.second = int((itr->second->y - ymin) / hash);
		view_center_hash.insert(std::make_pair(id, itr->second));
	}

	std::vector < std::pair<Point*, corrmap_3d::align_param2*>> ret;
	std::vector<corrmap_3d::align_param*> param_cand;
	int loop = 0, ix, iy, count = 0;
	for (auto itr = p.begin(); itr != p.end(); itr++) {
		if (count % 100000 == 0) {
			printf("\r search correspond triangles %d/%d(%4.1lf%%)", count, p.size(), count*100. / p.size());
		}
		count++;
		ix = ((*itr)->x - xmin) / hash;
		iy = ((*itr)->y - ymin) / hash;
		loop = 1;
		while (true) {
			param_cand.clear();
			for (int iix = ix - loop; iix <= ix + loop; iix++) {
				for (int iiy = iy - loop; iiy <= iy + loop; iiy++) {
					id.first = iix;
					id.second = iiy;
					if (view_center_hash.count(id) != 0) {
						auto range = view_center_hash.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							param_cand.push_back(res->second);
						}
					}
				}
			}
			if (param_cand.size() > 2)break;
			loop++;
		}
		corrmap_3d::align_param2* param2 = search_param(param_cand, *itr, triangles);
		ret.push_back(std::make_pair((*itr), param2));
	}
	printf("\r search correspond triangles %d/%d(%4.1lf%%)\n", count, p.size(), count*100. / p.size());

	return ret;
}
//変換 zshrink補正-->9para変換
void trans_base(std::vector<Point*>&p, corrmap_3d::align_param2 *param) {

	matrix_3D::matrix_33 x_rot_mat(0, param->x_rot), y_rot_mat(1, param->y_rot), z_rot_mat(2, param->z_rot), all_trans(0, 0), shear_mat(0, 0), shrink_mat(0, 0);

	shrink_mat.val[0][0] *= param->x_shrink;
	shrink_mat.val[1][1] *= param->y_shrink;
	//shrink_mat.val[2][2] *= param->z_shrink;
	shear_mat.val[0][1] = param->yx_shear;
	//shear_mat.val[0][2] = param->zx_shear;
	//shear_mat.val[1][2] = param->zy_shear;

	matrix_3D::vector_3D shift, center;
	center.x = param->x;
	center.y = param->y;
	center.z = param->z;
	shift.x = param->dx;
	shift.y = param->dy;
	shift.z = param->dz;

	all_trans.matrix_multiplication(shear_mat);
	all_trans.matrix_multiplication(shrink_mat);
	all_trans.matrix_multiplication(z_rot_mat);
	all_trans.matrix_multiplication(y_rot_mat);
	all_trans.matrix_multiplication(x_rot_mat);

	//all_trans.Print();
	matrix_3D::vector_3D base_p0;
	double base_thick = 210;
	for (auto itr = p.begin(); itr != p.end(); itr++) {
		base_p0.x = (*itr)->x;
		base_p0.y = (*itr)->y;
		base_p0.z = param->z;

		//base_p1.x = (*itr)->x + (*itr)->ax*base_thick;
		//base_p1.y = (*itr)->y + (*itr)->ay*base_thick;
		////角度shrink分はここでかける
		//base_p1.z = param->z + base_thick / param->z_shrink;

		//視野中心を原点に移動
		//base_p0 = matrix_3D::addition(base_p0, matrix_3D::const_multiple(center, -1));
		//base_p1 = matrix_3D::addition(base_p1, matrix_3D::const_multiple(center, -1));

		//変換の実行
		base_p0.matrix_multiplication(all_trans);
		base_p0 = matrix_3D::addition(base_p0, shift);
		//base_p1.matrix_multiplication(all_trans);
		//base_p1 = matrix_3D::addition(base_p1, shift);

		//原点をもとに戻す
		//base_p0 = matrix_3D::addition(base_p0, center);
		//base_p1 = matrix_3D::addition(base_p1, center);

		(*itr)->x = base_p0.x;
		(*itr)->y = base_p0.y;
		(*itr)->z = base_p0.z;

		//printf("ax:%.4lf --> %.4lf\n", (*itr)->ax, (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z));
		//printf("ay:%.4lf --> %.4lf\n", (*itr)->ay, (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z));

		//(*itr)->ax = (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z) + param->zx_shear;
		//(*itr)->ay = (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z) + param->zy_shear;

	}
}
void trans_base_all(std::vector < std::pair<Point*, corrmap_3d::align_param2*>>&track_pair) {
	std::map<std::tuple<int, int, int>, corrmap_3d::align_param2*> param_map;
	std::multimap<std::tuple<int, int, int>, Point*>base_map;
	std::tuple<int, int, int>id;
	//三角形ごとにbasetrackをまとめる
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		std::get<0>(id) = itr->second->corr_p[0]->id;
		std::get<1>(id) = itr->second->corr_p[1]->id;
		std::get<2>(id) = itr->second->corr_p[2]->id;
		param_map.insert(std::make_pair(id, itr->second));
		base_map.insert(std::make_pair(id, itr->first));
	}


	//ここで三角形ごとに変換
	int count = 0;
	std::vector<Point*> t_base;
	for (auto itr = param_map.begin(); itr != param_map.end(); itr++) {
		if (count % 1000 == 0) {
			printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)", count, param_map.size(), count*100. / param_map.size());
		}
		count++;

		t_base.clear();

		if (base_map.count(itr->first) == 0)continue;
		auto range = base_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			t_base.push_back(res->second);
		}
		trans_base(t_base, itr->second);

	}
	printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)\n", count, param_map.size(), count*100. / param_map.size());

}
