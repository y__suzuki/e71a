#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <filesystem>
#include <set>

std::vector<mfile0::M_Base> pickup_base(std::vector<mfile0::M_Chain> &c);
void outputfile(std::string filename, std::vector<mfile0::M_Base>&base);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::vector<mfile0::M_Base> base = pickup_base(m.chains);

	outputfile(file_out_mfile, base);


}
std::vector<mfile0::M_Base> pickup_base(std::vector<mfile0::M_Chain> &c) {
	std::vector<mfile0::M_Base> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->chain_id != 0)continue;
		ret.push_back(*itr->basetracks.rbegin());


	}
	fprintf(stderr,"track num=%d\n", ret.size());
	return ret;
}

void outputfile(std::string filename, std::vector<mfile0::M_Base>&base) {

	std::ofstream ofs(filename);
	for (auto& b : base) {
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << b.group_id << " "
			<< std::setw(4) << std::setprecision(0) << b.pos / 10 << " "
			<< std::setw(12) << std::setprecision(0) << b.rawid << " "
			<< std::setw(10) << std::setprecision(0) << b.ph << " "
			<< std::setw(7) << std::setprecision(4) << b.ax << " "
			<< std::setw(7) << std::setprecision(4) << b.ay << " "
			<< std::setw(8) << std::setprecision(1) << b.x << " "
			<< std::setw(8) << std::setprecision(1) << b.y << " "
			<< std::setw(8) << std::setprecision(1) << b.z << std::endl;
	}

}