#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include <vector>

void mfile_read_write_bin(std::string file_in_mfile, std::string file_out_group);
void wrtie_groupinf(std::ofstream &ofs, std::vector< std::vector< mfile1::MFileBase>> &all_basetracks);

bool sort_base(const mfile1::MFileBase& left, mfile1::MFileBase&right) {
	return left.pos < right.pos;
}
void main(int argc, char **argv)
{
	if (argc != 3) {
		fprintf(stderr, "usage : prg_name [input m-file-bin] [output group]\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_group = argv[2];

	mfile_read_write_bin(file_in_mfile, file_out_group);

}
void mfile_read_write_bin(std::string file_in_mfile, std::string file_out_group) {

	std::ofstream ofs(file_out_group);
	std::ifstream ifs(file_in_mfile, std::ios::binary);

	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }

	std::vector< mfile1::MFileChain> group;
	std::vector< std::vector< mfile1::MFileBase>> all_basetracks;
	int64_t gid = -1;
	for (int64_t c = 0; c < mfile.info_header.Nchain; c++) {
		//一旦chainを読む
		mfile1::MFileChain chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }

		std::vector< mfile1::MFileBase> basetracks;
		basetracks.reserve(chain.nseg);
		//Chain内のbasetrackの読み込み
		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			basetracks.emplace_back(base);
		}

		//最初のchain
		if (c==0) {

		}
		//最後のchain
		else if (c+1== mfile.info_header.Nchain) {

		}

		//basetrack,chainをpush back
		all_basetracks.emplace_back(basetracks);
		group.emplace_back(chain);

	}

}
void wrtie_groupinf(std::ofstream &ofs,std::vector< std::vector< mfile1::MFileBase>> &all_basetracks) {

}
