#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"PID_correct.lib")
#include <PID_correct.h>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <iostream>
#include <set>
#include <omp.h>
#include <filesystem>
#include <sstream>
class Fit_result {
public:
	double angle_min,angle_max;
	std::pair<double, double> fit_param;
};
Fit_result Calc_average(std::vector<Fit_result>&fit_v);
std::multimap < std::tuple<int, int, int>, Fit_result> read_fit_result(std::string filename);
std::multimap < std::tuple<int, int, int>, Fit_result>Calc_mean_pos(std::multimap < std::tuple<int, int, int>, Fit_result>&fit_res);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg in-file out-file\n");
		exit(1);
	}
	std::string file_in_fit = argv[1];
	std::string file_out = argv[2];

	std::multimap <std::tuple<int, int, int>, Fit_result >fit_res_map = read_fit_result(file_in_fit);
	fit_res_map = Calc_mean_pos(fit_res_map);
	std::ofstream ofs(file_out);
	for (auto itr = fit_res_map.begin(); itr != fit_res_map.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << std::get<0>(itr->first) << " "
			<< std::setw(3) << std::setprecision(0) << std::get<1>(itr->first) << " "
			<< std::setw(3) << std::setprecision(0) << std::get<2>(itr->first) << " "
			<< std::setw(4) << std::setprecision(1) << itr->second.angle_min << " "
			<< std::setw(4) << std::setprecision(1) << itr->second.angle_max << " "
			<< std::setw(6) << std::setprecision(3) << (itr->second.angle_max + itr->second.angle_min) / 2 << " "
			<< std::setw(6) << std::setprecision(3) << (itr->second.angle_max - itr->second.angle_min) / 2 << " "
			<< std::setw(6) << std::setprecision(1) << itr->second.fit_param.first << " "
			<< std::setw(6) << std::setprecision(1) << itr->second.fit_param.second << std::endl;
	}


}


std::multimap < std::tuple<int, int, int>, Fit_result> read_fit_result(std::string filename) {
	int pos, trackingid, sensorid;
	double angle_min, angle_max, mean, sigma;
	std::multimap < std::tuple<int, int, int>, Fit_result> ret;
	Fit_result res;
	std::ifstream ifs(filename.c_str());
	std::string str;
	int count = 0;
	while (std::getline(ifs, str))
	{
		sscanf(str.c_str(), "%d %d %d %lf %lf %lf %lf", &pos, &trackingid, &sensorid, &angle_min, &angle_max, &mean, &sigma);
		res.angle_min = angle_min;
		res.angle_max = angle_max;
		res.fit_param.first = mean;
		res.fit_param.second = sigma;
		ret.insert(std::make_pair(std::make_tuple(pos, trackingid, sensorid), res));
		count++;
		if (count % 10000 == 0) {
			fprintf(stderr, "\r Fill ---> %8d", count);
		}
	}
	fprintf(stderr, "\r Fill ---> %8d   finished\n", count);
	return ret;
}
//posをまとめる
std::multimap < std::tuple<int,int, int>, Fit_result>Calc_mean_pos(std::multimap < std::tuple<int, int, int>, Fit_result>&fit_res) {
	std::multimap < std::tuple<int, int,int>, Fit_result> res_tmp;
	int angle_id;
	std::tuple<int, int, int> id;
	for (auto itr = fit_res.begin(); itr != fit_res.end(); itr++) {
		angle_id = (itr->second.angle_min + itr->second.angle_max) / 2 * 10;
		std::get<0>(id) = std::get<1>(itr->first);
		std::get<1>(id) = std::get<2>(itr->first);
		std::get<2>(id) = angle_id;
		res_tmp.insert(std::make_pair(id, itr->second));
	}
	int count;
	std::multimap < std::tuple<int, int, int>, Fit_result> ret;
	for (auto itr = res_tmp.begin(); itr != res_tmp.end(); itr++) {
		count = res_tmp.count(itr->first);

		auto range = res_tmp.equal_range(itr->first);
		std::vector<Fit_result> fit_v;
		for (auto res = range.first; res != range.second; res++) {
			fit_v.push_back(res->second);
		}
		ret.insert(std::make_pair(itr->first, Calc_average(fit_v)));
		itr = std::next(itr, count - 1);
	}
	return ret;
}
Fit_result Calc_average(std::vector<Fit_result>&fit_v) {
	Fit_result res = *fit_v.begin();
	res.fit_param.first = 0;
	res.fit_param.second = 0;
	int count = 0;
	for (auto itr = fit_v.begin(); itr != fit_v.end(); itr++) {
		res.fit_param.first += itr->fit_param.first;
		res.fit_param.second += itr->fit_param.second;
		count++;
	}

	res.fit_param.first = res.fit_param.first / count;
	res.fit_param.second = res.fit_param.second / count;
	return res;
}