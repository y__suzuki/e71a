#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
std::vector<netscan::base_track_t>id_matching(std::vector<netscan::base_track_t> base1, std::vector<netscan::base_track_t> base2);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-bvxx1 in-bvxx2 pl zone out-bvxx\n");
		exit(1);
	}
	std::string file_in_base1 = argv[1];
	std::string file_in_base2 = argv[2];
	int pl = std::stoi(argv[3]);
	int zone = std::stoi(argv[4]);
	std::string file_out_base = argv[5];


	std::vector<netscan::base_track_t> base1, base2;
	netscan::read_basetrack_extension(file_in_base1, base1, pl, zone);
	netscan::read_basetrack_extension(file_in_base2, base2, pl, zone);
	//base = RankingCut_lateral(base);
	std::vector<netscan::base_track_t> base=id_matching(base1, base2);
	netscan::write_basetrack_vxx(file_out_base, base, pl, zone);

}
std::vector<netscan::base_track_t>id_matching(std::vector<netscan::base_track_t> base1, std::vector<netscan::base_track_t> base2) {
	std::vector<netscan::base_track_t> ret;
	std::map<std::pair<int, int>, netscan::base_track_t *>base_map;
	for (auto itr = base2.begin(); itr!= base2.end(); itr++) {
		base_map.insert(std::make_pair(std::make_pair(itr->m[0].rawid, itr->m[1].rawid), &(*itr)));
	}
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (base_map.count(std::make_pair(itr->m[0].rawid, itr->m[1].rawid)) == 0) {
			ret.push_back(*itr);
		}
	}
	return ret;

}