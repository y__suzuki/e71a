#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void Check_stop_chain(std::vector<Momentum_recon::Event_information>&momch, std::map<double, double>&stop_vph);
std::map<double, double> Read_proton_stop_vph(std::string filename);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_in_proton_vph = argv[2];
	std::string file_out_momch = argv[3];

	std::map<double, double> stop_vph_map = Read_proton_stop_vph(file_in_proton_vph);
	for (auto itr = stop_vph_map.begin(); itr != stop_vph_map.end(); itr++) {
		printf("%.1lf %.2lf\n", itr->first, itr->second);
	}

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	Check_stop_chain(momch, stop_vph_map);
	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);

}

std::map<double, double> Read_proton_stop_vph(std::string filename) {
	std::ifstream ifs(filename);
	double ang_min, ang_max, pb_min, pb_max, vph_mean, vph_mean_error[2], vph_sigma, vph_sigma_error[2];

	std::map<double, double> ret;
	int cnt = 0;
	while (ifs >> ang_min >> ang_max >> pb_min >> pb_max
		>> vph_mean >> vph_mean_error[0] >> vph_mean_error[1]
		>> vph_sigma >> vph_sigma_error[0] >> vph_sigma_error[1]) {
		if (ang_max > 3)continue;
		auto res = ret.insert(std::make_pair(ang_max, vph_mean - vph_sigma * 3));
		if (cnt == 1) {
			res.first->second = vph_mean - vph_sigma * 1.5;
		}
		else if (res.second) {
			cnt = 0;
		}
		cnt++;
	}
	return ret;

}

void Check_stop_chain(std::vector<Momentum_recon::Event_information>&momch, std::map<double, double>&stop_vph) {
	for (auto &ev : momch) {
		int vertex_pl = 0;
		for (auto &c : ev.chains) {
			if (c.chainid == 0) {
				vertex_pl = c.base.rbegin()->pl;
			}
		}
		int count = 0;
		for (auto &c : ev.chains) {
			//������
			if (c.base.rbegin()->pl <= vertex_pl) {
				c.direction = 1;
				//edge out
				if (c.base.begin()->x < 20000) {
					c.stop_flg = 0; continue;
				}
				if (c.base.begin()->x > 230000) {
					c.stop_flg = 0; continue;
				}
				if (c.base.begin()->y < 20000) {
					c.stop_flg = 0; continue;
				}
				if (c.base.begin()->y > 230000) {
					c.stop_flg = 0; continue;
				}
				if (c.base.begin()->pl <= 4) {
					c.stop_flg = 0; continue;
				}
				double angle = sqrt(pow(c.base.begin()->ax, 2) + pow(c.base.begin()->ay, 2));
				double vph_ave = 0;
				for (int i = 0; i < 2; i++) {
					vph_ave += c.base[i].m[0].ph % 10000;
					vph_ave += c.base[i].m[1].ph % 10000;
				}
				vph_ave /= 4;
				auto res = stop_vph.upper_bound(angle);
				if (res == stop_vph.end())res = std::next(res, -1);
				//stop����
				if (res->second > vph_ave) {
					c.stop_flg = -1;
				}
				else {
					c.stop_flg = 1;
				}
			}
			//�㗬��
			else if (c.base.begin()->pl > vertex_pl) {
				c.direction = -1;

				//edge out
				if (c.base.rbegin()->x < 20000) {
					c.stop_flg = 0; continue;
				}
				if (c.base.rbegin()->x > 230000) {
					c.stop_flg = 0; continue;
				}
				if (c.base.rbegin()->y < 20000) {
					c.stop_flg = 0; continue;
				}
				if (c.base.rbegin()->y > 230000) {
					c.stop_flg = 0; continue;
				}
				if (c.base.rbegin()->pl <= 4) {
					c.stop_flg = 0; continue;
				}
				double angle = sqrt(pow(c.base.rbegin()->ax, 2) + pow(c.base.rbegin()->ay, 2));
				double vph_ave = 0;
				for (int i = 0; i < 2; i++) {
					vph_ave += c.base[c.base.size() - i - 1].m[0].ph % 10000;
					vph_ave += c.base[c.base.size() - i - 1].m[1].ph % 10000;
				}
				vph_ave /= 4;
				auto res = stop_vph.upper_bound(angle);
				if (res == stop_vph.end())res = std::next(res, -1);
				//stop����
				if (res->second > vph_ave) {
					c.stop_flg = -1;
				}
				else {
					c.stop_flg = 1;
				}
			}
		}
	}
}