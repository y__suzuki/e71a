#include <vector>
#include <string>
#include <fstream>

struct Prediction {
	int PL, flg, rawid;
	double ax, ay, x, y;
}; 
std::vector<Prediction> read_txt_prediction(std::string filename);


void main(int argc, char**argv) {
	std::string filename = "I:\\NINJA\\E71a\\ECC5\\Area0\\0\\mfile2\\pred.txt";
	read_txt_prediction(filename);
	system("pause");
}
std::vector<Prediction> read_txt_prediction(std::string filename) {
	std::vector<Prediction> ret;
	std::ifstream ifs(filename);
	Prediction pred;
	uint64_t count = 0;
	while (ifs >> pred.PL >> pred.ax >> pred.ay >> pred.x >> pred.y >> pred.flg) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r read prediction %d", count);
		}
		count++;
		pred.rawid = 0;
		ret.push_back(pred);
	}
	fprintf(stderr, "\r read prediction %d fin\n", count);
	fprintf(stderr, "num prediction: %lld \n", ret.size());
	return ret;
}