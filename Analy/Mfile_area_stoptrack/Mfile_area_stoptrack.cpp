#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>

class Mfile_Area {
public:
	std::map<int, double> x_min, y_min, x_max, y_max, z;
};
class Stop_track {
public:
	int64_t chainid;
	int pl, id;
	double x, y, z, ax, ay;
};
std::vector<Stop_track> mfile_read_bin(std::string file_in_mfile);
void output_edgetrack(std::string filename, std::vector<Stop_track>&t);

void main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg in-mfile out-stop\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_area = argv[2];

	std::vector<Stop_track> stop = mfile_read_bin(file_in_mfile);
	output_edgetrack(file_out_area, stop);
}
std::vector<Stop_track> mfile_read_bin(std::string file_in_mfile) {

	std::vector<Stop_track> ret;
	std::ifstream ifs(file_in_mfile, std::ios::binary);


	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }
	//Mfile headerの書き込み
	//mfile info headerの書き込み

	mfile1::MFileChain w_chain;
	std::vector< mfile1::MFileBase> w_base;
	uint64_t count = 0;
	for (int64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		//一旦chainを読む
		mfile1::MFileChain chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }

		std::vector< mfile1::MFileBase> basetracks;
		basetracks.reserve(chain.nseg);

		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			basetracks.push_back(base);
		}

		if (chain.nseg > 10) {

			Stop_track up, down;
			down.chainid = chain.chain_id;
			down.pl = basetracks.begin()->pos / 10;
			down.x = basetracks.begin()->x;
			down.y = basetracks.begin()->y;
			down.z = basetracks.begin()->z;
			down.ax = basetracks.begin()->ax;
			down.ay = basetracks.begin()->ay;
			down.id = 0;
			up.chainid = chain.chain_id;
			up.pl = basetracks.rbegin()->pos / 10;
			up.x = basetracks.rbegin()->x;
			up.y = basetracks.rbegin()->y;
			up.z = basetracks.rbegin()->z;
			up.ax = basetracks.rbegin()->ax;
			up.ay = basetracks.rbegin()->ay;
			up.id = 1;
			ret.push_back(up);
			ret.push_back(down);


		}


	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	return ret;






}
void output_edgetrack(std::string filename, std::vector<Stop_track>&t) {
	std::ofstream ofs(filename, std::ios::binary);
	int64_t all = t.size(), cnt = 0;
	for (int64_t i = 0; i < t.size(); i++) {

		if (cnt % 100000 == 0) {
			printf("\r wrtie file %d/%d(%4.1lf%%)", cnt, all, cnt*100. / all);
		}
		cnt++;
		ofs.write((char*)&t[i], sizeof(Stop_track));
	}
	

}



