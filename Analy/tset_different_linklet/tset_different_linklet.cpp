//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>
using namespace l2c;
namespace l2c
{

	bool operator<(const Linklet &left, const Linklet &right) {
		if (left.pos1 != right.pos1)return left.pos1 < right.pos1;
		else if (left.pos2 != right.pos2)return left.pos2 < right.pos2;
		else if (left.id1 != right.id1)return left.id1 < right.id1;
		else {
			return left.id2 < right.id2;
		}
	}
	bool operator==(const Linklet &left, const Linklet &right) {
		return left.pos1 == right.pos1 &&left.pos2 == right.pos2&&left.id1 == right.id1&&left.id2 == right.id2;
	}
}
class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};

l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output);
void read_linklet_list(std::string filename, std::vector<Linklet> &ltlist);
void Linklet_reduction(std::vector<Linklet> &all_ltlist, int pl0, int pl1);
void write_linklet_list(std::string filename, std::vector<Linklet> &ltlist);

std::vector<Linklet> link_compare(std::vector<Linklet> &ltlist1, std::vector<Linklet> &ltlist2);
void wrtie_linklet(std::string filename, std::vector<Linklet> &ltlist);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file-in-linklet-list file-in-linklet-list file-out-linklet-list file-out-linklet-list\n");
		exit(1);
	}
	std::string file_in_link1 = argv[1];
	std::string file_in_link2 = argv[2];
	std::string file_out_link1 = argv[3];
	std::string file_out_link2 = argv[4];

	std::vector<Linklet> ltlist1, ltlist2;
	read_linklet_list(file_in_link1, ltlist1);
	read_linklet_list(file_in_link2, ltlist2);
	printf("all linklet num = %lld\n", ltlist1.size());
	printf("all linklet num = %lld\n", ltlist2.size());

	std::vector<Linklet> ltlist1_only = link_compare(ltlist1, ltlist2);
	std::vector<Linklet> ltlist2_only = link_compare(ltlist2, ltlist1);

	wrtie_linklet(file_out_link1, ltlist1_only);
	wrtie_linklet(file_out_link2, ltlist2_only);

}
int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}

void read_linklet_list(std::string filename, std::vector<Linklet> &ltlist) {
	int64_t link_num = Linklet_header_num(filename);
	ltlist.reserve(link_num);

	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	linklet_header l;
	while (ifs.read((char*)& l, sizeof(linklet_header))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		ltlist.emplace_back(l.pos0, l.pos1, l.raw0, l.raw1);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
}

std::vector<Linklet> link_compare(std::vector<Linklet> &ltlist1, std::vector<Linklet> &ltlist2) {
	printf("vector --> set");
	std::set<Linklet> ltset(ltlist2.begin(), ltlist2.end());
	printf(" fin\n");

	std::vector<Linklet> ret;
	int64_t count = 0, max = ltlist1.size();
	for (auto itr = ltlist1.begin(); itr != ltlist1.end(); itr++) {
		if (count % 1000000 == 0) {
			printf("\r compare linklet %lld/%lld(%4.1lf%%)", count, max, count*100. / max);
		}
		count++;
		if (ltset.count(*itr) == 0) {
			ret.push_back(*itr);
		}
	}
	printf("\r compare linklet %lld/%lld(%4.1lf%%)\n", count, max, count*100. / max);

	printf("only list1 = %d\n", ret.size());
	return ret;
}

void wrtie_linklet(std::string filename, std::vector<Linklet> &ltlist) {
	std::ofstream ofs(filename);
	int max = ltlist.size(), count = 0;;
	for (int i = 0; i < ltlist.size(); i++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "linklet write %d/%d(%4.1lf%%)", count, max, count*100. / max);
		}
		count++;

		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << ltlist[i].pos1 << " "
			<< std::setw(4) << std::setprecision(0) << ltlist[i].pos2 << " "
			<< std::setw(10) << std::setprecision(0) << ltlist[i].id1 << " "
			<< std::setw(10) << std::setprecision(0) << ltlist[i].id2 << std::endl;
	}
	fprintf(stderr, "linklet write %d/%d(%4.1lf%%)\n", count, max, count*100. / max);

}