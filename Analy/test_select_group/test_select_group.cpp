#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <map>

class Track_file {
public:
	int eventid, trackid, pl, rawid;
};
class Group_file :public Track_file {
public:
	int link_num;
	std::vector<std::tuple<int, int, int, int>> linklet;
};

std::vector<Group_file> read_group(std::string filename);
std::map<std::pair<int, int>, int> read_txt(std::string filename);
std::vector<Group_file> group_selection(std::vector<Group_file>&group, std::map<std::pair<int, int>, int>&id_list);

void output_group(std::string filename, std::vector<Group_file>&g);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:in-groupfile in-list output\n");
		exit(1);
	}
	std::string file_in_group = argv[1];
	std::string file_in_list = argv[2];
	std::string file_out = argv[3];

	std::vector<Group_file> g = read_group(file_in_group);
	std::map<std::pair<int, int>, int> id_list = read_txt(file_in_list);

	g = group_selection(g, id_list);
	output_group(file_out, g);

}
std::vector<Group_file> read_group(std::string filename) {
	std::vector<Group_file> ret;

	std::ifstream ifs(filename);
	Group_file g;
	std::tuple<int, int, int, int>link;
	while (ifs >> g.eventid >> g.trackid >> g.pl >> g.rawid >> g.link_num) {
		g.linklet.clear();
		for (int i = 0; i < g.link_num; i++) {
			ifs >> std::get<0>(link) >> std::get<1>(link) >> std::get<2>(link) >> std::get<3>(link);
			g.linklet.push_back(link);
		}
		ret.push_back(g);
	}
	return ret;
}
std::map<std::pair<int, int>, int> read_txt(std::string filename) {
	std::map<std::pair<int, int>, int> ret;
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	std::pair<int, int> id;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		if (str_v.size() != 1 && str_v.size() != 2)continue;
		int id_all = stoi(str_v[0]);
		id.first = id_all / 100000;
		id.second = (id_all % 100000) / 10;
		if (str_v.size() == 1) {
			ret.insert(std::make_pair(id, 0));
		}
		else {
			ret.insert(std::make_pair(id, 1));
		}
	}
	return ret;
}
std::vector<Group_file> group_selection(std::vector<Group_file>&group, std::map<std::pair<int, int>, int>&id_list) {
	std::vector<Group_file> ret;
	std::pair<int, int> id;

	for (auto &g : group) {
		id.first = g.eventid;
		id.second = g.trackid/10;
		if (id_list.count(id) == 1) {
			ret.push_back(g);
		}

	}
	return ret;
}

void output_group(std::string filename, std::vector<Group_file>&g) {
	std::ofstream ofs(filename);
	int count = 0;
	for (auto itr = g.begin(); itr != g.end(); itr++) {
		if (count % 1000 == 0) {
			fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)", count, g.size(), count*100. / g.size());
		}
		count++;

		ofs << std::fixed << std::right
			<< std::setw(10) << std::setprecision(0) << itr->eventid << " "
			<< std::setw(5) << std::setprecision(0) << itr->trackid << " "
			<< std::setw(4) << std::setprecision(0) << itr->pl << " "
			<< std::setw(4) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(10) << std::setprecision(0) << itr->link_num << std::endl;
		if (itr->linklet.size() == 0)continue;
		for (auto itr2 = itr->linklet.begin(); itr2 != itr->linklet.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(5) << std::setprecision(0) << std::get<0>(*itr2) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(*itr2) << std::endl;
		}
	}
	fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)\n", count, g.size(), count*100. / g.size());

}
