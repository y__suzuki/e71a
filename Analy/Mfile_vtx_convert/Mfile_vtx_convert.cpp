#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>

#include <list>
class stop_track {
public:
	int chainid, nseg, npl, pl0, pl1, ph, groupid, rawid;
	double ax, ay, x, y, z;
};
class track_pair {
public:
	int eventid;
	double x, y, z, md;
	stop_track t[2];
};
class track_multi {
public:
	int eventid;
	double x, y, z;
	std::vector< std::pair<double, stop_track>> trk;
	std::vector<track_pair>pair;
};

void read_vtx_file()