#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <filesystem>
#include <set>

class MD_data {
public:
	std::pair<int, int> muon, partner;
	double md, dz0, dz1;
};


bool getFileNames(std::string folderPath, std::vector<std::string> &file_names);
std::vector<mfile0::Mfile> read_mfiles(std::vector<std::string>&file_names, std::set<int> use_eventid);
bool judge_mip(double angle, double vph);
bool judge_mip(mfile0::M_Chain &c);
void mfile_penetrate_rejection(mfile0::Mfile &m);
std::multimap<std::pair<int, int>, MD_data> raed_md_data(std::string filename);
void Divide_event(std::string file_out, mfile0::Mfile&m, std::multimap<std::pair<int, int>, MD_data> &md_list, std::vector<mfile0::Mfile>& water, std::vector < mfile0::Mfile>& iron, std::vector < mfile0::Mfile>& ironECC);
void  out_mfile_all(std::string filename, std::vector<mfile0::Mfile> &m);
void track_count(std::vector<mfile0::Mfile>&m_all);
void mfile_track_divide(mfile0::Mfile &m);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "prg in-file out-file md-dat out-result\n");
		exit(1);
	}
	std::string file_in_mfile_folder = argv[1];
	std::string file_out_mfile_folder = argv[2];
	std::string file_out_result_file = argv[4];
	std::string file_in_md_data = argv[3];
	//std::string file_read = "K:\\NINJA\\E71a\\work\\suzuki\\MuonAnaly\\ECC5\\muon_penetrate_search\\stop_muon\\stop_mu.all";
	std::string file_read = "K:\\NINJA\\E71a\\work\\suzuki\\MuonAnaly\\ECC5\\ver2\\muon_partner_search\\stop_fin.all";

	mfile0::Mfile stop_mu_m;
	mfile0::read_mfile(file_read, stop_mu_m);
	std::set<int> use_eventid;
	for (auto c : stop_mu_m.chains) {
		use_eventid.insert(c.chain_id / 100000);
	}

	std::multimap<std::pair<int, int>, MD_data> md_list = raed_md_data(file_in_md_data);

	std::vector<std::string> m_files;
	getFileNames(file_in_mfile_folder, m_files);
	std::vector<mfile0::Mfile> m = read_mfiles(m_files, use_eventid);
	mfile0::Mfile m_all;
	std::vector<mfile0::Mfile> m_water, m_iron, m_ironECC;
	m_all.header = m[0].header;
	for (auto& ev : m) {
		//printf("event %d t %d --> ", ev.chains.begin()->basetracks.begin()->group_id / 100000, ev.chains.size());
		mfile_penetrate_rejection(ev);
		mfile_track_divide(ev);
		mfile_penetrate_rejection(ev);
		//printf("%d\n", ev.chains.size());

		Divide_event(file_out_result_file, ev, md_list, m_water, m_iron, m_ironECC);



		for (auto c : ev.chains) {
			m_all.chains.push_back(c);
		}
	}
	printf("iron  event = %d\n", m_iron.size());
	printf("water event = %d\n", m_water.size());
	printf("ECC   event = %d\n", m_ironECC.size());

	out_mfile_all("out_iron.all", m_iron);
	out_mfile_all("out_water.all", m_water);
	out_mfile_all("out_iron_ECC.all", m_ironECC);

	printf("Iron\n");
	track_count(m_iron);
	printf("Iron ECC\n");
	track_count(m_ironECC);
	printf("water\n");
	track_count(m_water);

	for (auto ev : m) {
		std::stringstream file_out;
		file_out << file_out_mfile_folder << "\\event_" << std::setw(10) << std::setfill('0') << ev.chains.begin()->basetracks.begin()->group_id / 100000 << ".all";
		mfile0::write_mfile(file_out.str(), ev);
	}
	std::stringstream file_out;
	file_out << file_out_mfile_folder << "\\all_event.all";
	mfile0::write_mfile(file_out.str(), m_all);

}
std::set<int> read_stop_mu(std::string filename) {

	std::set<int> ret;
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		ret.insert(std::stoi(str_v[0]));
	}
	return ret;

}
bool getFileNames(std::string folderPath, std::vector<std::string> &file_names)
{
	using namespace std::filesystem;
	directory_iterator iter(folderPath), end;
	std::error_code err;

	for (; iter != end && !err; iter.increment(err)) {
		const directory_entry entry = *iter;

		file_names.push_back(entry.path().string());
		//printf("%s\n", file_names.back().c_str());
	}

	/* エラー処理 */
	if (err) {
		std::cout << err.value() << std::endl;
		std::cout << err.message() << std::endl;
		return false;
	}
	return true;
}
std::vector<mfile0::Mfile> read_mfiles(std::vector<std::string>&file_names, std::set<int> use_eventid) {
	std::vector<mfile0::Mfile> ret;
	for (auto f : file_names) {

		std::string extension;
		extension = f.substr(f.size() - 3, 3);
		if (extension != "all")continue;
		std::string name_part;
		int last_a_pos = f.find_last_of('\\');

		if (last_a_pos == std::string::npos) {
			last_a_pos = 0;
		}
		int length = (f.size() - 4) - (last_a_pos + 7) ;
		name_part = f.substr(last_a_pos+7, length);
		if (std::all_of(name_part.cbegin(), name_part.cend(), isdigit)) {

			mfile0::Mfile m;
			mfile0::read_mfile(f, m);
			int eventid = m.chains.begin()->chain_id / 100000;
			if (use_eventid.count(eventid) == 1) {
					ret.push_back(m);
			}
		}
	}

	return ret;
}
void mfile_track_divide(mfile0::Mfile &m) {
	std::vector<mfile0::M_Chain> chain;
	int muonid = m.chains.begin()->chain_id;
	int int_pl = m.chains.begin()->basetracks.rbegin()->pos / 10;
	int pl0, pl1;
	for (auto c : m.chains) {
		if (c.chain_id == muonid) {
			chain.push_back(c);
			continue;
		}
		pl0 = c.pos0 / 10;
		pl1 = c.pos1 / 10;
		if (pl0 <= int_pl && pl1 <= int_pl) {
			chain.push_back(c);
		}
		else if (pl0 > int_pl && pl1 > int_pl) {
			chain.push_back(c);
		}
		else {
			mfile0::M_Chain c_up, c_down;
			c_up.chain_id = c.chain_id + 1;
			c_down.chain_id = c.chain_id;
			for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
				if (itr->pos / 10 <= int_pl)c_down.basetracks.push_back(*itr);
				else {
					c_up.basetracks.push_back(*itr);
				}
			}
			c_up.nseg = c_up.basetracks.size();
			c_up.pos0 = c_up.basetracks.begin()->pos;
			c_up.pos1 = c_up.basetracks.rbegin()->pos;
			c_down.nseg = c_down.basetracks.size();
			c_down.pos0 = c_down.basetracks.begin()->pos;
			c_down.pos1 = c_down.basetracks.rbegin()->pos;
			chain.push_back(c_down);
			chain.push_back(c_up);
		}
	}
	m.chains = chain;

}
void mfile_penetrate_rejection(mfile0::Mfile &m) {
	std::vector<mfile0::M_Chain> chain;
	int muonid = m.chains.begin()->chain_id;
	int int_pl = m.chains.begin()->basetracks.rbegin()->pos / 10;
	for (auto c : m.chains) {
		if (c.chain_id == muonid) {
			chain.push_back(c);
			continue;
		}
		//mipの場合
		if (judge_mip(c)) {
			if (c.nseg < 10)continue;
			int pl_min = c.pos0 / 10;
			int pl_max = c.pos1 / 10;
			if ((pl_max-1 - int_pl)*(pl_min - int_pl) > 0) {
				//printf("event %d int PL%d minPL %d maxPL %d\n", muonid / 100000, int_pl, pl_min, pl_max);
				chain.push_back(c);
			}
		}
		else {
			if (c.nseg < 2)continue;
			chain.push_back(c);
		}
	}
	m.chains = chain;
}

bool judge_mip(mfile0::M_Chain &c) {
	double ax_mean = 0, ay_mean = 0, angle, vph = 0;
	int count = 0;
	for (auto b : c.basetracks) {
		ax_mean += b.ax;
		ay_mean += b.ay;
		vph += b.ph % 10000;
		count++;
	}
	ax_mean = ax_mean / count;
	ay_mean = ay_mean / count;
	angle = sqrt(ax_mean*ax_mean + ay_mean * ay_mean);
	vph = vph / count;

	bool res= judge_mip(angle, vph);
	if (!res) {
		//printf("%d %.4lf %.1lf\n", c.chain_id, angle, vph);
	}
	return res;


}
bool judge_mip(double angle, double vph) {
	if (angle < 0.4) {
		return vph < -200 * angle + 200;
	}
	else if (angle < 1.0) {
		return vph < (-100 * angle + 400) / 3;
	}
	return vph < 100;


}


void Divide_event(std::string file_out,mfile0::Mfile&m, std::multimap<std::pair<int, int>, MD_data> &md_list,std::vector<mfile0::Mfile>& water, std::vector < mfile0::Mfile>& iron, std::vector < mfile0::Mfile>& ironECC) {
	mfile0::M_Chain muon;
	int muonid = m.chains.begin()->chain_id;
	int int_pl = m.chains.begin()->basetracks.rbegin()->pos / 10;

	std::map<std::pair<int, int>, MD_data> md_event;
	for (auto c : m.chains) {
		if (c.chain_id == muonid) {
			muon=c;
			for (auto b : c.basetracks) {
				if (md_list.count(std::make_pair(b.pos / 10, b.rawid) )== 0)continue;
				auto range = md_list.equal_range(std::make_pair(b.pos / 10, b.rawid));
				for (auto res = range.first; res != range.second; res++) {
					md_event.insert(std::make_pair(res->second.partner, res->second));
				}
			}
			continue;
		}
	}
	if (int_pl <= 15)ironECC.push_back(m);
	else if (int_pl %2 ==0)iron.push_back(m);
	else {
		water.push_back(m);
	}
	//MDの出力
	//for (auto c : m.chains) {
	//	for (auto b : c.basetracks) {
	//		if (md_event.count(std::make_pair(b.pos / 10, b.rawid)) == 0)continue;
	//		auto res = md_event.find(std::make_pair(b.pos / 10, b.rawid));
	//		std::cout << std::right << std::fixed
	//			<< std::setw(4) << std::setprecision(0) << std::setfill(' ') << res->second.muon.first << " "
	//			<< std::setw(10) << std::setprecision(0) << std::setfill(' ') << res->second.muon.second << " "
	//			<< std::setw(4) << std::setprecision(0) << std::setfill(' ') << res->second.partner.first << " "
	//			<< std::setw(10) << std::setprecision(0) << std::setfill(' ') << res->second.partner.second << " "
	//			<< std::setw(7) << std::setprecision(1) << std::setfill(' ') << res->second.dz0 << " "
	//			<< std::setw(7) << std::setprecision(1) << std::setfill(' ') << res->second.dz1 << " "
	//			<< std::setw(10) << std::setprecision(4) << std::setfill(' ') << res->second.md << std::endl;
	//	}
	//}

}

std::multimap<std::pair<int, int>, MD_data> raed_md_data(std::string filename) {
	std::ifstream ifs(filename);
	std::multimap<std::pair<int, int>, MD_data> ret;

	MD_data md_buf;
	while (ifs >> md_buf.muon.first >> md_buf.muon.second >> md_buf.partner.first >> md_buf.partner.second >> md_buf.dz0 >> md_buf.dz1 >> md_buf.md) {
		if (md_buf.muon.first == md_buf.partner.first&&md_buf.muon.second == md_buf.partner.second)continue;

		ret.insert(std::make_pair(md_buf.muon, md_buf));
	}
	return ret;
}

void  out_mfile_all(std::string filename, std::vector<mfile0::Mfile> &m) {
	if (m.size() == 0)return;
	std::ofstream ofs(filename);
	mfile0::write_mfile_header(ofs, m[0].header, 0);
	for (int i = 0; i < m.size(); i++) {
		for (auto itr = m[i].chains.begin(); itr != m[i].chains.end(); itr++) {
			mfile0::write_mfile_chain(ofs, *itr);
		}
	}

}

void track_count(std::vector<mfile0::Mfile>&m_all) {
	std::map<std::pair<int, int>, int> count_res;
	std::pair<int, int> count;
	for (auto m : m_all) {
		count.first = 0;
		count.second = 0;

		if (m.chains.size() == 1) {
			count.first = 0;
			count.second = 0;
		}
		else {
			for (auto itr = m.chains.begin() + 1; itr != m.chains.end(); itr++) {
				if (judge_mip(*itr))count.first++;
				else {
					count.second++;
				}
			}
		}



		auto res = count_res.insert(std::make_pair(count, 1));
		if (!res.second)res.first->second++;
	}



	for (auto itr = count_res.begin(); itr != count_res.end(); itr++) {
		printf("%d %d %d\n", itr->first.first, itr->first.second, itr->second);
	}


}