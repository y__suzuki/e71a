#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <fstream>
#include <ios>
#include <iomanip> 

class grain_data {
public:
	int layer;
	double x, y, z;
};
class track_data {
public:
	int id, grain_num;
	double ax,ay;
	double dx, dy, dz, gd;
	std::vector<grain_data> g;
};

track_data make_track(double ax, double ay, double dz, int num, double emulsion_thick);
void layer_projection(track_data&t, double emulsion_thick);
void grain_smear_xy(track_data&t);

void output_track(std::string filename, std::vector<track_data> &track);

int main(int argc, char**argv) {
	if (argc != 8) {
		fprintf(stderr, "usage:prg file-out angle-x angle-y dx dy dz gd\n");
	}
	std::string file_out = argv[1];
	double ax = std::stod(argv[2]);
	double ay = std::stod(argv[3]);
	double dx = std::stod(argv[4]);
	double dy = std::stod(argv[5]);
	double dz = std::stod(argv[6]);
	double gd = std::stod(argv[7]);

	int track_num = 10000;
	double emulsion_thick = 60;
	double path_length = emulsion_thick * sqrt(1 + ax * ax + ay * ay);
	std::vector<track_data> track;
	std::random_device seed_gen;
	std::default_random_engine engine(seed_gen());
	std::poisson_distribution<> dist(gd*path_length / 100);
	for (std::size_t n = 0; n < track_num; ++n) {
		// ポワソン分布で乱数を生成する
		int grain_num= dist(engine);

		//直線の上にgrainをのせる
		track_data t = make_track(ax, ay,dz, grain_num, emulsion_thick);
		t.ax = ax;
		t.ay = ay;
		t.dx = dx;
		t.dy = dy;
		t.dz = dz;
		t.gd = gd;
		t.id = n;
		t.grain_num = grain_num;

		////grainを16層の断層画像にprojection
		layer_projection(t, emulsion_thick);
		////x,y方向にsmear
		grain_smear_xy(t);
		track.push_back(t);
	}

	output_track(file_out, track);


}
track_data make_track(double ax, double ay,double dz, int num,double emulsion_thick) {
	track_data ret;
	double ini_x = 0, ini_y = 0, ini_z = 0, end_z = emulsion_thick;

	ret.ax = 0;
	ret.ay = 0;
	ret.dx = 0;
	ret.dy = 0;
	ret.dz = 0;
	ret.gd = 0;
	ret.id = 0;
	ret.grain_num = 0;


	std::random_device rnd;     // 非決定的な乱数生成器を生成
	std::mt19937 mt(rnd());     //  メルセンヌ・ツイスタの32ビット版、引数は初期シード値
	std::uniform_real_distribution<> rand(ini_z-dz, end_z+dz);        // [0, 99] 範囲の一様乱数
	for (int i = 0; i < num; ++i) {

		double grain_z = rand(mt);

		double grain_x = ini_x + ax * (grain_z - ini_z );
		double grain_y = ini_y + ay * (grain_z - ini_z );
		grain_data g;
		g.x = grain_x;
		g.y = grain_y;
		g.z = grain_z;
		g.layer = 0;
		ret.g.push_back(g);
	}

	return ret;
}

void layer_projection(track_data&t,  double emulsion_thick) {
	std::vector<grain_data> g;
	for (int i = 0; i < 16; i++) {
		double layer_z = i * emulsion_thick / 15;
		for (auto itr = t.g.begin(); itr != t.g.end(); itr++) {
			if (fabs(layer_z - itr->z) < t.dz) {
				grain_data grain;
				grain.x = itr->x;
				grain.y = itr->y;
				grain.z = layer_z;
				grain.layer = i;
				g.push_back(grain);
			}
		}
	}

	std::swap(t.g, g);
}

void grain_smear_xy(track_data&t) {
	std::random_device rnd_x,rnd_y;     // 非決定的な乱数生成器でシード生成機を生成
	std::mt19937 mt_x(rnd_x()); //  メルセンヌツイスターの32ビット版、引数は初期シード
	std::mt19937 mt_y(rnd_y()); //  メルセンヌツイスターの32ビット版、引数は初期シード
	std::normal_distribution<> norm_x(0, t.dx);      // 平均50, 分散値10の正規分布
	std::normal_distribution<> norm_y(0, t.dy);      // 平均50, 分散値10の正規分布
	//std::ofstream ofs("xy_smear.txt", std::ios::app);
	for (auto itr = t.g.begin(); itr != t.g.end(); itr++) {
		//double x_tmp = itr->x;
		//double y_tmp = itr->y;
		itr->x += norm_x(mt_x);
		itr->y += norm_y(mt_y);
		//ofs << itr->x - x_tmp << " " << itr->y - y_tmp << std::endl;
	}
}

void output_track(std::string filename, std::vector<track_data> &track) {
	std::ofstream ofs(filename);
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->id << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(4) << std::setprecision(1) << itr->gd << " "
			<< std::setw(4) << std::setprecision(0) << itr->grain_num << " "
			<< std::setw(7) << std::setprecision(4) << itr->dx << " "
			<< std::setw(7) << std::setprecision(4) << itr->dy << " "
			<< std::setw(7) << std::setprecision(4) << itr->dz << " "
			<< std::setw(4) << std::setprecision(0) << itr->g.size() << std::endl;
		for (auto itr2 = itr->g.begin(); itr2 != itr->g.end(); itr2++) {
			ofs << std::right << std::fixed
				<< std::setw(7) << std::setprecision(2) << itr2->x << " "
				<< std::setw(7) << std::setprecision(2) << itr2->y << " "
				<< std::setw(7) << std::setprecision(2) << itr2->z << " "
				<< std::setw(4) << std::setprecision(0) << itr2->layer << std::endl;
		}


	}


}