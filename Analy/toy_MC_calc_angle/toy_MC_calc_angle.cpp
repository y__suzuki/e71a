#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <fstream>
#include <ios>
#include <iomanip> 


class grain_data {
public:
	int layer;
	double x, y, z;
};
class track_data {
public:
	int id, grain_num;
	double ax, ay,ax_measure,ay_measure;
	double dx, dy, dz, gd;
	std::vector<grain_data> g;
};

std::vector<track_data> read_file(std::string filename);
void output_track(std::string filename, std::vector<track_data> &track);
void Calc_angle_x(track_data &t);
void Calc_angle_y(track_data &t);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "uage:prg input result\n");
		exit(1);
	}
	std::string file_in_track = argv[1];
	std::string file_out_track = argv[2];

	std::vector<track_data>track = read_file(file_in_track);
	//printf("read track fin\n");

	for (int i = 0; i < track.size(); i++) {

		Calc_angle_x(track[i]);
		Calc_angle_y(track[i]);
		//printf("%.4lf %.4lf\n", track[i].ax_measure, track[i].ay_measure);
	}
	output_track(file_out_track, track);

}

std::vector<track_data> read_file(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<track_data> ret;

	int id, grain_num, hit_num;
	double ax, ay, gd, dx, dy, dz;

	int g_layer;
	double g_x, g_y, g_z;
	while (ifs >> id >> ax >> ay >> gd >> grain_num >> dx >> dy >> dz >> hit_num) {
		track_data t;
		t.ax = ax;
		t.ay = ay;
		t.ax_measure = 0;
		t.ay_measure = 0;
		t.dx = dx;
		t.dy= dy;
		t.dz = dz;
		t.gd = gd;
		t.grain_num = grain_num;
		t.id = id;
		for (int i = 0; i < hit_num; i++) {
			ifs >> g_x >> g_y >> g_z >> g_layer;
			grain_data g;
			g.layer = g_layer;
			g.x = g_x;
			g.y = g_y;
			g.z = g_z;
			t.g.push_back(g);
		}
		ret.push_back(t);
	}
	return ret;

}

void Calc_angle_x(track_data &t) {

	double x, y, xy, xx, n;
	x = 0;
	y = 0;
	xy = 0;
	xx = 0;
	n = 0;
	for (auto itr = t.g.begin(); itr != t.g.end(); itr++) {
		x += itr->z;
		y += itr->x;
		xy += itr->z*itr->x;
		xx += itr->z*itr->z;
		n += 1;
	}
	double denominator = 0;
	denominator = n * xx - x * x;

	double ax = (n*xy - x * y) / denominator;
	t.ax_measure = ax;


}

void Calc_angle_y(track_data &t) {

	double x, y, xy, xx, n;
	x = 0;
	y = 0;
	xy = 0;
	xx = 0;
	n = 0;
	for (auto itr = t.g.begin(); itr != t.g.end(); itr++) {
		x += itr->z;
		y += itr->y;
		xy += itr->z*itr->y;
		xx += itr->z*itr->z;
		n += 1;
	}
	double denominator = 0;
	denominator = n * xx - x * x;

	double ay = (n*xy - x * y) / denominator;
	t.ay_measure = ay;


}
void output_track(std::string filename, std::vector<track_data> &track) {
	std::ofstream ofs(filename);
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->id << " "
			<< std::setw(7) << std::setprecision(6) << itr->ax << " "
			<< std::setw(7) << std::setprecision(6) << itr->ay << " "
			<< std::setw(7) << std::setprecision(6) << itr->ax_measure << " "
			<< std::setw(7) << std::setprecision(6) << itr->ay_measure << " "
			<< std::setw(4) << std::setprecision(1) << itr->gd << " "
			<< std::setw(4) << std::setprecision(0) << itr->grain_num << " "
			<< std::setw(7) << std::setprecision(4) << itr->dx << " "
			<< std::setw(7) << std::setprecision(4) << itr->dy << " "
			<< std::setw(7) << std::setprecision(4) << itr->dz << " "
			<< std::setw(4) << std::setprecision(0) << itr->g.size() << std::endl;
		for (auto itr2 = itr->g.begin(); itr2 != itr->g.end(); itr2++) {
			ofs << std::right << std::fixed
				<< std::setw(7) << std::setprecision(2) << itr2->x << " "
				<< std::setw(7) << std::setprecision(2) << itr2->y << " "
				<< std::setw(7) << std::setprecision(2) << itr2->z << " "
				<< std::setw(4) << std::setprecision(0) << itr2->layer << std::endl;
		}


	}


}