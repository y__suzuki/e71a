#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>

#include <set>
bool sort_Chain(const mfile0::M_Chain &left, const mfile0::M_Chain &right) {
	return left.basetracks.begin()->group_id < right.basetracks.begin()->group_id;

}
mfile0::Mfile muon_clearning(mfile0::Mfile&m);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:in-mfile out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);
	m = muon_clearning(m);
	mfile0::write_mfile(file_out_mfile, m);

}
mfile0::Mfile muon_clearning(mfile0::Mfile&m) {
	mfile0::Mfile ret;
	ret.header = m.header;

	int count = 0;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (itr->pos0 / 10 <= 4) {
			ret.chains.push_back(*itr);
			count++;
		}
	}
	printf("chain num= %d\n",count );
	sort(ret.chains.begin(), ret.chains.end(),sort_Chain);
	return ret;
}