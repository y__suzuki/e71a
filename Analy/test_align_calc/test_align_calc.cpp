#include <iostream>
#include <vector>
#include <string>

void GaussJorden(double in[4][4], double b[4], double c[4]);

int main() {
	std::pair<double, double> a0, a1, b0, b1;
	a0.first = 7500;
	a0.second = -1250;
	a1.first = 246000;
	a1.second = -1000;

	b0.first = -53.5710;
	b0.second = -41.268;
	b1.first = 184.7245;
	b1.second = -42.0655;

	double in[4][4] = { 
		{a0.first, -1 * a0.second,1,0},
		{a0.second,  a0.first,0,1},
		{a1.first, -1 * a1.second,1,0},
		{a1.second,  a1.first,0,1}
	};
	double b[4] = { b0.first,b0.second,b1.first,b1.second };
	double c[4];

	GaussJorden(in, b, c);
	
	std::pair<double, double> ap,bp;
	ap.first = 59953.3;
	ap.second = 197561.7;

	bp.first = ap.first*c[0] - ap.second*c[1] + c[2];
	bp.second = ap.first*c[1] + ap.second*c[0] + c[3];

	printf("x:%g\n",bp.first);
	printf("y:%g\n", bp.second);

}



void GaussJorden(double in[4][4], double b[4], double c[4]) {


	double a[4][5];
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 5; j++) {
			if (j < 4) {
				a[i][j] = in[i][j];
			}
			else {
				a[i][j] = b[i];
			}
		}
	}
	int N = 4;
	double p, d;         // ピボット係数、ピボット行ｘ係数
	double max, dummy;   // 最大絶対値、入れ替え時ダミー
	int s;

	//元の連立方程式をコンソール出力
   //for (int i = 0; i < N; i++) {
   //	for (int j = 0; j < N; j++)
   //		printf("%+fx%d ", a[i][j], j + 1);
   //	printf("= %+f\n", a[i][N]);
   //}

	for (int k = 0; k < N; k++) {
		// 行入れ替え
		max = 0; s = k;
		for (int j = k; j < N; j++) {
			if (fabs(a[j][k]) > max) {
				max = fabs(a[j][k]);
				s = j;
			}
		}
		if (max == 0) {
			printf("解けない！");
			exit(1);
		}
		for (int j = 0; j <= N; j++) {
			dummy = a[k][j];
			a[k][j] = a[s][j];
			a[s][j] = dummy;
		}

		// ピボット係数
		p = a[k][k];

		// ピボット行を p で除算
		for (int j = k; j < N + 1; j++)
			a[k][j] /= p;

		// ピボット列の掃き出し
		for (int i = 0; i < N; i++) {
			if (i != k) {
				d = a[i][k];
				for (int j = k; j < N + 1; j++)
					a[i][j] -= d * a[k][j];
			}
		}
	}

	// 結果出力
	for (int k = 0; k < N; k++) {
		c[k] = a[k][N];
		//printf("x%d = %f\n", k + 1, a[k][N]);
	}
}

