#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

mfile0::Mfile attach_merge(mfile0::Mfile &muon, std::vector<mfile0::M_Chain> &attach);

bool mfile_penetrate_rejection(mfile0::M_Chain &c, int int_pl);
std::vector<mfile0::M_Chain> mfile_track_divide(mfile0::M_Chain &c, int int_pl, int &chainid);



int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-muon-mfile file-in-attach-mfile file_out_mfile\n");
		exit(1);
	}

	std::string file_in_muon = argv[1];
	std::string file_in_attach = argv[2];
	std::string file_out= argv[3];

	mfile0::Mfile muon, attach;
	mfile0::read_mfile( file_in_muon, muon);
	mfile0::read_mfile(file_in_attach,attach);

	mfile0::Mfile merge = attach_merge(muon, attach.chains);
	
	mfile0::write_mfile(file_out, merge);
}

mfile0::Mfile attach_merge(mfile0::Mfile &muon, std::vector<mfile0::M_Chain> &attach) {
	std::multimap <int,mfile0::M_Chain> group_attach;
	for (auto itr = attach.begin(); itr != attach.end(); itr++) {
		group_attach.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}
	mfile0::Mfile ret;
	int chainid = 0;
	for (auto itr = muon.chains.begin(); itr != muon.chains.end(); itr++) {
		chainid = 0;
		itr->chain_id = chainid;
		ret.chains.push_back(*itr);
		chainid++;
		if (group_attach.count(itr->basetracks.begin()->group_id) == 0) continue;
		auto range = group_attach.equal_range(itr->basetracks.begin()->group_id);
		for(auto res=range.first;res!=range.second;res++){
			if (!mfile_penetrate_rejection(res->second, itr->basetracks.rbegin()->pos / 10))continue;

			std::vector<mfile0::M_Chain> chains_div = mfile_track_divide(res->second, itr->basetracks.rbegin()->pos / 10,chainid);

			for (auto itr2 = chains_div.begin(); itr2 != chains_div.end(); itr2++) {
				if (!mfile_penetrate_rejection(*itr2, itr->basetracks.rbegin()->pos / 10))continue;
				ret.chains.push_back(*itr2);
			}
		}
	}
	ret.header = muon.header;
	return ret;
}


bool judge_mip(double angle, double vph) {
	if (angle < 0.4) {
		return vph < -200 * angle + 200;
	}
	else if (angle < 1.0) {
		return vph < (-100 * angle + 400) / 3;
	}
	return vph < 100;


}
bool judge_mip(mfile0::M_Chain &c) {
	double ax_mean = 0, ay_mean = 0, angle, vph = 0;
	int count = 0;
	for (auto b : c.basetracks) {
		ax_mean += b.ax;
		ay_mean += b.ay;
		vph += b.ph % 10000;
		count++;
	}
	ax_mean = ax_mean / count;
	ay_mean = ay_mean / count;
	angle = sqrt(ax_mean*ax_mean + ay_mean * ay_mean);
	vph = vph / count;

	bool res = judge_mip(angle, vph);
	if (!res) {
		//printf("%d %.4lf %.1lf\n", c.chain_id, angle, vph);
	}
	return res;


}

bool mfile_penetrate_rejection(mfile0::M_Chain &c, int int_pl) {

	if (judge_mip(c)) {
		if (c.nseg < 10)return false;
		int pl_min = c.pos0 / 10;
		int pl_max = c.pos1 / 10;
		if ((pl_max - 1 - int_pl)*(pl_min - int_pl) > 0) {
			//printf("event %d int PL%d minPL %d maxPL %d\n", muonid / 100000, int_pl, pl_min, pl_max);
			//chain.push_back(c);
			return true;
		}
	}
	else {
		if (c.nseg < 2)return false;
		return true;
		//chain.push_back(c);
	}

	return false;
}

std::vector<mfile0::M_Chain> mfile_track_divide(mfile0::M_Chain &c, int int_pl,int &chainid) {
	std::vector<mfile0::M_Chain> ret;
	int pl0, pl1;
	pl0 = c.pos0 / 10;
	pl1 = c.pos1 / 10;
	if (pl0 <= int_pl && pl1 <= int_pl) {
		c.chain_id = chainid;
		chainid++;
		ret.push_back(c);
	}
	else if (pl0 > int_pl && pl1 > int_pl) {
		c.chain_id = chainid;
		chainid++;
		ret.push_back(c);
	}
	else {
		mfile0::M_Chain c_up, c_down;
		c_up.chain_id = chainid;
		chainid++;
		c_down.chain_id = chainid;
		chainid++;

		for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
			if (itr->pos / 10 <= int_pl)c_down.basetracks.push_back(*itr);
			else {
				c_up.basetracks.push_back(*itr);
			}
		}
		c_up.nseg = c_up.basetracks.size();
		c_up.pos0 = c_up.basetracks.begin()->pos;
		c_up.pos1 = c_up.basetracks.rbegin()->pos;
		c_down.nseg = c_down.basetracks.size();
		c_down.pos0 = c_down.basetracks.begin()->pos;
		c_down.pos1 = c_down.basetracks.rbegin()->pos;
		ret.push_back(c_down);
		ret.push_back(c_up);
	}
	return ret;
}

