#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>

std::vector<netscan::linklet_t>  linklet_cut(std::vector<netscan::linklet_t> &link);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg input-link output-link\n");
		exit(1);
	}
	std::string file_in_linklet = argv[1];
	std::string file_out_linklet = argv[2];

	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_txt(file_in_linklet, link);

	std::vector<netscan::linklet_t> link_sel = linklet_cut(link);
	netscan::write_linklet_txt(file_out_linklet, link_sel);
}
std::vector<netscan::linklet_t>  linklet_cut(std::vector<netscan::linklet_t> &link) {
	std::vector<netscan::linklet_t> ret;
	double dax, day;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		dax = itr->b[0].ax - itr->b[1].ax;
		day = itr->b[0].ay - itr->b[1].ay;
		//angle 3-0.2,
		//      0-0.02
		//
		//position 3-100 
		//         0-10
		if (fabs(dax) > 0.02 + 0.06*fabs(itr->b[0].ax + itr->b[1].ax) / 2)continue;
		if (fabs(day) > 0.02 + 0.06*fabs(itr->b[0].ay + itr->b[1].ay) / 2)continue;
		if (fabs(itr->dx) > 10 + 30 * fabs(itr->b[0].ax + itr->b[1].ax) / 2)continue;
		if (fabs(itr->dy) > 10 + 30 * fabs(itr->b[0].ay + itr->b[1].ay) / 2)continue;
		ret.push_back(*itr);
	}
	return ret;

}
