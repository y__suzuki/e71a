#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <map>

std::vector<std::pair<mfile0::M_Chain, mfile0::M_Chain>> same_event_pair(std::vector<mfile0::M_Chain>&chains, std::set<std::pair<int, int>> &sel0, std::set<std::pair<int, int>> &sel1);
bool judge_chain_matching(std::vector<mfile0::M_Base>&base0, std::vector<mfile0::M_Base>&base1);
mfile0::Mfile chain_selection(mfile0::Mfile&m, std::set<std::pair<int, int>> &sel);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr,"usage:file-in-mfile file-in-mfile2 file-out-mfile1 file-out-mfile2\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_mfile2 = argv[2];
	std::string file_out_mfile1 = argv[3];
	std::string file_out_mfile2 = argv[4];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::set<std::pair<int, int>> sel0, sel1;
	std::vector<std::pair<mfile0::M_Chain, mfile0::M_Chain>> chains_pair = same_event_pair(m.chains, sel0, sel1);


	mfile0::Mfile m_all, m_sel0, m_sel1;;
	mfile1::read_mfile_extension(file_in_mfile2, m_all);


	m_sel0 = chain_selection(m_all, sel0);
	m_sel1 = chain_selection(m_all, sel1);
	mfile1::write_mfile_extension(file_out_mfile1, m_sel0);
	mfile1::write_mfile_extension(file_out_mfile2, m_sel1);
}

std::vector<std::pair<mfile0::M_Chain, mfile0::M_Chain>> same_event_pair(std::vector<mfile0::M_Chain>&chains, std::set<std::pair<int, int>> &sel0, std::set<std::pair<int, int>> &sel1) {
	std::vector<std::pair<mfile0::M_Chain, mfile0::M_Chain>> ret;
	std::pair<mfile0::M_Chain, mfile0::M_Chain> p;
	int all = 0, pene_miss = 0, group_miss = 0;
	for (int i = 0; i < chains.size(); i++) {
		if (i + 1 == chains.size())continue;
		if (chains[i].basetracks.begin()->group_id!= chains[i+1].basetracks.begin()->group_id)continue;
		if (chains[i].chain_id / 10 != chains[i + 1].chain_id / 10)continue;
		p.first = chains[i];
		p.second = chains[i + 1];
		ret.push_back(p);

		printf("%5d %5d %5d", chains[i].basetracks.begin()->group_id, chains[i].chain_id, chains[i + 1].chain_id);
		all++;
		if (judge_chain_matching(p.first.basetracks, p.second.basetracks)){
			printf(" true\n");
			group_miss++;
			sel0.insert(std::make_pair(chains[i].basetracks.begin()->group_id, chains[i].chain_id/10));
		}
		else {
			printf(" false\n");
			pene_miss++;
			sel1.insert(std::make_pair(chains[i].basetracks.begin()->group_id, chains[i].chain_id / 10));

		}
	}
	printf("all %5d chain selection miss %d penetrate judge %d\n", all, group_miss, pene_miss);
	return ret;

}
bool judge_chain_matching(std::vector<mfile0::M_Base>&base0, std::vector<mfile0::M_Base>&base1) {
	if (base0.size() != base1.size())return false;
	for (int i = 0; i < base0.size(); i++) {
		if (base0[i].pos / 10 != base1[i].pos / 10)return false;
		if (base0[i].rawid != base1[i].rawid)return false;
	}
	return true;
}

mfile0::Mfile chain_selection(mfile0::Mfile&m, std::set<std::pair<int, int>> &sel) {
	mfile0::Mfile ret;
	ret.header = m.header;
	for (auto&c : m.chains) {
		int id = c.basetracks.begin()->group_id / 10;
		int gid = id / 10000;
		int cid=id% 10000;
		if (sel.count(std::make_pair(gid, cid)) == 1) {
			ret.chains.push_back(c);
		}
		//printf("gid %d cid %d\n", gid, cid);

	}
	return ret;
}