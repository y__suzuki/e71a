#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <omp.h>
using namespace l2c;

class Chain_baselist {
public:
	int64_t groupid, trackid;
	std::pair<int32_t, int64_t> target_track;
	std::set<std::pair<int32_t, int64_t>> btset;
	std::set<std::tuple<int, int, int, int>>ltlist;
	std::vector<int32_t> usepos;
	int chain_num;
	std::vector<Linklet> make_link_list();
	void set_usepos();
};

void Printbat(FILE *fp, int eventid, int cnt, int num);
std::vector < Chain_baselist > read_linklet_list2(std::string filename);
void out_linklet_list(std::string filename, std::vector < Chain_baselist >&c);
std::map<int, std::vector<Chain_baselist>> divide_group(std::vector < Chain_baselist >&chain);
FILE* fileopen(std::string filename, int num);

int main(int argc,char**argv) {
	if (argc != 3) {
		fprintf(stderr, "filename\n");
		exit(1);
	}
	std::string file_in_link_list = argv[1];
	std::string file_out = argv[2];
	std::vector<Chain_baselist> chain_list = read_linklet_list2(file_in_link_list);
	std::map<int, std::vector<Chain_baselist>> chain_map = divide_group(chain_list);

	FILE *fp = NULL;       // 出力ストリーム

	int cnt = 0;
	int num = 0;
	int interval = chain_map.size() / 4;
	for (auto &ev : chain_map) {
		if (cnt % interval == 0) {
			if (num != 0)fclose(fp);
			num += 1;
			fp = fileopen(file_out, num);

		}
		fprintf(stderr, "\r now %d/%d", cnt, chain_map.size());
		cnt++;
		//if (cnt > 1)continue;
		int eventid = ev.first;
		std::vector < Chain_baselist > ch_out=ev.second;
		//ch_out.push_back(ev);
		char outfile[256];
		sprintf(outfile, "track_group1_%010d.txt", eventid);
		out_linklet_list(outfile, ch_out);

		Printbat(fp, eventid,cnt,num);

	}
	fprintf(stderr, "\r now %d/%d\n", cnt, chain_map.size());
	fclose(fp);          // ファイルをクローズ(閉じる)
	return 0;

}
FILE* fileopen(std::string filename,int num) {
	FILE *fp;         // 出力ストリーム
	char file_out[256];
	sprintf(file_out, "%s_%03d.bat", filename.c_str(), num);
	fp = fopen(file_out, "w");  // ファイルを書き込み用にオープン(開く)
	if (fp == NULL) {          // オープンに失敗した場合
		printf("cannot open\n");         // エラーメッセージを出して
		exit(1);                         // 異常終了
	}
	fprintf(fp, "set path_ECC=I:\\NINJA\\E71a\\ECC5\n");
	fprintf(fp, "set path_ECC_Area=I:\\NINJA\\E71a\\ECC5\\Area0\n");
	fprintf(fp, "if not exist group_conv_done mkdir group_conv_done\n");
	fprintf(fp, "\n");

	return fp;

}
std::vector < Chain_baselist > read_linklet_list2(std::string filename) {
	std::vector < Chain_baselist > ret;
	std::ifstream ifs(filename);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;

	int gid, tid, muon_num, t_pl, t_rawid;
	int64_t link_num;
	int pl, raw;
	double weight;
	std::tuple<int, int, int, int> link;
	int count = 0;
	int pre_group = -1;
	int group_count = 0;
	double read_rate;
	while (ifs >> gid >> tid >> t_pl >> t_rawid >> link_num) {
		if (count % 1000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;

			printf("\r read line %8d group %5d gid=%8d link=%5lld (%4.1lf%%)", count,group_count, gid, link_num, size1 * 100. / size2);
		}
		count++;
		if (pre_group != gid) {
			group_count += 1;
			pre_group = gid;
		}

		Chain_baselist c;
		c.groupid = gid;
		c.trackid = tid;
		c.target_track.first = t_pl;
		c.target_track.second = t_rawid;
		for (int64_t i = 0; i < link_num; i++) {
			ifs >> std::get<0>(link) >> std::get<1>(link) >> std::get<2>(link) >> std::get<3>(link);
			c.btset.insert(std::make_pair(std::get<0>(link), std::get<2>(link)));
			c.btset.insert(std::make_pair(std::get<1>(link), std::get<3>(link)));
			c.ltlist.insert(link);
		}
		ret.push_back(c);

		//if (ret.size() > 10000)break;
	}
	auto size1 = eofpos - begpos;

	printf("\r read line %8d group %5d gid=%8d link=%5lld (%4.1lf%%)\n", count, group_count, gid, link_num, size1 * 100. / size2);

	return ret;

}
std::map<int, std::vector<Chain_baselist>> divide_group(std::vector < Chain_baselist >&chain) {
	std::multimap<int, Chain_baselist*> multi_map_chain;
	int count = 0;

	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r insert multimap ..%d/%d(%4.1lf%%)", count,chain.size(),count*100./chain.size());
		}
		count++;

		multi_map_chain.insert(std::make_pair(itr->groupid, &(*itr)));
	}
	printf("\r insert multimap ..%d/%d(%4.1lf%%)\n", count, chain.size(), count*100. / chain.size());

	count = 0;
	std::map<int, std::vector<Chain_baselist>> ret;
	for (auto itr = multi_map_chain.begin(); itr != multi_map_chain.end();itr++) {
		if (count % 100 == 0) {
			printf("\r group divide ..%d", count);
		}
		count++;

		std::vector<Chain_baselist> c_v;
		auto range = multi_map_chain.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			c_v.push_back(*res->second);
		}
		ret.insert(std::make_pair(itr->first, c_v));

		itr = std::next(itr, multi_map_chain.count(itr->first)-1);
	}
	printf("\r group divide ..%d\n", count);
	return ret;
}
void out_linklet_list(std::string filename, std::vector < Chain_baselist >&c) {
	std::ofstream ofs(filename);

	for (auto itr = c.begin(); itr != c.end(); itr++) {
		ofs << itr->groupid << " " << itr->trackid << " " << itr->target_track.first << " " << itr->target_track.second << " " << itr->ltlist.size() << std::endl;
		for (auto &l : itr->ltlist) {
			ofs << std::get<0>(l) << " " << std::get<1>(l) << " " << std::get<2>(l) << " " << std::get<3>(l) << std::endl;
		}

	}
}

void Printbat(FILE *fp, int eventid,int cnt,int num) {

	fprintf(fp, "title Event %10d\n", eventid);
	fprintf(fp, "set track_group1=track_group1_%010d.txt\n", eventid);
	fprintf(fp, "set track_group2=track_group2_%010d.txt\n", eventid);
	fprintf(fp, "set track_group_conv1=track_group_conv1_%010d.txt\n", eventid);
	fprintf(fp, "set track_group_conv2=track_group_conv2_%010d.txt\n", eventid);
	fprintf(fp, "set track_mfile_all=track.all_%010d\n", eventid);
	fprintf(fp, "set track_mfile_sel1=track_sel1_%010d.all\n", eventid);
	fprintf(fp, "set track_mfile_sel2=track_sel2_%010d.all\n", eventid);
	fprintf(fp, "set track_mfile_sel3=track_sel3_%010d.all\n", eventid);


	fprintf(fp, "M:\\data\\NINJA\\prg\\Chain_convolution_1_v4.exe %%track_group1%% %%track_group_conv1%% 5\n", eventid);
	//fprintf(fp, "M:\\data\\NINJA\\prg\\Chain_convolution_2.exe %%track_group_conv1%% %%path_ECC%% %%track_group_conv2%% 2\n", eventid);
	//fprintf(fp, "M:\\data\\NINJA\\prg\\Chain_convolution_3_partner.exe %%track_group1%% %%track_group_conv2%% %%track_group2%%\n", eventid);
	fprintf(fp, "\n");
	fprintf(fp, "move %%track_group1%% group_conv_done\n");
	fprintf(fp, "move %%track_group_conv1%% group_conv_done\n");
	//fprintf(fp, "move %%track_group_conv2%% group_conv_done\n");
	//fprintf(fp, "move %%track_group2%% group_conv_done\n");
	fprintf(fp, "\n");
	fprintf(fp, "echo fin %6d %010d >> fin_event.txt\n", cnt, eventid);
	fprintf(fp, "echo fin %6d %010d >> fin_event_%03d.txt\n", cnt, eventid, num);
	fprintf(fp, "\n");
	fprintf(fp, "\n");
}