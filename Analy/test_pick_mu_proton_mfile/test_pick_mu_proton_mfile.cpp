#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Event_information> Select_proton(std::vector<Momentum_recon::Event_information>&ev_all);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	momch = Select_proton(momch);
	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);

}
std::vector<Momentum_recon::Event_information> Select_proton(std::vector<Momentum_recon::Event_information>&ev_all) {
	std::vector<Momentum_recon::Event_information> ret;

	for (auto &ev : ev_all) {
		Momentum_recon::Event_information ev_tmp;
		ev_tmp = ev;
		ev_tmp.chains.clear();
		for (auto &c : ev.chains) {
			if (c.particle_flg == 13) {
				ev_tmp.chains.push_back(c);
			}
			else if ( c.particle_flg == 2212) {
				if (c.Get_muon_mcs_pb() > 500)continue;
				int ini_pl = c.base.begin()->pl;
				int end_pl = c.base.rbegin()->pl;
				if (ini_pl <= ev.vertex_pl&&ev.vertex_pl < end_pl) {
					ev_tmp.chains.push_back(c);
				}
			}
		}
		if (ev_tmp.chains.size() > 1) {
			ret.push_back(ev_tmp);
		}
	}
	return ret;
}
