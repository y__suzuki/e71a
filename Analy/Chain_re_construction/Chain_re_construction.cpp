#define _CRT_SECURE_NO_WARNINGS

#include <LibL2c-x.h>
//#include "../LibL2c-x/LibL2c-x.h"
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>

#pragma comment(lib, "LibL2c-x.lib")

using namespace l2c;
void l2c_x(std::set<std::pair<int32_t, int64_t>> btset, std::vector<Linklet> ltlist, std::vector<int32_t> usepos);

int main()
{


}

void l2c_x(std::set<std::pair<int32_t, int64_t>> btset, std::vector<Linklet> ltlist, std::vector<int32_t> usepos) {
	try
	{

		//std::vector<int32_t> usepos = { 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500 };
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = 100000, opt::output_isolated_linklet = true);

		//中身を出力する。
		size_t grsize = cdat.GetNumOfGroups();
		for (size_t grid = 0; grid < grsize; ++grid)
		{
			const Group& gr = cdat.GetGroup(grid);
			size_t chsize = gr.GetNumOfChains();
			if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
			int32_t spl = gr.GetStartPL();
			int32_t epl = gr.GetEndPL();

			//l2c-xのPLは一般に使われるPLとは意味が異なり、useposの中で"何番目のposであるか"を意味する。
			//LibL2c-xもl2c-xの仕様に倣うことにした（ただしl2c-xが1始まりなのに対してこちらは0始まり）。
			//例えば今回は、pos==310〜500の20枚でChain Groupを作成しているが、
			//GetStartPL()の戻り値が"3"であったとすると、これはこのGroupがusepos[3]すなわち380から始まっているという意味になる。

			fprintf(stdout, "Group ID:%-9llu nchain:%-7llu start:%-4d end:%-4d\n", gr.GetID(), chsize, spl, epl);
			if (gr.IsOverUpperLim())
			{
				//upperlimを超過している場合、chainの情報はない。
				//ただしchainの本数はGetNumOfChainsで正しく取得できる。
				//また属すBaseTrackの情報は保持しているので、GetBaseTracksで全BaseTrackを取得できる。
				fprintf(stdout, "over upperlim. nchain:%-9lld\n", chsize);
				continue;
			}
			for (size_t ich = 0; ich < chsize; ++ich)
			{
				Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
				int64_t chid = ch.GetID();
				int32_t nseg = ch.GetNSeg();
				int32_t spl = ch.GetStartPL();
				int32_t epl = ch.GetEndPL();
				fprintf(stdout, "    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d\n", chid, nseg, spl, epl);

				for (size_t pl = 0; pl < possize; ++pl)
				{
					//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
					//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
					BaseTrackID bt = ch.GetBaseTrack(pl);
					if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
					int32_t btpl = bt.GetPL();
					int64_t btid = bt.GetRawID();
					//btplとplは厳密に一致しなければおかしい。
					if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
					if (btset.find(std::make_pair(usepos[btpl], btid)) == btset.end())
					{
						//エラーチェック。
						//Chain内の全てのBaseTrackはbtset（ファイルから読み込まれた全BaseTrack）に必ず含まれているはず。
						//含まれていなければエラー。
						throw std::exception("BaseTrack is not found in btset.");
					}
					fprintf(stdout, "        pl:%-3d pos:%-4d rawid:%-9lld\n", btpl, usepos[pl], btid);
				}
			}

		}
	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}
