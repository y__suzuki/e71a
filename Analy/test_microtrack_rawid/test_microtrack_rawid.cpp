#pragma comment(lib, "VxxReader.lib")
#include <VxxReader.h>
#include <fstream>
#include <iostream>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
std::vector<vxx::micro_track_t> microtrack_selection(std::vector<vxx::micro_track_t> &m, double angmin, double angmax);
void file_out_rawid(std::vector<vxx::micro_track_t> &micro, std::string filename);
std::vector<vxx::micro_track_t> microtrack_selection2(std::vector<vxx::micro_track_t> &m, int maxtrack);

int main(int argc, char**argv) {
	if (argc != 8) {
		fprintf(stderr, "usage:prg file-in-fvxx pos zone angmin angmax maxtrack output-file\n");
		exit(1);
	}
	std::string file_in_fvxx = argv[1];
	int pos = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	double angmin = std::stod(argv[4]);
	double angmax= std::stod(argv[5]);
	int maxtrack= std::stoi(argv[6]);
	std::string file_out_list = argv[7];

	std::vector<vxx::micro_track_t> micro;
	vxx::FvxxReader fr;
	micro = fr.ReadAll(file_in_fvxx, pos, zone);
	micro = microtrack_selection(micro, angmin, angmax);
	micro = microtrack_selection2(micro, maxtrack);
	file_out_rawid(micro, file_out_list);

}
std::vector<vxx::micro_track_t> microtrack_selection(std::vector<vxx::micro_track_t> &m, double angmin, double angmax) {
	std::vector<vxx::micro_track_t> ret;
	double angle;
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angmin <= angle && angle < angmax) {
			ret.push_back(*itr);
		}
	}

	fprintf(stderr, "angle cut %.1lf <= angle < %.1lf : %d -->%d (%4.1lf %%)\n", angmin, angmax, m.size(), ret.size());
	return ret;
}
std::vector<vxx::micro_track_t> microtrack_selection2(std::vector<vxx::micro_track_t> &m, int maxtrack) {
	if (m.size() < maxtrack)return m;
	std::vector<vxx::micro_track_t> ret;
	ret.reserve(maxtrack);
	int denominator = m.size() / maxtrack;

	for (int i = 0; i < m.size();i++) {
		if (i / denominator >= maxtrack)continue;
		if (i%denominator == 0) {
			ret.push_back(m[i]);
		}
	}
	fprintf(stderr,"number of track selection (max track=%d):%d --> %d\n", maxtrack, m.size(), ret.size());
	return ret;
}
void file_out_rawid(std::vector<vxx::micro_track_t> &micro,std::string filename) {
	std::ofstream ofs(filename);
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		ofs << std::right << std::fixed << std::setfill(' ')
			<< std::setw(12) << std::setprecision(0) << itr->rawid << std::endl;
	}
}