#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class eventfile {
public:
	int muon_pl;
	mfile0::M_Base muon;
	std::vector<mfile0::M_Base> partner;

};

inline bool judge_black_base(mfile0::M_Base &b);
std::vector<eventfile> basetrack_extract(std::vector<mfile0::M_Chain>&chain);
void output_track_multi(std::string filename, std::vector<eventfile>&eve);


int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:filename in-mifle\n");
			exit(1);
	}
	std::string file_in_mfile = argv[1];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	std::vector<eventfile> eve = basetrack_extract(m.chains);
	
	output_track_multi("output_multi.txt", eve);

}
std::vector<eventfile> basetrack_extract(std::vector<mfile0::M_Chain>&chain) {
	std::multimap<int, mfile0::M_Chain> event_chain;
	for (auto &c : chain) {
		event_chain.insert(std::make_pair(c.basetracks.begin()->group_id, c));
	}
	std::vector<eventfile> ret;

	for (auto itr = event_chain.begin(); itr != event_chain.end(); itr++) {
		int count = event_chain.count(itr->first);
		eventfile eve;
		auto range = event_chain.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.chain_id == 0) {
				eve.muon = *res->second.basetracks.rbegin();
				eve.muon_pl = res->second.pos1 / 10;
			}
		}
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.chain_id == 0)continue;
			if (res->second.nseg < 2)continue;
			if(res->second.pos1 / 10<=eve.muon_pl){
				mfile0::M_Base b = *res->second.basetracks.rbegin();
				b.flg_i[2] = judge_black_base(b);
				eve.partner.push_back(b);
			}
			else {
				mfile0::M_Base b = *res->second.basetracks.begin();
				b.flg_i[2] = judge_black_base(b);
				eve.partner.push_back(b);
			}
		}
		ret.push_back(eve);

		itr = std::next(itr, count - 1);
	}
	return ret;
}

inline bool judge_black_base(mfile0::M_Base &b) {
	double angle = sqrt(b.ax*b.ax + b.ay*b.ay);
	int vph = b.ph % 10000;
	if (angle < 0.4) {
		return vph > -200 * angle + 200;
	}
	else if (angle < 1.0) {
		return vph > (-100 * angle + 400) / 3;
	}
	return vph > 100;


}


void output_track_multi(std::string filename, std::vector<eventfile>&eve) {
	std::ofstream ofs(filename);
	for (auto itr = eve.begin(); itr != eve.end(); itr++) {

		int mip = 0;
		int hip = 0;
		for (int i = 0; i < itr->partner.size(); i++) {
			if (itr->partner[i].flg_i[2])hip++;
			else mip++;
		}
		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << itr->muon.group_id << " "
			<< std::setw(3) << std::setprecision(0) << itr->muon_pl << " "
			<< std::setw(3) << std::setprecision(0) << mip << " "
			<< std::setw(3) << std::setprecision(0) << hip << std::endl;

	}


}