#include <fstream>
#include <iostream>
#include <ios>
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <map>

#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

//class align_param {
//public:
//	int id, signal, ix, iy;
//	//���쒆�S
//	double x, y, z;
//	//parameter(9);
//	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;
//
//};
std::vector<std::string> StringSplit(std::string str);
std::vector <corrmap_3d::align_param > read_ali(std::string filename, int output = 0);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg file-in-ali delaunay_before delaunay_after\n");
		exit(1);
	}

	std::string file_in_ali = argv[1];
	std::string file_out_before = argv[2];
	std::string file_out_after = argv[3];

	std::vector <corrmap_3d::align_param > ali_param = corrmap_3d::read_ali_param(file_in_ali, 1);

	std::vector<double> x, y;
	for (auto itr = ali_param.begin(); itr != ali_param.end(); itr++) {
		x.push_back(itr->x);
		y.push_back(itr->y);
	}

	delaunay::DelaunayTriangulation DT(x, y); // (std::vector<double> x, std::vector<double> y, uint32_t seed_)
	DT.execute(); // (double min_delta = 1e-6, double max_delta = 1e-5, int max_miss_count = 30)
	std::vector<delaunay::Edge> edge = DT.get_edges();
	{
		std::ofstream ofs(file_out_before);
		for (auto e : edge) {
			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.first).x << " "
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.first).y << " "
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.first).z << " "
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.second).x << " "
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.second).y << " "
				<< std::setw(8) << std::setprecision(1) << ali_param.at(e.second).z << std::endl;
		}
	}
	{
		double cos_z[2], sin_z[2];
		corrmap_3d::align_param p[2];
		std::ofstream ofs(file_out_after);
		for (auto e : edge) {
			p[0] = ali_param.at(e.first);
			p[1] = ali_param.at(e.second);
			cos_z[0] = cos(p[0].z_rot);
			cos_z[1] = cos(p[1].z_rot);
			sin_z[0] = sin(p[0].z_rot);
			sin_z[1] = sin(p[1].z_rot);

			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(1) << p[0].x_shrink*cos_z[0] * p[0].x - p[0].y_shrink*sin_z[0] * p[0].y + p[0].dx << " "
				<< std::setw(8) << std::setprecision(1) << p[0].x_shrink*sin_z[0] * p[0].x + p[0].y_shrink*cos_z[0] * p[0].y + p[0].dy << " "
				<< std::setw(8) << std::setprecision(1) << p[0].z + p[0].dz << " "
				<< std::setw(8) << std::setprecision(1) << p[1].x_shrink*cos_z[1] * p[1].x - p[1].y_shrink*sin_z[1] * p[1].y + p[1].dx << " "
				<< std::setw(8) << std::setprecision(1) << p[1].x_shrink*sin_z[1] * p[1].x + p[1].y_shrink*cos_z[1] * p[1].y + p[1].dy << " "
				<< std::setw(8) << std::setprecision(1) << p[1].z + p[1].dz << std::endl;
		}
	}
	//DT.dump(ofs);

}
