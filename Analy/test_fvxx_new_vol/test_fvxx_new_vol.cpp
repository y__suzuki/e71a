#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg fvxx pos\n");
		exit(1);
	}
	std::string file_in_fvxx = argv[1];
	int pos = std::stoi(argv[2]);

	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t >micro = fr.ReadAll(file_in_fvxx, pos, 0);

	int min_ph = 9;
	int max_ph = 0;
	double max_ratio = 0;
	double min_ratio = 1;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		int num = itr->px;
		int vola = itr->py;

		int pixelnum = num >> 5;
		int ph = num & 0x1f;
		int hitnum = vola;
		min_ph = std::min(min_ph, ph);
		max_ph = std::max(max_ph, ph);
		max_ratio = std::max(max_ratio, hitnum*1. / (pixelnum * 32));
		max_ratio = std::max(max_ratio, hitnum*1. / (pixelnum * 32));
		if (itr->ph / 10000 < 14)continue;
		printf("%d %d %d %d %d\n", itr->ph / 10000, itr->ph % 10000, ph, hitnum, pixelnum);

	}
	printf("new PH %d - %d\n", min_ph, max_ph);
}