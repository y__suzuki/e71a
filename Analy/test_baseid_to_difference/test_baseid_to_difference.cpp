#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
class Base_pair {
public:
	int eventid,pl[2],rawid[2];
	vxx::base_track_t base[2];
	double dax, day, dx, dy, dr, dl, dar, dal, md,oa;
};

std::vector<Base_pair> Read_file(std::string filename);
void add_xy_diff(vxx::base_track_t &t1, vxx::base_track_t &t2, Base_pair &p);
void add_rl_diff(vxx::base_track_t &t1, vxx::base_track_t &t2, Base_pair &p);

void Calc_position_difference(vxx::base_track_t &t1, vxx::base_track_t &t2, double &dr, double &dl, double &md, double &oa);
void Calc_position_difference(vxx::base_track_t &t1, vxx::base_track_t &t2, double &md, double &oa);

void output_file(std::string filename, std::vector<Base_pair>&p);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:input-txt ECC-path output-txt");
		exit(1);
	}
	std::string file_in_txt = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_out_txt = argv[3];

	std::vector<Base_pair>base_pair_v= Read_file(file_in_txt);
	vxx::BvxxReader br;
	int all = base_pair_v.size(),count=0;

	for (auto &p : base_pair_v) {
		printf("now %d/%d\n", count, all);
		count++;


		std::stringstream file_in_align, file_in_base[2];
		file_in_align << file_in_ECC << "Area0\\0\\align\\fine\\ali_" << std::setw(3) << std::setfill('0') << p.pl[0]
			<<"_" << std::setw(3) << std::setfill('0') << p.pl[1] << "_interpolation.txt";
		if (!std::filesystem::exists(file_in_align.str())) continue;
		file_in_base[0] << file_in_ECC << "Area0\\PL" << std::setw(3) << std::setfill('0') << p.pl[0] << "\\b" << std::setw(3) << std::setfill('0') << p.pl[0] << ".sel.cor.vxx";
		file_in_base[1] << file_in_ECC << "Area0\\PL" << std::setw(3) << std::setfill('0') << p.pl[1] << "\\b" << std::setw(3) << std::setfill('0') << p.pl[1] << ".sel.cor.vxx";
		if (!std::filesystem::exists(file_in_base[0].str())) continue;
		if (!std::filesystem::exists(file_in_base[1].str())) continue;
		std::vector<vxx::base_track_t> base[2];
		std::array<int, 2> index0 = { p.rawid[0], p.rawid[0] + 1 };
		base[0] = br.ReadAll(file_in_base[0].str(), p.pl[0], 0, vxx::opt::index = index0);
		std::array<int, 2> index1 = { p.rawid[1], p.rawid[1] + 1 };
		base[1] = br.ReadAll(file_in_base[1].str(), p.pl[1], 0, vxx::opt::index = index1);
		base[0][0].z = 0;
		base[1][0].z = 0;

		std::vector <corrmap_3d::align_param > corr = corrmap_3d::read_ali_param(file_in_align.str(), false);
		//delaunay3角形分割
		std::vector <corrmap_3d::align_param2 >corr2 = DelaunayDivide(corr);
		//trackとdelaunay3角形の対応
		std::vector < std::pair<vxx::base_track_t*, corrmap_3d::align_param2*>> track_param = track_affineparam_correspondence(base[1], corr2);
		//basetrackを変換
		corrmap_3d::trans_base_all(track_param);

		add_xy_diff(base[0][0], base[1][0], p);
		add_rl_diff(base[0][0], base[1][0], p);
		p.base[0] = base[0][0];
		p.base[1] = base[1][0];

	}
	printf("now %d/%d\n", count, all);

	output_file(file_out_txt, base_pair_v);


}
std::vector<Base_pair> Read_file(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<Base_pair> ret;
	Base_pair b;
	while (ifs >> b.eventid >> b.pl[0] >> b.rawid[0] >> b.pl[1] >> b.rawid[1]) {
		ret.push_back(b);
	}
	return ret;
}

void add_xy_diff(vxx::base_track_t &t1, vxx::base_track_t &t2, Base_pair &p) {
	double angle, d_pos_x, d_pos_y, d_ang_x, d_ang_y;
	double all_pos_x, all_pos_y, all_ang_x, all_ang_y;


	p.dx = t2.x - t1.x - (t1.ax + t2.ax) / 2 * (t2.z - t1.z);
	p.dy= t2.y - t1.y - (t1.ay + t2.ay) / 2 * (t2.z - t1.z);

	p.dax = (t2.ax - t1.ax);
	p.day = (t2.ay - t1.ay);

}
void add_rl_diff(vxx::base_track_t &t1, vxx::base_track_t &t2, Base_pair &p) {
	double angle;
	angle = sqrt(t1.ax*t1.ax + t1.ay*t1.ay);
	if (angle < 0.1) {
		p.dl = p.dx;
		p.dal = p.dax;
		p.dr = p.dy;
		p.dar = p.day;
		Calc_position_difference(t1, t2,  p.md, p.oa);
		return;
	}

	p.dar = ((t2.ax - t1.ax)*t1.ax + (t2.ay - t1.ay)*t1.ay);
	p.dal = ((t2.ax - t1.ax)*t1.ay - (t2.ay - t1.ay)*t1.ax);

	Calc_position_difference(t1, t2, p.dr, p.dl,p.md,p.oa);
}

void Calc_position_difference(vxx::base_track_t &t1, vxx::base_track_t &t2, double &dr, double &dl, double &md, double &oa) {
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = t1.x;
	pos0.y = t1.y;
	pos0.z = t1.z;
	dir0.x = t1.ax;
	dir0.y = t1.ay;
	dir0.z = 1;
	pos1.x = t2.x;
	pos1.y = t2.y;
	pos1.z = t2.z;
	dir1.x = t2.ax;
	dir1.y = t2.ay;
	dir1.z = 1;

	double extra[2], z_range[2] = { pos1.z ,pos0.z };
	md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
	oa = matrix_3D::opening_angle(dir0, dir1);
	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);

}

void Calc_position_difference(vxx::base_track_t &t1, vxx::base_track_t &t2,  double &md, double &oa) {
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = t1.x;
	pos0.y = t1.y;
	pos0.z = t1.z;
	dir0.x = t1.ax;
	dir0.y = t1.ay;
	dir0.z = 1;
	pos1.x = t2.x;
	pos1.y = t2.y;
	pos1.z = t2.z;
	dir1.x = t2.ax;
	dir1.y = t2.ay;
	dir1.z = 1;

	double extra[2], z_range[2] = { pos1.z ,pos0.z };
	printf("%.1lf %.1lf\n", pos0.z, pos1.z);

	md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
	oa = matrix_3D::opening_angle(dir0, dir1);
}

void output_file(std::string filename, std::vector<Base_pair>&p) {

	std::ofstream ofs(filename);
	for (auto itr = p.begin(); itr != p.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(6) << std::setprecision(0) << itr->eventid << " "
			<< std::setw(4) << std::setprecision(0) << itr->pl[0] << " "
			<< std::setw(4) << std::setprecision(0) << itr->pl[1] << " "
			<< std::setw(7) << std::setprecision(4) << itr->base[0].ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->base[0].ay << " "
			<< std::setw(7) << std::setprecision(4) << itr->dax << " "
			<< std::setw(7) << std::setprecision(4) << itr->day << " "
			<< std::setw(7) << std::setprecision(4) << itr->dar << " "
			<< std::setw(7) << std::setprecision(4) << itr->dal << " "
			<< std::setw(6) << std::setprecision(1) << itr->dx << " "
			<< std::setw(6) << std::setprecision(1) << itr->dy << " "
			<< std::setw(6) << std::setprecision(1) << itr->dr << " "
			<< std::setw(6) << std::setprecision(1) << itr->dl << " "
			<< std::setw(6) << std::setprecision(1) << itr->md << " "
			<< std::setw(5) << std::setprecision(4) << itr->oa << std::endl;
	}

}

