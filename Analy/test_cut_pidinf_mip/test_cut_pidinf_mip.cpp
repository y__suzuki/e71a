#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output {
public:
	int groupid, chainid, pid;
	double pb, vph, angle;
};
class VPH_pion_mip {
public:
	double angle_min, angle_max, pb_max, expect, vph, prob, prob_acc;

};

std::vector<output> inputfile(std::string filename);
void outputfile(std::string filename, std::vector<output> &out);
std::map<int, std::map<double, VPH_pion_mip>> Read_pion_PDF(std::string filename);
double Calc_prob(double vph, std::map<double, VPH_pion_mip>pion_vph);
double Calc_prob_vph(double prob, std::map<double, VPH_pion_mip>pion_vph);

std::vector<output> cut_mip(std::vector<output> &inf, std::map<int, std::map<double, VPH_pion_mip>> pion_pdf_map);
std::vector<output> sel_mip(std::vector<output> &inf, std::map<int, std::map<double, VPH_pion_mip>> pion_pdf_map);


int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage\n");
		exit(1);
	}

	std::string file_in_inf = argv[1];
	std::string file_in_vph = argv[2];
	std::string file_out_inf_hip = argv[3];
	std::string file_out_inf_mip = argv[4];

	std::map<int, std::map<double, VPH_pion_mip>> pion_pdf_map=Read_pion_PDF(file_in_vph);
	//for (auto itr = pion_pdf_map.begin(); itr != pion_pdf_map.end(); itr++) {
	//	double prob = Calc_prob_vph(0.99, itr->second);
	//	printf("fin\n%.1lf %.1lf %.4lf\n", itr->second.begin()->second.angle_min, itr->second.begin()->second.angle_max, prob);
	//}
	//exit(1);
	std::vector<output> inf = inputfile(file_in_inf);
	std::vector<output> inf_hip = cut_mip(inf, pion_pdf_map);
	std::vector<output> inf_mip = sel_mip(inf, pion_pdf_map);

	outputfile(file_out_inf_hip, inf_hip);
	outputfile(file_out_inf_mip, inf_mip);

}
std::vector<output> inputfile(std::string filename) {
	std::vector<output>ret;
	std::ifstream ifs(filename);
	output data;
	int cnt = 0;
	while (ifs >> data.groupid >> data.chainid >> data.pid >> data.angle >> data.pb >> data.vph) {
		if (cnt % 10000 == 0) {
			fprintf(stderr, "\r read file %d",cnt);
		}
		cnt++;
		
		
		ret.push_back(data);
	}
	fprintf(stderr, "\r read file %d\n",cnt);

	return ret;
}
std::map<int, std::map<double, VPH_pion_mip>> Read_pion_PDF(std::string filename) {
	std::ifstream ifs(filename);
	std::map<int, std::map<double, VPH_pion_mip>> ret;


	VPH_pion_mip data;
	int ibin, count, i_ang;
	while (ifs >> data.angle_min >> data.angle_max >> data.pb_max >> data.expect >> ibin >> data.vph >> count >> data.prob >> data.prob_acc) {
		i_ang = data.angle_max * 10;
		auto res = ret.find(i_ang);

		if (res == ret.end()) {
			std::map<double, VPH_pion_mip> map_tmp;
			map_tmp.insert(std::make_pair(data.vph, data));
			ret.insert(std::make_pair(i_ang, map_tmp));
		}
		else {
			res->second.insert(std::make_pair(data.vph, data));
		}
	}

	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	printf("%.1d\n", itr->first);
	//	for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
	//		printf("\t%.1lf\n", itr2->first);
	//	}
	//}
	return ret;

}
double Calc_prob(double vph, std::map<double, VPH_pion_mip>pion_vph) {
	double sum = 0;
	VPH_pion_mip point[2];
	for (auto itr = pion_vph.rbegin(); itr != std::next(pion_vph.rend(), -1); itr++) {
		point[0] = std::next(itr, 1)->second;
		point[1] = itr->second;
		//�l�p�`�̖ʐ�
		if (point[1].vph > vph) {
			sum += (point[0].prob + point[1].prob)*(point[1].vph - point[0].vph) / 2;
		}
		else if (point[0].vph > vph) {
			double prob_middle;
			prob_middle = point[0].prob + (point[1].prob - point[0].prob) / (point[1].vph - point[0].vph)*(vph - point[0].vph);
			sum += (prob_middle + point[1].prob)*(point[1].vph - vph) / 2;
		}
	}

	return sum;
}
double Calc_prob_vph(double prob, std::map<double, VPH_pion_mip>pion_vph) {
	double sum = 0, next_prob;

	VPH_pion_mip point[2];
	for (auto itr = pion_vph.begin(); itr != std::next(pion_vph.end(), -1); itr++) {
		point[0] = itr->second;
		point[1] = std::next(itr, 1)->second;

		//next_prob = (point[0].prob + point[1].prob)*(point[1].vph - point[0].vph) / 2;
		next_prob = (point[0].prob + point[1].prob)/ 2;
		//printf("%.5lf %.5lf\n", sum, next_prob);
		if (sum + next_prob < prob) {
			sum += next_prob;
		}
		else  {
			double prob_res = (prob - sum)/next_prob*((point[0].prob + point[1].prob)*(point[1].vph - point[0].vph) / 2);
			double y = sqrt(2 * prob_res*(point[1].prob - point[0].prob) / (point[1].vph - point[0].vph) + pow(point[0].prob, 2));
			double x = (y - point[0].prob)*(point[1].vph - point[0].vph) / (point[1].prob - point[0].prob) + point[0].vph;
			//printf("calc\n");
			//printf("%.5lf %.5lf\n", point[0].vph, point[1].vph);
			//printf("%.5lf %.5lf %.5lf\n", prob_res, y, x);
			//printf("------------\n");

			return x;
		}
	}
	return - 1;

}


std::vector<output> cut_mip(std::vector<output> &inf, std::map<int, std::map<double, VPH_pion_mip>> pion_pdf_map) {
	std::vector<output> ret;
	int cnt = 0, all = inf.size();
	for (auto itr = inf.begin(); itr != inf.end(); itr++) {
		if (cnt % 10000==0) {
			fprintf(stderr, "\r calc mip cut %d/%d(%4.1lf%%)", cnt, all, cnt*100. / all);
		}
		cnt++;
		int i_ang = int(itr->angle * 10);
		std::map<double, VPH_pion_mip> pion_pdf;
		if (pion_pdf_map.upper_bound(i_ang) == pion_pdf_map.end()) {
			pion_pdf = pion_pdf_map.rbegin()->second;
		}
		else {
			pion_pdf = pion_pdf_map.upper_bound(i_ang)->second;
		}
		double thr_vph = Calc_prob_vph(0.9987, pion_pdf);
		if (thr_vph < itr->vph) {
			ret.push_back(*itr);
		}

	}
	fprintf(stderr, "\r calc mip cut %d/%d(%4.1lf%%)\n", cnt, all, cnt*100. / all);

	return ret;
}
std::vector<output> sel_mip(std::vector<output> &inf, std::map<int, std::map<double, VPH_pion_mip>> pion_pdf_map) {
	std::vector<output> ret;
	int cnt = 0, all = inf.size();
	for (auto itr = inf.begin(); itr != inf.end(); itr++) {
		if (cnt % 10000==0) {
			fprintf(stderr, "\r calc mip cut %d/%d(%4.1lf%%)", cnt, all, cnt*100. / all);
		}
		cnt++;
		int i_ang = int(itr->angle * 10);
		std::map<double, VPH_pion_mip> pion_pdf;
		if (pion_pdf_map.upper_bound(i_ang) == pion_pdf_map.end()) {
			pion_pdf = pion_pdf_map.rbegin()->second;
		}
		else {
			pion_pdf = pion_pdf_map.upper_bound(i_ang)->second;
		}
		double thr_vph = Calc_prob_vph(0.9987, pion_pdf);
		if (thr_vph > itr->vph&&itr->pb>300&&itr->pb<1500) {
			ret.push_back(*itr);
		}

	}
	fprintf(stderr, "\r calc mip cut %d/%d(%4.1lf%%)\n", cnt, all, cnt*100. / all);

	return ret;

}
void outputfile(std::string filename, std::vector<output> &out) {
	std::ofstream ofs(filename);
	int cnt = 0,all=out.size();
	for (auto itr = out.begin(); itr != out.end(); itr++) {
		if (cnt % 10000 == 0) {
			fprintf(stderr, "\r write file %d/%d(%4.1lf%%)", cnt, all, cnt*100. / all);
		}
		cnt++;

		ofs << std::right << std::fixed
			<< std::setw(5) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(5) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(5) << std::setprecision(0) << itr->pid << " "
			<< std::setw(7) << std::setprecision(4) << itr->angle << " "
			<< std::setw(8) << std::setprecision(1) << itr->pb << " "
			<< std::setw(6) << std::setprecision(1) << itr->vph << std::endl;
	}

	fprintf(stderr, "\r write file %d/%d(%4.1lf%%)\n", cnt, all, cnt*100. / all);

}