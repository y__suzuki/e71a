//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>
using namespace l2c;
class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};

bool comp_link(const Linklet &letf, const Linklet&right)
{
	if (letf.pos1 != right.pos1)return letf.pos1 < right.pos1;
	else if (letf.pos2 != right.pos2)return letf.pos2 < right.pos2;
	else if (letf.id1 != right.id1)return letf.id1 < right.id1;
	else return letf.id2 < right.id2;
}
bool eq_link(const Linklet &letf, const Linklet&right)
{
	return letf.pos1 == right.pos1&&letf.pos2 == right.pos2&&letf.id1 == right.id1&&letf.id2 == right.id2;
}

void read_linklet_list(std::string filename, std::vector<Linklet> &ltlist);
void write_linklet_list(std::string filename, std::vector<Linklet> &ltlist);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg file-in-linklet-list file-out-linklet-list\n");
		exit(1);
	}
	std::string file_in_link = argv[1];
	std::string file_out_link = argv[2];

	std::vector<Linklet> ltlist;
	read_linklet_list(file_in_link, ltlist);
	printf("all linklet num = %lld\n", ltlist.size());
	sort(ltlist.begin(), ltlist.end(), comp_link);
	auto result = std::unique(ltlist.begin(), ltlist.end(), eq_link);
	printf("same linklet delte %d -->", ltlist.size());
	ltlist.erase(result, ltlist.end());
	printf("%d\n", ltlist.size());
	write_linklet_list(file_out_link, ltlist);


}

int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}

void read_linklet_list(std::string filename, std::vector<Linklet> &ltlist) {
	int64_t link_num = Linklet_header_num(filename);
	ltlist.reserve(link_num);

	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	linklet_header l;
	while (ifs.read((char*)& l, sizeof(linklet_header))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		ltlist.emplace_back(l.pos0, l.pos1, l.raw0, l.raw1);
		//printf("%d %d %d %d\n", l.pos0, l.pos1, l.raw0, l.raw1);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
}

void write_linklet_list(std::string filename, std::vector<Linklet> &ltlist) {
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (ltlist.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = ltlist.size();
	linklet_header l;

	for (int i = 0; i < ltlist.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		l.pos0 = ltlist[i].pos1;
		l.pos1 = ltlist[i].pos2;
		l.raw0 = ltlist[i].id1;
		l.raw1 = ltlist[i].id2;
		ofs.write((char*)& l, sizeof(linklet_header));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}
