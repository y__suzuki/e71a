
//basetrack読む
//ranking cut
//microtrack読む
//singal の fvxx出力
//noise　の fvxx出力
//col row は保存? 72センサー*2の分離する? 

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#include <fstream>
#include <list>
#include <set>
#include <iostream>
#include <ios>
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <map>
struct CutParam {
	double angle[2];
	double intercept, slope, ymax;
};
struct BaseInformation {
	double angle, x, y;
	vxx::base_track_t *ptr;
};

std::vector<vxx::micro_track_t>  select_micro_face0(std::vector<vxx::base_track_t> &base, int pl, std::string file_path_thick0, std::string file_path_thick1, std::string file_path_thin0, std::string file_path_thin1);
std::vector<vxx::micro_track_t>  select_micro_face1(std::vector<vxx::base_track_t> &base, int pl, std::string file_path_thick0, std::string file_path_thick1, std::string file_path_thin0, std::string file_path_thin1);


int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-bvxx pl in-fvxx-path out-file-path\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_fvxx_path = argv[3];
	std::string file_out_path = argv[4];

	std::vector<vxx::base_track_t> base;

	int64_t count = 0;
	vxx::BvxxReader br;
	base = br.ReadAll(file_in_base, pl, 0);

	std::stringstream file_in_fvxx_thick00, file_in_fvxx_thick01, file_in_fvxx_thick10, file_in_fvxx_thick11;
	std::stringstream file_in_fvxx_thin00, file_in_fvxx_thin01, file_in_fvxx_thin10, file_in_fvxx_thin11;
	file_in_fvxx_thick00 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << "_thick_0.vxx";
	file_in_fvxx_thick01 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << "_thick_1.vxx";
	file_in_fvxx_thick10 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << "_thick_0.vxx";
	file_in_fvxx_thick11 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << "_thick_1.vxx";
	file_in_fvxx_thin00 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << "_thin_0.vxx";
	file_in_fvxx_thin01 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << "_thin_1.vxx";
	file_in_fvxx_thin10 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << "_thin_0.vxx";
	file_in_fvxx_thin11 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << "_thin_1.vxx";

	std::vector<vxx::micro_track_t> sel_face0, sel_face1;
	std::vector<vxx::micro_track_t> cut_face0, cut_face1;

	sel_face0 = select_micro_face0(base,pl*10+1, file_in_fvxx_thick00.str(), file_in_fvxx_thick01.str(), file_in_fvxx_thin00.str(), file_in_fvxx_thin01.str());
	//cut_face0 = select_micro_face0(cut, thick00, thick01, thin00, thin01);

	sel_face1 = select_micro_face1(base, pl*10+2, file_in_fvxx_thick10.str(), file_in_fvxx_thick11.str(), file_in_fvxx_thin10.str(), file_in_fvxx_thin11.str());
	//cut_face1 = select_micro_face1(cut, thick10, thick11, thin10, thin11);


	std::stringstream file_out_fvxx_sel0, file_out_fvxx_sel1, file_out_fvxx_cut0, file_out_fvxx_cut1;
	file_out_fvxx_sel0 << file_out_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << ".sel.vxx";
	file_out_fvxx_sel1 << file_out_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << ".sel.vxx";
	file_out_fvxx_cut0 << file_out_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << ".cut.vxx";
	file_out_fvxx_cut1 << file_out_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << ".cut.vxx";
	
	vxx::FvxxWriter fw;
	fw.Write(file_out_fvxx_sel0.str(), pl * 10 + 1, 0, sel_face0);
	fw.Write(file_out_fvxx_sel1.str(), pl * 10 + 2, 0, sel_face1);
	//fw.Write(file_out_fvxx_cut0.str(), pl * 10 + 1, 0, cut_face0);
	//fw.Write(file_out_fvxx_cut1.str(), pl * 10 + 2, 0, cut_face1);


	printf("selected microtrack face0 = %d\n", sel_face0.size());
	printf("selected microtrack face1 = %d\n", sel_face1.size());
	//printf("cut basetrack        = %d\n", cut.size());
	//printf("cut microtrack face0 = %d\n", cut_face0.size());
	//printf("cut microtrack face1 = %d\n", cut_face1.size());
}

std::vector<vxx::micro_track_t>  select_micro_face0(std::vector<vxx::base_track_t> &base, int pl,std::string file_path_thick0, std::string file_path_thick1, std::string file_path_thin0, std::string file_path_thin1){
	std::set<std::tuple<int, int, int>>id_thick0, id_thick1, id_thin0, id_thin1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if ((itr->m[0].zone - 1) / 6 == 0)id_thick0.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1) / 6 == 1)id_thick0.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1) / 6 == 2)id_thick1.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1) / 6 == 3)id_thick1.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1) / 6 == 4)id_thin0.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1) / 6 == 5)id_thin0.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1) / 6 == 6)id_thin1.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1) / 6 == 7)id_thin1.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else {
			fprintf(stderr, "except zone: zone=%d\n", itr->m[0].zone);
		}
	}

	std::tuple<int, int, int >id;
	std::map<std::tuple<int, int, int>, vxx::micro_track_t> ret;
	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> micro;

	micro = fr.ReadAll(file_path_thick0, pl, 0);
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thick0.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	micro.clear();
	micro.shrink_to_fit();
	micro = fr.ReadAll(file_path_thick1, pl, 0);
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thick1.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	micro.clear();
	micro.shrink_to_fit();
	micro = fr.ReadAll(file_path_thin0, pl, 0);
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thin0.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	micro.clear();
	micro.shrink_to_fit();
	micro = fr.ReadAll(file_path_thin1, pl, 0);
	for (auto itr =micro.begin(); itr != micro.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thin1.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	micro.clear();
	micro.shrink_to_fit();
	std::vector<vxx::micro_track_t> ret_m;
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		ret_m.push_back(itr->second);
	}
	return ret_m;
}
std::vector<vxx::micro_track_t>  select_micro_face1(std::vector<vxx::base_track_t> &base, int pl, std::string file_path_thick0, std::string file_path_thick1, std::string file_path_thin0, std::string file_path_thin1) {
	std::set<std::tuple<int, int, int>>id_thick0, id_thick1, id_thin0, id_thin1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if ((itr->m[1].zone - 1) / 6 == 0)id_thick0.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1) / 6 == 1)id_thick1.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1) / 6 == 2)id_thick0.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1) / 6 == 3)id_thick1.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1) / 6 == 4)id_thin0.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1) / 6 == 5)id_thin1.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1) / 6 == 6)id_thin0.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1) / 6 == 7)id_thin1.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else {
			fprintf(stderr, "except zone: zone=%d\n", itr->m[1].zone);
		}
	}

	std::tuple<int, int, int >id;
	std::map<std::tuple<int, int, int>, vxx::micro_track_t> ret;
	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> micro;

	micro = fr.ReadAll(file_path_thick0, pl, 0);
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thick0.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	micro.clear();
	micro.shrink_to_fit();
	micro = fr.ReadAll(file_path_thick1, pl, 0);
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thick1.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	micro.clear();
	micro.shrink_to_fit();
	micro = fr.ReadAll(file_path_thin0, pl, 0);
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thin0.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	micro.clear();
	micro.shrink_to_fit();
	micro = fr.ReadAll(file_path_thin1, pl, 0);
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thin1.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	micro.clear();
	micro.shrink_to_fit();

	std::vector<vxx::micro_track_t> ret_m;
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		ret_m.push_back(itr->second);
	}
	return ret_m;
}