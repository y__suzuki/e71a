
//basetrack読む
//ranking cut
//microtrack読む
//singal の fvxx出力
//noise　の fvxx出力
//col row は保存? 72センサー*2の分離する? 

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#include <fstream>
#include <list>
#include <set>
#include <iostream>
#include <ios>
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <map>
struct CutParam {
	double angle[2];
	double intercept, slope, ymax;
};
struct BaseInformation {
	double angle, x, y;
	vxx::base_track_t *ptr;
};

std::vector<CutParam>  read_cut_param(std::string filename);
void RankingCut(std::vector<vxx::base_track_t> &base, std::vector<CutParam> param, std::vector<vxx::base_track_t> &sel, std::vector<vxx::base_track_t> &cut);
std::vector<vxx::micro_track_t>  select_micro_face0(std::vector<vxx::base_track_t> &base, std::vector<vxx::micro_track_t>&thick0, std::vector<vxx::micro_track_t>&thick1, std::vector<vxx::micro_track_t>&thin0, std::vector<vxx::micro_track_t>&thin1);
std::vector<vxx::micro_track_t>  select_micro_face1(std::vector<vxx::base_track_t> &base, std::vector<vxx::micro_track_t>&thick0, std::vector<vxx::micro_track_t>&thick1, std::vector<vxx::micro_track_t>&thin0, std::vector<vxx::micro_track_t>&thin1);


int main2(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-bvxx pl cut-param-file in-fvxx-path out-file-path\n");
		fprintf(stderr, "cut-param-file:ang_min ang_max x1 y1 x2 y2\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_cut_normal = argv[3];
	std::string file_in_fvxx_path = argv[4];
	std::string file_out_path = argv[5];

	std::vector<CutParam> cutparam_norm = read_cut_param(file_in_cut_normal);
	std::vector<vxx::base_track_t> base,sel,cut;

	int64_t count = 0;
	vxx::BvxxReader br;
	base = br.ReadAll(file_in_base, pl, 0);
	RankingCut(base, cutparam_norm, sel, cut);
	vxx::BvxxWriter bw;
	std::stringstream file_out_bvxx_sel, file_out_bvxx_cut;
	file_out_bvxx_sel << file_out_path << "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.vxx";
	file_out_bvxx_cut << file_out_path << "\\b" << std::setw(3) << std::setfill('0') << pl << ".cut.vxx";
	bw.Write(file_out_bvxx_sel.str(), pl, 0, sel);
	bw.Write(file_out_bvxx_cut.str(), pl, 0, cut);

	std::stringstream file_in_fvxx_thick00, file_in_fvxx_thick01, file_in_fvxx_thick10, file_in_fvxx_thick11;
	std::stringstream file_in_fvxx_thin00, file_in_fvxx_thin01, file_in_fvxx_thin10, file_in_fvxx_thin11;
	file_in_fvxx_thick00 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << "_thick_0.vxx";
	file_in_fvxx_thick01 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << "_thick_1.vxx";
	file_in_fvxx_thick10 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << "_thick_0.vxx";
	file_in_fvxx_thick11 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << "_thick_1.vxx";
	file_in_fvxx_thin00 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << "_thin_0.vxx";
	file_in_fvxx_thin01 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << "_thin_1.vxx";
	file_in_fvxx_thin10 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << "_thin_0.vxx";
	file_in_fvxx_thin11 << file_in_fvxx_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << "_thin_1.vxx";
	
	std::vector<vxx::micro_track_t> sel_face0, sel_face1;
	std::vector<vxx::micro_track_t> cut_face0, cut_face1;

	std::vector<vxx::micro_track_t> thick00, thick01, thick10, thick11, thin00, thin01, thin10, thin11;
	vxx::FvxxReader fr;
	thick00 = fr.ReadAll(file_in_fvxx_thick00.str(), pl * 10 + 1, 0);
	thick01 = fr.ReadAll(file_in_fvxx_thick01.str(), pl * 10 + 1, 0);
	thin00 = fr.ReadAll(file_in_fvxx_thin00.str(), pl * 10 + 1, 0);
	thin01 = fr.ReadAll(file_in_fvxx_thin01.str(), pl * 10 + 1, 0);
	sel_face0 = select_micro_face0(sel, thick00, thick01, thin00, thin01);
	cut_face0 = select_micro_face0(cut, thick00, thick01, thin00, thin01);

	thick00.clear();
	thick00.shrink_to_fit();
	thick01.clear();
	thick01.shrink_to_fit();
	thin00.clear();
	thin00.shrink_to_fit();
	thin01.clear();
	thin01.shrink_to_fit();

	thick10 = fr.ReadAll(file_in_fvxx_thick10.str(), pl * 10 + 2, 0);
	thick11 = fr.ReadAll(file_in_fvxx_thick11.str(), pl * 10 + 2, 0);
	thin10 = fr.ReadAll(file_in_fvxx_thin10.str(), pl * 10 + 2, 0);
	thin11 = fr.ReadAll(file_in_fvxx_thin11.str(), pl * 10 + 2, 0);

	sel_face1 = select_micro_face1(sel, thick10, thick11, thin10, thin11);
	cut_face1 = select_micro_face1(cut, thick10, thick11, thin10, thin11);

	thick10.clear();
	thick10.shrink_to_fit();
	thick11.clear();
	thick11.shrink_to_fit();
	thin10.clear();
	thin10.shrink_to_fit();
	thin11.clear();
	thin11.shrink_to_fit();

	std::stringstream file_out_fvxx_sel0, file_out_fvxx_sel1, file_out_fvxx_cut0, file_out_fvxx_cut1;
	file_out_fvxx_sel0 << file_out_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << ".sel.vxx";
	file_out_fvxx_sel1 << file_out_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << ".sel.vxx";
	file_out_fvxx_cut0 << file_out_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << ".cut.vxx";
	file_out_fvxx_cut1 << file_out_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << ".cut.vxx";

	vxx::FvxxWriter fw;
	fw.Write(file_out_fvxx_sel0.str(), pl * 10 + 1, 0, sel_face0);
	fw.Write(file_out_fvxx_sel1.str(), pl * 10 + 2, 0, sel_face1);
	fw.Write(file_out_fvxx_cut0.str(), pl * 10 + 1, 0, cut_face0);
	fw.Write(file_out_fvxx_cut1.str(), pl * 10 + 2, 0, cut_face1);

	printf("selected basetrack        = %d\n", sel.size());
	printf("selected microtrack face0 = %d\n", sel_face0.size());
	printf("selected microtrack face1 = %d\n", sel_face1.size());
	printf("cut basetrack        = %d\n", cut.size());
	printf("cut microtrack face0 = %d\n", cut_face0.size());
	printf("cut microtrack face1 = %d\n", cut_face1.size());
}
std::vector<CutParam>  read_cut_param(std::string filename) {
	std::vector<CutParam> ret;
	std::ifstream ifs(filename);
	double angle[2], x[2], y[2];
	while (ifs >> angle[0] >> angle[1] >> x[0] >> y[0] >> x[1] >> y[1]) {
		CutParam par;
		par.angle[0] = angle[0];
		par.angle[1] = angle[1];
		par.slope = (y[1] - y[0]) / (x[1] - x[0]);
		par.intercept = y[0] - par.slope * x[0];
		par.ymax = y[1];
		ret.push_back(par);
		//printf("y=%lf x+%lf\n", par.slope, par.intercept);
	}
	return ret;
}

void RankingCut(std::vector<vxx::base_track_t> &base, std::vector<CutParam> param, std::vector<vxx::base_track_t> &sel, std::vector<vxx::base_track_t> &cut) {
	
	double angle;
	double x, y;

	std::vector<vxx::base_track_t> all = base;
	for (auto i_cut = param.begin(); i_cut != param.end(); i_cut++) {
		sel.clear();
		for (auto itr = all.begin(); itr != all.end(); itr++) {
			angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
			if (i_cut->angle[0] <= angle && angle < i_cut->angle[1]) {
				x = sqrt(pow(itr->m[0].ax - itr->ax, 2) + pow(itr->m[1].ax - itr->ax, 2) + pow(itr->m[0].ay - itr->ay, 2) + pow(itr->m[1].ay - itr->ay, 2));
				y = (itr->m[0].ph + itr->m[1].ph) % 10000;
				if (y >= x * i_cut->slope + i_cut->intercept || i_cut->ymax <= y) {
					sel.push_back(*itr);
				}
			}
			else {
				sel.push_back(*itr);
			}
		}
		all = sel;
	}
	std::set<int> sel_raw;
	for (auto itr = sel.begin(); itr != sel.end(); itr++) {
		sel_raw.insert(itr->rawid);
	}
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (sel_raw.count(itr->rawid) == 0) {
			cut.push_back(*itr);
		}
	}
	fprintf(stderr, "ranking cut %d --> sel:%d + cut:%d\n", base.size(), sel.size(), cut.size());
}
std::vector<vxx::micro_track_t>  select_micro_face0(std::vector<vxx::base_track_t> &base, std::vector<vxx::micro_track_t>&thick0, std::vector<vxx::micro_track_t>&thick1, std::vector<vxx::micro_track_t>&thin0, std::vector<vxx::micro_track_t>&thin1) {
	std::set<std::tuple<int, int, int>>id_thick0, id_thick1, id_thin0, id_thin1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if ((itr->m[0].zone - 1 )/ 6 == 0)id_thick0.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1 )/ 6 == 1)id_thick0.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1 )/ 6 == 2)id_thick1.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1 )/ 6 == 3)id_thick1.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1 )/ 6 == 4)id_thin0.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1 )/ 6 == 5)id_thin0.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1 )/ 6 == 6)id_thin1.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else if ((itr->m[0].zone - 1 )/ 6 == 7)id_thin1.insert(std::make_tuple(itr->m[0].col, itr->m[0].row, itr->m[0].isg));
		else {
			fprintf(stderr, "except zone: zone=%d\n", itr->m[0].zone);
		}
	}

	std::map<std::tuple<int, int, int>, vxx::micro_track_t> ret;

	std::tuple<int, int, int >id;
	for (auto itr = thick0.begin(); itr != thick0.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thick0.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	for (auto itr = thick1.begin(); itr != thick1.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thick1.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	for (auto itr = thin0.begin(); itr != thin0.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thin0.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	for (auto itr = thin1.begin(); itr != thin1.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thin1.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}

	std::vector<vxx::micro_track_t> ret_m;
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		ret_m.push_back(itr->second);
	}
	return ret_m;
}std::vector<vxx::micro_track_t>  select_micro_face1(std::vector<vxx::base_track_t> &base, std::vector<vxx::micro_track_t>&thick0, std::vector<vxx::micro_track_t>&thick1, std::vector<vxx::micro_track_t>&thin0, std::vector<vxx::micro_track_t>&thin1) {
	std::set<std::tuple<int, int, int>>id_thick0, id_thick1, id_thin0, id_thin1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if ((itr->m[1].zone - 1) / 6 == 0)id_thick0.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1 )/ 6 == 1)id_thick1.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1 )/ 6 == 2)id_thick0.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1 )/ 6 == 3)id_thick1.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1 )/ 6 == 4)id_thin0.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1 )/ 6 == 5)id_thin1.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1 )/ 6 == 6)id_thin0.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else if ((itr->m[1].zone - 1 )/ 6 == 7)id_thin1.insert(std::make_tuple(itr->m[1].col, itr->m[1].row, itr->m[1].isg));
		else {
			fprintf(stderr, "except zone: zone=%d\n", itr->m[1].zone);
		}
	}

	std::map<std::tuple<int, int, int>, vxx::micro_track_t> ret;

	std::tuple<int, int, int >id;
	for (auto itr = thick0.begin(); itr != thick0.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thick0.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	for (auto itr = thick1.begin(); itr != thick1.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thick1.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	for (auto itr = thin0.begin(); itr != thin0.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thin0.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}
	for (auto itr = thin1.begin(); itr != thin1.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (id_thin1.count(id) == 0)continue;
		auto res = ret.insert(std::make_pair(id, *itr));
		while (!res.second) {
			itr->isg++;
			std::get<2>(id) = itr->isg;
			res = ret.insert(std::make_pair(id, *itr));
		}
	}

	std::vector<vxx::micro_track_t> ret_m;
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		ret_m.push_back(itr->second);
	}
	return ret_m;
}