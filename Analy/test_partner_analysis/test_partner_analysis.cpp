#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void out_Chain_inf(std::vector<mfile0::M_Chain>&chains, std::ofstream &ofs);
int iron_penetrate_flg(mfile0::M_Chain&c);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "uage:filename\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_chain = argv[2];

	mfile0::Mfile m;

	mfile1::read_mfile_extension(file_in_mfile, m);

	std::ofstream ofs(file_out_chain);
	out_Chain_inf(m.chains, ofs);

}
void out_Chain_inf(std::vector<mfile0::M_Chain>&chains, std::ofstream &ofs) {
	for (auto &c : chains) {
		double ax = mfile0::chain_ax(c);
		double ay = mfile0::chain_ay(c);
		int nseg = c.nseg;
		double vph = mfile0::chain_vph(c);
		int iron_flg = iron_penetrate_flg(c);

		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << c.basetracks.begin()->group_id << " "
			<< std::setw(10) << std::setprecision(0) << c.basetracks.begin()->flg_i[1] << " "
			<< std::setw(8) << std::setprecision(4) << ax << " "
			<< std::setw(8) << std::setprecision(4) << ay << " "
			<< std::setw(5) << std::setprecision(0) << nseg << " "
			<< std::setw(7) << std::setprecision(2) << vph << " "
			<< std::setw(3) << std::setprecision(0) << iron_flg << std::endl;

	}



}
int iron_penetrate_flg(mfile0::M_Chain&c) {
	int pl0 = c.basetracks.begin()->pos / 10;
	int pl1 = c.basetracks.rbegin()->pos / 10;
	if (pl1 - pl0 == 0)return 0;
	else if (pl1 - pl0 == 1) {
		if (pl0 <= 4 && pl0 <= 14)return 1;
		else if (pl0 % 2 == 0)return 1;
		else return 0;
	}
	else return 1;
}