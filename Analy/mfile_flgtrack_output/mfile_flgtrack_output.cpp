#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void output_upstream_track(std::ofstream &ofs, std::vector<mfile0::M_Chain>&chain);

int main(int argc, char**argv) {
	if (argc <3) {
		fprintf(stderr, "usage: prg output-path file-in-mfile0(txt) file-in-mfile1(txt) ...\n");
		exit(1);
	}
	std::string file_out = argv[1];
	std::vector<std::string> file_in_mfile;
	for (int num = 2; num < argc; num++) {
		file_in_mfile.push_back(argv[num]);
	}

	std::vector<mfile0::Mfile> m_v;
	for (int i = 0; i < file_in_mfile.size(); i++) {
		mfile0::Mfile m;
		mfile0::read_mfile(file_in_mfile[i], m);
		m_v.push_back(m);
	}

	std::ofstream ofs(file_out);
	for (int i = 0; i < m_v.size(); i++) {
		output_upstream_track(ofs, m_v[i].chains);
	}
}


void output_upstream_track(std::ofstream &ofs, std::vector<mfile0::M_Chain>&chain) {
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (itr2->flg_i[0] != 1)continue;
			ofs << std::right << std::fixed
				<< std::setw(10) << std::setprecision(0) << itr2->group_id << " "
				<< std::setw(5) << std::setprecision(0) << itr2->flg_i[1] << " "
				<< std::setw(5) << std::setprecision(0) << itr2->pos / 10 << " "
				<< std::setw(10) << std::setprecision(0) << itr2->rawid << std::endl;
		}
	}
}

