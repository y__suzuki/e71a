#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class PID_inf {
public:
	int groupid, chainid, pid;
	double  angle, pb, vph, proton_likelihood, pion_likelihood, likelihood_ratio;
};
std::vector<PID_inf> read_pid_inf(std::string filename);
void Add_PID_inf(std::vector<Momentum_recon::Event_information> &momch, std::vector<PID_inf>&pid_inf);
std::vector<Momentum_recon::Event_information> Add_PID_inf_sel_hip(std::vector<Momentum_recon::Event_information> &momch, std::vector<PID_inf>&pid_inf);
std::vector<Momentum_recon::Event_information> Add_PID_inf_sel_mip(std::vector<Momentum_recon::Event_information> &momch, std::vector<PID_inf>&pid_inf);
std::vector<Momentum_recon::Event_information> Add_PID_inf_sel_mip_hip(std::vector<Momentum_recon::Event_information> &momch, std::vector<PID_inf>&pid_inf_mip, std::vector<PID_inf>&pid_inf_hip);


int main(int argc, char** argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:filename");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_in_pid_hip = argv[2];
	std::string file_in_pid_mip = argv[3];
	std::string file_out_momch = argv[4];


	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	std::vector<PID_inf> pid_inf_hip = read_pid_inf(file_in_pid_hip);
	std::vector<PID_inf> pid_inf_mip = read_pid_inf(file_in_pid_mip);

	//Add_PID_inf(momch, pid_inf);
	//std::vector<Momentum_recon::Event_information>momch_p_hip = Add_PID_inf_sel_hip(momch, pid_inf_hip);
	//std::vector<Momentum_recon::Event_information>momch_p_mip = Add_PID_inf_sel_mip(momch, pid_inf_mip);
	std::vector<Momentum_recon::Event_information>momch_p_mip_hip = Add_PID_inf_sel_mip_hip(momch, pid_inf_mip, pid_inf_hip);


	Momentum_recon::Write_Event_information_extension(file_out_momch, momch_p_mip_hip);

}
std::vector<PID_inf> read_pid_inf(std::string filename) {
	std::vector<PID_inf> ret;
	PID_inf c_tmp;
	int num = 0;
	std::ifstream ifs(filename.c_str());
	while (ifs >> c_tmp.groupid >> c_tmp.chainid >> c_tmp.pid >> c_tmp.angle >> c_tmp.pb >> c_tmp.vph) {
		if (num % 100000 == 0) {
			printf("\r read chain %d", num);
		}
		num++;

		if (c_tmp.pb < 0)c_tmp.pb = 10;
		ret.push_back(c_tmp);
	}
	printf("\r read chain %d\n", num);

	ifs.close();
	return ret;
}

void Add_PID_inf(std::vector<Momentum_recon::Event_information> &momch, std::vector<PID_inf>&pid_inf) {
	std::map<std::pair<int, int>, PID_inf>pid_map;
	for (auto itr = pid_inf.begin(); itr != pid_inf.end(); itr++) {
		auto res = pid_map.insert(std::make_pair(std::make_pair(itr->groupid, itr->chainid), *itr));
		if (!res.second) {
			printf("double information %5d %5d\n", itr->groupid, itr->chainid);
		}
	}

	std::pair<int, int> id;
	for (auto &ev : momch) {
		id.first = ev.groupid;

		for (auto &c : ev.chains) {
			id.second = c.chainid;

			if (pid_map.count(id) == 0)continue;
			auto res = pid_map.find(id);
			c.particle_flg = res->second.pid;
			//c.muon_likelihood = res->second.pion_likelihood;
			//c.proton_likelihood = res->second.proton_likelihood;
		}
	}
}

std::vector<Momentum_recon::Event_information> Add_PID_inf_sel_hip(std::vector<Momentum_recon::Event_information> &momch, std::vector<PID_inf>&pid_inf) {
	std::map<std::pair<int, int>, PID_inf>pid_map;
	for (auto itr = pid_inf.begin(); itr != pid_inf.end(); itr++) {
		auto res = pid_map.insert(std::make_pair(std::make_pair(itr->groupid, itr->chainid), *itr));
		if (!res.second) {
			printf("double information %5d %5d\n", itr->groupid, itr->chainid);
		}
	}

	std::vector<Momentum_recon::Event_information> ret;
	std::pair<int, int> id;
	for (auto &ev : momch) {
		id.first = ev.groupid;
		Momentum_recon::Event_information ev_tmp = ev;
		ev_tmp.chains.clear();
		for (auto &c : ev.chains) {
			id.second = c.chainid;

			if (pid_map.count(id) == 0)continue;
			auto res = pid_map.find(id);
			c.particle_flg = res->second.pid;
			if (c.particle_flg == 2212) {
				ev_tmp.chains.push_back(c);
			}
			//c.muon_likelihood = res->second.pion_likelihood;
			//c.proton_likelihood = res->second.proton_likelihood;
		}
		if (ev_tmp.chains.size() > 0) {
			ret.push_back(ev);
		}
	}
	return ret;
}

std::vector<Momentum_recon::Event_information> Add_PID_inf_sel_mip(std::vector<Momentum_recon::Event_information> &momch, std::vector<PID_inf>&pid_inf) {
	std::map<std::pair<int, int>, PID_inf>pid_map;
	for (auto itr = pid_inf.begin(); itr != pid_inf.end(); itr++) {
		auto res = pid_map.insert(std::make_pair(std::make_pair(itr->groupid, itr->chainid), *itr));
		if (!res.second) {
			printf("double information %5d %5d\n", itr->groupid, itr->chainid);
		}
	}

	std::vector<Momentum_recon::Event_information> ret;
	std::pair<int, int> id;
	for (auto &ev : momch) {
		id.first = ev.groupid;
		Momentum_recon::Event_information ev_tmp = ev;
		ev_tmp.chains.clear();
		for (auto &c : ev.chains) {
			id.second = c.chainid;

			if (pid_map.count(id) == 0)continue;
			auto res = pid_map.find(id);
			c.particle_flg = res->second.pid;
			ev_tmp.chains.push_back(c);
			//c.muon_likelihood = res->second.pion_likelihood;
			//c.proton_likelihood = res->second.proton_likelihood;
		}
		if (ev_tmp.chains.size() > 0) {
			ret.push_back(ev);
		}
	}
	return ret;
}

std::vector<Momentum_recon::Event_information> Add_PID_inf_sel_mip_hip(std::vector<Momentum_recon::Event_information> &momch, std::vector<PID_inf>&pid_inf_mip,std::vector<PID_inf>&pid_inf_hip) {
	std::map<std::pair<int, int>, PID_inf>pid_map_hip;
	std::map<std::pair<int, int>, PID_inf>pid_map_mip;
	int cnt = 0;

	for (auto itr = pid_inf_hip.begin(); itr != pid_inf_hip.end(); itr++) {
		auto res = pid_map_hip.insert(std::make_pair(std::make_pair(itr->groupid, itr->chainid), *itr));
		if (!res.second) {
			printf("double information %5d %5d\n", itr->groupid, itr->chainid);
		}
	}
	for (auto itr = pid_inf_mip.begin(); itr != pid_inf_mip.end(); itr++) {
		auto res = pid_map_mip.insert(std::make_pair(std::make_pair(itr->groupid, itr->chainid), *itr));
		if (!res.second) {
			printf("double information %5d %5d\n", itr->groupid, itr->chainid);
		}
	}

	std::vector<Momentum_recon::Event_information> ret;
	std::pair<int, int> id;
	for (auto &ev : momch) {
		id.first = ev.groupid;
		Momentum_recon::Event_information ev_tmp = ev;
		ev_tmp.chains.clear();
		for (auto &c : ev.chains) {
			id.second = c.chainid;

			if (pid_map_hip.count(id) == 0&& pid_map_mip.count(id) == 0)continue;
			else if (pid_map_hip.count(id) != 0) {
				auto res = pid_map_hip.find(id);
				c.particle_flg = res->second.pid;
				if (c.particle_flg == 2212) {
					ev_tmp.chains.push_back(c);
				}
			}
			else if (pid_map_mip.count(id) != 0) {
				if (c.Get_proton_mcs_pb() < 700)continue;
				if (c.Get_proton_mcs_pb() > 1500)continue;
				auto res = pid_map_mip.find(id);
				c.particle_flg = res->second.pid;
				if (cnt % 10 == 0) {
					ev_tmp.chains.push_back(c);
				}
				cnt++;
			}
		}
		if (ev_tmp.chains.size() > 0) {
			ret.push_back(ev_tmp);
		}
	}
	return ret;
}

