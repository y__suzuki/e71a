#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
class Cahin_difference {
public:
	int groupid, chainid[2],direction;
	double dar, dal, dr, dl, ax[2], ay[2];
	Momentum_recon::Mom_chain c[2];
};

std::vector<Cahin_difference>calc_difference(Momentum_recon::Event_information&ev);
void output_diff(std::ofstream &ofs, std::vector<Cahin_difference>&diff);
std::set<int> Decide_remove_chain(std::vector<Cahin_difference>&diff);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-momch file-out-momch file-out\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];
	std::string file_out = argv[3];
	std::ofstream ofs(file_out);
	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	for (auto &ev : momch) {
		std::vector<Cahin_difference>diff = calc_difference(ev);
		output_diff(ofs, diff);
		std::set<int> delete_list = Decide_remove_chain(diff);
		for (auto itr = ev.chains.begin(); itr != ev.chains.end();) {
			if (delete_list.count(itr->chainid) == 1) {
				itr = ev.chains.erase(itr);
			}
			else {
				itr++;
			}
		}
	}
	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);


}
std::vector<Cahin_difference>calc_difference(Momentum_recon::Event_information&ev) {

	int vertex_pl = ev.vertex_pl;
	std::vector<Momentum_recon::Mom_chain> forward_chain, backward_chain;
	for (auto itr = ev.chains.begin(); itr != ev.chains.end(); itr++) {
		if (itr->chainid == 0)continue;
		if (itr->base.rbegin()->pl <= vertex_pl) {
			forward_chain.push_back(*itr);
		}
		else {
			backward_chain.push_back(*itr);
		}
	}
	std::vector<Cahin_difference>diff_v;
	for (int i = 0; i < forward_chain.size(); i++) {
		for (int j = i + 1; j < forward_chain.size(); j++) {
			Cahin_difference diff;
			diff.groupid = ev.groupid;
			diff.direction = 1;
			diff.c[0] = forward_chain[i];
			diff.c[1] = forward_chain[j];
			diff.chainid[0] = forward_chain[i].chainid;
			diff.chainid[1] = forward_chain[j].chainid;
			diff.ax[0] = forward_chain[i].base.rbegin()->ax;
			diff.ay[0] = forward_chain[i].base.rbegin()->ay;
			diff.ax[1] = forward_chain[j].base.rbegin()->ax;
			diff.ay[1] = forward_chain[j].base.rbegin()->ay;
			double angle = sqrt(diff.ax[0] * diff.ax[0] + diff.ay[0] * diff.ay[0]);
			double dax = forward_chain[j].base.rbegin()->ax - forward_chain[i].base.rbegin()->ax;
			double day = forward_chain[j].base.rbegin()->ay - forward_chain[i].base.rbegin()->ay;
			double dx = forward_chain[j].base.rbegin()->x - forward_chain[i].base.rbegin()->x;
			double dy = forward_chain[j].base.rbegin()->y - forward_chain[i].base.rbegin()->y;

			if (angle < 0.01) {
				diff.dal = dax;
				diff.dar = day;
				diff.dl = dx;
				diff.dr = dy;
			}
			else {
				diff.dal = (dax* diff.ay[0] - day * diff.ax[0]) / angle;
				diff.dar = (dax* diff.ax[0] + day * diff.ay[0]) / angle;
				diff.dl = (dx* diff.ay[0] - dy * diff.ax[0]) / angle;
				diff.dr = (dx* diff.ax[0] + dy * diff.ay[0]) / angle;
			}

			if (forward_chain[i].base.rbegin()->pl != forward_chain[j].base.rbegin()->pl)continue;
			diff_v.push_back(diff);
		}

	}
	for (int i = 0; i < backward_chain.size(); i++) {
		for (int j = i + 1; j < backward_chain.size(); j++) {
			Cahin_difference diff;
			diff.groupid = ev.groupid;
			diff.direction = -1;
			diff.c[0] = backward_chain[i];
			diff.c[1] = backward_chain[j];
			diff.chainid[0] = backward_chain[i].chainid;
			diff.chainid[1] = backward_chain[j].chainid;
			diff.ax[0] = backward_chain[i].base.begin()->ax;
			diff.ay[0] = backward_chain[i].base.begin()->ay;
			diff.ax[1] = backward_chain[j].base.begin()->ax;
			diff.ay[1] = backward_chain[j].base.begin()->ay;
			double angle = sqrt(diff.ax[0] * diff.ax[0] + diff.ay[0] * diff.ay[0]);
			double dax = backward_chain[j].base.begin()->ax - backward_chain[i].base.begin()->ax;
			double day = backward_chain[j].base.begin()->ay - backward_chain[i].base.begin()->ay;
			double dx = backward_chain[j].base.begin()->x - backward_chain[i].base.begin()->x;
			double dy = backward_chain[j].base.begin()->y - backward_chain[i].base.begin()->y;

			if (angle < 0.01) {
				diff.dal = dax;
				diff.dar = day;
				diff.dl = dx;
				diff.dr = dy;
			}
			else {
				diff.dal = (dax* diff.ay[0] - day * diff.ax[0]) / angle;
				diff.dar = (dax* diff.ax[0] + day * diff.ay[0]) / angle;
				diff.dl = (dx* diff.ay[0] - dy * diff.ax[0]) / angle;
				diff.dr = (dx* diff.ax[0] + dy * diff.ay[0]) / angle;
			}

			if (backward_chain[i].base.begin()->pl != backward_chain[j].base.begin()->pl)continue;
			diff_v.push_back(diff);
		}

	}
	return diff_v;

}
void output_diff(std::ofstream &ofs, std::vector<Cahin_difference>&diff) {
	for (auto &d : diff) {
		ofs << std::right << std::fixed
			<< std::setw(5) << std::setprecision(0) << d.groupid << " "
			<< std::setw(5) << std::setprecision(0) << d.chainid[0] << " "
			<< std::setw(5) << std::setprecision(0) << d.chainid[1] << " "
			<< std::setw(7) << std::setprecision(4) << d.ax[0] << " "
			<< std::setw(7) << std::setprecision(4) << d.ay[0] << " "
			<< std::setw(7) << std::setprecision(4) << d.ax[1] << " "
			<< std::setw(7) << std::setprecision(4) << d.ay[1] << " "
			<< std::setw(7) << std::setprecision(4) << d.dal << " "
			<< std::setw(7) << std::setprecision(4) << d.dar << " "
			<< std::setw(8) << std::setprecision(1) << d.dl << " "
			<< std::setw(8) << std::setprecision(1) << d.dr << std::endl;
	}
}

std::set<int> Decide_remove_chain(std::vector<Cahin_difference>&diff) {
	std::set<int> ret;
	for (auto &d : diff) {
		if (fabs(d.dal) > 0.025)continue;
		if (fabs(d.dar)> 0.2)continue;
		if (fabs(d.dl) > 5)continue;
		if (fabs(d.dr) > 60)continue;

		if (d.c[0].base.size() == d.c[1].base.size()) {
			if (d.direction == 1) {
				if ((d.c[0].base.rbegin()->m[0].ph + d.c[0].base.rbegin()->m[1].ph) % 10000 <
					(d.c[1].base.rbegin()->m[0].ph + d.c[1].base.rbegin()->m[1].ph) % 10000) {
					ret.insert(d.chainid[0]);
				}
				else {
					ret.insert(d.chainid[1]);
				}
			}
			else {
				if ((d.c[0].base.begin()->m[0].ph + d.c[0].base.begin()->m[1].ph) % 10000 <
					(d.c[1].base.begin()->m[0].ph + d.c[1].base.begin()->m[1].ph) % 10000) {
					ret.insert(d.chainid[0]);
				}
				else {
					ret.insert(d.chainid[1]);
				}
			}
		}
		else if (d.c[0].base.size() > d.c[1].base.size()) {
			ret.insert(d.chainid[1]);
		}
		else {
			ret.insert(d.chainid[0]);
		}
	}
	return ret;
}
