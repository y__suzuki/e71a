#include <fstream>
#include <vector>
#include <string>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <iostream>
#include <algorithm>
#include<sstream>
#include <map>

//num = 195252453
#define TRACK_NUM 200000000
class SimpleMCBaseTrackCollection {
public:
	int particle_id, plate_id, ecc_id;
	double energy_deposit_1, energy_deposit_2, ax, ay, x, y, z;
	int track_id, event_id, file_id;
	double weight;
};
bool sort_track(const SimpleMCBaseTrackCollection &left, const SimpleMCBaseTrackCollection &right) {
	return left.plate_id < right.plate_id;
}
bool sort_track_inv(const SimpleMCBaseTrackCollection &left, const SimpleMCBaseTrackCollection &right) {
	return left.plate_id > right.plate_id;
}
std::vector<SimpleMCBaseTrackCollection> read_file(std::string filname);
std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>>track_divide(std::vector<SimpleMCBaseTrackCollection>&track);

void output_energy_deposit(std::string filename, std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>> &track);
std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>>select_stop(std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>>&track_map);

bool judge_track_dir(std::vector<SimpleMCBaseTrackCollection> &t_vec);
void output_energy_deposit_range_pid(std::string filename, std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>> &track, double ang_min, double ang_max, int pid);

int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "prg filename\n");
		exit(1);
	}
	std::string file_in_bin = argv[1];

	std::vector<SimpleMCBaseTrackCollection> mc_track = read_file(file_in_bin);
	std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>>mc_track_event = track_divide(mc_track);
	mc_track.clear();
	mc_track.shrink_to_fit();

	mc_track_event = select_stop(mc_track_event);
	double angle_min, angle_max;
	for (int i = 0; i < 20; i++) {
		angle_min = i * 0.2;
		angle_max = (i + 1)*0.2;
		std::stringstream filename;
		filename << "dedx_"
			<< std::setw(2) << std::setfill('0') << angle_min * 10 << "_"
			<< std::setw(2) << std::setfill('0') << angle_max * 10 << "_2212.txt";
		output_energy_deposit_range_pid(filename.str(), mc_track_event, angle_min, angle_max, 2212);
	}

	for (int i = 0; i < 20; i++) {
		angle_min = i * 0.2;
		angle_max = (i + 1)*0.2;
		std::stringstream filename;
		filename << "dedx_"
			<< std::setw(2) << std::setfill('0') << angle_min * 10 << "_"
			<< std::setw(2) << std::setfill('0') << angle_max * 10 << "_0211.txt";
		output_energy_deposit_range_pid(filename.str(), mc_track_event, angle_min, angle_max, 211);
	}

}

std::vector<SimpleMCBaseTrackCollection> read_file(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	std::vector<SimpleMCBaseTrackCollection> ret;
	ret.reserve(TRACK_NUM);

	SimpleMCBaseTrackCollection trk;
	while (ifs.read((char*)& trk, sizeof(SimpleMCBaseTrackCollection))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(trk);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;
}

std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>>track_divide(std::vector<SimpleMCBaseTrackCollection>&track) {

	std::multimap<std::tuple<int, int, int>, SimpleMCBaseTrackCollection*> track_multimap;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		track_multimap.insert(std::make_pair(std::make_tuple(itr->file_id, itr->event_id, itr->track_id), &(*itr)));
	}

	std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>> ret;
	int count = 0;
	int all = track_multimap.size(), now = 0, loop_num = 0;
	std::vector<SimpleMCBaseTrackCollection> t_vec;
	for (auto itr = track_multimap.begin(); itr != track_multimap.end(); itr++) {
		count = track_multimap.count(itr->first);
		t_vec.clear();
		t_vec.reserve(count);

		if (loop_num % 10000 == 0) {
			printf("\r track divide %10d/%10d (%4.1lf%%)", now, all, now*100. / all);
		}
		loop_num++;
		now += count;

		auto range = track_multimap.equal_range(itr->first);
		for(auto res=range.first;res!=range.second;res++){
			t_vec.push_back(*(res->second));
		}
		sort(t_vec.begin(), t_vec.end(), sort_track);
		//�t����
		if (!judge_track_dir(t_vec)) {
			sort(t_vec.begin(), t_vec.end(), sort_track_inv);
		}
		ret.insert(std::make_pair(itr->first, t_vec));
		itr = std::next(itr, count - 1);
	}
	printf("\r track divide %10d/%10d (%4.1lf%%)\n", now, all, now*100. / all);

	return ret;
}
bool judge_track_dir(std::vector<SimpleMCBaseTrackCollection> &t_vec) {
	
	int count = t_vec.size() / 2 - 1;
	if (count < 1)return true;

	double de_sum0 = 0;
	for (int i = 0; i < count; i++) {
		de_sum0 += t_vec[i].energy_deposit_1;
		de_sum0 += t_vec[i].energy_deposit_2;
	}

	double de_sum1 = 0;
	for (int i = 0; i < count; i++) {
		de_sum1 += t_vec[t_vec.size()-1-i].energy_deposit_1;
		de_sum1 += t_vec[t_vec.size() - 1 - i].energy_deposit_2;
	}

	return de_sum0 > de_sum1;
}
std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>>select_stop(std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>>&track_map) {

	std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>> ret;


	std::vector<std::vector<SimpleMCBaseTrackCollection>>track_v;
	double xmin, ymin, xmax, ymax, zmin, zmax;
	int count = 0;
	for (auto itr = track_map.begin(); itr != track_map.end(); itr++) {
		if (itr->second.begin()->ecc_id != 5)continue;

		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (count == 0) {
				xmin = itr2->x;
				xmax = itr2->x;
				ymin = itr2->y;
				ymax = itr2->y;
				zmin = itr2->z;
				zmax = itr2->z;
				count++;
			}
			xmin = std::min(itr2->x, xmin);
			xmax = std::max(itr2->x, xmax);
			ymin = std::min(itr2->y, ymin);
			ymax = std::max(itr2->y, ymax);
			zmin = std::min(itr2->z, zmin);
			zmax = std::max(itr2->z, zmax);
		}

	}

	printf("ECC5\n");
	printf("%.1lf %.1lf %.1lf %.1lf\n", xmin, xmax, ymin, ymax);
	printf("%8.1lf %8.1lf\n", xmax - xmin, ymax - ymin);

	bool flg = false;
	double mergin = 10000;
	for (auto itr = track_map.begin(); itr != track_map.end(); itr++) {
		if (itr->second.begin()->ecc_id != 5)continue;
		flg = true;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (itr2->plate_id == 1)flg = false;
			if (itr2->plate_id == 133)flg = false;
			if (itr2->x < xmin + mergin)flg = false;
			if (itr2->x > xmax - mergin)flg = false;
			if (itr2->y < ymin + mergin)flg = false;
			if (itr2->y > ymax - mergin)flg = false;
		}
		if (flg) {
			ret.insert(*itr);
		}
	}
	return ret;

}

void output_energy_deposit(std::string filename, std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>> &track) {
	std::ofstream ofs(filename);

	double dep_sum1, dep_sum2;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(0) << itr->second.begin()->particle_id << " "
			<< std::setw(5) << std::setprecision(0) << itr->second.begin()->plate_id << " "
			<< std::setw(5) << std::setprecision(0) << itr->second.rbegin()->plate_id << " "
			<< std::setw(7) << std::setprecision(4) << itr->second.rbegin()->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->second.rbegin()->ay << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.begin()->energy_deposit_1 << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.begin()->energy_deposit_2 << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.rbegin()->energy_deposit_1 << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.rbegin()->energy_deposit_2 << " "
			<< std::setw(8) << std::setprecision(4) << itr->second.begin()->weight << std::endl;
	}
}
void output_energy_deposit_range_pid(std::string filename, std::map<std::tuple<int, int, int>, std::vector<SimpleMCBaseTrackCollection>> &track, double ang_min, double ang_max, int pid) {

	//x-range
	//y-de/dx
	int x_bin = 133;
	int y_bin = 500;
	double x_min = 0.5, x_max = 133.5;
	double y_min = 0, y_max = 0.6;

	double x_step = (x_max - x_min) / x_bin;
	double y_step = (y_max - y_min) / y_bin;

	std::map<std::pair<int, int>, double> data;


	double x, y, z;
	double range, angle;
	int npl,pl0;
	std::pair<int, int> id;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		x = itr->second.begin()->x;
		y = itr->second.begin()->y;
		z = itr->second.begin()->z;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			npl = abs(itr->second.begin()->plate_id - itr2->plate_id) + 1;
			range = sqrt(pow(x - itr2->x, 2) + pow(y - itr2->y, 2) + pow(z - itr2->z, 2));
			angle = sqrt(itr2->ax*itr2->ax + itr2->ay*itr2->ay);
			if (itr2->particle_id != pid)continue;
			if (angle < ang_min)continue;
			if (ang_max <= angle)continue;
			//id.first = (range - x_min) / x_step;
			id.first = (npl - x_min) / x_step;
			id.second = ((itr2->energy_deposit_1 + itr2->energy_deposit_2) - y_min) / y_step;
			auto res = data.insert(std::make_pair(id, itr2->weight));
			if (!res.second) {
				res.first->second += itr2->weight;
			}
		}
	}

	std::ofstream ofs(filename);
	ofs << std::right << std::fixed
		<< std::setw(8) << std::setprecision(0) << x_bin << " "
		<< std::setw(8) << std::setprecision(1) << x_min << " "
		<< std::setw(8) << std::setprecision(1) << x_max << " "
		<< std::setw(8) << std::setprecision(0) << y_bin << " "
		<< std::setw(8) << std::setprecision(3) << y_min << " "
		<< std::setw(8) << std::setprecision(3) << y_max << std::endl;
	for (auto itr = data.begin(); itr != data.end(); itr++) {
		if (itr->first.first >= x_bin)continue;
		if (itr->first.second >= y_bin)continue;
		if (itr->first.first < 0)continue;
		if (itr->first.second < 0)continue;
		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(0) << itr->first.first << " "
			<< std::setw(8) << std::setprecision(0) << itr->first.second << " "
			<< std::setw(12) << std::setprecision(4) << itr->second << std::endl;
	}


}