#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>

class Mfile_Area {
public:
	std::map<int, double> x_min, y_min, x_max, y_max, z;
};

uint64_t mfile_size(const mfile1::MFile_minimum &m_all);
Mfile_Area Mfile_area(mfile1::MFile_minimum &m);
std::vector<uint64_t> chain_id_list(const mfile1::MFile_minimum &m_all);
std::vector<uint64_t> divide_downstream_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, int PL_thr);
std::vector<uint64_t> divide_upstream_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, int PL_thr);

std::vector<uint64_t> divide_up_edgeout_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, const Mfile_Area &area, double edge_thr);
bool judge_up_edgeout(const std::vector<mfile1::MFileBase> &base, const Mfile_Area &area, double edge_cut);
std::vector<uint64_t> divide_down_edgeout_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, const Mfile_Area &area, double edge_thr);
bool judge_down_edgeout(const std::vector<mfile1::MFileBase> &base, const Mfile_Area &area, double edge_cut);

std::vector<uint64_t> short_thin_track_selection(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all);
bool VPH_cut(const std::vector<mfile1::MFileBase> &base);

void mfile_wrtie(std::string filename, mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &veto0, const std::vector<uint64_t> &veto1, const std::vector<uint64_t> &veto2);


int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:prg in-mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];

	mfile1::MFile_minimum m;
	mfile1::read_mfile(file_in_mfile, m);
