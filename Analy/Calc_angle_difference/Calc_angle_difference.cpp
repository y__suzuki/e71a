#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <chrono>

#include <FILE_structure.hpp>

class output_format_micro {
public:
	int pos, view, imager, zone, isg, ph, vph, px;
};
class output_format_base {
public:
	int pl, rawid;
	double ax, ay, x, y, z;
	output_format_micro m[2];
};

class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};

std::vector<output_format_link> read_link_bin(std::string filename);
std::set<std::pair<int, int>> rawid_pickup(std::vector<mfile0::M_Chain> &c, int pl[2]);
std::vector<output_format_link> link_pickup(std::vector<output_format_link>&link, std::set<std::pair<int, int>>&raw);
void output_linklet_inf(std::string filename, std::vector<output_format_link>&link);


int main(int argc, char** argv) {
	if (argc != 4) {
		fprintf(stderr, "usage :filename\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_linklet = argv[2];
	std::string file_out = argv[3];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::vector<output_format_link> link=read_link_bin(file_in_linklet);

	int pl[2];
	pl[0] = link.begin()->b[0].pl;
	pl[1] = link.begin()->b[1].pl;
	std::set<std::pair<int, int>> raw_pair = rawid_pickup(m.chains, pl);

	link = link_pickup(link, raw_pair);
	output_linklet_inf(file_out, link);
}
std::vector<output_format_link> read_link_bin(std::string filename) {
	std::vector<output_format_link> link;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	output_format_link l;
	while (ifs.read((char*)& l, sizeof(output_format_link))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		link.emplace_back(l);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return link;
}

std::set<std::pair<int, int>> rawid_pickup(std::vector<mfile0::M_Chain> &c, int pl[2]) {
	std::set<std::pair<int, int>> ret;
	bool flg[2];
	std::pair<int, int> rawid;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		flg[0] = false;
		flg[1] = false;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (itr2->pos / 10 == pl[0]) {
				rawid.first = itr2->rawid;
				flg[0] = true;
			}
			if (itr2->pos / 10 == pl[1]) {
				rawid.second = itr2->rawid;
				flg[1] = true;
			}
		}
		ret.insert(rawid);
	}

	return ret;
}

std::vector<output_format_link> link_pickup(std::vector<output_format_link>&link, std::set<std::pair<int, int>>&raw) {
	std::vector<output_format_link> ret;
	std::pair<int, int>id;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		id.first = itr->b[0].rawid;
		id.second = itr->b[1].rawid;
		if (raw.count(id) == 0)continue;
		ret.push_back(*itr);

	}
	return ret;
}

void output_linklet_inf(std::string filename, std::vector<output_format_link>&link) {

	std::ofstream ofs(filename);

	double diff_rad,diff_lat;
	double v[3], r[3], l[3], v1[3], fact;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (sqrt(pow(itr->b[1].ax, 2) + pow(itr->b[1].ay, 2)) < 0.01)continue;
		//方向vecの定義
		//下流
		v1[0] = itr->b[0].ax;
		v1[1] = itr->b[0].ay;
		v1[2] = 1;
		//上流
		v[0] = itr->b[1].ax;
		v[1] = itr->b[1].ay;
		v[2] = 1;

		l[0] = -1 * v[1];
		l[1] = v[0];
		l[2] = 0;

		r[0] = -1 * v[0];
		r[1] = -1 * v[1];
		r[2] = v[0] * v[0] + v[1] * v[1];

		//単位vector化
		fact = 1. / (sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]));
		v[0] = v[0] * fact;
		v[1] = v[1] * fact;
		v[2] = v[2] * fact;

		fact = 1. / (sqrt(l[0] * l[0] + l[1] * l[1] + l[2] * l[2]));
		l[0] = l[0] * fact;
		l[1] = l[1] * fact;
		l[2] = l[2] * fact;

		fact = 1. / (sqrt(r[0] * r[0] + r[1] * r[1] + r[2] * r[2]));
		r[0] = r[0] * fact;
		r[1] = r[1] * fact;
		r[2] = r[2] * fact;

		diff_rad = atan((v1[0] * r[0] + v1[1] * r[1] + v1[2] * r[2]) / (v1[0] * v[0] + v1[1] * v[1] + v1[2] * v[2]));
		diff_lat = atan((v1[0] * l[0] + v1[1] * l[1] + v1[2] * l[2]) / (v1[0] * v[0] + v1[1] * v[1] + v1[2] * v[2]));

		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(4) << itr->b[1].ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->b[1].ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[1].x << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[1].y << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[1].z << " "
			<< std::setw(8) << std::setprecision(4) << itr->b[0].ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->b[0].ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[0].x << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[0].y << " "
			<< std::setw(10) << std::setprecision(1) << itr->b[0].z << " "
			<< std::setw(8) << std::setprecision(6) << diff_rad << " "
			<< std::setw(8) << std::setprecision(6) << diff_lat << std::endl;
	}

}