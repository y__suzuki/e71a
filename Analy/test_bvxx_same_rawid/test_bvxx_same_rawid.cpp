#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
std::vector<netscan::base_track_t>  smae_rawid(std::vector<netscan::base_track_t> &base1, std::vector<netscan::base_track_t> & base2);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "prg input-bvxx input-bvxx pl zone output_bvxx\n");
		exit(1);
	}
	std::string file_in_bvxx1 = argv[1];
	std::string file_in_bvxx2 = argv[2];
	int pl = std::stoi(argv[3]);
	int zone = std::stoi(argv[4]);
	std::string file_out_bvxx = argv[5];

	std::vector<netscan::base_track_t> base1;
	std::vector<netscan::base_track_t> base2;
	netscan::read_basetrack_extension(file_in_bvxx1, base1, pl, zone);
	netscan::read_basetrack_extension(file_in_bvxx2, base2, pl, zone);

	std::vector<netscan::base_track_t> base_out = smae_rawid(base1, base2);
	netscan::write_basetrack_vxx(file_out_bvxx, base_out, pl, zone);
}
std::vector<netscan::base_track_t>  smae_rawid(std::vector<netscan::base_track_t> &base1, std::vector<netscan::base_track_t> & base2) {
	std::vector<netscan::base_track_t>ret;

	std::map<std::pair<int,int>, netscan::base_track_t> base_map;
	for (auto itr = base2.begin(); itr != base2.end(); itr++) {
		base_map.insert(std::make_pair(std::make_pair(itr->m[0].rawid,itr->m[1].rawid), *itr));
	}
	for (auto itr= base1.begin(); itr != base1.end(); itr++) {
		if (base_map.count(std::make_pair(itr->m[0].rawid, itr->m[1].rawid)) == 0)continue;
		ret.push_back(base_map.find(std::make_pair(itr->m[0].rawid, itr->m[1].rawid))->second);
	}
	return ret;
}
