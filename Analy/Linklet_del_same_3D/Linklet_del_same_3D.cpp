#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"my_algorithm.lib")
#pragma comment(lib,"functions.lib")

#include <FILE_structure.hpp>
#include <my_algorithm.hpp>
#include <functions.hpp>
#include <set>


class output_format_micro {
public:
	int pos, view, imager, zone, isg, ph, vph, px;
};
class output_format_base {
public:
	int pl, rawid;
	double ax, ay, x, y, z;
	output_format_micro m[2];
};

class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};

class Linklet_val {
public:
	double val, dr, dl;
	int flg;
	std::vector<output_format_link> link;
};
bool sort_id0(const output_format_link& left, const output_format_link& right) {
	return left.b[0].rawid == right.b[0].rawid ? left.b[1].rawid < right.b[1].rawid : left.b[0].rawid < right.b[0].rawid;
}
bool sort_id1(const output_format_link& left, const output_format_link& right) {
	return left.b[1].rawid == right.b[1].rawid ? left.b[0].rawid < right.b[0].rawid : left.b[1].rawid < right.b[1].rawid;
}
std::vector<output_format_link> read_linklet_bin(std::string filename);
std::vector<std::pair<int, int>> multi_id_pair(std::vector<output_format_link> link, std::vector<std::pair<int, int>>&single);
void output_pair_bin(std::string filename, std::vector<output_format_link>&link);

std::vector<output_format_link> LinkleConvolution(std::vector<output_format_link> link);
std::vector<std::vector<output_format_link>> LinkletConbination(std::vector<output_format_link> link, int LInkletnum);
std::vector<std::vector<std::pair<int, int>>> IDConbination(std::vector<int> id0, std::vector<int> id1);
std::vector<std::vector<std::pair<int, int>>> RemoveException(std::vector<std::vector<std::pair<int, int>>> all_pair, std::vector<std::pair<int, int>> exception_pair);
std::vector<output_format_link> SelectLinklet(std::vector<std::vector<output_format_link>> link);
std::vector<output_format_link> SelectBestLinklet(std::vector<output_format_link> link, int Linkletnum);
std::vector<output_format_link> LinkletDelete(std::vector<output_format_link> link, std::vector<output_format_link> del);
int Permutation(int n, int r);
void Permutation(int n, int r, int &step, std::vector<std::vector<int>> &per);

std::vector<Linklet_val> LinkleConvolution_val(std::vector<output_format_link> link);
std::vector<Linklet_val> SelectLinklet_val(std::vector<std::vector<output_format_link>> link);
void Calc_ConnectionValue(std::vector<output_format_link> link, double dr_sigma, double dl_sigma, Linklet_val &ret);
void position_difference(netscan::linklet_t link, double &dr, double &dl);
void Calc_sigma(double angle, double gap, double &sigma_dr, double &sigma_dl);
std::vector<Linklet_val> SelectBestLinklet_val(std::vector<output_format_link> link, int Linkletnum);
double Calc_ConnectionValue(output_format_link link);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:in-link(bin) out-link(bin)\n");
		exit(1);
	}

	std::string in_file_link = argv[1];
	std::string out_file_link = argv[2];

	//std::vector<netscan::linklet_t> link;
	//std::vector<netscan::linklet_t> link_out;
	//netscan::read_linklet_bin(in_file_link, link);
	std::vector<output_format_link> link, link_out;
	link = read_linklet_bin(in_file_link);

	std::multimap<int, output_format_link> link_raw0;
	std::multimap<int, output_format_link> link_raw1;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		link_raw0.insert(std::make_pair(itr->b[0].rawid, *itr));
		link_raw1.insert(std::make_pair(itr->b[1].rawid, *itr));
	}


	std::vector<std::pair<int, int>> multi, single;

	multi = multi_id_pair(link, single);
	for (auto itr = single.begin(); itr != single.end(); itr++) {
		link_out.push_back(link_raw0.find(itr->first)->second);
	}

	std::vector<id_pair> pair = id_clustering(multi);

	int count = 0;
	std::map<std::pair<int, int>, int> multi_count;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		count++;
		if (count % 100 == 0) {
			printf("\r count %d / %d", count, pair.size());
		}
		std::map<std::pair<int, int>, output_format_link> out_link_map;
		for (auto itr2 = itr->rawid0.begin(); itr2 != itr->rawid0.end(); itr2++) {
			if (link_raw0.count(*itr2) == 0) {
				fprintf(stderr, "exception base not found\n");
				exit(1);
			}
			else if (link_raw0.count(*itr2) == 1) {
				auto res = link_raw0.find(*itr2);
				out_link_map.insert(std::make_pair(std::make_pair(res->second.b[0].rawid, res->second.b[1].rawid), res->second));

			}
			else {
				auto range = link_raw0.equal_range(*itr2);
				for (auto itr3 = range.first; itr3 != range.second; itr3++) {
					out_link_map.insert(std::make_pair(std::make_pair(itr3->second.b[0].rawid, itr3->second.b[1].rawid), itr3->second));
				}
			}
		}
		for (auto itr2 = itr->rawid1.begin(); itr2 != itr->rawid1.end(); itr2++) {
			if (link_raw1.count(*itr2) == 0) {
				fprintf(stderr, "exception base not found\n");
				exit(1);
			}
			else if (link_raw1.count(*itr2) == 1) {
				auto res = link_raw1.find(*itr2);
				out_link_map.insert(std::make_pair(std::make_pair(res->second.b[0].rawid, res->second.b[1].rawid), res->second));

			}
			else {
				auto range = link_raw1.equal_range(*itr2);
				for (auto itr3 = range.first; itr3 != range.second; itr3++) {
					out_link_map.insert(std::make_pair(std::make_pair(itr3->second.b[0].rawid, itr3->second.b[1].rawid), itr3->second));
				}
			}
		}

		std::vector<output_format_link> out_link;
		for (auto itr2 = out_link_map.begin(); itr2 != out_link_map.end(); itr2++) {
			out_link.push_back(itr2->second);
		}
		//basetrack本数のカウント
		//auto res = multi_count.insert(std::make_pair(std::make_pair(std::min(itr->rawid0.size(), itr->rawid1.size()), std::max(itr->rawid0.size(), itr->rawid1.size())), 1));
		//if (!res.second) {
		//	res.first->second++;
		//}

		std::vector<Linklet_val> link_val = LinkleConvolution_val(out_link);
		for (auto itr2 = link_val.begin(); itr2 != link_val.end(); itr2++) {
			if (itr2->flg) {
				for (auto itr3 = itr2->link.begin(); itr3 != itr2->link.end(); itr3++) {
					link_out.push_back(*itr3);
				}
			}
		}
		//1groupをout_linkにまとめた
		//convolution
		//std::vector<netscan::linklet_t> link_sel = LinkleConvolution(out_link);
	}
	printf("\r count %d / %d\n", count, pair.size());

	printf("multi del %d --> %d\n", link.size(), link_out.size());
	output_pair_bin(out_file_link, link_out);

	//for (auto itr = multi_count.begin(); itr != multi_count.end(); itr++) {
	//	printf("%5d %5d %10d\n", itr->first.first, itr->first.second, itr->second);
	//}
}
std::vector<output_format_link> read_linklet_bin(std::string filename) {
	std::vector<output_format_link> ret;

	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	output_format_link l;
	while (ifs.read((char*)& l, sizeof(output_format_link))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(l);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;

}
std::vector<std::pair<int, int>> multi_id_pair(std::vector<output_format_link> link, std::vector<std::pair<int, int>>&single) {
	std::set <int> single_cand0;
	std::multimap <int, int> single_cnad1;
	std::set <std::pair<int, int>> single_pair;

	std::multimap <int, int> rawid0;
	std::multimap <int, int> rawid1;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		rawid0.insert(std::make_pair(itr->b[0].rawid, itr->b[1].rawid));
		rawid1.insert(std::make_pair(itr->b[1].rawid, itr->b[0].rawid));
	}

	for (auto itr = rawid0.begin(); itr != rawid0.end(); itr++) {
		if (rawid0.count(itr->first) == 1) {
			single_cand0.insert(itr->second);
		}
	}
	for (auto itr = single_cand0.begin(); itr != single_cand0.end(); itr++) {
		if (rawid1.count(*itr) == 1) {
			auto res = rawid1.find(*itr);
			single_pair.insert(std::make_pair(res->second, res->first));
		}
	}


	std::vector<std::pair<int, int>> ret;

	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (single_pair.count(std::make_pair(itr->b[0].rawid, itr->b[1].rawid)) == 0) {
			ret.push_back(std::make_pair(itr->b[0].rawid, itr->b[1].rawid));
		}
		else {
			single.push_back(std::make_pair(itr->b[0].rawid, itr->b[1].rawid));
		}
	}
	printf("all    =%d\n", link.size());
	printf("single =%d\n", single.size());
	printf("multi  =%d\n", ret.size());

	printf("multi select %d --> %d (%4.1lf%%)\n", link.size(), ret.size(), ret.size()*100. / link.size());

	return ret;
}
std::vector<Linklet_val> LinkleConvolution_val(std::vector<output_format_link> link) {
	std::vector<Linklet_val> ret;

	//最終的に残すlinkletの本数
	int Linkletnum, BaseNum0 = 0, BaseNum1 = 0, id_tmp = -1;
	sort(link.begin(), link.end(), sort_id0);
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (id_tmp != itr->b[0].rawid) {
			BaseNum0++;
			id_tmp = itr->b[0].rawid;
		}
	}
	id_tmp = -1;
	sort(link.begin(), link.end(), sort_id1);
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (id_tmp != itr->b[1].rawid) {
			BaseNum1++;
			id_tmp = itr->b[1].rawid;
		}
	}
	Linkletnum = std::min(BaseNum0, BaseNum1);
	/*
	for (int i = 0; i < link.size(); i++) {
		printf("%d %d\n", link[i].b[0].rawid, link[i].b[1].rawid);
	}
	printf("Linknum = %d %d %d\n", Linkletnum, BaseNum0 , BaseNum1);
	*/

	//Linkletの決定
	//組み合わせ数(計算量)はmaxのほうに依存。最高max!
	if (Linkletnum == 1 || 8 < std::max(BaseNum0, BaseNum1)) {
		ret = SelectBestLinklet_val(link, Linkletnum);
	}
	else {
		std::vector<std::vector<output_format_link>> link_tmp;
		link_tmp = LinkletConbination(link, Linkletnum);
		ret = SelectLinklet_val(link_tmp);
	}


	return ret;
}

std::vector<output_format_link> LinkleConvolution(std::vector<output_format_link> link) {
	//最終的に残すlinkletの本数
	int Linkletnum, BaseNum0 = 0, BaseNum1 = 0, id_tmp = -1;
	sort(link.begin(), link.end(), sort_id0);
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (id_tmp != itr->b[0].rawid) {
			BaseNum0++;
			id_tmp = itr->b[0].rawid;
		}
	}
	id_tmp = -1;
	sort(link.begin(), link.end(), sort_id1);
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (id_tmp != itr->b[1].rawid) {
			BaseNum1++;
			id_tmp = itr->b[1].rawid;
		}
	}
	Linkletnum = std::min(BaseNum0, BaseNum1);
	/*
	for (int i = 0; i < link.size(); i++) {
		printf("%d %d\n", link[i].b[0].rawid, link[i].b[1].rawid);
	}
	printf("Linknum = %d %d %d\n", Linkletnum, BaseNum0 , BaseNum1);
	*/
	std::vector<output_format_link> ret;
	//Linkletの決定
	if (Linkletnum == 1 || 8 < std::max(BaseNum0, BaseNum1)) {
		ret = SelectBestLinklet(link, Linkletnum);
	}
	else {
		std::vector<std::vector<output_format_link>> link_tmp;
		link_tmp = LinkletConbination(link, Linkletnum);
		ret = SelectLinklet(link_tmp);
	}

	return ret;
}

//LinkletConbination(IDConbination,Permutation,RemoveException)+SelectLinklet でlinkletの選択
//unique　linkletの組み合わせを返す
std::vector<std::vector<output_format_link>> LinkletConbination(std::vector<output_format_link> link, int LInkletnum) {
	std::vector<std::vector<output_format_link>> ret_link;

	//id0のlist
	std::vector<int> id0;
	//id1のlist
	std::vector<int> id1;
	//linkletで繋がれないid pairのlist
	std::vector<std::pair<int, int>> exception_pair;

	//id0のlist作成
	int id_tmp = -1;
	sort(link.begin(), link.end(), sort_id0);
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (id_tmp != itr->b[0].rawid) {
			id0.push_back(itr->b[0].rawid);
			id_tmp = itr->b[0].rawid;
		}
	}
	//id1のlist作成
	id_tmp = -1;
	sort(link.begin(), link.end(), sort_id1);
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (id_tmp != itr->b[1].rawid) {
			id1.push_back(itr->b[1].rawid);
			id_tmp = itr->b[1].rawid;
		}
	}
	//exception pairのlist作成
	int flg = 0;
	sort(link.begin(), link.end(), sort_id0);
	for (int i = 0; i < id0.size(); i++) {
		for (int j = 0; j < id1.size(); j++) {
			flg = 0;
			for (auto itr = link.begin(); itr != link.end() && flg == 0; itr++) {
				if (id0[i] == itr->b[0].rawid&&id1[j] == itr->b[1].rawid) {
					flg = 1;
				}
			}
			if (flg == 0) {
				exception_pair.push_back(std::make_pair(id0[i], id1[j]));
			}
		}
	}

	//printf("id0 = %d id1 = %d exception = %d\n", int(id0.size()), int(id1.size()), int(exception_pair.size()));
	std::vector<std::vector<std::pair<int, int>>> all_pair, slct_pair;
	//IDpairでできるすべての組み合わせを列挙
	all_pair = IDConbination(id0, id1);
	/*
	printf("ID Conbination\n");
	for (int i = 0; i < all_pair.size(); i++) {
		for (int j = 0; j < all_pair[i].size(); j++) {
			printf("%d %d\n", all_pair[i][j].first, all_pair[i][j].second);
		}
		printf("\n");
	}
	printf("ID Exception\n");
	for (int i = 0; i < exception_pair.size(); i++) {
		printf("%d %d\n", exception_pair[i].first, exception_pair[i].second);
	}
	printf("\n");
	*/

	//Exceprion pairを除く
	slct_pair = RemoveException(all_pair, exception_pair);
	//printf("all_pair = %d remove_pair = %d\n", int(all_pair.size()), int(slct_pair.size()));

	//
	//ここまでで、linkletの組み合わせの抽出完了。
	//
	//以下id でlinkletの抽出
	//
	//
	sort(link.begin(), link.end(), sort_id0);
	for (int i = 0; i < slct_pair.size(); i++) {
		std::vector<output_format_link> link_tmp;
		auto itr = link.begin();
		sort(slct_pair[i].begin(), slct_pair[i].end());
		for (int j = 0; j < slct_pair[i].size(); j++) {
			for (; itr != link.end(); itr++) {
				if (itr->b[0].rawid == slct_pair[i][j].first&&itr->b[1].rawid == slct_pair[i][j].second) {
					link_tmp.push_back(*itr);
					break;
				}
			}
		}
		ret_link.push_back(link_tmp);
	}
	return ret_link;
}
//nPr通りのlinkletが作成可能
//少ない方はidでソートして入れる
//nこの数字を使ってr個の配列を作成、その数字のIDを持ってくる
std::vector<std::vector<std::pair<int, int>>> IDConbination(std::vector<int> id0, std::vector<int> id1) {
	int id0_num, id1_num, link_num, conb_num;
	id0_num = id0.size();
	id1_num = id1.size();
	link_num = std::min(id0_num, id1_num);
	conb_num = Permutation(std::max(id0_num, id1_num), link_num);

	std::vector<std::vector<std::pair<int, int>>> ret;

	if (id0_num == link_num) {

		//id0<id1またはid0=id1
		sort(id0.begin(), id0.end());
		sort(id1.begin(), id1.end());

		int step = 0;
		std::vector<std::vector<int>> per;
		Permutation(id1_num, id0_num, step, per);
		for (int i = 0; i < per.size(); i++) {
			std::vector<std::pair<int, int>> pair_tmp;
			for (int j = 0; j < per[i].size(); j++) {
				pair_tmp.push_back(std::make_pair(id0[j], id1[per[i][j]]));
			}
			ret.push_back(pair_tmp);
		}
	}
	else {
		//id0>id1
		sort(id0.begin(), id0.end());
		sort(id1.begin(), id1.end());

		int step = 0;
		std::vector<std::vector<int>> per;
		Permutation(id0_num, id1_num, step, per);
		for (int i = 0; i < per.size(); i++) {
			std::vector<std::pair<int, int>> pair_tmp;
			for (int j = 0; j < per[i].size(); j++) {
				pair_tmp.push_back(std::make_pair(id0[per[i][j]], id1[j]));
			}
			ret.push_back(pair_tmp);
		}
	}

	return ret;
}

//IDConbinationで作った組み合わせに対して、ExceptionListにあるものを除く
std::vector<std::vector<std::pair<int, int>>> RemoveException(std::vector<std::vector<std::pair<int, int>>> all_pair, std::vector<std::pair<int, int>> exception_pair) {
	std::vector<std::vector<std::pair<int, int>>> ret;
	std::vector<std::vector<std::pair<int, int>>> ret_tmp;
	//exception pairの要素を削除
	int flg = 0;
	for (int i = 0; i < all_pair.size(); i++) {
		std::vector<std::pair<int, int>> ret_tmptmp;
		for (int j = 0; j < all_pair[i].size(); j++) {
			flg = 0;
			for (auto itr = exception_pair.begin(); itr != exception_pair.end(); itr++) {
				if (all_pair[i][j].first == itr->first&&all_pair[i][j].second == itr->second) {
					flg = 1;
					break;
				}
			}
			if (flg == 0) {
				ret_tmptmp.push_back(all_pair[i][j]);
			}
		}
		ret_tmp.push_back(ret_tmptmp);
	}
	int maxLink = 0;
	for (int i = 0; i < ret_tmp.size(); i++) {
		if (maxLink < ret_tmp[i].size()) {
			maxLink = ret_tmp[i].size();
		}
	}
	for (int i = 0; i < ret_tmp.size(); i++) {
		if (maxLink == ret_tmp[i].size()) {
			ret.push_back(ret_tmp[i]);
		}
	}
	return ret;
}
//Linkletの組み合わせに対して一番良いlinkletを返す
std::vector<output_format_link> SelectLinklet(std::vector<std::vector<output_format_link>> link) {
	std::vector<output_format_link> ret;
	int ret_i = -1;
	double CV = -1;
	for (int i = 0; i < link.size(); i++) {
		double CV_tmp = 0;
		for (auto itr = link[i].begin(); itr != link[i].end(); itr++) {
			//CV(Connection Value:つなぎの評価値)
			CV_tmp += itr->dx*itr->dx + itr->dy*itr->dy;
		}
		if (CV < 0 || CV_tmp < CV) {
			CV = CV_tmp;
			ret_i = i;
		}
	}
	ret = link[ret_i];
	return ret;
}
std::vector<Linklet_val> SelectLinklet_val(std::vector<std::vector<output_format_link>> link) {
	std::vector<Linklet_val> ret;
	int ret_i = -1;
	double angle, gap, sigma_dr, sigma_dl;
	int num = 0;

	for (int i = 0; i < link.size(); i++) {
		Linklet_val link_tmp;
		angle = 0;
		gap = 0;
		num = 0;
		for (auto itr = link[i].begin(); itr != link[i].end(); itr++) {
			angle += sqrt(pow(itr->b[0].ax, 2) + pow(itr->b[0].ay, 2));
			angle += sqrt(pow(itr->b[1].ax, 2) + pow(itr->b[1].ay, 2));
			gap += itr->b[0].z - itr->b[1].z;
			num += 1;
		}
		angle = angle / (num * 2);
		gap = fabs(gap / num);
		Calc_sigma(angle, gap, sigma_dr, sigma_dl);
		Calc_ConnectionValue(link[i], sigma_dr, sigma_dl, link_tmp);
		ret.push_back(link_tmp);
	}

	auto sel = ret.begin();
	double CV = ret.begin()->val;
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		if (CV > itr->val) {
			CV = itr->val;
			sel = itr;
		}
	}
	sel->flg = 1;
	return ret;
}
//connectionValueの計算
void Calc_ConnectionValue(std::vector<output_format_link> link, double dr_sigma, double dl_sigma, Linklet_val &ret) {
	ret.dl = 0;
	ret.dr = 0;
	ret.val = 0;
	double dr, dl;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		ret.link.push_back(*itr);
		ret.val += pow(itr->dr / dr_sigma, 2) + pow(itr->dl / dl_sigma, 2);
		ret.dl += fabs(itr->dl / dl_sigma);
		ret.dr += fabs(itr->dr / dr_sigma);
		//ret.val += pow(itr->dx, 2) + pow(itr->dy, 2);
		//ret.dl += fabs(itr->dx);
		//ret.dr += fabs(itr->dy);
	}
	ret.val = sqrt(ret.val);
	ret.flg = 0;
}
void position_difference(netscan::linklet_t link, double &dr, double &dl) {
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = link.b[0].x;
	pos0.y = link.b[0].y;
	pos0.z = link.b[0].z;
	dir0.x = link.b[0].ax;
	dir0.y = link.b[0].ay;
	dir0.z = 1;
	pos1.x = link.b[1].x;
	pos1.y = link.b[1].y;
	pos1.z = link.b[1].z;
	dir1.x = link.b[1].ax;
	dir1.y = link.b[1].ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);

}
void Calc_sigma(double angle, double gap, double &sigma_dr, double &sigma_dl) {
	double dr_thr;
	dr_thr = 0.012*gap;

	sigma_dr = std::min((0.0023 + 0.0037*angle)*gap, dr_thr);
	sigma_dl = gap * 0.0035;
}


//一番良いlinkletから選択してく
std::vector<Linklet_val> SelectBestLinklet_val(std::vector<output_format_link> link, int Linkletnum) {
	std::vector<Linklet_val> ret;
	int flg = 0, count = 0;
	double dr, dl;
	Linklet_val link_val;
	std::vector<output_format_link> link_tmp;
	while (flg == 0) {
		double cval_now, Cval = -1;
		flg = 0;
		std::vector<output_format_link>::iterator Citr;

		for (auto itr = link.begin(); itr != link.end(); itr++) {
			//linkletの評価に使う変数cval(connection value)

			cval_now = Calc_ConnectionValue(*itr);
			if (itr == link.begin() || cval_now < Cval) {
				Cval = cval_now;
				Citr = itr;
			}
		}
		link_tmp.push_back(*Citr);
		int Bid0 = Citr->b[0].rawid;
		int Bid1 = Citr->b[1].rawid;
		count++;

		//選ばれたlinkletで使用しているbasetrackの削除
		auto itr = link.begin();
		while (itr != link.end()) {
			if (itr->b[0].rawid == Bid0 || itr->b[1].rawid == Bid1) {
				itr = link.erase(itr);
			}
			else
			{
				itr++;
			}

		}
		if (count >= Linkletnum) {
			flg = 1;
		}
		else if (link.size() == 0) {
			//この場合最大限のlinkletを残せるようなselectionにしたい
			//最大linklet数になる組み合わせを抽出-->複数候補の場合はCvalで選択
			flg = 2;
		}
	}

	double  angle = 0;
	double gap = 0;
	int num = 0;
	for (auto itr = link_tmp.begin(); itr != link_tmp.end(); itr++) {
		angle += sqrt(pow(itr->b[0].ax, 2) + pow(itr->b[0].ay, 2));
		angle += sqrt(pow(itr->b[1].ax, 2) + pow(itr->b[1].ay, 2));
		gap += itr->b[0].z - itr->b[1].z;
		num += 1;
	}
	double sigma_dr, sigma_dl;
	angle = angle / (num * 2);
	gap = fabs(gap / num);
	Calc_sigma(angle, gap, sigma_dr, sigma_dl);
	Calc_ConnectionValue(link_tmp, sigma_dr, sigma_dl, link_val);
	link_val.flg = 1;
	ret.push_back(link_val);


	return ret;
}
double Calc_ConnectionValue(output_format_link link) {
	double angle, gap, dr, dl;
	angle = (sqrt(pow(link.b[0].ax, 2.) + pow(link.b[0].ay, 2.)) + sqrt(pow(link.b[1].ax, 2.) + pow(link.b[1].ay, 2.))) / 2.;
	gap = fabs(link.b[0].z - link.b[1].z);
	double dr_sigma, dl_sigma;
	Calc_sigma(angle, gap, dr_sigma, dl_sigma);
	return sqrt(pow(link.dr / dr_sigma, 2) + pow(link.dl / dl_sigma, 2));

}

std::vector<output_format_link> SelectBestLinklet(std::vector<output_format_link> link, int Linkletnum) {
	std::vector<output_format_link> ret;
	int flg = 0, count = 0;
	while (flg == 0) {
		double cval_now, Cval = -1;
		flg = 0;
		std::vector<output_format_link>::iterator Citr;

		for (auto itr = link.begin(); itr != link.end(); itr++) {
			//linkletの評価に使う変数cval(connection value)
			cval_now = itr->dx*itr->dx + itr->dy*itr->dy;
			if (itr == link.begin() || cval_now < Cval) {
				Cval = cval_now;
				Citr = itr;
			}
		}
		ret.push_back(*Citr);
		int Bid0 = Citr->b[0].rawid;
		int Bid1 = Citr->b[1].rawid;
		count++;

		//選ばれたlinkletで使用しているbasetrackの削除
		auto itr = link.begin();
		while (itr != link.end()) {
			if (itr->b[0].rawid == Bid0 || itr->b[1].rawid == Bid1) {
				itr = link.erase(itr);
			}
			else
			{
				itr++;
			}

		}
		if (count >= Linkletnum) {
			flg = 1;
		}
		else if (link.size() == 0) {
			//この場合最大限のlinkletを残せるようなselectionにしたい
			//最大linklet数になる組み合わせを抽出-->複数候補の場合はCvalで選択
			flg = 2;
		}
	}
	return ret;
}

std::vector<output_format_link> LinkletDelete(std::vector<output_format_link> link, std::vector<output_format_link> del) {
	sort(link.begin(), link.end(), sort_id0);
	sort(del.begin(), del.end(), sort_id0);

	std::vector<output_format_link> ret;
	auto itr_del = del.begin();
	auto itr = link.begin();
	while (itr_del != del.end()) {
		if (itr->b[0].rawid == itr_del->b[0].rawid&&itr->b[1].rawid == itr_del->b[1].rawid) {
			//完全一致でiteratorを進める
			itr++;
			itr_del++;
		}
		else {
			ret.push_back(*itr);
			itr++;
		}
	}
	while (itr != link.end()) {
		ret.push_back(*itr);
		itr++;

	}
	return ret;
}

int Permutation(int n, int r) {
	int calc = 1;
	for (int i = 0; i < r; i++) {
		calc = calc * (n - i);
	}
	return calc;
}
void Permutation(int n, int r, int &step, std::vector<std::vector<int>> &per) {
	//printf("step=%d n=%d r=%d\n", step, n, r);
	if (step == 0) {

		per.clear();
		for (int i = 0; i < n; i++) {
			std::vector<int> tmp{ i };
			per.push_back(tmp);
		}
		step++;
		Permutation(n, r, step, per);

	}
	else if (step < r) {
		//step=sの段階でnPs個の配列が生成されている
		int flg = 0;
		std::vector<std::vector<int>>tmp_v;
		std::vector<std::vector<int>>_per(per);

		for (int i = 0; i < _per.size(); i++) {
			for (int j = 0; j < n; j++) {
				flg = 0;
				for (int k = 0; k < _per[i].size(); k++) {
					if (j == _per[i][k]) {
						flg = 1;
						break;
					}
				}
				if (flg == 0) {
					std::vector<int> add_v(_per[i]);
					add_v.push_back(j);
					tmp_v.push_back(add_v);
				}
			}
		}
		per = tmp_v;
		step++;
		Permutation(n, r, step, per);
	}
	else {
		return;
	}

}
void output_pair_bin(std::string filename, std::vector<output_format_link>&link) {

	std::ofstream ofs(filename, std::ios::binary);
	int count = 0;
	for (auto l : link) {
		if (count % 10000 == 0) {
			printf("\r write linklet %10d/%10d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;
		ofs.write((char*)& l, sizeof(output_format_link));
	}

	printf("\r write linklet %10d/%10d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

}

