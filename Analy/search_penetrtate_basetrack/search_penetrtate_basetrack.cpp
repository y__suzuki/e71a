#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <filesystem>
#include <omp.h>

class basetrack_minimum {
public:
	int pl, rawid;
	float ax, ay, x, y;
	Momentum_recon::microtrack_minimum m[2];
};
class align_param {
public:
	int pl[2];
	double position[6],angle[6],dz;
};
std::vector<mfile0::M_Chain> chain_selection(std::vector<mfile0::M_Chain>&c);
void connect_pair_extraction_forward(std::vector<mfile0::M_Chain>&c, std::multimap<std::pair<int, int>, std::pair<int, int>>&path, int pl);
void connect_pair_extraction_backward(std::vector<mfile0::M_Chain>&c, std::multimap<std::pair<int, int>, std::pair<int, int>>&path, int pl);

void set_pl_rawid(std::pair<int, int>&id, Momentum_recon::Mom_chain&c);
void search_forward(Momentum_recon::Mom_basetrack&b, std::multimap<std::pair<int, int>, std::pair<int, int>>&path, std::string file_in_base, int group, int chain);
void search_backward(Momentum_recon::Mom_basetrack&b, std::multimap<std::pair<int, int>, std::pair<int, int>>&path, std::string file_in_base, int group, int chain);

std::vector< basetrack_minimum> read_base_inf(std::string file_in_base_all, int pl, bool output_flg);
align_param Calc_align(std::vector<std::pair< basetrack_minimum, basetrack_minimum>> &use_base);




void Set_m_base(std::vector<Momentum_recon::Event_information> &momch_all,
	std::map<std::pair<int, int>, std::pair<int, int>> &base_origin,
	std::map<int, std::vector<mfile0::M_Base>> &base_forward,
	std::map<int, std::vector<mfile0::M_Base>> &base_backward);
void trans_local(std::multimap<int, mfile0::M_Base*>&base_map_single, std::map<int, std::vector<corrmap_3d::align_param2>> &corr);
void set_basetrack_inf(std::map<int, std::vector<mfile0::M_Base>> &base_map, std::map<int, std::vector<corrmap_3d::align_param2>>&corrmap_dd, std::string file_in_base);

void penetrate_cand_search_forward(std::map<int, std::vector<mfile0::M_Base>> &base_forward,
	std::map<std::pair<int, int>, std::vector<mfile0::M_Base>>&pene_cand,
	std::map<int, std::vector<corrmap_3d::align_param2>>&corrmap_dd,
	std::string file_in_base);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:file-in-momch\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_in_mfile = argv[2];
	std::string file_in_base = argv[3];
	std::string file_out_mfile = argv[4];

	//momchの読み込み
	std::vector<Momentum_recon::Event_information> momch_all = Momentum_recon::Read_Event_information_extension(file_in_momch);

	//groupid 131 chainid 70の抜出
	std::vector<Momentum_recon::Event_information> momch_sel;
	for (auto itr = momch_all.begin(); itr != momch_all.end(); itr++) {
		if (itr->groupid != 210)continue;
		Momentum_recon::Event_information sel = *itr;
		sel.chains.clear();
		for (auto itr2 = itr->chains.begin(); itr2 != itr->chains.end(); itr2++) {
			if (itr2->chainid == 207 || itr2->chainid == 169 ) {
				sel.chains.push_back(*itr2);
			}
		}
		momch_sel.push_back(sel);
	}
	momch_all = momch_sel;

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	m.chains = chain_selection(m.chains);
	printf("chain selection fin\n");
	//mfile1::write_mfile_extension(file_out_mfile, m);



	for (auto itr = momch_all.begin(); itr != momch_all.end(); itr++) {
		for (auto itr2 = itr->chains.begin(); itr2 != itr->chains.end(); itr2++) {
			Momentum_recon::Mom_basetrack b;
			printf("chain %d %d\n", itr2->chainid, itr2->direction);
			std::multimap<std::pair<int, int>, std::pair<int, int>>path;
			//forward
			b = *itr2->base.rbegin();
			printf("PL%03d rawid=%d\n", b.pl, b.rawid);
			connect_pair_extraction_forward(m.chains, path, b.pl);
			search_forward(b, path, file_in_base, itr->groupid, itr2->chainid);
			//backward
			b = *itr2->base.begin();
			printf("PL%03d rawid=%d\n", b.pl, b.rawid);
			connect_pair_extraction_backward(m.chains, path, b.pl);
			search_backward(b, path, file_in_base, itr->groupid, itr2->chainid);
		}
	}


}
std::vector<mfile0::M_Chain> chain_selection(std::vector<mfile0::M_Chain>&c) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->nseg < 10)continue;
		int npl = itr->pos1 / 10 - itr->pos0 / 10 + 1;
		if (itr->nseg*1. / npl < 0.9)continue;
		//間引き
		//if (itr->chain_id % 5 != 0)continue;
		double ax, ay, div_rad, div_lat;
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		mfile0::angle_diff_dev_theta(*itr, ax, ay, div_rad, div_lat);
		if (div_lat > 0.01)continue;

		ret.push_back(*itr);


	}
	int64_t n_base[2] = {};
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		n_base[0] += itr->nseg;
	}
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		n_base[1] += itr->nseg;
	}

	printf("%lld --> %lld (%4.1lf%%)\n", n_base[0], n_base[1], n_base[1] * 100. / n_base[0]);

	return ret;
}

void connect_pair_extraction_forward(std::vector<mfile0::M_Chain>&c, std::multimap<std::pair<int, int>, std::pair<int, int>>&path, int pl) {
	int now = 0, all = c.size();
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (now % 10000 == 0) {
			printf("\r now %8d/%8d(%4.1lf%%)", now, all, now*100. / all);
		}
		now++;
		for (int i = 0; i < itr->basetracks.size(); i++) {
			if (itr->basetracks[i].pos / 10 != pl)continue;
			for (int j = i + 1; j < itr->basetracks.size() && j - i <= 3; j++) {
				if (itr->basetracks[j].pos / 10 - itr->basetracks[i].pos / 10 > 3)continue;

				//printf("PL%03d - PL%03d\n", itr->basetracks[i].pos / 10, itr->basetracks[j].pos / 10);

				path.insert(std::make_pair(
					std::make_pair(itr->basetracks[i].pos / 10, itr->basetracks[i].rawid),
					std::make_pair(itr->basetracks[j].pos / 10, itr->basetracks[j].rawid)
				));
			}
		}
	}
	printf("\r now %8d/%8d(%4.1lf%%)\n", now, all, now*100. / all);

}
void connect_pair_extraction_backward(std::vector<mfile0::M_Chain>&c, std::multimap<std::pair<int, int>, std::pair<int, int>>&path, int pl) {
	int now = 0, all = c.size();
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (now % 10000 == 0) {
			printf("\r now %8d/%8d(%4.1lf%%)", now, all, now*100. / all);
		}
		now++;
		for (int i = 0; i < itr->basetracks.size(); i++) {
			if (itr->basetracks[i].pos / 10 < pl - 3)continue;
			if (itr->basetracks[i].pos / 10 >= pl )continue;
			for (int j = i + 1; j < itr->basetracks.size() && j - i <= 3; j++) {
				if (itr->basetracks[j].pos / 10 != pl)continue;

				if (itr->basetracks[j].pos / 10 - itr->basetracks[i].pos / 10 > 3)continue;

				//printf("PL%03d - PL%03d\n", itr->basetracks[i].pos / 10, itr->basetracks[j].pos / 10);

				path.insert(std::make_pair(
					std::make_pair(itr->basetracks[j].pos / 10, itr->basetracks[j].rawid),
					std::make_pair(itr->basetracks[i].pos / 10, itr->basetracks[i].rawid)
				));
			}
		}
	}
	printf("\r now %8d/%8d(%4.1lf%%)\n", now, all, now*100. / all);

}
mfile0::M_Base format_change(basetrack_minimum&b, align_param&p,bool trans) {
	mfile0::M_Base ret;

	if (trans) {
		ret.x = b.x*p.position[0] + b.y*p.position[1] + p.position[4];
		ret.y = b.x*p.position[2] + b.y*p.position[3] + p.position[5];
		ret.ax = b.ax*p.angle[0] + b.ay*p.angle[1] + p.angle[4];
		ret.ay = b.ax*p.angle[2] + b.ay*p.angle[3] + p.angle[5];
		ret.z = p.dz;
	}
	else {
		ret.x = b.x;
		ret.y = b.y;
		ret.ax = b.ax;
		ret.ay = b.ay;
		ret.z = 0;
	}
	ret.flg_d[0] = 0;
	ret.flg_d[1] = 0;
	ret.flg_i[0] = 0;
	ret.flg_i[1] = 0;
	ret.flg_i[2] = 0;
	ret.flg_i[3] = 0;
	ret.group_id = 0;
	ret.pos = b.pl * 10 + 1;
	ret.rawid = b.rawid;
	ret.ph = b.m[0].ph + b.m[1].ph;

	return ret;

}
std::vector< basetrack_minimum> read_base_inf(std::string file_in_base_all, int pl, bool output_flg) {
	std::ifstream ifs(file_in_base_all, std::ios::binary);
	int read_pl;
	int64_t read_num;

	std::vector< basetrack_minimum> ret;
	while (1) {
		if (ifs.eof())break;
		if (!ifs.read((char*)& read_pl, sizeof(read_pl)))break;
		if (!ifs.read((char*)& read_num, sizeof(read_num)))break;


		if (pl == read_pl) {
			if (output_flg) {
				printf("Read PL%03d tracknum =%lld\n", read_pl, read_num);
			}
			ret.reserve(read_num);
			for (int64_t i = 0; i < read_num; i++) {
				basetrack_minimum b;
				ifs.read((char*)&  b, sizeof(basetrack_minimum));
				ret.push_back(b);
			}
			break;
		}
		else {
			ifs.seekg(sizeof(basetrack_minimum)*read_num, std::ios_base::cur);
		}
	}
	ifs.close();

	return ret;
}

void search_forward(Momentum_recon::Mom_basetrack&b, std::multimap<std::pair<int, int>, std::pair<int, int>>&path, std::string file_in_base, int group, int chain) {
	int pl = b.pl;
	std::vector< basetrack_minimum> base = read_base_inf(file_in_base, pl, true);
	basetrack_minimum target_base;
	target_base.rawid = -1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->rawid == b.rawid) {
			target_base = *itr;
			break;
		}
	}
	if (target_base.rawid < 0) {
		printf("PL%03d rawid = %d not found\n", b.pl, b.rawid);
	}
	else {
		printf("target base PL%03d rawid = %d\n", target_base.pl, target_base.rawid);
		printf("%7.4lf %7.4lf %8.1lf %8.1lf\n", target_base.ax, target_base.ay, target_base.x, target_base.y);
	}

	mfile0::Mfile m_out;
	mfile0::set_header(3, 133, m_out);
	int cid = 0;
	{
		mfile0::M_Chain c_tmp;
		mfile0::M_Base b[2];
		align_param param_tmp;
		b[0] = format_change(target_base, param_tmp, false);
		b[0].group_id = 2;
		c_tmp.chain_id = cid;
		cid++;
		c_tmp.basetracks.push_back(b[0]);
		c_tmp.nseg = c_tmp.basetracks.size();
		c_tmp.pos0 = c_tmp.basetracks.begin()->pos;
		c_tmp.pos1 = c_tmp.basetracks.rbegin()->pos;
		m_out.chains.push_back(c_tmp);
	}

	for (int i_pl = 1; i_pl <= 3; i_pl++) {
		int pl_next = pl + i_pl;
		printf("PL%03d --> PL%03d\n", pl, pl_next);

		std::vector<std::pair< basetrack_minimum, basetrack_minimum>> use_base;
		std::set<int> selected_id;
		selected_id.insert(target_base.rawid);

		double d_pos = 100;
		double d_ang = 0.5;
		double diff_pos, diff_ang;
		int loop_num = 0;
		//while (use_base.size() < 20 && d_pos < 10000) {
		while (use_base.size() < 40) {
			//printf("loop num=%d dpos<%.0lf um\n", loop_num, d_pos);
			loop_num++;

			for (auto itr = base.begin(); itr != base.end(); itr++) {
				if (selected_id.count(itr->rawid) == 1)continue;
				diff_pos = sqrt(pow(target_base.x - itr->x, 2) + pow(target_base.y - itr->y, 2));
				diff_ang = sqrt(pow(target_base.ax - itr->ax, 2) + pow(target_base.ay - itr->ay, 2));
				if (d_pos < diff_pos)continue;
				if (d_ang < diff_ang)continue;

				//printf("(%d,%d) in range --> ", itr->pl, itr->rawid);
				if (path.count(std::make_pair(itr->pl, itr->rawid)) == 0) {
					//printf("\n");
					continue;
				}
				else {
					//printf("path num %d\n", path.count(std::make_pair(itr->pl, itr->rawid)));
				}
				auto range = path.equal_range(std::make_pair(itr->pl, itr->rawid));
				basetrack_minimum b;
				b.pl = -1;
				b.rawid = -1;
				for (auto res = range.first; res != range.second; res++) {
					//printf("\t next pl %3d\n", res->second.first);
					if (res->second.first != pl_next)continue;

					b.pl = res->second.first;
					b.rawid = res->second.second;
					break;
				}
				if (b.rawid >= 0) {
					use_base.push_back(std::make_pair(*itr, b));
					selected_id.insert(itr->rawid);
				}
			}
			d_pos += 100;
		}
		printf("base num %d\n", use_base.size());
		if (use_base.size() < 40)continue;

		//basetrackよみこみ
		std::vector< basetrack_minimum> base = read_base_inf(file_in_base, pl_next, true);
		std::map<int, basetrack_minimum> base_m;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_m.insert(std::make_pair(itr->rawid, *itr));
		}
		for (auto itr = use_base.begin(); itr != use_base.end(); itr++) {
			if (base_m.count(itr->second.rawid) == 0) {
				fprintf(stderr, "PL%03d rawid=%d not found\n", itr->second.pl, itr->second.rawid);
			}
			itr->second = base_m.find(itr->second.rawid)->second;
		}

		//aliment 決定
		align_param param = Calc_align(use_base);

		//use baseの出力
		for (auto itr = use_base.begin(); itr != use_base.end(); itr++) {
			mfile0::M_Base b[2];
			b[0] = format_change(itr->first, param, false);
			b[1] = format_change(itr->second, param, true);
			mfile0::M_Chain c_tmp;
			c_tmp.chain_id = cid;
			cid++;
			c_tmp.basetracks.push_back(b[0]);
			c_tmp.basetracks.push_back(b[1]);
			c_tmp.nseg = c_tmp.basetracks.size();
			c_tmp.pos0 = c_tmp.basetracks.begin()->pos;
			c_tmp.pos1 = c_tmp.basetracks.rbegin()->pos;
			m_out.chains.push_back(c_tmp);
		}
		//track 探索
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			double x, y;
			x = itr->x;
			y = itr->y;
			itr->x = x * param.position[0] + y * param.position[1] + param.position[4];
			itr->y = x * param.position[2] + y * param.position[3] + param.position[5];
			x = itr->ax;
			y = itr->ay;
			itr->ax = x * param.angle[0] + y * param.angle[1] + param.angle[4];
			itr->ay = x * param.angle[2] + y * param.angle[3] + param.angle[5];
		}
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			double dx = itr->x - target_base.x - (itr->ax + target_base.ax) / 2 * param.dz;
			double dy = itr->y - target_base.y - (itr->ay + target_base.ay) / 2 * param.dz;
			double dax = itr->ax - target_base.ax;
			double day = itr->ay - target_base.ay;

			double dr = (dx*target_base.ax + dy * target_base.ay) / sqrt(target_base.ax*target_base.ax + target_base.ay*target_base.ay);
			double dl = (dy*target_base.ax - dx * target_base.ay) / sqrt(target_base.ax*target_base.ax + target_base.ay*target_base.ay);
			double dar = (dax*target_base.ax + day * target_base.ay) / sqrt(target_base.ax*target_base.ax + target_base.ay*target_base.ay);
			double dal = (day*target_base.ax - dax * target_base.ay) / sqrt(target_base.ax*target_base.ax + target_base.ay*target_base.ay);

			if (fabs(dr) > 500)continue;
			if (fabs(dl) > 300)continue;
			if (fabs(dar) > 0.3)continue;
			if (fabs(dal) > 0.1)continue;

			mfile0::M_Chain c_tmp;
			mfile0::M_Base b[2];
			b[0] = format_change(target_base, param, false);
			b[1] = format_change(*itr, param, false);
			b[1].z = param.dz;
			b[0].group_id = 1;
			b[1].group_id = 1;
			c_tmp.chain_id = cid;
			cid++;
			c_tmp.basetracks.push_back(b[0]);
			c_tmp.basetracks.push_back(b[1]);
			c_tmp.nseg = c_tmp.basetracks.size();
			c_tmp.pos0 = c_tmp.basetracks.begin()->pos;
			c_tmp.pos1 = c_tmp.basetracks.rbegin()->pos;
			m_out.chains.push_back(c_tmp);

		}
		{
			mfile0::M_Chain c_tmp;
			mfile0::M_Base b[2];
			align_param param_tmp;
			b[0] = format_change(target_base, param_tmp, false);
			b[0].x = b[0].x + b[0].ax*param_tmp.dz;
			b[0].y = b[0].y + b[0].ay*param_tmp.dz;
			b[0].z = param_tmp.dz;
			b[0].group_id = 3;
			b[0].pos = pl_next * 10 + 1;
			b[0].rawid = -1;
			c_tmp.chain_id = cid;
			cid++;
			c_tmp.basetracks.push_back(b[0]);
			c_tmp.nseg = c_tmp.basetracks.size();
			c_tmp.pos0 = c_tmp.basetracks.begin()->pos;
			c_tmp.pos1 = c_tmp.basetracks.rbegin()->pos;
			m_out.chains.push_back(c_tmp);
		}
	}
	char outname[256];
	sprintf_s(outname, "gr%05d_ch%05d_f.all", group,chain);
	mfile1::write_mfile_extension(outname, m_out);
}
void search_backward(Momentum_recon::Mom_basetrack&b, std::multimap<std::pair<int, int>, std::pair<int, int>>&path, std::string file_in_base, int group, int chain) {
	int pl = b.pl;
	std::vector< basetrack_minimum> base = read_base_inf(file_in_base, pl, true);
	basetrack_minimum target_base;
	target_base.rawid = -1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->rawid == b.rawid) {
			target_base = *itr;
			break;
		}
	}
	if (target_base.rawid < 0) {
		printf("PL%03d rawid = %d not found\n", b.pl, b.rawid);
	}
	else {
		printf("target base PL%03d rawid = %d\n", target_base.pl, target_base.rawid);
		printf("%7.4lf %7.4lf %8.1lf %8.1lf\n", target_base.ax, target_base.ay, target_base.x, target_base.y);
	}

	mfile0::Mfile m_out;
	mfile0::set_header(3, 133, m_out);
	int cid = 0;
	{
		mfile0::M_Chain c_tmp;
		mfile0::M_Base b[2];
		align_param param_tmp;
		b[0] = format_change(target_base, param_tmp, false);
		b[0].group_id = 2;
		c_tmp.chain_id = cid;
		cid++;
		c_tmp.basetracks.push_back(b[0]);
		c_tmp.nseg = c_tmp.basetracks.size();
		c_tmp.pos0 = c_tmp.basetracks.begin()->pos;
		c_tmp.pos1 = c_tmp.basetracks.rbegin()->pos;
		m_out.chains.push_back(c_tmp);
	}

	for (int i_pl = 1; i_pl <= 3; i_pl++) {
		int pl_next = pl - i_pl;
		printf("PL%03d --> PL%03d\n", pl, pl_next);

		std::vector<std::pair< basetrack_minimum, basetrack_minimum>> use_base;
		std::set<int> selected_id;
		selected_id.insert(target_base.rawid);

		double d_pos = 100;
		double d_ang = 0.5;
		double diff_pos, diff_ang;
		int loop_num = 0;
		//while (use_base.size() < 20 && d_pos < 10000) {
		while (use_base.size() < 40) {
			//printf("loop num=%d dpos<%.0lf um\n", loop_num, d_pos);
			loop_num++;

			for (auto itr = base.begin(); itr != base.end(); itr++) {
				if (selected_id.count(itr->rawid) == 1)continue;
				diff_pos = sqrt(pow(target_base.x - itr->x, 2) + pow(target_base.y - itr->y, 2));
				diff_ang = sqrt(pow(target_base.ax - itr->ax, 2) + pow(target_base.ay - itr->ay, 2));
				if (d_pos < diff_pos)continue;
				if (d_ang < diff_ang)continue;

				//printf("(%d,%d) in range --> ", itr->pl, itr->rawid);
				if (path.count(std::make_pair(itr->pl, itr->rawid)) == 0) {
					//printf("\n");
					continue;
				}
				else {
					//printf("path num %d\n", path.count(std::make_pair(itr->pl, itr->rawid)));
				}
				auto range = path.equal_range(std::make_pair(itr->pl, itr->rawid));
				basetrack_minimum b;
				b.pl = -1;
				b.rawid = -1;
				for (auto res = range.first; res != range.second; res++) {
					//printf("\t next pl %3d\n", res->second.first);
					if (res->second.first != pl_next)continue;

					b.pl = res->second.first;
					b.rawid = res->second.second;
					break;
				}
				if (b.rawid >= 0) {
					use_base.push_back(std::make_pair(*itr, b));
					selected_id.insert(itr->rawid);
				}
			}
			d_pos += 100;
		}
		printf("base num %d\n", use_base.size());
		if (use_base.size() < 40)continue;

		//basetrackよみこみ
		std::vector< basetrack_minimum> base = read_base_inf(file_in_base, pl_next, true);
		std::map<int, basetrack_minimum> base_m;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_m.insert(std::make_pair(itr->rawid, *itr));
		}
		for (auto itr = use_base.begin(); itr != use_base.end(); itr++) {
			if (base_m.count(itr->second.rawid) == 0) {
				fprintf(stderr, "PL%03d rawid=%d not found\n", itr->second.pl, itr->second.rawid);
			}
			itr->second = base_m.find(itr->second.rawid)->second;
		}

		//aliment 決定
		align_param param = Calc_align(use_base);

		//use baseの出力
		for (auto itr = use_base.begin(); itr != use_base.end(); itr++) {
			mfile0::M_Base b[2];
			b[0] = format_change(itr->first, param, false);
			b[1] = format_change(itr->second, param, true);
			mfile0::M_Chain c_tmp;
			c_tmp.chain_id = cid;
			cid++;
			c_tmp.basetracks.push_back(b[0]);
			c_tmp.basetracks.push_back(b[1]);
			c_tmp.nseg = c_tmp.basetracks.size();
			c_tmp.pos0 = c_tmp.basetracks.begin()->pos;
			c_tmp.pos1 = c_tmp.basetracks.rbegin()->pos;
			m_out.chains.push_back(c_tmp);
		}
		//track 探索
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			double x, y;
			x = itr->x;
			y = itr->y;
			itr->x = x * param.position[0] + y * param.position[1] + param.position[4];
			itr->y = x * param.position[2] + y * param.position[3] + param.position[5];
			x = itr->ax;
			y = itr->ay;
			itr->ax = x * param.angle[0] + y * param.angle[1] + param.angle[4];
			itr->ay = x * param.angle[2] + y * param.angle[3] + param.angle[5];
		}
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			double dx = itr->x - target_base.x - (itr->ax + target_base.ax) / 2 * param.dz;
			double dy = itr->y - target_base.y - (itr->ay + target_base.ay) / 2 * param.dz;
			double dax = itr->ax - target_base.ax;
			double day = itr->ay - target_base.ay;

			double dr = (dx*target_base.ax + dy * target_base.ay) / sqrt(target_base.ax*target_base.ax + target_base.ay*target_base.ay);
			double dl = (dy*target_base.ax - dx * target_base.ay) / sqrt(target_base.ax*target_base.ax + target_base.ay*target_base.ay);
			double dar = (dax*target_base.ax + day * target_base.ay) / sqrt(target_base.ax*target_base.ax + target_base.ay*target_base.ay);
			double dal = (day*target_base.ax - dax * target_base.ay) / sqrt(target_base.ax*target_base.ax + target_base.ay*target_base.ay);

			if (fabs(dr) > 500)continue;
			if (fabs(dl) > 300)continue;
			if (fabs(dar) > 0.3)continue;
			if (fabs(dal) > 0.1)continue;

			mfile0::M_Chain c_tmp;
			mfile0::M_Base b[2];
			b[0] = format_change(target_base, param, false);
			b[1] = format_change(*itr, param, false);
			b[1].z = param.dz;
			b[0].group_id = 1;
			b[1].group_id = 1;
			c_tmp.chain_id = cid;
			cid++;
			c_tmp.basetracks.push_back(b[0]);
			c_tmp.basetracks.push_back(b[1]);
			c_tmp.nseg = c_tmp.basetracks.size();
			c_tmp.pos0 = c_tmp.basetracks.begin()->pos;
			c_tmp.pos1 = c_tmp.basetracks.rbegin()->pos;
			m_out.chains.push_back(c_tmp);

		}
		{
			mfile0::M_Chain c_tmp;
			mfile0::M_Base b[2];
			align_param param_tmp;
			b[0] = format_change(target_base, param_tmp, false);
			b[0].x = b[0].x + b[0].ax*param_tmp.dz;
			b[0].y = b[0].y + b[0].ay*param_tmp.dz;
			b[0].z = param_tmp.dz;
			b[0].group_id = 3;
			b[0].pos = pl_next * 10 + 1;
			b[0].rawid = -1;
			c_tmp.chain_id = cid;
			cid++;
			c_tmp.basetracks.push_back(b[0]);
			c_tmp.nseg = c_tmp.basetracks.size();
			c_tmp.pos0 = c_tmp.basetracks.begin()->pos;
			c_tmp.pos1 = c_tmp.basetracks.rbegin()->pos;
			m_out.chains.push_back(c_tmp);
		}
	}
	char outname[256];
	sprintf_s(outname, "gr%05d_ch%05d_b.all", group, chain);
	mfile1::write_mfile_extension(outname, m_out);
}
void Calc_Linear_dx(std::vector<std::pair< basetrack_minimum, basetrack_minimum>> use_base, double &slope, double &interacept,align_param &param) {
	//x:ax
	//y:dx

	for (auto &p : use_base) {
		double x, y;
		x = p.second.x;
		y = p.second.y;
		p.second.x = param.position[0] * x + param.position[1] * y + param.position[4];
		p.second.y = param.position[2] * x + param.position[3] * y + param.position[5];
		x = p.second.ax;
		y = p.second.ay;
		p.second.ax = param.angle[0] * x + param.angle[1] * y + param.angle[4];
		p.second.ay = param.angle[2] * x + param.angle[3] * y + param.angle[5];
	}

	double x = 0;
	double y = 0;
	double xx = 0;
	double xy = 0;
	double n = 0;
	for (auto &p : use_base) {
		x += p.first.ax;
		y += p.second.x - p.first.x - (p.first.ax + p.second.ax) / 2 * param.dz;
		xx += p.first.ax*p.first.ax;
		xy += p.first.ax*(p.second.x - p.first.x-(p.first.ax + p.second.ax) / 2 * param.dz);
		n += 1;
	}
	double denominator = n * xx - x * x;
	slope = (n * xy - x * y) / denominator;
	interacept = (xx*y - xy * x) / denominator;

}
void Calc_Linear_dy(std::vector<std::pair< basetrack_minimum, basetrack_minimum>> use_base, double &slope, double &interacept, align_param &param) {
	//x:ay
	//y:dy
	for (auto &p : use_base) {
		double x, y;
		x = p.second.x;
		y = p.second.y;
		p.second.x = param.position[0] * x + param.position[1] * y + param.position[4];
		p.second.y = param.position[2] * x + param.position[3] * y + param.position[5];
		x = p.second.ax;
		y = p.second.ay;
		p.second.ax = param.angle[0] * x + param.angle[1] * y + param.angle[4];
		p.second.ay = param.angle[2] * x + param.angle[3] * y + param.angle[5];
	}

	double x = 0;
	double y = 0;
	double xx = 0;
	double xy = 0;
	double n = 0;
	for (auto &p : use_base) {
		x += p.first.ay;
		y += p.second.y - p.first.y- (p.first.ay + p.second.ay) / 2 * param.dz;
		xx += p.first.ay*p.first.ay;
		xy += p.first.ay*(p.second.y - p.first.y -(p.first.ay + p.second.ay) / 2 * param.dz);
		n += 1;
	}
	double denominator = n * xx - x * x;
	slope = (n * xy - x * y) / denominator;
	interacept = (xx*y - xy * x) / denominator;

}
void Calc_plane(std::vector<std::pair< basetrack_minimum, basetrack_minimum>> use_base, double &rotation, double &shrink,double &dx,double &dy, align_param &param) {
	for (auto &p : use_base) {
		double x, y;
		x = p.second.x;
		y = p.second.y;
		p.second.x = param.position[0] * x + param.position[1] * y + param.position[4];
		p.second.y = param.position[2] * x + param.position[3] * y + param.position[5];
		x = p.second.ax;
		y = p.second.ay;
		p.second.ax = param.angle[0] * x + param.angle[1] * y + param.angle[4];
		p.second.ay = param.angle[2] * x + param.angle[3] * y + param.angle[5];

		//中点に外挿
		p.first.x = p.first.x + p.first.ax*param.dz / 2;
		p.second.x = p.second.x - p.second.ax*param.dz / 2;
		p.first.y = p.first.y + p.first.ay*param.dz / 2;
		p.second.y = p.second.y - p.second.ay*param.dz / 2;
	}

	double  xa, ya, xa2, ya2;
	double  xb, yb;
	double xaxb, yayb, xayb, xbya, num;
	num = 0;
	xa = 0;
	ya = 0;
	xa2 = 0;
	ya2 = 0;
	xb = 0;
	yb = 0;
	xaxb = 0;
	yayb = 0;
	xayb = 0;
	xbya = 0;
	for (auto &p : use_base) {
		num += 1;
		xa += p.first.x;
		ya += p.first.y;
		xa2 += p.first.x*p.first.x;
		ya2 += p.first.y*p.first.y;
		xb += p.second.x;
		yb += p.second.y;
		xaxb += p.first.x*p.second.x;
		yayb += p.first.y*p.second.y;
		xayb += p.first.x*p.second.y;
		xbya += p.first.y*p.second.x;
	}
	if (num < 1) {

		printf("error\n");
		return;
	}
	double denominator = xa2 + ya2 - (xa*xa + ya * ya) / num;
	if (denominator == 0) {
		printf("error\n");
		return;
	}

	double a, b, p, q;
	a = xaxb + yayb - (xa*xb + ya * yb) / num;
	b = -1 * xbya + xayb - (-1 * ya*xb + xa * yb) / num;
	a /= denominator;
	b /= denominator;
	p = xb / num - (a*xa / num - b * ya / num);
	q = yb / num - (b*xa / num + a * ya / num);
	//printf("%g %g %g %g\n", a, b, p, q);

	rotation = atan(b / a);
	shrink = sqrt(a*a + b * b);
	dx= p;
	dy= q;
	//printf("%g %g %g %g\n", ret.rotation, ret.shrink, ret.shift[0], ret.shift[1]);




}
void apply_param(double &rotation, double &shrink, double &dx, double &dy, align_param &param) {
	double ori_rot, ori_shrink;
	ori_rot = atan(param.position[1] / param.position[0]);
	ori_shrink = sqrt(param.position[0] * param.position[3] - param.position[1] * param.position[2]);
	printf("rotation %g -->%g\n", ori_rot, ori_rot + rotation);
	printf("shrink %g -->%g\n", ori_shrink, ori_shrink/shrink);

	ori_rot += rotation;
	ori_shrink /= shrink;

	param.position[0] = cos(ori_rot)*ori_shrink;
	param.position[1] = sin(ori_rot)*ori_shrink;
	param.position[2] = -1*sin(ori_rot)*ori_shrink;
	param.position[3] = cos(ori_rot)*ori_shrink;
	param.angle[0] = cos(ori_rot);
	param.angle[1] = sin(ori_rot);
	param.angle[2] = -1*sin(ori_rot);
	param.angle[3] = cos(ori_rot);

	param.position[4] -= dx;
	param.position[5] -= dy;




}
align_param Calc_align(std::vector<std::pair< basetrack_minimum, basetrack_minimum>> &use_base) {
	align_param ret;

	ret.pl[0] = use_base[0].first.pl;
	ret.pl[1] = use_base[0].second.pl;
	ret.position[0] = 1;
	ret.position[1] = 0;
	ret.position[2] = 0;
	ret.position[3] = 1;
	ret.position[4] = 0;
	ret.position[5] = 0;

	ret.angle[0] = 1;
	ret.angle[1] = 0;
	ret.angle[2] = 0;
	ret.angle[3] = 1;
	ret.angle[4] = 0;
	ret.angle[5] = 0;

	ret.dz = 0;

	printf("PL%03d - PL%03d\n", ret.pl[0], ret.pl[1]);
	double dz[2], dx, dy,rotation,shrink;

	for (int i = 0; i < 5; i++) {
		//1回目
		Calc_Linear_dx(use_base, dz[0], dx, ret);
		Calc_Linear_dy(use_base, dz[1], dy, ret);
		printf("dz:%.2lf dx:%.2lf\n", dz[0], dx);
		printf("dz:%.2lf dy:%.2lf\n", dz[1], dy);
		ret.dz += (dz[0] + dz[1]) / 2;
		ret.position[4] -= dx;
		ret.position[5] -= dy;
		//2回目
		Calc_Linear_dx(use_base, dz[0], dx, ret);
		Calc_Linear_dy(use_base, dz[1], dy, ret);
		printf("dz:%.2lf dx:%.2lf\n", dz[0], dx);
		printf("dz:%.2lf dy:%.2lf\n", dz[1], dy);
		ret.dz += (dz[0] + dz[1]) / 2;
		ret.position[4] -= dx;
		ret.position[5] -= dy;

		//1回目
		Calc_plane(use_base, rotation, shrink, dx, dy, ret);
		printf("%g %g %g %g\n", rotation, shrink, dx, dy);
		apply_param(rotation, shrink, dx, dy, ret);
		//2回目
		Calc_plane(use_base, rotation, shrink, dx, dy, ret);
		printf("%g %g %g %g\n", rotation, shrink, dx, dy);
		apply_param(rotation, shrink, dx, dy, ret);
		Calc_plane(use_base, rotation, shrink, dx, dy, ret);
	}

	//かくにｎ
	printf("-------------------\n");
	Calc_Linear_dx(use_base, dz[0], dx, ret);
	Calc_Linear_dy(use_base, dz[1], dy, ret);
	printf("dz:%.2lf dx:%.2lf\n", dz[0], dx);
	printf("dz:%.2lf dy:%.2lf\n", dz[1], dy);
	Calc_plane(use_base, rotation, shrink, dx, dy, ret);
	printf("%g %g %g %g\n", rotation, shrink, dx, dy);
	return ret;

}










void Set_m_base(std::vector<Momentum_recon::Event_information> &momch_all,
	std::map<std::pair<int, int>, std::pair<int, int>> &base_origin,
	std::map<int, std::vector<mfile0::M_Base>> &base_forward,
	std::map<int, std::vector<mfile0::M_Base>> &base_backward) {
	for (auto itr = momch_all.begin(); itr != momch_all.end(); itr++) {
		for (auto itr2 = itr->chains.begin(); itr2 != itr->chains.end(); itr2++) {
			if (itr2->chainid == 0)continue;
			Momentum_recon::Mom_basetrack base;
			if (itr2->direction == 1) {
				base = *itr2->base.rbegin();
				mfile0::M_Base b;
				b.pos = base.pl * 10 + 1;
				b.rawid = base.rawid;
				if (base_forward.count(base.pl) == 0) {
					std::vector<mfile0::M_Base> base_v;
					base_v.push_back(b);
					base_forward.insert(std::make_pair(base.pl, base_v));
				}
				else {
					base_forward.find(base.pl)->second.push_back(b);
				}
			}
			else if (itr2->direction == -1) {
				base = *itr2->base.begin();
				mfile0::M_Base b;
				b.pos = base.pl * 10 + 1;
				b.rawid = base.rawid;
				if (base_backward.count(base.pl) == 0) {
					std::vector<mfile0::M_Base> base_v;
					base_v.push_back(b);
					base_backward.insert(std::make_pair(base.pl, base_v));
				}
				else {
					base_backward.find(base.pl)->second.push_back(b);
				}

			}
			else continue;

			base_origin.insert(std::make_pair(std::make_pair(itr->groupid, itr2->chainid), std::make_pair(base.pl, base.rawid)));
		}
	}
}

void set_basetrack_inf(mfile0::M_Base&b, basetrack_minimum&b_inf) {
	b.ax = b_inf.ax;
	b.ay = b_inf.ay;
	b.x = b_inf.x;
	b.y = b_inf.y;
	b.z = 0;
	b.ph = b_inf.m[0].ph + b_inf.m[1].ph;
	b.group_id = 0;
	b.flg_d[0] = 0;
	b.flg_d[1] = 0;
	b.flg_i[0] = 0;
	b.flg_i[1] = 0;
	b.flg_i[2] = 0;
	b.flg_i[3] = 0;

}
void set_basetrack_inf(std::map<int, std::vector<mfile0::M_Base>> &base_map, std::map<int, std::vector<corrmap_3d::align_param2>>&corrmap_dd,std::string file_in_base) {
	std::multimap<int, mfile0::M_Base*>base_map_single;

	//basetrack情報の付与
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		int pl = itr->first;
		std::vector< basetrack_minimum> base = read_base_inf(file_in_base, pl, true);
		std::map<int, basetrack_minimum> base_m;
		for (auto &b : base) {
			base_m.insert(std::make_pair(b.rawid, b));
		}

		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (base_m.count(itr2->rawid) == 0) {
				fprintf(stderr,"PL%03d rawid =%d not found\n", pl, itr2->rawid);
				exit(1);
			}

			auto b = base_m.at(itr2->rawid);
			set_basetrack_inf(*itr2, b);
			base_map_single.insert(std::make_pair(pl, &(*itr2)));
		}
	}
	trans_local(base_map_single, corrmap_dd);
}

void penetrate_cand_search_forward(std::map<int, std::vector<mfile0::M_Base>> &base_forward,
	std::map<std::pair<int, int>, std::vector<mfile0::M_Base>>&pene_cand,
	std::map<int, std::vector<corrmap_3d::align_param2>>&corrmap_dd,
	std::string file_in_base) {

	//basetrack情報の付与
	for (auto itr = base_forward.begin(); itr != base_forward.end(); itr++) {
		printf("PL%03d size = %d\n", itr->first, itr->second.size());
		int pl = itr->first;
		for (int i_pl = 1; i_pl <= 3; i_pl++) {
			//basetrackよみこみ
			std::vector< basetrack_minimum> base = read_base_inf(file_in_base, pl+i_pl, true);
			std::vector<mfile0::M_Base> base_m;
			for (auto &b : base) {
				mfile0::M_Base b_m;
				set_basetrack_inf(b_m, b);
				base_m.push_back(b_m);
			}

			//座標変換
			std::multimap<int, mfile0::M_Base*> base_map;
			for (auto &b : base_m) {
				base_map.insert(std::make_pair(pl + i_pl, &b));
			}
			trans_local(base_map, corrmap_dd);

			printf("size=%d\n", itr->second.size());
			for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
				//探索
				printf("%lf %lf\n", itr2->z, base_m.begin()->z);
			}
			printf("hoge\n");

		}

	}


}

void trans_local(std::multimap<int, mfile0::M_Base*>&base_map_single, std::map<int, std::vector<corrmap_3d::align_param2>> &corr) {
	int count = 0;
	for (auto itr = base_map_single.begin(); itr != base_map_single.end(); itr++) {
		count = base_map_single.count(itr->first);
		int pl = itr->first;
		printf("PL%03d basetrack trans\n", pl);
		if (corr.count(pl) == 0) {
			fprintf(stderr, "PL%03d corrmap not found\n", pl);
		}
		std::vector<corrmap_3d::align_param2> param = corr.at(pl);

		std::vector< mfile0::M_Base*> base_trans;
		auto range = base_map_single.equal_range(itr->first);
		printf("loop start\n");
		for (auto res = range.first; res != range.second; res++) {
			base_trans.push_back(res->second);
		}
		printf("loop fin\n");
		std::vector <std::pair<mfile0::M_Base*, corrmap_3d::align_param2*>> base_trans_map = corrmap_3d::track_affineparam_correspondence(base_trans, param);
		
		printf("trans fin\n");
		trans_base_all(base_trans_map);

		printf("trans fin\n");
		itr = std::next(itr, count - 1);
	}

}