#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#include <chrono>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

bool sort_chain(const mfile0::M_Chain &left, const mfile0::M_Chain &right) {
	int gid_l = left.basetracks.begin()->group_id;
	int gid_r = right.basetracks.begin()->group_id;
	if (gid_l != gid_r)return gid_l < gid_r;

	int tid_l = left.chain_id;
	int tid_r = right.chain_id;
	if (tid_l != tid_r)return tid_l < tid_r;

	int tid2_l = left.basetracks.begin()->flg_i[1];
	int tid2_r = right.basetracks.begin()->flg_i[1];
	return tid2_l < tid2_r;
}

std::vector<mfile0::M_Chain>  merge_chain(std::vector<mfile0::M_Chain> &c1, std::vector<mfile0::M_Chain> &c2);
mfile0::M_Chain merge_chain(mfile0::M_Chain &c1, mfile0::M_Chain &c2);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string file_in_stop_muon = argv[1];
	std::string file_in_penetrate_cand = argv[2];
	std::string file_out_muon = argv[3];

	mfile0::Mfile stop_mu, pene_cand;
	mfile0::read_mfile(file_in_stop_muon, stop_mu);
	mfile0::read_mfile(file_in_penetrate_cand, pene_cand);

	stop_mu.chains = merge_chain(stop_mu.chains, pene_cand.chains);

	mfile0::write_mfile(file_out_muon, stop_mu);
}
std::vector<mfile0::M_Chain>  merge_chain(std::vector<mfile0::M_Chain> &c1, std::vector<mfile0::M_Chain> &c2) {
	std::vector<mfile0::M_Chain> ret;

	std::map<int, mfile0::M_Chain> muon;
	std::multimap<int, mfile0::M_Chain> pene_cand;
	for (auto itr = c1.begin(); itr != c1.end(); itr++) {
		muon.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}
	for (auto itr = c2.begin(); itr != c2.end(); itr++) {
		pene_cand.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}

	int gid = 0;
	int tid = 0;
	for (auto itr = muon.begin(); itr  !=muon.end(); itr++) {
		gid = itr->first;
		tid = 0;
		itr->second.chain_id = tid;
		tid++;
		ret.push_back(itr->second);
		if (pene_cand.count(itr->first) == 0)continue;
		auto range = pene_cand.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			mfile0::M_Chain chain_add = merge_chain(itr->second, res->second);
			if (chain_add.basetracks.size() == 0)continue;
			chain_add.chain_id = tid;
			tid++;
			ret.push_back(chain_add);

		}
	}

	return ret;
}
mfile0::M_Chain merge_chain(mfile0::M_Chain &c1, mfile0::M_Chain &c2) {
	std::set<std::pair<int, int>> base_id;
	for (auto itr = c1.basetracks.begin(); itr != c1.basetracks.end(); itr++) {
		base_id.insert(std::make_pair(itr->pos / 10, itr->rawid));
	}
	mfile0::M_Chain ret;
	for (auto itr = c2.basetracks.begin(); itr != c2.basetracks.end(); itr++) {
		if (base_id.count(std::make_pair(itr->pos / 10, itr->rawid)) == 0) {
			ret.basetracks.push_back(*itr);
		}
	}
	//上流から詰めていき不自然なgapにはまったらカット
	mfile0::M_Chain ret2;
	for (auto itr = ret.basetracks.rbegin(); itr != ret.basetracks.rend(); itr++) {
		ret2.basetracks.push_back(*itr);
		if (std::next(itr, 1) == ret.basetracks.rend())continue;
		int pl1 = itr->pos / 10;
		int pl0 = std::next(itr, 1)->pos / 10;
		if (pl1 <= 15 && pl1 - pl0 > 5)break;
		else if (pl1 == 16 && pl1 - pl0 > 5)break;
		else if (pl1 == 17 && pl1 - pl0 > 5)break;
		else if (pl1 == 18 && pl1 - pl0 > 5)break;
		else if (pl1 == 19 && pl1 - pl0 > 5)break;
		else if (pl1 == 19 && pl1 - pl0 > 5)break;
		else if (pl0 % 2 == 0 && pl1 - pl0 > 5)break;
		else if (pl0 % 2 == 1 && pl1 - pl0 > 4)break;
	}
	ret.basetracks.clear();
	for (auto itr = ret2.basetracks.rbegin(); itr != ret2.basetracks.rend(); itr++) {
		ret.basetracks.push_back(*itr);
	}

	ret.chain_id = c2.chain_id;
	ret.nseg = ret.basetracks.size();
	
	if (ret.nseg != 0) {
		ret.pos0 = ret.basetracks.begin()->pos;
		ret.pos1 = ret.basetracks.rbegin()->pos;
	}
	else {
		ret.pos0 = 0;
		ret.pos1 = 0;
	}
	return ret;
}

