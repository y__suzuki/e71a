#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#include <fstream>
#include <list>
#include <omp.h>

struct CutParam {
	double angle[2];
	double intercept, slope, ymax;
};
struct BaseInformation {
	double angle, x, y;
	vxx::base_track_t *ptr;
};

std::vector<CutParam>  read_cut_param(std::string filename);
std::vector<std::pair<bool, vxx::base_track_t>> read_bvxx_id(std::string filename, int pl, int zone, int &id);
void RankingCut(std::vector<std::pair<bool, vxx::base_track_t>> &base, CutParam param);
void RankingCut_lateral(std::vector<std::pair<bool, vxx::base_track_t>> &base, CutParam param);
int use_thread(double ratio, bool output);
std::vector<vxx::base_track_t> angle_cut(std::vector<vxx::base_track_t>&base, double threshold);

int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:prg in-bvxx pl zone out-bvxx cut-param-file cut-param-file-lat\n");
		fprintf(stderr, "cut-param-file:ang_min ang_max x1 y1 x2 y2\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_base = argv[4];
	std::string file_in_cut_normal = argv[5];
	std::string file_in_cut_lat = argv[6];

	std::vector<CutParam> cutparam_norm = read_cut_param(file_in_cut_normal);
	std::vector<CutParam> cutparam_lat = read_cut_param(file_in_cut_lat);
	std::vector<vxx::base_track_t> base_out;
	int id = 0;
	int64_t count = 0;

	while (true) {
		std::vector<std::pair<bool, vxx::base_track_t>>base = read_bvxx_id(file_in_base, pl, zone, id);
		if (base.size() == 0)break;

		for (auto itr = cutparam_lat.begin(); itr != cutparam_lat.end(); itr++) {
			RankingCut_lateral(base, *itr);
		}
		for (auto itr = cutparam_norm.begin(); itr != cutparam_norm.end(); itr++) {
			RankingCut(base, *itr);
		}
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			if (itr->first) {
				base_out.push_back(itr->second);
			}
		}
	}

	//角度が大きすぎるものはcut
	base_out = angle_cut(base_out, 10 * sqrt(2));
	vxx::BvxxWriter bw;
	bw.Write(file_out_base, pl, zone, base_out);
}
std::vector<CutParam>  read_cut_param(std::string filename) {
	std::vector<CutParam> ret;
	std::ifstream ifs(filename);
	double angle[2], x[2], y[2];
	while (ifs >> angle[0] >> angle[1] >> x[0] >> y[0] >> x[1] >> y[1]) {
		CutParam par;
		par.angle[0] = angle[0];
		par.angle[1] = angle[1];
		par.slope = (y[1] - y[0]) / (x[1] - x[0]);
		par.intercept = y[0] - par.slope * x[0];
		par.ymax = y[1];
		ret.push_back(par);
		//printf("y=%lf x+%lf\n", par.slope, par.intercept);
	}
	return ret;
}

std::vector<std::pair<bool,vxx::base_track_t>> read_bvxx_id(std::string filename, int pl, int zone, int &id) {
	//bvxx 176 byte/track
	//50Mtrack=8.8GB
	int read_track_num = 50 * 1000 * 1000;

	std::vector<vxx::base_track_t> b;
	vxx::BvxxReader br;
	std::array<int, 2> index = { id*read_track_num, (id + 1)*read_track_num };//1234<=rawid<=5678であるようなものだけを読む。
	b = br.ReadAll(filename, pl, zone, vxx::opt::index = index);

	std::vector<std::pair<bool, vxx::base_track_t>> ret;
	ret.reserve(read_track_num);
	for (auto itr = b.begin(); itr != b.end(); itr++) {
		ret.push_back(std::make_pair(true, *itr));
	}
	id++;
	return ret;

}
void RankingCut(std::vector<std::pair<bool, vxx::base_track_t>> &base, CutParam param) {

	int all_base = base.size();
	int count = 0;
	int all = 0, lost = 0;
	double CPU_ratio = 0.4;
#pragma omp parallel for num_threads(use_thread(CPU_ratio,false)) schedule(guided)
	for (int i = 0; i < all_base; i++) {
		if (count % 1000000 == 0) {
			fprintf(stderr, "\r angle %3.1lf - %3.1lf %12d/%12d(%4.1lf%%)", param.angle[0], param.angle[1], count, all_base, count*100. / all_base);
		}
#pragma omp atomic
		count++;
		if (base[i].first == false)continue;

		double angle;
		double x, y;
		angle = sqrt(base[i].second.ax*base[i].second.ax + base[i].second.ay*base[i].second.ay);
		if (param.angle[0] <= angle && angle < param.angle[1]) {
			all++;
			x = sqrt(pow(base[i].second.m[0].ax - base[i].second.ax, 2) + pow(base[i].second.m[1].ax - base[i].second.ax, 2) + pow(base[i].second.m[0].ay - base[i].second.ay, 2) + pow(base[i].second.m[1].ay - base[i].second.ay, 2));
			y = (base[i].second.m[0].ph + base[i].second.m[1].ph) % 10000;
			if (y < x * param.slope + param.intercept && param.ymax > y) {
				base[i].first = false;
				lost++;
			}
		}
	}

	fprintf(stderr, "\r angle %3.1lf - %3.1lf %12d/%12d(%4.1lf%%)\n", param.angle[0], param.angle[1], count, all_base, count*100. / all_base);
	fprintf(stderr, "all %d ,lost %d\n", all, lost);
}
void RankingCut_lateral(std::vector<std::pair<bool, vxx::base_track_t>> &base, CutParam param) {

	int all_base = base.size();
	int count = 0;
	int all = 0, lost = 0;
	double CPU_ratio = 0.4;
#pragma omp parallel for num_threads(use_thread(CPU_ratio,false)) schedule(guided)
	for (int i = 0; i < all_base; i++) {
		if (count % 1000000 == 0) {
			fprintf(stderr, "\r angle %3.1lf - %3.1lf %12d/%12d(%4.1lf%%) (lateral)", param.angle[0], param.angle[1], count, all_base, count*100. / all_base);
		}
#pragma omp atomic
		count++;
		if (base[i].first == false)continue;

		double angle;
		double x, y;
		angle = sqrt(base[i].second.ax*base[i].second.ax + base[i].second.ay*base[i].second.ay);
		if (param.angle[0] <= angle && angle < param.angle[1]) {
			all++;
			x = sqrt(pow((base[i].second.ax*base[i].second.m[0].ay - base[i].second.ay*base[i].second.m[0].ax) / angle, 2) + pow((base[i].second.ax*base[i].second.m[1].ay - base[i].second.ay*base[i].second.m[1].ax) / angle, 2));
			y = (base[i].second.m[0].ph + base[i].second.m[1].ph) % 10000;
			if (y < x * param.slope + param.intercept && param.ymax > y) {
				base[i].first = false;
				lost++;
			}
		}
	}

	fprintf(stderr, "\r angle %3.1lf - %3.1lf %12d/%12d(%4.1lf%%) (lateral)\n", param.angle[0], param.angle[1], count, all_base, count*100. / all_base);
	fprintf(stderr, "all %d ,lost %d\n", all, lost);
}
std::vector<vxx::base_track_t> angle_cut(std::vector<vxx::base_track_t>&base, double threshold) {
	std::vector<vxx::base_track_t> ret;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		angle = itr->ax*itr->ax + itr->ay*itr->ay;
		if (angle > threshold*threshold)continue;
		ret.push_back(*itr);
	}
	return ret;
}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
