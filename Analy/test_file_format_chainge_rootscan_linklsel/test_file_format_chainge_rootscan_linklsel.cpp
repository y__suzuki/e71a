#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class linksel_inf {
public:
	int pl0, pl1, rawid0, rawid1;
	double chis_da_r, chis_da_l, chis_dp_r, chis_dp_l;
	double chis_sum() {
		return chis_da_r + chis_da_l + chis_dp_r + chis_dp_l;
	}
};


std::vector<linksel_inf> Read_file(std::string filename);
std::vector<std::string> StringSplit_ast(std::string str);
bool isNumeric(std::string const &str);
void output_file(std::string filename, std::vector<linksel_inf> &link);
void output_file_bin(std::string filename, std::vector<linksel_inf> &link);


int main(int argc,char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in \n");
		exit(1);
	}
	std::string file_in = argv[1];
	std::string file_out = argv[2];
	std::string file_out_bin = argv[3];

	std::vector<linksel_inf> link = Read_file(file_in);
	printf("file size = %d\n", link.size());
	output_file(file_out,link);
	output_file_bin(file_out_bin, link);

}

std::vector<std::string> StringSplit_ast(std::string str) {
	std::stringstream ss{ str };
	std::vector<std::string> v;
	std::string buf;
	while (std::getline(ss, buf, '*')) {
		if (buf != "") {
			v.push_back(buf);
		}
	}
	return v;
}

std::vector<linksel_inf> Read_file(std::string filename) {
	std::vector<linksel_inf> ret;
	std::ifstream ifs(filename);
	std::string str;
	int count = 0;
	while (std::getline(ifs, str)) {
		count++;
		if (str[0] != '*')continue;
		if (count % 10000 == 0) {
			printf("\r read file %d", count);
		}

		int count_ast = std::count(str.cbegin(), str.cend(), '*');
		if (count_ast != 10)continue;
		std::vector<std::string> str_v = StringSplit_ast(str);
		if (str_v.size() != 9)continue;
		if (!isNumeric(str_v[0]))continue;

		linksel_inf l;
		l.pl0 = std::stoi(str_v[1]) / 10;
		l.rawid0 = std::stoi(str_v[2]);
		l.pl1 = std::stoi(str_v[3]) / 10;
		l.rawid1 = std::stoi(str_v[4]);
		l.chis_da_r = std::stod(str_v[5]);
		l.chis_da_l = std::stod(str_v[6]);
		l.chis_dp_r = std::stod(str_v[7]);
		l.chis_dp_l = std::stod(str_v[8]);

		ret.push_back(l);
	}
	printf("\r read file %d\n", count);

	return ret;

}
bool isNumeric(std::string const &str)
{
	auto it = str.begin();
	while (it != str.end() ) {
		if (*it == ' ') {
			it++;
			continue;
		}
		if (!isdigit(*it))break;
		it++;

	}
	return !str.empty() && it == str.end();
}

void output_file(std::string filename, std::vector<linksel_inf> &link) {
	std::ofstream ofs(filename);
	int count = 0, all = link.size();
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r write file %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << itr->pl0 << " "
			<< std::setw(10) << std::setprecision(0) << itr->rawid0 << " "
			<< std::setw(3) << std::setprecision(0) << itr->pl1 << " "
			<< std::setw(10) << std::setprecision(0) << itr->rawid1 << " "
			<< std::setw(8) << std::setprecision(3) << itr->chis_da_r << " "
			<< std::setw(8) << std::setprecision(3) << itr->chis_da_l << " "
			<< std::setw(8) << std::setprecision(3) << itr->chis_dp_r << " "
			<< std::setw(8) << std::setprecision(3) << itr->chis_dp_l << std::endl;
	}
	printf("\r write file %d/%d(%4.1lf%%)\n", count, all, count*100. / all);


}
void output_file_bin(std::string filename, std::vector<linksel_inf> &link) {
	std::ofstream ofs(filename, std::ios::binary);
	int count = 0, all = link.size();
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r write file %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		ofs.write((char*)&*itr, sizeof(linksel_inf));
	}
	printf("\r write file %d/%d(%4.1lf%%)\n", count, all, count*100. / all);


}
