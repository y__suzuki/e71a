#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <omp.h>
#include <set>
struct Efficiency {
	int pl, all, hit;
	double eff, eff_err, ang_min, ang_max;
};

void Basetrack_matching(std::vector<netscan::base_track_t> base_pred, std::vector<netscan::base_track_t>  &base_all, double ang_min, double ang_max, std::vector<Efficiency> &eff, std::set<int> &pred_raw);
	void Basetrack_matching_BG(std::vector<netscan::base_track_t> base_pred, std::vector<netscan::base_track_t>  &base_all, double ang_min, double ang_max, std::vector<Efficiency> &eff);
std::vector<std::vector<netscan::base_track_t>> Base_Area_Cut_Hash(std::vector<netscan::base_track_t> base);
void output_eff(std::string filename, std::vector<Efficiency> eff);
void base_alignment(std::vector <netscan::base_track_t> pred, std::vector <netscan::base_track_t>&base0, std::vector <netscan::base_track_t>&base0_sel, std::vector <netscan::base_track_t>&base1, std::vector <netscan::base_track_t>&base1_sel);
	void fit_line(std::vector<double>x, std::vector<double>y, double &slope, double &intercept);
void fit_line_intercept0(std::vector<double>x, std::vector<double>y, double &slope);
std::vector<netscan::base_track_t> missmatch_ID(std::vector<netscan::base_track_t> base, std::set<int> pred_raw0, std::set<int> pred_raw1);

int main(int argc, char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-bvxx(prediction) in-bvxx1 in-bvxx2 pl out-txt-sig(app)\n");
		exit(1);
	}
	std::string file_in_base_pred = argv[1];
	std::string file_in_base1 = argv[2];
	std::string file_in_base2 = argv[3];
	int pl = std::stoi(argv[4]);
	std::string file_out_txt_sig = argv[5];

	std::vector<netscan::base_track_t> base_pred;
	std::vector<netscan::base_track_t> base0,base1;

	netscan::read_basetrack_extension(file_in_base_pred, base_pred, pl, 0);
	netscan::read_basetrack_extension(file_in_base1, base0, pl, 0);
	netscan::read_basetrack_extension(file_in_base2, base1, pl, 0);
	std::vector<std::vector<netscan::base_track_t>>base_pred_hash = Base_Area_Cut_Hash(base_pred);

	std::vector<Efficiency> eff0,eff1;
	std::vector<Efficiency> eff_bg;
	std::vector <netscan::base_track_t> base0_sel, base1_sel;
	std::set<int> pred_raw0, pred_raw1;
	double ang_min, ang_max;
	int i = 0;
	for (i = 0; i < base_pred_hash.size(); i++) {
		printf("\r %d/%d(%4.1lf%%)", i, base_pred_hash.size(), i*100. / base_pred_hash.size());
		base_alignment(base_pred_hash[i], base0, base0_sel, base1, base1_sel);
		for (int j = 0; j < 15; j++) {
			ang_min = j * 0.1;
			ang_max = (j + 1) * 0.1;

			Basetrack_matching(base_pred_hash[i], base0_sel, ang_min, ang_max, eff0, pred_raw0);
			Basetrack_matching(base_pred_hash[i], base1_sel, ang_min, ang_max, eff1, pred_raw1);
		}
	}
	printf("\r %d/%d(%4.1lf%%)\n", i, base_pred_hash.size(), i*100. / base_pred_hash.size());
	std::vector<netscan::base_track_t>base_diff = missmatch_ID(base_pred, pred_raw0, pred_raw1);
	for (auto itr = base_diff.begin(); itr != base_diff.end(); itr++) {
		itr->ax = itr->ax / 0.951;
		itr->ay = itr->ay / 0.951;
	}
	netscan::write_basetrack_vxx("b004_diff.vxx", base_diff, pl, 0);
	
	for (auto itr = eff0.begin(); itr != eff0.end(); itr++) {
		itr->pl = pl;
	}
	for (auto itr = eff1.begin(); itr != eff1.end(); itr++) {
		itr->pl = pl;
	}
	output_eff(file_out_txt_sig, eff0);
	output_eff(file_out_txt_sig, eff1);
}
std::vector<std::vector<netscan::base_track_t>> Base_Area_Cut_Hash(std::vector<netscan::base_track_t> base) {
	std::vector<std::vector<netscan::base_track_t>> ret;
	double area[4];
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			area[0] = itr->x;
			area[1] = itr->x;
			area[2] = itr->y;
			area[3] = itr->y;
		}
		area[0] = std::min(area[0], itr->x);
		area[1] = std::max(area[1], itr->x);
		area[2] = std::min(area[2], itr->y);
		area[3] = std::max(area[3], itr->y);
	}
	double cut = 5000;
	area[0] += cut;
	area[1] -= cut;
	area[2] += cut;
	area[3] -= cut;

	std::multimap<std::pair<int, int>, netscan::base_track_t>  base_map;
	double hash = 7500;
	int ix, iy;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->x < area[0])continue;
		if (itr->x > area[1])continue;
		if (itr->y < area[2])continue;
		if (itr->y > area[3])continue;
		ix = (itr->x - area[0]) / hash;
		iy = (itr->y - area[2]) / hash;
		base_map.insert(std::make_pair(std::make_pair(ix, iy), *itr));

	}
	std::vector<netscan::base_track_t> base_vec;
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		if (itr->first == std::next(itr, 1)->first) {
			base_vec.push_back(itr->second);
		}
		else {
			base_vec.push_back(itr->second);
			ret.push_back(base_vec);
			base_vec.clear();
		}
	}
	return ret;
}
//std::vector <netscan::base_track_t> base_alignment(std::vector <netscan::base_track_t>&base0, std::vector <netscan::base_track_t> &base1) {
void base_alignment(std::vector <netscan::base_track_t> pred,std::vector <netscan::base_track_t>&base0,  std::vector <netscan::base_track_t>&base0_sel, std::vector <netscan::base_track_t>&base1, std::vector <netscan::base_track_t>&base1_sel) {
		double x_min, x_max, y_min, y_max;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		if (itr == pred.begin()) {
			x_min = itr->x;
			x_max = itr->x;
			y_min = itr->y;
			y_max = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		x_max = std::max(itr->x, x_max);
		y_min = std::min(itr->y, y_min);
		y_max = std::max(itr->y, y_max);
	}
	double overrap = 2000;
	x_min -= overrap;
	x_max += overrap;
	y_min -= overrap;
	y_max += overrap;
	base0_sel.clear();
	base1_sel.clear();
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		if (itr->x < x_min)continue;
		if (itr->x > x_max)continue;
		if (itr->y < y_min)continue;
		if (itr->y > y_max)continue;
		base0_sel.push_back(*itr);
	}
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (itr->x < x_min)continue;
		if (itr->x > x_max)continue;
		if (itr->y < y_min)continue;
		if (itr->y > y_max)continue;
		base1_sel.push_back(*itr);
	}



	//predとbase0のaignmen-->base0,base1を変換
	double shift_x, shift_y, gap;
	std::vector<std::pair<netscan::base_track_t, netscan::base_track_t>> match;
	double diff_pos[2], diff_ang[2], angle;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		for (auto itr2 =base0_sel.begin(); itr2 != base0_sel.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.05 * angle + 0.02)continue;
			if (fabs(diff_ang[1]) > 0.02)continue;
			if (fabs(diff_pos[0]) > 10 * angle + 15)continue;
			if (fabs(diff_pos[1]) > 15)continue;
			match.push_back(std::make_pair(*itr, *itr2));
		}
	}
	std::vector<double>dx, ax, dy, ay;
	std::vector<double>angle_v, dr;
	double tmp[2];

	for (auto itr = match.begin(); itr != match.end(); itr++) {
		ax.push_back(itr->first.ax);
		ay.push_back(itr->first.ay);
		dx.push_back(itr->first.x - itr->second.x);
		dy.push_back(itr->first.y - itr->second.y);
		tmp[0] = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);
		tmp[1] = ((itr->first.x - itr->second.x)*itr->first.ax + (itr->first.y - itr->second.y)*itr->first.ay) / tmp[0];
		angle_v.push_back(tmp[0]);
		dr.push_back(tmp[1]);

	}
	double slope;
	fit_line(ax, dx, slope, shift_x);
	fit_line(ay, dy, slope, shift_y);
	fit_line_intercept0(angle_v, dr, gap);

	//printf("shift x=%lf\n", shift_x);
	//printf("shift y=%lf\n", shift_y);
	//printf("gap=%lf\n", gap);
	for (auto itr = base0_sel.begin(); itr != base0_sel.end(); itr++) {
		itr->x = itr->x + shift_x + itr->ax*gap;
		itr->y = itr->y + shift_y + itr->ay*gap;
	}
	for (auto itr = base1_sel.begin(); itr != base1_sel.end(); itr++) {
		itr->x = itr->x + shift_x + itr->ax*gap;
		itr->y = itr->y + shift_y + itr->ay*gap;
	}


	//以下確認
	match.clear();
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		for (auto itr2 = base0_sel.begin(); itr2 != base0_sel.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.05 * angle + 0.02)continue;
			if (fabs(diff_ang[1]) > 0.02)continue;
			if (fabs(diff_pos[0]) > 10 * angle + 15)continue;
			if (fabs(diff_pos[1]) > 15)continue;
			match.push_back(std::make_pair(*itr, *itr2));
		}
	}
	//	printf("match num=%d\n", match.size());

	dx.clear();
	ax.clear();
	dy.clear();
	ay.clear();
	for (auto itr = match.begin(); itr != match.end(); itr++) {
		ax.push_back(itr->first.ax);
		ay.push_back(itr->first.ay);
		dx.push_back(itr->first.x - itr->second.x);
		dy.push_back(itr->first.y - itr->second.y);
		tmp[0] = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);
		tmp[1] = ((itr->first.x - itr->second.x)*itr->first.ax + (itr->first.y - itr->second.y)*itr->first.ay) / tmp[0];
		angle_v.push_back(tmp[0]);
		dr.push_back(tmp[1]);

	}
	fit_line(ax, dx, slope, shift_x);
	fit_line(ay, dy, slope, shift_y);
	fit_line_intercept0(angle_v, dr, gap);

	//printf("shift x=%lf\n", shift_x);
	//printf("shift y=%lf\n", shift_y);
	//printf("gap=%lf\n", gap);
	for (auto itr = base0_sel.begin(); itr != base0_sel.end(); itr++) {
		itr->x = itr->x + itr->ax*gap;
		itr->y = itr->y + itr->ay*gap;
	}
	for (auto itr = base1_sel.begin(); itr != base1_sel.end(); itr++) {
		itr->x = itr->x + itr->ax*gap;
		itr->y = itr->y + itr->ay*gap;
	}
	return;
}
void fit_line(std::vector<double>x, std::vector<double>y, double &slope, double &intercept) {
	if (x.size() != y.size()) {
		printf("exeption\n");
		exit(1);
	}
	double x1, y1, x2, y2, xy, n;
	x1 = 0;
	y1 = 0;
	x2 = 0;
	y2 = 0;
	xy = 0;
	n = 0;
	for (int i = 0; i < x.size(); i++) {
		x1 += x[i];
		y1 += y[i];
		x2 += x[i] * x[i];
		xy += x[i] * y[i];
		n += 1;
	}
	slope = (n*xy - x1 * y1) / (n*x2 - x1 * x1);
	intercept = (x2*y1 - xy * x1) / (n*x2 - x1 * x1);
}
void fit_line_intercept0(std::vector<double>x, std::vector<double>y, double &slope) {
	if (x.size() != y.size()) {
		printf("exeption\n");
		exit(1);
	}
	double x2, xy;
	x2 = 0;
	xy = 0;
	for (int i = 0; i < x.size(); i++) {
		x2 += x[i] * x[i];
		xy += x[i] * y[i];
	}
	slope = xy / x2;
}

void Basetrack_matching(std::vector<netscan::base_track_t> base_pred, std::vector<netscan::base_track_t>  &base_all, double ang_min, double ang_max, std::vector<Efficiency> &eff,std::set<int> &pred_raw) {
//並列化事故あり
	double angle_eff = (ang_min + ang_max) / 2;
	bool angle_flg = false;
	Efficiency* eff_tmp = nullptr;


	for (auto itr = eff.begin(); itr != eff.end(); itr++) {
		if (itr->ang_min < angle_eff&&angle_eff < itr->ang_max) {
			eff_tmp = &(*itr);
			angle_flg = true;
		}
	}
	if (angle_flg == false) {
		eff_tmp = new Efficiency();
		eff_tmp->ang_max = ang_max;
		eff_tmp->ang_min = ang_min;
		eff_tmp->all = 0;
		eff_tmp->hit = 0;
	}

	//int count = 0;

#pragma omp parallel for
	for (int i = 0; i < base_pred.size(); i++) {
		double diff_pos[2], diff_ang[2];
		auto itr = &(base_pred[i]);
		bool hit_flg = false;
		double angle;
		int ix, iy;
		int flg;
		std::pair<int, int> id;
		netscan::base_track_t base_hit;

		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angle < eff_tmp->ang_min)continue;
		if (angle >= eff_tmp->ang_max)continue;
#pragma omp atomic
		eff_tmp->all++;

		for (auto itr2 = base_all.begin(); itr2 != base_all.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.05 * angle + 0.04)continue;
			if (fabs(diff_ang[1]) > 0.04)continue;
			if (fabs(diff_pos[0]) > 10 * angle + 15)continue;
			if (fabs(diff_pos[1]) > 15)continue;
			base_hit = *(itr2);
			hit_flg = true;
		}
		if (hit_flg) {
#pragma omp atomic
			eff_tmp->hit++;
#pragma omp critical
			pred_raw.insert(itr->m[0].rawid);
		}
	}
	//fprintf(stderr, "ang:%4.1lf\n", (ang_min + ang_max) / 2);
	//ここまででmtaching終了
	if (eff_tmp->all == 0) {
		eff_tmp->eff = 0;
		eff_tmp->eff_err = 0;
	}
	else {
		eff_tmp->eff = eff_tmp->hit*1.0 / eff_tmp->all;
		eff_tmp->eff_err = sqrt(eff_tmp->all*eff_tmp->eff*(1 - eff_tmp->eff)) / eff_tmp->all;
	}
	if (angle_flg == false) {
		eff.push_back(*eff_tmp);
	}
}
void output_eff(std::string filename, std::vector<Efficiency> eff) {
	std::ofstream ofs(filename, std::ios::app);
	for (auto itr = eff.begin(); itr != eff.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << itr->pl << " "
			<< std::setw(3) << std::setprecision(1) << itr->ang_min << " "
			<< std::setw(3) << std::setprecision(1) << itr->ang_max << " "
			<< std::setw(10) << std::setprecision(0) << itr->hit << " "
			<< std::setw(10) << std::setprecision(0) << itr->all << " "
			<< std::setw(5) << std::setprecision(4) << itr->eff << " "
			<< std::setw(5) << std::setprecision(4) << itr->eff_err << std::endl;
	}
}

std::vector<netscan::base_track_t> missmatch_ID(std::vector<netscan::base_track_t> base, std::set<int> pred_raw0, std::set<int> pred_raw1) {
	std::map<int, netscan::base_track_t> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_map.insert(std::make_pair(itr->m[0].rawid, *itr));
	}

	std::set <int> raw0_only;
	std::set <int> raw1_only;

	for (auto itr = pred_raw0.begin(); itr != pred_raw0.end(); itr++) {
		if (pred_raw1.count(*itr) == 0) {
			raw0_only.insert(*itr);
		}
	}
	for (auto itr = pred_raw1.begin(); itr != pred_raw1.end(); itr++) {
		if (pred_raw0.count(*itr) == 0) {
			raw1_only.insert(*itr);
		}
	}
	printf("num raw0=%d\n", pred_raw0.size());
	printf("num raw1=%d\n", pred_raw1.size());
	printf("num raw0 only=%d\n", raw0_only.size());
	printf("num raw1 only=%d\n", raw1_only.size());

	std::vector<netscan::base_track_t> ret;
	for (auto itr = raw0_only.begin(); itr != raw0_only.end(); itr++) {
		if (base_map.count(*itr) == 0) {
			fprintf(stderr, "rawid = %d not found\n", *itr);
			exit(1);
		}
		else {
			ret.push_back(base_map.find(*itr)->second);
		}
	}
	return ret;
}