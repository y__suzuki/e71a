#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>
#include <omp.h>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <string>
class microtrack_new_inf {
public:
	short col, row;
	int isg;
	float px, py;
};
std::vector<microtrack_new_inf>read_micro_inf(std::string filename, int pos);
void out_microtrack_inf(std::string filename, std::vector<microtrack_new_inf> &m);

int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-fvxx pos out-bin\n");
		exit(1);
	}
	std::string file_in_fvxx = argv[1];
	int pos = std::stoi(argv[2]);
	std::string file_out_bin = argv[3];
	std::vector<microtrack_new_inf> m_inf = read_micro_inf(file_in_fvxx, pos);
	out_microtrack_inf(file_out_bin, m_inf);
}
std::vector<microtrack_new_inf>read_micro_inf(std::string filename, int pos) {
	std::vector<microtrack_new_inf> ret;

	//fvxx xx byte/track
	//1Mtrack=xxGB
	int read_track_num = (1 * 1000 * 1000);
	int id = 0;
	int t_num = -1;
	std::vector<vxx::micro_track_t> m;
	m.reserve(read_track_num);
	vxx::FvxxReader fr;
	microtrack_new_inf m_tmp;
	int num2, vola;
	while (t_num != 0) {
		std::array<int, 2> index = { id*read_track_num, (id + 1)*read_track_num };//1234<=rawid<=5678であるようなものだけを読む。
		m = fr.ReadAll(filename, pos, 0, vxx::opt::index = index);
		t_num = m.size();

		for (auto itr = m.begin(); itr != m.end(); itr++) {
			m_tmp.col = itr->col;
			m_tmp.row = itr->row;
			m_tmp.isg = itr->isg;
			m_tmp.px = itr->px;
			m_tmp.py = itr->py;
			ret.push_back(m_tmp);
		}
		m.clear();
		id++;
	}
	m.clear();
	m.shrink_to_fit();

	return ret;
}
void out_microtrack_inf(std::string filename, std::vector<microtrack_new_inf> &m) {
	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (m.size() == 0) {
		fprintf(stderr, "target file ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = m.size();
	for (int i = 0; i < m.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& m[i], sizeof(microtrack_new_inf));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();
}