#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include<vector>

int64_t max_groupid(chain::Chain_file &c);
int64_t max_chainid(chain::Chain_file &c);
class Chain_matching {
public:
	int64_t chain_id;
	int start_pl, end_pl, nseg;
	std::vector<int> rawid;
};

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg input-chain1 input-chain2\n");
		exit(1);
	}
	std::string file_in_chain1 = argv[1];
	std::string file_in_chain2 = argv[2];
	chain::Chain_file c1,c2;
	chain::read_chain(file_in_chain1, c1);
	chain::read_chain(file_in_chain2, c2);
	printf("read fin\n");

	int64_t max_group = max_groupid(c1);
	printf("max group:%lld\n", max_group);
	max_group = max_groupid(c2);
	printf("max group:%lld\n", max_group);

	int64_t max_chain = max_chainid(c1);
	printf("max chain:%lld\n", max_chain);
	max_chain = max_chainid(c2);
	printf("max chain:%lld\n", max_chain);

	std::map<uint64_t,>
}
int64_t max_groupid(chain::Chain_file &c) {
	int64_t ret = 0;
	for (auto itr = c.groups.begin(); itr != c.groups.end(); itr++) {
		ret = std::max(ret, itr->group_id);
	}
	return ret;
}
int64_t max_chainid(chain::Chain_file &c) {
	int64_t ret = 0;
	for (int i = 0; i < c.chains.size(); i++) {
		for (int j = 0; j < c.chains[i].size(); j++) {
			ret = std::max(ret, c.chains[i][j].chain_id);
		}
	}
	return ret;
}
