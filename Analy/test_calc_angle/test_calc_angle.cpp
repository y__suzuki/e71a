#include <fstream>
#include <iostream>
#include <ios>
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <map>

void Calc_rotation(double val[3], double alpha, double theta);
void Calc_rotation_xy(double val[3], double theta);

int main() {
	//rad = [0.316228, 0.948683, 0]
	//	lat = [-0.948683, 0.316228, 0]

	//x_axis2 = [0.70014, 0.140028, 0.70014]
	//	y_axis2 = [-0.196116, 0.980581, 0]
	//	z_axis2 = [-0.686544, -0.137309, 0.714006]

	//axis�����S��vec����]
	double axis[3] = { 1, 0.2, 1 };
	double point[3] = { 0.6,0.3,1 };

	//oda-axis
	double projection_axis0[3] = { axis[0],axis[1],axis[2] };
	double projection_axis1[3] = { -1 * axis[1],axis[0],0 };
	double projection_axis2[3] = { -1 * axis[0],-1 * axis[1],axis[0] * axis[0] + axis[1] * axis[1] };

	double alpha = atan(axis[1]/axis[2]);
	double beta = atan(-1*axis[0] / (axis[1]*sin(alpha) + axis[2]*cos(alpha)));
	
	Calc_rotation(projection_axis0, alpha, beta);
	Calc_rotation(projection_axis1, alpha, beta);
	Calc_rotation(projection_axis2, alpha, beta);
	double theta = -1 * atan2(projection_axis2[1], projection_axis2[0]);

	Calc_rotation_xy(projection_axis0, theta);
	Calc_rotation_xy(projection_axis1, theta);
	Calc_rotation_xy(projection_axis2, theta);
	//printf("val: %g, %g, %g\n", projection_axis0[0], projection_axis0[1], projection_axis0[2]);
	//printf("val: %g, %g, %g\n", projection_axis1[0], projection_axis1[1], projection_axis1[2]);
	//printf("val: %g, %g, %g\n", projection_axis2[0], projection_axis2[1], projection_axis2[2]);

	Calc_rotation(point, alpha, beta);
	Calc_rotation_xy(point, theta);
	printf("val: %g, %g, %g\n", point[0], point[1], point[2]);

	double val[3];
	val[0] = point[0] * cos(beta) + sin(alpha)*sin(beta)*point[1] + sin(beta)*cos(alpha)*point[2];
	val[1] = cos(alpha)*point[1] - sin(alpha)*point[2];
	val[2] = -1 * point[0] * sin(beta) + sin(alpha)*cos(beta)*point[1] + cos(alpha)*cos(beta)*point[2];


	//rad-late�̌v�Z
	double rad[3] = { 1, 0.2, 0 };
	double length = sqrt(rad[0] * rad[0] + rad[1] * rad[1] + rad[2] * rad[2]);
	rad[0] = rad[0] / length;
	rad[1] = rad[1] / length;
	rad[2] = rad[2] / length;
	printf("rad: %g, %g, %g\n", rad[0], rad[1], rad[2]);
	printf("lat: %g, %g, %g\n", -1*rad[1], rad[0], rad[2]);


	//double projection_axis0[3] = { 0.1,0.3,1 };
	//double projection_axis1[3] = { -0.3,0.1,0 };
	//double projection_axis2[3] = { -0.1,-0.3,0.1*0.1 + 0.3*0.3 };
	
	
	length = sqrt(pow(projection_axis0[0], 2) + pow(projection_axis0[1], 2) + pow(projection_axis0[2], 2));
	point[0] = projection_axis0[0] / length;
	point[1] = projection_axis0[1] / length;
	point[2] = projection_axis0[2] / length;
	printf("val: %g, %g, %g\n", point[0], point[1], point[2]);

	val[0] = point[0] * cos(beta) + sin(alpha)*sin(beta)*point[1] + sin(beta)*cos(alpha)*point[2];
	val[1] = cos(alpha)*point[1] - sin(alpha)*point[2];
	val[2] = -1 * point[0] * sin(beta) + sin(alpha)*cos(beta)*point[1] + cos(alpha)*cos(beta)*point[2];

	//printf("val: %g, %g, %g\n", val[0], val[1], val[2]);

	length = sqrt(pow(projection_axis1[0], 2) + pow(projection_axis1[1], 2) + pow(projection_axis1[2], 2));
	point[0] = projection_axis1[0] / length;
	point[1] = projection_axis1[1] / length;
	point[2] = projection_axis1[2] / length;
	printf("val: %g, %g, %g\n", point[0], point[1], point[2]);

	val[0] = point[0] * cos(beta) + sin(alpha)*sin(beta)*point[1] + sin(beta)*cos(alpha)*point[2];
	val[1] = cos(alpha)*point[1] - sin(alpha)*point[2];
	val[2] = -1 * point[0] * sin(beta) + sin(alpha)*cos(beta)*point[1] + cos(alpha)*cos(beta)*point[2];

	//printf("val: %g, %g, %g\n", val[0], val[1], val[2]);

	length = sqrt(pow(projection_axis2[0], 2) + pow(projection_axis2[1], 2) + pow(projection_axis2[2], 2));
	point[0] = projection_axis2[0] / length;
	point[1] = projection_axis2[1] / length;
	point[2] = projection_axis2[2] / length;
	printf("val: %g, %g, %g\n", point[0], point[1], point[2]);

	val[0] = point[0] * cos(beta) + sin(alpha)*sin(beta)*point[1] + sin(beta)*cos(alpha)*point[2];
	val[1] = cos(alpha)*point[1] - sin(alpha)*point[2];
	val[2] = -1 * point[0] * sin(beta) + sin(alpha)*cos(beta)*point[1] + cos(alpha)*cos(beta)*point[2];

	//printf("val: %g, %g, %g\n", val[0], val[1], val[2]);


}
void Calc_rotation(double val[3], double alpha, double beta) {
	double point[3];
	point[0] = val[0];
	point[1] = val[1];
	point[2] = val[2];

	val[0] = point[0] * cos(beta) + sin(alpha)*sin(beta)*point[1] + sin(beta)*cos(alpha)*point[2];
	val[1] = cos(alpha)*point[1] - sin(alpha)*point[2];
	val[2] = -1 * point[0] * sin(beta) + sin(alpha)*cos(beta)*point[1] + cos(alpha)*cos(beta)*point[2];

}
void Calc_rotation_xy(double val[3], double theta){
	double point[3];
	point[0] = val[0];
	point[1] = val[1];
	point[2] = val[2];

	val[0] = point[0] * cos(theta) - point[1] * sin(theta);
	val[1] = point[0] * sin(theta) +point[1] * cos(theta);
	val[2] = point[2];

}

double calc_theta(double val[3]) {
	double length = val[0] * val[0] + val[1] * val[1];
	double cos = val[0] / length;
	double sin = -1 * val[1] / length;

}