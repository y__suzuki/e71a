#include <picojson.h>
#include <iostream>
#include <fstream>
#include <cassert>  // std::assert
#include <sstream>
#include <iomanip>
#include <string>
#include <numeric>
#include <filesystem>
#include <vector>
#include <map>

class EachShot_Param {
public:
	int View, Imager, StartAnalysisPicNo, GridX, GridY;
	double Z_begin, X_center, Y_center;
	std::string TrackFilePath;
	//GridX,Y intでダイジョブ?
};
class EachView_Param {
public:
	int  LayerID, NPicThickOfLayer;
	double Stage_x, Stage_y, ThickOfLayer, Z_begin, Z_end;
};

void file_check(std::string filename, std::string log_file);
bool track_data_check(std::string filename, std::vector<EachShot_Param>&shot_all, std::map<int, EachView_Param> &view_map, std::string log_file);
std::vector<EachShot_Param> read_EachShot(std::string filename);
std::map<int, EachView_Param> read_EachView(std::string filename);


int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:prg tracking_data_path pl area delete_path flg output_Error_file\n");
		exit(1);
	}
	int pl = std::stoi(argv[2]);
	int area = std::stoi(argv[3]);
	std::string file_tracking_path = argv[1];
	std::string file_delete=argv[4];
	int delete_flg = std::stoi(argv[5]);
	std::string file_error_log = argv[6];

	std::stringstream file_tracking_DATA, file_in_beta;
	file_in_beta << file_tracking_path << "\\Area" << area << "\\PL" << std::setfill('0') << std::setw(3) << pl;
	file_tracking_DATA << file_tracking_path << "\\Area" << area << "\\PL" << std::setfill('0') << std::setw(3) << pl << "\\DATA";

	//EachImager:1視野内での各sensorの値 センサー数(72)個
	//EachShot:全視野での各sensorの値 視野数*センサー数(72)個
	//EachView:各視野の値 視野数
	std::string file_in_Beta_EachShotParam = file_in_beta.str() + "\\Beta_EachShotParam.json";
	std::string file_in_Beta_EachViewParam = file_in_beta.str() + "\\Beta_EachViewParam.json";


	file_check(file_tracking_DATA.str(), file_error_log);
	file_check(file_in_Beta_EachShotParam, file_error_log);
	file_check(file_in_Beta_EachViewParam, file_error_log);


	std::vector<EachShot_Param> shot_vec = read_EachShot(file_in_Beta_EachShotParam);
	std::map<int, EachView_Param> view_map = read_EachView(file_in_Beta_EachViewParam);

	track_data_check(file_tracking_DATA.str(), shot_vec, view_map, file_error_log);
	if (delete_flg == 1) {

		std::filesystem::remove_all(file_delete);

	}
	return 0;
}

void file_check(std::string filename, std::string log_file) {
	if (!std::filesystem::exists(filename)) {
		std::ofstream ofs(log_file, std::ios::app);
		ofs << filename << " not found" << std::endl;
		exit(1);
	}
}

std::vector<EachShot_Param> read_EachShot(std::string filename) {
	//JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}

	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	//位置-->shot paramに行けるようにしたい-->全beta.json読み込んだ後でやる
	std::vector<EachShot_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachShot_Param param;
		param.Z_begin = obj["Z_begin"].get<double>();
		param.View = (int)obj["View"].get<double>();
		param.Imager = (int)obj["Imager"].get<double>();
		param.StartAnalysisPicNo = (int)obj["StartAnalysisPicNo"].get<double>();
		param.GridX = (int)obj["GridX"].get<double>();
		param.GridY = (int)obj["GridY"].get<double>();
		param.TrackFilePath = obj["TrackFilePath"].get<std::string>();
		param.X_center = -1;
		param.Y_center = -1;
		ret.push_back(param);
	}
	printf("number of shot = %zd\n", ret.size());

	return ret;
}
std::map<int, EachView_Param> read_EachView(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}
	std::map<int, EachView_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	int ViewID = 0;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachView_Param param;
		param.Stage_x = obj["Stage_x"].get<double>();
		param.Stage_y = obj["Stage_y"].get<double>();
		param.ThickOfLayer = obj["ThickOfLayer"].get<double>();
		param.Z_begin = obj["Z_begin"].get<double>();
		param.Z_end = obj["Z_end"].get<double>();
		param.LayerID = (int)obj["LayerID"].get<double>();
		param.NPicThickOfLayer = (int)obj["NPicThickOfLayer"].get<double>();
		ret.insert(std::make_pair(ViewID, param));
		ViewID++;
	}
	printf("number of view = %d\n", ViewID);
	return ret;
}

bool track_data_check(std::string filename, std::vector<EachShot_Param>&shot_all, std::map<int, EachView_Param> &view_map,std::string log_file) {
	EachView_Param view;
	int count = 0, all = shot_all.size();
	for (auto shot : shot_all) {
		if (count % 100 == 0) {
			printf("\r file check %5d/%5d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		if (view_map.count(shot.View) == 0) {
			std::ofstream ofs(log_file, std::ios::app);
			ofs << filename << " view=" << shot.View << "not found" << std::endl;
			exit(1);
		}
		view = view_map.at(shot.View);
		std::stringstream check_file;
		check_file << filename << "\\" << std::setfill('0') << std::setw(2) << shot.Imager / 12 << "_" << std::setfill('0') << std::setw(2) << shot.Imager % 12
			<< "\\TrackHit2_0_" << std::setfill('0') << std::setw(8) << shot.View << "_" << view.LayerID << "_000";
		file_check(check_file.str() + ".dat", log_file);
		file_check(check_file.str() + "_0.dat", log_file);
		file_check(check_file.str() + "_1.dat", log_file);
		file_check(check_file.str() + "_2.dat", log_file);
	}
	printf("\r file check %5d/%5d(%4.1lf%%)\n", count, all, count*100. / all);

	return true;
}