#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_inf {
public:
	int groupid, chainid, nseg, npl;
	double ax, ay, vph_ave, vph_sigma;
};
class unit_inf {
public:
	int groupid, chainid, pl[2], rawid[2];
};
std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch);
std::map<double, std::pair<double, double>> vph_mip_distribution();
std::vector<output_inf> Calc_nseg_npl(std::vector<Momentum_recon::Event_information> &ev_v, std::map<double, std::pair<double, double>> &vph_mip);
void output_file(std::string filename, std::vector<output_inf>&output);
std::vector<Momentum_recon::Event_information> Cut_vph(std::vector<Momentum_recon::Event_information> &ev_v, std::map<double, std::pair<double, double>> &vph_mip, std::vector<unit_inf> &cut_unit);
void output_unit_inf(std::string filename, std::vector<unit_inf>&output);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file-in-momch file-in-ECC fa.txt file-out-momch file-out-inf\n");
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];
	std::string file_out = argv[3];
	std::string file_out_unit = argv[4];


	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);

	std::map<double, std::pair<double, double>> vph_mip = vph_mip_distribution();

	std::vector<output_inf> out = Calc_nseg_npl(momch, vph_mip);

	output_file(file_out, out);

	std::vector<unit_inf> cut_unit;
	momch = Cut_vph(momch, vph_mip,cut_unit);
	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);
	output_unit_inf(file_out_unit, cut_unit);

	std::set<int> group;
	for (auto itr = cut_unit.begin(); itr != cut_unit.end(); itr++) {
		group.insert(itr->groupid);
	}
	for (auto itr = group.begin(); itr != group.end(); itr++) {
		printf("%d\n", *itr);
	}

}

std::map<double, std::pair<double, double>> vph_mip_distribution() {
	std::map<double, std::pair<double, double>> ret;
	ret.insert(std::make_pair(0.05, std::make_pair(61.86, 6.8)));
	ret.insert(std::make_pair(0.15, std::make_pair(49.68, 7.6)));
	ret.insert(std::make_pair(0.25, std::make_pair(36.38, 5.0)));
	ret.insert(std::make_pair(0.35, std::make_pair(30.85, 4.0)));
	ret.insert(std::make_pair(0.45, std::make_pair(29.20, 3.8)));
	ret.insert(std::make_pair(0.55, std::make_pair(27.17, 3.9)));
	ret.insert(std::make_pair(0.65, std::make_pair(25.14, 3.8)));
	ret.insert(std::make_pair(0.80, std::make_pair(22.26, 3.6)));
	ret.insert(std::make_pair(1.00, std::make_pair(19.83, 3.3)));
	ret.insert(std::make_pair(1.20, std::make_pair(18.39, 3.2)));
	ret.insert(std::make_pair(1.40, std::make_pair(17.24, 3.1)));
	ret.insert(std::make_pair(1.70, std::make_pair(16.02, 3.0)));
	ret.insert(std::make_pair(2.10, std::make_pair(15.29, 3.0)));
	ret.insert(std::make_pair(2.50, std::make_pair(14.19, 3.0)));
	return ret;
}

std::vector<output_inf> Calc_nseg_npl(std::vector<Momentum_recon::Event_information> &ev_v, std::map<double, std::pair<double, double>> &vph_mip) {
	std::vector<output_inf> ret;
	output_inf out;
	int vertex_pl, direction;

	for (auto &ev : ev_v) {
		for (auto itr = ev.chains.begin(); itr != ev.chains.end(); itr++) {
			if (itr->chainid == 0)continue;

			vertex_pl = ev.vertex_pl;
			direction = itr->direction;
			Momentum_recon::Mom_chain chain;
			chain = *itr;
			//chain.base.clear();
			//chain.base_pair.clear();

			out.groupid = ev.groupid;
			out.chainid = chain.chainid;

			out.npl = chain.base.rbegin()->pl - chain.base.begin()->pl + 1;
			out.nseg = chain.base.size();
			out.ax = 0;
			out.ay = 0;
			out.vph_ave = 0;
			int count_angle = 0;
			int count_vph = 0;
			for (auto &b : chain.base) {
				out.ax += b.ax;
				out.ay += b.ay;
				out.vph_ave += b.m[0].ph % 10000;
				out.vph_ave += b.m[1].ph % 10000;
				count_angle += 1;
				count_vph += 2;
			}
			out.ax /= count_angle;
			out.ay /= count_angle;
			out.vph_ave /= count_vph;
			double angle = sqrt(out.ax*out.ax + out.ay * out.ay);

			double sigma = 0;
			auto vph_angle = vph_mip.upper_bound(angle);
			if (vph_angle == vph_mip.begin()) {
				sigma = (out.vph_ave - vph_angle->second.first) / vph_angle->second.second;
			}
			else if (vph_angle == vph_mip.end()) {
				sigma = (out.vph_ave - vph_mip.rbegin()->second.first) / vph_mip.rbegin()->second.second;
			}
			else {
				double vph_mean, vph_sigma;
				auto val0 = std::next(vph_angle, -1);
				auto val1 = vph_angle;
				vph_mean = (val1->second.first - val0->second.first) / (val1->first - val0->first)*(angle - val0->first) + val0->second.first;
				vph_sigma = (val1->second.second - val0->second.second) / (val1->first - val0->first)*(angle - val0->first) + val0->second.second;
				sigma = (out.vph_ave - vph_mean) / vph_sigma;
			}
			out.vph_sigma = sigma;
			ret.push_back(out);
		}
	}
	return ret;

}

std::vector<Momentum_recon::Event_information> Cut_vph(std::vector<Momentum_recon::Event_information> &ev_v, std::map<double, std::pair<double, double>> &vph_mip,std::vector<unit_inf> &cut_unit) {
	std::vector<Momentum_recon::Event_information> ret;
	output_inf out;
	int vertex_pl, direction;
	for (auto &ev : ev_v) {
		Momentum_recon::Event_information cut_ev = ev;
		cut_ev.chains.clear();
		for (auto itr = ev.chains.begin(); itr != ev.chains.end(); itr++) {
			if (itr->chainid == 0) {
				cut_ev.chains.push_back(*itr);
				continue;
			}

			vertex_pl = ev.vertex_pl;
			direction = itr->direction;
			Momentum_recon::Mom_chain chain;
			chain = *itr;
			//chain.base.clear();
			//chain.base_pair.clear();

			out.groupid = ev.groupid;
			out.chainid = chain.chainid;
			out.npl = chain.base.rbegin()->pl - chain.base.begin()->pl + 1;
			out.nseg = chain.base.size();
			out.ax = 0;
			out.ay = 0;
			out.vph_ave = 0;
			int count_angle = 0;
			int count_vph = 0;
			for (auto &b : chain.base) {
				out.ax += b.ax;
				out.ay += b.ay;
				out.vph_ave += b.m[0].ph % 10000;
				out.vph_ave += b.m[1].ph % 10000;
				count_angle += 1;
				count_vph += 2;
			}
			out.ax /= count_angle;
			out.ay /= count_angle;
			out.vph_ave /= count_vph;
			double angle = sqrt(out.ax*out.ax + out.ay * out.ay);

			double sigma = 0;
			auto vph_angle = vph_mip.upper_bound(angle);
			if (vph_angle == vph_mip.begin()) {
				sigma = (out.vph_ave - vph_angle->second.first) / vph_angle->second.second;
			}
			else if (vph_angle == vph_mip.end()) {
				sigma = (out.vph_ave - vph_mip.rbegin()->second.first) / vph_mip.rbegin()->second.second;
			}
			else {
				double vph_mean, vph_sigma;
				auto val0 = std::next(vph_angle, -1);
				auto val1 = vph_angle;
				vph_mean = (val1->second.first - val0->second.first) / (val1->first - val0->first)*(angle - val0->first) + val0->second.first;
				vph_sigma = (val1->second.second - val0->second.second) / (val1->first - val0->first)*(angle - val0->first) + val0->second.second;
				sigma = (out.vph_ave - vph_mean) / vph_sigma;
			}
			out.vph_sigma = sigma;

			if (out.npl == 1) {
				if (out.vph_sigma < 20)continue;
			}
			//unit track�̂΂���
			else if (out.npl ==2&&(chain.base[0].pl<=15|| chain.base[0].pl%2==0)) {
				if (out.vph_sigma < 10) {
					unit_inf uni;
					uni.groupid = ev.groupid;
					uni.chainid = itr->chainid;
					uni.pl[0] = chain.base[0].pl;
					uni.pl[1] = chain.base[1].pl;
					uni.rawid[0] = chain.base[0].rawid;
					uni.rawid[1] = chain.base[1].rawid;
					cut_unit.push_back(uni);
					continue;
				}
			}
			cut_ev.chains.push_back(*itr);
		}
		ret.push_back(cut_ev);
	}
	return ret;

}
void output_file(std::string filename, std::vector<output_inf>&output) {
	std::ofstream ofs(filename);
	for (auto itr = output.begin(); itr != output.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(6) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(6) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(6) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(6) << std::setprecision(0) << itr->npl << " "
			<< std::setw(6) << std::setprecision(1) << itr->vph_ave << " "
			<< std::setw(6) << std::setprecision(4) << itr->vph_sigma << std::endl;
	}
}

void output_unit_inf(std::string filename, std::vector<unit_inf>&output) {
	std::ofstream ofs(filename);
	for (auto itr = output.begin(); itr != output.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(10) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(3) << std::setprecision(0) << itr->pl[0] << " "
			<< std::setw(10) << std::setprecision(0) << itr->rawid[0] << " "
			<< std::setw(3) << std::setprecision(0) << itr->pl[1] << " "
			<< std::setw(10) << std::setprecision(0) << itr->rawid[1] << std::endl;
	}


}

