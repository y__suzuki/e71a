#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
void out_connection_value(std::string filename, std::vector < mfile0::Mfile>&m);
bool connect_judge(mfile0::M_Chain &c0, mfile0::M_Chain &c1);
void connect_judge(mfile0::M_Chain &c0, mfile0::M_Chain &c1, std::ofstream &ofs);

bool checkFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}
int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-mfile-path(selected) inpu-mfile-path(all) out-txt out-txt\n");
		exit(1);
	}
	std::string file_in_mfile0 = argv[1];
	std::string file_in_mfile1 = argv[2];
	std::string file_out_txt0 = argv[3];
	std::string file_out_txt1 = argv[4];
	std::vector<mfile0::Mfile> m_vec;
	int num = 0;
	while (true) {
		if (num > 100)break;
		std::stringstream in_mfile;
		in_mfile << file_in_mfile0 << "\\m_out_" << std::setw(3) << std::setfill('0') << num << ".all";
		if (!checkFileExistence(in_mfile.str())) {
			break;
		}
		mfile0::Mfile m;
		mfile1::read_mfile_extension(in_mfile.str(), m);
		m_vec.push_back(m);
		num++;
	}
	
	std::vector<mfile0::Mfile> m_vec2;
	for (int i = 0; i < num; i++) {
		std::stringstream in_mfile;
		in_mfile << file_in_mfile1 << "\\m_out_" << std::setw(3) << std::setfill('0') << i << ".all";
		if (!checkFileExistence(in_mfile.str())) {
			continue;
		}
		mfile0::Mfile m;
		mfile1::read_mfile_extension(in_mfile.str(), m);
		m_vec2.push_back(m);
	}

	out_connection_value(file_out_txt0, m_vec);
	out_connection_value(file_out_txt1, m_vec2);

}
void out_connection_value(std::string filename, std::vector < mfile0::Mfile>&m) {
	std::ofstream ofs(filename);
	bool flg = false;
	for (auto itr_m = m.begin(); itr_m != m.end(); itr_m++) {
		mfile0::M_Chain c0;
		flg = false;
		for (auto itr = itr_m->chains.begin(); itr != itr_m->chains.end(); itr++) {
			if (itr->pos0 / 10 < 30) {
				c0 = *itr;
				flg = true;
			}
		}
		if (!flg)continue;
		for (auto itr = itr_m->chains.begin(); itr != itr_m->chains.end(); itr++) {
			if (itr->chain_id == c0.chain_id)continue;
			connect_judge(c0,*itr, ofs);
		}
	}
}
bool connect_judge(mfile0::M_Chain &c0, mfile0::M_Chain &c1) {
	matrix_3D::vector_3D dir0, dir1, pos0, pos1;
	dir0.x = mfile0::chain_ax(c0);
	dir0.y = mfile0::chain_ay(c0);
	dir0.z = 1;

	dir1.x = mfile0::chain_ax(c1);
	dir1.y = mfile0::chain_ay(c1);
	dir1.z = 1;

	pos0.x = c0.basetracks.rbegin()->x;
	pos0.y = c0.basetracks.rbegin()->y;
	pos0.z = c0.basetracks.rbegin()->z;

	pos1.x = c1.basetracks.begin()->x;
	pos1.y = c1.basetracks.begin()->y;
	pos1.z = c1.basetracks.begin()->z;
	double oa, md;
	double range_z[2] = { pos1.z ,pos0.z };
	double extra[2];
	oa = matrix_3D::opening_angle(dir0, dir1);
	md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, range_z, extra);
	if (md > 200)return false;
	if (fabs(oa) > 0.3)return false;
	printf("oa:%lf md %lf\n", oa, md);

	return true;
}
void connect_judge(mfile0::M_Chain &c0, mfile0::M_Chain &c1, std::ofstream &ofs) {
	matrix_3D::vector_3D dir0, dir1, pos0, pos1;
	dir0.x = mfile0::chain_ax(c0);
	dir0.y = mfile0::chain_ay(c0);
	dir0.z = 1;

	dir1.x = mfile0::chain_ax(c1);
	dir1.y = mfile0::chain_ay(c1);
	dir1.z = 1;

	pos0.x = c0.basetracks.rbegin()->x;
	pos0.y = c0.basetracks.rbegin()->y;
	pos0.z = c0.basetracks.rbegin()->z;

	pos1.x = c1.basetracks.begin()->x;
	pos1.y = c1.basetracks.begin()->y;
	pos1.z = c1.basetracks.begin()->z;
	double oa, md;
	double range_z[2] = { pos1.z ,pos0.z };
	double extra[2];
	oa = matrix_3D::opening_angle(dir0, dir1);
	md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, range_z, extra);
	if (md > 1000)return;
	if (fabs(oa) > 1.0)return;
	ofs << std::right << std::fixed
		<< std::setw(12) << std::setprecision(0) << c0.chain_id << " "
		<< std::setw(3) << std::setprecision(0) << c0.pos0 / 10 << " "
		<< std::setw(3) << std::setprecision(0) << c0.pos1 / 10 << " "
		<< std::setw(3) << std::setprecision(0) << c0.nseg << " "
		<< std::setw(7) << std::setprecision(4) << dir0.x << " "
		<< std::setw(7) << std::setprecision(4) << dir0.y << " "
		<< std::setw(12) << std::setprecision(0) << c1.chain_id << " "
		<< std::setw(3) << std::setprecision(0) << c1.pos0 / 10 << " "
		<< std::setw(3) << std::setprecision(0) << c1.pos1 / 10 << " "
		<< std::setw(3) << std::setprecision(0) << c1.nseg << " "
		<< std::setw(7) << std::setprecision(4) << dir1.x << " "
		<< std::setw(7) << std::setprecision(4) << dir1.y << " "
		<< std::setw(6) << std::setprecision(4) << oa << " "
		<< std::setw(6) << std::setprecision(1) << md << std::endl;
	return;
}