#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
#include <set>
class Matched_base {
public:
	vxx::base_track_t base_pre;
	std::vector<vxx::base_track_t >base;
};
class Match_param {
public:
	//[0]x+[1]
	double lateral_position_difference[2];
	double radial_position_difference[2];
	double lateral_angle_difference[2];
	double radial_angle_difference[2];
};

Match_param Set_param();
std::vector<Matched_base> basetrack_matching(std::vector<vxx::base_track_t>&base_pre, std::vector<vxx::base_track_t> &base_all, Match_param&param);
void output_matched_track(std::string filename, std::vector<Matched_base>&match);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:in-prediction pl in-all pl res\n");
		exit(1);
	}
	std::string file_in_base_pre = argv[1];
	int pl_pre = std::stoi(argv[2]);
	std::string file_in_base_all = argv[3];
	int pl_all = std::stoi(argv[4]);
	std::string file_out = argv[5];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base_pre = br.ReadAll(file_in_base_pre, pl_pre, 0);
	std::vector<vxx::base_track_t> base_all = br.ReadAll(file_in_base_all, pl_all, 0);
	Match_param param = Set_param();
	std::vector<Matched_base> match = basetrack_matching(base_pre, base_all, param);

	output_matched_track(file_out, match);

}
Match_param Set_param() {
	Match_param param;
	param.lateral_angle_difference[0] = 0;
	param.lateral_angle_difference[1] = 0.05;
	param.radial_angle_difference[0] = 0.1;
	param.radial_angle_difference[1] = 0.1;
	param.lateral_position_difference[0] = 0;
	param.lateral_position_difference[1] = 50;
	param.radial_position_difference[0] = 30;
	param.radial_position_difference[1] = 50;
	return param;
}
std::vector<Matched_base> basetrack_matching(std::vector<vxx::base_track_t>&base_pre, std::vector<vxx::base_track_t> &base_all, Match_param&param) {

	double xmin, ymin;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		if (itr == base_all.begin()) {
			xmin = itr->x; 
			ymin = itr->y;
		}
		xmin = std::min(itr->x, xmin);
		ymin = std::min(itr->y, ymin);
	}

	std::multimap<std::pair<int, int>, vxx::base_track_t> base_map;
	std::pair<int, int> id;
	double hash_size = 2000;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		id.first = (itr->x - xmin) / hash_size;
		id.second = (itr->y - ymin) / hash_size;
		base_map.insert(std::make_pair(id, *itr));
	}

	std::vector<vxx::base_track_t> base_buf;
	std::vector<Matched_base> ret;
	int ix, iy;
	double angle;
	double diff_lat_pos, diff_lat_ang, diff_rad_ang, diff_rad_pos;
	double dax, day, dx, dy;
	int count = 0;

	for (auto itr = base_pre.begin(); itr != base_pre.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r basetrack matching %d/%d(%4.1lf%%)", count, base_pre.size(), count*100. / base_pre.size());
		}
		count++;

		Matched_base match;
		match.base_pre = *itr;
		base_buf.clear();
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		ix = (itr->x - xmin) / hash_size;
		iy = (itr->y - ymin) / hash_size;
		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				id.first = ix + iix;
				id.second = iy + iiy;
				if (base_map.count(id) == 0)continue;
				auto range = base_map.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					base_buf.push_back(res->second);
				}
			}
		}

		for (auto itr2 = base_buf.begin(); itr2 != base_buf.end(); itr2++) {
			dax = itr->ax - itr2->ax;
			day = itr->ay- itr2->ay;
			dx = itr->x - itr2->x;
			dy = itr->y - itr2->y;

			if (angle < 0.1) {
				if (fabs(dax) > param.lateral_angle_difference[1])continue;
				if (fabs(day) > param.lateral_angle_difference[1])continue;
				if (fabs(dx) > param.lateral_position_difference[1])continue;
				if (fabs(dy) > param.lateral_position_difference[1])continue;
				match.base.push_back(*itr2);
			}
			else {
				diff_rad_ang = dax * itr->ax + day * itr->ay;
				diff_lat_ang = dax * itr->ay - day * itr->ax;
				diff_rad_pos = dx * itr->ax + dy * itr->ay;
				diff_lat_pos = dx * itr->ay - dy * itr->ax;
				if (fabs(diff_lat_pos) > (param.lateral_position_difference[0] * angle + param.lateral_position_difference[1])*angle)continue;
				if (fabs(diff_rad_pos) > (param.radial_position_difference[0] * angle + param.radial_position_difference[1])*angle)continue;
				if (fabs(diff_lat_ang) > (param.lateral_angle_difference[0] * angle + param.lateral_angle_difference[1])*angle)continue;
				if (fabs(diff_rad_ang) > (param.radial_angle_difference[0] * angle + param.radial_angle_difference[1])*angle)continue;
				match.base.push_back(*itr2);
			}
		}
		ret.push_back(match);
	}
	fprintf(stderr, "\r basetrack matching %d/%d(%4.1lf%%)\n", count, base_pre.size(), count*100. / base_pre.size());
	return ret;
}

void output_matched_track(std::string filename, std::vector<Matched_base>&match) {
	std::ofstream ofs(filename);
	uint32_t ShotID;
	int ViewID = 0, ImagerID = 0, NumberOfImager=72;

	int count = 0;
	for (auto itr = match.begin(); itr != match.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r file write %d/%d(%4.1lf%%)", count, match.size(), count*100. / match.size());
		}
		count++;


		ofs << std::right << std::fixed
			<< std::setw(7) << std::setprecision(4) << itr->base_pre.ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->base_pre.ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->base_pre.x << " "
			<< std::setw(8) << std::setprecision(1) << itr->base_pre.y << " "
			<< std::setw(3) << std::setprecision(0) << itr->base.size() << " ";
		vxx::base_track_t base;
		base.ax = 0;
		base.ay = 0;
		base.x = 0;
		base.y = 0;
		ViewID = -1;
		ImagerID = -1;
		double dist = -1;
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			if (itr2 == itr->base.begin()) {
				base = *itr2;
				dist = pow(itr->base_pre.x - itr2->x, 2) + pow(itr->base_pre.y - itr2->y, 2);
				ShotID = ((uint32_t)(uint16_t)base.m[0].row << 16) | ((uint32_t)(uint16_t)base.m[0].col);
				ViewID = ShotID / NumberOfImager;
				ImagerID = ShotID % NumberOfImager;

			}
			if (dist > pow(itr->base_pre.x - itr2->x, 2) + pow(itr->base_pre.y - itr2->y, 2)) {
				base = *itr2;
				dist = pow(itr->base_pre.x - itr2->x, 2) + pow(itr->base_pre.y - itr2->y, 2);
				ShotID = ((uint32_t)(uint16_t)base.m[0].row << 16) | ((uint32_t)(uint16_t)base.m[0].col);
				ViewID = ShotID / NumberOfImager;
				ImagerID = ShotID % NumberOfImager;

			}
		}
		ofs << std::right << std::fixed
			<< std::setw(7) << std::setprecision(4) << base.ax << " "
			<< std::setw(7) << std::setprecision(4) << base.ay << " "
			<< std::setw(8) << std::setprecision(1) << base.x << " "
			<< std::setw(8) << std::setprecision(1) << base.y << " "
			<< std::setw(5) << std::setprecision(0) << ViewID << " "
			<< std::setw(5) << std::setprecision(0) << ImagerID << std::endl;




	}
	printf("\r file write %d/%d(%4.1lf%%)\n", count, match.size(), count*100. / match.size());

}