#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>

void Set_area(std::string file_Area_path, int pl0, int pl1, std::map<int, double>&xmin, std::map<int, double>&xmax, std::map<int, double>&ymin, std::map<int, double>&ymax);
void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs);

void xy_map_correction(std::map<int, double>&xmin, std::map<int, double>&xmax, std::map<int, double>&ymin, std::map<int, double>&ymax, std::vector<corrmap0::Corrmap> &corr_abs);
void output_area(std::string filename, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-base in-corr-abs in-z out-map\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	std::string file_in_corr = argv[2];
	std::string file_in_z = argv[3];
	std::string file_out = argv[4];

	//corrmap absの読み込み
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corr, corr_abs);

	//gap nominal read
	chamber1::Chamber chamber;
	chamber1::read_structure(file_in_z, chamber);

	std::map<int, double> z, xmin, xmax, ymin, ymax;

	z=chamber1::base_z_convert(chamber);
	Set_area(file_in_base, 1, 133, xmin, xmax, ymin, ymax);
	z_map_correction(z, corr_abs);
	
	xy_map_correction(xmin, xmax, ymin, ymax, corr_abs);

	output_area(file_out,xmin, xmax, ymin, ymax, z);

}
void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs) {

	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		auto res = z_map.find(pl);
		if (res == z_map.end())continue;
		res->second += itr->dz;
		//printf("%03d z:%.1lf\n", pl,res->second);
	}

}
void xy_map_correction(std::map<int, double>&xmin, std::map<int, double>&xmax, std::map<int, double>&ymin, std::map<int, double>&ymax, std::vector<corrmap0::Corrmap> &corr_abs) {

	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		auto res0 = xmin.find(pl);
		auto res1 = xmax.find(pl);
		auto res2 = ymin.find(pl);
		auto res3 = ymax.find(pl);
		if (res0 == xmin.end())continue;
		if (res1 == xmax.end())continue;
		if (res2 == ymin.end())continue;
		if (res3 == ymax.end())continue;
		double tmp_pos[4];
		tmp_pos[0] = res0->second;
		tmp_pos[1] = res1->second;
		tmp_pos[2] = res2->second;
		tmp_pos[3] = res3->second;
		double cand[2];
		cand[0] = tmp_pos[0] * itr->position[0] + tmp_pos[2] * itr->position[1] + itr->position[4];
		cand[1] = tmp_pos[0] * itr->position[0] + tmp_pos[3] * itr->position[1] + itr->position[4];
		res0->second = std::min(cand[0], cand[1]);
		cand[0] = tmp_pos[1] * itr->position[0] + tmp_pos[2] * itr->position[1] + itr->position[4];
		cand[1] = tmp_pos[1] * itr->position[0] + tmp_pos[3] * itr->position[1] + itr->position[4];
		res1->second = std::max(cand[0], cand[1]);
		cand[0] = tmp_pos[0] * itr->position[2] + tmp_pos[2] * itr->position[3] + itr->position[5];
		cand[1] = tmp_pos[1] * itr->position[2] + tmp_pos[2] * itr->position[3] + itr->position[5];
		res2->second = std::min(cand[0], cand[1]);
		cand[0] = tmp_pos[0] * itr->position[2] + tmp_pos[3] * itr->position[3] + itr->position[5];
		cand[1] = tmp_pos[1] * itr->position[2] + tmp_pos[3] * itr->position[3] + itr->position[5];
		res3->second = std::max(cand[0], cand[1]);
	}

}

void Set_area(std::string file_Area_path, int pl0, int pl1, std::map<int, double>&xmin, std::map<int, double>&xmax, std::map<int, double>&ymin, std::map<int, double>&ymax) {

	for (int pl = pl0; pl <= pl1; pl++) {
		printf("now PL%03d\n", pl);
		std::stringstream file_in_base;
		file_in_base << file_Area_path << "\\PL" << std::setw(3) << std::setfill('0') << pl
			<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
		if (!std::filesystem::exists(file_in_base.str())) {
			fprintf(stderr,"%s not exist\n", file_in_base.str().c_str());
			continue;
		}

		vxx::BvxxReader br;
		std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), pl, 0);
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			auto res0 = xmin.insert(std::make_pair(itr->pl, itr->x));
			auto res1 = xmax.insert(std::make_pair(itr->pl, itr->x));
			auto res2 = ymin.insert(std::make_pair(itr->pl, itr->y));
			auto res3 = ymax.insert(std::make_pair(itr->pl, itr->y));
			if (!res0.second)res0.first->second = std::min(res0.first->second, itr->x);
			if (!res1.second)res1.first->second = std::max(res1.first->second, itr->x);
			if (!res2.second)res2.first->second = std::min(res2.first->second, itr->y);
			if (!res3.second)res3.first->second = std::max(res3.first->second, itr->y);
		}
		printf("PL%03d %8.1lf %8.1lf %8.1lf %8.1lf\n", pl,xmin[pl], xmax[pl], ymin[pl], ymax[pl]);
	}



}

void output_area(std::string filename, std::map<int, double> &xmin, std::map<int, double> &xmax, std::map<int, double> &ymin, std::map<int, double> &ymax, std::map<int, double> &z) {
	std::ofstream ofs(filename);


	for (auto itr = z.begin(); itr != z.end(); itr++) {
		double area[4];
		int pl = itr->first;
		auto res0 = xmin.find(pl);
		auto res1 = xmax.find(pl);
		auto res2 = ymin.find(pl);
		auto res3 = ymax.find(pl);
		if (res0 == xmin.end() || res1 == xmax.end() || res2 == ymin.end() || res3 == ymax.end()) {
			fprintf(stderr, "PL%03 exception\n", pl);
			//exit(1);
			continue;
		}
		ofs << std::right << std::fixed << std::setfill(' ')
			<< std::setw(3) << std::setprecision(0) << pl << " "
			<< std::setw(8) << std::setprecision(1) << res0->second << " "
			<< std::setw(8) << std::setprecision(1) << res1->second << " "
			<< std::setw(8) << std::setprecision(1) << res2->second << " "
			<< std::setw(8) << std::setprecision(1) << res3->second << " "
			<< std::setw(8) << std::setprecision(1) << itr->second << std::endl;

	}
}