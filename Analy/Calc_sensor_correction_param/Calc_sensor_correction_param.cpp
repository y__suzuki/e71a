#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
class input_param {
public:
	int i_sensor;
	double angle_min, angle_max, mom_min, mom_max, mean, mean_error, sigma, sigma_error;
};

void Read_param(std::string filename, std::vector<input_param>&sensor0, std::vector<input_param>&sensor1);
void Calc_ab(input_param&sensor0_mip, std::vector<input_param>&sensor0_hip_v, std::vector<input_param>&sensor1_mip_v, std::vector<input_param>&sensor1_hip_v, double &a, double &b, double &a_err, double &b_err);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage;file_in_mip file_in_hip output\n");
		exit(1);
	}
	std::string file_in_mip = argv[1];
	std::string file_in_hip = argv[2];
	std::string file_out= argv[3];

	std::vector<input_param>sensor0_mip, sensor0_hip;
	std::vector<input_param>sensor1_mip, sensor1_hip;

	Read_param(file_in_mip, sensor0_mip, sensor1_mip);
	Read_param(file_in_hip, sensor0_hip, sensor1_hip);

	std::ofstream ofs(file_out);
	for (auto &p : sensor0_mip) {
		double a, b, a_err, b_err;
		Calc_ab(p, sensor0_hip, sensor1_mip, sensor1_hip, a, b, a_err, b_err);

		
		ofs << std::right << std::fixed
			<< std::setw(5) << std::setprecision(2) << p.angle_min << " "
			<< std::setw(5) << std::setprecision(2) << p.angle_max << " "
			<< std::setw(10) << std::setprecision(4) << a << " "
			<< std::setw(10) << std::setprecision(4) << a_err << " "
			<< std::setw(10) << std::setprecision(4) << b << " "
			<< std::setw(10) << std::setprecision(4) << b_err << std::endl;
	}

}
void Read_param(std::string filename, std::vector<input_param>&sensor0, std::vector<input_param>&sensor1) {
	std::ifstream ifs(filename);
	input_param in;
	while (ifs >> in.i_sensor >> in.angle_min >> in.angle_max >> in.mom_min >> in.mom_max >> in.mean >> in.mean_error >> in.sigma >> in.sigma_error) {
		if (in.i_sensor == 0) {
			sensor0.push_back(in);
		}
		else {
			sensor1.push_back(in);
		}
	}
}
void Calc_ab(input_param&sensor0_mip, std::vector<input_param>&sensor0_hip_v, std::vector<input_param>&sensor1_mip_v, std::vector<input_param>&sensor1_hip_v, double &a, double &b, double &a_err, double &b_err) {
	double angle = (sensor0_mip.angle_min + sensor0_mip.angle_max) / 2;
	double mom = (sensor0_mip.mom_min + sensor0_mip.mom_max) / 2;

	input_param sensor0_hip, sensor1_mip, sensor1_hip;
	bool flg_detect = false;
	for (auto &p : sensor0_hip_v) {
		if (p.angle_min < angle&&angle < p.angle_max) {
			if (p.mom_min < mom&&mom < p.mom_max) {
				flg_detect = true;
				sensor0_hip = p;
			}
		}
	}
	if (!flg_detect) {
		fprintf(stderr, "angle:%.2lf\n", angle);
		fprintf(stderr, "mom:%.2lf\n", mom);
		fprintf(stderr, "not found\n");
	}
	flg_detect = false;
	for (auto &p : sensor1_mip_v) {
		if (p.angle_min < angle&&angle < p.angle_max) {
			if (p.mom_min < mom&&mom < p.mom_max) {
				flg_detect = true;
				sensor1_mip = p;
			}
		}
	}
	if (!flg_detect) {
		fprintf(stderr, "angle:%.2lf\n", angle);
		fprintf(stderr, "mom:%.2lf\n", mom);
		fprintf(stderr, "not found\n");
	}
	flg_detect = false;
	for (auto &p : sensor1_hip_v) {
		if (p.angle_min < angle&&angle < p.angle_max) {
			if (p.mom_min < mom&&mom < p.mom_max) {
				flg_detect = true;
				sensor1_hip = p;
			}
		}
	}
	if (!flg_detect) {
		fprintf(stderr, "angle:%.2lf\n", angle);
		fprintf(stderr, "mom:%.2lf\n", mom);
		fprintf(stderr, "not found\n");
	}

	a = (sensor1_hip.mean - sensor1_mip.mean) / (sensor0_hip.mean - sensor0_mip.mean);
	b = (sensor0_hip.mean *sensor1_mip.mean - sensor0_mip.mean*sensor1_hip.mean) / (sensor0_hip.mean - sensor0_mip.mean);
	a_err = sqrt(
		pow(sensor1_hip.mean_error / (sensor0_hip.mean - sensor0_mip.mean), 2) +
		pow(sensor1_mip.mean_error / (sensor0_hip.mean - sensor0_mip.mean), 2) +
		pow(sensor0_hip.mean_error*a / (sensor0_hip.mean - sensor0_mip.mean), 2) +
		pow(sensor0_mip.mean_error*a / (sensor0_hip.mean - sensor0_mip.mean), 2) 
	);
	b_err = sqrt(
		pow(sensor1_hip.mean_error *sensor0_mip.mean / (sensor0_hip.mean - sensor0_mip.mean), 2) +
		pow(sensor1_mip.mean_error *sensor0_hip.mean / (sensor0_hip.mean - sensor0_mip.mean), 2) +
		pow(sensor0_hip.mean_error *(sensor1_mip.mean*(sensor0_hip.mean - sensor0_mip.mean) - (sensor0_hip.mean *sensor1_mip.mean - sensor0_mip.mean*sensor1_hip.mean)) / pow(sensor0_hip.mean - sensor0_mip.mean, 2), 2) +
		pow(sensor0_mip.mean_error *-1 * (sensor1_hip.mean*(sensor0_hip.mean - sensor0_mip.mean) + (sensor0_hip.mean *sensor1_mip.mean - sensor0_mip.mean*sensor1_hip.mean)) / pow(sensor0_hip.mean - sensor0_mip.mean, 2), 2)
	);


}

