#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Event_information> proton_selection(std::vector<Momentum_recon::Event_information>&momch);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "uasge:input-chain-inf output-pid\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_infmomch = argv[2];
	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);

	momch = proton_selection(momch);
	Momentum_recon::Write_Event_information_extension(file_out_infmomch, momch);
}
std::vector<Momentum_recon::Event_information> proton_selection(std::vector<Momentum_recon::Event_information>&momch) {
	std::vector<Momentum_recon::Event_information> ret;
	for (auto &ev : momch) {
		Momentum_recon::Event_information g_tmp = ev;
		g_tmp.chains.clear();
		for (auto &c : ev.chains) {
			if (c.particle_flg != 2212)continue;
			if (c.Get_proton_mcs_pb() > 500)continue;
			g_tmp.chains.push_back(c);
		}
		if (g_tmp.chains.size() > 0) {
			ret.push_back(g_tmp);
		}

	}

	return ret;
}