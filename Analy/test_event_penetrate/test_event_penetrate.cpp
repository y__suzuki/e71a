#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>


std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch);

bool penetrate_check_first(Momentum_recon::Mom_chain&mu, Momentum_recon::Mom_chain&partner);

std::vector<Momentum_recon::Mom_chain> converge_momch(std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&event_momch);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file_in_momch file_out_momch\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_out = argv[2];
	std::string file_out_reject = argv[3];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);

	std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> mom_event = divide_event(momch);
	
	std::vector<Momentum_recon::Mom_chain> reject_momch;

	for (auto itr = mom_event.begin(); itr != mom_event.end(); itr++) {
		//printf("event %d\n", itr->first);
		std::vector<Momentum_recon::Mom_chain> p;
		for (auto partner : itr->second.second) {
			if (!penetrate_check_first(itr->second.first,partner)) {
				p.push_back(partner);
				continue;
			}
			reject_momch.push_back(partner);
		}
		itr->second.second = p;
	}
	
	momch = converge_momch(mom_event);
	Momentum_recon::Write_mom_chain_extension(file_out, momch);
	Momentum_recon::Write_mom_chain_extension(file_out_reject, reject_momch);
	//for (auto itr = mom_event.begin(); itr != mom_event.end(); itr++) {
	//	printf("%d\n", itr->first);
	//	for (auto &b : itr->second.first.base) {
	//		printf("%4d %10d %.4lf %.4lf\n", b.pl, b.rawid, b.ax, b.ay);
	//	}
	//	for (auto itr2 = itr->second.second.begin(); itr2 != itr->second.second.end(); itr2++) {
	//		printf("chianid %d\n", itr2->chainid);
	//		for (auto &b : itr2->base) {
	//			printf("%4d %10d %.4lf %.4lf\n", b.pl, b.rawid, b.ax, b.ay);
	//		}
	//	}
	//}

}
std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch) {
	std::multimap<int, Momentum_recon::Mom_chain> partner;
	std::map<int, Momentum_recon::Mom_chain> muon;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		if (itr->chainid == 0) {
			muon.insert(std::make_pair(itr->groupid, *itr));
		}
		else {
			partner.insert(std::make_pair(itr->groupid, *itr));
		}
	}

	std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> ret;

	for (auto itr = muon.begin(); itr != muon.end(); itr++) {
		std::vector<Momentum_recon::Mom_chain> p;
		if (partner.count(itr->first) == 0) {
			ret.insert(std::make_pair(itr->first,std::make_pair(itr->second, p)));
			continue;
		}
		auto range = partner.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			p.push_back(res->second);
		}
		ret.insert(std::make_pair(itr->first, std::make_pair(itr->second, p)));
	}
	return ret;
}
bool penetrate_check_first(Momentum_recon::Mom_chain&mu, Momentum_recon::Mom_chain&partner) {

	int muon_pl = mu.base.rbegin()->pl;
	int down_pl = partner.base.begin()->pl;
	int up_pl = partner.base.rbegin()->pl;

	if (down_pl <= muon_pl && up_pl <= muon_pl)return false;
	if ( muon_pl<down_pl && muon_pl < up_pl )return false;
	return true;

}
bool penetrate_check_second(Momentum_recon::Mom_chain&mu, Momentum_recon::Mom_chain&partner) {

	int muon_pl = mu.base.rbegin()->pl;

	int down_pl = partner.base.begin()->pl;
	int up_pl = partner.base.rbegin()->pl;

	if (down_pl <= muon_pl && up_pl <= muon_pl)return false;
	if (muon_pl < down_pl && muon_pl < up_pl)return false;
	return true;



}
std::vector<Momentum_recon::Mom_chain> converge_momch(std::map<int, std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&event_momch) {
	std::vector<Momentum_recon::Mom_chain> ret;
	for (auto itr = event_momch.begin(); itr != event_momch.end(); itr++) {
		ret.push_back(itr->second.first);
		for (auto itr2 = itr->second.second.begin(); itr2 != itr->second.second.end(); itr2++) {
			ret.push_back(*itr2);
		}


	}
	return ret;
}