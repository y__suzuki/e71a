#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void SetDirection(std::vector<Momentum_recon::Event_information> &momch);

int main(int argc, char** argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	SetDirection(momch);
	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);


}
void SetDirection(std::vector<Momentum_recon::Event_information> &momch) {
	for (auto &ev : momch) {
		int vertex_pl = 0;
		for (auto itr = ev.chains.begin(); itr != ev.chains.end(); itr++) {
			if (itr->chainid == 0) {
				vertex_pl = itr->base.rbegin()->pl;
				break;
			}
		}
		if (vertex_pl <= 0) {
			fprintf(stderr, "muon track not found %d\n", ev.groupid);
			continue;
		}
		for (auto itr = ev.chains.begin(); itr != ev.chains.end(); itr++) {
			if (itr->base.rbegin()->pl <= vertex_pl) {
				itr->direction = 1;
			}
			else if (itr->base.begin()->pl > vertex_pl) {
				itr->direction = -1;
			}
			else {
				fprintf(stderr, "%d %d penetrate track\n", ev.groupid, itr->chainid);
				continue;
			}
		}
	}
}