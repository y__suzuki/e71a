#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int main(int argc, char** argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg\n");
	}
	std::string file_in_momch = argv[1];
	std::string file_in_sf = argv[2];
	std::string file_in_ECC = argv[3];
	std::string file_out_momch = argv[4];

	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_extension(file_in_sf);

	std::vector<vxx::base_track_t> base[2];
	vxx::BvxxReader br;
	base[0] = br.ReadAll(file_in_ECC + "\\Area0\\PL003\b003.sel.cor.vxx", 3, 0);
	base[1] = br.ReadAll(file_in_ECC + "\\Area0\\PL004\b004.sel.cor.vxx", 4, 0);




	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);

}
