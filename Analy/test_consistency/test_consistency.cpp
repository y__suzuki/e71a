#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"


#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <map>
#include <tuple>
class microtrack_inf {
public:
	int zone, pos, col, row, isg, ph, pixelnum, hitnum;
};

std::vector<microtrack_inf> read_microtrack_inf(std::string filename);
bool matching(microtrack_inf&m1, microtrack_inf&m2);
void print_mismatch(microtrack_inf&m1, microtrack_inf&m2);
void file_matching(std::vector<microtrack_inf> &m1, std::vector<microtrack_inf> &m2);

int main() {


	std::string file_path1 = "I:\\NINJA\\E71a\\work\\suzuki\\Microtrack\\test\\test2\\";
	std::string file_path2 = "I:\\NINJA\\E71a\\work\\suzuki\\Microtrack\\test\\test3\\";

	std::string filename, filename1, filename2;
	std::vector<microtrack_inf> m1, m2;
	{
		filename = "micro_inf_thick_0";
		printf("%s start\n", filename.c_str());
		filename1 = file_path1 + filename;
		filename2 = file_path2 + filename;
		m1 = read_microtrack_inf(filename1);
		m2 = read_microtrack_inf(filename2);
		file_matching(m1, m2);
		m1.clear();
		m2.clear();
	}
	{
		filename = "micro_inf_thick_1";
		printf("%s start\n", filename.c_str());
		filename1 = file_path1 + filename;
		filename2 = file_path2 + filename;
		m1 = read_microtrack_inf(filename1);
		m2 = read_microtrack_inf(filename2);
		file_matching(m1, m2);
		m1.clear();
		m2.clear();
	}
	{
		filename = "micro_inf_thick_2";
		printf("%s start\n", filename.c_str());
		filename1 = file_path1 + filename;
		filename2 = file_path2 + filename;
		m1 = read_microtrack_inf(filename1);
		m2 = read_microtrack_inf(filename2);
		file_matching(m1, m2);
		m1.clear();
		m2.clear();
	}
	{
		filename = "micro_inf_thick_3";
		printf("%s start\n", filename.c_str());
		filename1 = file_path1 + filename;
		filename2 = file_path2 + filename;
		m1 = read_microtrack_inf(filename1);
		m2 = read_microtrack_inf(filename2);
		file_matching(m1, m2);
		m1.clear();
		m2.clear();
	}
	{
		filename = "micro_inf_thin_0";
		printf("%s start\n", filename.c_str());
		filename1 = file_path1 + filename;
		filename2 = file_path2 + filename;
		m1 = read_microtrack_inf(filename1);
		m2 = read_microtrack_inf(filename2);
		file_matching(m1, m2);
		m1.clear();
		m2.clear();
	}
	{
		filename = "micro_inf_thin_1";
		printf("%s start\n", filename.c_str());
		filename1 = file_path1 + filename;
		filename2 = file_path2 + filename;
		m1 = read_microtrack_inf(filename1);
		m2 = read_microtrack_inf(filename2);
		file_matching(m1, m2);
		m1.clear();
		m2.clear();
	}
	{
		filename = "micro_inf_thin_2";
		printf("%s start\n", filename.c_str());
		filename1 = file_path1 + filename;
		filename2 = file_path2 + filename;
		m1 = read_microtrack_inf(filename1);
		m2 = read_microtrack_inf(filename2);
		file_matching(m1, m2);
		m1.clear();
		m2.clear();
	}
	{
		filename = "micro_inf_thin_3";
		printf("%s start\n", filename.c_str());
		filename1 = file_path1 + filename;
		filename2 = file_path2 + filename;
		m1 = read_microtrack_inf(filename1);
		m2 = read_microtrack_inf(filename2);
		file_matching(m1, m2);
		m1.clear();
		m2.clear();
	}
}
std::vector<microtrack_inf> read_microtrack_inf(std::string filename) {
	std::vector<microtrack_inf> ret;

	std::ifstream ifs(filename, std::ios::binary);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	microtrack_inf m;
	while (ifs.read((char*)& m, sizeof(microtrack_inf))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(m);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;

}
void file_matching(std::vector<microtrack_inf> &m1, std::vector<microtrack_inf> &m2) {
	std::map<std::tuple<int, int, int>, microtrack_inf*> map1, map2;

	for (auto itr = m1.begin(); itr != m1.end(); itr++) {
		map1.insert(std::make_pair(std::make_tuple(itr->col, itr->row, itr->isg), &(*itr)));
	}
	for (auto itr = m2.begin(); itr != m2.end(); itr++) {
		map2.insert(std::make_pair(std::make_tuple(itr->col, itr->row, itr->isg), &(*itr)));
	}

	std::tuple<int, int, int> id;
	for (auto itr = m1.begin(); itr != m1.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (map2.count(id) == 0) {
			printf("m1 exist,m2 not found\n");
			microtrack_inf m;
			m = *itr;
			std::cout << std::right << std::fixed
				<< std::setw(3) << std::setprecision(0) << m.pos << " "
				<< std::setw(8) << std::setprecision(0) << m.col << " "
				<< std::setw(8) << std::setprecision(0) << m.row << " "
				<< std::setw(10) << std::setprecision(0) << m.isg << " "
				<< std::setw(3) << std::setprecision(0) << m.ph << " "
				<< std::setw(5) << std::setprecision(0) << m.pixelnum << " "
				<< std::setw(5) << std::setprecision(0) << m.hitnum << " "
				<< std::setw(3) << std::setprecision(0) << m.zone << std::endl;
			exit(1);
		}
		auto res = map2.at(id);
		if (!matching(*itr, *res)) {
			print_mismatch(*itr, *res);
			exit(1);
		}

	}
	for (auto itr = m2.begin(); itr != m2.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;
		if (map1.count(id) == 0) {
			printf("m2 exist,m1 not found\n");
			microtrack_inf m;
			m = *itr;
			std::cout << std::right << std::fixed
				<< std::setw(3) << std::setprecision(0) << m.pos << " "
				<< std::setw(8) << std::setprecision(0) << m.col << " "
				<< std::setw(8) << std::setprecision(0) << m.row << " "
				<< std::setw(10) << std::setprecision(0) << m.isg << " "
				<< std::setw(3) << std::setprecision(0) << m.ph << " "
				<< std::setw(5) << std::setprecision(0) << m.pixelnum << " "
				<< std::setw(5) << std::setprecision(0) << m.hitnum << " "
				<< std::setw(3) << std::setprecision(0) << m.zone << std::endl;
			exit(1);
		}
		auto res = map1.at(id);
		if (!matching(*itr, *res)) {
			print_mismatch(*itr, *res);
			exit(1);
		}

	}
}
bool matching(microtrack_inf&m1, microtrack_inf&m2) {
	if (m1.col != m2.col)return false;
	if (m1.hitnum != m2.hitnum)return false;
	if (m1.isg != m2.isg)return false;
	if (m1.ph != m2.ph)return false;
	if (m1.pixelnum != m2.pixelnum)return false;
	if (m1.pos != m2.pos)return false;
	if (m1.row != m2.row)return false;
	//if (m1.zone != m2.zone)return false;
	return true;
}
void print_mismatch(microtrack_inf&m1, microtrack_inf&m2) {
	microtrack_inf m;
	m = m1;
	std::cout << std::right << std::fixed
		<< std::setw(3) << std::setprecision(0) << m.pos << " "
		<< std::setw(8) << std::setprecision(0) << m.col << " "
		<< std::setw(8) << std::setprecision(0) << m.row << " "
		<< std::setw(10) << std::setprecision(0) << m.isg << " "
		<< std::setw(3) << std::setprecision(0) << m.ph << " "
		<< std::setw(5) << std::setprecision(0) << m.pixelnum << " "
		<< std::setw(5) << std::setprecision(0) << m.hitnum << " "
		<< std::setw(3) << std::setprecision(0) << m.zone << std::endl;

	m = m2;
	std::cout << std::right << std::fixed
		<< std::setw(3) << std::setprecision(0) << m.pos << " "
		<< std::setw(8) << std::setprecision(0) << m.col << " "
		<< std::setw(8) << std::setprecision(0) << m.row << " "
		<< std::setw(10) << std::setprecision(0) << m.isg << " "
		<< std::setw(3) << std::setprecision(0) << m.ph << " "
		<< std::setw(5) << std::setprecision(0) << m.pixelnum << " "
		<< std::setw(5) << std::setprecision(0) << m.hitnum << " "
		<< std::setw(3) << std::setprecision(0) << m.zone << std::endl;
}