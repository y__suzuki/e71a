#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>

std::vector<mfile0::M_Chain> Chain_Selection(std::vector<mfile0::M_Chain> c);
void Chain_angle_distribution(std::vector<mfile0::M_Chain> c);
void d_lateral_output(std::vector<mfile0::M_Chain> c, std::string filename);
std::vector<mfile0::M_Chain> Chain_Selection_lateral(std::vector<mfile0::M_Chain> c, double threshold_lateral);
void prediction_reliability(std::vector<mfile0::M_Chain> c, int pl, std::string filename);
std::vector<netscan::base_track_t> make_prediction(std::vector<mfile0::M_Chain> c, int pl, std::map<int, double> gap);

int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mfile out-bvxx pl\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_bvxx = argv[2];
	int prediction_pl = std::stoi(argv[3]);

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::map<int, double> gap;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			gap.insert(std::make_pair(itr2->pos / 10, itr2->z));
		}
	}

	m.chains = Chain_Selection(m.chains);
	//Chain_angle_distribution(m.chains);
	d_lateral_output(m.chains, "diff_lat.txt");
	m.chains = Chain_Selection_lateral(m.chains, 0.003);
	Chain_angle_distribution(m.chains);
	prediction_reliability(m.chains, prediction_pl, "diff_adjacent.txt");
	std::vector<netscan::base_track_t> prediction= make_prediction(m.chains, prediction_pl, gap);
	printf("hogw\n");
	netscan::write_basetrack_vxx(file_out_bvxx, prediction, prediction_pl, 0);
}
std::vector<mfile0::M_Chain> Chain_Selection(std::vector<mfile0::M_Chain> c) {
	std::vector<mfile0::M_Chain> sel0;
	int count = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		count = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (itr2->pos / 10 == 5)count++;
			if (itr2->pos / 10 == 6)count++;
			if (itr2->pos / 10 == 7)count++;
			if (itr2->pos / 10 == 8)count++;
			if (itr2->pos / 10 == 9)count++;
			if (itr2->pos / 10 == 10)count++;
			if (itr2->pos / 10 == 11)count++;
			if (itr2->pos / 10 == 12)count++;
			if (itr2->pos / 10 == 13)count++;
			if (itr2->pos / 10 == 14)count++;
			if (itr2->pos / 10 == 15)count++;
		}

		if (count == 11) {
			sel0.push_back(*itr);
		}
	}
	printf("chain Fe ECC penetrate %d --> %d\n", c.size(), sel0.size());

	//gorup1本化
	std::vector<mfile0::M_Chain> sel1;
	std::multimap<int, mfile0::M_Chain*> group;
	std::vector<mfile0::M_Chain> g_tmp1,g_tmp2;
	for (auto itr = sel0.begin(); itr != sel0.end(); itr++) {
		group.insert(std::make_pair(itr->basetracks.begin()->group_id, &(*itr)));
	}
	for (auto itr = group.begin(); itr != group.end(); ) {
		count = group.count(itr->first);
		if (count == 1) {
			sel1.push_back(*(itr->second));
			itr = std::next(itr, 1);
		}
		else {
			//groupに複数のchain
			g_tmp1.clear();
			g_tmp2.clear();
			int nseg_max = 0;

			for (int i = 0; i < count; i++) {
				g_tmp1.push_back(*(itr->second));
				nseg_max = std::max(nseg_max, itr->second->nseg);
				itr = std::next(itr, 1);
			}

			for (auto itr2 = g_tmp1.begin(); itr2 != g_tmp1.end(); itr2++) {
				if (itr2->nseg == nseg_max) {
					g_tmp2.push_back(*itr2);
				}
			}

			if (g_tmp2.size() == 1) {
				sel1.push_back(g_tmp2[0]);
			}
			else {
				mfile0::M_Chain select;
				double diff;
				for (auto itr2 = g_tmp2.begin(); itr2 != g_tmp2.end(); itr2++) {
					if (itr2 == g_tmp2.begin()) {
						diff = mfile0::angle_diff_dev_lat(*itr2, mfile0::chain_ax(*itr2), mfile0::chain_ay(*itr2));
						select = *itr2;
					}
					if (diff > mfile0::angle_diff_dev_lat(*itr2, mfile0::chain_ax(*itr2), mfile0::chain_ay(*itr2))) {
						diff = mfile0::angle_diff_dev_lat(*itr2, mfile0::chain_ax(*itr2), mfile0::chain_ay(*itr2));
						select = *itr2;
					}
				}
				sel1.push_back(select);
			}
		}
	}
	printf("group selection %d --> %d\n", sel0.size(), sel1.size());

	return sel1;
}
std::vector<mfile0::M_Chain> Chain_Selection_lateral(std::vector<mfile0::M_Chain> c,double threshold_lateral) {
	std::vector<mfile0::M_Chain> sel0;

	double ax, ay;
	double d_lat, d_lat2, d_lat1;
	int count = 0;
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		count = 0;
		ax = 0;
		ay = 0;
		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			if (5 <= itr->pos / 10 && itr->pos / 10 <= 15) {
				ax += itr->ax;
				ay += itr->ay;
				count++;
			}
		}
		ax = ax / count;
		ay = ay / count;

		count = 0;
		d_lat = 0;
		d_lat1 = 0;
		d_lat2 = 0;

		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			if (5 <= itr->pos / 10 && itr->pos / 10 <= 15) {
				d_lat = ((itr->ax - ax)*ay - (itr->ay - ay)*ax) / sqrt(ax*ax + ay * ay);
				d_lat1 += d_lat;
				d_lat2 += d_lat * d_lat;
				count++;
			}
		}
		if (sqrt(d_lat2 / count - pow(d_lat / count, 2)) > threshold_lateral)continue;
		sel0.push_back(*itr_c);
	}
	printf("chain lateral angle cut(<%5.4lf) %d --> %d\n",threshold_lateral, c.size(), sel0.size());
	return sel0;

}

void Chain_angle_distribution(std::vector<mfile0::M_Chain> c) {
	std::map<int, int> angle_map;
	double angle;
	int i_ang;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		angle = sqrt(pow(mfile0::chain_ax(*itr), 2) + pow(mfile0::chain_ay(*itr), 2));
		i_ang = angle / 0.1;
		auto res = angle_map.insert(std::make_pair(i_ang, 1));
		if (!res.second) {
			res.first->second++;
		}
	}
	for (auto itr = angle_map.begin(); itr != angle_map.end(); itr++) {
		printf("%3.1lf -%3.1lf : %8d\n", itr->first*0.1, (itr->first + 1)*0.1, itr->second);
	}
}

void d_lateral_output(std::vector<mfile0::M_Chain> c, std::string filename) {
	std::ofstream ofs(filename);

	double ax, ay;
	double d_lat, d_lat2,d_lat1;
	int count = 0;
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		count = 0;
		ax = 0;
		ay = 0;
		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			if (5 <= itr->pos / 10 && itr->pos / 10 <= 15) {
				ax += itr->ax;
				ay += itr->ay;
				count++;
			}
		}
		ax = ax / count;
		ay = ay / count;

		count = 0;
		d_lat = 0;
		d_lat1 = 0;
		d_lat2 = 0;

		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			if (5 <= itr->pos / 10 && itr->pos / 10 <= 15) {
				d_lat = ((itr->ax - ax)*ay - (itr->ay - ay)*ax) / sqrt(ax*ax + ay * ay);
				d_lat1 += d_lat;
				d_lat2 += d_lat * d_lat;
				count++;
			}
		}
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr_c->chain_id << " "
			<< std::setw(7) << std::setprecision(4) << ax << " "
			<< std::setw(7) << std::setprecision(4) << ay << " "
			<< std::setw(7) << std::setprecision(4) << sqrt(ax*ax + ay * ay) << " "
			<< std::setw(7) << std::setprecision(6) << sqrt(d_lat2 / count - pow(d_lat / count, 2)) << std::endl;
	}
}
void prediction_reliability(std::vector<mfile0::M_Chain> c, int pl, std::string filename) {
	std::ofstream ofs(filename);
	mfile0::M_Base adjacent[2];
	int count = 0;
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		count = 0;
		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			if (itr->pos / 10 - pl == 1) {
				adjacent[0] = *itr;
			}
			else if (itr->pos / 10 - pl == -1) {
				adjacent[1] = *itr;
			}
			if (abs(itr->pos / 10 - pl) == 1 || abs(itr->pos / 10 - pl) == 2) {
				count++;
			}
		}
		if (count != 4)continue;
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr_c->chain_id << " "
			<< std::setw(7) << std::setprecision(4) << adjacent[0].ax << " "
			<< std::setw(7) << std::setprecision(4) << adjacent[0].ay << " "
			<< std::setw(8) << std::setprecision(1) << adjacent[0].x << " "
			<< std::setw(8) << std::setprecision(1) << adjacent[0].y << " "
			<< std::setw(8) << std::setprecision(1) << adjacent[0].z << " "
			<< std::setw(7) << std::setprecision(4) << adjacent[1].ax << " "
			<< std::setw(7) << std::setprecision(4) << adjacent[1].ay << " "
			<< std::setw(8) << std::setprecision(1) << adjacent[1].x << " "
			<< std::setw(8) << std::setprecision(1) << adjacent[1].y << " "
			<< std::setw(8) << std::setprecision(1) << adjacent[1].z << std::endl;


	}

}
std::vector<netscan::base_track_t> make_prediction(std::vector<mfile0::M_Chain> c, int pl,std::map<int,double> gap) {
	if (gap.find(pl) == gap.end() || gap.find(pl - 1) == gap.end() || gap.find(pl + 1) == gap.end()) {
		fprintf(stderr, "gap not foune\n");
		exit(1);
	}

	int ex_pl;
	if (fabs(gap[pl] - gap[pl - 1]) < fabs(gap[pl] - gap[pl + 1])) {
		ex_pl = pl - 1;
	}
	else {
		ex_pl = pl + 1;
	}
	fprintf(stderr, "PL%03d --> PL%03d extrapolate\n", ex_pl, pl);
	std::vector < netscan::base_track_t> ret;
	std::vector<mfile0::M_Base> base;
	mfile0::M_Base base_tmp;

	int count = 0;
	int isg = 0;
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		base.clear();
		count = 0;
		for (auto itr = itr_c->basetracks.begin(); itr!=itr_c->basetracks.end(); itr++) {
			if (itr->pos / 10 != pl) {
				base.push_back(*itr);
			}
			if (abs(itr->pos / 10 - pl) == 1 || abs(itr->pos / 10 - pl) == 2) {
				count++;
			}
			if (itr->pos / 10 == ex_pl) {
				base_tmp = *itr;
			}
		}
		if (count == 4) {
			netscan::base_track_t ret_base;
			ret_base.dmy = 0;
			ret_base.isg = isg;
			isg++;
			ret_base.pl = pl;
			ret_base.rawid = itr_c->chain_id;
			ret_base.zone = 0;
			ret_base.ax = 0;
			ret_base.ay = 0;
			for (auto itr = base.begin(); itr != base.end(); itr++) {
				ret_base.ax += itr->ax;
				ret_base.ay += itr->ay;
			}
			ret_base.ax = ret_base.ax / base.size();
			ret_base.ay = ret_base.ay / base.size();
			ret_base.x = base_tmp.x + (gap[pl] - gap[ex_pl])*ret_base.ax;
			ret_base.y = base_tmp.y + (gap[pl] - gap[ex_pl])*ret_base.ay;
			ret_base.z = gap[pl];

			ret_base.m[0].ax = ret_base.ax;
			ret_base.m[0].ay = ret_base.ay;
			ret_base.m[0].col = 0;
			ret_base.m[0].row = 0;
			ret_base.m[0].isg = isg;
			ret_base.m[0].zone = 0;
			ret_base.m[0].z = 0;
			ret_base.m[0].rawid = ret_base.rawid;
			ret_base.m[0].ph = int(base_tmp.ph / 20000) * 10000 + int((base_tmp.ph % 10000) / 2);
			ret_base.m[0].pos = ret_base.pl * 10 + 1;

			ret_base.m[1].ax = ret_base.ax;
			ret_base.m[1].ay = ret_base.ay;
			ret_base.m[1].col = 0;
			ret_base.m[1].row = 0;
			ret_base.m[1].isg = isg;
			ret_base.m[1].zone = 0;
			ret_base.m[1].z = 210;
			ret_base.m[1].rawid = ret_base.rawid;
			ret_base.m[1].ph = int(base_tmp.ph / 20000) * 10000 + int((base_tmp.ph % 10000) / 2);
			ret_base.m[1].pos = ret_base.pl * 10 + 2;

			ret.push_back(ret_base);
		}
	}
	return ret;
}