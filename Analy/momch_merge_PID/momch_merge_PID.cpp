#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class PID_inf {
public:
	int groupid, chainid, pid;
	double  angle, pb, vph, proton_likelihood, pion_likelihood, likelihood_ratio;
};
std::vector<PID_inf> read_pid_inf(std::string filename);
void Add_PID_inf(std::vector<Momentum_recon::Event_information> &momch, std::vector<PID_inf>&pid_inf);


int main(int argc, char** argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:filename");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_in_pid = argv[2];
	std::string file_out_momch = argv[3];


	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	std::vector<PID_inf> pid_inf=read_pid_inf(file_in_pid);

	 Add_PID_inf(momch, pid_inf);


	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);

}
std::vector<PID_inf> read_pid_inf(std::string filename) {
	std::vector<PID_inf> ret;
	PID_inf c_tmp;
	int num = 0;
	std::ifstream ifs(filename.c_str());
	while (ifs >> c_tmp.groupid >> c_tmp.chainid >> c_tmp.pid >> c_tmp.angle >> c_tmp.pb >> c_tmp.vph) {
		if (num % 100000 == 0) {
			printf("\r read chain %d", num);
		}
		num++;

		if (c_tmp.pb < 0)c_tmp.pb = 10;
		ret.push_back(c_tmp);
	}
	printf("\r read chain %d\n", num);

	ifs.close();
	return ret;
}

void Add_PID_inf(std::vector<Momentum_recon::Event_information> &momch, std::vector<PID_inf>&pid_inf) {
	std::map<std::pair<int, int>, PID_inf>pid_map;
	for (auto itr = pid_inf.begin(); itr != pid_inf.end(); itr++) {
		auto res=pid_map.insert(std::make_pair(std::make_pair(itr->groupid, itr->chainid), *itr));
		if (!res.second) {
			printf("double information %5d %5d\n", itr->groupid, itr->chainid);
		}
	}

	std::pair<int, int> id;
	for (auto &ev : momch) {
		id.first = ev.groupid;

		for (auto &c : ev.chains) {
			id.second = c.chainid;

			if (pid_map.count(id) == 0)continue;
			auto res = pid_map.find(id);
			c.particle_flg = res->second.pid;
			//c.muon_likelihood = res->second.pion_likelihood;
			//c.proton_likelihood = res->second.proton_likelihood;
		}
	}
}
