#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class Sharing_file {
public:
	int32_t pl, ecc_id, fixedwall_id, trackerwall_id, spotid, zone[2], rawid[2], unix_time, tracker_track_id;
	//spotid:spotA * 100 + spotB
};

std::vector<Sharing_file>  Read_sharing_file_txt(std::string filename);
std::multimap<std::pair<int, int>, Sharing_file> correspond_sharingfile_base(std::vector<Sharing_file>&sf, std::string file_in_ECC);
bool output_chain_ind(std::multimap<int, mfile0::M_Chain>&event_chain, std::multimap<std::pair<int, int>, Sharing_file>&sf);
void output_chain(mfile0::M_Chain &chains);
std::multimap<int, mfile0::M_Chain> chain_event(std::vector<mfile0::M_Chain>&c);

int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "filename:prg \n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_shifter = argv[2];
	std::string file_in_ECC = argv[3];

	std::vector<Sharing_file>  sf = Read_sharing_file_txt(file_in_shifter);

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	std::multimap<std::pair<int, int>, Sharing_file> sf_map = correspond_sharingfile_base(sf, file_in_ECC);
	std::multimap<int, mfile0::M_Chain> event_chains = chain_event(m.chains);

	while (output_chain_ind(event_chains, sf_map)) {

	}

}

std::vector<Sharing_file>  Read_sharing_file_txt(std::string filename) {
	std::vector<Sharing_file> ret;
	std::ifstream ifs(filename);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	Sharing_file t;
	//while (ifs.read((char*)& t, sizeof(Sharing_file))) {
	while (ifs >> t.pl >> t.ecc_id >> t.fixedwall_id >> t.trackerwall_id >> t.spotid >> t.zone[0] >> t.rawid[0] >> t.zone[1] >> t.rawid[1] >> t.unix_time >> t.tracker_track_id) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(t);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no data!\n", filename.c_str());
		exit(1);
	}
	return ret;
}

std::multimap<std::pair<int, int>, Sharing_file> correspond_sharingfile_base(std::vector<Sharing_file>&sf, std::string file_in_ECC) {

	std::stringstream file_in_base;
	file_in_base << file_in_ECC << "\\Area0\\PL003\\b003.sel.cor.vxx";
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), 3, 0);
	std::multimap<std::tuple<int, int, int, int>, Sharing_file> sf_map;
	
	std::tuple<int, int, int, int> id;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		std::get<0>(id) = itr->zone[0];
		std::get<1>(id) = itr->rawid[0];
		std::get<2>(id) = itr->zone[1];
		std::get<3>(id) = itr->rawid[1];

		sf_map.insert(std::make_pair(id, *itr));
	}

	std::multimap<std::pair<int, int>, Sharing_file> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::get<0>(id) = itr->m[0].zone;
		std::get<1>(id) = itr->m[0].rawid;
		std::get<2>(id) = itr->m[1].zone;
		std::get<3>(id) = itr->m[1].rawid;

		if (sf_map.count(id) == 0)continue;
		auto range = sf_map.equal_range(id);
		for (auto res = range.first; res != range.second; res++) {
			ret.insert(std::make_pair(std::make_pair(itr->pl, itr->rawid), res->second));
		}


	}

	printf("%d --> %d\n", sf.size(), ret.size());
	return ret;

}

std::multimap<int, mfile0::M_Chain> chain_event(std::vector<mfile0::M_Chain>&c) {

	std::multimap<int, mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		ret.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));

	}
	return ret;
}

bool output_chain_ind(std::multimap<int, mfile0::M_Chain>&event_chain, std::multimap<std::pair<int, int>, Sharing_file>&sf) {

	int eventid = 0;
	printf("input eventid\n");
	std::cin >> eventid;
	if (eventid < 0)return false;

	if (event_chain.count(eventid) == 0) {
		printf("eventid = %d not found\n");
		return true;
	}
	auto range = event_chain.equal_range(eventid);
	std::vector<mfile0::M_Chain > chains;
	std::map<int, std::pair<int, int>> base_rawid;
	for (auto res = range.first; res != range.second; res++) {
		chains.push_back(res->second);
		output_chain(res->second);

		for (auto &b : res->second.basetracks) {
			if (b.pos / 10 == 3) {
				base_rawid.insert(std::make_pair(res->second.chain_id, std::make_pair(b.pos / 10, b.rawid)));
			}
		}
	}
	
	printf("PL003 reached chain %d\n",base_rawid.size());
	if (base_rawid.size() == 0)return true;

	for (auto itr = base_rawid.begin(); itr != base_rawid.end(); itr++) {
		printf("chainid = %d\n", itr->first);
		int timestamp = -1;
		if (sf.count(itr->second) == 0) {
			printf("-1\n");
		}
		else {
			auto range2 = sf.equal_range(itr->second);
			for (auto res2 = range2.first; res2 != range2.second; res2++) {
				printf("%5d %12d %5d\n", res2->second.spotid, res2->second.unix_time, res2->second.tracker_track_id);
			}
		}
	}
	
	return true;

}
void output_chain(mfile0::M_Chain &chains) {
	std::cout << std::right << std::fixed<<std::setfill(' ')
		<< std::setw(10) << chains.chain_id << " "
		<< std::setw(5) << chains.nseg << " "
		<< std::setw(6) << chains.pos0 << " "
		<< std::setw(6) << chains.pos1 << std::endl;
	for (auto& p : chains.basetracks) {
		std::cout << std::right << std::fixed
			<< std::setw(6) << p.pos << " "
			<< std::setw(20) << p.group_id << " "
			<< std::setw(9) << p.rawid << " "
			<< std::setw(9) << p.ph << " "
			<< std::setw(8) << std::setprecision(4) << p.ax << " "
			<< std::setw(8) << std::setprecision(4) << p.ay << " "
			<< std::setw(10) << std::setprecision(1) << p.x << " "
			<< std::setw(10) << std::setprecision(1) << p.y << " "
			<< std::setw(10) << std::setprecision(0) << p.z << " "
			<< std::setw(2) << p.flg_i[0] << " "
			<< std::setw(2) << p.flg_i[1] << " "
			<< std::setw(2) << p.flg_i[2] << " "
			<< std::setw(2) << p.flg_i[3] << " "
			<< std::setw(5) << std::setprecision(4) << p.flg_d[0] << " "
			<< std::setw(5) << std::setprecision(4) << p.flg_d[1] << " "
			<< std::endl;
	}

}