#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <filesystem>
#include <set>
#include <omp.h>

vxx::base_track_t read_basetrack(std::string file_in_ECC, int pl, int rawid);
std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_filename(std::string file_in_ECC, int pl);
int use_thread(double ratio, bool output);
void connect_base(vxx::base_track_t t_base, std::vector<vxx::base_track_t>  &all, std::vector<vxx::base_track_t>  &connect);
void output_Track_inf(std::string filename, vxx::base_track_t &target, std::vector<vxx::base_track_t>&connect);


int main(int argc, char**argv) {
	if(argc!=5){
		fprintf(stderr, "usage:prg file-in-ECC pl rawid output-file\n");
		exit(1);
	}

	std::string file_in_ECC = argv[1];
	int pl = std::stoi(argv[2]);
	int rawid = std::stoi(argv[3]);
	std::string file_out = argv[4];

	vxx::base_track_t target_base = read_basetrack(file_in_ECC, pl, rawid);

	//alignment paramerter read
//これはポインタで参照されるためとっておく
	std::vector < std::pair<std::pair<int, int>, std::vector <corrmap_3d::align_param >>>all_align_param;
	std::vector<std::pair< std::pair<int, int>, std::vector <corrmap_3d::align_param2 >>>all_align_param2;
	std::vector<std::pair<std::pair<int, int>, std::string> >  files_in_align = Get_alignment_filename(file_in_ECC,pl);

	int count = 0, all = files_in_align.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < files_in_align.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align read %d/%d", count, all);
		}
#pragma omp atomic
		count++;

		std::vector <corrmap_3d::align_param > corr = corrmap_3d::read_ali_param(files_in_align[i].second, false);
#pragma omp critical
		all_align_param.push_back(std::make_pair(files_in_align[i].first, corr));
	}
	printf("\r corrmap align read %d/%d fin\n", count, all);

	count = 0, all = all_align_param.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < all_align_param.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align calc delaunay %d/%d", count, all);
		}
#pragma omp atomic
		count++;

		std::vector <corrmap_3d::align_param2 >corr2 = DelaunayDivide(all_align_param[i].second);
#pragma omp critical
		all_align_param2.push_back(std::make_pair(all_align_param[i].first, corr2));

	}
	printf("\r corrmap align calc delaunay %d/%d fin\n", count, all);

	std::vector<vxx::base_track_t>  connect;
	for (int i = 0; i < all_align_param2.size(); i++) {

		printf("connect PL%03d -PL%03d\n", all_align_param2[i].first.first, all_align_param2[i].first.second);
		int target_pl = all_align_param2[i].first.second;
		std::stringstream file_in_base;
		file_in_base << file_in_ECC << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << target_pl
			<< "\\b" << std::setw(3) << std::setfill('0') << target_pl << ".sel.cor.vxx";
		vxx::BvxxReader br;
		std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), target_pl, 0);

		//trackとdelaunay3角形の対応
		std::vector < std::pair<vxx::base_track_t*, corrmap_3d::align_param2*>> track_param = track_affineparam_correspondence(base, all_align_param2[i].second);
		//basetrackを変換
		corrmap_3d::trans_base_all(track_param);

		connect_base(target_base, base, connect);
	}

	output_Track_inf(file_out, target_base, connect);


}
vxx::base_track_t read_basetrack(std::string file_in_ECC, int pl, int rawid) {

	std::stringstream file_in_base;
	file_in_base << file_in_ECC<<"\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";

	vxx::BvxxReader br;
	std::array<int, 2> index = { rawid,rawid + 1 };//1234<=rawid<=5678であるようなものだけを読む。

	std::vector<vxx::base_track_t >base = br.ReadAll(file_in_base.str(), pl, 0, vxx::opt::index = index);

	vxx::base_track_t ret;
	bool flg = false;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->rawid ==rawid) {
			ret = *itr;
			flg = true;
			break;
		}
	}

	if (!flg) {
		printf("rawid=%d basetrack not found\n", rawid);
		return ret;
	}
	return ret;
}


std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_filename(std::string file_in_ECC,int pl) {
	std::vector<std::pair<std::pair<int, int>, std::string> > ret;
	std::map<std::pair<int, int>, std::string> ret_map;

	std::string file_in_align_path = file_in_ECC + "\\Area0\\0\\align\\fine";
	std::filesystem::directory_iterator iter(file_in_align_path), end;
	std::error_code err;

	std::vector<std::string > file_names;
	for (; iter != end && !err; iter.increment(err)) {
		const std::filesystem::directory_entry entry = *iter;

		file_names.push_back(entry.path().string());
		//printf("%s\n", file_names.back().c_str());
	}
	for (int i = 0; i < file_names.size(); i++) {
		int length = file_names[i].size();
		if (file_names[i].substr(length - 4, 4) != ".txt")continue;
		if (file_names[i].substr(length - 18, 14) != "_interpolation")continue;

		int pl0 = std::stoi(file_names[i].substr(length - 25, 3));
		int pl1 = std::stoi(file_names[i].substr(length - 21, 3));
		if (pl0 != pl)continue;
		ret_map.insert(std::make_pair(std::make_pair(pl0, pl1), file_names[i]));
	}
	for (auto itr = ret_map.begin(); itr != ret_map.end(); itr++) {
		ret.push_back(*itr);
	}
	return ret;
}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}

void connect_base(vxx::base_track_t t_base, std::vector<vxx::base_track_t>  &all, std::vector<vxx::base_track_t>  &connect) {

	matrix_3D::vector_3D pos, dir;
	pos.x = t_base.x;
	pos.y = t_base.y;
	pos.z = 0;

	dir.x = t_base.ax;
	dir.y = t_base.ay;
	dir.z = 1;

	double md,oa, extra[2], allowance, z_range[2] = {0,0};
	matrix_3D::vector_3D pos_all, dir_all;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		if (itr->pl == t_base.pl&&itr->rawid == t_base.rawid)continue;
		pos_all.x = itr->x;
		pos_all.y = itr->y;
		pos_all.z = itr->z;
		dir_all.x = itr->ax;
		dir_all.y = itr->ay;
		dir_all.z = 1;
		z_range[1] = itr->z;
		double ex_x = t_base.x + t_base.ax*(itr->z - pos.z);
		double ex_y = t_base.y + t_base.ay*(itr->z - pos.z);
		if (fabs(dir_all.x) > 4.0)continue;
		if (fabs(dir_all.y) > 4.0)continue;
		md = matrix_3D::minimum_distance(pos, pos_all, dir, dir_all, z_range, extra);
		oa = matrix_3D::opening_angle(dir, dir_all);

		if (oa > 0.3)continue;
		//if (md > 200)continue;
		if (fabs(ex_x - itr->x) > 1000)continue;
		if (fabs(ex_y - itr->y) > 1000)continue;
		connect.push_back(*itr);
	}


	return;
}

void output_Track_inf(std::string filename, vxx::base_track_t &target,std::vector<vxx::base_track_t>&connect) {
	std::ofstream ofs(filename);
	int t_pl = target.pl;
	ofs << std::fixed << std::right
		<< std::setw(3) << std::setprecision(0) << target.pl- t_pl << " "
		<< std::setw(3) << std::setprecision(0) << target.pl << " "
		<< std::setw(10) << std::setprecision(0) << target.rawid << " "
		<< std::setw(7) << std::setprecision(0) << target.m[0].ph+ target.m[1].ph << " "
		<< std::setw(8) << std::setprecision(4) << target.ax << " "
		<< std::setw(8) << std::setprecision(4) << target.ay << " "
		<< std::setw(10) << std::setprecision(1) << target.x << " "
		<< std::setw(10) << std::setprecision(1) << target.y << " "
		<< std::setw(10) << std::setprecision(1) << target.z << std::endl;

	for (auto itr = connect.begin(); itr != connect.end(); itr++) {
		ofs << std::fixed << std::right
			<< std::setw(3) << std::setprecision(0) << itr->pl - t_pl << " "
			<< std::setw(3) << std::setprecision(0) << itr->pl << " "
			<< std::setw(10) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(7) << std::setprecision(0) << itr->m[0].ph + itr->m[1].ph << " "
			<< std::setw(8) << std::setprecision(4) << itr->ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(10) << std::setprecision(1) << itr->z << std::endl;
	}
}
