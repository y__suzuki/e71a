#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include<vector>

uint64_t GetFileSize(std::string filename);
void read_write_mfile(std::string in_file, std::string out_file);
void read_write_cout(std::string file_in_mfile);

void header_out(std::string file_in_mfile);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg file-in-mfile file-out-mfile memory\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];
	int memory = std::stoi(argv[3]);
	uint64_t filesize = GetFileSize(file_in_mfile);
	int filesize_gb = filesize / 1000 / 1000 / 1000;
	printf("mfile size: %d[GB] memory:%d[GB]\n", filesize_gb, memory);
	if (memory < filesize_gb) {
		read_write_mfile(file_in_mfile, file_out_mfile);
	}
	else {
		mfile1::MFile m;
		mfile1::read_mfile_txt(file_in_mfile, m);
		mfile1::write_mfile(file_out_mfile, m);
	}

}
uint64_t GetFileSize(std::string filename) {
	std::ifstream ifs(filename);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2;
}

void read_write_mfile(std::string in_file, std::string out_file) {
	std::ofstream ofs(out_file, std::ios::binary);
	//filesize取得
	mfile1::MFile mfile;
	std::string  filetype = "mfile-a0";
	mfile.header.filetype = 0;
	memcpy((char*)& mfile.header.filetype, filetype.data(), filetype.size());

	mfile.info_header.classsize1 = sizeof(mfile1::MFileChain);
	mfile.info_header.classsize2 = sizeof(mfile1::MFileBase);

	mfile.info_header.Nbasetrack = 0;
	mfile.info_header.Nchain = 0;

	ofs.write((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	ofs.write((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));



	std::ifstream ifs(in_file);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}

	std::string str;

	mfile0::M_Header head_tmp;
	for (int i = 0; i < 3; i++) {
		std::getline(ifs, str);
		head_tmp.head[i] = str;
	}
	std::getline(ifs, str);
	head_tmp.num_all_plate = stoi(str);
	{
		std::getline(ifs, str);
		auto str_v = StringSplit(str);
		for (int i = 0; i < str_v.size(); i++) {
			head_tmp.all_pos.push_back(stoi(str_v[i]));
		}
	}

	int cnt = 0;
	mfile1::MFileChain chain;
	mfile1::MFileBase base;
	while (std::getline(ifs, str)) {
		if (cnt % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now mfile convert ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		cnt++;

		auto strs = StringSplit(str);
		if (strs.size() == 4) {
			chain.chain_id = stoll(strs[0]);
			chain.nseg = stoi(strs[1]);
			chain.pos0 = stoi(strs[2]);
			chain.pos1 = stoi(strs[3]);
			ofs.write((char*)&chain, sizeof(mfile1::MFileChain));
			mfile.info_header.Nchain++;
			for (int i = 0; i < chain.nseg; i++) {

				std::getline(ifs, str);
				strs = StringSplit(str);
				if (strs.size() == 15) {
					base.pos = stoi(strs[0]);
					base.group_id = stoi(strs[1]);
					base.rawid = stoll(strs[2]);
					base.ph = stoi(strs[3]);
					base.ax = stof(strs[4]);
					base.ay = stof(strs[5]);
					base.x = stod(strs[6]);
					base.y = stod(strs[7]);
					base.z = stod(strs[8]);
					ofs.write((char*)&base, sizeof(mfile1::MFileBase));
					mfile.info_header.Nbasetrack++;

				}
				else {
					fprintf(stderr, "mfile format err! cannot read file\n");
					fprintf(stderr, "base track information 15 -->%d\n", int(strs.size()));
					for (int i = 0; i < strs.size(); i++) {
						printf("%s ", strs[i].c_str());
					}
					printf("\n");
					throw std::exception();
				}
			}
		}
		else {
			fprintf(stderr, "mfile format err! cannot read file\n");
			fprintf(stderr, "chain information 4 -->%d\n", int(strs.size()));
			for (int i = 0; i < strs.size(); i++) {
				printf("%s ", strs[i].c_str());
			}
			printf("\n");

			throw std::exception();
		}
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now mfile convert ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	
	//file pointerを最初に戻してヘッダを書く
	ofs.clear();
	ofs.seekp(0, std::ios::beg);

	ofs.write((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	ofs.write((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));

}
void read_write_cout(std::string file_in_mfile) {

	std::ifstream ifs(file_in_mfile, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }

	std::cout << mfile.header.filetype << std::endl;

	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }
	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	std::cout << mfile.info_header.classsize1 << " "
		<< mfile.info_header.classsize2 << " "
		<< mfile.info_header.Nbasetrack << " "
		<< mfile.info_header.Nchain << std::endl;

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }

	for (uint32_t c = 0; c < mfile.info_header.Nchain; c++) {
		mfile1::MFileChain1 chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }
		std::cout << chain.chain_id << " " << chain.nseg << " " << chain.pos0 << " " << chain.pos1 << std::endl;

		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase1 base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			std::cout << base.pos << " "
				<< base.group_id << " "
				<< base.rawid << " "
				<< base.ph << " "
				<< base.ax << " "
				<< base.ay << " "
				<< base.x << " "
				<< base.y << " "
				<< base.z << std::endl;

		}
	}
}

void header_out(std::string file_in_mfile) {

	std::cout << "header size " << sizeof(mfile1::MFileHeader) << std::endl;
	std::cout << "header inf size " << sizeof(mfile1::MFileInfoHeader) << std::endl;

	std::ifstream ifs(file_in_mfile, std::ios::binary);




	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }

	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }
	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	std::cout << mfile.header.filetype << " "
		<< mfile.header.filesize << " "
		<< mfile.header.reserved << " "
		<< mfile.header.offset1 << " "
		<< mfile.header.offset2 << std::endl;
	for (int i = 0; i < 64; i++) {
		std::cout << mfile.header.name[i] << " ";
	}
	std::cout << std::endl;
	for (int i = 0; i < 256; i++) {
		std::cout << mfile.header.object[i] << " ";
	}
	std::cout << std::endl;

	std::cout << mfile.info_header.classsize1 << " "
		<< mfile.info_header.classsize2 << " "
		<< mfile.info_header.reserved1 << " "
		<< mfile.info_header.Nbasetrack << " "
		<< mfile.info_header.Nchain << " "
		<< mfile.info_header.reserved2 << std::endl;
}