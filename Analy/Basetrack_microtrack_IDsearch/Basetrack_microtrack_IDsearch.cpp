#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>

std::vector<vxx::micro_track_t> micro_id_search(std::vector<vxx::base_track_t> &base, std::map<int, vxx::micro_track_t *> &micro_map);

int main(int argc, char **argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:prg bvxx pl fvxx pos zone out-fvxx\n");
		exit(1);
	}

	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_fvxx = argv[3];
	int pos = std::stoi(argv[4]);
	int zone = std::stoi(argv[5]);
	std::string file_out_fvxx = argv[6];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);

	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> micro = fr.ReadAll(file_in_fvxx, pos, zone);

	std::map<int, vxx::micro_track_t *> micro_map;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		micro_map.insert(std::make_pair(itr->rawid, &(*itr)));
	}

	std::vector<vxx::micro_track_t> out = micro_id_search(base, micro_map);
	printf("output microtrack num=%d\n", out.size());
	vxx::FvxxWriter fw;
	fw.Write(file_out_fvxx, pos, zone, out);



}
std::vector<vxx::micro_track_t> micro_id_search(std::vector<vxx::base_track_t> &base, std::map<int, vxx::micro_track_t *> &micro_map) {
	std::vector<vxx::micro_track_t> ret;
	std::map<int, vxx::micro_track_t*> ret_map;
	int pos_id;
	if (base.begin()->m[0].pos == micro_map.begin()->second->pos) {
		pos_id = 0;
	}
	else if(base.begin()->m[1].pos == micro_map.begin()->second->pos){
		pos_id = 1;
	}
	else {
		fprintf(stderr, "pos not match\n");
		exit(1);
	}

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		auto res = micro_map.find(itr->m[pos_id].rawid);
		if (res == micro_map.end()) {
			fprintf(stderr, "id =%d not found\n", itr->rawid);
		}
		else {
			ret_map.insert(*res);
		}
	}
	for (auto itr = ret_map.begin(); itr != ret_map.end(); itr++) {
		ret.push_back(*itr->second);
	}
	return ret;

}
