#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <omp.h>
struct Efficiency {
	int pl, ph, bin, zfilter, all, hit;
	double eff, eff_err, all_pos_r, all_pos_l, all_ang_r, all_ang_l, ang_min, ang_max;
};

void Basetrack_matching(std::vector<netscan::base_track_t> base_pred, std::multimap<std::pair<int, int>, netscan::base_track_t *> &base_map, double x_min, double y_min, double hash, std::vector<netscan::linklet_t> &link, std::vector<Efficiency> &eff, double ang_min, double ang_max, double all_pos_r, double all_pos_l, double all_ang_r, double all_ang_l);
void Basetrack_matching_BG(std::vector<netscan::base_track_t> base_pred, std::multimap<std::pair<int, int>, netscan::base_track_t *> &base_map, double x_min, double y_min, double hash, std::vector<netscan::linklet_t> &link, std::vector<Efficiency> &eff, double ang_min, double ang_max, double all_pos_r, double all_pos_l, double all_ang_r, double all_ang_l);
		std::vector<netscan::base_track_t> Base_Area_Cut(std::vector<netscan::base_track_t> base);
void output_eff(std::string filename, std::vector<Efficiency> eff);

int main(int argc, char **argv) {
	if (argc !=9) {
		fprintf(stderr, "usage:prg in-bvxx(prediction) in-bvxx pl out-txt(sig) out-txt(bg) ph bin zfilter\n");
		exit(1);
	}
	std::string file_in_base_pred = argv[1];
	std::string file_in_base=argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_out_txt_sig = argv[4];
	std::string file_out_txt_bg = argv[5];
	int ph = std::stoi(argv[6]);
	int bin = std::stoi(argv[7]);
	int zfilter = std::stoi(argv[8]);

	std::vector<netscan::base_track_t> base_pred;
	std::vector<netscan::base_track_t> base;

	netscan::read_basetrack_extension(file_in_base_pred, base_pred, pl, 0);
	netscan::read_basetrack_extension(file_in_base, base, pl, 0);


	double hash = 2000;
	int ix, iy;
	double x_min, y_min;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(x_min, itr->x);
		y_min = std::min(y_min, itr->y);
	}

	std::multimap<std::pair<int, int>, netscan::base_track_t *> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ix = (itr->x - x_min) / hash;
		iy = (itr->y - y_min) / hash;
		base_map.insert(std::make_pair(std::make_pair(ix, iy), &(*itr)));
	}

	base_pred = Base_Area_Cut(base_pred);

	std::vector<Efficiency> eff,eff_bg;
	std::vector<netscan::linklet_t> link;
	double all_ang_r = 0.1;
	double all_ang_l = 0.1;
	double all_pos_r = 30;
	double all_pos_l = 30;

	for (int i = 0; i < 15; i++) {
		for (all_pos_l = 1; all_pos_l <= 30.1; all_pos_l += 1) {
			Basetrack_matching(base_pred, base_map, x_min, y_min, hash, link, eff, i*0.1, (i + 1)*0.1, all_pos_r, all_pos_l, all_ang_r, all_ang_l);
			Basetrack_matching_BG(base_pred, base_map, x_min, y_min, hash, link, eff_bg, i*0.1, (i + 1)*0.1, all_pos_r, all_pos_l, all_ang_r, all_ang_l);
		}
		all_pos_l = 30;
		for (all_pos_r = 1; all_pos_r <= 30.1; all_pos_r += 1) {
			Basetrack_matching(base_pred, base_map, x_min, y_min, hash, link, eff, i*0.1, (i + 1)*0.1, all_pos_r, all_pos_l, all_ang_r, all_ang_l);
			Basetrack_matching_BG(base_pred, base_map, x_min, y_min, hash, link, eff_bg, i*0.1, (i + 1)*0.1, all_pos_r, all_pos_l, all_ang_r, all_ang_l);
		}
		all_pos_r = 30;
		for (all_ang_l = 0.005; all_ang_l <= 0.1000001; all_ang_l += 0.005) {
			Basetrack_matching(base_pred, base_map, x_min, y_min, hash, link, eff, i*0.1, (i + 1)*0.1, all_pos_r, all_pos_l, all_ang_r, all_ang_l);
			Basetrack_matching_BG(base_pred, base_map, x_min, y_min, hash, link, eff_bg, i*0.1, (i + 1)*0.1, all_pos_r, all_pos_l, all_ang_r, all_ang_l);
		}
		all_ang_l = 0.1;
		for (all_ang_r = 0.005; all_ang_r <= 0.1000001; all_ang_r += 0.005) {
			Basetrack_matching(base_pred, base_map, x_min, y_min, hash, link, eff, i*0.1, (i + 1)*0.1, all_pos_r, all_pos_l, all_ang_r, all_ang_l);
			Basetrack_matching_BG(base_pred, base_map, x_min, y_min, hash, link, eff_bg, i*0.1, (i + 1)*0.1, all_pos_r, all_pos_l, all_ang_r, all_ang_l);
		}
		all_ang_r = 0.1;
	}

	for (auto itr = eff.begin(); itr != eff.end(); itr++) {
		itr->pl = pl;
		itr->bin = bin;
		itr->ph = ph;
		itr->zfilter = zfilter;
	}
	for (auto itr = eff_bg.begin(); itr != eff_bg.end(); itr++) {
		itr->pl = pl;
		itr->bin = bin;
		itr->ph = ph;
		itr->zfilter = zfilter;
	}
	//netscan::write_linklet_txt(file_out_link, link);
	output_eff(file_out_txt_sig, eff);
	output_eff(file_out_txt_bg, eff_bg);
}
std::vector<netscan::base_track_t> Base_Area_Cut(std::vector<netscan::base_track_t> base) {
	std::vector<netscan::base_track_t> ret;
	double area[4];
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			area[0] = itr->x;
			area[1] = itr->x;
			area[2] = itr->y;
			area[3] = itr->y;
		}
		area[0] = std::min(area[0], itr->x);
		area[1] = std::max(area[1], itr->x);
		area[2] = std::min(area[2], itr->y);
		area[3] = std::max(area[3], itr->y);
	}
	double cut = 5000;
	area[0] += cut;
	area[1] -= cut;
	area[2] += cut;
	area[3] -= cut;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->x < area[0])continue;
		if (itr->x > area[1])continue;
		if (itr->y < area[2])continue;
		if (itr->y > area[3])continue;
		ret.push_back(*itr);
	}
	fprintf(stderr, "Area cut %d --> %d (%4.1lf%%)\n", base.size(), ret.size(), ret.size()*100. / base.size());
	return ret;
}
void Basetrack_matching(std::vector<netscan::base_track_t> base_pred, std::multimap<std::pair<int, int>, netscan::base_track_t *> &base_map, double x_min, double y_min, double hash, std::vector<netscan::linklet_t> &link, std::vector<Efficiency> &eff, double ang_min, double ang_max, double all_pos_r,double all_pos_l, double all_ang_r,double all_ang_l) {
	Efficiency eff_tmp;
	eff_tmp.all_ang_r = all_ang_r;
	eff_tmp.all_ang_l = all_ang_l;
	eff_tmp.all_pos_r = all_pos_r;
	eff_tmp.all_pos_l = all_pos_l;
	eff_tmp.ang_max = ang_max;
	eff_tmp.ang_min = ang_min;
	eff_tmp.all = 0;
	eff_tmp.hit = 0;

	//int count = 0;

#pragma omp parallel for
	for (int i = 0; i < base_pred.size(); i++) {
		double diff_pos[2], diff_ang[2];
		auto itr = &(base_pred[i]);
		bool hit_flg = false;
		double angle;
		int ix, iy;
		int flg;
		std::pair<int, int> id;

		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angle < ang_min)continue;
		if (angle >= ang_max)continue;
#pragma omp atomic
		eff_tmp.all++;

		ix = (itr->x - x_min) / hash;
		iy = (itr->y - y_min) / hash;
		for (int iix = -1; iix <= 1; iix++) {
			if (hit_flg)break;
			for (int iiy = -1; iiy <= 1; iiy++) {
				if (hit_flg)break;
				id.first = ix + iix;
				id.second = iy + iiy;
				flg = base_map.count(id);
				if (flg == 1) {
					auto res = base_map.find(id);
					diff_ang[0] = ((res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay) / angle;
					diff_ang[1] = ((res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax) / angle;
					diff_pos[0] = ((res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay) / angle;
					diff_pos[1] = ((res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax) / angle;

					if (fabs(diff_ang[0]) > all_ang_r)continue;
					if (fabs(diff_ang[1]) > all_ang_l)continue;
					if (fabs(diff_pos[0]) > all_pos_r)continue;
					if (fabs(diff_pos[1]) > all_pos_l)continue;
					if (fabs(diff_ang[0]) > all_ang_r)continue;
					hit_flg = true;
				}
				else if (flg > 1) {
					auto range = base_map.equal_range(id);
					for (auto itr2 = range.first; itr2 != range.second; itr2++) {
						auto res = itr2;
						diff_ang[0] = ((res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay) / angle;
						diff_ang[1] = ((res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax) / angle;
						diff_pos[0] = ((res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay) / angle;
						diff_pos[1] = ((res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax) / angle;

						if (fabs(diff_ang[0]) > all_ang_r)continue;
						if (fabs(diff_ang[1]) > all_ang_l)continue;
						if (fabs(diff_pos[0]) > all_pos_r)continue;
						if (fabs(diff_pos[1]) > all_pos_l)continue;
						hit_flg = true;
						break;
					}
				}
			}
		}
		if (hit_flg) {
#pragma omp atomic
			eff_tmp.hit++;
		}
	}
	fprintf(stderr, "angle %4.3lf [angle radial:%4.3lf lateral:%4.3lf] [position radial:%4.1lf radial:%4.1lf]\n", (ang_min + ang_max) / 2, all_ang_r, all_ang_l, all_pos_r, all_pos_l);

	//ここまででmtaching終了
	if (eff_tmp.all == 0) {
		eff_tmp.eff = 0;
		eff_tmp.eff_err = 0;
	}
	else {
		eff_tmp.eff = eff_tmp.hit*1.0 / eff_tmp.all;
		eff_tmp.eff_err = sqrt(eff_tmp.all*eff_tmp.eff*(1 - eff_tmp.eff)) / eff_tmp.all;
	}
	eff.push_back(eff_tmp);
}
void Basetrack_matching_BG(std::vector<netscan::base_track_t> base_pred, std::multimap<std::pair<int, int>, netscan::base_track_t *> &base_map, double x_min, double y_min, double hash, std::vector<netscan::linklet_t> &link, std::vector<Efficiency> &eff, double ang_min, double ang_max, double all_pos_r, double all_pos_l, double all_ang_r, double all_ang_l) {
	Efficiency eff_tmp;
	eff_tmp.all_ang_r = all_ang_r;
	eff_tmp.all_ang_l = all_ang_l;
	eff_tmp.all_pos_r = all_pos_r;
	eff_tmp.all_pos_l = all_pos_l;
	eff_tmp.ang_max = ang_max;
	eff_tmp.ang_min = ang_min;
	eff_tmp.all = 0;
	eff_tmp.hit = 0;

	//int count = 0;
	for (auto itr = base_pred.begin(); itr != base_pred.end(); itr++) {
		//位置をシフト
		itr->x += 2000;
		itr->y += 2000;
	}

#pragma omp parallel for
	for (int i = 0; i < base_pred.size(); i++) {
		double diff_pos[2], diff_ang[2];
		auto itr = &(base_pred[i]);
		bool hit_flg = false;
		double angle;
		int ix, iy;
		int flg;
		std::pair<int, int> id;

		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angle < ang_min)continue;
		if (angle >= ang_max)continue;
#pragma omp atomic
		eff_tmp.all++;

		ix = (itr->x - x_min) / hash;
		iy = (itr->y - y_min) / hash;
		for (int iix = -1; iix <= 1; iix++) {
			if (hit_flg)break;
			for (int iiy = -1; iiy <= 1; iiy++) {
				if (hit_flg)break;
				id.first = ix + iix;
				id.second = iy + iiy;
				flg = base_map.count(id);
				if (flg == 1) {
					auto res = base_map.find(id);
					diff_ang[0] = ((res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay) / angle;
					diff_ang[1] = ((res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax) / angle;
					diff_pos[0] = ((res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay) / angle;
					diff_pos[1] = ((res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax) / angle;

					if (fabs(diff_ang[0]) > all_ang_r)continue;
					if (fabs(diff_ang[1]) > all_ang_l)continue;
					if (fabs(diff_pos[0]) > all_pos_r)continue;
					if (fabs(diff_pos[1]) > all_pos_l)continue;
					hit_flg = true;
				}
				else if (flg > 1) {
					auto range = base_map.equal_range(id);
					for (auto itr2 = range.first; itr2 != range.second; itr2++) {
						auto res = itr2;
						diff_ang[0] = ((res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay) / angle;
						diff_ang[1] = ((res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax) / angle;
						diff_pos[0] = ((res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay) / angle;
						diff_pos[1] = ((res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax) / angle;

						if (fabs(diff_ang[0]) > all_ang_r)continue;
						if (fabs(diff_ang[1]) > all_ang_l)continue;
						if (fabs(diff_pos[0]) > all_pos_r)continue;
						if (fabs(diff_pos[1]) > all_pos_l)continue;
						hit_flg = true;
						break;
					}
				}
			}
		}
		if (hit_flg) {
#pragma omp atomic
			eff_tmp.hit++;
		}
	}
	fprintf(stderr, "angle %4.3lf [angle radial:%4.3lf lateral:%4.3lf] [position radial:%4.1lf radial:%4.1lf]\n", (ang_min + ang_max) / 2, all_ang_r, all_ang_l, all_pos_r, all_pos_l);
	//ここまででmtaching終了
	if (eff_tmp.all == 0) {
		eff_tmp.eff = 0;
		eff_tmp.eff_err = 0;
	}
	else {
		eff_tmp.eff = eff_tmp.hit*1.0 / eff_tmp.all;
		eff_tmp.eff_err = sqrt(eff_tmp.all*eff_tmp.eff*(1 - eff_tmp.eff)) / eff_tmp.all;
	}
	eff.push_back(eff_tmp);
}
void output_eff(std::string filename, std::vector<Efficiency> eff) {
	std::ofstream ofs(filename);
	for (auto itr = eff.begin(); itr != eff.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << itr->pl << " "
			<< std::setw(3) << std::setprecision(0) << itr->ph << " "
			<< std::setw(3) << std::setprecision(0) << itr->bin << " "
			<< std::setw(3) << std::setprecision(0) << itr->zfilter << " "
			<< std::setw(3) << std::setprecision(1) << itr->ang_min << " "
			<< std::setw(3) << std::setprecision(1) << itr->ang_max << " "
			<< std::setw(5) << std::setprecision(4) << itr->all_ang_r << " "
			<< std::setw(5) << std::setprecision(4) << itr->all_ang_l << " "
			<< std::setw(5) << std::setprecision(1) << itr->all_pos_r << " "
			<< std::setw(5) << std::setprecision(1) << itr->all_pos_l << " "
			<< std::setw(10) << std::setprecision(0) << itr->hit << " "
			<< std::setw(10) << std::setprecision(0) << itr->all << " "
			<< std::setw(5) << std::setprecision(4) << itr->eff << " "
			<< std::setw(5) << std::setprecision(4) << itr->eff_err << std::endl;
	}
}

