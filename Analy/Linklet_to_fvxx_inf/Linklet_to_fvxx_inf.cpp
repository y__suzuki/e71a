#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "VxxReader.lib")

#include <VxxReader.h>
# include <iostream>
# include <random>
#include <set>

std::vector<vxx::micro_track_t> search_microtrack(std::vector<vxx::micro_track_t> micro, std::set<std::tuple<int, int, int>>id);
void out_micro_rawid(std::vector<netscan::linklet_t> link, int face, std::set<std::tuple<int, int, int>>&raw1, std::set<std::tuple<int, int, int>>&raw2);
void read_microtrack(std::string file_path, std::vector<vxx::micro_track_t> &micro0, std::vector<vxx::micro_track_t> &micro1, std::vector<netscan::linklet_t> link, int id, int pl);


int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "prg in-linklet(bin) out-side in-fvxx-path pl out-fvxx-x1 out-fvxx-x2\n");
		exit(1);
	}
	std::string file_in_link = argv[1];
	int face = std::stoi(argv[2]);
	std::string file_in_fvxx_path = argv[3];
	int pl = std::stoi(argv[4]);
	std::string file_out_fvxx1 = argv[5];
	std::string file_out_fvxx2 = argv[6];

	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_bin(file_in_link, link);


	std::vector<vxx::micro_track_t> micro0, micro1;
	read_microtrack(file_in_fvxx_path, micro0, micro1, link, face, pl);

	vxx::FvxxWriter fw;
	fw.Write(file_out_fvxx1, pl * 10 + 1, 0, micro0);
	fw.Write(file_out_fvxx2, pl * 10 + 2, 0, micro1);
}
void out_micro_rawid(std::vector<netscan::linklet_t> link, int face, std::set<std::tuple<int, int, int>>&raw1, std::set<std::tuple<int,int,int>>&raw2) {
	if (face != 0 && face != 1) {
		fprintf(stderr, "face exception\n");
		fprintf(stderr, "0 or 1\n");
		exit(1);
	}
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		raw1.insert(std::make_tuple(itr->b[face].m[0].col, itr->b[face].m[0].row, itr->b[face].m[0].isg));
		raw2.insert(std::make_tuple(itr->b[face].m[1].col, itr->b[face].m[1].row, itr->b[face].m[1].isg));
	}
	return;
}
std::vector<vxx::micro_track_t> search_microtrack(std::vector<vxx::micro_track_t> micro, std::set<std::tuple<int, int, int>>id) {
	std::map<std::tuple<int, int, int>, vxx::micro_track_t*> micro_map;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		micro_map.insert(std::make_pair(std::make_tuple(itr->col, itr->row, itr->isg), &(*itr)));
	}
	std::vector<vxx::micro_track_t>ret;
	for (auto itr = id.begin(); itr != id.end(); itr++) {
		auto res = micro_map.find(*itr);
		if (res == micro_map.end()) {
			fprintf(stderr, "microtrack not found\n");
			fprintf(stderr, "col=%d, row=%d, isg=%d\n", std::get<0>(*itr), std::get<1>(*itr), std::get<2>(*itr));
			continue;
		}
		ret.push_back(*res->second);
	}
	return ret;
}
void read_microtrack(std::string file_path, std::vector<vxx::micro_track_t> &micro0, std::vector<vxx::micro_track_t> &micro1, std::vector<netscan::linklet_t> link,int id,int pl) {
	if (id != 0 && id != 1) {
		fprintf(stderr, "id exception\n");
		fprintf(stderr, "0 or 1\n");
		exit(1);
	}

	std::map<int, int> micro_id0,micro_id1;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		micro_id0.insert(std::make_pair(itr->b[id].m[0].zone, itr->b[id].m[0].rawid));
		micro_id1.insert(std::make_pair(itr->b[id].m[1].zone, itr->b[id].m[1].rawid));
	}
	
	std::string file_name;
	vxx::FvxxReader fr;
	for (auto itr = micro_id0.begin(); itr != micro_id0.end(); ) {
		auto range = micro_id0.equal_range(itr->first);
		if ((itr->first - 1) / 6 == 0) file_name = "_thick_0.vxx";
		else if ((itr->first - 1) / 6 == 1)file_name = "_thick_0.vxx";
		else if ((itr->first - 1) / 6 == 2)file_name = "_thick_1.vxx";
		else if ((itr->first - 1) / 6 == 3)file_name = "_thick_1.vxx";
		else if ((itr->first - 1) / 6 == 4)file_name = "_thin_0.vxx";
		else if ((itr->first - 1) / 6 == 5)file_name = "_thin_0.vxx";
		else if ((itr->first - 1) / 6 == 6)file_name = "_thin_1.vxx";
		else if ((itr->first - 1) / 6 == 7)file_name = "_thin_1.vxx";
		else {
			fprintf(stderr, "exception fvxx zone err\n");
			exit(1);
		}

		std::stringstream file_in_fvxx;
		file_in_fvxx << file_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 1 << file_name;
		//std::cout << file_in_fvxx.str() << std::endl;
		for (auto res = range.first; res != range.second; res++) {
			std::array<int, 2> index = { res->second, res->second+1 };//1234<=rawid<=5678であるようなものだけを読む。
			if (fr.Begin(file_in_fvxx.str(), pl * 10 + 1, 0, vxx::opt::index = index)) {
				vxx::HashEntry h;
				vxx::micro_track_t m;
				while (fr.NextHashEntry(h))
				{
					while (fr.NextMicroTrack(m))
					{
						micro0.push_back(m);
					}
				}
				fr.End();
			}
			itr++;
		}
	}
	for (auto itr = micro_id1.begin(); itr != micro_id1.end(); ) {
		auto range = micro_id1.equal_range(itr->first);
		if ((itr->first - 1) / 6 == 0) file_name = "_thick_0.vxx";
		else if ((itr->first - 1) / 6 == 1)file_name = "_thick_1.vxx";
		else if ((itr->first - 1) / 6 == 2)file_name = "_thick_0.vxx";
		else if ((itr->first - 1) / 6 == 3)file_name = "_thick_1.vxx";
		else if ((itr->first - 1) / 6 == 4)file_name = "_thin_0.vxx";
		else if ((itr->first - 1) / 6 == 5)file_name = "_thin_1.vxx";
		else if ((itr->first - 1) / 6 == 6)file_name = "_thin_0.vxx";
		else if ((itr->first - 1) / 6 == 7)file_name = "_thin_1.vxx";
		else {
			fprintf(stderr, "exception fvxx zone err\n");
			exit(1);
		}

		std::stringstream file_in_fvxx;
		file_in_fvxx << file_path << "\\f" << std::setw(4) << std::setfill('0') << pl * 10 + 2 << file_name;
		//std::cout << file_in_fvxx.str() << std::endl;

		for (auto res = range.first; res != range.second; res++) {
			std::array<int, 2> index = { res->second, res->second+1 };//1234<=rawid<=5678であるようなものだけを読む。
			if (fr.Begin(file_in_fvxx.str(), pl * 10 + 2, 0, vxx::opt::index = index)) {
				vxx::HashEntry h;
				vxx::micro_track_t m;
				while (fr.NextHashEntry(h))
				{
					while (fr.NextMicroTrack(m))
					{
						micro1.push_back(m);
					}
				}
				fr.End();
			}
			itr++;
		}
	}
	return;

}