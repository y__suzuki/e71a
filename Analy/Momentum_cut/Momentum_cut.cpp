#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Event_information> mom_Cut_up(std::vector<Momentum_recon::Event_information>&momch, double cut_val);
	std::vector<Momentum_recon::Event_information> mom_Cut_down(std::vector<Momentum_recon::Event_information>&momch, double cut_val);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-momch mom_cut file-out-momch\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	double mom_cut = std::stod(argv[2]);
	std::string file_out_momch = argv[3];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	//std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	momch = mom_Cut_down(momch, mom_cut);
	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);
	exit(0);

}
std::vector<Momentum_recon::Event_information> mom_Cut_up(std::vector<Momentum_recon::Event_information>&momch, double cut_val) {
	std::vector<Momentum_recon::Event_information> ret;
	for (auto &ev : momch) {
		Momentum_recon::Event_information ev_tmp;
		ev_tmp = ev;
		ev_tmp.chains.clear();
		for (auto &c : ev.chains) {
			if (!isfinite(c.ecc_mcs_mom[0]))continue;
			if (c.ecc_mcs_mom[0] < 0)continue;
			if (cut_val > c.ecc_mcs_mom[0])continue;
			ev_tmp.chains.push_back(c);
		}
		if (ev_tmp.chains.size() > 0) {
			ret.push_back(ev_tmp);
		}
	}
	return ret;
}
std::vector<Momentum_recon::Event_information> mom_Cut_down(std::vector<Momentum_recon::Event_information>&momch, double cut_val) {
	std::vector<Momentum_recon::Event_information> ret;
	for (auto &ev : momch) {
		Momentum_recon::Event_information ev_tmp;
		ev_tmp = ev;
		ev_tmp.chains.clear();
		for (auto &c : ev.chains) {
			if (!isfinite(c.ecc_mcs_mom[0]))continue;
			if (c.ecc_mcs_mom[0] < 0)continue;
			if (cut_val< c.ecc_mcs_mom[0])continue;
			ev_tmp.chains.push_back(c);
		}
		if (ev_tmp.chains.size() > 0) {
			ret.push_back(ev_tmp);
		}
	}
	return ret;
}