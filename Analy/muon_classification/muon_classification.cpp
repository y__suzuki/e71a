#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

class Fiducial_Area {
public:
	int pl;
	double x0, y0, z0, x1, y1, z1;
};

std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename);
void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map);
std::vector<std::vector<mfile0::M_Chain>> group_divide(mfile0::Mfile&m);
std::vector<mfile0::M_Chain> divide_single_chain(std::vector<std::vector<mfile0::M_Chain>>&group);
std::map<std::pair<int, int>, mfile0::M_Base> divide_base(std::vector< mfile0::M_Chain>&chain);
bool unique_base_to_chain(std::map<std::pair<int, int>, mfile0::M_Base>&base_all, mfile0::M_Chain&c);
bool merge_chains(std::map<std::pair<int, int>, mfile0::M_Base>&base_all, mfile0::M_Chain&c);
mfile0::M_Chain select_best_chain(std::vector<mfile0::M_Chain> &group);
std::vector<mfile0::M_Chain> divide_penetrate(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&penetrate, int veto_pl);
std::vector<mfile0::M_Chain> divide_IronECC(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&iron, int veto_pl);
std::vector<mfile0::M_Chain> divide_edge_out(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&edge_out, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map, double edge_cut, int ex_pl_max);
bool judge_fiducial_area(std::vector<Fiducial_Area>&area, double x, double y);
void output_mfile(std::string filename, std::vector<mfile0::M_Chain>&chain, mfile0::M_Header &header);
void output_upstream_track(std::ofstream &ofs, std::vector<mfile0::M_Chain>&chain);

bool sort_M_base(const mfile0::M_Base &left, const mfile0::M_Base &right) {
	return left.pos < right.pos;
}

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage: prg file-in-mfile(txt) file-in-ECC fa.txt output-path\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_in_area = argv[3];
	std::string file_out_path = argv[4];

	//corrmap absの読み込み
	std::string file_in_corrabs = file_in_ECC + "\\0\\align\\corrmap-abs.lst";
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrabs, corr_abs);
	//fiducial areaの読み込み
	std::map<int, std::vector<Fiducial_Area>> area = read_fiducial_Area(file_in_area);
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);

	//corrmap absの適用
	trans_mfile_cordinate(corr_abs, area, z_map);


	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	printf("chain size=%d\n", m.chains.size());

	//penetrate check
	std::vector<mfile0::M_Chain> penetrate;
	//最上流がPL132以上
	m.chains = divide_penetrate(m.chains, penetrate, 132);

	//edgeout check
	std::vector<mfile0::M_Chain> edge_out;
	//最上流から4PL外挿,edgeから5mm以内に入ったらedge out
	m.chains = divide_edge_out(m.chains, edge_out, area, z_map, 0, 4);

	////Iron ECC event
	//std::vector<mfile0::M_Chain> iron_ecc;
	//single_chain_v = divide_IronECC(single_chain_v, iron_ecc, 15);


	printf("stop = %d\n", m.chains.size());

	std::string file_out_penetrate = file_out_path + "\\muon_penetrate.all";
	std::string file_out_edgeout = file_out_path + "\\muon_edgeouot.all";
	std::string file_out_stop = file_out_path + "\\muon_stop.all";
	std::string file_up_inf = file_out_path + "\\muon_stop.txt";

	std::ofstream ofs_log(file_out_path + "\\classification_log.txt");
	ofs_log << std::right << std::fixed
		<< "penetarte : " << std::setw(5) << std::setprecision(0) << penetrate.size() << " "
		<< "edge out  : " << std::setw(5) << std::setprecision(0) << edge_out.size() << " "
		<< "stop      : " << std::setw(5) << std::setprecision(0) << m.chains.size() << std::endl;
	ofs_log.close();

	output_mfile(file_out_penetrate, penetrate, m.header);
	output_mfile(file_out_edgeout, edge_out, m.header);
	output_mfile(file_out_stop, m.chains, m.header);

	std::ofstream ofs(file_up_inf);
	//output_upstream_track(ofs, penetrate);
	//output_upstream_track(ofs, edge_out);
	output_upstream_track(ofs, m.chains);
}


std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename) {

	std::ifstream ifs(filename);
	std::multimap<int, Fiducial_Area> fa_multi;
	std::map<int, std::vector<Fiducial_Area>> ret;
	Fiducial_Area fa;
	while (ifs >> fa.pl >> fa.x0 >> fa.y0 >> fa.z0 >> fa.x1 >> fa.y1 >> fa.z1) {
		fa_multi.insert(std::make_pair(fa.pl, fa));
	}

	int count = 0;
	for (auto itr = fa_multi.begin(); itr != fa_multi.end(); itr++) {
		count = fa_multi.count(itr->first);
		auto range = fa_multi.equal_range(itr->first);
		std::vector<Fiducial_Area> fa_vec;
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			fa_vec.push_back(itr2->second);
		}
		ret.insert(std::make_pair(itr->first, fa_vec));
	}

	return ret;

}

void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map) {
	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		if (area.count(pl) != 0) {
			auto vec = area.find(pl);
			double tmp_x, tmp_y;
			for (auto itr2 = vec->second.begin(); itr2 != vec->second.end(); itr2++) {
				tmp_x = itr2->x0;
				tmp_y = itr2->y0;
				itr2->x0 = tmp_x * itr->position[0] + tmp_y * itr->position[1] + itr->position[4];
				itr2->y0 = tmp_x * itr->position[2] + tmp_y * itr->position[3] + itr->position[5];
				tmp_x = itr2->x1;
				tmp_y = itr2->y1;
				itr2->x1 = tmp_x * itr->position[0] + tmp_y * itr->position[1] + itr->position[4];
				itr2->y1 = tmp_x * itr->position[2] + tmp_y * itr->position[3] + itr->position[5];
			}
		}
		if (z_map.count(pl) != 0) {
			auto z = z_map.find(pl);
			z->second = z->second + itr->dz;
			//printf("PL%03d z:%.1lf\n", pl, z->second);
		}
	}
}

std::vector<std::vector<mfile0::M_Chain>> group_divide(mfile0::Mfile&m) {

	std::vector<std::vector<mfile0::M_Chain>> ret;
	std::multimap<int64_t, mfile0::M_Chain> group_map;
	int gid;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		group_map.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}
	int count = 0;
	for (auto itr = group_map.begin(); itr != group_map.end(); itr++) {
		count = group_map.count(itr->first);
		std::vector<mfile0::M_Chain> group;
		auto range = group_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			group.push_back(res->second);
		}
		ret.push_back(group);
		itr = std::next(itr, count - 1);
	}

	gid = -1;
	for (auto itr = group_map.begin(); itr != group_map.end(); itr++) {
		count = group_map.count(itr->first);

		if (itr->first / 100000 - gid != 1) {
			printf("gid %d - %d\n", gid, itr->first / 100000);
		}
		gid = itr->first / 100000;
		itr = std::next(itr, count - 1);
	}
	return ret;

}

std::vector<mfile0::M_Chain> divide_single_chain(std::vector<std::vector<mfile0::M_Chain>>&group) {
	std::vector<mfile0::M_Chain> single;
	std::vector<std::vector<mfile0::M_Chain>> multi;
	for (int i = 0; i < group.size(); i++) {
		printf("\r count %d %d", i, group[i].begin()->chain_id);
		std::map<std::pair<int, int>, mfile0::M_Base> base_all = divide_base(group[i]);
		mfile0::M_Chain c;

		if (unique_base_to_chain(base_all, c)) {
			single.push_back(c);
		}
		else if (merge_chains(base_all, c)) {
			single.push_back(c);
		}
		else {
			//multiの場合
			multi.push_back(group[i]);
		}
	}
	printf("\r fin                          \n");

	std::vector<mfile0::M_Chain>multi_group;
	for (int i = 0; i < multi.size(); i++) {
		printf("\r count %d %d %d", i, multi[i].begin()->chain_id, multi.size());
		multi_group.push_back(select_best_chain(multi[i]));
	}
	printf("\r fin                          \n");

	printf("multi = %d\n", multi_group.size());
	printf("single = %d\n", single.size());
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = single.begin(); itr != single.end(); itr++) {
		ret.push_back(*itr);
	}
	for (auto itr = multi_group.begin(); itr != multi_group.end(); itr++) {
		ret.push_back(*itr);
	}
	return ret;
}

std::map<std::pair<int, int>, mfile0::M_Base> divide_base(std::vector< mfile0::M_Chain>&chain) {
	std::map<std::pair<int, int>, mfile0::M_Base> ret;
	for (int i = 0; i < chain.size(); i++) {
		for (int j = 0; j < chain[i].basetracks.size(); j++) {
			ret.insert(std::make_pair(std::make_pair(chain[i].basetracks[j].pos, chain[i].basetracks[j].rawid), chain[i].basetracks[j]));
		}
	}
	return ret;

}
bool unique_base_to_chain(std::map<std::pair<int, int>, mfile0::M_Base>&base_all, mfile0::M_Chain&c) {
	std::map<int, int> unique_count;
	c.basetracks.clear();

	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		auto res = unique_count.insert(std::make_pair(itr->first.first, 1));
		if (!res.second)return false;
		c.basetracks.push_back(itr->second);
	}
	sort(c.basetracks.begin(), c.basetracks.end(), sort_M_base);
	c.nseg = c.basetracks.size();
	c.pos0 = c.basetracks.begin()->pos;
	c.pos1 = c.basetracks.rbegin()->pos;
	c.chain_id = (c.basetracks.begin()->group_id / 100000) * 100000;
	return true;
}
bool merge_chains(std::map<std::pair<int, int>, mfile0::M_Base>&base_all, mfile0::M_Chain&c) {
	std::map<int, int> unique_count;
	std::multimap<int, mfile0::M_Base> base_map;
	c.basetracks.clear();
	mfile0::M_Base start_base;
	bool start_flg = false;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		auto res = unique_count.insert(std::make_pair(itr->first.first, 1));
		if (!res.second) {
			res.first->second++;
		}
		base_map.insert(std::make_pair(itr->second.pos, itr->second));

		if (itr->second.flg_i[0] == 1) {
			if (start_flg&&itr->second.pos < start_base.pos) {
				start_base = itr->second;
			}
			else {
				start_base = itr->second;
				start_flg = true;
			}
		}
	}
	if (!start_flg)return false;

	int double_count = 0;
	for (auto itr = unique_count.begin(); itr != unique_count.end(); itr++) {
		if (itr->second == 1) {
			double_count = 0;
		}
		if (itr->second == 2) {
			double_count++;
		}
		if (double_count > 2)return false;
		if (itr->second > 2)return false;
	}

	//開始basetrackの決定
	int count = 0;
	auto start = base_map.begin();
	start_flg = false;
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		if (itr->second.pos == start_base.pos&&itr->second.rawid == start_base.rawid&&itr->second.flg_i[0] == 1) {
			c.basetracks.push_back(itr->second);
			start = itr;
			start_flg = true;
		}
	}
	if (!start_flg)return false;

	for (auto itr = std::next(start, 1); itr != base_map.end(); itr++) {
		if (itr->second.pos == start->second.pos)continue;

		count = base_map.count(itr->first);
		if (count == 1) {
			c.basetracks.push_back(itr->second);
		}
		//2track以上ある場合
		else {
			//距離を比較。離れていたらmutli-->flgをtrue
			std::vector<mfile0::M_Base> multi_base;
			mfile0::M_Base cand;
			auto range = base_map.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				multi_base.push_back(res->second);
			}
			//近接なら良いほうをselにpush back
			mfile0::M_Base ori = *(c.basetracks.rbegin());
			double md, extra[2], z_range[2];
			matrix_3D::vector_3D pos0, pos1, dir0, dir1;
			pos0.x = c.basetracks.rbegin()->x;
			pos0.y = c.basetracks.rbegin()->y;
			pos0.z = c.basetracks.rbegin()->z;
			dir0.x = c.basetracks.rbegin()->ax;
			dir0.y = c.basetracks.rbegin()->ay;
			dir0.z = 1;

			for (int i = 0; i < multi_base.size(); i++) {
				pos1.x = multi_base[i].x;
				pos1.y = multi_base[i].y;
				pos1.z = multi_base[i].z;
				dir1.x = multi_base[i].ax;
				dir1.y = multi_base[i].ay;
				dir1.z = 1;
				z_range[0] = pos0.z;
				z_range[1] = pos1.z;
				if (i == 0) {
					md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
					cand = multi_base[i];
				}
				if (md > matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra)) {
					md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
					cand = multi_base[i];
				}
			}
			c.basetracks.push_back(cand);
		}

		itr = std::next(itr, count - 1);
	}

	sort(c.basetracks.begin(), c.basetracks.end(), sort_M_base);
	c.nseg = c.basetracks.size();
	c.pos0 = c.basetracks.begin()->pos;
	c.pos1 = c.basetracks.rbegin()->pos;
	c.chain_id = (c.basetracks.begin()->group_id / 100000) * 100000;

	return true;

}

mfile0::M_Chain select_best_chain(std::vector<mfile0::M_Chain> &group) {
	std::vector<mfile0::M_Chain> muon_chains;
	bool flg = false;
	int nseg_max = 0;
	for (auto itr = group.begin(); itr != group.end(); itr++) {
		flg = false;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (itr2->flg_i[0] == 1)flg = true;
		}
		if (flg) {
			muon_chains.push_back(*itr);
			nseg_max = std::max(nseg_max, itr->nseg);
		}
	}
	std::vector<mfile0::M_Chain> long_chain;
	for (auto itr = muon_chains.begin(); itr != muon_chains.end(); itr++) {
		if (itr->nseg == nseg_max) {
			long_chain.push_back(*itr);
		}
	}

	double dlat, ax, ay;
	mfile0::M_Chain ret;
	for (auto itr = long_chain.begin(); itr != long_chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (itr == long_chain.begin()) {
			dlat = mfile0::angle_diff_dev_lat(*itr, ax, ay);
			ret = *itr;
		}
		if (dlat > mfile0::angle_diff_dev_lat(*itr, ax, ay)) {
			dlat = mfile0::angle_diff_dev_lat(*itr, ax, ay);
			ret = *itr;
		}
	}

	for (auto itr = ret.basetracks.begin(); itr != ret.basetracks.end(); itr++) {
		itr->flg_i[1] = 1;
	}

	return ret;
}



//mfile0::M_Chain Get_chain(std::vector<mfile0::M_Base>&basetracks, int &chainid) {
//	sort(basetracks.begin(), basetracks.end(), sort_M_base);
//	mfile0::M_Chain ret;
//	ret.basetracks = basetracks;
//	ret.chain_id = chainid;
//	chainid++;
//	ret.nseg = ret.basetracks.size();
//	ret.pos0 = ret.basetracks.begin()->pos;
//	ret.pos1 = ret.basetracks.rbegin()->pos;
//	return ret;
//}

std::vector<mfile0::M_Chain> divide_penetrate(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&penetrate, int veto_pl) {
	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		if (up_pl >= veto_pl) {
			penetrate.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("penetrate = %d\n", penetrate.size());
	return ret;
}

std::vector<mfile0::M_Chain> divide_IronECC(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&iron, int veto_pl) {
	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		if (up_pl <= veto_pl) {
			iron.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("IronECC = %d\n", iron.size());
	return ret;
}

std::vector<mfile0::M_Chain> divide_edge_out(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&edge_out, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map, double edge_cut, int ex_pl_max) {


	double xmin, xmax, ymin, ymax;


	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	double up_z, up_x, up_y, up_ax, up_ay, ex_z, ex_x, ex_y;
	bool flg = false;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		flg = false;
		up_z = z_map.at(up_pl);
		up_x = itr->basetracks.rbegin()->x;
		up_y = itr->basetracks.rbegin()->y;
		up_ax = itr->basetracks.rbegin()->ax;
		up_ay = itr->basetracks.rbegin()->ay;
		for (int ex_pl = 0; ex_pl <= ex_pl_max; ex_pl++) {
			if (z_map.count(up_pl + ex_pl) == 0)continue;
			ex_z = z_map.at(up_pl + ex_pl);
			ex_x = up_x + up_ax * (ex_z - up_z);
			ex_y = up_y + up_ay * (ex_z - up_z);

			if (!judge_fiducial_area(area.at(up_pl + ex_pl), ex_x, ex_y)) {
				flg = true;
			}
		}
		if (flg) {
			edge_out.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("edge out = %d\n", edge_out.size());
	return ret;
}

bool judge_fiducial_area(std::vector<Fiducial_Area>&area, double x, double y) {
	//true でArea内　falseでarea外

	//点(x,y)からx軸性の方向に直線を引き、その直線と多角形の辺が何回交わるか。
	//下から上に交わったときwn+1
	//上から下に交わったときwn-1
	int wn = 0;
	double vt;
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		// 上向きの辺、下向きの辺によって処理が分かれる。
	// 上向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、終点は含まない。(ルール1)
		if (itr->y0 <= y && itr->y1 > y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->y0) / (itr->y1 - itr->y0);
			if (x < itr->x0 + vt * (itr->x1 - itr->x0)) {
				++wn;  //ここが重要。上向きの辺と交差した場合は+1
			}
		}
		// 下向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、始点は含まない。(ルール2)
		else if (itr->y0 > y && itr->y1 <= y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->y0) / (itr->y1 - itr->y0);
			if (x < itr->x0 + vt * (itr->x1 - itr->x0)) {
				--wn;  //ここが重要。下向きの辺と交差した場合は-1
			}
		}
	}
	if (wn >= 1)return true;
	return false;
}

void output_mfile(std::string filename, std::vector<mfile0::M_Chain>&chain, mfile0::M_Header &header) {

	mfile0::Mfile m;
	m.header = header;
	m.chains = chain;
	mfile0::write_mfile(filename, m);

}

void output_upstream_track(std::ofstream &ofs, std::vector<mfile0::M_Chain>&chain) {
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (itr2->flg_i[0] != 1)continue;
			ofs << std::right << std::fixed
				<< std::setw(10) << std::setprecision(0) << itr2->group_id<< " "
				<< std::setw(5) << std::setprecision(0) << itr2->flg_i[1]<< " "
				<< std::setw(5) << std::setprecision(0) << itr2->pos/10<< " "
				<< std::setw(10) << std::setprecision(0) << itr2->rawid <<  std::endl;
		}
	}
}

std::vector<std::vector<mfile0::M_Base>> divide_mutli(std::vector<std::vector<mfile0::M_Base>>&all, std::vector<std::vector<mfile0::M_Base>>&single) {
	std::vector<std::vector<mfile0::M_Base>> ret;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		bool flg = false;
		std::multimap<int, mfile0::M_Base> base_map;
		for (auto itr2 = itr->begin(); itr2 != itr->end(); itr2++) {
			base_map.insert(std::make_pair(itr2->pos / 10, *itr2));
		}
		//最初はmuonなのでmultiになりえない
		mfile0::M_Base prev = base_map.begin()->second;
		int count;
		std::vector<mfile0::M_Base> sel;
		for (auto itr2 = base_map.begin(); itr2 != base_map.end(); itr2++) {
			count = base_map.count(itr2->first);
			if (count == 1) {
				prev = itr2->second;
				sel.push_back(prev);
			}
			//2track以上ある場合
			else {
				//距離を比較。離れていたらmutli-->flgをtrue
				std::vector<mfile0::M_Base> multi_base;
				auto range = base_map.equal_range(itr2->first);
				for (auto res = range.first; res != range.second; res++) {
					multi_base.push_back(res->second);
				}
				double dist;
				for (int i = 0; i < multi_base.size(); i++) {
					for (int j = i + 1; j < multi_base.size(); j++) {
						dist = sqrt(pow(multi_base[i].x - multi_base[j].x, 2) + pow(multi_base[i].x - multi_base[j].x, 2));
						if (dist > 500)flg = true;
					}
				}
				//近接なら良いほうをselにpush back
				if (!flg) {
					double ex_x, ex_y, gap;
					gap = multi_base.begin()->z - prev.z;
					ex_x = prev.x + prev.ax*(gap);
					ex_y = prev.y + prev.ay*(gap);
					for (int i = 0; i < multi_base.size(); i++) {
						if (i == 0) {
							dist = sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2));
							prev = multi_base[i];
						}
						if (dist > sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2))) {
							dist = sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2));
							prev = multi_base[i];
						}
					}
					sel.push_back(prev);
				}
			}

			itr2 = std::next(itr2, count - 1);
		}
		//近接にしかmultiがなかった場合
		if (!flg) {
			single.push_back(sel);
			//single.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("mutli --> single %d\n", single.size());
	printf("mutli --> mutli  %d\n", ret.size());
	return ret;
}

