#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
std::vector<vxx::micro_track_t> angle_cut(std::vector<vxx::micro_track_t>&micro, double angle_min, double angle_max);

int main(int argc, char** argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg fvxx pos anglemin anglemax file-out-fvxx\n");
		exit(1);
	}
	std::string file_in_fvxx = argv[1];
	int pos = std::stoi(argv[2]);
	double anglemin = std::stod(argv[3]);
	double anglemax = std::stod(argv[4]);
	std::string file_out_fvxx = argv[5];

	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> micro = fr.ReadAll(file_in_fvxx, pos, 0);
	micro = angle_cut(micro, anglemin, anglemax);

	vxx::FvxxWriter fw;
	fw.Write(file_out_fvxx,pos, 0, micro);
	
}
std::vector<vxx::micro_track_t> angle_cut(std::vector<vxx::micro_track_t>&micro, double angle_min, double angle_max) {
	std::vector<vxx::micro_track_t> ret;
	double angle;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angle < angle_min)continue;
		if (angle_max < angle)continue;
		ret.push_back(*itr);
	}
	return ret;
}