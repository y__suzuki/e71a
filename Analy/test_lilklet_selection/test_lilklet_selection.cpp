#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_format_micro {
public:
	int pos, view, imager, zone, isg, ph, vph, px;
};
class output_format_base {
public:
	int pl, rawid;
	double ax, ay, x, y, z;
	output_format_micro m[2];
};

class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};

std::vector<output_format_link> read_linklet_bin(std::string filename);
std::vector<output_format_link>  linklet_selection(std::vector<output_format_link>&link0, std::vector<output_format_link>&link1);
void output_pair_bin(std::string filename, std::vector<output_format_link>&link);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-link0 file0in-link1 file_out_link\n");
		exit(1);
	}
	std::string file_in_link0 = argv[1];
	std::string file_in_link1 = argv[2];
	std::string file_out_link = argv[3];

	std::vector<output_format_link> link0 = read_linklet_bin(file_in_link0);
	std::vector<output_format_link> link1 = read_linklet_bin(file_in_link1);

	link1 = linklet_selection(link0,link1);
	output_pair_bin(file_out_link, link1);



}
std::vector<output_format_link> read_linklet_bin(std::string filename) {
	std::vector<output_format_link> ret;

	std::ifstream ifs(filename, std::ios::binary);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	output_format_link l;
	while (ifs.read((char*)& l, sizeof(output_format_link))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(l);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;

}
std::vector<output_format_link>  linklet_selection(std::vector<output_format_link>&link0, std::vector<output_format_link>&link1){
	std::vector<output_format_link> ret;
	
	std::set<std::pair<int, int>> use_pair;
	std::pair<int, int> id;
	for (auto itr = link0.begin(); itr != link0.end(); itr++) {
		id.first = itr->b[0].pl;
		id.second = itr->b[0].rawid;
		use_pair.insert(id);
		id.first = itr->b[1].pl;
		id.second = itr->b[1].rawid;
		use_pair.insert(id);

	}	
	for (auto itr = link1.begin(); itr != link1.end(); itr++) {
		int count0 = use_pair.count(std::make_pair(itr->b[0].pl, itr->b[0].rawid));
		int count1 = use_pair.count(std::make_pair(itr->b[1].pl, itr->b[1].rawid));
		if (count0 == 0 && count1 == 0)continue;
		ret.push_back(*itr);
	}
	return ret;

}
void output_pair_bin(std::string filename, std::vector<output_format_link>&link) {

	std::ofstream ofs(filename, std::ios::binary);
	int count = 0;
	for (auto l : link) {
		if (count % 10000 == 0) {
			printf("\r write linklet %10d/%10d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;
		ofs.write((char*)& l, sizeof(output_format_link));
	}

	printf("\r write linklet %10d/%10d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

}
