#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>


class basetrack_minimum {
public:
	int pl, rawid, ph;
	float ax, ay, x, y;
};

class basetrack_minimum_z :public basetrack_minimum {
public:
	int trkid, black_flg;
	float z, ex_z0, ex_z1, md;
};

class output_track {
public:
	int eventid;
	basetrack_minimum_z muon;
	std::vector<basetrack_minimum_z> partner;
};
class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};
struct Segment {
	int16_t pos;
	int32_t rawid;
	bool operator==(const Segment& rhs) const
	{
		return pos == rhs.pos && rawid == rhs.rawid;
	}
	bool operator<(const Segment& rhs) const {
		if (pos == rhs.pos) {
			return rawid < rhs.rawid;
		}
		return pos < rhs.pos;
	}
};
size_t hash_value(const Segment& d)
{
	// 複数の値のハッシュ値を組み合わせてハッシュ値を計算するには、
	// boost::hash_combine を使います。
	size_t h = 0;
	boost::hash_combine(h, d.pos);
	boost::hash_combine(h, d.rawid);
	return h;
}

std::vector<output_track> read_interaction_file(std::string filename);
bool judge_penetrate_search_upstream(int muon_pl, Segment start, boost::unordered_multimap<Segment, Segment> &link_next);

bool judge_penetrate_search_downstream(int muon_pl, Segment start, boost::unordered_multimap<Segment, Segment> &link_prev);
void reject_penetrate(std::vector<output_track>&interactions, boost::unordered_multimap<Segment, Segment> &link_next, boost::unordered_multimap<Segment, Segment> &link_prev);

std::vector<std::tuple<int, int, int, int>> read_event_id(std::string filename);
void read_linklet_list(std::string filename, boost::unordered_multimap<Segment, Segment> &link_next, boost::unordered_multimap<Segment, Segment> &link_prev);

void output_Track(std::string filename, std::vector<std::tuple<int, int, int, int>>&id_list, std::vector<output_track>&interactions);

int main(int argc, char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-partner-file in-id-file file-in-ECC file-out-id\n");
		exit(1);
	}


	std::string file_in_partner = argv[1];
	std::string file_in_id = argv[2];
	std::string file_in_link = argv[3];
	std::string file_out_id = argv[4];

	std::vector<output_track> interactions = read_interaction_file(file_in_partner);
	std::vector<std::tuple<int, int, int, int>>event_id = read_event_id(file_in_id);

	boost::unordered_multimap<Segment, Segment> link_next;
	boost::unordered_multimap<Segment, Segment> link_prev;
	read_linklet_list(file_in_link, link_next, link_prev);

	reject_penetrate(interactions, link_next, link_prev);

	output_Track(file_out_id, event_id, interactions);

	exit(0);

}

std::vector<output_track> read_interaction_file(std::string filename) {
	std::vector<output_track> ret;

	std::ifstream ifs(filename);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}

	std::string str;
	int cnt = 0;
	output_track int_file;
	basetrack_minimum_z track;

	int event_id = 0, track_num = 0;
	while (ifs >> int_file.eventid >> track_num) {
		nowpos = ifs.tellg();
		auto size1 = nowpos - begpos;
		std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		int_file.partner.clear();
		for (int i = 0; i < track_num; i++) {
			ifs >> track.trkid >> track.pl >> track.rawid >> track.ph >> track.black_flg >> track.ax >> track.ay >> track.x >> track.y >> track.z >> track.ex_z0 >> track.ex_z1 >> track.md;
			if (i == 0) {
				int_file.muon = track;
			}
			else {
				int_file.partner.push_back(track);
			}
		}
		ret.push_back(int_file);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;
	return ret;
}
int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}

void read_linklet_list(std::string filename, boost::unordered_multimap<Segment, Segment> &link_next, boost::unordered_multimap<Segment, Segment> &link_prev) {
	int64_t link_num = Linklet_header_num(filename);

	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	linklet_header l;
	Segment seg0, seg1;

	while (ifs.read((char*)& l, sizeof(linklet_header))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		seg0.pos = l.pos0;
		seg0.rawid = l.raw0;
		seg1.pos = l.pos1;
		seg1.rawid = l.raw1;
		if (seg0.pos > seg1.pos)std::swap(seg0, seg1);

		link_next.insert(std::make_pair(seg0, seg1));
		link_prev.insert(std::make_pair(seg1, seg0));
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
}

void reject_penetrate(std::vector<output_track>&interactions, boost::unordered_multimap<Segment, Segment> &link_next, boost::unordered_multimap<Segment, Segment> &link_prev) {

	int muon_pl = 0, count = 0;
	Segment start;
	for (int i = 0; i < interactions.size(); i++) {
		muon_pl = interactions[i].muon.pl;
		count = 0;
		if (interactions[i].partner.size() == 0)continue;
		printf("event=%6d track num=%7d\n", interactions[i].eventid, interactions[i].partner.size());

		for (auto itr = interactions[i].partner.begin(); itr != interactions[i].partner.end(); ) {
			printf("\r now processing =%7d", count);
			count++;
			if (itr->black_flg == 1) {
				itr++;
				continue;
			}
			start.pos = itr->pl / 10 * 10;
			start.rawid = itr->rawid;
			if (itr->pl <= muon_pl) {
				if (judge_penetrate_search_upstream(muon_pl, start, link_next)) {
					itr = interactions[i].partner.erase(itr);
				}
				else {
					itr++;
				}
			}
			else {
				if (judge_penetrate_search_downstream(muon_pl, start, link_prev)) {
					itr = interactions[i].partner.erase(itr);
				}
				else {
					itr++;
				}
			}
		}
		printf("\r now processing =%7d --> track num=%d\n", count, interactions[i].partner.size());

	}
	printf("\n");
}
bool judge_penetrate_search_upstream(int muon_pl, Segment start, boost::unordered_multimap<Segment, Segment> &link_next) {
	bool ret = false;
	std::set<int> reach_pl;
	std::set<Segment > all,add,now;

	add.insert(start);
	reach_pl.insert(start.pos / 10);
	while (add.size() > 0) {
		now = add;
		add.clear();
		
		for (auto itr = now.begin(); itr != now.end(); itr++) {
			if (link_next.count(*itr) == 0)continue;
			auto range = link_next.equal_range(*itr);
			for (auto res = range.first; res != range.second; res++) {
				auto insert_res=all.insert(res->second);
				if (insert_res.second) {
					add.insert(res->second);
				}
			}
		}

		for (auto itr = add.begin(); itr != add.end(); itr++) {
			reach_pl.insert(itr->pos/10);
		}

		//貫通条件
		if (*reach_pl.rbegin() > muon_pl) {
			ret = true;
			break;
		}
		
	}
	return ret;

}
bool judge_penetrate_search_downstream(int muon_pl, Segment start, boost::unordered_multimap<Segment, Segment> &link_prev) {
	bool ret = false;
	std::set<int> reach_pl;
	std::set<Segment > all, add, now;

	add.insert(start);
	reach_pl.insert(start.pos / 10);
	while (add.size() > 0) {
		now = add;
		add.clear();

		for (auto itr = now.begin(); itr != now.end(); itr++) {
			if (link_prev.count(*itr) == 0)continue;
			auto range = link_prev.equal_range(*itr);
			for (auto res = range.first; res != range.second; res++) {
				auto insert_res = all.insert(res->second);
				if (insert_res.second) {
					add.insert(res->second);
				}
			}
		}

		for (auto itr = add.begin(); itr != add.end(); itr++) {
			reach_pl.insert(itr->pos / 10);
		}

		//貫通条件
		if (*reach_pl.begin() <= muon_pl) {
			ret = true;
			break;
		}

	}
	return ret;
}

//eventid,pl,rawid
std::vector<std::tuple<int, int, int, int>> read_event_id(std::string filename) {
	std::vector<std::tuple<int, int, int, int>>ret;
	std::ifstream ifs(filename);
	std::tuple<int, int, int, int> id;
	while (ifs >> std::get<0>(id) >> std::get<1>(id) >> std::get<2>(id) >> std::get<3>(id)) {
		ret.push_back(id);
	}
	return ret;
}
void output_Track(std::string filename, std::vector<std::tuple<int, int, int, int>>&id_list, std::vector<output_track>&interactions) {

	std::set<std::tuple<int, int, int>> output_list;
	std::tuple<int, int, int>id;
	for (int i = 0; i < interactions.size(); i++) {
		std::get<0>(id) = interactions[i].eventid;

		for (auto itr = interactions[i].partner.begin(); itr != interactions[i].partner.end(); itr++) {
			std::get<1>(id) = itr->pl;
			std::get<2>(id) = itr->rawid;
			output_list.insert(id);
		}
	}
	std::ofstream ofs(filename);
	for (auto itr = id_list.begin(); itr != id_list.end(); itr++) {
		std::get<0>(id) = std::get<0>(*itr);
		std::get<1>(id) = std::get<2>(*itr);
		std::get<2>(id) = std::get<3>(*itr);
		if (output_list.count(id) == 0)continue;


		ofs << std::fixed << std::right
			<< std::setw(10) << std::setprecision(0) << std::get<0>(*itr) << " "
			<< std::setw(5) << std::setprecision(0) << std::get<1>(*itr) << " "
			<< std::setw(4) << std::setprecision(0) << std::get<2>(*itr) << " "
			<< std::setw(10) << std::setprecision(0) << std::get<3>(*itr) << std::endl;
	}

}
