#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <omp.h>
#include <set>

mfile0::Mfile mfile_pickup(mfile0::Mfile&m, mfile0::Mfile&msel);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage;file-in-mfile file-in-mfile2 file-out\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_mfile2 = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m_all;
	mfile0::Mfile m_sel;
	mfile0::read_mfile(file_in_mfile, m_all);
	mfile0::read_mfile(file_in_mfile2, m_sel);

	m_all = mfile_pickup(m_all, m_sel);

	mfile0::write_mfile(file_out_mfile, m_all);

}
mfile0::Mfile mfile_pickup(mfile0::Mfile&m, mfile0::Mfile&msel) {

	std::set<int> gid;
	for (auto itr = msel.chains.begin(); itr != msel.chains.end(); itr++) {
		gid.insert(itr->basetracks.begin()->group_id / 100000);
	}
	mfile0::Mfile ret;
	ret.header = m.header;

	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (gid.count(itr->basetracks.begin()->group_id / 100000) == 1) {
			ret.chains.push_back(*itr);

		}
	}

	printf("num of event = %d\n", gid.size());
	return ret;
}