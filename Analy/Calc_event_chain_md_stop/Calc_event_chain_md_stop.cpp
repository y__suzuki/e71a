#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <filesystem>
#include <set>
#include <omp.h>

class basetrack_minimum {
public:
	int pl, rawid, ph;
	float ax, ay, x, y;
};

class basetrack_minimum_z :public basetrack_minimum {
public:
	int trkid, black_flg;
	float z, ex_z0, ex_z1, md;
};

Momentum_recon::Event_information Momch_chain_sel(int eventid, int chainid, std::vector<Momentum_recon::Event_information> &momch_divide);
std::vector <std::pair<basetrack_minimum_z*, corrmap_3d::align_param2*>>track_affineparam_correspondence(std::vector<basetrack_minimum_z>&base, std::vector <corrmap_3d::align_param2> &param);
void trans_base_all(std::vector < std::pair<basetrack_minimum_z*, corrmap_3d::align_param2*>>&track_pair);
void Calc_md(Momentum_recon::Event_information&momch, std::string file_in_ECC, std::string file_in_Base);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage filename ECC-path groupid chainid output\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_in_ECC = argv[2];
	int groupid = std::stod(argv[3]);
	int chainid = std::stod(argv[4]);
	std::string file_out = argv[5];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	Momentum_recon::Event_information momch_sel = Momch_chain_sel(groupid, chainid, momch);

	Calc_md(momch_sel, file_in_ECC, file_out);


}


Momentum_recon::Event_information Momch_chain_sel(int eventid, int chainid, std::vector<Momentum_recon::Event_information> &momch_divide) {

	Momentum_recon::Event_information ret;

	for (auto itr = momch_divide.begin(); itr != momch_divide.end(); itr++) {
		if (itr->groupid != eventid)continue;
		ret = *itr;
		ret.chains.clear();
		for (auto &c : itr->chains) {
			if (c.chainid == 0) {
				ret.chains.push_back(c);
				continue;
			}

			if (c.chainid != chainid)continue;
			ret.chains.push_back(c);
		}


	}
	return ret;

}


int64_t Basetrack_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(basetrack_minimum);
}

basetrack_minimum_z read_base(std::string file_in_ECC, int pl, int rawid) {


	std::stringstream filename;
	filename << file_in_ECC << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl << "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";

	std::vector<vxx::base_track_t> base;
	vxx::BvxxReader br;
	std::array<int, 2> index = { rawid, rawid + 1 };
	base = br.ReadAll(filename.str(), pl, 0, vxx::opt::index = index);
	basetrack_minimum b;
	b.ax = base[0].ax;
	b.ay = base[0].ay;
	b.x = base[0].x;
	b.y = base[0].y;
	b.ph = base[0].m[0].ph + base[0].m[1].ph;
	b.rawid = base[0].rawid;
	b.pl = base[0].pl;


	basetrack_minimum_z b_z;
	b_z.ax = b.ax;
	b_z.ay = b.ay;
	b_z.x = b.x;
	b_z.y = b.y;
	b_z.z = 0;
	b_z.ph = b.ph;
	b_z.pl = b.pl;
	b_z.rawid = b.rawid;
	b_z.trkid = 0;
	b_z.black_flg = 0;
	b_z.ex_z0 = 0;
	b_z.ex_z1 = 0;

	return b_z;

}


std::string Get_alignment_filename(std::string file_in_ECC, int in_pl0, int in_pl1) {
	std::string ret;

	std::string file_in_align_path = file_in_ECC + "\\Area0\\0\\align\\fine";
	std::filesystem::directory_iterator iter(file_in_align_path), end;
	std::error_code err;

	std::vector<std::string > file_names;
	for (; iter != end && !err; iter.increment(err)) {
		const std::filesystem::directory_entry entry = *iter;

		file_names.push_back(entry.path().string());
		//printf("%s\n", file_names.back().c_str());
	}
	for (int i = 0; i < file_names.size(); i++) {
		int length = file_names[i].size();
		if (file_names[i].substr(length - 4, 4) != ".txt")continue;
		if (file_names[i].substr(length - 18, 14) != "_interpolation")continue;

		int pl0 = std::stoi(file_names[i].substr(length - 25, 3));
		int pl1 = std::stoi(file_names[i].substr(length - 21, 3));
		if (in_pl0 == pl0 && in_pl1 == pl1) {
			return file_names[i];
		}
	}
	return ret;
}

std::string Get_alignment_inv_filename(std::string file_in_ECC, int in_pl0, int in_pl1) {
	std::string ret;

	std::string file_in_align_path = file_in_ECC + "\\0\\align_inv\\fine";
	std::filesystem::directory_iterator iter(file_in_align_path), end;
	std::error_code err;

	std::vector<std::string > file_names;
	for (; iter != end && !err; iter.increment(err)) {
		const std::filesystem::directory_entry entry = *iter;

		file_names.push_back(entry.path().string());
		//printf("%s\n", file_names.back().c_str());
	}
	for (int i = 0; i < file_names.size(); i++) {
		int length = file_names[i].size();
		if (file_names[i].substr(length - 4, 4) != ".txt")continue;
		if (file_names[i].substr(length - 18, 14) != "_interpolation")continue;

		int pl0 = std::stoi(file_names[i].substr(length - 25, 3));
		int pl1 = std::stoi(file_names[i].substr(length - 21, 3));

		if (in_pl0 == pl0 && in_pl1 == pl1) {
			return file_names[i];
		}
	}
	return ret;
}
void Set_search_z(double z_range[2], int muonPL) {
	bool iron = false;
	bool water = false;
	bool other = false;
	if (muonPL <= 15)iron = true;
	else if (16 <= muonPL && muonPL % 2 == 0)iron = true;
	else if (16 <= muonPL && muonPL % 2 == 1)water = true;
	else other = true;

	if (water) {
		//water-emulsion-iron-emulsion-water- (+a)
		z_range[0] = -2300 - 350 - 110 * 2 - 1000;
		//z_range[1] = +210 + 70 + 30;
		z_range[1] = 0;
	}
	else {
		//iron-emulsion-water- (+a)
		z_range[0] = -500 - 350 - 70 - 50;
		//z_range[1] = +210 + 70 + 30;
		z_range[1] = 0;
	}
}

std::vector <std::pair< basetrack_minimum_z, double>> search_local_dz(std::vector<std::pair<basetrack_minimum_z, basetrack_minimum_z>>attach_candidate, std::vector <  basetrack_minimum_z>&local_dz) {
	std::vector <std::pair< basetrack_minimum_z, double>> ret;
	std::pair< basetrack_minimum_z, double> t_dz_pair;
	for (auto &t : attach_candidate) {

		double dist = DBL_MAX;
		double dist_tmp = -1;
		double dz_min;
		for (auto &dz : local_dz) {
			dist_tmp = pow(dz.x - t.second.x, 2) + pow(dz.y - t.second.y, 2);
			if (dist_tmp < dist) {
				dist = dist_tmp;
				dz_min = dz.z;
			}
		}
		//printf("distans %.1lf dz=%.1lf\n", sqrt(dist), dz_min);
		t_dz_pair.first = t.first;
		t_dz_pair.second = dz_min;
		ret.push_back(t_dz_pair);
	}
	return ret;
}

void pre_partner_search(basetrack_minimum_z target, std::vector<basetrack_minimum_z>&all, double z_range[2], std::vector<std::pair<basetrack_minimum_z, basetrack_minimum_z>>&attach_candidate) {

	matrix_3D::vector_3D pos, dir;
	pos.x = target.x;
	pos.y = target.y;
	pos.z = 0;

	dir.x = target.ax;
	dir.y = target.ay;
	dir.z = 1;

	basetrack_minimum_z md_point;

	double md, extra[2], allowance, angle;
	matrix_3D::vector_3D pos_all, dir_all;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		if (itr->pl == target.pl&&itr->rawid == target.rawid)continue;
		pos_all.x = itr->x;
		pos_all.y = itr->y;
		pos_all.z = itr->z;
		dir_all.x = itr->ax;
		dir_all.y = itr->ay;
		dir_all.z = 1;
		md = matrix_3D::minimum_distance(pos, pos_all, dir, dir_all, z_range, extra);

		md_point = *itr;
		//mdを組むxy座標の取得
		matrix_3D::vector_3D extra0 = matrix_3D::addition(pos, const_multiple(dir, extra[0]));
		matrix_3D::vector_3D extra1 = matrix_3D::addition(pos_all, const_multiple(dir_all, extra[1]));

		md_point.x = (extra0.x + extra1.x) / 2;
		md_point.y = (extra0.y + extra1.y) / 2;
		md_point.z = 0;

		attach_candidate.push_back(std::make_pair(*itr, md_point));



	}


}
void partner_search_single(basetrack_minimum_z target, basetrack_minimum_z&all, double z_range[2], double &md, double &extra_muon, double &extra_proton) {

	matrix_3D::vector_3D pos, dir;
	pos.x = target.x;
	pos.y = target.y;
	pos.z = 0;

	dir.x = target.ax;
	dir.y = target.ay;
	dir.z = 1;

	double  extra[2], allowance, angle;
	matrix_3D::vector_3D pos_all, dir_all;
	pos_all.x = all.x;
	pos_all.y = all.y;
	pos_all.z = all.z;
	dir_all.x = all.ax;
	dir_all.y = all.ay;
	dir_all.z = 1;

	md = matrix_3D::minimum_distance(pos, pos_all, dir, dir_all, z_range, extra);
	//printf("%.1lf %.1lf\n", z_range[0], z_range[1]);
	all.ex_z0 = pos_all.z;
	all.ex_z1 = pos_all.z + extra[1];

	extra_muon = extra[0];
	extra_proton = extra[1];

	//printf("muon PL%03d proton PL%03d\n", target.pl, all.pl);
	//printf("muon ex %.1lf proton ex %.1lf MD %.1lf\n", extra_muon, extra_proton, md);
}



void Calc_md(Momentum_recon::Event_information&momch, std::string file_in_ECC, std::string file_out) {
	std::ofstream ofs(file_out);

	std::pair<int, int> muon, proton;
	Momentum_recon::Mom_chain muon_chain;
	for (auto &c : momch.chains) {
		if (c.chainid == 0) {
			muon.first = c.base.rbegin()->pl;
			muon.second = c.base.rbegin()->rawid;
			muon_chain = c;
		}
	}
	basetrack_minimum_z muon_base = read_base(file_in_ECC, muon.first, muon.second);

	double z_range[2];

	//z_rangeの設定をする
	Set_search_z(z_range, muon.first);
	std::vector<basetrack_minimum_z> local_dz;
	{
		std::string file_in_align = Get_alignment_filename(file_in_ECC, muon.first, muon.first + 1);
		if (file_in_align == "") {
			fprintf(stderr, "PL%03d - PL%03d align map not found\n", muon.first, muon.first + 1);
		}
		std::vector <corrmap_3d::align_param > corr = corrmap_3d::read_ali_param(file_in_align, false);
		std::vector <corrmap_3d::align_param2 >corr2 = DelaunayDivide(corr);
		int ali_id = 0;
		for (auto &ali : corr2) {
			basetrack_minimum_z point;
			point.x = ali.x;
			point.y = ali.y;
			point.z = 0;
			point.ax = 0;
			point.ay = 0;
			point.black_flg = 0;
			point.ex_z0 = 0;
			point.ex_z1 = 0;
			point.md = 0;
			point.ph = 0;
			point.rawid = ali_id;
			point.trkid = 0;
			point.pl = muon.first + 1;
			ali_id++;
			local_dz.push_back(point);
		}
		//trackとdelaunay3角形の対応
		std::vector <std::pair<basetrack_minimum_z*, corrmap_3d::align_param2*>>track_param_dz = track_affineparam_correspondence(local_dz, corr2);
		//basetrackを変換
		trans_base_all(track_param_dz);
	}

	basetrack_minimum_z proton_base;
	bool forward_flg = true;
	for (auto &c : momch.chains) {
		std::string file_in_align;

		if (c.chainid == 0)continue;
		else if (c.direction==1) {
			proton.first = c.base.rbegin()->pl;
			proton.second = c.base.rbegin()->rawid;
			proton_base = read_base(file_in_ECC, proton.first, proton.second);
			forward_flg = true;
		}
		else if (c.direction == -1) {
			proton.first = c.base.begin()->pl;
			proton.second = c.base.begin()->rawid;
			proton_base = read_base(file_in_ECC, proton.first, proton.second);
			forward_flg = false;

			std::vector<basetrack_minimum_z> target_base;
			target_base.push_back(proton_base);
			file_in_align = Get_alignment_filename(file_in_ECC, muon.first, proton.first);
			if (file_in_align == "") {
				fprintf(stderr, "PL%03d - PL%03d align map not found\n", muon.first, proton.first);
			}
			std::vector <corrmap_3d::align_param > corr = corrmap_3d::read_ali_param(file_in_align, false);
			std::vector <corrmap_3d::align_param2 >corr2 = DelaunayDivide(corr);

			//trackとdelaunay3角形の対応
			std::vector <std::pair<basetrack_minimum_z*, corrmap_3d::align_param2*>>track_param = track_affineparam_correspondence(target_base, corr2);
			trans_base_all(track_param);
			proton_base = target_base[0];
		}

		std::vector<basetrack_minimum_z>all;
		all.push_back(proton_base);

		std::vector<std::pair<basetrack_minimum_z, basetrack_minimum_z>>attach_candidate_all;
		pre_partner_search(muon_base, all, z_range, attach_candidate_all);

		std::vector <std::pair< basetrack_minimum_z, double>>target_base_sel;
		target_base_sel = search_local_dz(attach_candidate_all, local_dz);
		for (int j = 0; j < target_base_sel.size(); j++) {
			//dzの設定(gap+emulsion+a)
			z_range[0] = target_base_sel[j].second - 70 - 30;
			double md, extra_muon, extra_proton;
			partner_search_single(muon_base, target_base_sel[j].first, z_range, md, extra_muon, extra_proton);

			if (!isfinite(c.ecc_mcs_mom[1])) {
				c.ecc_mcs_mom[1] = 0;
			}
			ofs << std::right << std::fixed
				<< std::setw(6) << std::setprecision(0) << momch.groupid << " "
				<< std::setw(6) << std::setprecision(0) << c.chainid << " "
				<< std::setw(2) << std::setprecision(0) << forward_flg << " "
				<< std::setw(7) << std::setprecision(4) << sqrt(pow(muon_base.ax, 2) + pow(muon_base.ay, 2)) << " "
				<< std::setw(6) << std::setprecision(1) << muon_chain.ecc_mcs_mom[0] << " "
				<< std::setw(7) << std::setprecision(4) << sqrt(pow(proton_base.ax, 2) + pow(proton_base.ay, 2)) << " "
				<< std::setw(6) << std::setprecision(1) << c.ecc_mcs_mom[1] << " "
				<< std::setw(5) << std::setprecision(1) << extra_muon << " "
				<< std::setw(5) << std::setprecision(1) << extra_proton << " "
				<< std::setw(8) << std::setprecision(3) << md << std::endl;

		}
	}
}



//basetrack-alignment mapの対応
double select_triangle_vale(corrmap_3d::align_param2* param, basetrack_minimum_z&base) {
	double x, y;
	double dist = 0;
	x = (param->corr_p[0]->x + param->corr_p[1]->x + param->corr_p[2]->x) / 3;
	y = (param->corr_p[0]->y + param->corr_p[1]->y + param->corr_p[2]->y) / 3;
	dist = (base.x - x)*(base.x - x) + (base.y - y)*(base.y - y);
	return dist;
}
corrmap_3d::align_param2* search_param(std::vector<corrmap_3d::align_param*> &param, basetrack_minimum_z&base, std::multimap<int, corrmap_3d::align_param2*>&triangles) {
	//三角形内部
	//最近接三角形
	double dist = 0;
	std::map<double, corrmap_3d::align_param* > dist_map;
	//align_paramを近い順にsort
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		dist = ((*itr)->x - base.x)*((*itr)->x - base.x) + ((*itr)->y - base.y)*((*itr)->y - base.y);
		dist_map.insert(std::make_pair(dist, (*itr)));
	}

	double sign[3];
	bool flg = false;
	int id;

	corrmap_3d::align_param2* ret = triangles.begin()->second;
	for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
		if (itr != dist_map.begin())continue;


		//corrmapのID
		id = itr->second->id;
		if (triangles.count(id) == 0) {
			fprintf(stderr, "alignment triangle ID=%d not found\n", id);
			exit(1);
		}
		//idの属する三角形を探索
		auto range = triangles.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			sign[0] = (itr2->second->corr_p[1]->x - itr2->second->corr_p[0]->x)*(base.y - itr2->second->corr_p[1]->y) - (itr2->second->corr_p[1]->y - itr2->second->corr_p[0]->y)*(base.x - itr2->second->corr_p[1]->x);
			sign[1] = (itr2->second->corr_p[2]->x - itr2->second->corr_p[1]->x)*(base.y - itr2->second->corr_p[2]->y) - (itr2->second->corr_p[2]->y - itr2->second->corr_p[1]->y)*(base.x - itr2->second->corr_p[2]->x);
			sign[2] = (itr2->second->corr_p[0]->x - itr2->second->corr_p[2]->x)*(base.y - itr2->second->corr_p[0]->y) - (itr2->second->corr_p[0]->y - itr2->second->corr_p[2]->y)*(base.x - itr2->second->corr_p[0]->x);
			//printf("point %.lf,%.1lf\n", base.x, base.y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[0]->x, itr2->second->corr_p[0]->y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[1]->x, itr2->second->corr_p[1]->y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[2]->x, itr2->second->corr_p[2]->y);
			//printf("sign %.1lf %1.lf %.1lf\n", sign[0], sign[1], sign[2]);
			//printf("  signbit %d %d %d\n", std::signbit(sign[0]), std::signbit(sign[1]), std::signbit(sign[2]));
			//printf("n signbit %d %d %d\n", !std::signbit(sign[0]), !std::signbit(sign[1]), !std::signbit(sign[2]));
			//printf("judge %d\n", (std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2])));
			//printf("\n");

			//符号が3つとも一致でtrue
			if ((std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2]))) {
				ret = itr2->second;
				flg = true;
				break;
			}
		}
		if (flg)break;
	}
	if (flg) {
		//printf("point in trianlge\n");
		return ret;
	}

	//distが最小になるcorrmapをとってくる
	dist = -1;
	for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
		//corrmapのID
		id = itr->second->id;
		if (triangles.count(id) == 0) {
			fprintf(stderr, "alignment triangle ID=%d not found\n", id);
			exit(1);
		}
		//idの属する三角形を探索
		auto range = triangles.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			if (dist<0 || dist>select_triangle_vale(itr2->second, base)) {
				dist = select_triangle_vale(itr2->second, base);
				ret = itr2->second;
			}
		}
	}
	//printf("point not in trianlge\n");
	return ret;
}
std::vector <std::pair<basetrack_minimum_z*, corrmap_3d::align_param2*>>track_affineparam_correspondence(std::vector<basetrack_minimum_z>&base, std::vector <corrmap_3d::align_param2> &param) {

	//local alignの視野中心を取り出して、位置でhash
	//local alignの視野中心の作るdelaunay三角形をmapで対応

	std::map<int, corrmap_3d::align_param*> view_center;
	std::multimap<int, corrmap_3d::align_param2*>triangles;
	double xmin = 999999, ymin = 999999, hash = 2000;
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		for (int i = 0; i < 3; i++) {
			view_center.insert(std::make_pair(itr->corr_p[i]->id, (itr->corr_p[i])));
			triangles.insert(std::make_pair(itr->corr_p[i]->id, &(*itr)));
			xmin = std::min(itr->corr_p[i]->x, xmin);
			ymin = std::min(itr->corr_p[i]->y, ymin);
		}
	}
	std::multimap<std::pair<int, int>, corrmap_3d::align_param*> view_center_hash;
	std::pair<int, int>id;
	for (auto itr = view_center.begin(); itr != view_center.end(); itr++) {
		id.first = int((itr->second->x - xmin) / hash);
		id.second = int((itr->second->y - ymin) / hash);
		view_center_hash.insert(std::make_pair(id, itr->second));
	}

	std::vector < std::pair<basetrack_minimum_z*, corrmap_3d::align_param2*>> ret;
	std::vector<corrmap_3d::align_param*> param_cand;
	int loop = 0, ix, iy, count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		//if (count % 100000 == 0) {
		//	printf("\r search correspond triangles %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
		//}
		count++;
		ix = (itr->x - xmin) / hash;
		iy = (itr->y - ymin) / hash;
		loop = 1;
		while (true) {
			param_cand.clear();
			for (int iix = ix - loop; iix <= ix + loop; iix++) {
				for (int iiy = iy - loop; iiy <= iy + loop; iiy++) {
					id.first = iix;
					id.second = iiy;
					if (view_center_hash.count(id) != 0) {
						auto range = view_center_hash.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							param_cand.push_back(res->second);
						}
					}
				}
			}
			if (param_cand.size() > 2)break;
			loop++;
		}
		corrmap_3d::align_param2* param2 = search_param(param_cand, *itr, triangles);
		ret.push_back(std::make_pair(&(*itr), param2));
	}
	//printf("\r search correspond triangles %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

	return ret;
}


//変換 zshrink補正-->9para変換
void trans_base(std::vector<basetrack_minimum_z*>&base, corrmap_3d::align_param2 *param) {

	matrix_3D::matrix_33 x_rot_mat(0, param->x_rot), y_rot_mat(1, param->y_rot), z_rot_mat(2, param->z_rot), all_trans(0, 0), shear_mat(0, 0), shrink_mat(0, 0);

	shrink_mat.val[0][0] *= param->x_shrink;
	shrink_mat.val[1][1] *= param->y_shrink;
	//shrink_mat.val[2][2] *= param->z_shrink;
	shear_mat.val[0][1] = param->yx_shear;
	//shear_mat.val[0][2] = param->zx_shear;
	//shear_mat.val[1][2] = param->zy_shear;

	matrix_3D::vector_3D shift, center;
	center.x = param->x;
	center.y = param->y;
	center.z = param->z;
	shift.x = param->dx;
	shift.y = param->dy;
	shift.z = param->dz;

	all_trans.matrix_multiplication(shear_mat);
	all_trans.matrix_multiplication(shrink_mat);
	all_trans.matrix_multiplication(z_rot_mat);
	all_trans.matrix_multiplication(y_rot_mat);
	all_trans.matrix_multiplication(x_rot_mat);

	//all_trans.Print();
	matrix_3D::vector_3D base_p0, base_p1;
	double base_thick = 210;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_p0.x = (*itr)->x;
		base_p0.y = (*itr)->y;
		base_p0.z = param->z;

		base_p1.x = (*itr)->x + (*itr)->ax*(base_thick);
		base_p1.y = (*itr)->y + (*itr)->ay*(base_thick);
		//角度shrink分はここでかける
		base_p1.z = param->z + (base_thick) / param->z_shrink;

		//視野中心を原点に移動
		//base_p0 = matrix_3D::addition(base_p0, matrix_3D::const_multiple(center, -1));
		//base_p1 = matrix_3D::addition(base_p1, matrix_3D::const_multiple(center, -1));

		//変換の実行
		base_p0.matrix_multiplication(all_trans);
		base_p0 = matrix_3D::addition(base_p0, shift);
		base_p1.matrix_multiplication(all_trans);
		base_p1 = matrix_3D::addition(base_p1, shift);

		//原点をもとに戻す
		//base_p0 = matrix_3D::addition(base_p0, center);
		//base_p1 = matrix_3D::addition(base_p1, center);

		(*itr)->x = base_p0.x;
		(*itr)->y = base_p0.y;
		(*itr)->z = base_p0.z;

		//printf("ax:%.4lf --> %.4lf\n", (*itr)->ax, (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z));
		//printf("ay:%.4lf --> %.4lf\n", (*itr)->ay, (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z));

		(*itr)->ax = (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z) + param->zx_shear;
		(*itr)->ay = (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z) + param->zy_shear;
		(*itr)->trkid = -1;
	}
}
void trans_base_all(std::vector < std::pair<basetrack_minimum_z*, corrmap_3d::align_param2*>>&track_pair) {
	std::map<std::tuple<int, int, int>, corrmap_3d::align_param2*> param_map;
	std::multimap<std::tuple<int, int, int>, basetrack_minimum_z*>base_map;
	std::tuple<int, int, int>id;
	//三角形ごとにbasetrackをまとめる
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		std::get<0>(id) = itr->second->corr_p[0]->id;
		std::get<1>(id) = itr->second->corr_p[1]->id;
		std::get<2>(id) = itr->second->corr_p[2]->id;
		param_map.insert(std::make_pair(id, itr->second));
		base_map.insert(std::make_pair(id, itr->first));
	}


	//ここで三角形ごとに変換
	int count = 0;
	std::vector<basetrack_minimum_z*> t_base;
	for (auto itr = param_map.begin(); itr != param_map.end(); itr++) {
		//if (count % 1000 == 0) {
		//	printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)", count, param_map.size(), count*100. / param_map.size());
		//}
		count++;

		t_base.clear();

		if (base_map.count(itr->first) == 0)continue;
		auto range = base_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			t_base.push_back(res->second);
		}
		trans_base(t_base, itr->second);

	}
	//printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)\n", count, param_map.size(), count*100. / param_map.size());

}
