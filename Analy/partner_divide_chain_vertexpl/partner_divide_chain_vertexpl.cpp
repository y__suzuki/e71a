#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Event_information> cut_momch(std::vector<Momentum_recon::Event_information>&ev_v);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-momch file-out-momch\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);

	momch = cut_momch(momch);

	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);
	exit(0);
}
std::vector<Momentum_recon::Event_information> cut_momch(std::vector<Momentum_recon::Event_information>&ev_v) {
	std::vector<Momentum_recon::Event_information> ret;
	for (auto&ev : ev_v) {
		//printf("%d %d\n", ev.groupid, ev.vertex_pl);
		Momentum_recon::Event_information ori = ev;
		Momentum_recon::Event_information back = ev;
		back.groupid = back.groupid*-1;
		ori.chains.clear();
		back.chains.clear();
		for (auto &c : ev.chains) {
			Momentum_recon::Mom_chain c_ori = c;
			Momentum_recon::Mom_chain c_back = c;
			if (c.chainid == 0) {
				ori.chains.push_back(c_ori);
				continue;
			}

			c_ori.base.clear();
			c_ori.base_pair.clear();
			c_back.base.clear();
			c_back.base_pair.clear();
			if (c.direction == 1) {
				for (auto&b : c.base) {
					if (b.pl <= ev.vertex_pl) {
						c_ori.base.push_back(b);
					}
					else {
						c_back.base.push_back(b);
					}
				}
				for (auto &pair : c.base_pair) {
					if (pair.second.pl <= ev.vertex_pl) {
						c_ori.base_pair.push_back(pair);
					}
					else if (pair.first.pl > ev.vertex_pl) {
						c_back.base_pair.push_back(pair);
					}
				}
			}
			else if (c.direction == -1) {
				for (auto&b : c.base) {
					if (b.pl > ev.vertex_pl) {
						c_ori.base.push_back(b);
					}
					else {
						c_back.base.push_back(b);
					}
				}
				for (auto &pair : c.base_pair) {
					if (pair.first.pl > ev.vertex_pl) {
						c_ori.base_pair.push_back(pair);
					}
					else if (pair.second.pl <= ev.vertex_pl) {
						c_back.base_pair.push_back(pair);
					}
				}
			}

			if (c_ori.base.size() > 0) {
				ori.chains.push_back(c_ori);
			}
			if (c_back.base.size() > 0) {
				back.chains.push_back(c_back);
			}

		}
		ret.push_back(ori);
		ret.push_back(back);
	}
	return ret;
}
