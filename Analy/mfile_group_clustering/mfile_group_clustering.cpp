#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>
std::map<int64_t, mfile0::M_Chain> Set_muon_chain_map(std::vector<mfile0::M_Chain>&c);
std::map<int64_t, std::set<std::pair<int, int>>> extract_muon_base(std::vector<mfile0::M_Chain>&c);
std::vector<std::vector<mfile0::M_Chain>> divide_event(mfile0::Mfile &m, std::map<int64_t, std::set<std::pair<int, int>>>&muon);
std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>&c);

bool sort_base(const mfile0::M_Base&left, const  mfile0::M_Base&right) {
	if (left.pos != right.pos) {
		return left.pos < right.pos;
	}
	return left.rawid < right.rawid;
}
bool sort_chain(const mfile0::M_Chain&left, const  mfile0::M_Chain&right) {
	if (left.basetracks.begin()->group_id != right.basetracks.begin()->group_id) {
		return left.basetracks.begin()->group_id < right.basetracks.begin()->group_id;
	}
	return left.chain_id < right.chain_id;

}

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-mfile in-mu-mfile out-event-mfile-path out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_mu_mfile = argv[2];
	std::string file_out_mfile_path = argv[3];
	std::string file_out_mfile = argv[4];

	mfile0::Mfile m;
	mfile0::Mfile m_mu;
	mfile0::Mfile m_out;

	mfile0::read_mfile(file_in_mu_mfile, m_mu);
	std::map<int64_t, mfile0::M_Chain>mu_map = Set_muon_chain_map(m_mu.chains);
	//for (auto itr = mu_map.begin(); itr != mu_map.end(); itr++) {
	//	printf("muon GID=%d\n", itr->first);
	//}
	//printf("muon size=%d\n", mu_map.size());
	std::map<int64_t, std::set<std::pair<int, int>>> muon_base = extract_muon_base(m_mu.chains);
	printf("extract fin\n");
	//for (auto itr = muon_base.begin(); itr != muon_base.end(); itr++) {
	//	printf("gid=%d\n", itr->first);
	//	for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
	//		printf("%3d %10d\n", itr2->first, itr2->second);
	//	}
	//}

	mfile0::read_mfile(file_in_mfile, m);
	m_out.header = m.header;

	std::vector<std::vector<mfile0::M_Chain>> all_event_cand = divide_event(m, muon_base);
	//printf("divide event fin\n");
	//printf("all event size=%d", all_event_cand.size());
	//for (int i = 0; i < 10; i++) {
	//	printf("event %d size=%d\n", all_event_cand[i].begin()->basetracks.begin()->group_id / 100000, all_event_cand[i].size());
	//	for (int j = 0; j < 10; j++) {
	//		printf("%d %d gid=%d\n", i, j, all_event_cand[i][j].basetracks.begin()->group_id / 100000);
	//		for (auto itr = all_event_cand[i][j].basetracks.begin(); itr != all_event_cand[i][j].basetracks.end(); itr++) {
	//			printf("%d %d %d\n", itr->group_id / 100000, itr->pos, itr->rawid);
	//		}
	//	}
	//}
	int64_t gid;
	for (int i = 0; i < all_event_cand.size(); i++) {
		gid = all_event_cand[i].begin()->basetracks.begin()->group_id / 100000;
		printf("\r event %d/%d (ID=%d)", i, all_event_cand.size(), gid);
		std::vector<mfile0::M_Chain> c = group_clustering(all_event_cand[i]);
		//muon trackを入れる
		if (mu_map.count(gid) == 0) {
			printf("muon not found\n");
		}
		else {
			c.push_back(mu_map.at(gid));
		}
		//m_outにすべてpush back
		for (auto itr = c.begin(); itr != c.end(); itr++) {
			m_out.chains.push_back(*itr);
		}
		sort(c.begin(), c.end(), sort_chain);
		mfile0::Mfile m_out_one;
		m_out_one.header = m.header;
		m_out_one.chains = c;

		std::stringstream file_out_mfile_event;
		file_out_mfile_event << file_out_mfile_path << "\\event_" << std::setw(10) << std::setfill('0') << gid << ".all";

		mfile0::write_mfile(file_out_mfile_event.str(), m_out_one,0);

	}
	sort(m_out.chains.begin(), m_out.chains.end(), sort_chain);
	mfile0::write_mfile(file_out_mfile, m_out);



}
std::map<int64_t, mfile0::M_Chain> Set_muon_chain_map(std::vector<mfile0::M_Chain>&c) {
	std::map<int64_t, mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		ret.insert(std::make_pair(itr->basetracks.begin()->group_id / 100000, *itr));
	}
	return ret;
}

std::map<int64_t, std::set<std::pair<int, int>>> extract_muon_base(std::vector<mfile0::M_Chain>&c) {
	std::map<int64_t, std::set<std::pair<int, int>>> ret;
	int64_t gid = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			gid = itr2->group_id / 100000;
			if (ret.count(gid) == 0) {
				std::set < std::pair<int, int> >pos_raw;
				pos_raw.insert(std::make_pair(itr2->pos, itr2->rawid));
				ret.insert(std::make_pair(gid, pos_raw));
			}
			else {
				auto res = ret.find(gid);
				res->second.insert(std::make_pair(itr2->pos, itr2->rawid));
			}
		}		
	}
	return ret;
}
std::vector<std::vector<mfile0::M_Chain>> divide_event(mfile0::Mfile &m, std::map<int64_t, std::set<std::pair<int, int>>>&muon) {
	std::vector<std::vector<mfile0::M_Chain>> ret;
	std::multimap<int64_t, mfile0::M_Chain> chain_map;
	int64_t gid;
	bool flg = false;
	int all = m.chains.size(), num = 0;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (num % 10000 == 0) {
			printf("\r now %d/%d group divide1", num, all);
		}
		num++;
		gid = itr->basetracks.begin()->group_id / 100000;
		auto res = muon.find(gid);
		if (res == muon.end()) {
			printf("muon id=%d not found\n", gid);
			continue;
		}
		std::set<std::pair<int, int>> pos_raw = res->second;
		mfile0::M_Chain c;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			//muon chainはよける
			if (pos_raw.count(std::make_pair(itr2->pos, itr2->rawid)) == 1) {
				continue;
			}
			c.basetracks.push_back(*itr2);
		}
		if (c.basetracks.size() > 0) {
			c.chain_id = itr->chain_id;
			c.nseg = c.basetracks.size();
			c.pos0 = c.basetracks.begin()->pos;
			c.pos1 = c.basetracks.rbegin()->pos;

			chain_map.insert(std::make_pair(gid, c));
		}
	}
	printf("\r now %d/%d group divide1\n", num, all);

	int count = 0;
	all = chain_map.size();
	num = 0;
	for (auto itr = chain_map.begin(); itr != chain_map.end(); itr++) {

		count = chain_map.count(itr->first);
		auto range = chain_map.equal_range(itr->first);
		std::vector<mfile0::M_Chain> chain_v;
		for (auto res = range.first; res != range.second; res++) {
			if (num % 10000 == 0) {
				printf("\r now %d/%d group divide2", num, all);
			}
			num++;

			chain_v.push_back(res->second);
		}
		ret.push_back(chain_v);
		itr = std::next(itr, count - 1);
	}
	printf("\r now %d/%d group divide2\n", num, all);

	return ret;
}

std::vector<mfile0::M_Chain> group_clustering(std::vector<mfile0::M_Chain>&c) {
	std::multimap<int64_t, mfile0::M_Base> group;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			group.insert(std::make_pair(itr2->group_id, *itr2));
		}
	}

	std::vector<mfile0::M_Chain> ret;
	for (auto itr = group.begin(); itr != group.end(); itr++) {
		int count = group.count(itr->first);
		mfile0::M_Chain c;
		auto range = group.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			c.basetracks.push_back(res->second);
		}
		sort(c.basetracks.begin(), c.basetracks.end(), sort_base);
		c.nseg = c.basetracks.size();
		c.pos0 = c.basetracks.begin()->pos;
		c.pos1 = c.basetracks.rbegin()->pos;
		c.chain_id = c.basetracks.begin()->group_id;
		ret.push_back(c);
		itr = std::next(itr, count - 1);
	}
	return ret;
}