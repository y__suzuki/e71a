#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <fstream>
#include <algorithm>
#include <map>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <omp.h>
#include <chrono>
class Ranking_chi2 {
public:
	double angle,chi2;
	int vph;
};
class Ranking_chi2_plot {
public:
	double ang_min, ang_max;
	double chi_min, chi_max, vph_min, vph_max;
	int x_bin, y_bin;
	std::map<std::pair<int, int>, std::tuple<double, double, double, int>> val;
};
class Cut_param {
public:
	double ang_min, ang_max;
	std::pair<double, double> p[3];
	double eff;
	int count;
};
bool sort_chi2(const Ranking_chi2 &left, const Ranking_chi2 &right) {
	return left.chi2 > right.chi2;
}
bool sort_vph(const Ranking_chi2 &left, const Ranking_chi2 &right) {
	return left.vph < right.vph;
}
bool sort_cut_param(const Cut_param &left, const Cut_param & right) {
	if (left.count != right.count) {
		return left.count < right.count;
	}
	else if (left.p[1].second != right.p[1].second) {
		return left.p[1].second < right.p[1].second;
	}
	else {
		return left.eff > right.eff;
	}
}
bool sort_cut_param_angle(const Cut_param &left, const Cut_param & right) {
	return left.ang_min < right.ang_min;
}

std::pair<double, double> get_angle_acc(std::string filename, int pl, int area);
std::vector<Ranking_chi2> clac_chi2(std::vector<vxx::base_track_t> &base, std::pair<double, double>&param);
std::vector<Ranking_chi2> read_clac_chi2(std::string filename, int pl, std::pair<double, double>&param);

std::vector<Ranking_chi2> rank_anglecut(std::vector<Ranking_chi2>&rank, double angle_min, double angle_max);

std::vector<Ranking_chi2> rank_anglecut(std::vector<Ranking_chi2>&rank, double angle_min, double angle_max);
double get_cut_chi2(std::vector<Ranking_chi2>&rank, double efficiency);
double get_cut_vph(std::vector<Ranking_chi2>&rank, double efficiency);
double get_vph_median(std::vector<Ranking_chi2>&rank);
double get_max_chi2(std::vector<Ranking_chi2>&rank);
double get_min_vph(std::vector<Ranking_chi2>&rank);

Cut_param cut_point(std::vector<Ranking_chi2>&rank, std::vector<Ranking_chi2>&rank_all, double vph_cut, double chi2_cut, double min_eff);
void cut_point_SN(std::vector<Ranking_chi2>&rank, std::vector<Ranking_chi2>&rank_all, double vph_cut, double chi2_cut, double min_eff, Ranking_chi2_plot &plot);
inline bool judege_hit(Ranking_chi2 &t, double x0, double &y0, double &x1, double &y1, double &x2, double &y2);
void output_cut_param(std::string filename, std::vector<Cut_param> param);
std::vector<Ranking_chi2> cut_noise(std::vector<Ranking_chi2>&rank, double chi_min, double chi_max, double vph_min, double vph_max);
int use_thread(double ratio, bool output);

int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:prg in-bvxx-connect in-bvxx-all pl area in-angle-acc out-cut-param\n");
		exit(1);
	}

	std::string file_in_bvxx_connect = argv[1];
	std::string file_in_bvxx_all = argv[2];
	int pl = std::stoi(argv[3]);
	int area = std::stoi(argv[4]);
	std::string file_in_angle_acc = argv[5];
	std::string file_out_cutparam = argv[6];
	
	std::pair<double, double> param = get_angle_acc(file_in_angle_acc, pl, area);
	vxx::BvxxReader br;

	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx_connect, pl, 0);
	std::vector<Ranking_chi2>ranking = clac_chi2(base, param);
	base.clear();
	base.shrink_to_fit();

	std::vector<Ranking_chi2>ranking_all = read_clac_chi2(file_in_bvxx_all, pl, param);
	//std::vector<vxx::base_track_t> base_all = br.ReadAll(file_in_bvxx_all, pl, 0);
	//std::vector<Ranking_chi2>ranking_all = clac_chi2(base_all, param);
	//base_all.clear();
	//base_all.shrink_to_fit();

	//std::ofstream ofs("debug.txt");

	std::vector<Cut_param> param_all;
	std::vector<Ranking_chi2> rank_cut,rank_cut_all;
	double chi2_cut, vph_cut;
	double ang_min=0, ang_max=0, offset=0;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 6; j++) {
			offset = ang_max;

			ang_min = offset;
			ang_max = (i + 1) * 0.1 + offset;
			printf("%.1lf - %.1lf\n", ang_min, ang_max);
			rank_cut = rank_anglecut(ranking, ang_min, ang_max);
			if (rank_cut.size() < 100)continue;
			rank_cut_all = rank_anglecut(ranking_all, ang_min, ang_max);
			chi2_cut = get_cut_chi2(rank_cut, 0.995);
			//vph_cut = get_cut_vph(rank_cut, 0.99);
			//chi2_cut = get_max_chi2(rank_cut);
			vph_cut = get_min_vph(rank_cut);
			
			Cut_param param=cut_point(rank_cut, rank_cut_all, vph_cut, chi2_cut, 0.998);
			param.ang_min = ang_min;
			param.ang_max = ang_max;
			param_all.push_back(param);
			
			////デバッグ用出力
			//Ranking_chi2_plot plot;
			//plot.ang_min = ang_min;
			//plot.ang_max = ang_max;
			//cut_point_SN(rank_cut, rank_cut_all, vph_cut, chi2_cut, 0.99, plot);
			//ofs << std::right << std::fixed
			//	<< std::setw(8) << std::setprecision(4) << plot.ang_min << " "
			//	<< std::setw(8) << std::setprecision(4) << plot.ang_max << " "
			//	<< std::setw(8) << std::setprecision(4) << plot.chi_min << " "
			//	<< std::setw(8) << std::setprecision(4) << plot.chi_max << " "
			//	<< std::setw(8) << std::setprecision(4) << plot.vph_min << " "
			//	<< std::setw(8) << std::setprecision(4) << plot.vph_max << " "
			//	<< std::setw(5) << std::setprecision(0) << plot.x_bin << " "
			//	<< std::setw(5) << std::setprecision(0) << plot.y_bin << " "
			//	<< std::setw(8) << std::setprecision(0) << plot.val.size() << std::endl;

			//for (auto itr = plot.val.begin(); itr != plot.val.end(); itr++) {

			//	ofs << std::right << std::fixed
			//		<< std::setw(5) << std::setprecision(0) << itr->first.first << " "
			//		<< std::setw(5) << std::setprecision(0) << itr->first.second << " "
			//		<< std::setw(8) << std::setprecision(4) << std::get<0>(itr->second) << " "
			//		<< std::setw(8) << std::setprecision(4) << std::get<1>(itr->second) << " "
			//		<< std::setw(8) << std::setprecision(4) << std::get<2>(itr->second) << " "
			//		<< std::setw(8) << std::setprecision(0) << std::get<3>(itr->second) << std::endl;
			//}
		}
	}

	output_cut_param(file_out_cutparam, param_all);

}
std::pair<double, double> get_angle_acc(std::string filename, int pl, int area) {
	std::ifstream ifs(filename.c_str());
	int pl_tmp, area_tmp;
	double dx, dz;
	std::pair<double, double>param;
	bool flg = false;
	while (ifs >> pl_tmp >> area_tmp >> dx >> dz) {
		if (pl_tmp == pl && area_tmp == area) {
			flg = true;
			param.first = dx;
			param.second = dz;
			break;
		}
	}
	if (!flg) {
		fprintf(stderr, "angle acc parameter not found\n");
		exit(1);
	}
	return param;

}
std::vector<Ranking_chi2> clac_chi2(std::vector<vxx::base_track_t> &base, std::pair<double, double>&param) {

	std::vector<Ranking_chi2> ret;
	ret.reserve(base.size());

	Ranking_chi2 chi2_tmp;
	double angle, dlat0, dlat1, drad0, drad1, sigma_lat, sigma_rad, chi;
	int vph;
	for (auto itr = base.begin(); itr != base.end(); itr++) {

		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		dlat0 = ((itr->m[0].ax - itr->ax)*itr->ay - (itr->m[0].ay - itr->ay)*itr->ax) / angle;
		dlat1 = ((itr->m[1].ax - itr->ax)*itr->ay - (itr->m[1].ay - itr->ay)*itr->ax) / angle;

		drad0 = ((itr->m[0].ax - itr->ax)*itr->ax + (itr->m[0].ay - itr->ay)*itr->ay) / angle;
		drad1 = ((itr->m[1].ax - itr->ax)*itr->ax + (itr->m[1].ay - itr->ay)*itr->ay) / angle;

		sigma_lat = sqrt(2) / 60 * param.first;
		sigma_rad = sqrt(2) / 60 * sqrt(param.first*param.first + angle * angle*param.second*param.second);

		chi2_tmp.chi2 = pow(dlat0 / sigma_lat, 2) + pow(dlat1 / sigma_lat, 2) + pow(drad0 / sigma_rad, 2) + pow(drad1 / sigma_rad, 2);
		chi2_tmp.vph = (itr->m[0].ph + itr->m[1].ph) % 10000;

		chi2_tmp.angle = angle;

		ret.push_back(chi2_tmp);

	}

	return ret;
}
std::vector<Ranking_chi2> read_clac_chi2(std::string filename,int pl, std::pair<double, double>&param) {
	//stringとPlを入力
	std::vector<Ranking_chi2> ret;

	//bvxx xx byte/track
	//1Mtrack=xxGB
	int read_track_num = (10 * 1000 * 1000);
	int id = 0;
	int t_num = -1;
	std::vector<vxx::base_track_t> base;
	base.reserve(read_track_num);
	vxx::BvxxReader br;
	Ranking_chi2 chi2_tmp;
	double angle, dlat0, dlat1, drad0, drad1, sigma_lat, sigma_rad, chi;
	int vph;
	int64_t count = 0, all = base.size();

	while (t_num != 0) {
		std::array<int, 2> index = { id*read_track_num, (id + 1)*read_track_num };//1234<=rawid<=5678であるようなものだけを読む。
		base = br.ReadAll(filename, pl, 0, vxx::opt::index = index);
		t_num = base.size();
		printf("base trans %lld\n", count);
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			count++;

			angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
			dlat0 = ((itr->m[0].ax - itr->ax)*itr->ay - (itr->m[0].ay - itr->ay)*itr->ax) / angle;
			dlat1 = ((itr->m[1].ax - itr->ax)*itr->ay - (itr->m[1].ay - itr->ay)*itr->ax) / angle;

			drad0 = ((itr->m[0].ax - itr->ax)*itr->ax + (itr->m[0].ay - itr->ay)*itr->ay) / angle;
			drad1 = ((itr->m[1].ax - itr->ax)*itr->ax + (itr->m[1].ay - itr->ay)*itr->ay) / angle;

			sigma_lat = sqrt(2) / 60 * param.first;
			sigma_rad = sqrt(2) / 60 * sqrt(param.first*param.first + angle * angle*param.second*param.second);

			chi2_tmp.chi2 = pow(dlat0 / sigma_lat, 2) + pow(dlat1 / sigma_lat, 2) + pow(drad0 / sigma_rad, 2) + pow(drad1 / sigma_rad, 2);
			chi2_tmp.vph = (itr->m[0].ph + itr->m[1].ph) % 10000;

			chi2_tmp.angle = angle;

			ret.push_back(chi2_tmp);
		}
		base.clear();
		id++;
	}

	printf("base trans %lld\n", count);

	return ret;
}
std::vector<Ranking_chi2> rank_anglecut(std::vector<Ranking_chi2>&rank, double angle_min, double angle_max) {
	std::vector<Ranking_chi2> ret;
	for (auto itr = rank.begin(); itr != rank.end(); itr++) {
		if (itr->angle < angle_min)continue;
		if (itr->angle >=angle_max)continue;
		ret.push_back(*itr);
	}
	return ret;
}

double get_cut_chi2(std::vector<Ranking_chi2>&rank, double efficiency) {
	//efficiecnyを切る直前のchi2を返す

	int all = rank.size();
	int num = all * (1 - efficiency);

	sort(rank.begin(), rank.end(), sort_chi2);
	auto itr = rank.begin();
	itr = std::next(itr, num - 1);
	return std::max(20., itr->chi2);
}
double get_cut_vph(std::vector<Ranking_chi2>&rank, double efficiency) {
	//efficiecnyを切る直前のvphを返す

	int all = rank.size();
	int num = all * (1 - efficiency);

	sort(rank.begin(), rank.end(), sort_vph);
	auto itr = rank.begin();
	itr = std::next(itr, num - 1);
	return itr->vph;
}
double get_max_chi2(std::vector<Ranking_chi2>&rank) {

	sort(rank.begin(), rank.end(), sort_chi2);
	auto itr = rank.begin();
	return std::max(20., itr->chi2);
}
double get_min_vph(std::vector<Ranking_chi2>&rank) {
	//efficiecnyを切る直前のvphを返す

	sort(rank.begin(), rank.end(), sort_vph);
	auto itr = rank.begin();
	return itr->vph;
}
double get_vph_median(std::vector<Ranking_chi2>&rank) {
	sort(rank.begin(), rank.end(), sort_vph);
	int all = rank.size();
	int num = all * 0.5;

	auto itr = rank.begin();
	itr = std::next(itr, num);
	return itr->vph;
}

Cut_param cut_point(std::vector<Ranking_chi2>&rank, std::vector<Ranking_chi2>&rank_all, double vph_cut, double chi2_cut, double min_eff) {
	double vph_max = get_vph_median(rank);

	std::chrono::system_clock::time_point start, p0, p1, p2, end;
	start = std::chrono::system_clock::now();


	//signal region決定
	std::vector<Ranking_chi2>rank_cut;
	for (auto itr = rank.begin(); itr != rank.end(); itr++) {
		if (itr->vph < vph_cut - 0.1)continue;
		if (itr->chi2 > chi2_cut)continue;
		rank_cut.push_back(*itr);
	}
	const int all = rank_cut.size();
	int count = 0;
	////parameterの探索範囲は広げる
	//vph_cut -= 2;
	//chi2_cut += 2;

	int xbin = 100, ybin = 50;
	xbin = std::min(int(chi2_cut - 4), xbin);
	ybin = std::min(int(vph_max - vph_cut), ybin);
	double vph_min,chi_max;
	std::vector<Cut_param> point_v;
	std::pair<double, double> point;
	for (int i = 0; i <= 2; i++) {
		vph_min = vph_cut - i;
		for (int j = 0; j <= 1; j++) {
			chi_max = chi2_cut + 2 * j;
			double x_step = (chi_max - 4.) / xbin;
			double y_step = (vph_max - vph_min) / ybin;
			for (int ix = 1; ix < xbin; ix++) {
				for (int iy = 1; iy < ybin; iy++) {
					point.first = ix * x_step + 4;
					point.second = iy * y_step + vph_min;
					//対角線より上ならcut
					if (point.second > (vph_max - vph_min) / (chi_max - 4) * (point.first - 4) + vph_min)continue;
					Cut_param param;
					param.p[0].first = 4;
					param.p[0].second = vph_min;
					param.p[1].first = point.first;
					param.p[1].second = point.second;
					param.p[2].first = chi_max;
					param.p[2].second = vph_max;
					param.count = 0;
					param.eff = 0;
					point_v.push_back(param);
				}
			}

		}
	}
	rank_all = cut_noise(rank_all, 0, chi2_cut + 2, vph_min - 2, vph_max);
	std::vector<Cut_param> point_v2;

	p0= std::chrono::system_clock::now();

	//pararel
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(guided)
	for(int i=0;i<point_v.size();i++){
	//for (auto itr = point_v.begin(); itr != point_v.end(); itr++) {
		int sig_count = 0;
		for (auto itr2 = rank_cut.begin(); itr2 != rank_cut.end(); itr2++) {
			if (judege_hit(*itr2, point_v[i].p[0].first, point_v[i].p[0].second, point_v[i].p[1].first, point_v[i].p[1].second, point_v[i].p[2].first, point_v[i].p[2].second)) {
				sig_count++;
			}
		}
		point_v[i].eff = sig_count * 1. / all;
		//min_effを超えるefficiencyのpointを選別
		if (point_v[i].eff >= min_eff) {
#pragma omp critical
			point_v2.push_back(point_v[i]);
		}
	}
	if (point_v2.size() == 0) {
		fprintf(stderr, "Efficiecny satisfied point not found(Efficiency>=%.4lf)", min_eff);
		exit(1);
	}


	p1= std::chrono::system_clock::now();
	//残った中でnoiseが少ないものを選別
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(guided)
	for (int i = 0; i < point_v2.size(); i++) {
		int noise_count = 0;
		for (auto itr2 = rank_all.begin(); itr2 != rank_all.end(); itr2++) {
			if (judege_hit(*itr2, point_v2[i].p[0].first, point_v2[i].p[0].second, point_v2[i].p[1].first, point_v2[i].p[1].second, point_v2[i].p[2].first, point_v2[i].p[2].second)) {
				noise_count++;
			}
		}
		point_v2[i].count = noise_count;
	}

	std::sort(point_v2.begin(), point_v2.end(), sort_cut_param);


	end = std::chrono::system_clock::now();

	double time0 = static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(p0- start).count() / 1000.0);
	double time1 = static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(p1 - p0).count() / 1000.0);
	double time2 = static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(end - p1).count() / 1000.0);
	//printf("time0 %lf[ms]\n", time0);
	//printf("time1 %lf[ms]\n", time1);
	//printf("time2 %lf[ms]\n", time2);


	return *(point_v2.begin());
}
void cut_point_SN(std::vector<Ranking_chi2>&rank, std::vector<Ranking_chi2>&rank_all, double vph_cut, double chi2_cut, double min_eff, Ranking_chi2_plot &plot) {
	
	double vph_max = get_vph_median(rank);
	std::vector<Ranking_chi2>rank_cut;
	for (auto itr = rank.begin(); itr != rank.end(); itr++) {
		if (itr->vph < vph_cut - 0.1)continue;
		if (itr->chi2 > chi2_cut)continue;
		rank_cut.push_back(*itr);
	}
	const int all = rank_cut.size();
	int count = 0;
	//parameterの探索範囲は広げる
	vph_cut -= 2;
	chi2_cut += 2;
	int xbin = 100, ybin = 50;
	double x_step = (chi2_cut - 4.) / xbin;
	double y_step = (vph_max - vph_cut) / ybin;
	std::vector<std::pair<double, double>> point_v;
	std::pair<double, double> point;
	std::map<std::pair<int, int>, std::tuple<double, double, double, int>> val;

	for (int ix = 0; ix <= xbin; ix++) {
		for (int iy = 0; iy <= ybin; iy++) {
			point.first = ix * x_step + 4;
			point.second = iy * y_step + vph_cut;
			//対角線より上ならcut
			if (point.second > (vph_max - vph_cut) / chi2_cut * point.first + vph_cut)continue;
			//if (point.first <4)continue;
			//point_v.push_back(point);
			val.insert(std::make_pair(std::make_pair(ix, iy), std::make_tuple(point.first, point.second, 0, 0)));
		}
	}
	
	for (auto itr = val.begin(); itr != val.end(); itr++) {
		count = 0;
		for (auto itr2 = rank_cut.begin(); itr2 != rank_cut.end(); itr2++) {
			if (judege_hit(*itr2, 4, vph_cut, std::get<0>(itr->second), std::get<1>(itr->second), chi2_cut, vph_max))count++;
		}
		std::get<2>(itr->second) = count * 1. / all;
	}

	//残った中でnoiseが少ないものを選別
	for (auto itr = val.begin(); itr != val.end(); itr++) {
		count = 0;
		for (auto itr2 = rank_all.begin(); itr2 != rank_all.end(); itr2++) {
			if (judege_hit(*itr2, 4, vph_cut, std::get<0>(itr->second), std::get<1>(itr->second), chi2_cut, vph_max))count++;
		}
		std::get<3>(itr->second) = count;
	}

	plot.vph_min = vph_cut;
	plot.vph_max = vph_max;
	plot.chi_min = 4;
	plot.chi_max= chi2_cut;
	plot.x_bin = xbin;
	plot.y_bin = ybin;
	plot.val = val;
}
bool judege_hit(Ranking_chi2 &t, double x0, double &y0, double &x1, double &y1, double &x2, double &y2) {
	if (t.chi2 <= x0)return t.vph >= y0;
	else if (t.chi2 < x1) return t.vph >= (y1 - y0) / (x1 - x0)*(t.chi2 - x0) + y0;
	else if (t.chi2 < x2) return t.vph >= (y2 - y1) / (x2 - x1)*(t.chi2 - x1) + y1;
	else return t.vph >= y2;
}

std::vector<Ranking_chi2> cut_noise(std::vector<Ranking_chi2>&rank, double chi_min,double chi_max, double vph_min,double vph_max) {
	std::vector<Ranking_chi2> ret;
	for (auto itr = rank.begin(); itr != rank.end(); itr++) {
		if (itr->chi2 < chi_min)continue;
		if (itr->chi2 > chi_max)continue;
		if (itr->vph < vph_min)continue;
		if (itr->vph > vph_max)continue;
		ret.push_back(*itr);
	}
	return ret;
}
void output_cut_param(std::string filename, std::vector<Cut_param> param) {
	sort(param.begin(), param.end(), sort_cut_param_angle);
	std::ofstream ofs(filename);
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(5) << std::setprecision(2) << itr->ang_min << " "
			<< std::setw(5) << std::setprecision(2) << itr->ang_max << " "
			<< std::setw(7) << std::setprecision(3) << itr->p[0].first << " "
			<< std::setw(7) << std::setprecision(3) << itr->p[0].second << " "
			<< std::setw(7) << std::setprecision(3) << itr->p[1].first << " "
			<< std::setw(7) << std::setprecision(3) << itr->p[1].second << " "
			<< std::setw(7) << std::setprecision(3) << itr->p[2].first << " "
			<< std::setw(7) << std::setprecision(3) << itr->p[2].second << " "
			<< std::setw(5) << std::setprecision(4) << itr->eff << " "
			<< std::setw(12) << std::setprecision(0) << itr->count << std::endl;
	}
}
int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
