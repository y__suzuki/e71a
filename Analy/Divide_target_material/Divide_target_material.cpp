#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Event_information>extract_water(std::vector<Momentum_recon::Event_information>&momch_ev);
std::vector<Momentum_recon::Event_information>extract_iron(std::vector<Momentum_recon::Event_information>&momch_ev);

std::vector<Momentum_recon::Mom_chain > format_change(std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&mom_ev);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-momch file-out\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch_name = argv[2];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	std::vector<Momentum_recon::Event_information>momch_water = extract_water(momch);
	std::vector<Momentum_recon::Event_information>momch_iron = extract_iron(momch);

	Momentum_recon::Write_Event_information_extension(file_out_momch_name + "_water.momch", momch_water);
	Momentum_recon::Write_Event_information_extension(file_out_momch_name + "_iron.momch", momch_iron);
}
std::vector<Momentum_recon::Event_information>extract_water(std::vector<Momentum_recon::Event_information>&momch_ev) {
	std::vector<Momentum_recon::Event_information>ret;
	int all = momch_ev.size();
	int vertex_pl;
	for (auto &ev : momch_ev) {
		vertex_pl = ev.vertex_pl;
		if (vertex_pl >= 16 && vertex_pl % 2 == 1) {
			ret.push_back(ev);
		}
	}
	printf("water interaction %d --> %d\n", all, ret.size());
	return ret;
}
std::vector<Momentum_recon::Event_information>extract_iron(std::vector<Momentum_recon::Event_information>&momch_ev) {
	std::vector<Momentum_recon::Event_information> ret;
	int all = momch_ev.size();
	int vertex_pl;
	for (auto &ev : momch_ev) {
		vertex_pl = ev.vertex_pl;
		if (vertex_pl >= 16 && vertex_pl % 2 == 0) {
			ret.push_back(ev);
		}
	}
	printf("iron  interaction %d --> %d\n", all, ret.size());
	return ret;
}


std::vector<Momentum_recon::Mom_chain > format_change(std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&mom_ev) {
	std::vector<Momentum_recon::Mom_chain > ret;
	for (auto itr = mom_ev.begin(); itr != mom_ev.end(); itr++) {
		ret.push_back(itr->first);
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (itr2->base.size() > 0) {
				ret.push_back(*itr2);
			}

		}
	}
	return ret;

}