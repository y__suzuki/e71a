#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>

using namespace l2c;

class Sharing_file {
public:
	int32_t pl, ecc_id, fixedwall_id, trackerwall_id, spotid, zone[2], rawid[2], unix_time, tracker_track_id;
	//spotid:spotA * 100 + spotB
};

class Chain_baselist {
public:
	int64_t groupid;
	std::set<std::pair<int32_t, int64_t>> btset;
	std::set<std::tuple<int,int,int,int>>ltlist;
	std::vector<int32_t> usepos;

	std::vector<Linklet> make_link_list();
	void set_usepos();
};

class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};



std::vector<Chain_baselist> read_mfile(std::string filename);

void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs);


int64_t read_linklet_list_001to133(std::string filename, std::vector<Chain_baselist>&chain, int pl_min, int pl_max);
int64_t read_linklet_list_133to001(std::string filename, std::vector<Chain_baselist>&chain, int pl_min, int pl_max);

std::multimap<int, mfile0::M_Base> Chain_base(std::vector<Chain_baselist> &chain_list, std::map<int, double> &z_map);

void basetrack_pick_up(std::string file_path, int pl, std::vector<mfile0::M_Base*>&base_p);
void basetrack_pick_up2(std::string file_path, int pl, std::vector<mfile0::M_Base*>&base_p);

void basetrack_mfile_trans(std::vector<mfile0::M_Base*>&base_p, int pl, std::vector<corrmap0::Corrmap>&corr_abs);

mfile0::Mfile basetrack2chian(std::multimap<int, mfile0::M_Base> &chains);
int64_t Linklet_header_num(std::string filename);
void output_Saringfile_w_gid(std::string filename, std::vector<Chain_baselist>&gsf);

void read_linklet_list(std::string filename, std::multimap<std::pair<short, int>, std::pair<short, int>> &link_id_to_up, std::multimap<std::pair<short, int>, std::pair<short, int>> &link_id_to_down);
int64_t make_chain_001to133(std::multimap<std::pair<short, int>, std::pair<short, int>> &linked_id, std::vector<Chain_baselist>&chain, int pl_min, int pl_max);
int64_t make_chain_133to001(std::multimap<std::pair<short, int>, std::pair<short, int>> &linked_id, std::vector<Chain_baselist>&chain, int pl_min, int pl_max);
void l2c_all(std::vector<Chain_baselist> &chain_list);
void l2c_x(std::set<std::pair<int32_t, int64_t>> btset, std::vector<Linklet> ltlist, std::vector<int32_t> usepos);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-mfile ECC-Area-path mode output-mfile linklet-header-list(bin)\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_ECC = argv[2];
	int mode = std::stoi(argv[3]);
	std::string file_out_mfile = argv[4];
	std::string file_in_link = argv[5];

	std::string file_in_corrabs = file_in_ECC + "\\0\\align\\corrmap-abs.lst";

	if (mode != -1 && mode != 1 && mode != 2 && mode != 3) {
		fprintf(stderr, "mode exception\n");
		fprintf(stderr, "mode=-1 PL133 --> PL001 follow down\n");
		fprintf(stderr, "mode= 1 PL001 --> PL133 scan back\n");
		fprintf(stderr, "mode= 2 PL001 --> PL133 -->PL001\n");
		fprintf(stderr, "mode= 3 PL001 --> PL133 -->PL001--->:endless\n");
		exit(1);
	}

	//corrmap absの読み込み
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrabs, corr_abs);

	//mfileの読み込み
	std::vector<Chain_baselist> chain_list = read_mfile(file_in_mfile);
	printf("num of track %d\n", chain_list.size());
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	z_map_correction(z_map, corr_abs);

	//全linkletの読み込み
	std::multimap<std::pair<short, int>, std::pair<short, int>> link_id_to_up, link_id_to_down;
	read_linklet_list(file_in_link, link_id_to_up, link_id_to_down);

	//全linkletからchainを再作成
	int64_t base_num = 0;
	if (mode == 3) {
		bool flg = true;
		int64_t base_num_prev = 0;
		while (flg) {
			base_num = make_chain_001to133(link_id_to_up, chain_list, 1, 133);
			base_num = make_chain_133to001(link_id_to_down, chain_list, 1, 133);
			printf("base_num=%lld\n", base_num);
			if (base_num == base_num_prev)flg = false;
			base_num_prev = base_num;
		}
	}
	else {
		if (mode == 1 || mode == 2) {
			base_num = make_chain_001to133(link_id_to_up, chain_list, 1, 133);
		}
		if (mode == -1 || mode == 2) {
			base_num = make_chain_133to001(link_id_to_down, chain_list, 1, 133);
		}
	}

	//メモリ開放
	link_id_to_up.clear();
	link_id_to_down.clear();

	//linkletの組からchainに変換
	l2c_all(chain_list);

	return 0;


	//chainを構成するbasetrackを読み出し-->mfile座標系に変換
	std::map<int, int64_t> gid_cid;
	std::multimap<int, mfile0::M_Base> basetracks = Chain_base(chain_list, z_map);
	int count = 0;
	for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
		count = basetracks.count(itr->first);
		std::vector<mfile0::M_Base*> base_p;
		auto range = basetracks.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_p.push_back(&(res->second));
		}
		basetrack_pick_up2(file_in_ECC, itr->first, base_p);
		basetrack_mfile_trans(base_p, itr->first, corr_abs);

		itr = std::next(itr, count - 1);
	}

	std::multimap<int, mfile0::M_Base> chains;
	for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
		chains.insert(std::make_pair(itr->second.group_id, itr->second));
	}

	mfile0::Mfile m = basetrack2chian(chains);

	mfile0::write_mfile(file_out_mfile, m);

}

std::vector<Chain_baselist> read_mfile(std::string filename) {
	mfile0::Mfile m;
	mfile1::read_mfile_extension(filename, m);

	std::multimap<int64_t, mfile0::M_Base> group;
	std::map<std::pair<int64_t, int>, mfile0::M_Base> group_map;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			group_map.insert(std::make_pair(std::make_pair(itr2->group_id, itr2->rawid), *itr2));
		}
	}
	for (auto itr = group_map.begin(); itr != group_map.end(); itr++) {
		group.insert(std::make_pair(itr->first.first, itr->second));
	}
	std::vector<Chain_baselist> ret;
	int count = 0;
	for (auto itr = group.begin(); itr != group.end(); itr++) {
		count = group.count(itr->first);

		Chain_baselist c;
		c.groupid = itr->second.group_id;
		auto range = group.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			c.btset.insert(std::make_pair(res->second.pos, res->second.rawid));
		}
		ret.push_back(c);

		itr = std::next(itr, count - 1);
	}
	return ret;
}

void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs) {

	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		auto res = z_map.find(pl);
		if (res == z_map.end())continue;
		res->second += itr->dz;
	}

}

void read_linklet_list(std::string filename, std::multimap<std::pair<short, int>, std::pair<short, int>> &link_id_to_up, std::multimap<std::pair<short, int>, std::pair<short, int>> &link_id_to_down) {
	int64_t link_num = Linklet_header_num(filename);
	FILE*fp_in;
	if ((fp_in = fopen(filename.c_str(), "rb")) == NULL) {
		printf("%s file not open!\n", filename.c_str());
		exit(EXIT_FAILURE);
	}

	int count = 0;
	const int read_num_max = 10000;
	//std::vector<linklet_header> link;
	//link.reserve(read_num_max);
	linklet_header link[read_num_max];
	int64_t  now = 0;
	int read_num;
	bool flg = true;
	while (flg) {
		if (link_num - now == read_num_max) {
			read_num = read_num_max;
			flg = false;
		}
		else if (link_num - now < read_num_max) {
			read_num = link_num - now;
			flg = false;
		}
		else {
			read_num = read_num_max;
		}
		fread(&link, sizeof(linklet_header), read_num, fp_in);
		for (int i = 0; i < read_num; i++) {
			//printf("%d %d %d %d\n", link[i].pos0, link[i].raw0, link[i].pos1, link[i].raw1);
			link_id_to_up.insert(std::make_pair(std::make_pair(link[i].pos0+1 , link[i].raw0), std::make_pair(link[i].pos1+1 , link[i].raw1)));
			link_id_to_down.insert(std::make_pair(std::make_pair(link[i].pos1+1 , link[i].raw1), std::make_pair(link[i].pos0+1 , link[i].raw0)));
		}
		if (count % 100 == 0) {
			printf("\r read fin %12lld/%12lld (%4.1lf%%)", now, link_num, now*100. / link_num);
		}
		now += read_num;
		count++;
	}
	printf("\r read fin %12lld/%12lld (%4.1lf%%)\n", now, link_num, now*100. / link_num);
}
int64_t make_chain_001to133(std::multimap<std::pair<short, int>, std::pair<short, int>> &linked_id, std::vector<Chain_baselist>&chain, int pl_min, int pl_max) {
	bool flg;
	int all = chain.size(), cnt = 0, plus = 0;
	std::vector<std::pair<int, int>> pair_buf;
	std::set<std::tuple<int, int, int, int>> link_buf;
	pair_buf.reserve(1000);

	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		printf("\r now search linklet %d/%d", cnt, all);
		cnt++;
		flg = true;
		while (flg) {
			flg = false;
			plus = 0;
			pair_buf.clear();
			//printf("rawid size =%d flg=%d\n", itr->rawid.size(), flg);
			for (auto itr2 = itr->btset.begin(); itr2 != itr->btset.end(); itr2++) {
				if (linked_id.count(*itr2) == 0)continue;
				auto range = linked_id.equal_range(*itr2);
				for (auto res = range.first; res != range.second; res++) {
					pair_buf.push_back(res->second);
					//pos,pos,raw,raw
					itr->ltlist.insert(std::make_tuple(res->first.first, res->second.first, res->first.second, res->second.second));
				}
			}
			for (auto itr2 = pair_buf.begin(); itr2 != pair_buf.end(); itr2++) {
				auto res = itr->btset.insert(*itr2);
				if (res.second)plus++;
			}
			if (plus != 0) {
				flg = true;
			}
			//printf("rawid size =%d flg=%d plus=%d\n", itr->rawid.size(), flg, plus);

		}
	}
	printf("\r now search linklet %d/%d\n", cnt, all);

	int64_t all_base_num = 0;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		all_base_num += itr->btset.size();
	}
	return all_base_num;
}
int64_t make_chain_133to001(std::multimap<std::pair<short, int>, std::pair<short, int>> &linked_id, std::vector<Chain_baselist>&chain, int pl_min, int pl_max) {
	bool flg;
	int all = chain.size(), cnt = 0, plus = 0;
	std::vector<std::pair<int, int>> pair_buf;
	pair_buf.reserve(1000);
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		printf("\r now search linklet %d/%d", cnt, all);
		cnt++;
		flg = true;
		while (flg) {
			flg = false;
			plus = 0;
			pair_buf.clear();
			//printf("rawid size =%d flg=%d\n", itr->rawid.size(), flg);
			//printf("\n");
			for (auto itr2 = itr->btset.rbegin(); itr2 != itr->btset.rend(); itr2++) {
				//printf("%3d %10d\n", itr2->first, itr2->second);

				if (linked_id.count(*itr2) == 0)continue;
				//挿入が発生したらbreak
				auto range = linked_id.equal_range(*itr2);
				for (auto res = range.first; res != range.second; res++) {
					pair_buf.push_back(res->second);
					itr->ltlist.insert(std::make_tuple(res->second.first, res->first.first, res->second.second, res->first.second));
				}
			}
			//printf("\n");
			for (auto itr2 = pair_buf.begin(); itr2 != pair_buf.end(); itr2++) {
				auto res = itr->btset.insert(*itr2);
				if (res.second)plus++;
			}
			if (plus != 0) {
				flg = true;
			}
			//printf("rawid size =%d flg=%d plus=%d\n", itr->rawid.size(), flg, plus);

		}
	}
	printf("\r now search linklet %d/%d\n", cnt, all);

	int64_t all_base_num = 0;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		all_base_num += itr->btset.size();
	}
	return all_base_num;
}


std::multimap<int, mfile0::M_Base> Chain_base(std::vector<Chain_baselist> &chain_list, std::map<int, double> &z_map) {
	std::multimap<int, mfile0::M_Base> ret;

	mfile0::M_Base base;
	int gid = 0;
	for (auto itr = chain_list.begin(); itr != chain_list.end(); itr++) {
		gid = itr->groupid;
		for (auto b : itr->btset) {
			base.group_id = gid;
			base.pos = b.first * 10 + 1;
			base.rawid = b.second;
			if (z_map.count(b.first) == 0) base.z = 0;
			else base.z = z_map.at(b.first);

			base.flg_d[0] = 0;
			base.flg_d[1] = 0;
			base.flg_i[0] = 0;
			base.flg_i[1] = 0;
			base.flg_i[2] = 0;
			base.flg_i[3] = 0;

			ret.insert(std::make_pair(b.first, base));
		}
		gid++;
	}
	return ret;
}

void basetrack_pick_up2(std::string file_path, int pl, std::vector<mfile0::M_Base*>&base_p) {

	std::stringstream file_in_bvxx;
	file_in_bvxx << file_path << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	if (!std::filesystem::exists(file_in_bvxx.str())) {
		fprintf(stderr, "file[%s] not exist\n", file_in_bvxx.str().c_str());
		exit(1);
	}

	printf("Read %s\n", file_in_bvxx.str().c_str());
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx.str(), pl, 0);
	std::map<int, vxx::base_track_t> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_map.insert(std::make_pair(itr->rawid, *itr));
	}
	int rawid = 0;
	for (auto itr = base_p.begin(); itr != base_p.end(); itr++) {
		auto res = base_map.find((*itr)->rawid);
		(*itr)->ax = res->second.ax;
		(*itr)->ay = res->second.ay;
		(*itr)->x = res->second.x;
		(*itr)->y = res->second.y;

		(*itr)->ph = res->second.m[0].ph + res->second.m[1].ph;

	}
}
void basetrack_pick_up(std::string file_path, int pl, std::vector<mfile0::M_Base*>&base_p) {

	std::stringstream file_in_bvxx;
	file_in_bvxx << file_path << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	if (!std::filesystem::exists(file_in_bvxx.str())) {
		fprintf(stderr, "file[%s] not exist\n", file_in_bvxx.str().c_str());
		exit(1);
	}

	printf("Read %s\n", file_in_bvxx.str().c_str());
	vxx::BvxxReader br;

	int rawid = 0;
	for (auto itr = base_p.begin(); itr != base_p.end(); itr++) {
		rawid = (*itr)->rawid;
		std::vector<vxx::base_track_t> b;
		vxx::BvxxReader br;
		std::array<int, 2> index = { rawid, rawid + 1 };//1234<=rawid<=5678であるようなものだけを読む。
		b = br.ReadAll(file_in_bvxx.str(), pl, 0, vxx::opt::index = index);
		(*itr)->ax = b.begin()->ax;
		(*itr)->ay = b.begin()->ay;
		(*itr)->x = b.begin()->x;
		(*itr)->y = b.begin()->y;

		(*itr)->ph = b.begin()->m[0].ph + b.begin()->m[1].ph;

	}
}
void basetrack_mfile_trans(std::vector<mfile0::M_Base*>&base_p, int pl, std::vector<corrmap0::Corrmap>&corr_abs) {

	corrmap0::Corrmap param;
	bool flg = false;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		if (itr->pos[0] / 10 == pl) {
			flg = true;
			param = *itr;
			break;
		}
	}
	if (!flg) {
		fprintf(stderr, "PL%03d corrmap abs not found\n", pl);
		return;
	}

	double tmp_x, tmp_y;
	for (auto itr = base_p.begin(); itr != base_p.end(); itr++) {
		tmp_x = (*itr)->x;
		tmp_y = (*itr)->y;
		(*itr)->x = tmp_x * param.position[0] + tmp_y * param.position[1] + param.position[4];
		(*itr)->y = tmp_x * param.position[2] + tmp_y * param.position[3] + param.position[5];
		tmp_x = (*itr)->ax;
		tmp_y = (*itr)->ay;
		(*itr)->ax = tmp_x * param.angle[0] + tmp_y * param.angle[1] + param.angle[4];
		(*itr)->ay = tmp_x * param.angle[2] + tmp_y * param.angle[3] + param.angle[5];
	}


}

mfile0::Mfile basetrack2chian(std::multimap<int, mfile0::M_Base> &chains) {
	//chains(gid,M_Base)
	//gid_cid(gid,chainid)

	mfile0::Mfile ret;
	int  num = 0, count = 0;
	int64_t gid;
	for (auto itr = chains.begin(); itr != chains.end(); itr++) {
		count = chains.count(itr->first);
		auto range = chains.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			mfile0::M_Chain c;
			c.basetracks.push_back(res->second);
			c.chain_id = num;
			c.nseg = c.basetracks.size();
			c.pos0 = c.basetracks.begin()->pos;
			c.pos1 = c.basetracks.rbegin()->pos;
			ret.chains.push_back(c);
			num++;
		}
		itr = std::next(itr, count - 1);
	}
	//chainを構成するbasetrackをlistにつめる

	ret.header.head[0] = "% Created by mkmf";
	ret.header.head[1] = "% on 2007 / 12 / 25 23:28 : 09 + 09:00 (JST)";
	ret.header.head[2] = "	0       0   3   0      0.0   0.0000";
	std::set<int> pos;
	for (int i = 0; i < ret.chains.size(); i++) {
		for (int j = 0; j < ret.chains[i].basetracks.size(); j++) {
			pos.insert(ret.chains[i].basetracks[j].pos);
		}
	}

	ret.header.num_all_plate = int(pos.size());

	for (auto itr = pos.begin(); itr != pos.end(); itr++) {
		ret.header.all_pos.push_back(*itr);
	}

	return ret;

}
int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}

void l2c_all(std::vector<Chain_baselist> &chain_list) {
	int count = 0;
	for (auto itr = chain_list.begin(); itr != chain_list.end(); itr++) {
		count++;
		itr->set_usepos();
		system("pause");
		printf("pos %d :", itr->usepos.size());
		for (auto itr2 = itr->usepos.begin(); itr2 != itr->usepos.end(); itr2++) {
			printf(" %4d", *itr2);
		}
		printf("\n");
		std::vector<Linklet> link = itr->make_link_list();
		for (auto itr2 = link.begin(); itr2 != link.end(); itr2++) {
			printf("%4d %4d %10d %10d\n", itr2->pos1, itr2->pos2, itr2->id1, itr2->id2);
		}
		printf("eventid = %d  %d/%d\n", itr->groupid, count, chain_list.size());
		l2c_x(itr->btset, link, itr->usepos);
	}


}
void l2c_x(std::set<std::pair<int32_t, int64_t>> btset, std::vector<Linklet> ltlist, std::vector<int32_t> usepos) {
	try
	{

		//std::vector<int32_t> usepos = { 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500 };
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = 100000, opt::output_isolated_linklet = true);

		//中身を出力する。
		size_t grsize = cdat.GetNumOfGroups();
		for (size_t grid = 0; grid < grsize; ++grid)
		{
			const Group& gr = cdat.GetGroup(grid);
			size_t chsize = gr.GetNumOfChains();
			if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
			int32_t spl = gr.GetStartPL();
			int32_t epl = gr.GetEndPL();

			//l2c-xのPLは一般に使われるPLとは意味が異なり、useposの中で"何番目のposであるか"を意味する。
			//LibL2c-xもl2c-xの仕様に倣うことにした（ただしl2c-xが1始まりなのに対してこちらは0始まり）。
			//例えば今回は、pos==310〜500の20枚でChain Groupを作成しているが、
			//GetStartPL()の戻り値が"3"であったとすると、これはこのGroupがusepos[3]すなわち380から始まっているという意味になる。

			fprintf(stdout, "Group ID:%-9llu nchain:%-7llu start:%-4d end:%-4d\n", gr.GetID(), chsize, spl, epl);
			if (gr.IsOverUpperLim())
			{
				//upperlimを超過している場合、chainの情報はない。
				//ただしchainの本数はGetNumOfChainsで正しく取得できる。
				//また属すBaseTrackの情報は保持しているので、GetBaseTracksで全BaseTrackを取得できる。
				fprintf(stdout, "over upperlim. nchain:%-9lld\n", chsize);
				continue;
			}
			for (size_t ich = 0; ich < chsize; ++ich)
			{
				Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
				int64_t chid = ch.GetID();
				int32_t nseg = ch.GetNSeg();
				int32_t spl = ch.GetStartPL();
				int32_t epl = ch.GetEndPL();
				fprintf(stdout, "    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d\n", chid, nseg, spl, epl);

				for (size_t pl = 0; pl < possize; ++pl)
				{
					//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
					//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
					BaseTrackID bt = ch.GetBaseTrack(pl);
					if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
					int32_t btpl = bt.GetPL();
					int64_t btid = bt.GetRawID();
					//btplとplは厳密に一致しなければおかしい。
					if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
					if (btset.find(std::make_pair(usepos[btpl], btid)) == btset.end())
					{
						//エラーチェック。
						//Chain内の全てのBaseTrackはbtset（ファイルから読み込まれた全BaseTrack）に必ず含まれているはず。
						//含まれていなければエラー。
						throw std::exception("BaseTrack is not found in btset.");
					}
					fprintf(stdout, "        pl:%-3d pos:%-4d rawid:%-9lld\n", btpl, usepos[pl], btid);
				}
			}

		}
	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}
std::vector<Linklet> Chain_baselist::make_link_list() {
	std::vector<Linklet> ret;
	ret.reserve(ltlist.size());
	for (auto itr = ltlist.begin(); itr != ltlist.end(); itr++) {
		ret.emplace_back(std::get<0>(*itr), std::get<1>(*itr), std::get<2>(*itr), std::get<3>(*itr));
	}
	return ret;
}
void Chain_baselist::set_usepos() {
	std::set<int> pos_set;
	for (auto itr = btset.begin(); itr != btset.end(); itr++) {
		pos_set.insert(itr->first);
	}
	usepos.clear();
	for (auto itr = pos_set.begin(); itr != pos_set.end(); itr++) {
		usepos.push_back(*itr);
	}
}
