#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void Calc_divide_vph(Momentum_recon::Mom_chain  &c, int pl, double mean[2], double mean_err[2], double sd[2], int count[2]);
void Calc_divide_pixel(Momentum_recon::Mom_chain  &c, int pl, double mean[2], double mean_err[2], double sd[2], int count[2]);
void output_file(std::string filename, std::vector<Momentum_recon::Mom_chain>&mom);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-momch file-out-momch\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);

	output_file(file_out_momch, momch);


}
void output_file(std::string filename, std::vector<Momentum_recon::Mom_chain>&mom) {
	std::ofstream ofs(filename);

	double mean[2], mean_err[2], sd[2];
	int count[2];
	for (auto &c : mom) {
		std::set<int> pl_set;
		for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
			pl_set.insert(itr->pl);
		}

		int pl_min = *pl_set.begin();
		int pl_max= *pl_set.rbegin();
		printf("PL range %d %d\n", pl_min, pl_max);
		for (int pl = pl_min; pl <= pl_max; pl++) {
			ofs << std::fixed << std::right
				<< std::setw(10) << std::setprecision(0) << c.groupid << " "
				<< std::setw(10) << std::setprecision(0) << c.chainid << " "
				<< std::setw(10) << std::setprecision(0) << pl << " ";
			Calc_divide_vph(c, pl, mean, mean_err, sd, count);
			ofs << std::fixed << std::right
				<< std::setw(4) << std::setprecision(0) << count[0] << " "
				<< std::setw(8) << std::setprecision(2) << mean[0] << " "
				<< std::setw(6) << std::setprecision(3) << mean_err[0] << " "
				<< std::setw(4) << std::setprecision(0) << count[1] << " "
				<< std::setw(8) << std::setprecision(2) << mean[1] << " "
				<< std::setw(6) << std::setprecision(3) << mean_err[1] << " ";

			Calc_divide_pixel(c, pl, mean, mean_err, sd, count);
			ofs << std::fixed << std::right
				<< std::setw(4) << std::setprecision(0) << count[0] << " "
				<< std::setw(8) << std::setprecision(2) << mean[0] << " "
				<< std::setw(6) << std::setprecision(3) << mean_err[0] << " "
				<< std::setw(4) << std::setprecision(0) << count[1] << " "
				<< std::setw(8) << std::setprecision(2) << mean[1] << " "
				<< std::setw(6) << std::setprecision(3) << mean_err[1] << std::endl;

		}

	}
}
void Calc_divide_vph(Momentum_recon::Mom_chain  &c, int pl, double mean[2], double mean_err[2], double sd[2],  int count[2]) {
	count[0] = 0;
	count[1] = 0;
	int sum2[2] = {}, sum1[2] = {};

	for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
		if (itr->pl < pl) {
			for (int i = 0; i < 2; i++) {
				count[0] += 1;
				sum1[0] += itr->m[i].ph;
				sum2[0] += pow(itr->m[i].ph, 2);
			}
		}
		else {
			for (int i = 0; i < 2; i++) {
				count[1] += 1;
				sum1[1] += itr->m[i].ph;
				sum2[1] += pow(itr->m[i].ph, 2);
			}
		}
	}

	for (int i = 0; i < 2; i++) {

		if (count[i] < 2) {
			mean[i] = -1;
			mean_err[i] = -1;
			sd[i] = -1;
		}
		else {
			mean[i] = sum1[i] / count[i];
			sd[i] = sum2[i] / count[i] - mean[i] * mean[i];
			if (sd[i] <= 0) {
				mean[i] = -1;
				mean_err[i] = -1;
				sd[i] = -1;
			}
			else {
				sd[i] = sqrt(sd[i]) * sqrt(count[i] - 1) / sqrt(count[i]);
				mean_err[i] = sd[i] / sqrt(count[i]);
			}
		}
	}
}
void Calc_divide_pixel(Momentum_recon::Mom_chain  &c, int pl, double mean[2],  double mean_err[2], double sd[2], int count[2]) {
	count[0] = 0;
	count[1] = 0;
	int sum2[2] = {}, sum1[2] = {};

	for (auto itr = c.base.begin(); itr != c.base.end(); itr++) {
		if (itr->pl < pl) {
			for (int i = 0; i < 2; i++) {
				if (itr->m[i].hitnum < 0)continue;
				count[0] += 1;
				sum1[0] += itr->m[i].hitnum;
				sum2[0] += pow(itr->m[i].hitnum, 2);
			}
		}
		else {
			for (int i = 0; i < 2; i++) {
				if (itr->m[i].hitnum < 0)continue;
				count[1] += 1;
				sum1[1] += itr->m[i].hitnum;
				sum2[1] += pow(itr->m[i].hitnum, 2);
			}
		}
	}

	for (int i = 0; i < 2; i++) {

		if (count[i] < 2) {
			mean[i] = -1;
			mean_err[i] = -1;
			sd[i] = -1;
		}
		else {
			mean[i] = sum1[i] / count[i];
			sd[i] = sum2[i] / count[i] - mean[i] * mean[i];
			if (sd[i] <= 0) {
				mean[i] = -1;
				mean_err[i] = -1;
				sd[i] = -1;
			}
			else {
				sd[i] = sqrt(sd[i]) * sqrt(count[i] - 1) / sqrt(count[i]);
				mean_err[i] = sd[i] / sqrt(count[i]);
			}
		}
	}
}