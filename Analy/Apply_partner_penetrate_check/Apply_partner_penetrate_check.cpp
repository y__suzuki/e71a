#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::set<std::pair<int, int>>read_checkfile(std::string filename);
std::vector<mfile0::M_Chain> reject_chain(std::vector<mfile0::M_Chain>&chain, std::set<std::pair<int, int>>&reject_list);

int main(int argc, char**argv) {

	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-mfile file-out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_check = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	std::set<std::pair<int, int>> reject_list=read_checkfile(file_in_check);
	m.chains = reject_chain(m.chains, reject_list);

	mfile0::write_mfile(file_out_mfile, m);
}
std::set<std::pair<int, int>>read_checkfile(std::string filename) {
	std::ifstream ifs(filename);
	std::set<std::pair<int, int>> ret;
	int gid, chainid;
	int eventid, trackid;
	while (ifs >> gid >> chainid) {
		eventid = gid / 100000;
		trackid= gid % 100000;
		ret.insert(std::make_pair(eventid, trackid));
	}
	return ret;
}
std::vector<mfile0::M_Chain> reject_chain(std::vector<mfile0::M_Chain>&chain, std::set<std::pair<int, int>>&reject_list) {
	std::vector<mfile0::M_Chain> ret;
	std::pair<int, int> id;
	for (auto &c : chain) {
		id.first = c.basetracks.begin()->group_id;
		id.second = c.chain_id;
		if (reject_list.count(id) == 1)continue;
		if (id.first == 4384)continue;
		if (id.first == 3584)continue;
		if (id.first == 3757)continue;
		ret.push_back(c);
	}

	printf("chain %d --> %d\n", chain.size(), ret.size());
	return ret;


}