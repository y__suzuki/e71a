
#include <string>
#include <vector>
#include <windows.h>
#include <iostream>
#include <map>
#include <sstream>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <set>
#include <fstream>

bool checkFileExistence(const std::string& str);
int half_thread();
bool getFileNames(std::string folderPath, std::vector<std::string> &file_names);

int main() {
	std::string file_in_path = "I:\\NINJA\\E71a\\ECC5";
	int max_shot = 936;

	std::set<std::pair<int, int>> err_data;
	for (int area = 1; area <= 6; area++) {
		for (int pl = 16; pl <= 45; pl++) {
			printf("Area%d PL%03d start\n", area, pl);
			for (int imager = 0; imager < 72; imager++) {

				std::stringstream folder_path;
				folder_path << file_in_path << "\\Area" << area << "\\PL" << std::setw(3) << std::setfill('0') << pl << "\\DATA\\"
					<< std::setw(2) << std::setfill('0') << imager / 12 << "_" << std::setw(2) << std::setfill('0') << imager % 12;

				std::vector<std::string> file_list;
				getFileNames(folder_path.str(), file_list);
				std::set<int> all_shot;
				for (int i = 0; i < file_list.size(); i++) {
					int shot_id = std::stoi(file_list[i].substr(file_list[i].size() - 19, 8));
					//printf("%d\n", shot_id);
					all_shot.insert(shot_id);
				}
				for (int shot = 0; shot < max_shot; shot++) {
					if (all_shot.count(shot) == 0) {
						fprintf(stderr, "file not found %02d_%02d shot=%d\n", imager / 12, imager % 12, shot);
						err_data.insert(std::make_pair( pl, area));
					}
				}
			}
		}
	}
	system("pause");

	for (auto itr = err_data.begin(); itr != err_data.end(); itr++) {
		printf("PL%03d Area%d\n", itr->first, itr->second);
	}
	system("pause");

}

bool getFileNames(std::string folderPath, std::vector<std::string> &file_names)
{
	HANDLE hFind;
	WIN32_FIND_DATA win32fd;
	std::string search_name = folderPath + "\\*";

	hFind = FindFirstFile(search_name.c_str(), &win32fd);

	if (hFind == INVALID_HANDLE_VALUE) {
		throw std::runtime_error("file not found");
		return false;
	}

	/* 指定のディレクトリ以下のファイル名をファイルがなくなるまで取得する */
	do {
		if (win32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			/* ディレクトリの場合は何もしない */
			//printf("directory\n");
		}
		else {
			/* ファイルが見つかったらVector配列に保存する */
			std::string extension = win32fd.cFileName;
			extension = extension.substr(extension.size() - 5, 5);
			if (extension == ".spng") {
				file_names.push_back(win32fd.cFileName);
			}
			//printf("%s\n", file_names.back().c_str());
		}
	} while (FindNextFile(hFind, &win32fd));

	FindClose(hFind);

	return true;
}
