#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"PID_correct.lib")
#include <PID_correct.h>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <iostream>
#include <set>
#include <omp.h>
#include <filesystem>
#include <sstream>
class CL_map {
public:
	double angle_max;
	std::map<int, double> cl_map;
};
std::multimap < std::tuple<int, int, int>, CL_map>raed_CL(std::string filename);
std::set<uint64_t> read_chain(std::string filename);
void pickup_chain(std::vector<PID_track> &track, std::multimap<uint64_t, PID_track> &chains, std::set<uint64_t> &chainid);
void output_chain_CL(std::string filename, std::multimap<uint64_t, PID_track> &chains);
double Calc_total_CL(std::vector<PID_track> &chain);
void log_div_factorial_part(int&k, int max, double log_p, std::vector<std::pair<int, double>>&result, std::pair<int, double>&preb_res);
void product_vec(std::vector<double> v, int &exp, double &mant);
std::vector<PID_track> reject_track(std::vector<PID_track>&track);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg CL_file base_path chain_dat output\n");
		exit(1);
	}


	std::string file_in_CL_list = argv[1];
	std::string file_in_base_path = argv[2];
	std::string file_in_chain = argv[3];
	std::string file_out = argv[4];

	std::multimap < std::tuple<int, int, int>, CL_map>CL_map = raed_CL(file_in_CL_list);

	////debug output
	//for (auto itr = CL_map.begin(); itr != CL_map.end(); itr++) {
	//	int count = CL_map.count(itr->first);
	//	auto range = CL_map.equal_range(itr->first);
	//	printf("%d %d %d\n", std::get<0>(itr->first), std::get<1>(itr->first), std::get<2>(itr->first));
	//	for (auto res = range.first; res != range.second; res++) {
	//		printf("\t angle_max=%.1lf\n", res->second.angle_max);
	//		for (auto itr2 = res->second.cl_map.begin(); itr2 != res->second.cl_map.end(); itr2++) {
	//			printf("\t %d %.5lf\n", itr2->first, itr2->second);
	//		}
	//	}
	//	//system("pause");
	//	itr = std::next(itr, count - 1);
	//}


	std::set<uint64_t> chainid = read_chain(file_in_chain);

	//std::multimap<uint64_t, double> chain_CL;
	//std::multimap <uint64_t, std::pair<short, float>> chain_CL;
	std::vector<PID_track> track_buf;
	std::multimap<uint64_t, PID_track> chains;
	for (int pl = 1; pl <= 133; pl++) {
		printf("read CL file PL%03d\n", pl);
		std::stringstream filename;
		filename << file_in_base_path << "\\base_CL_pixel_" << std::setw(3) << std::setfill('0') << pl << ".bin";
		if (!std::filesystem::exists(filename.str())) {
			printf("%s no track\n", filename.str().c_str());
			continue;
		}
		PID_track::read_pid_track(filename.str(), track_buf);
		track_buf = reject_track(track_buf);
		pickup_chain(track_buf, chains, chainid);
		track_buf.clear();
	}
	std::ofstream ofs(file_out + ".bin", std::ios::binary);
	std::ofstream ofs_txt(file_out + ".txt");
	for (auto itr = chains.begin(); itr != chains.end(); itr++) {
		int count = chains.count(itr->first);
		std::vector<PID_track> chain;
		auto range = chains.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			chain.push_back(res->second);
		}
		double CL_val = Calc_total_CL(chain);
		//binary出力にしたほうがいいかも
		ofs_txt << std::right << std::fixed
			<< std::setw(20) << std::setprecision(0) << chain.begin()->chainid << " "
			<< std::scientific << std::setprecision(30) << CL_val << std::endl;
		ofs.write((char *)&chain.begin()->chainid, sizeof(uint64_t));
		ofs.write((char *)&CL_val, sizeof(double));

		itr = std::next(itr, count - 1);
	}
	//output_chain_CL(file_out, chains);
}
std::multimap < std::tuple<int, int, int>, CL_map>raed_CL(std::string filename) {
	std::multimap < std::tuple<int, int, int>, std::tuple<double, int, double> >read_CL;
	std::ifstream ifs(filename);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	int pos, sensor_id, tracking_id, vph;
	double angle_min, angle_max, cl;
	while (ifs >> pos >> tracking_id >> sensor_id >> angle_min >> angle_max >> vph >> cl) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		read_CL.insert(std::make_pair(std::make_tuple(pos, tracking_id, sensor_id), std::make_tuple(angle_max, vph, cl)));
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no track!\n", filename.c_str());
		exit(1);
	}

	angle_max = 0;
	CL_map cl_tmp;
	std::multimap < std::tuple<int, int, int>, CL_map >ret;
	for (auto itr = read_CL.begin(); itr != read_CL.end(); itr++) {
		count = read_CL.count(itr->first);
		auto range = read_CL.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res == range.first) {
				cl_tmp.angle_max = std::get<0>(res->second);
				cl_tmp.cl_map.clear();
			}
			else if (std::next(res, 1) == range.second) {
				ret.insert(std::make_pair(itr->first, cl_tmp));
			}
			else if (cl_tmp.angle_max + 0.001 < std::get<0>(res->second)) {
				ret.insert(std::make_pair(itr->first, cl_tmp));
				cl_tmp.angle_max = std::get<0>(res->second);
				cl_tmp.cl_map.clear();
			}
			cl_tmp.cl_map.insert(std::make_pair(std::get<1>(res->second), std::get<2>(res->second)));
		}

		itr = std::next(itr, count - 1);
	}
	return ret;
}
std::set<uint64_t> read_chain(std::string filename) {
	std::ifstream ifs(filename);
	std::set<uint64_t> ret;
	uint64_t cid;
	while (ifs >> cid) {
		ret.insert(cid);
	}
	return ret;
}
std::vector<PID_track> reject_track(std::vector<PID_track>&track) {
	std::vector<PID_track> ret;
	ret.reserve(track.size());
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		//vphにCLが入っている
		//CL<0は評価失敗(-1が入っている)
		if (itr->vph < -0.1)continue;
		ret.push_back(*itr);
	}
	return ret;
}
void pickup_chain(std::vector<PID_track> &track, std::multimap<uint64_t, PID_track> &chains, std::set<uint64_t> &chainid) {
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		if (chainid.count(itr->chainid) == 0)continue;
		chains.insert(std::make_pair(itr->chainid, *itr));
	}
	return;
}

int sensor_id(int sensor) {
	if ((24 <= sensor && sensor < 36) || sensor == 52)return 1;
	return 0;
}
int angle_id(std::set<double >angle_range, double angle) {
	int i = 0;
	for (auto itr = angle_range.begin(); itr != angle_range.end(); itr++) {
		if (angle < *itr)return i;
		i++;
	}
	return i - 1;
}
int tracking_id(int track) {
	if (track == 0 || track == 1)return 0;
	else if (track == 2)return 1;
	return 2;
}

void output_chain_CL(std::string filename, std::multimap<uint64_t, PID_track> &chains) {

	std::vector<PID_track> out;
	for (auto itr = chains.begin(); itr != chains.end(); itr++) {
		out.push_back(itr->second);
	}

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (out.size() == 0) {
		fprintf(stderr, "target track ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = out.size();
	for (int i = 0; i < out.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs << std::right << std::fixed
			<< std::setw(20) << std::setprecision(0) << out[i].chainid << " "
			<< std::setw(12) << std::setprecision(0) << out[i].rawid << " "
			<< std::setw(4) << std::setprecision(2) << out[i].pid << " "
			<< std::setw(8) << std::setprecision(1) << out[i].pb << " "
			<< std::setw(7) << std::setprecision(4) << out[i].angle << " "
			<< std::setw(4) << std::setprecision(0) << out[i].pos << " "
			<< std::setw(3) << std::setprecision(0) << out[i].trackingid << " "
			<< std::setw(15) << std::setprecision(14) << out[i].vph << " "
			<< std::setw(6) << std::setprecision(1) << out[i].pixelnum << " "
			<< std::setw(3) << std::setprecision(0) << out[i].sensorid << std::endl;

	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}
double Calc_total_CL(std::vector<PID_track> &chain) {
	std::multimap<int, PID_track> pos_divide;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		pos_divide.insert(std::make_pair(itr->pos, *itr));
	}

	std::vector<PID_track> chain2;
	for (auto itr = pos_divide.begin(); itr != pos_divide.end(); itr++) {
		int count = pos_divide.count(itr->first);
		chain2.push_back(pos_divide.find(itr->first)->second);
		itr = std::next(itr, count - 1);
	}

	std::vector<double> calc_val;
	for (auto itr = chain2.begin(); itr != chain2.end(); itr++) {
		calc_val.push_back(itr->vph);
	}
	std::pair<int, double> p_product;
	//CLの積を計算
	product_vec(calc_val, p_product.first, p_product.second);
	double ln_p = log(p_product.second) + p_product.first*log(2.);

	std::vector<std::pair<int, double>> each_log_div_fact;
	std::pair<int, double> tmp_res;
	int k = 0;
	log_div_factorial_part(k, calc_val.size() - 1, ln_p, each_log_div_fact, tmp_res);

	std::vector<std::pair<int, double>> before_sum;
	for (auto itr = each_log_div_fact.begin(); itr != each_log_div_fact.end(); itr++) {
		tmp_res.first = itr->first + p_product.first;
		tmp_res.second = itr->second*p_product.second;
		before_sum.push_back(tmp_res);
	}
	double res;
	int exp_max, dbl_min_exp;
	res = std::frexpl(DBL_MIN, &exp_max);
	res = std::frexpl(DBL_MIN, &dbl_min_exp);

	res = 0;
	for (auto itr = before_sum.begin(); itr != before_sum.end(); itr++) {
		//doubleで表現できない値(指数部小さい)はcontinue
		if (itr->first <= dbl_min_exp)continue;
		exp_max = std::max(exp_max, itr->first);
		res += itr->second*pow(2, itr->first);
	}
	if (exp_max == dbl_min_exp) {
		return DBL_MIN;
	}
	return res;
}
void product_vec(std::vector<double> v, int &exp, double &mant) {
	std::vector<int> exp_v;
	std::vector<double> mant_v;
	double mant_part;
	int exp_part;
	//変数をすべて指数と仮数に分離
	for (auto itr = v.begin(); itr != v.end(); itr++) {
		mant_part = std::frexpl(*itr, &exp_part);
		mant_v.push_back(mant_part);
		exp_v.push_back(exp_part);
	}

	//指数毎、仮数毎に計算
	exp = 0;
	mant = 1;
	for (int i = 0; i < exp_v.size(); i++) {
		mant = mant * mant_v[i];
		exp += exp_v[i];
	}
	//計算結果を指数と仮数に分離
	mant = std::frexpl(mant, &exp_part);
	exp += exp_part;
}
void log_div_factorial_part(int&k, int max, double log_p, std::vector<std::pair<int, double>>&result, std::pair<int, double>&preb_res) {
	double mant_part;
	int exp_part;

	if (k == 0) {
		mant_part = 1;
		result.clear();
		result.reserve(max);
		preb_res.second = std::frexpl(mant_part, &preb_res.first);
		result.push_back(preb_res);
	}
	if (k == max)return;
	k++;

	mant_part = -1 * log_p / k;
	std::pair<int, double> current_res;
	current_res.second = std::frexpl(mant_part, &current_res.first);

	preb_res.second = preb_res.second* current_res.second;
	preb_res.second = std::frexpl(preb_res.second, &exp_part);
	preb_res.first += exp_part + current_res.first;
	result.push_back(preb_res);
	log_div_factorial_part(k, max, log_p, result, preb_res);
}
