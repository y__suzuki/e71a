#define _CRT_SECURE_NO_WARNINGS


#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>
#include <filesystem>

class microtrack_inf {
public:
	int zone, pos, col, row, isg, ph, pixelnum, hitnum;
};
int64_t file_size(std::string filename);
std::vector<microtrack_inf> read_microtrack_inf(std::string filename, bool output);
std::map<int, std::vector<vxx::base_track_t>> divide_zone(std::vector<vxx::base_track_t>&base);
std::map<std::tuple<int, int, int, int, int>, int> read_pixel_inf(std::string file_in_ECC, int pl);
void pixe_inf_count(std::map<std::tuple<int, int, int, int, int>, int> &pixel_inf);
void zone_trans(std::map<std::tuple<int, int, int, int, int>, int> &pixel_inf);

int main(int argc, char**argv) {
	std::string file_in_ECC = "I:\\NINJA\\E71a\\ECC5";
	int area = 0;
	int pl = 65;

	std::stringstream file_in_base_all;
	file_in_base_all << file_in_ECC << "\\Area" << std::setw(1) << area << "\\PL" << std::setfill('0') << std::setw(3) << pl
		<< "\\b" << std::setfill('0') << std::setw(3) << pl << ".sel.cor.vxx";
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base_all.str(), pl, 0);

	std::map<int, std::vector<vxx::base_track_t>> base_zone = divide_zone(base);
	std::map<std::tuple<int, int, int, int, int>, int> pixel_inf = read_pixel_inf(file_in_ECC, pl);
	pixe_inf_count(pixel_inf);



	system("pause");


}


std::map<int, std::vector<vxx::base_track_t>> divide_zone(std::vector<vxx::base_track_t>&base) {

	std::multimap<int, vxx::base_track_t> base_multimap;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->m[0].zone != itr->m[1].zone) {
			fprintf(stderr, "zone not match %d < -- > %d\n", itr->m[0].zone, itr->m[1].zone);
			exit(1);
		}
		base_multimap.insert(std::make_pair(itr->m[0].zone, *itr));
	}

	std::map<int, std::vector<vxx::base_track_t>> base_map;
	std::vector<vxx::base_track_t> base_tmp;
	for (auto itr = base_multimap.begin(); itr != base_multimap.end(); itr++) {
		int count = base_multimap.count(itr->first);
		printf("zone %2d tracknum = %10d\n", itr->first, count);

		base_tmp.clear();
		auto range = base_multimap.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_tmp.push_back(res->second);
		}
		base_map.insert(std::make_pair(itr->first, base_tmp));
		itr = std::next(itr,count-1);
	}
	return base_map;
}

int64_t file_size(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2;
}

std::vector<microtrack_inf> read_microtrack_inf(std::string filename, bool output) {

	int64_t micro_num = file_size(filename) / sizeof(microtrack_inf);

	FILE*fp_in;
	if ((fp_in = fopen(filename.c_str(), "rb")) == NULL) {
		printf("%s file not open!\n", filename.c_str());
		exit(EXIT_FAILURE);
	}

	std::vector<microtrack_inf> ret;
	ret.reserve(micro_num);
	const int Read_Block = 10000;

	microtrack_inf m_buf[Read_Block];
	int64_t  now = 0;
	int read_num;
	bool flg = true;
	while (flg) {
		if (micro_num - now == Read_Block) {
			read_num = micro_num - now;
			flg = false;
		}
		else if (micro_num - now < Read_Block) {
			read_num = micro_num - now;
			flg = false;
		}
		else {
			read_num = Read_Block;
		}
		fread(&m_buf, sizeof(microtrack_inf), read_num, fp_in);
		for (int i = 0; i < read_num; i++) {
			ret.push_back(m_buf[i]);
		}
		now += read_num;
	}
	return ret;
}

std::map<std::tuple<int, int, int, int, int>, int> read_pixel_inf(std::string file_in_ECC, int pl) {
	std::map<std::tuple<int, int, int, int, int>, int> pixel_inf;
	const int NumberOfImager = 72;
	uint32_t ShotID;
	int view, imager;

	std::vector<microtrack_inf> input_micro_buf;

	std::set<int>zone_pos1, zone_pos2;
	for (int Area = 1; Area <= 6; Area++) {
		for (int iscan = 0; iscan < 4; iscan++) {
			printf("PL%03d Area%d scan %d", pl, Area, iscan);

			std::stringstream infile_thick, infile_thin;
			infile_thick << file_in_ECC << "\\Area"
				<< std::setw(1) << Area << "\\PL"
				<< std::setw(3) << std::setfill('0') << pl << "\\micro_inf_thick_"
				<< std::setw(1) << iscan;
			infile_thin << file_in_ECC << "\\Area"
				<< std::setw(1) << Area << "\\PL"
				<< std::setw(3) << std::setfill('0') << pl << "\\micro_inf_thin_"
				<< std::setw(1) << iscan;

			if (!std::filesystem::exists(infile_thick.str())) {
				fprintf(stderr, "%s not exist\n", infile_thick.str().c_str());
				exit(1);
			}
			if (!std::filesystem::exists(infile_thin.str())) {
				fprintf(stderr, "%s not exist\n", infile_thin.str().c_str());
				exit(1);
			}
			printf(" thick %d , thin %d\n", file_size(infile_thick.str()) / sizeof(microtrack_inf), file_size(infile_thin.str()) / sizeof(microtrack_inf));


			input_micro_buf = read_microtrack_inf(infile_thick.str(), false);
			for (auto itr = input_micro_buf.begin(); itr != input_micro_buf.end(); itr++) {
				ShotID = ((uint32_t)(uint16_t)itr->row << 16) | ((uint32_t)(uint16_t)itr->col);
				view = ShotID / NumberOfImager;
				imager = ShotID % NumberOfImager;

				pixel_inf.insert(std::make_pair(std::make_tuple(itr->pos, itr->zone, view, imager, itr->isg), itr->hitnum));
				if (itr->pos % 10 == 1)zone_pos1.insert(itr->zone);
				if (itr->pos % 10 == 2)zone_pos2.insert(itr->zone);
			}
			printf("pixel_inf size = %d\n", pixel_inf.size());

			input_micro_buf = read_microtrack_inf(infile_thin.str(), false);
			for (auto itr = input_micro_buf.begin(); itr != input_micro_buf.end(); itr++) {
				ShotID = ((uint32_t)(uint16_t)itr->row << 16) | ((uint32_t)(uint16_t)itr->col);
				view = ShotID / NumberOfImager;
				imager = ShotID % NumberOfImager;

				pixel_inf.insert(std::make_pair(std::make_tuple(itr->pos, itr->zone, view, imager, itr->isg), itr->hitnum));
				if (itr->pos % 10 == 1)zone_pos1.insert(itr->zone);
				if (itr->pos % 10 == 2)zone_pos2.insert(itr->zone);
			}
			printf("pixel_inf size = %d\n", pixel_inf.size());
		}
	}
	if (zone_pos1.size() == 24 && zone_pos2.size() == 24) {
		zone_trans(pixel_inf);
	}
	else if (zone_pos1.size() == 48 && zone_pos2.size() == 48) {
		//変更の必要なし
	}
	else {
		fprintf(stderr, "file format exception\n");
		fprintf(stderr, "zone1 size = %d zone2 size=%d\n", zone_pos1.size(), zone_pos2.size());
		exit(1);
	}

	return pixel_inf;
}

void pixe_inf_count(std::map<std::tuple<int, int, int, int, int>, int> &pixel_inf) {
	std::map<std::tuple<int, int>,int>count;

	for (auto itr = pixel_inf.begin(); itr != pixel_inf.end(); itr++) {
		auto res = count.insert(std::make_pair(std::make_tuple(std::get<0>(itr->first), std::get<1>(itr->first)), 1));
		if (!res.second) {
			res.first->second += 1;
		}
	}



	for (auto itr = count.begin(); itr != count.end(); itr++) {
		printf("pos %4d zone %2d count %10d\n", std::get<0>(itr->first), std::get<1>(itr->first), itr->second);
	}

}

void zone_trans(std::map<std::tuple<int, int, int, int, int>, int> &pixel_inf) {
	int pos, zone;
	std::tuple<int, int, int, int, int> newid;
	std::vector<std::pair<std::tuple<int, int, int, int, int>, int>> add_pixel_inf;
	add_pixel_inf.reserve(pixel_inf.size());

	for (auto itr = pixel_inf.begin(); itr != pixel_inf.end(); itr++) {
		pos = std::get<0>(itr->first);
		zone = std::get<1>(itr->first);
		newid = itr->first;
		if (pos % 10 == 1) {
			if ((zone - 1) / 6 == 0 || (zone - 1) / 6 == 2 || (zone - 1) / 6 == 4 || (zone - 1) / 6 == 6) {
				std::get<1>(newid) = zone + 6;
				add_pixel_inf.push_back(std::make_pair(newid, itr->second));
			}
			else{
				fprintf(stderr, "file format exception\n");
				fprintf(stderr, "pos %d zone %d\n", pos, zone);
				exit(1);
			}
		}
		else if (pos % 10 == 2) {
			if ((zone - 1) / 6 == 0 || (zone - 1) / 6 == 1 || (zone - 1) / 6 == 4 || (zone - 1) / 6 == 5) {
				std::get<1>(newid) = zone + 12;
				add_pixel_inf.push_back(std::make_pair(newid, itr->second));
			}
			else {
				fprintf(stderr, "file format exception\n");
				fprintf(stderr, "pos %d zone %d\n", pos, zone);
				exit(1);
			}
		}
	}

	for (auto itr = add_pixel_inf.begin(); itr != add_pixel_inf.end(); itr++) {
		pixel_inf.insert(*itr);
	}
}