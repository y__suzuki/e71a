#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class minimum_distance {
public:
	int trackid, pl, rawid, ph, flg_black;
	double ax, ay, x, y, z, ex_z0, ex_z1,md,vph;

	//1144   448
	//	1  87    5731772  240066   0 - 0.1361 - 0.3613   233671.0   118911.1        0.0 - 3870.0      310.0
	//	0  87    4427619  160017   0   0.7329   2.7621   234568.6   121897.0        0.0        0.0 - 961.3

};
std::multimap<int, minimum_distance> read_md(std::string filename);
std::multimap<int, minimum_distance> md_selection(std::multimap<int, minimum_distance> &md, std::vector<mfile0::M_Chain>&chain);
void output_md(std::string filename, std::multimap<int, minimum_distance> &md);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-mfile file-in-md file-out\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_md = argv[2];
	std::string file_out_md = argv[3];

	std::multimap<int, minimum_distance> md = read_md(file_in_md);
	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	md = md_selection(md, m.chains);

	output_md(file_out_md, md);

}
std::multimap<int, minimum_distance> read_md(std::string filename) {
	std::ifstream ifs(filename);
	std::multimap<int, minimum_distance> ret;
	int eventid, tracknum;
	minimum_distance m;
	while (ifs >> eventid >> tracknum) {
		for (int i = 0; i < tracknum; i++) {
			ifs >> m.trackid >> m.pl >> m.rawid >> m.ph >> m.flg_black >> m.ax >> m.ay >> m.x >> m.y >> m.z >> m.ex_z0 >> m.ex_z1>>m.md;
			m.vph = 0;
			if (eventid == 4384)continue;
			if (eventid == 3584)continue;
			if (eventid == 3757)continue;
			ret.insert(std::make_pair(eventid, m));
		}

	}
	return ret;
}

std::multimap<int, minimum_distance> md_selection(std::multimap<int, minimum_distance> &md, std::vector<mfile0::M_Chain>&chain) {
	std::multimap<int, minimum_distance> ret;


	std::multimap<int, mfile0::M_Chain> event_chain;
	for (auto&c : chain) {
		event_chain.insert(std::make_pair(c.basetracks.begin()->group_id, c));
	}

	for (auto itr = md.begin(); itr != md.end(); itr++) {
		if (event_chain.count(itr->first) == 0)continue;
		bool flg = false;
		mfile0::M_Chain c;
		auto range = event_chain.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			for (auto itr2 = res->second.basetracks.begin(); itr2 != res->second.basetracks.end(); itr2++) {
				if (itr->second.pl == itr2->pos / 10 && itr->second.rawid == itr2->rawid) {
					flg = true;
					c = res->second;
					break;
				}
			}
			if (flg)break;
		}
		if (!flg)continue;

		itr->second.vph = 0;
		int count = 0;
		for (auto itr2 = c.basetracks.begin(); itr2 != c.basetracks.end(); itr2++) {
			itr->second.vph += itr2->ph % 10000;
			count++;
		}
		itr->second.vph /= count;
		ret.insert(*itr);
	}

	printf("%d -->%d\n", md.size(), ret.size());

	return ret;

}

void output_md(std::string filename, std::multimap<int, minimum_distance> &md) {
	std::ofstream ofs(filename);

	for (auto itr = md.begin(); itr != md.end(); itr++) {
		int count = md.count(itr->first);
		if (count == 1)continue;

		minimum_distance muon;
		bool flg = false;
		auto range = md.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.trackid == 1) {
				muon = itr->second;
				flg = true;
				break;
			}
		}
		if (flg) {
			for (auto res = range.first; res != range.second; res++) {
				//if (res->second.trackid == 1)continue;
				ofs << std::right << std::fixed
					<< std::setw(5) << std::setprecision(0) << itr->first << " "
					<< std::setw(3) << std::setprecision(0) << muon.pl << " "
					<< std::setw(3) << std::setprecision(0) << res->second.pl << " "
					<< std::setw(3) << std::setprecision(0) << res->second.flg_black << " "
					<< std::setw(6) << std::setprecision(1) << res->second.vph << " "
					<< std::setw(7) << std::setprecision(4) << res->second.ax << " "
					<< std::setw(7) << std::setprecision(4) << res->second.ay << " "
					<< std::setw(8) << std::setprecision(3) << res->second.md << " "
					<< std::setw(8) << std::setprecision(1) << -1*(res->second.ex_z0- res->second.ex_z1)<< std::endl;
			}
		}

		itr = std::next(itr, count - 1);
	}

}