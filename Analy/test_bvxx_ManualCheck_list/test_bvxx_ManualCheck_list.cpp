#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
void out_txt(std::string filename, std::vector<netscan::base_track_t> base);
bool sort_isg(netscan::base_track_t &right, netscan::base_track_t &left) {
	return right.m[0].isg < left.m[0].isg;
}
int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:bvxx pl zone out-txt\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_txt = argv[4];

	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_bvxx, base, pl, zone);
	out_txt(file_out_txt, base);
}
void out_txt(std::string filename, std::vector<netscan::base_track_t> base) {
	sort(base.begin(),base.end(), sort_isg);
	std::ofstream ofs(filename);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(10) << std::setprecision(0) << itr->m[0].isg << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(7) << std::setprecision(4) << sqrt(itr->ax*itr->ax + itr->ay*itr->ay) << " "
			<< std::setw(8) << std::setprecision(0) << itr->m[0].ph + itr->m[1].ph << std::endl;
	}
}