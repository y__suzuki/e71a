//2次元飛跡でのVPHモデル
//定式化
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include<vector>
#include<map>
#include <algorithm>
#include <list>
#include <set>
#include<sstream>
#include <fstream>
#include <iomanip> 
#include <chrono>
#include <omp.h>

double Pass_length(double L, double D, double angle);
double Probability_hit_expansion(double N, double p);
double Probaility_PH(double p_hit, int ph);
std::map<int, double>Probaility_VPH(double p_hit, int ph_cut, int max_pixel);
std::map<int, double>Probaility_VPH(double p_hit, int ph_cut, int max_pixel, std::map<int, std::vector<std::map<int, int>>>&combi_map);

void summation_combination(int &k, int x, int num, int max, std::vector<std::map<int, int>>&combi);
void Print_comb(std::map<int, int>&combi);

inline void summation_combination2(int &k, int x, int num, int max, std::vector<std::map<int, int>>&combi);

void comb(std::vector<std::vector <uint64_t> > &v);
uint64_t combination(uint64_t n, uint64_t k);
double multinomial_distribution_probability(std::map<int, double>&p_PH, std::map<int, int>&combi, int num_try);
uint64_t facctorialMethod(int k);
void output_vph(std::string filename, std::map<int, double>Probaility_VPH);
int use_thread(double ratio, bool output);

int main() {
	double L, D;
	//pixel size
	L = 0.45;
	//被写界深度
	D = 4;
	std::string output_path = "I:\\NINJA\\E71a\\work\\suzuki\\PID\\VPH_model";
	double CPU_ratio = 0.6;
	std::chrono::system_clock::time_point  start, p0, p1, end; // 型は auto で可
	start = std::chrono::system_clock::now(); // 計測開始時間
	int n_angle = 60, n_phit = 100;
	int num = 0, all = n_angle * n_phit;

	for (int ph_cut = 6; ph_cut <= 7; ph_cut++) {
		num = 0;
		for (int iang = 0; iang < n_angle; iang++) {
			double angle = iang * 6. / n_angle+0.0001;
			printf("angle %.1lf start\n", angle);

			//VPHを計算する際のpixel
			//小数点以下切り上げ+expansion
			int CalcVPHpixel = (int)std::min(11., D*angle / L + 2);
			//lateral方向は固定2pixel
			CalcVPHpixel = CalcVPHpixel * 2;

			std::map<int, std::vector<std::map<int, int>>>combi_map;
			for (int i = 0; i <= (16 - ph_cut + 1)*CalcVPHpixel; i++) {
				std::vector<std::map<int, int>>combi;
				int k = 0;
				summation_combination2(k, i, CalcVPHpixel, 16 - ph_cut + 1, combi);
				combi_map.insert(std::make_pair(i, combi));
			}
			//for (auto itr = combi_map.begin(); itr != combi_map.end(); itr++) {
			//	for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			//		Print_comb(*itr2);
			//	}
			//}
			for (int ip = 0; ip < n_phit;ip++) {
				double p_hit = ip * 1. / n_phit + 0.0001;
				printf("\r p hit=%.2lf", p_hit);
				std::map<int, double>VPH_Probability = Probaility_VPH(p_hit, ph_cut, CalcVPHpixel, combi_map);

				std::stringstream out_file;
				char name[256];
				sprintf(name, "PHcut_%1d_angle_%02d_p_%03d.txt", ph_cut, int(angle * 10), int(p_hit * 100));
				out_file << output_path << "\\" << name;
				output_vph(out_file.str(), VPH_Probability);

			}

		}

	}		
	printf("-------------------Calc fin-------------------\n");
	end = std::chrono::system_clock::now(); // 計測開始時間
	double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
	printf("all %.3lf\n", elapsed / 1000);

}
double Pass_length(double L, double D,double angle) {
	if (angle < L / D) {
		return D * sqrt(1 + pow(angle, 2));
	}
	else {
		return L * sqrt(1 + 1 / pow(angle, 2));
	}
}
double Probability_hit_expansion(double N, double p) {
	//double p_hit = 1 - pow(1 - p, N);
	double p_hit = 1 - exp(-1 * p*N);
	//printf("p hit = %.4lf\n", p_hit);
	return 1. - pow(1 - p_hit, 2);
}
double Probaility_PH(double p_hit, int ph) {
	return combination(16, ph)*pow(p_hit, ph)*pow(1 - p_hit, 16 - ph);
}
std::map<int, double>Probaility_VPH(double p_hit, int ph_cut, int max_pixel) {

	std::map<int, double>p_PH;
	p_PH.insert(std::make_pair(0, 1));
	auto res = p_PH.find(0);
	for (int i = ph_cut; i <= 16; i++) {
		p_PH.insert(std::make_pair(i - ph_cut + 1, Probaility_PH(p_hit, i)));
		res->second -= Probaility_PH(p_hit, i);
	}

	//for (auto itr = p_PH.begin(); itr != p_PH.end(); itr++) {
	//	printf("%d %.5lf\n", itr->first, itr->second);
	//}

	std::map<int, double> p_VPH;
	double p;
	uint64_t combi_val;
	double prob_val;
	// 処理
	for (int i = 0; i <= (16 - ph_cut + 1)*max_pixel * 2; i++) {

		//printf("\rVPH=%d start", i);
		//VPH=iになる確率

		//max_pixel*2回のサイコロの試行、出目は0-(16 - ph_cut + 1)、それぞれの確率はp_PH
		std::vector<std::map<int, int>>combi;
		int k = 0;
		summation_combination(k, i, max_pixel * 2, 16 - ph_cut + 1, combi);

		p = 0;
		for (auto itr = combi.begin(); itr != combi.end(); itr++) {
			//	Print_comb(*itr);
			p += multinomial_distribution_probability(p_PH, *itr, max_pixel);
		}

		p_VPH.insert(std::make_pair(i, p));

	}

	return p_VPH;
}
std::map<int, double>Probaility_VPH(double p_hit, int ph_cut, int max_pixel, std::map<int, std::vector<std::map<int, int>>>&combi_map) {

	std::map<int, double>p_PH;
	p_PH.insert(std::make_pair(0, 1));
	auto res = p_PH.find(0);
	for (int i = ph_cut; i <= 16; i++) {
		p_PH.insert(std::make_pair(i - ph_cut + 1, Probaility_PH(p_hit, i)));
		res->second -= Probaility_PH(p_hit, i);
	}

	std::map<int, double> p_VPH;
	double p;
	// 処理
	for (auto itr = combi_map.begin(); itr != combi_map.end();itr++){

		//printf("\rVPH=%d start", i);
		//VPH=iになる確率

		//max_pixel*2回のサイコロの試行、出目は0-(16 - ph_cut + 1)、それぞれの確率はp_PH
		std::vector<std::map<int, int>>combi = itr->second;

		p = 0;
		for (auto itr2 = combi.begin(); itr2 != combi.end(); itr2++) {
			//	Print_comb(*itr);
			p += multinomial_distribution_probability(p_PH, *itr2, max_pixel);
		}

		p_VPH.insert(std::make_pair(itr->first, p));

	}

	return p_VPH;
}




//0-maxまでのnum個の整数で和がxとなる組み合わせ
//時間かかってる
void summation_combination(int &k, int x, int num, int max, std::vector<std::map<int, int>>&combi) {
	if (num <= 0) {
		fprintf(stderr, "number of integer error\n");
		fprintf(stderr, "please put num>0\n");
		exit(1);
	}
	//kは何回足し算をしたかの指標
	if (k == 0) {
		combi.clear();
		std::map<int, int> tmp_map;
		for (int j = 0; j <= max; j++) {
			tmp_map.insert(std::make_pair(j, 0));
		}
		combi.push_back(tmp_map);
		//for (auto itr = combi.begin(); itr != combi.end(); itr++) {
		//	Print_comb(*itr);
		//}
	}
	//規定回数足したらsum=xとなるものを取り出して終わり
	if (k == num) {
		std::vector<std::map<int, int>>ret;
		int sum = 0;
		for (auto itr = combi.begin(); itr != combi.end(); itr++) {
			sum = 0;
			for (auto itr2 = itr->begin(); itr2 != itr->end(); itr2++) {
				sum += itr2->first*itr2->second;
			}
			if (sum == x)ret.push_back(*itr);
		}

		combi = ret;
		return;
	}
	k++;

	std::set<std::map<int, int>> map_set;
	int sum = 0;

	for (auto itr = combi.begin(); itr != combi.end(); itr++) {
		for (int i = 0; i <= max; i++) {
			std::map<int, int> map_tmp = *itr;
			//iを足す
			auto res = map_tmp.find(i);
			res->second++;

			//合計がx以下か検証
			sum = 0;
			for (auto itr2 = map_tmp.begin(); itr2 != map_tmp.end(); itr2++) {
				sum += itr2->first*itr2->second;
			}
			//Print_comb(map_tmp);
			if (sum <= x)map_set.insert(map_tmp);
		}
	}
	combi.clear();
	for (auto itr = map_set.begin(); itr != map_set.end(); itr++) {
		combi.push_back(*itr);
	}

	summation_combination(k, x, num, max, combi);
}


void Print_comb(std::map<int, int>&combi) {
	printf("sum=");
	int sum = 0;
	for (auto itr = combi.begin(); itr!= combi.end(); itr++) {
		printf("%d*%d", itr->first, itr->second);
		if (std::next(itr, 1) != combi.end()) {
			printf("+");
		}
		sum += itr->first*itr->second;
	}
	printf("=%d\n", sum);

}


//0-maxまでのnum個の整数で和がxとなる組み合わせ
//時間かかってる
void summation_combination2(int &k, int x, int num, int max, std::vector<std::map<int, int>>&combi) {
	if (num <= 0) {
		fprintf(stderr, "number of integer error\n");
		fprintf(stderr, "please put num>0\n");
		exit(1);
	}
	//kは何回足し算をしたかの指標
	if (k == 0) {
		combi.clear();
		std::map<int, int> tmp_map;
		for (int j = 0; j <= max; j++) {
			tmp_map.insert(std::make_pair(j, 0));
		}
		combi.push_back(tmp_map);
		//for (auto itr = combi.begin(); itr != combi.end(); itr++) {
		//	Print_comb(*itr);
		//}
	}
	//規定回数足したらsum=xとなるものを取り出して終わり
	if (k == num) {
		std::vector<std::map<int, int>>ret;
		int sum = 0;
		for (auto itr = combi.begin(); itr != combi.end(); itr++) {
			sum = 0;
			for (auto itr2 = itr->begin(); itr2 != itr->end(); itr2++) {
				sum += itr2->first*itr2->second;
			}
			if (sum == x)ret.push_back(*itr);
		}

		combi = ret;
		return;
	}
	k++;

	//この部分が実行されている
	std::set<std::map<int, int>> map_set;
	int sum = 0;

	for (auto itr = combi.begin(); itr != combi.end(); itr++) {
		for (int i = 0; i <= max; i++) {
			std::map<int, int> map_tmp = *itr;
			//iを足す
			auto res = map_tmp.find(i);
			res->second++;

			//合計がx以下か/最高の目が出続けたときxに到達するか検証
			sum = 0;
			for (auto itr2 = map_tmp.begin(); itr2 != map_tmp.end(); itr2++) {
				sum += itr2->first*itr2->second;
			}
			//Print_comb(map_tmp);
			if (sum <= x&&sum+(num-k)*max>=x)map_set.insert(map_tmp);
		}
	}
	combi.clear();
	for (auto itr = map_set.begin(); itr != map_set.end(); itr++) {
		combi.push_back(*itr);
	}

	summation_combination2(k, x, num, max, combi);
}

//nCrの計算
uint64_t combination(uint64_t n, uint64_t k) {
	std::vector<std::vector<uint64_t> > v(n + 1, std::vector<uint64_t>(n + 1, 0));
	comb(v);

	return v[n][k];
}
void comb(std::vector<std::vector <uint64_t> > &v) {
	for (int i = 0; i < v.size(); i++) {
		v[i][0] = 1;
		v[i][i] = 1;
	}
	for (int k = 1; k < v.size(); k++) {
		for (int j = 1; j < k; j++) {
			v[k][j] = (v[k - 1][j - 1] + v[k - 1][j]);
		}
	}
}

double multinomial_distribution_probability(std::map<int, double>&p_PH, std::map<int, int>&combi, int num_try) {
	double ret=facctorialMethod(num_try);
	for (auto itr = combi.begin(); itr != combi.end(); itr++) {
		ret = ret / facctorialMethod(itr->second);
		ret = ret * pow(p_PH.at(itr->first), itr->second);
	}
	return ret;
}
uint64_t facctorialMethod(int k) {
	uint64_t sum = 1;
	for (int i = 1; i <= k; ++i)
	{
		sum *= i;
	}
	return sum;
}

void output_vph(std::string filename, std::map<int, double>Probaility_VPH) {
	std::ofstream ofs(filename);
	for (auto itr = Probaility_VPH.begin(); itr != Probaility_VPH.end(); itr++) {
		//doubleの制度くらいまで出力
		ofs << std::right << std::fixed
			<< std::setw(5) << std::setprecision(0) << itr->first << " "
			<< std::setw(15) << std::setprecision(14) << itr->second << std::endl;
	}
	ofs.close();
}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
