#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
std::vector<netscan::base_track_t> base_selection(std::vector<netscan::base_track_t> base, std::vector<netscan::linklet_t> link);

int main(int argc, char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg input-bvxx pl zone link output-bvxx\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_in_link = argv[4];
	std::string file_out_bvxx = argv[5];

	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_bvxx, base, pl, zone);
	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_txt(file_in_link, link);
	std::vector<netscan::base_track_t> sel = base_selection(base, link);
	netscan::write_basetrack_vxx(file_out_bvxx, sel, pl, zone);

}
std::vector<netscan::base_track_t> base_selection(std::vector<netscan::base_track_t> base, std::vector<netscan::linklet_t> link) {
	int pl = base.begin()->pl;
	std::vector<int> rawid;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (itr->pos[1] / 10 == pl) {
			rawid.push_back(itr->b[1].rawid);
		}
		else if (itr->pos[0] / 10 == pl) {
			rawid.push_back(itr->b[0].rawid);
		}
	}
	std::map<int, netscan::base_track_t *>base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_map.insert(std::make_pair(itr->rawid, &(*itr)));
	}
	std::vector<netscan::base_track_t> ret;

	for (auto itr = rawid.begin(); itr != rawid.end(); itr++) {
		auto res = (base_map.find(*itr));
		if (res == base_map.end())continue;
		ret.push_back(*(base_map.find(*itr)->second));
	}
	return ret;

}