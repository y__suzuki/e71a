#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <chrono>
#include <map>
#include <omp.h>

std::vector<std::pair<int, int>> bvxx_matching_0(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1);
std::vector<std::pair<int, int>> bvxx_matching_1(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1);
std::vector<std::pair<int, int>> bvxx_matching_2(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1);
std::vector<std::pair<int, int>> bvxx_matching_3(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1);

int use_thread(double ratio, bool output);

int main() {

	std::string file_in_bvxx0 = "I:\\NINJA\\E71a\\work\\suzuki\\test\\b130_thick_0.sel.vxx";
	std::string file_in_bvxx1 = "I:\\NINJA\\E71a\\work\\suzuki\\test\\b130_thick_3.sel.vxx";
	int pl = 130;
	vxx::BvxxReader br;
	std::vector<vxx::CutArea> area;
	area.push_back(vxx::CutArea(30000, 70000, 30000, 70000, -1.0, 1.0, -1.0, 1.0));//xmin, xmax, ymin, ymax, axmin, axmax, aymin, aymax

	std::vector<vxx::base_track_t> base0 = br.ReadAll(file_in_bvxx0, pl, 0, vxx::opt::a = area);
	std::vector<vxx::base_track_t> base1 = br.ReadAll(file_in_bvxx1, pl, 0, vxx::opt::a = area);

	auto time0 = std::chrono::system_clock::now(); // 計測開始時間

	///////////////////// 処理/////////////////////////////
	std::vector<std::pair<int, int>> res0 = bvxx_matching_0(base0, base1);
	auto time1 = std::chrono::system_clock::now(); // 計測終了時間
	std::vector<std::pair<int, int>> res1 = bvxx_matching_1(base0, base1);
	auto time2 = std::chrono::system_clock::now(); // 計測終了時間
	std::vector<std::pair<int, int>> res2 = bvxx_matching_2(base0, base1);
	auto time3 = std::chrono::system_clock::now(); // 計測終了時間
	std::vector<std::pair<int, int>> res3 = bvxx_matching_3(base0, base1);
	auto time4 = std::chrono::system_clock::now(); // 計測終了時間

	//////////////////////////////////////////////////////

	double elapsed;
	elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(time1 - time0).count();
	printf("elapsed %.1lf[s]\n", elapsed / 1000);
	elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(time2 - time1).count();
	printf("elapsed %.1lf[s]\n", elapsed / 1000);
	elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(time3 - time2).count();
	printf("elapsed %.1lf[s]\n", elapsed / 1000);
	elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(time4 - time3).count();
	printf("elapsed %.1lf[s]\n", elapsed / 1000);

	system("pause");
}

std::vector<std::pair<int, int>> bvxx_matching_0(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1) {
	std::vector<std::pair<int, int>> ret;

	double all_pos_lateral = 5;
	double all_pos_radial[2] = { 5,5 };
	double all_ang_lateral = 0.005;
	double all_ang_radial[2] = { 0.005,0.01 };

	double diff_pos_lateral;
	double diff_pos_radial;
	double diff_ang_lateral;
	double diff_ang_radial;

	double angle;
	int all = base0.size(), count = 0;
	for (auto itr0 = base0.begin(); itr0 != base0.end(); itr0++) {
		if (count % 100 == 0) {
			printf("\r matching... %d/%d (%4.1lf%%)", count, all, 100.*count / all);
		}
		count++;
		angle = sqrt(itr0->ax*itr0->ax + itr0->ay*itr0->ay);
		for (auto itr1 = base1.begin(); itr1 != base1.end(); itr1++) {
			if (angle < 0.01) {
				diff_pos_lateral = itr0->x - itr1->x;
				diff_pos_radial = itr0->y - itr1->y;
				diff_ang_lateral = itr0->ax - itr1->ax;
				diff_ang_radial = itr0->ay - itr1->ay;
			}
			else {
				diff_pos_lateral = ((itr0->x - itr1->x)*itr0->ay - (itr0->y - itr1->y)*itr0->ax) / angle;
				diff_pos_radial = ((itr0->x - itr1->x)*itr0->ax + (itr0->y - itr1->y)*itr0->ay) / angle;
				diff_ang_lateral = ((itr0->ax - itr1->ax)*itr0->ay - (itr0->ay - itr1->ay)*itr0->ax) / angle;
				diff_ang_radial = ((itr0->ax - itr1->ax)*itr0->ax + (itr0->ay - itr1->ay)*itr0->ay) / angle;
			}
			if (fabs(diff_pos_lateral) < all_pos_lateral)continue;
			if (fabs(diff_pos_radial) < all_pos_radial[0] + all_pos_radial[1] * angle)continue;
			if (fabs(diff_ang_lateral) < all_ang_lateral)continue;
			if (fabs(diff_ang_radial) < all_ang_radial[0] + all_pos_radial[1] * angle)continue;
			ret.push_back(std::make_pair(itr0->rawid, itr1->rawid));
		}
	}
	printf("\r matching... %d/%d (%4.1lf%%)\n", count, all, 100.*count / all);
	printf("matched track %d\n", ret.size());
	return ret;
}
std::vector<std::pair<int, int>> bvxx_matching_1(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1) {
	//割り算-->掛け算にする
	std::vector<std::pair<int, int>> ret;

	double all_pos_lateral = 5;
	double all_pos_radial[2] = { 5,5 };
	double all_ang_lateral = 0.005;
	double all_ang_radial[2] = { 0.005,0.01 };

	double diff_pos_lateral;
	double diff_pos_radial;
	double diff_ang_lateral;
	double diff_ang_radial;

	double angle;
	int all = base0.size(), count = 0;
	for (auto itr0 = base0.begin(); itr0 != base0.end(); itr0++) {
		if (count % 100 == 0) {
			printf("\r matching... %d/%d (%4.1lf%%)", count, all, 100.*count / all);
		}
		count++;
		angle = sqrt(itr0->ax*itr0->ax + itr0->ay*itr0->ay);
		for (auto itr1 = base1.begin(); itr1 != base1.end(); itr1++) {
			if (angle < 0.01) {
				diff_pos_lateral = itr0->x - itr1->x;
				diff_pos_radial = itr0->y - itr1->y;
				diff_ang_lateral = itr0->ax - itr1->ax;
				diff_ang_radial = itr0->ay - itr1->ay;
			}
			else {
				diff_pos_lateral = ((itr0->x - itr1->x)*itr0->ay - (itr0->y - itr1->y)*itr0->ax);
				diff_pos_radial = ((itr0->x - itr1->x)*itr0->ax + (itr0->y - itr1->y)*itr0->ay);
				diff_ang_lateral = ((itr0->ax - itr1->ax)*itr0->ay - (itr0->ay - itr1->ay)*itr0->ax);
				diff_ang_radial = ((itr0->ax - itr1->ax)*itr0->ax + (itr0->ay - itr1->ay)*itr0->ay);
			}
			if (fabs(diff_pos_lateral) < all_pos_lateral*angle)continue;
			if (fabs(diff_pos_radial) < (all_pos_radial[0] + all_pos_radial[1] * angle)*angle)continue;
			if (fabs(diff_ang_lateral) < all_ang_lateral*angle)continue;
			if (fabs(diff_ang_radial) < (all_ang_radial[0] + all_pos_radial[1] * angle)*angle)continue;
			ret.push_back(std::make_pair(itr0->rawid, itr1->rawid));
		}
	}
	printf("\r matching... %d/%d (%4.1lf%%)\n", count, all, 100.*count / all);
	printf("matched track %d\n", ret.size());
	return ret;
}
std::vector<std::pair<int, int>> bvxx_matching_2(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1) {
	//割り算-->掛け算にする
	//hash化
	std::vector<std::pair<int, int>> ret;

	double hash_size = 500;
	double x_min, y_min;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (itr == base1.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		y_min = std::min(itr->y, y_min);
	}

	std::multimap<std::pair<int, int>, vxx::base_track_t*>base1_hash;

	std::pair<int, int> id;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		id.first = (itr->x - x_min) / hash_size;
		id.second = (itr->y - y_min) / hash_size;
		base1_hash.insert(std::make_pair(id, &(*itr)));
	}

	double all_pos_lateral = 5;
	double all_pos_radial[2] = { 5,5 };
	double all_ang_lateral = 0.005;
	double all_ang_radial[2] = { 0.005,0.01 };

	double diff_pos_lateral;
	double diff_pos_radial;
	double diff_ang_lateral;
	double diff_ang_radial;

	double angle;
	int all = base0.size(), count = 0;
	int ix, iy;
	for (auto itr0 = base0.begin(); itr0 != base0.end(); itr0++) {
		if (count % 100 == 0) {
			printf("\r matching... %d/%d (%4.1lf%%)", count, all, 100.*count / all);
		}
		count++;
		angle = sqrt(itr0->ax*itr0->ax + itr0->ay*itr0->ay);
		ix = (itr0->x - x_min) / hash_size;
		iy = (itr0->y - y_min) / hash_size;
		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				id.first = ix + iix;
				id.second = iy + iiy;
				if (base1_hash.count(id) == 0)continue;
				auto range = base1_hash.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					auto itr1 = res->second;
					if (angle < 0.01) {
						diff_pos_lateral = itr0->x - itr1->x;
						diff_pos_radial = itr0->y - itr1->y;
						diff_ang_lateral = itr0->ax - itr1->ax;
						diff_ang_radial = itr0->ay - itr1->ay;
					}
					else {
						diff_pos_lateral = ((itr0->x - itr1->x)*itr0->ay - (itr0->y - itr1->y)*itr0->ax);
						diff_pos_radial = ((itr0->x - itr1->x)*itr0->ax + (itr0->y - itr1->y)*itr0->ay);
						diff_ang_lateral = ((itr0->ax - itr1->ax)*itr0->ay - (itr0->ay - itr1->ay)*itr0->ax);
						diff_ang_radial = ((itr0->ax - itr1->ax)*itr0->ax + (itr0->ay - itr1->ay)*itr0->ay);
					}
					if (fabs(diff_pos_lateral) < all_pos_lateral*angle)continue;
					if (fabs(diff_pos_radial) < (all_pos_radial[0] + all_pos_radial[1] * angle)*angle)continue;
					if (fabs(diff_ang_lateral) < all_ang_lateral*angle)continue;
					if (fabs(diff_ang_radial) < (all_ang_radial[0] + all_pos_radial[1] * angle)*angle)continue;
					ret.push_back(std::make_pair(itr0->rawid, itr1->rawid));
				}
			}

		}
	}
	printf("\r matching... %d/%d (%4.1lf%%)\n", count, all, 100.*count / all);
	printf("matched track %d\n", ret.size());
	return ret;
}
std::vector<std::pair<int, int>> bvxx_matching_3(std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1) {
	//割り算-->掛け算にする
	//ompによる並列計算
	std::vector<std::pair<int, int>> ret;
	double CPU_ratio = 0.5;

	double all_pos_lateral = 5;
	double all_pos_radial[2] = { 5,5 };
	double all_ang_lateral = 0.005;
	double all_ang_radial[2] = { 0.005,0.01 };

	double diff_pos_lateral;
	double diff_pos_radial;
	double diff_ang_lateral;
	double diff_ang_radial;

	double angle;
	int all = base0.size(), count = 0;

	for (auto itr0 = base0.begin(); itr0 != base0.end(); itr0++) {
		if (count % 100 == 0) {
			printf("\r matching... %d/%d (%4.1lf%%)", count, all, 100.*count / all);
		}
		count++;
		angle = sqrt(itr0->ax*itr0->ax + itr0->ay*itr0->ay);
#pragma omp parallel for num_threads(use_thread(CPU_ratio,false)) schedule(static) private(diff_pos_lateral,diff_pos_radial,diff_ang_lateral,diff_ang_radial)
		for (int i = 0; i < base1.size(); i++) {
			vxx::base_track_t* itr1 = &(base1[i]);
			if (angle < 0.01) {
				diff_pos_lateral = itr0->x - itr1->x;
				diff_pos_radial = itr0->y - itr1->y;
				diff_ang_lateral = itr0->ax - itr1->ax;
				diff_ang_radial = itr0->ay - itr1->ay;
			}
			else {
				diff_pos_lateral = ((itr0->x - itr1->x)*itr0->ay - (itr0->y - itr1->y)*itr0->ax);
				diff_pos_radial = ((itr0->x - itr1->x)*itr0->ax + (itr0->y - itr1->y)*itr0->ay);
				diff_ang_lateral = ((itr0->ax - itr1->ax)*itr0->ay - (itr0->ay - itr1->ay)*itr0->ax);
				diff_ang_radial = ((itr0->ax - itr1->ax)*itr0->ax + (itr0->ay - itr1->ay)*itr0->ay);
			}
			if (fabs(diff_pos_lateral) < all_pos_lateral*angle)continue;
			if (fabs(diff_pos_radial) < (all_pos_radial[0] + all_pos_radial[1] * angle)*angle)continue;
			if (fabs(diff_ang_lateral) < all_ang_lateral*angle)continue;
			if (fabs(diff_ang_radial) < (all_ang_radial[0] + all_pos_radial[1] * angle)*angle)continue;
#pragma omp critical
			ret.push_back(std::make_pair(itr0->rawid, itr1->rawid));
		}
	}
	printf("\r matching... %d/%d (%4.1lf%%)\n", count, all, 100.*count / all);
	printf("matched track %d\n", ret.size());
	return ret;
}


int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
