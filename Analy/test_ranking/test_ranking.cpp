#include <fstream>
#include <iostream>
#include <ios>
#include <iomanip> 
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cassert>
#include <map>



class micro_track_subset_t {
public:
	double ax, ay;
	double z;
	int ph;
	int pos, col, row, zone, isg;
	int64_t rawid;
};

class base_track_t {
public:
	double ax, ay;
	double x, y, z;
	int pl;
	int isg, zone;
	int dmy;    // In ROOT, you will have to add this member because CINT does not handle 8byte alignment. 
	int64_t rawid;
	micro_track_subset_t m[2];
};
bool read_basetrack_txt(std::string filename, std::vector<base_track_t> &base, int output);
void output_ranking(std::string filename, std::vector<base_track_t> &base);
std::vector<std::string> StringSplit(std::string str);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg input-bvxx-txt output-file\n");
		fprintf(stderr, "output file format\n");
		fprintf(stderr, "*******************************\n");
		fprintf(stderr, "$1:microtrack0-rawid\n");
		fprintf(stderr, "$2:microtrack1-rawid\n");
		fprintf(stderr, "$3:track angle(basetrack)\n");
		fprintf(stderr, "$4:angle difference (x axis)\n");
		fprintf(stderr, "$5:angle difference lateral(x axis)\n");
		fprintf(stderr, "$6:VPH sum (y axis)\n");
		fprintf(stderr, "*******************************\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	std::string file_out = argv[2];
	std::vector<base_track_t> base;

	read_basetrack_txt(file_in_base, base, 1);
	output_ranking(file_out, base);


}



bool read_basetrack_txt(std::string filename, std::vector<base_track_t> &base, int output) {

	std::ifstream ifs(filename);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (output == 1) {
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
	}

	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		base_track_t basetrack;

		basetrack.rawid = stoi(str_v[0]);
		basetrack.pl = stoi(str_v[1]);
		basetrack.isg = stoi(str_v[2]);
		basetrack.ax = stod(str_v[3]);
		basetrack.ay = stod(str_v[4]);
		basetrack.x = stod(str_v[5]);
		basetrack.y = stod(str_v[6]);

		basetrack.m[0].ph = stoi(str_v[7]);
		basetrack.m[0].ax = stod(str_v[8]);
		basetrack.m[0].ay = stod(str_v[9]);
		basetrack.m[0].z = stod(str_v[12]);

		basetrack.m[0].pos = stoi(str_v[13]);
		basetrack.m[0].col = stoi(str_v[14]);
		basetrack.m[0].row = stoi(str_v[15]);
		basetrack.m[0].zone = stoi(str_v[16]);
		basetrack.m[0].isg = stoi(str_v[17]);
		basetrack.m[0].rawid = stoi(str_v[18]);

		basetrack.m[1].ph = stoi(str_v[19]);
		basetrack.m[1].ax = stod(str_v[20]);
		basetrack.m[1].ay = stod(str_v[21]);
		basetrack.m[1].z = stod(str_v[24]);

		basetrack.m[1].pos = stoi(str_v[25]);
		basetrack.m[1].col = stoi(str_v[26]);
		basetrack.m[1].row = stoi(str_v[27]);
		basetrack.m[1].zone = stoi(str_v[28]);
		basetrack.m[1].isg = stoi(str_v[29]);
		basetrack.m[1].rawid = stoi(str_v[30]);

		basetrack.dmy = 0;
		basetrack.zone = basetrack.m[0].zone;

		base.push_back(basetrack);
		if (cnt % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			if (output == 1) {
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
		}
		cnt++;

	}
	auto size1 = eofpos - begpos;
	if (output == 1) {
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;
	}
	if (cnt == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return true;

}
void output_ranking(std::string filename, std::vector<base_track_t> &base) {
	std::ofstream ofs(filename);

	if (!ofs) {
		//file open ���s
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (base.size() == 0) {
		fprintf(stderr, "target Chain Information ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		double rank_x, rank_y, rank_xl, angle;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write basetrack ranking ... %d/%d (%4.1lf%%)", count, int(base.size()), count*100. / base.size());
			}
			count++;
			angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
			rank_x = sqrt(pow(itr->m[0].ax - itr->ax, 2) + pow(itr->m[1].ax - itr->ax, 2) + pow(itr->m[0].ay - itr->ay, 2) + pow(itr->m[1].ay - itr->ay, 2));
			rank_y = (itr->m[0].ph + itr->m[1].ph) % 10000;
			rank_xl = sqrt(pow((itr->ax*itr->m[0].ay - itr->ay*itr->m[0].ax) / angle, 2) + pow((itr->ax*itr->m[1].ay - itr->ay*itr->m[1].ax) / angle, 2));

			ofs << std::right << std::fixed
				<< std::setw(10) << std::setprecision(0) << itr->m[0].rawid << " "
				<< std::setw(10) << std::setprecision(0) << itr->m[1].rawid << " "
				<< std::setw(7) << std::setprecision(4) << angle << " "
				<< std::setw(4) << std::setprecision(4) << rank_x << " "
				<< std::setw(7) << std::setprecision(4) << rank_xl << " "
				<< std::setw(7) << std::setprecision(0) << rank_y << std::endl;
		}
		fprintf(stderr, "\r Write basetrack ranking ... %d/%d (%4.1lf%%)\n", count, int(base.size()), count*100. / base.size());

	}

}
std::vector<std::string> StringSplit(std::string str) {
	std::stringstream ss{ str };
	std::vector<std::string> v;
	std::string buf;
	while (std::getline(ss, buf, ' ')) {
		if (buf != "") {
			v.push_back(buf);
		}
	}
	return v;
}
