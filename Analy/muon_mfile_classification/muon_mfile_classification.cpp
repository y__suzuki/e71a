#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>
class mfile_area {
public:
	std::map<int, double> x_min, x_max, y_min, y_max, z;
};
std::vector<std::vector<mfile0::M_Base>> group_divide(mfile0::Mfile&m);
std::vector<std::vector<mfile0::M_Base>> divide_single_chain(std::vector<std::vector<mfile0::M_Base>>&group, std::vector<std::vector<mfile0::M_Base>>&multi_group);

mfile0::M_Chain Get_chain(std::vector<mfile0::M_Base>&basetracks, int &chainid);

std::vector<mfile0::M_Chain> divide_penetrate(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&stop, int veto_pl);
std::vector<mfile0::M_Chain> divide_edge_out(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&edge_out, mfile_area &area, double edge_cut, int ex_pl_max);
std::vector<mfile0::M_Chain> divide_IronECC(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&iron, int veto_pl);

mfile_area read_mfile_area(std::string filename);
void output_mfile(std::string filename, std::vector<mfile0::M_Chain>&chain, mfile0::M_Header &header);
void output_upstream_track(std::ofstream &ofs, std::vector<mfile0::M_Chain>&chain);

std::vector<std::vector<mfile0::M_Base>> divide_mutli(std::vector<std::vector<mfile0::M_Base>>&all, std::vector<std::vector<mfile0::M_Base>>&single);

bool sort_M_base(const mfile0::M_Base &left, const mfile0::M_Base &right) {
	return left.pos < right.pos;
}
int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage: prg file-in-mfile(txt) mfile_area.txt output-path\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_area = argv[2];
	std::string file_out_path = argv[3];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	mfile_area area = read_mfile_area(file_in_area);

	std::vector<std::vector<mfile0::M_Base>> group = group_divide(m);

	std::vector<std::vector<mfile0::M_Base>> single_chain, multi_chain;

	//single,multi chainの分離
	single_chain = divide_single_chain(group, multi_chain);
	std::vector<std::vector<mfile0::M_Base>> multi_single_chain;
	multi_chain = divide_mutli(multi_chain, multi_single_chain);

	std::vector<mfile0::M_Chain> single_chain_v;
	int chainid = 0;
	//chainの形成
	for (auto itr = single_chain.begin(); itr != single_chain.end(); itr++) {
		single_chain_v.push_back(Get_chain((*itr), chainid));
	}
	for (auto itr = multi_single_chain.begin(); itr != multi_single_chain.end(); itr++) {
		single_chain_v.push_back(Get_chain((*itr), chainid));
	}

	//penetrate check
	std::vector<mfile0::M_Chain> penetrate;
	//最上流がPL132以上
	single_chain_v = divide_penetrate(single_chain_v, penetrate, 132);

	//edgeout check
	std::vector<mfile0::M_Chain> edge_out;
	//最上流から4PL外挿,edgeから5mm以内に入ったらedge out
	single_chain_v = divide_edge_out(single_chain_v, edge_out, area, 0, 4);

	//Iron ECC event
	std::vector<mfile0::M_Chain> iron_ecc;
	single_chain_v = divide_IronECC(single_chain_v, iron_ecc, 15);


	printf("stop = %d\n", single_chain_v.size());

	//multi chainの出力
	std::vector<mfile0::M_Chain> multi;
	for (auto itr = multi_chain.begin(); itr != multi_chain.end(); itr++) {
		for (auto itr2 = itr->begin(); itr2 != itr->end(); itr2++) {
			std::vector<mfile0::M_Base> base_tmp;
			base_tmp.push_back(*itr2);
			multi.push_back(Get_chain(base_tmp, chainid));
		}
	}

	std::string file_out_multi = file_out_path + "\\muon_multi.all";

	std::string file_out_penetrate = file_out_path + "\\muon_penetrate.all";
	std::string file_out_iron= file_out_path + "\\muon_ironecc.all";
	std::string file_out_edgeout = file_out_path + "\\muon_edgeouot.all";
	std::string file_out_stop = file_out_path + "\\muon_stop.all";
	std::string file_up_inf = file_out_path + "\\muon_stop.txt";

	output_mfile(file_out_multi, multi, m.header);
	output_mfile(file_out_penetrate, penetrate, m.header);
	output_mfile(file_out_iron, iron_ecc, m.header);
	output_mfile(file_out_edgeout, edge_out, m.header);
	output_mfile(file_out_stop, single_chain_v, m.header);

	std::ofstream ofs(file_up_inf);
	//output_upstream_track(ofs, penetrate);
	//output_upstream_track(ofs, edge_out);
	output_upstream_track(ofs, single_chain_v);
}
mfile_area read_mfile_area(std::string filename) {
	mfile_area ret;
	std::ifstream ifs(filename);
	double xmin, ymin, xmax, ymax, z;
	int pl;
	//	1   1157.7 250914.1 - 3778.3 242202.9 - 280.0

	while (ifs >> pl >> xmin >> xmax >> ymin >> ymax >> z) {
		ret.x_min.insert(std::make_pair(pl, xmin));
		ret.x_max.insert(std::make_pair(pl, xmax));
		ret.y_min.insert(std::make_pair(pl, ymin));
		ret.y_max.insert(std::make_pair(pl, ymax));
		ret.z.insert(std::make_pair(pl, z));
	}

	return ret;
}
std::vector<std::vector<mfile0::M_Base>> group_divide(mfile0::Mfile&m) {

	std::vector<std::vector<mfile0::M_Base>> ret;
	std::multimap<int64_t, mfile0::M_Base> group_map;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {

			group_map.insert(std::make_pair(itr2->group_id, *itr2));

		}
	}

	int count = 0;
	for (auto itr = group_map.begin(); itr != group_map.end(); itr++) {
		count = group_map.count(itr->first);

		std::vector<mfile0::M_Base> base_v;
		base_v.reserve(count);
		auto range = group_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_v.push_back(res->second);
		}
		ret.push_back(base_v);


		itr = std::next(itr, count - 1);
	}
	return ret;

}

std::vector<std::vector<mfile0::M_Base>> divide_single_chain(std::vector<std::vector<mfile0::M_Base>>&group, std::vector<std::vector<mfile0::M_Base>>&multi_group) {
	std::vector<std::vector<mfile0::M_Base>> ret;
	for (int i = 0; i < group.size(); i++) {
		std::set<int> pos_all;
		bool flg = false;
		for (auto itr = group[i].begin(); itr != group[i].end(); itr++) {
			auto res=pos_all.insert(itr->pos);
			if (!res.second) {
				flg = true;
			}
		}
		if (flg) {
			multi_group.push_back(group[i]);
		}
		else {
			ret.push_back(group[i]);
		}
	}
	printf("multi = %d\n", multi_group.size());
	printf("single = %d\n", ret.size());
	return ret;
}

mfile0::M_Chain Get_chain(std::vector<mfile0::M_Base>&basetracks,int &chainid) {
	sort(basetracks.begin(), basetracks.end(), sort_M_base);
	mfile0::M_Chain ret;
	ret.basetracks = basetracks;
	ret.chain_id = chainid;
	chainid++;
	ret.nseg = ret.basetracks.size();
	ret.pos0 = ret.basetracks.begin()->pos;
	ret.pos1 = ret.basetracks.rbegin()->pos;
	return ret;
}

std::vector<mfile0::M_Chain> divide_penetrate(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&penetrate, int veto_pl) {
	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		if (up_pl >= veto_pl) {
			penetrate.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("penetrate = %d\n", penetrate.size());
	return ret;
}

std::vector<mfile0::M_Chain> divide_IronECC(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&iron, int veto_pl) {
	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		if (up_pl <= veto_pl) {
			iron.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("IronECC = %d\n", iron.size());
	return ret;
}

std::vector<mfile0::M_Chain> divide_edge_out(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&edge_out, mfile_area &area,double edge_cut,int ex_pl_max) {
	
	
	double xmin, xmax, ymin, ymax;
	
	
	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	double up_z, up_x, up_y, up_ax, up_ay, ex_z, ex_x, ex_y;
	bool flg = false;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		flg = false;
		up_z = area.z.at(up_pl);
		up_x = itr->basetracks.rbegin()->x;
		up_y = itr->basetracks.rbegin()->y;
		up_ax = itr->basetracks.rbegin()->ax;
		up_ay = itr->basetracks.rbegin()->ay;
		for (int ex_pl = 0; ex_pl <= ex_pl_max; ex_pl++) {
			if (area.z.count(up_pl + ex_pl) == 0)continue;
			ex_z = area.z.at(up_pl + ex_pl);
			ex_x = up_x + up_ax * (ex_z - up_z);
			ex_y = up_y + up_ay * (ex_z - up_z);
			if (area.x_min.at(up_pl + ex_pl) + edge_cut > ex_x)flg = true;
			if (area.x_max.at(up_pl + ex_pl) - edge_cut < ex_x)flg = true;
			if (area.y_min.at(up_pl + ex_pl) + edge_cut > ex_y)flg = true;
			if (area.y_max.at(up_pl + ex_pl) - edge_cut < ex_y)flg = true;
		}
		if (flg) {
			edge_out.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("edge out = %d\n", edge_out.size());
	return ret;
}

void output_mfile(std::string filename, std::vector<mfile0::M_Chain>&chain,mfile0::M_Header &header) {

	mfile0::Mfile m;
	m.header = header;
	m.chains = chain;
	mfile0::write_mfile(filename, m);

}

void output_upstream_track(std::ofstream &ofs, std::vector<mfile0::M_Chain>&chain) {
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->basetracks.rbegin()->pos / 10 << " "
			<< std::setw(10) << std::setprecision(0) << itr->basetracks.rbegin()->group_id << " "
			<< std::setw(10) << std::setprecision(0) << itr->basetracks.rbegin()->rawid << " "
			<< std::setw(7) << std::setprecision(0) << itr->basetracks.rbegin()->ph << " "
			<< std::setw(7) << std::setprecision(4) << itr->basetracks.rbegin()->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->basetracks.rbegin()->ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->basetracks.rbegin()->x << " "
			<< std::setw(8) << std::setprecision(1) << itr->basetracks.rbegin()->y << " "
			<< std::setw(8) << std::setprecision(1) << itr->basetracks.rbegin()->z << std::endl;
	}
}

std::vector<std::vector<mfile0::M_Base>> divide_mutli(std::vector<std::vector<mfile0::M_Base>>&all, std::vector<std::vector<mfile0::M_Base>>&single) {
	std::vector<std::vector<mfile0::M_Base>> ret;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		bool flg = false;
		std::multimap<int, mfile0::M_Base> base_map;
		for (auto itr2 = itr->begin(); itr2 != itr->end(); itr2++) {
			base_map.insert(std::make_pair(itr2->pos / 10, *itr2));
		}
		//最初はmuonなのでmultiになりえない
		mfile0::M_Base prev=base_map.begin()->second;
		int count;
		std::vector<mfile0::M_Base> sel;
		for (auto itr2 = base_map.begin(); itr2 != base_map.end(); itr2++) {
			count = base_map.count(itr2->first);
			if (count == 1) {
				prev = itr2->second;
				sel.push_back(prev);
			}
			//2track以上ある場合
			else {
				//距離を比較。離れていたらmutli-->flgをtrue
				std::vector<mfile0::M_Base> multi_base;
				auto range = base_map.equal_range(itr2->first);
				for (auto res = range.first; res != range.second; res++) {
					multi_base.push_back(res->second);
				}
				double dist;
				for (int i = 0; i < multi_base.size(); i++) {
					for (int j = i + 1; j < multi_base.size(); j++) {
						dist = sqrt(pow(multi_base[i].x - multi_base[j].x, 2) + pow(multi_base[i].x - multi_base[j].x, 2));
						if (dist > 500)flg = true;
					}
				}
				//近接なら良いほうをselにpush back
				if (!flg) {
					double ex_x, ex_y,gap;
					gap = multi_base.begin()->z - prev.z;
					ex_x = prev.x + prev.ax*(gap);
					ex_y = prev.y + prev.ay*(gap);
					for (int i = 0; i < multi_base.size(); i++) {
						if (i == 0) {
							dist = sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2));
							prev = multi_base[i];
						}
						if (dist > sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2))) {
							dist = sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2));
							prev = multi_base[i];
						}
					}
					sel.push_back(prev);
				}
			}

			itr2 = std::next(itr2, count - 1);
		}
		//近接にしかmultiがなかった場合
		if (!flg) {
			single.push_back(sel);
			//single.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("mutli --> single %d\n", single.size());
	printf("mutli --> mutli  %d\n", ret.size());
	return ret;
}

