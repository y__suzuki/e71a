#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <omp.h>
#include <set>

void write_eventid(mfile0::Mfile m);

int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "uasge:prg\n");
		exit(1);
	}
	std::string filename = argv[1];
	mfile0::Mfile m;
	mfile0::read_mfile(filename, m);

	write_eventid(m);
}
void write_eventid(mfile0::Mfile m) {

	std::set<int> eventid;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		eventid.insert(itr->basetracks.begin()->group_id / 100000);


	}
	for (auto itr = eventid.begin(); itr != eventid.end(); itr++) {
		printf("%d\n",*itr);
	}
}