#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include <functions.hpp>
# include <iostream>
# include <random>
#include "VxxReader.h"
#include <list>
#include <set>
class stop_track {
public:
	int64_t chainid, groupid;
	int  nseg, npl, pl0, pl1, ph, rawid;
	double ax, ay, x, y, z;
};
void read_stop_txt(std::string filename, std::map<int, std::vector<stop_track>> &stop);
std::vector<vxx::base_track_t> stop_selection(std::vector<vxx::base_track_t>&base, std::vector<stop_track>&stop);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg stop.txt ECC_file_path\n");
		exit(1);
	}

	std::string file_in_stop = argv[1];
	std::string file_in_ECC = argv[2];
	int PL = 91;
	std::map<int, std::vector<stop_track>> stop;
	read_stop_txt(file_in_stop, stop);
	auto stop_sel = stop.at(PL);

	std::stringstream file_in_base;
	file_in_base << file_in_ECC << "\\Area0\\PL"<<std::setw(3)<<std::setfill('0')<<PL<<"\\b" << std::setw(3) << std::setfill('0') << PL << ".sel.cor.vxx";
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), PL, 0);
	std::vector<vxx::base_track_t> stop_base = stop_selection(base, stop_sel);

	std::stringstream file_out_base;
	file_out_base <<"b" << std::setw(3) << std::setfill('0') << PL << ".sel.cor.vxx";

	vxx::BvxxWriter bw;
	bw.Write(file_out_base.str(), PL, 0, stop_base);
}


void read_stop_txt(std::string filename, std::map<int, std::vector<stop_track>> &stop) {
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		stop_track stop_tmp;
		stop_tmp.chainid = stoll(str_v[0]);
		stop_tmp.nseg = stoi(str_v[1]);
		stop_tmp.pl0 = stoi(str_v[2]);
		stop_tmp.pl1 = stoi(str_v[3]);
		stop_tmp.groupid = stoll(str_v[4]);
		stop_tmp.rawid = stoi(str_v[5]);
		stop_tmp.ph = stoi(str_v[6]);
		stop_tmp.ax = stod(str_v[7]);
		stop_tmp.ay = stod(str_v[8]);
		stop_tmp.x = stod(str_v[9]);
		stop_tmp.y = stod(str_v[10]);
		stop_tmp.z = stod(str_v[11]);
		stop_tmp.npl = stop_tmp.pl1 - stop_tmp.pl0 + 1;
		cnt++;
		auto res = stop.find(stop_tmp.pl1);
		if (res == stop.end()) {
			std::vector<stop_track> tmp_s{ stop_tmp };
			stop.insert(std::make_pair(stop_tmp.pl1, tmp_s));
		}
		else {
			res->second.push_back(stop_tmp);
		}
	}

	printf("input fin %d track\n", cnt);
}
std::vector<vxx::base_track_t> stop_selection(std::vector<vxx::base_track_t>&base, std::vector<stop_track>&stop) {
	std::vector<vxx::base_track_t> ret;
	std::set<int> rawid;
	for (auto itr = stop.begin(); itr != stop.end(); itr++) {
		rawid.insert(itr->rawid);
	}
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (rawid.count(itr->rawid) == 0)continue;
		ret.push_back(*itr);
	}
	return ret;
}