//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
//大体2^50
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

//#pragma comment(lib, "VxxReader.lib")
//#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

using namespace l2c;

class output_format_micro {
public:
	int pos, view, imager, zone, isg, ph, vph, px;
};
class output_format_base {
public:
	int pl, rawid;
	double ax, ay, x, y, z;
	output_format_micro m[2];
};

class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};

std::vector<output_format_link> read_linklet_bin(std::string filename);
std::vector<std::vector<output_format_link>> make_group(std::vector<output_format_link>&link);

l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim = DEFAULT_CHAIN_UPPERLIM, bool output = false);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file_in_link file_out_link\n");
		exit(1);
	}
	std::string in_file_link = argv[1];
	std::string out_file_link = argv[2];

	std::vector<output_format_link> link, link_out;
	link = read_linklet_bin(in_file_link);
	std::vector<std::vector<output_format_link>> group=make_group(link);


}
std::vector<output_format_link> read_linklet_bin(std::string filename) {
	std::vector<output_format_link> ret;

	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	output_format_link l;
	while (ifs.read((char*)& l, sizeof(output_format_link))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(l);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;

}

std::vector<std::vector<output_format_link>> make_group(std::vector<output_format_link>&link) {

	std::set<std::pair<int32_t, int64_t>> btset;
	std::vector<Linklet> ltlist;
	std::set<int32_t> usepos_set;
	for (auto &l : link) {
		ltlist.emplace_back(l.b[0].pl * 10, l.b[1].pl * 10, l.b[0].rawid, l.b[1].rawid);
		btset.insert(std::make_pair(l.b[0].pl * 10, l.b[0].rawid));
		btset.insert(std::make_pair(l.b[1].pl * 10, l.b[1].rawid));
		usepos_set.insert(l.b[0].pl * 10);
		usepos_set.insert(l.b[1].pl * 10);
	}
	std::vector<int32_t> usepos(usepos_set.begin(), usepos_set.end());
	l2c::Cdat cdat = l2c_x(btset, ltlist, usepos, DEFAULT_CHAIN_UPPERLIM, true);

}

l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output) {
	try
	{
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		if (output) {
			l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
			return cdat;
		}
		l2c::Cdat cdat = l2c::MakeCdat_no_out(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
		return cdat;

	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}

