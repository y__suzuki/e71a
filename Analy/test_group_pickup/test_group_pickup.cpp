#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<mfile0::M_Chain> group_pickup(std::vector<mfile0::M_Chain>&c, int groupid);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:mfile groupid out-mfile");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[3];
	int groupid = std::stoi(argv[2]);

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	m.chains = group_pickup(m.chains, groupid);
	mfile1::write_mfile_extension(file_out_mfile, m);


}
std::vector<mfile0::M_Chain> group_pickup(std::vector<mfile0::M_Chain>&c, int groupid) {

	std::vector<mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->basetracks.begin()->group_id == groupid) {
			ret.push_back(*itr);
		}
	}

	return ret;
}