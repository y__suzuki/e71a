//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>
using namespace l2c;

class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};
class group {
public:
	int groupid, trackid, linklet_num, pl, rawid;
	std::vector<Linklet> link;
};
l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output);
void read_linklet_list(std::string filename, std::vector<Linklet> &ltlist);
void Linklet_reduction(std::vector<Linklet> &all_ltlist, int pl0, int pl1);
void write_linklet_list(std::string filename, std::vector<Linklet> &ltlist);

std::vector<group>read_group(std::string filename);

void Linklet_reduction(std::vector<Linklet> &all_ltlist, int pl0, int pl1);
void output_group(std::string filename, std::vector<group>&g);


int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file-in-linklet-list file-out-linklet-list20 file-out-linklet-list40 file-out-linklet-listall\n");
		exit(1);
	}
	std::string file_in_link = argv[1];
	std::string file_out_link1 = argv[2];
	std::string file_out_link2 = argv[3];
	std::string file_out_link_all = argv[4];

	std::vector<group>group_v=read_group(file_in_link);
	std::vector<group>group_v_20, group_v_40, group_v_all;
	for (int i = 0; i < group_v.size(); i++) {
		printf("group %d/%d\n", i, group_v.size());

		group g;
		g= group_v[i];
		Linklet_reduction(g.link, 1, 40);
		Linklet_reduction(g.link, 30, 70);
		Linklet_reduction(g.link, 60, 100);
		Linklet_reduction(g.link, 90, 150);
		group_v_40.push_back(g);

		g = group_v[i];
		Linklet_reduction(g.link, 1, 20);
		Linklet_reduction(g.link, 10, 50);
		Linklet_reduction(g.link, 40, 80);
		Linklet_reduction(g.link, 70, 110);
		Linklet_reduction(g.link, 100, 150);
		group_v_20.push_back(g);

		//if (group_v_20.rbegin()->link.size() != group_v_40.rbegin()->link.size()) {
		//	printf("diff %d\n", i);
		//}
		g = group_v[i];
		Linklet_reduction(g.link, 1, 150);
		group_v_all.push_back(g);
	}


	output_group(file_out_link1, group_v_20);
	output_group(file_out_link2, group_v_40);
	output_group(file_out_link_all, group_v_all);

}
std::vector<group>read_group(std::string filename) {
	std::ifstream ifs(filename);
	group g;
	std::vector<group> ret;
	while (ifs >> g.groupid >> g.trackid >> g.pl >> g.rawid >> g.linklet_num) {
		g.link.clear();
		g.link.reserve(g.linklet_num);
		for (int i = 0; i < g.linklet_num; i++) {
			linklet_header link;
			ifs >> link.pos0 >> link.pos1 >> link.raw0 >> link.raw1;
			g.link.emplace_back(link.pos0, link.pos1, link.raw0, link.raw1);
		}
		ret.push_back(g);
	}
	return ret;
}

l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output) {
	try
	{
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		if (output) {
			l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
			return cdat;
		}
		l2c::Cdat cdat = l2c::MakeCdat_no_out(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
		return cdat;

	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}

void Linklet_reduction(std::vector<Linklet> &all_ltlist, int pl0, int pl1) {
	//pl0<= PL <=pl1 の範囲でlinkletの畳み込み
	std::set<std::pair<int32_t, int64_t>> btset;
	std::vector<Linklet> ltlist, not_use_ltlist;
	std::set<int32_t> pos_set;
	for (auto itr = all_ltlist.begin(); itr != all_ltlist.end(); itr++) {
		if (itr->pos1 / 10 < pl0 || itr->pos2 / 10 > pl1) {
			not_use_ltlist.push_back(*itr);
			continue;
		}
		ltlist.push_back(*itr);
		btset.insert(std::make_pair(itr->pos1, itr->id1));
		btset.insert(std::make_pair(itr->pos2, itr->id2));
		pos_set.insert(itr->pos1);
		pos_set.insert(itr->pos2);

	}
	all_ltlist.clear();
	all_ltlist.shrink_to_fit();

	printf("PL%03d<= PL <=PL%03d linklet num =%lld(not use %lld)\n", pl0, pl1, ltlist.size(), not_use_ltlist.size());
	std::vector<int32_t> usepos(pos_set.begin(), pos_set.end());

	l2c::Cdat cdat = l2c_x(btset, ltlist, usepos, 1000, false);
	ltlist.clear();
	size_t possize = usepos.size();

	size_t grsize = cdat.GetNumOfGroups();
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
		auto link_list = gr.GetLinklets();
		for (auto itr = link_list.begin(); itr != link_list.end(); itr++) {
			int32_t btpl0 = usepos[itr->first.GetPL()];
			int64_t btid0 = itr->first.GetRawID();
			int32_t btpl1 = usepos[itr->second.GetPL()];
			int64_t btid1 = itr->second.GetRawID();
			ltlist.emplace_back(btpl0, btpl1, btid0, btid1);
		}
		size_t chsize = gr.GetNumOfChains();
		//debug
		for (size_t ich = 0; ich < chsize; ++ich)
		{
			Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
			int64_t chid = ch.GetID();
			int32_t nseg = ch.GetNSeg();
			int32_t spl = ch.GetStartPL();
			int32_t epl = ch.GetEndPL();
			fprintf(stdout, "    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d\n", chid, nseg, spl, epl);

			for (size_t pl = 0; pl < possize; ++pl)
			{
				//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
				//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
				BaseTrackID bt = ch.GetBaseTrack(pl);
				if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
				int32_t btpl = bt.GetPL();
				int64_t btid = bt.GetRawID();
				//btplとplは厳密に一致しなければおかしい。
				if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
				if (btset.find(std::make_pair(usepos[btpl], btid)) == btset.end())
				{
					//エラーチェック。
					//Chain内の全てのBaseTrackはbtset（ファイルから読み込まれた全BaseTrack）に必ず含まれているはず。
					//含まれていなければエラー。
					throw std::exception("BaseTrack is not found in btset.");
				}
				fprintf(stdout, "        pl:%-3d pos:%-4d rawid:%-9lld\n", btpl, usepos[pl], btid);
			}
		}

	}
	printf("after reduction linklet num =%lld\n", ltlist.size());

	for (auto itr = ltlist.begin(); itr != ltlist.end(); itr++) {
		not_use_ltlist.push_back(*itr);
	}
	printf("all linklet num =%lld\n", not_use_ltlist.size());

	std::swap(not_use_ltlist, all_ltlist);

}


void output_group(std::string filename, std::vector<group>&g) {
	std::ofstream ofs(filename);
	int count = 0;
	for (auto itr = g.begin(); itr != g.end(); itr++) {
		if (count % 1000 == 0) {
			printf("\r wrtie file %d/%d(%4.1lf%%)", count, g.size(), count*100. / g.size());
		}
		count++;

		ofs << std::fixed << std::right
			<< std::setw(10) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(5) << std::setprecision(0) << itr->trackid << " "
			<< std::setw(4) << std::setprecision(0) << itr->pl << " "
			<< std::setw(4) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(10) << std::setprecision(0) << itr->link.size() << std::endl;
		if (itr->link.size() == 0)continue;
		for (auto itr2 = itr->link.begin(); itr2 != itr->link.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(5) << std::setprecision(0) << itr2->pos1 << " "
				<< std::setw(5) << std::setprecision(0) << itr2->pos2 << " "
				<< std::setw(10) << std::setprecision(0) << itr2->id1 << " "
				<< std::setw(10) << std::setprecision(0) << itr2->id2 << std::endl;
		}
	}
	printf("\r wrtie file %d/%d(%4.1lf%%)\n", count, g.size(), count*100. / g.size());

}