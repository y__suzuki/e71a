#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int main(int argc, char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-bvxx out-bvxx pl zone corrmap\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	std::string file_out_bvxx = argv[2];
	int pl = std::stoi(argv[3]);
	int zone = std::stoi(argv[4]);
	std::string file_in_corrmap = argv[5];

	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_bvxx, base, pl, zone);

	corrmap0::Corrmap corr_abs;
	corrmap0::read_cormap(file_in_corrmap, corr_abs);

	std::vector<netscan::base_track_t> base_inv;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_inv.push_back(netscan::base_trans_inv(*itr, corr_abs));
	}

	netscan::write_basetrack_vxx(file_out_bvxx, base_inv, pl, zone);
}