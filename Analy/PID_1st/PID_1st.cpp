#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <FILE_structure.hpp>

class Chain_PID {
public:
	uint64_t chainid;
	int nseg;
	double ax, ay, angle, vph, pb,pid,mip,hip;

};
class VPH_param {
public:
	double angle_min,angle_max,pb_min,pb_max;
	double hip_mean, hip_sigma, mip_mean, mip_sigma;
	double MIP_value(double vph);
	double HIP_value(double vph);
	double PID_value(double vph);
	void Print();

};
double VPH_param::PID_value(double vph) {
	double mip_likelihood, hip_likelihood;

	mip_likelihood = 1. / (sqrt(2 * M_PI)*mip_sigma)*exp(-1 * pow(vph - mip_mean, 2) / (2 * pow(mip_sigma, 2)));
	hip_likelihood = 1. / (sqrt(2 * M_PI)*hip_sigma)*exp(-1 * pow(vph - hip_mean, 2) / (2 * pow(hip_sigma, 2)));
	return mip_likelihood / (mip_likelihood + hip_likelihood);
}
double VPH_param::MIP_value(double vph) {
	double mip_likelihood, hip_likelihood;

	mip_likelihood = 1. / (sqrt(2 * M_PI)*mip_sigma)*exp(-1 * pow(vph - mip_mean, 2) / (2 * pow(mip_sigma, 2)));

	return mip_likelihood;
}
double VPH_param::HIP_value(double vph) {
	double mip_likelihood, hip_likelihood;

	hip_likelihood = 1. / (sqrt(2 * M_PI)*hip_sigma)*exp(-1 * pow(vph - hip_mean, 2) / (2 * pow(hip_sigma, 2)));
	return  hip_likelihood;
}

void VPH_param::Print() {
	printf("%.0lf %.0lf %.1lf %.1lf %.1lf %.1lf %.1lf %.1lf\n", pb_min, pb_max, angle_min, angle_max, mip_mean, mip_sigma, hip_mean, hip_sigma);

	return;
}


std::vector<Chain_PID >  read_Chain_inf(std::string filename);
std::vector < VPH_param> read_VPH_param(std::string filename);
void Calc_PID(std::vector<Chain_PID >&chain, std::vector<VPH_param>&vph_param);
void Calc_PID_value(Chain_PID &chain, std::vector<VPH_param>&vph_param);
void Calc_PID_value_binary(Chain_PID &chain);

void Output_chain_inf(std::string filename, std::vector<Chain_PID >&chain);


int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file_chain file_vph output_file");
	}
	std::string file_in_chain = argv[1];
	std::string file_in_PID = argv[2];
	std::string file_out = argv[3];

	std::vector<Chain_PID> chain = read_Chain_inf(file_in_chain);

	std::vector <VPH_param> vph_param = read_VPH_param(file_in_PID);
	Calc_PID(chain, vph_param);
	Output_chain_inf(file_out, chain);
}
std::vector<Chain_PID >  read_Chain_inf(std::string filename) {
	std::vector<Chain_PID > ret;
	std::ifstream ifs(filename);
	Chain_PID chain_tmp;

	uint64_t count = 0;
	while (ifs >> chain_tmp.chainid >> chain_tmp.nseg >> chain_tmp.pb >> chain_tmp.ax >> chain_tmp.ay >> chain_tmp.vph) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r read file %llu", count);
		}
		count++;
		
		chain_tmp.angle = sqrt(chain_tmp.ax*chain_tmp.ax + chain_tmp.ay*chain_tmp.ay);
		chain_tmp.mip = -1;
		chain_tmp.hip = -1;
		chain_tmp.pid = -1;
		ret.push_back(chain_tmp);
	}
	fprintf(stderr, "\r read file %llu fin\n", count);
	return ret;

}
std::vector < VPH_param> read_VPH_param(std::string filename) {
	std::vector<VPH_param > ret;
	std::ifstream ifs(filename);
	VPH_param param_tmp;
	std::string label;
	//mip 0 100 1.6 1.8 40.6432 7.64676
	double mean, sigma, pb, angle;
	uint64_t count = 0;
	while (ifs >> label >> param_tmp.pb_min >> param_tmp.pb_max >> param_tmp.angle_min >> param_tmp.angle_max >> mean >> sigma) {
		pb = (param_tmp.pb_min + param_tmp.pb_max) / 2;
		angle = (param_tmp.angle_min + param_tmp.angle_max) / 2;
		if (ret.size() == 0) {
			ret.push_back(param_tmp);
		}
		bool flg = false;
		for (auto itr = ret.begin(); itr != ret.end(); itr++) {
			if (itr->angle_min > angle)continue;
			if (itr->angle_max < angle)continue;
			if (itr->pb_min > pb)continue;
			if (itr->pb_max < pb)continue;
			flg = true;
			if (label == "mip") {
				itr->mip_mean = mean;
				itr->mip_sigma = sigma;
			}
			else if (label == "hip") {
				itr->hip_mean = mean;
				itr->hip_sigma = sigma;
			}
			else {
				fprintf(stderr, "label error\n");
				fprintf(stderr, "label=[%s]\n", label.c_str());
			}
			break;
		}
		if (!flg) {
			if (label == "mip") {
				param_tmp.mip_mean = mean;
				param_tmp.mip_sigma = sigma;
			}
			else if (label == "hip") {
				param_tmp.hip_mean = mean;
				param_tmp.hip_sigma = sigma;
			}
			else {
				fprintf(stderr, "label error\n");
				fprintf(stderr, "label=[%s]\n", label.c_str());
			}
			ret.push_back(param_tmp);
		}
	}
	return ret;
}


void Calc_PID(std::vector<Chain_PID >&chain, std::vector<VPH_param>&vph_param) {
	uint64_t count = 0;
	for (auto itr = chain.begin(); itr != chain.end();itr++) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r Calc likelihood value %d/%d(%4.1lf%%)", count, chain.size(), count*100. / chain.size());
		}
		count++;
		//Calc_PID_value(*itr, vph_param);
		Calc_PID_value_binary(*itr);
	}
	fprintf(stderr, "\r Calc likelihood value %d/%d(%4.1lf%%)\n", count, chain.size(), count*100. / chain.size());

}

void Calc_PID_value(Chain_PID &chain, std::vector<VPH_param>&vph_param) {
	bool flg = false;
	for (auto itr = vph_param.begin(); itr != vph_param.end(); itr++) {
		if (itr->angle_min > chain.angle)continue;
		if (itr->angle_max <= chain.angle)continue;
		if (itr->pb_min > chain.pb)continue;
		if (itr->pb_max <= chain.pb)continue;
		chain.mip = itr->MIP_value(chain.vph);
		chain.hip = itr->HIP_value(chain.vph);
		chain.pid = itr->PID_value(chain.vph);
		flg = true;
	}
	if (!flg) {
		chain.pid = -1;
	}
}
void Calc_PID_value_binary(Chain_PID &chain) {
		double param[4][3];
		param[0][0] = 8386.17;
		param[0][1] = -5.16376;
		param[0][2] = -10.0886;

		param[1][0] = 3319.48;
		param[1][1] = -3.71618;
		param[1][2] = 13.4601;

		param[2][0] = 305.617;
		param[2][1] = -1.56164;
		param[2][2] = 54.5582;

		param[3][0] = 171.387;
		param[3][1] = -1.24361;
		param[3][2] = 48.9496;

		int i;
		if (0 <= chain.pb&&chain.pb < 100) i = 0;
		else if (100 <= chain.pb&&chain.pb < 200)i = 1;
		else if (200 <= chain.pb&&chain.pb < 300)i = 2;
		else if (300 <= chain.pb&&chain.pb < 500)i = 3;
		else i = -1;


		if (i < 0) {
			chain.hip = -1;
			chain.mip = -1;
			chain.pid = -1;
		}
		else {
			if (param[i][0] / pow(chain.angle - param[i][1], 2) + param[i][2] > chain.vph) {
				chain.hip = 0;
				chain.mip = 1;
				chain.pid = chain.mip / (chain.mip + chain.hip);
			}
			else {
				chain.hip = 1;
				chain.mip = 0;
				chain.pid = chain.mip / (chain.mip + chain.hip);
			}
		}
}
void Output_chain_inf(std::string filename, std::vector<Chain_PID >&chain) {
	std::ofstream ofs(filename);
	uint64_t count = 0;

	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r Write Chain inf %d/%d(%4.1lf%%)", count, chain.size(), count*100. / chain.size());
		}
		count++;
		//if (itr->pid < 0)continue;
		ofs << std::right << std::fixed
			<< std::setw(20) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(4) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(12) << std::setprecision(4) << itr->pb << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(12) << std::setprecision(2) << itr->vph << " "
			<< std::setw(6) << std::setprecision(4) << itr->mip << " "
			<< std::setw(6) << std::setprecision(4) << itr->hip << " "
			<< std::setw(6) << std::setprecision(4) << itr->pid << std::endl;
	}
	fprintf(stderr, "\r Write Chain inf %d/%d(%4.1lf%%)\n", count, chain.size(), count*100. / chain.size());

}
