#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<mfile0::M_Chain> reject_iron_stop(std::vector<mfile0::M_Chain>&chains);
int iron_penetrate_flg(mfile0::M_Chain&c);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "uage:filename\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;

	mfile1::read_mfile_extension(file_in_mfile, m);
	m.chains= reject_iron_stop(m.chains);
	mfile1::write_mfile_extension(file_out_mfile, m);

}
std::vector<mfile0::M_Chain> reject_iron_stop(std::vector<mfile0::M_Chain>&chains) {
	std::vector<mfile0::M_Chain> ret;
	for (auto &c : chains) {
		int iron_flg = iron_penetrate_flg(c);
		if (iron_flg == 0)continue;
		ret.push_back(c);
	}
	return ret;
}
int iron_penetrate_flg(mfile0::M_Chain&c) {
	int pl0 = c.basetracks.begin()->pos / 10;
	int pl1 = c.basetracks.rbegin()->pos / 10;
	if (pl1 - pl0 == 0)return 0;
	else if (pl1 - pl0 == 1) {
		if (pl0 <= 4 && pl0 <= 14)return 1;
		else if (pl0 % 2 == 0)return 1;
		else return 0;
	}
	else return 1;
}