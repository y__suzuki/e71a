#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>

std::set<int> read_stop(std::string filename);

int main(int argc, char** argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-stop file-in-sharingfile-bin\n");
		exit(1);
	}
	std::string file_in_stop = argv[1];
	std::string file_in_sharing_file = argv[2];

	std::vector<Sharing_file::Sharing_file> sharing_file = Sharing_file::Read_sharing_file_bin(file_in_sharing_file);
	std::set<int> groupid = read_stop(file_in_stop);


}
std::set<int> read_stop(std::string filename) {
	std::set<int> ret;
	std::ifstream ifs(filename);
	int groupid, trackid, pl, rawid;
	while (ifs >> groupid >> trackid >> pl >> rawid) {
		ret.insert(groupid);
	}
	return ret;
}
