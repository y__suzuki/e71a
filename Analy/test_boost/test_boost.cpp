#include <iostream>
#include <string>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
using namespace std;

// 自分で作った型
struct MyData
{
	int    x, y;
	string data;

	bool operator==(const MyData& rhs) const
	{
		return x == rhs.x && y == rhs.y && data == rhs.data;
	}
};

// 自分で作った型をunorderdコンテナに入れたいときは、operator== の他に
// 型と同じ名前空間で hash_value 関数を定義

size_t hash_value(const MyData& d)
{
	// 複数の値のハッシュ値を組み合わせてハッシュ値を計算するには、
	// boost::hash_combine を使います。
	size_t h = 0;
	boost::hash_combine(h, d.x);
	boost::hash_combine(h, d.y);
	boost::hash_combine(h, d.data);
	return h;
}

int main()
{
	boost::unordered_map<string, int> m;
	m["one"] = 1;
	m["two"] = 2;
	m["three"] = 3;
	cout << m["one"] + m["two"] << endl;

	boost::unordered_set<MyData> s;
	MyData d = { 1,2,"onetwo" };
	s.insert(d);
	s.insert(d);
	cout << s.size() << endl;
}