#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

//#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <filesystem>
std::vector<cv::Mat> read_image(std::string filename);
std::vector<cv::Mat> image_merge(std::vector< std::vector<cv::Mat>> &image_v);
void write_image(std::string filename, std::vector<cv::Mat>&image);
void high_contrast(std::vector<cv::Mat>&image, int max_val);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "uasge:input-path output-path\n");
		exit(1);
	}

	std::string file_in_path = argv[1];
	std::string file_out_path = argv[2];
	std::vector< std::vector<cv::Mat>> image_v;


	for (int i = 0; i < 25; i++) {
		printf("\r image input %03d", i);
		//if (i != 4)continue;
		std::stringstream filename;
		filename << file_in_path << "\\face1_" << std::setw(1) << i%5<<"_"<<std::setw(1) << i/5;
		image_v.push_back(read_image(filename.str()));
	}
	printf("\r image input %03d\n", image_v.size());

	std::vector<cv::Mat>image_m = image_merge( image_v);
	//high_contrast(image_m, 50);

	write_image(file_out_path, image_m);
}
std::vector<cv::Mat> read_image(std::string filename) {
	std::vector<cv::Mat> ret;
	for (int i = 0; i < 100; i++) {
		std::stringstream file_image;
		file_image << filename << "\\" << std::setw(4) << std::setfill('0') << i<<".png";
		cv::Mat image = cv::imread(file_image.str(), cv::IMREAD_GRAYSCALE);
		//printf("\n%d %d\n", image.cols, image.rows);

		ret.push_back(image);

	}
	return ret;
}
std::vector<cv::Mat> image_merge(std::vector< std::vector<cv::Mat>> &image_v) {
	std::vector<cv::Mat> ret;
	int cols = image_v[0][0].cols;
	int rows = image_v[0][0].rows;
	for (int i = 0; i < image_v[0].size(); i++) {
		printf("\r merge %d/%d", i, image_v[0].size());

		cv::Mat image_m = cv::Mat::zeros(rows * 5, cols * 5, CV_8UC1);

		for (int num = 0; num < image_v.size(); num++) {
			int ix = num % 5;
			int iy = num / 5;
			if (iy == 4)iy = 0;
			else if (iy == 3)iy = 1;
			else if (iy == 2)iy = 2;
			else if (iy == 1)iy = 3;
			else if (iy == 0)iy = 4;

			int cols_offset = ix * cols;
			int rows_offset = iy * rows;
			for (int j = 0; j < rows; j++) {
				for (int k = 0; k < cols; k++) {
					image_m.at<unsigned char>(j + rows_offset, k + cols_offset) = image_v[num][i].at<unsigned char>(j, k);
					if (j == 0 || k == 0) {
						image_m.at<unsigned char>(j + rows_offset, k + cols_offset) = 0x00;
					}
				}
			}
		}

		ret.push_back(image_m);
	}
	printf("\r merge %d/%d\n", image_v[0].size(), image_v[0].size());

	return ret;
}
void high_contrast(std::vector<cv::Mat>&image, int min_val) {
	for (int i = 0; i < image.size(); i++) {
		int cols = image[i].cols;
		int rows = image[i].rows;
		for (int j = 0; j < rows; j++) {
			for (int k = 0; k < cols; k++) {
				if (image[i].at<unsigned char>(j, k) - min_val <=0) {
					image[i].at<unsigned char>(j, k) = 0x00;
				}
				else {
					image[i].at<unsigned char>(j, k) = image[i].at<unsigned char>(j, k) * (256. /  (256-min_val));
				}
			}
		}
	}
}
void write_image(std::string filename,std::vector<cv::Mat>&image) {
	for (int i = 0; i < image.size(); i++) {
		printf("\r write image %d/%d", i, image.size());
		std::stringstream file_name;
		file_name << filename << "\\" << std::setw(4) << std::setfill('0') << i << ".png";
		cv::resize(image[i], image[i], cv::Size(), 0.25, 0.25);
		cv::imwrite(file_name.str(), image[i]);
	}
	printf("\r write image %d/%d\n", image.size(), image.size());

}