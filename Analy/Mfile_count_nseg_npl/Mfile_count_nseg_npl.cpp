#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <ios>     // std::left, std::right
#include <iomanip> 

#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

using namespace mfile0;

std::map<std::pair<int, int>, int> count_mfile(std::string file_path);
void output_file(std::string filename, std::map<std::pair<int, int>, int>cnt);

int main(int argc, char** argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-mfile file-out-file\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out = argv[2];

	std::map<std::pair<int, int>, int> count= count_mfile(file_in_mfile);
	output_file(file_out, count);
}
std::map<std::pair<int, int>, int> count_mfile(std::string file_path) {
	std::map<std::pair<int, int>, int> ret;

	std::ifstream ifs(file_path);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}

	std::string str;

	M_Header head_tmp;
	for (int i = 0; i < 3; i++) {
		std::getline(ifs, str);
		head_tmp.head[i] = str;
	}
	std::getline(ifs, str);
	head_tmp.num_all_plate = stoi(str);

	{
		std::getline(ifs, str);
		auto str_v = StringSplit(str);
		for (int i = 0; i < str_v.size(); i++) {
			head_tmp.all_pos.push_back(stoi(str_v[i]));
		}
	}

	int cnt = 0;

	while (std::getline(ifs, str)) {
		if (cnt % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		cnt++;
		int nseg, npl;
		auto strs = StringSplit(str);
		if (strs.size() == 4) {
			nseg= stoi(strs[1]);
			npl = (stoi(strs[3]) - stoi(strs[2])) / 10 + 1;
			auto res=ret.insert(std::make_pair(std::make_pair(nseg, npl), 1));
			if (!res.second) {
				res.first->second++;
			}
			for (int i = 0; i < nseg; i++) {
				std::getline(ifs, str);
			}
		}
	}
	return ret;

}
void output_file(std::string filename, std::map<std::pair<int, int>, int>cnt) {
	std::ofstream ofs(filename);
	for (auto itr = cnt.begin(); itr != cnt.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(10) << itr->first.first << " "
			<< std::setw(10) << itr->first.second << " "
			<< std::setw(5) << itr->second << std::endl;
	}
}
