#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
std::map<std::pair<int, int>, int> mfile_group(std::vector<mfile0::M_Chain> &chains);
std::vector<mfile0::M_Chain> group_pickup(std::map<std::pair<int, int>, int> &base, std::vector<mfile0::M_Chain> &chains);
mfile1::MFile  read_mfile_group_pickup(std::string filepath, std::map<std::pair<int, int>, int> &base_map);

int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "prg in-mfile all-mfile out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_all_mfile = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m;
	mfile0::Mfile m_all;

	mfile0::read_mfile(file_in_mfile, m);
	//mfile1::read_mfile_extension(file_in_all_mfile, m_all);

	std::map<std::pair<int, int>, int> gr = mfile_group(m.chains);
	mfile1::MFile m_all2 = read_mfile_group_pickup(file_in_all_mfile, gr);
	mfile1::converter(m_all2,m_all);

	for (int i = 0; i < m_all.chains.size(); i++) {
		for (int j = 0; j < m_all.chains[i].basetracks.size(); j++) {
			m_all.chains[i].basetracks[j].group_id += i;
		}
	}
	//m.chains = group_pickup(gr, m_all.chains);

	mfile0::write_mfile(file_out_mfile, m_all);
}
std::map<std::pair<int, int>, int> mfile_group(std::vector<mfile0::M_Chain> &chains) {

	std::map<std::pair<int, int>, int> ret;
	for (auto itr = chains.begin(); itr != chains.end(); itr++) {
		for (auto b : itr->basetracks) {
			ret.insert(std::make_pair(std::make_pair(b.pos, b.rawid), b.group_id));
		}

	}
	return ret;
}

std::vector<mfile0::M_Chain> group_pickup(std::map<std::pair<int, int>, int> &base, std::vector<mfile0::M_Chain> &chains) {
	std::vector<mfile0::M_Chain> ret;
	int groupid;
	for (auto itr = chains.begin(); itr != chains.end(); itr++) {
		groupid = -1;
		for (auto b : itr->basetracks) {
			if (base.count(std::make_pair(b.pos, b.rawid)) == 0)continue;
			auto res = base.find(std::make_pair(b.pos, b.rawid));
			groupid = res->second;


		}
		if (groupid > 0) {
			for (auto b : itr->basetracks) {
				b.group_id = groupid;
			}
			ret.push_back(*itr);
		}
	}

	return ret;


}

mfile1::MFile  read_mfile_group_pickup(std::string filepath, std::map<std::pair<int, int>, int> &base_map) {
	std::ifstream ifs(filepath, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }

	std::vector< mfile1::MFileChain1> chains;
	chains.reserve(mfile.info_header.Nchain);

	std::vector< std::vector< mfile1::MFileBase1>> all_basetracks;
	uint64_t count = 0;
	int gid = 0;
	for (uint64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		mfile1::MFileChain1 chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }

		std::vector< mfile1::MFileBase1> basetracks;
		basetracks.reserve(chain.nseg);
		gid = -1;
		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase1 base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			basetracks.emplace_back(base);
			if (base_map.count(std::make_pair(base.pos, base.rawid)) == 1) {
				auto res = base_map.find(std::make_pair(base.pos, base.rawid));
				gid = res->second;
			}
		}
		if (gid>=0) {
			for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
				itr->group_id = gid;
			}
			mfile.chains.push_back(chain);
			mfile.all_basetracks.push_back(basetracks);

		}
	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;
	return mfile;



}
