//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<mfile0::M_Chain> chain_selection(std::vector<mfile0::M_Chain>&c, double thrshold, int nseg);
std::vector<netscan::linklet_t> link_selection(std::vector<netscan::linklet_t>&link, std::vector<mfile0::M_Chain>&c);
double chain_dl(mfile0::M_Chain&chain, int pl);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "input-mfile input-linklet output-linklet\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_link = argv[2];
	std::string file_out_link = argv[3];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_bin(file_in_link, link);

	m.chains = chain_selection(m.chains, 0.01, 10);
	link = link_selection(link, m.chains);


	netscan::write_linklet_bin(file_out_link, link);
}
std::vector<mfile0::M_Chain> chain_selection(std::vector<mfile0::M_Chain>&c,double thrshold,int nseg) {
	std::vector<mfile0::M_Chain>ret;
	std::ofstream ofs("out.txt");
	std::ofstream ofs2("out_sel.txt");
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		double ax = mfile0::chain_ax(*itr);
		double ay = mfile0::chain_ay(*itr);
		double diff_rad, diff_lat;
		if (itr->nseg < nseg)continue;
		diff_lat = chain_dl(*itr, 3);
		if (diff_lat < 0.00000000000001)continue;
		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(3) << std::setprecision(0) << itr->pos0 / 10 << " "
			<< std::setw(3) << std::setprecision(0) << itr->pos1 / 10 << " "
			<< std::setw(7) << std::setprecision(4) << ax << " "
			<< std::setw(7) << std::setprecision(4) << ay << " "
			<< std::setw(7) << std::setprecision(4) << diff_lat << std::endl;

		if (13.6 / diff_lat * sqrt(0.5*sqrt(1 + ax * ax + ay * ay) / 17.57) > 1000) {
			ret.push_back(*itr);
			ofs2 << std::right << std::fixed
				<< std::setw(3) << std::setprecision(0) << itr->nseg << " "
				<< std::setw(3) << std::setprecision(0) << itr->pos0 / 10 << " "
				<< std::setw(3) << std::setprecision(0) << itr->pos1 / 10 << " "
				<< std::setw(7) << std::setprecision(4) << ax << " "
				<< std::setw(7) << std::setprecision(4) << ay << " "
				<< std::setw(7) << std::setprecision(4) << diff_lat << std::endl;
		}
		//if (diff_lat < thrshold)ret.push_back(*itr);
	}


	printf("cut nseg>=%d diff_div_lat >=%.4lf\n", nseg, thrshold);
	printf("\t%d --> %d(%4.1lf%%)\n", c.size(), ret.size(), ret.size()*100. / c.size());
	return ret;


}
double chain_dl(mfile0::M_Chain&chain, int pl) {
	double diff1 = 0, diff2 = 0;
	int count = 0;
	for (int i = 0; i < chain.basetracks.size(); i++) {
		if (i + 1 == chain.basetracks.size())continue;
		if (chain.basetracks[i].pos / 10 == pl)continue;
		if (chain.basetracks[i + 1].pos / 10 == pl)continue;
		if (chain.basetracks[i + 1].pos / 10 - chain.basetracks[i].pos / 10 != 1)continue;

		if (chain.basetracks[i].pos / 10 > 15) {
			if (chain.basetracks[i].pos / 10 % 2 == 1)continue;
		}
		else {
			if (chain.basetracks[i].pos / 10 == 15)continue;
			if (chain.basetracks[i].pos / 10 == 3)continue;
		}

		double ax0, ax1, ay0, ay1, diff;
		ax0 = chain.basetracks[i].ax;
		ax1 = chain.basetracks[i + 1].ax;
		ay0 = chain.basetracks[i].ay;
		ay1 = chain.basetracks[i + 1].ay;
		if (sqrt(ax0*ax0 + ay0 * ay0) > 0.01) {
			double denominator;
			denominator = 1 / sqrt(ax0*ax0 + ay0 * ay0 + 1);
			double t[3] = {
				ax0 *denominator,
			ay0 * denominator,
			1 * denominator
			};
			denominator = 1 / sqrt(ax0*ax0 + ay0 * ay0 + pow(ax0*ax0 + ay0 * ay0, 2));
			double r[3] = {
			-1 * ax0 * denominator,
			-1 * ay0 * denominator,
				(ax0*ax0 + ay0 * ay0)*denominator
			};
			denominator = 1 / sqrt(ax0*ax0 + ay0 * ay0);
			double l[3] = {
				-1 * ay0*denominator,
				ax0*denominator,
				0
			};

			denominator = 1 / (ax1*t[0] + ay1 * t[1] + t[2]);
			diff = (ax1*l[0] + ay1 * l[1] + l[2])*denominator;
		}
		else {
			diff = ay1 - ay0;
		}
		diff1 += diff;
		diff2 += diff * diff;
		count++;
	}

	if (count < 5)return -1;

	double diff_sig = sqrt(diff2 / count - pow(diff1 / count, 2));
	return diff_sig;


}

std::vector<netscan::linklet_t> link_selection(std::vector<netscan::linklet_t>&link, std::vector<mfile0::M_Chain>&c) {
	std::vector<netscan::linklet_t> ret;
	std::map<std::tuple<int, int, int, int>, netscan::linklet_t>link_map;
	std::set<std::pair<int, int>> link_b;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		auto b0 = std::make_pair(itr->b[0].pl, itr->b[0].rawid);
		auto b1 = std::make_pair(itr->b[1].pl, itr->b[1].rawid);
		link_b.insert(b0);
		link_b.insert(b1);
		link_map.insert(std::make_pair(std::make_tuple(b0.first, b0.second, b1.first, b1.second), *itr));
	}
	std::tuple<int, int, int, int> id;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		int count = 0;
		for (auto itr_b = itr->basetracks.begin(); itr_b != itr->basetracks.end(); itr_b++) {
			count += link_b.count(std::make_pair(itr_b->pos / 10, itr_b->rawid)) == 1;
			if (link_b.count(std::make_pair(itr_b->pos / 10, itr_b->rawid)) == 1&&count == 1) {
				std::get<0>(id) = itr_b->pos / 10;
				std::get<1>(id) = itr_b->rawid;
			}
			else if (link_b.count(std::make_pair(itr_b->pos / 10, itr_b->rawid)) == 1&&count == 2) {
				std::get<2>(id) = itr_b->pos / 10;
				std::get<3>(id) = itr_b->rawid;
			}
		}
		if (count == 2) {
			auto res = link_map.find(id);
			if (res == link_map.end())continue;
			ret.push_back(res->second);
		}
	}

	printf("linklet selection %d --> %d (%4.1lf%%)\n", link.size(), ret.size(), ret.size()*100. / link.size());
	return ret;

}
