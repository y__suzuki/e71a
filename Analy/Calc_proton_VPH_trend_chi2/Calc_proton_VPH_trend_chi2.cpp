﻿#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include "M:\data\NINJA\PID\PDG.h"
#include "M:\data\NINJA\PID\AbsorberMaterial_Emulsion_NINJA_Run6.h"
class DataPoint {
public:
	double input_ang_min, input_ang_max, input_mom_min, input_mom_max;
	double mean[3], mean_low[3], mean_hi[3], sigma[3], sigma_low[3], sigma_hi[3];
};
class Chain_inf {
public:
	int groupid, chainid, pid;
	double pb, vph, angle, proton_likelihood, pion_likelihood, likelihood_ratio;
};

class chain_inf {
public:
	int groupid, chainid,num;
	std::vector<double> angle, vph, pb,sigma;
	double chi2;
	double angle_ave() {
		double sum = 0;
		for (auto &ang : angle) {
			sum +=ang;
		}
		return sum / angle.size();
	}
};

std::vector<chain_inf>Read_chain_information(std::string filename);
void Output_chain_information(std::vector<chain_inf>&c_inf, std::string filename);
void Output_chain_summary(std::vector<chain_inf>&c_inf, std::string filename);
void iang_to_angle_range(int i_ang, double &angle_min, double &angle_max);
void Read_Theoretical_formula(std::string filename, double &angle_min, double &angle_max, double *vph_mean_param, double *vph_sigma_param);
void Read_Data_point(std::string filename, double &angle_min, double &angle_max, DataPoint*data, double &thr_sigma, int &DataNum);

double search_vph_mean_cross_point(double *par, DataPoint *data, int DataNum, double &data_pb_max);
double search_vph_sigma_cross_point(double*par, double *par_sigma, double vph_start, double thr_sigma);

double Calc_PHV(double var, double *par, double mean_thr, DataPoint *data, int DataNum, double &data_pb_max);
double Calc_PHV_sigma(double var, double *par, double *par_sigma, double mean_thr, double sigma_thr, DataPoint *data, int DataNum);

int main(int argc,char**argv){

	if (argc != 4) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}

	std::string file_in_chain = argv[1];
	std::string file_out_chain = argv[2];
	std::string file_out_chain_summary = argv[3];
	std::string filename = "M:\\data\\NINJA\\PID\\average.bin.root_fitresult.txt";
	std::string fileame_fit = "M:\\data\\NINJA\\PID\\average.bin.root_fitresult.txt_fit_mom.txt";

	std::vector<chain_inf> c_inf=Read_chain_information(file_in_chain);

	double pth_muon = 0.10*m_mu*(0.10*m_mu / sqrt(m_mu*m_mu + 0.10*0.10*m_mu*m_mu));
	double pth_proton = 0.10*m_p*(0.10*m_p / sqrt(m_p*m_p + 0.10*0.10*m_p*m_p));

	double angle_min, angle_max;

	for (int i_ang = 0; i_ang < 20; i_ang++) {
		iang_to_angle_range(i_ang, angle_min, angle_max);
		printf("%d angle %.1lf - %.1lf\n", i_ang, angle_min, angle_max);
		//理論式のパラメータを読み込む
		double vph_mean_param[4] = {}, vph_sigma_param[2] = {};
		Read_Theoretical_formula(fileame_fit, angle_min, angle_max, vph_mean_param, vph_sigma_param);
		//printf("%.1lf %.1lf %.1lf %.1lf\n", vph_mean_param[0], vph_mean_param[1], vph_mean_param[2], vph_mean_param[3]);
		double thr_sigma = -1;
		int DataNum = 0;
		DataPoint data[30] = {};
		//ここがバグ
		Read_Data_point(filename, angle_min, angle_max, data, thr_sigma, DataNum);
		printf("Data Num %d\n", DataNum);
		//交点を求める
		double data_pb_max = -1;
		double mean_p_thr = search_vph_mean_cross_point(vph_mean_param, data, DataNum, data_pb_max);
		//printf("thr vph=%lf\n", mean_p_thr);
		double sigma_p_thr = search_vph_sigma_cross_point(vph_mean_param, vph_sigma_param, mean_p_thr, thr_sigma);
		printf("thr vph=%lf , %lf\n", mean_p_thr, sigma_p_thr);


		double angle;
		double vph_mean, vph_sigma, sigma;
		for (auto &c : c_inf) {
			angle = c.angle_ave();
			if (angle < angle_min)continue;
			if (angle_max <= angle)continue;
			c.chi2 = 0;
			for (int i = 0; i < c.angle.size(); i++) {
				vph_mean = Calc_PHV(c.pb[i], vph_mean_param, mean_p_thr, data, DataNum, data_pb_max);
				vph_sigma = Calc_PHV_sigma(c.pb[i], vph_mean_param, vph_sigma_param, mean_p_thr, sigma_p_thr, data, DataNum);
				sigma = (vph_mean - c.vph[i]) / vph_sigma;
				c.sigma.push_back(sigma);
				c.chi2 += sigma * sigma;
			}
		}
	}

	printf("calc fin\n");
	Output_chain_information(c_inf, file_out_chain);
	Output_chain_summary(c_inf, file_out_chain_summary);

	

}
std::vector<chain_inf>Read_chain_information(std::string filename) {
	std::ifstream ifs(filename);
	std::vector < chain_inf> ret;
	chain_inf c;
	c.angle.clear();
	c.vph.clear();
	c.pb.clear();

	std::pair<int, int> id_pair;
	int groupid, chainid, num, pl;
	double angle, pb, vph;
	std::set<std::pair<int, int>> input_list;
	int count = 0;
	while (ifs >> groupid>>chainid>>num>>pl>>angle>>pb>>vph) {
		if (count % 10000 == 0) {
			printf("\r input chain data ...%d",count);
		}
		count++;

		id_pair.first = groupid;
		id_pair.second = chainid;
		if (input_list.count(id_pair) == 0) {
			input_list.insert(id_pair);
			c.num = c.angle.size();
			if (c.num > 0) {
				ret.push_back(c);
			}
			c.groupid = groupid;
			c.chainid = chainid;
			c.angle.clear();
			c.vph.clear();
			c.pb.clear();
			c.sigma.clear();
			c.chi2 = -1;
		}
		c.angle.push_back(angle);
		c.vph.push_back(vph);
		c.pb.push_back(pb);
	}
	c.num = c.angle.size();
	if (c.num > 0) {
		ret.push_back(c);
	}

	printf("\r input chain data ...%d\n",count);

	return ret;
}
void Output_chain_information(std::vector<chain_inf>&c_inf, std::string filename) {
	std::ofstream ofs(filename);
	for (auto &c : c_inf) {
		if (c.angle.size() != c.sigma.size())continue;
		for (int i = 0; i < c.angle.size(); i++) {
			ofs << std::setw(10) << c.groupid << " " << c.chainid << " " << c.chi2 << " " << i << " "
				<< c.angle[i] << " " << c.pb[i] << " " << c.vph[i] << " " << c.sigma[i] << std::endl;
		}
	}
}
void Output_chain_summary(std::vector<chain_inf>&c_inf, std::string filename) {
	std::ofstream ofs(filename);
	for (auto &c : c_inf) {
		if (c.angle.size() != c.sigma.size())continue;
		ofs << std::setw(10) << c.groupid << " " << c.chainid << " "
			<< c.chi2 << " " << c.num << " " << c.chi2 / c.num << std::endl;
	}
}

void iang_to_angle_range(int i_ang, double &angle_min, double &angle_max) {
	if (i_ang < 7) {
		angle_min = i_ang * 0.1;
		angle_max = (i_ang + 1)*0.1;
	}
	else if (i_ang < 11) {
		angle_min = (i_ang - 7) * 0.2 + 0.7;
		angle_max = (i_ang - 7 + 1)*0.2 + 0.7;
	}
	else if (i_ang < 15) {
		angle_min = (i_ang - 11) * 0.4 + 1.5;
		angle_max = (i_ang - 11 + 1)*0.4 + 1.5;
	}
	else {
		angle_min = (i_ang - 15) * 0.6 + 3.1;
		angle_max = (i_ang - 15 + 1)*0.6 + 3.1;
	}
}

//理論式のパラメータを読み込む
void Read_Theoretical_formula(std::string filename, double &angle_min, double &angle_max, double *vph_mean_param, double *vph_sigma_param) {

	std::ifstream ifs(filename.c_str());
	double input_ang_min, input_ang_max, buf[10];
	//double vph_mean_param[4] = {}, vph_sigma_param[2] = {};
	//mass
	vph_mean_param[0] = m_p;
	//charge(abs)
	vph_mean_param[1] = 1;
	vph_mean_param[2] = 0;
	vph_mean_param[3] = 0;
	double all_angle_max = 0;
	while (ifs >> input_ang_min >> input_ang_max
		>> buf[0] >> buf[1] >> buf[2] >> buf[3] >> buf[4] >> buf[5] >> buf[6] >> buf[7]) {
		if (all_angle_max < (input_ang_min + input_ang_max) / 2)all_angle_max = (input_ang_min + input_ang_max) / 2;
		if ((input_ang_min + input_ang_max) / 2 < angle_min)continue;
		if ((input_ang_min + input_ang_max) / 2 > angle_max)continue;

		//if (all_angle_max < (input_ang_min + input_ang_max) / 2)all_angle_max = (input_ang_min + input_ang_max) / 2;
		//if ((input_ang_min + input_ang_max) / 2 < angle_min)continue;
		//if ((input_ang_min + input_ang_max) / 2 > angle_max)continue;
		vph_mean_param[2] = buf[0];
		vph_mean_param[3] = buf[2];
		vph_sigma_param[0] = buf[4];
		vph_sigma_param[1] = buf[6];
		break;
	}
	ifs.close();
	if (vph_mean_param[2] < 0.01) {
		ifs.open(filename.c_str());
		while (ifs >> input_ang_min >> input_ang_max
			>> buf[0] >> buf[1] >> buf[2] >> buf[3] >> buf[4] >> buf[5] >> buf[6] >> buf[7]) {
			if (all_angle_max > input_ang_max)continue;
			vph_mean_param[2] = buf[0];
			vph_mean_param[3] = buf[2];
			vph_sigma_param[0] = buf[4];
			vph_sigma_param[1] = buf[6];
			break;
		}
		ifs.close();
	}

}
//各点のデータを読み込む
void Read_Data_point(std::string filename, double &angle_min, double &angle_max, DataPoint*data, double &thr_sigma, int &DataNum) {
	std::ifstream ifs(filename.c_str());
	double input_mom_min, input_mom_max;
	double input_ang_min, input_ang_max;
	double mean[3], mean_low[3], mean_hi[3], sigma[3], sigma_low[3], sigma_hi[3];
	//Double_t thr_sigma = -1;
	thr_sigma = -1;
	//int DataNum = 0;
	DataNum = 0;
	double all_angle_max = 0;
	double input_ang_max_max = 0;
	while (ifs >> input_ang_min >> input_ang_max >> input_mom_min >> input_mom_max
		>> mean[0] >> mean_low[0] >> mean_hi[0] >> sigma[0] >> sigma_low[0] >> sigma_hi[0]
		>> mean[1] >> mean_low[1] >> mean_hi[1] >> sigma[1] >> sigma_low[1] >> sigma_hi[1]
		>> mean[2] >> mean_low[2] >> mean_hi[2] >> sigma[2] >> sigma_low[2] >> sigma_hi[2]) {
		//if (input_ang_max_max < input_ang_max)input_ang_max_max = input_ang_max;
		//if (input_ang_min > c.angle)continue;
		//if (input_ang_max <= c.angle)continue;

		if (all_angle_max < (input_ang_min + input_ang_max) / 2)all_angle_max = (input_ang_min + input_ang_max) / 2;
		if ((input_ang_min + input_ang_max) / 2 < angle_min)continue;
		if ((input_ang_min + input_ang_max) / 2 > angle_max)continue;
		data[DataNum].input_ang_min = input_ang_min;
		data[DataNum].input_ang_max = input_ang_max;
		data[DataNum].input_mom_min = input_mom_min;
		data[DataNum].input_mom_max = input_mom_max;
		data[DataNum].mean[0] = mean[0];
		data[DataNum].mean[1] = mean[1];
		data[DataNum].mean[2] = mean[2];
		data[DataNum].mean_low[0] = mean_low[0];
		data[DataNum].mean_low[1] = mean_low[1];
		data[DataNum].mean_low[2] = mean_low[2];
		data[DataNum].mean_hi[0] = mean_hi[0];
		data[DataNum].mean_hi[1] = mean_hi[1];
		data[DataNum].mean_hi[2] = mean_hi[2];
		data[DataNum].sigma[0] = sigma[0];
		data[DataNum].sigma[1] = sigma[1];
		data[DataNum].sigma[2] = sigma[2];
		data[DataNum].sigma_low[0] = sigma_low[0];
		data[DataNum].sigma_low[1] = sigma_low[1];
		data[DataNum].sigma_low[2] = sigma_low[2];
		data[DataNum].sigma_hi[0] = sigma_hi[0];
		data[DataNum].sigma_hi[1] = sigma_hi[1];
		data[DataNum].sigma_hi[2] = sigma_hi[2];
		DataNum++;
		if (1200 < (input_mom_min + input_mom_max) / 2 && (input_mom_min + input_mom_max) / 2 < 1300) {
			thr_sigma = sigma[1];
		}
	}
	ifs.close();
	if (DataNum == 0) {
		ifs.open(filename.c_str());
		while (ifs >> input_ang_min >> input_ang_max >> input_mom_min >> input_mom_max
			>> mean[0] >> mean_low[0] >> mean_hi[0] >> sigma[0] >> sigma_low[0] >> sigma_hi[0]
			>> mean[1] >> mean_low[1] >> mean_hi[1] >> sigma[1] >> sigma_low[1] >> sigma_hi[1]
			>> mean[2] >> mean_low[2] >> mean_hi[2] >> sigma[2] >> sigma_low[2] >> sigma_hi[2]) {
			if (all_angle_max > input_ang_max)continue;
			//if (c.angle < input_ang_max)continue;
			//if (input_ang_max_max - 0.1 > input_ang_max)continue;

			data[DataNum].input_ang_min = input_ang_min;
			data[DataNum].input_ang_max = input_ang_max;
			data[DataNum].input_mom_min = input_mom_min;
			data[DataNum].input_mom_max = input_mom_max;
			data[DataNum].mean[0] = mean[0];
			data[DataNum].mean[1] = mean[1];
			data[DataNum].mean[2] = mean[2];
			data[DataNum].mean_low[0] = mean_low[0];
			data[DataNum].mean_low[1] = mean_low[1];
			data[DataNum].mean_low[2] = mean_low[2];
			data[DataNum].mean_hi[0] = mean_hi[0];
			data[DataNum].mean_hi[1] = mean_hi[1];
			data[DataNum].mean_hi[2] = mean_hi[2];
			data[DataNum].sigma[0] = sigma[0];
			data[DataNum].sigma[1] = sigma[1];
			data[DataNum].sigma[2] = sigma[2];
			data[DataNum].sigma_low[0] = sigma_low[0];
			data[DataNum].sigma_low[1] = sigma_low[1];
			data[DataNum].sigma_low[2] = sigma_low[2];
			data[DataNum].sigma_hi[0] = sigma_hi[0];
			data[DataNum].sigma_hi[1] = sigma_hi[1];
			data[DataNum].sigma_hi[2] = sigma_hi[2];
			DataNum++;
			if (1200 < (input_mom_min + input_mom_max) / 2 && (input_mom_min + input_mom_max) / 2 < 1300) {
				thr_sigma = sigma[1];
			}
		}
		ifs.close();

	}

	if (thr_sigma < 0) {
		printf("sigma threshold not found\n");
		exit(1);
	}
}

double Calc_momentum_PHV_emulsion_Fit(double var, double *par) {

	double pbeta = var;

	double dEdx = 0.0;


	for (int i = 0; i < 12; i++) {

		switch (i) {
		case  0: Absorber = Ag; break;
		case  1: Absorber = Br; break;
		case  2: Absorber = I;  break;
		case  3: Absorber = C;  break;
		case  4: Absorber = N;  break;
		case  5: Absorber = O;  break;
		case  6: Absorber = H;  break;
		case  7: Absorber = S;  break;
		case  8: Absorber = Na; break;
		case  9: Absorber = Fe; break;
		case 10: Absorber = Au; break;
		case 11: Absorber = Cl; break;
		default: break;
		}


		double mass = par[0];
		double z = par[1];

		// information of constants //
		double me = m_e;                   // mass of electron [GeV]
		double mec2 = 0.510998928 / 1000.0;  // electron mass * c^2 [GeV]
		double K = 0.307075;               // 4πNA re^2 me c^2 [MeV mol^-1 cm^2]

		// information of absorber (Fe) //
		double Z = mtrl_Z[Absorber];              // atomic number of absorber           (PDG2017)
		double A = mtrl_A[Absorber];              // atomic mass of absorber [g mol^-1]  (PDG2017)
		double I = mtrl_I[Absorber] / 1000000000.0; // Mean excitation energy of Fe [GeV]  (PDG2017)
		double density = mtrl_Density[Absorber];  // density of Fe [g cm^-3]             (PDG2017)


		/* Calculation of dE/dx */

		// Energy, Beta, Lorentz factor //
		double E = (pbeta + sqrt(pbeta*pbeta + 4.0*mass*mass)) / 2.0;   // Energy [GeV]
		double p = sqrt(pbeta*E);
		double beta = p / E;                         // Beta
		double gamma = 1.0 / sqrt(1.0 - beta * beta);  // Lorentz factor

		// Maximum Energy Transfer to an electron in a single collision //
		double Wmax = (2.0*mec2*beta*beta*gamma*gamma) / (1.0 + 2.0*gamma*me / mass + (me / mass)*(me / mass)); // maximum energy transfer to an electron in a single collision

		// density effect //
		double  C_bar = mtrl_C_bar[Absorber];
		double      a = mtrl_a[Absorber];
		double      m = mtrl_m[Absorber];
		double     x1 = mtrl_x1[Absorber];
		double     x0 = mtrl_x0[Absorber];
		double delta0 = mtrl_delta0[Absorber];

		double x = log10(p / mass);
		double density_effect = 0.0;

		if (x >= x1)
			density_effect = (2.0*log(10)*x - C_bar) / 2.0;
		else if (x0 <= x && x < x1)
			density_effect = (2.0*log(10)*x - C_bar + a * pow(x1 - x, m)) / 2.0;
		else if (x < x0)
			density_effect = (delta0*pow(10.0, 2.0*(x - x0))) / 2.0; // conductors
			//density_effect = 0; // nonconductors // 180424 modified


		// weight ratio //
		double wratio = mtrl_wratio[Absorber];

		dEdx += wratio * (K*z*z*(Z / A)*(1.0 / (beta*beta))*((1.0 / 2.0)*log(2.0*mec2*beta*beta*gamma*gamma*Wmax / (I*I)) - beta * beta - density_effect));

	}


	double slope = par[2];     // 傾き
	double intercept = par[3]; // 切片

	return slope * dEdx + intercept;

}



double search_vph_mean_cross_point(double *par, DataPoint *data, int DataNum, double &data_pb_max) {
	double mean_p_thr;
	double pb_min = 1;
	double pb_max = 300;
	double pich = 1;
	double vph = 0;
	int sign = 0;
	for (double pb = pb_min; pb <= pb_max; pb += pich) {
		vph = Calc_momentum_PHV_emulsion_Fit(pb / 1000, par);

		double vph_data = 0;
		double data_pb_max;
		for (int i = 0; i < DataNum; i++) {
			if (data[i].input_mom_max > pb) {
				vph_data = data[i].mean[2];
				data_pb_max = data[i].input_mom_max;
				break;
			}
		}
		//printf("pb:%lf %lf %lf\n",pb, vph_data, vph);
		if (vph_data < 1)continue;
		if (vph_data >= vph) {
			mean_p_thr = Calc_momentum_PHV_emulsion_Fit((int(pb / 100) + 1) * 100. / 1000, par);
			break;
		}
	}
	if (mean_p_thr < 0) {
		mean_p_thr = Calc_momentum_PHV_emulsion_Fit((int(pb_max / 100) + 1) * 100. / 1000, par);
	}
	return mean_p_thr;
}


double search_vph_sigma_cross_point(double*par, double *par_sigma, double vph_start, double thr_sigma) {
	double sigma_p_thr = -1;
	double pich = 1;
	double vph = 0;
	double sigma = 0;
	for (double pb = vph_start; pb < 2000; pb += pich) {
		vph = Calc_momentum_PHV_emulsion_Fit(pb / 1000, par);
		sigma = par_sigma[0] + par_sigma[1] * sqrt(vph);
		if (sigma - thr_sigma < 1) {
			sigma_p_thr = vph;
			break;
		}
	}
	if (sigma_p_thr < 0) {
		printf("cross point not found\n");
		exit(1);
	}
	return sigma_p_thr;


}



double Calc_PHV(double var, double *par, double mean_thr, DataPoint *data, int DataNum, double &data_pb_max) {

	double vph = Calc_momentum_PHV_emulsion_Fit(var / 1000, par);
	if (vph < mean_thr)return vph;

	double vph_min = -1, vph_max = -1;
	double pb_min = -1, pb_max = -1;

	if ((data[0].input_mom_min + data[0].input_mom_max) / 2 > var) {
		vph_min = data[0].mean[2];
		pb_min = (data[0].input_mom_min + data[0].input_mom_max) / 2;
		vph_max = data[1].mean[2];
		pb_max = (data[1].input_mom_min + data[1].input_mom_max) / 2;
	}
	else {
		for (int i = 0; i < DataNum; i++) {
			if ((data[i].input_mom_min + data[i].input_mom_max) / 2 > var) {
				vph_min = data[i - 1].mean[2];
				pb_min = (data[i - 1].input_mom_min + data[i - 1].input_mom_max) / 2;
				vph_max = data[i].mean[2];
				pb_max = (data[i].input_mom_min + data[i].input_mom_max) / 2;
				break;
			}
		}
	}
	if (pb_max >= data_pb_max) {
		pb_max = data_pb_max;
		vph_max = mean_thr;
	}
	return vph_min + (vph_max - vph_min) / (pb_max - pb_min)*(var - pb_min);

	printf("Data not found\n");
	printf("pb %lf\n", var);
	printf("VPH %lf\n", vph);
	exit(1);
	return -1;
}
double Calc_PHV_sigma(double var, double *par, double *par_sigma, double mean_thr, double sigma_thr, DataPoint *data, int DataNum) {

	double vph = Calc_momentum_PHV_emulsion_Fit(var / 1000, par);
	//VPH��mean thr���傫��-->��pb�̃f�[�^
	if (vph > mean_thr) {
		for (int i = 0; i < DataNum; i++) {
			if (data[i].input_mom_max > var) {
				return data[i].sigma[2];
			}
		}
	}
	else if (vph < sigma_thr) {
		vph = sigma_thr;
	}
	double sigma = par_sigma[0] + par_sigma[1] * sqrt(vph);
	return sigma;
}
