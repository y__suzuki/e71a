#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class event_inf {
public:
	int eventid, chainid,vertexpl;
	double md[3], ip[3],oa[3],vertex_position[3];
};
Momentum_recon::Mom_chain select_chain(std::vector<Momentum_recon::Event_information> &momch, int eventid, int chainid);
void divide_chain(Momentum_recon::Mom_chain&ori, int vertex_pl, Momentum_recon::Mom_chain&down, Momentum_recon::Mom_chain&up);
double calc_md(Momentum_recon::Mom_basetrack&b0, Momentum_recon::Mom_basetrack &b1, double z0, double z1);
void calc_ip(Momentum_recon::Mom_basetrack&b0, Momentum_recon::Mom_basetrack &b1, Momentum_recon::Mom_basetrack &b2, double z0, double z1, double*ip, double*position);
double calc_oa(Momentum_recon::Mom_basetrack&b0, Momentum_recon::Mom_basetrack &b1);
void output(std::string filename, event_inf&out, Momentum_recon::Mom_chain&c);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:file-in-momch eventid chainid outputfile\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	int eventid = std::stoi(argv[2]);
	int chainid = std::stoi(argv[3]);
	std::string file_out= argv[4];
	event_inf out;
	out.chainid = chainid;
	out.eventid = eventid;

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	
	Momentum_recon::Mom_chain muon = select_chain(momch, eventid,0);
	Momentum_recon::Mom_chain proton = select_chain(momch, eventid, chainid);

	out.vertexpl = muon.base.rbegin()->pl;
	
	Momentum_recon::Mom_chain down, up;
	divide_chain(proton, out.vertexpl, down, up);

	Momentum_recon::Mom_basetrack b_mu, b_down, b_up;
	b_mu = *muon.base.rbegin();
	b_down = *down.base.rbegin();
	b_up = *up.base.begin();
	double z0, z1;
	if (b_mu.z > b_down.z) {
		z1 = b_mu.z;
	}
	else {
		z1 = b_down.z;
	}
	z0 = b_up.z;

	out.md[0]= calc_md(b_mu, b_down, z0, z1);
	out.md[1] = calc_md(b_mu, b_up, z0, z1);
	out.md[2] = calc_md(b_down, b_up, z0, z1);
	out.oa[0] = calc_oa(b_mu, b_down);
	out.oa[1] = calc_oa(b_mu, b_up);
	out.oa[2] = calc_oa(b_down, b_up);

	calc_ip(b_mu, b_down,b_up, z0, z1,out.ip,out.vertex_position);
	out.vertex_position[2] = out.vertex_position[2] - (z1 - z0);

	//print_vph(proton);
	output(file_out, out, proton);

	printf("sqrt(ip2): %.1lf um\n", sqrt(pow(out.ip[0], 2) + pow(out.ip[1], 2) + pow(out.ip[2], 2)));
	printf("oa       : %.4lf\n", out.oa[2]);
}
Momentum_recon::Mom_chain select_chain(std::vector<Momentum_recon::Event_information> &momch,int eventid,int chainid) {
	Momentum_recon::Mom_chain ret;
	bool flg = false;
	for (auto &ev : momch) {
		if (flg)break;
		if (ev.groupid != eventid)continue;
		for (auto &c : ev.chains) {
			if (flg)break;
			if (c.chainid != chainid)continue;
			flg = true;
			ret = c;
		}
	}
	if (!flg) {
		fprintf(stderr, "group=%5d chain=%5d not found\n", eventid, chainid);
		exit(1);
	}
	return ret;
}
void divide_chain(Momentum_recon::Mom_chain&ori,int vertex_pl, Momentum_recon::Mom_chain&down, Momentum_recon::Mom_chain&up) {

	down = ori;
	up = ori;
	down.base.clear();
	down.base_pair.clear();
	up.base.clear();
	up.base_pair.clear();

	for (auto &b : ori.base) {
		if (b.pl <= vertex_pl) {
			down.base.push_back(b);
		}
		else {
			up.base.push_back(b);
		}
	}

	for (auto &p : ori.base_pair) {
		if (p.first.pl <= vertex_pl && p.second.pl <= vertex_pl) {
			down.base_pair.push_back(p);
		}
		else if (p.first.pl > vertex_pl && p.second.pl > vertex_pl) {
			up.base_pair.push_back(p);
		}
	}


}
//md �v�Z
double calc_md(Momentum_recon::Mom_basetrack&b0, Momentum_recon::Mom_basetrack &b1, double z0,double z1) {
	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	pos0.x = b0.x;
	pos0.y = b0.y;
	pos0.z = b0.z-z0;
	pos1.x = b1.x;
	pos1.y = b1.y;
	pos1.z = b1.z-z0;
	dir0.x = b0.ax;
	dir0.y = b0.ay;
	dir0.z = 1;
	dir1.x = b1.ax;
	dir1.y = b1.ay;
	dir1.z = 1;

	double z_range[2], extra[2];
	z_range[0] = z0-z0;
	z_range[1] = z1-z0;
	double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);

	//printf("Calc md %.0lf - %.0lf:z=%.0lf/%.0lf md=%.1lf\n", z_range[0],z_range[1], extra[0], extra[1], md);
	return md;
}

//ip�v�Z
matrix_3D::vector_3D clac_md_midposition(Momentum_recon::Mom_basetrack&b0, Momentum_recon::Mom_basetrack &b1, double z0, double z1) {
	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	pos0.x = b0.x;
	pos0.y = b0.y;
	pos0.z = b0.z - z0;
	pos1.x = b1.x;
	pos1.y = b1.y;
	pos1.z = b1.z - z0;
	dir0.x = b0.ax;
	dir0.y = b0.ay;
	dir0.z = 1;
	dir1.x = b1.ax;
	dir1.y = b1.ay;
	dir1.z = 1;

	double z_range[2], extra[2];
	z_range[0] = z0 - z0;
	z_range[1] = z1 - z0;
	double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);

	matrix_3D::vector_3D extra0 = addition(pos0, const_multiple(dir0, extra[0]));
	matrix_3D::vector_3D extra1 = addition(pos1, const_multiple(dir1, extra[1]));

	//printf("Calc md \n");
	//printf("(%.1lf, %.1lf, %.1lf)\n", extra0.x, extra0.y, extra0.z);
	//printf("(%.1lf, %.1lf, %.1lf)\n", extra1.x, extra1.y, extra1.z);

	matrix_3D::vector_3D ret = addition(extra0, extra1);
	ret= const_multiple(ret, 1 / 2.);
	return ret;
}
double calc_impactparameter(Momentum_recon::Mom_basetrack&b0, matrix_3D::vector_3D&point,double z0) {
	matrix_3D::vector_3D pos0, dir0;

	pos0.x = b0.x;
	pos0.y = b0.y;
	pos0.z = b0.z-z0;
	dir0.x = b0.ax;
	dir0.y = b0.ay;
	dir0.z = 1;

	double ip = matrix_3D::inpact_parameter(pos0, dir0, point);
	//printf("impact parameter %.1lf\n", ip);
	return ip;
}
void calc_ip(Momentum_recon::Mom_basetrack&b0, Momentum_recon::Mom_basetrack &b1, Momentum_recon::Mom_basetrack &b2, double z0, double z1,double*ip,double*position) {
	//md�̒��_���v�Z
	matrix_3D::vector_3D md0 = clac_md_midposition(b0, b1, z0, z1);
	matrix_3D::vector_3D md1 = clac_md_midposition(b0, b2, z0, z1);
	matrix_3D::vector_3D md2 = clac_md_midposition(b1, b2, z0, z1);

	matrix_3D::vector_3D midpoint;
	midpoint= addition(md0, addition(md1, md2));
	midpoint = const_multiple(midpoint,1 / 3.);

	//printf("(%.1lf, %.1lf, %.1lf)\n", midpoint.x, midpoint.y, midpoint.z);

	ip[0] = calc_impactparameter(b0, midpoint,z0);
	ip[1] = calc_impactparameter(b1, midpoint,z0);
	ip[2] = calc_impactparameter(b2, midpoint,z0);
	position[0] = midpoint.x;
	position[1] = midpoint.y;
	position[2] = midpoint.z;
}

//oa�v�Z
double calc_oa(Momentum_recon::Mom_basetrack&b0, Momentum_recon::Mom_basetrack &b1) {
	matrix_3D::vector_3D  dir0, dir1;
	dir0.x = b0.ax;
	dir0.y = b0.ay;
	dir0.z = 1;
	dir1.x = b1.ax;
	dir1.y = b1.ay;
	dir1.z = 1;

	double oa = matrix_3D::opening_angle(dir0, dir1);
	//printf("oa %.4lf\n",oa);
	return oa;

}

void output(std::string filename, event_inf&out, Momentum_recon::Mom_chain&c) {
	std::ofstream ofs;
	//ofs.open(filename, std::ios::app);
	ofs.open(filename);

	ofs << std::right << std::fixed
		<< std::setw(5) << std::setprecision(0) << out.eventid << " "
		<< std::setw(5) << std::setprecision(0) << out.chainid << " "
		<< std::setw(3) << std::setprecision(0) << out.vertexpl << " "
		<< std::setw(8) << std::setprecision(1) << out.vertex_position[0] << " "
		<< std::setw(8) << std::setprecision(1) << out.vertex_position[1] << " "
		<< std::setw(8) << std::setprecision(1) << out.vertex_position[2] <<std::endl
		<< std::setw(5) << std::setprecision(1) << out.md[0] << " "
		<< std::setw(5) << std::setprecision(1) << out.md[1] << " "
		<< std::setw(5) << std::setprecision(1) << out.md[2] << " "
		<< std::setw(5) << std::setprecision(4) << out.oa[0] << " "
		<< std::setw(5) << std::setprecision(4) << out.oa[1] << " "
		<< std::setw(5) << std::setprecision(4) << out.oa[2] << " "
		<< std::setw(5) << std::setprecision(1) << out.ip[0] << " "
		<< std::setw(5) << std::setprecision(1) << out.ip[1] << " "
		<< std::setw(5) << std::setprecision(1) << out.ip[2] << std::endl;


	//double angle = 0;
	//for (auto &b : c.base) {
	//	angle += sqrt(b.ax*b.ax + b.ay*b.ay);
	//}
	//angle /= c.base.size();

	//ofs << std::right << std::fixed
	//	<< std::setw(8) << std::setprecision(1) << c.Get_muon_mcs_pb() << " "
	//	<< std::setw(8) << std::setprecision(1) << c.Get_proton_mcs_pb() << " "
	//	<< std::setw(5) << std::setprecision(4) << angle << " "
	//	<< std::setw(5) << std::setprecision(0) << c.base.size() << std::endl;


	//for (auto &b : c.base) {
	//	ofs << std::right << std::fixed
	//		<< std::setw(4) << std::setprecision(0) << b.pl<< " "
	//		<< std::setw(4) << std::setprecision(0) << b.m[0].ph % 10000 << " "
	//		<< std::setw(4) << std::setprecision(0) << b.m[1].ph % 10000 << std::endl;
	//}
	ofs.close();

}