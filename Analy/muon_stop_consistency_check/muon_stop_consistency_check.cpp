#define _CRT_SECURE_NO_WARNINGS
//#pragma comment(lib, "VxxReader.lib")
//#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <filesystem>
#include <omp.h>

int count_stop_in_PM(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&c);
int count_stop_in_PM_penecheck(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&c);
std::vector<std::pair<mfile0::M_Chain, Sharing_file::Sharing_file>> sharing_file_matching(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&c);
std::vector<mfile0::M_Chain> chain_merge(std::vector<mfile0::M_Chain>&c1, std::vector<mfile0::M_Chain>&c2);
void Print_PM_tracktype(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&c);
void Print_ECC_tracktype(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&stop, std::vector<mfile0::M_Chain>&penetrate, std::vector<mfile0::M_Chain>&edgeout);

std::vector < Sharing_file::Sharing_file> pickup_sharingfile(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&c, int track_type);
std::vector < Sharing_file::Sharing_file> sharing_file_merge(std::vector < Sharing_file::Sharing_file>&sf0, std::vector < Sharing_file::Sharing_file>&sf1);
std::vector < Sharing_file::Sharing_file> sharing_file_delete_sametime(std::vector < Sharing_file::Sharing_file>&sf);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:file-in-sf file-in-stop file-in-penetrate file-in-edgeout");
		exit(1);
	}

	std::string file_in_sharingfile = argv[1];
	std::string file_in_stop = argv[2];
	std::string file_in_penetrate = argv[3];
	std::string file_in_edgeout = argv[4];

	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_bin(file_in_sharingfile);
	mfile0::Mfile stop, edgeout, penetrate;
	mfile1::read_mfile_extension(file_in_stop, stop);
	mfile1::read_mfile_extension(file_in_penetrate, penetrate);
	mfile1::read_mfile_extension(file_in_edgeout, edgeout);
	std::vector<mfile0::M_Chain> all_tmp = chain_merge(penetrate.chains, edgeout.chains);
	std::vector<mfile0::M_Chain> all = chain_merge(stop.chains, all_tmp);

	Print_PM_tracktype(sf, all);
	std::vector < Sharing_file::Sharing_file> pm_stop_type0 = pickup_sharingfile(sf, all, 0);
	pm_stop_type0 =sharing_file_delete_sametime(pm_stop_type0);

	Print_ECC_tracktype(pm_stop_type0, stop.chains, penetrate.chains, edgeout.chains);


	printf("ECC stop num      %d\n", stop.chains.size());
	Print_PM_tracktype(sf, stop.chains);
	std::vector < Sharing_file::Sharing_file> ecc_stop_type0 = pickup_sharingfile(sf, stop.chains, 0);
	std::vector < Sharing_file::Sharing_file> ecc_stop_type1 = pickup_sharingfile(sf, stop.chains, 1);
	std::vector < Sharing_file::Sharing_file> ecc_stop_type2 = pickup_sharingfile(sf, stop.chains, 2);
	std::vector < Sharing_file::Sharing_file> ecc_stop_type12 = sharing_file_merge(ecc_stop_type1, ecc_stop_type2);
	Sharing_file::Write_sharing_file_txt("ecc_stop_type0.txt", ecc_stop_type0);
	Sharing_file::Write_sharing_file_txt("ecc_stop_type12.txt", ecc_stop_type12);


	printf("ECC penetrate num %d\n", penetrate.chains.size());
	printf("ECC edgeout num   %d\n", edgeout.chains.size());

}
int count_stop_in_PM(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&c) {
	int ret = 0;
	std::map<int, Sharing_file::Sharing_file> sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(itr->eventid, *itr));
	}
	int event_id = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		event_id = itr->basetracks.begin()->group_id;
		if (sf_map.count(event_id) == 0) {
			fprintf(stderr,"eventid = %d not found\n", event_id);
			exit(1);
		}
		if (sf_map.at(event_id).track_type == 0)ret += 1;
	}
	return ret;
}
int count_stop_in_PM_penecheck(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&c) {
	std::vector<std::pair<mfile0::M_Chain,int>> pene_cand;
	std::map<int, Sharing_file::Sharing_file> sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(itr->eventid, *itr));
	}
	int event_id = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		event_id = itr->basetracks.begin()->group_id;
		if (sf_map.count(event_id) == 0) {
			fprintf(stderr, "eventid = %d not found\n", event_id);
			exit(1);
		}
		if (sf_map.at(event_id).track_type != 0) {
			pene_cand.push_back(std::make_pair(*itr, sf_map.at(event_id).unix_time));
		}
	}

	std::multimap<int, Sharing_file::Sharing_file> sf_map_time;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(itr->unix_time, *itr));
	}

	int ret = 0;
	for (auto itr = pene_cand.begin(); itr != pene_cand.end(); itr++) {
		auto range = sf_map_time.equal_range(itr->second);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.trackerwall_id == 0)ret++;
			break;
		}
	}

	return ret;
}

std::vector<std::pair<mfile0::M_Chain, Sharing_file::Sharing_file>> sharing_file_matching(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&c) {
	std::vector<std::pair<mfile0::M_Chain, Sharing_file::Sharing_file>> ret;
	std::map<int, Sharing_file::Sharing_file> sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(itr->eventid, *itr));
	}
	int event_id = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		event_id = itr->basetracks.begin()->group_id;
		if (sf_map.count(event_id) == 0)continue;
		ret.push_back(std::make_pair(*itr, sf_map.at(event_id)));
	}
	return ret;
}
std::vector<mfile0::M_Chain> chain_merge(std::vector<mfile0::M_Chain>&c1, std::vector<mfile0::M_Chain>&c2) {
	std::vector<mfile0::M_Chain> ret;
	for (auto &c : c1) {
		ret.push_back(c);
	}
	for (auto &c : c2) {
		ret.push_back(c);
	}
	return ret;
}
std::vector < Sharing_file::Sharing_file> sharing_file_merge(std::vector < Sharing_file::Sharing_file>&sf0, std::vector < Sharing_file::Sharing_file>&sf1) {
	std::vector < Sharing_file::Sharing_file> ret;
	for (auto &sf : sf0) {
		ret.push_back(sf);
	}
	for (auto &sf : sf1) {
		ret.push_back(sf);
	}
	return ret;
}
std::vector < Sharing_file::Sharing_file> sharing_file_delete_sametime(std::vector < Sharing_file::Sharing_file>&sf) {
	std::vector < Sharing_file::Sharing_file> ret;
	std::map<std::pair<int, int>, Sharing_file::Sharing_file> sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		auto res = sf_map.insert(std::make_pair(std::make_pair(itr->unix_time, itr->tracker_track_id), *itr));
		if (!res.second) {
			if (res.first->second.chi2_shifter[2] > itr->chi2_shifter[2]) {
				res.first->second = *itr;
			}
		}
	}
	for (auto itr = sf_map.begin(); itr != sf_map.end(); itr++) {
		ret.push_back(itr->second);
	}

	return ret;

}


void Print_PM_tracktype(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&c) {

	std::vector<std::pair<mfile0::M_Chain, Sharing_file::Sharing_file>> chain_p = sharing_file_matching(sf, c);

	int stop = 0, penetrate = 0;
	for (auto itr = chain_p.begin(); itr != chain_p.end(); itr++) {
		if (itr->second.track_type == 0) {
			stop++;
		}
		else {
			penetrate++;
		}
	}
	printf("PM stop num      %d\n", stop);
	printf("PM pene num      %d\n", penetrate);
}

void Print_ECC_tracktype(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&stop, std::vector<mfile0::M_Chain>&penetrate, std::vector<mfile0::M_Chain>&edgeout) {

	std::vector<std::pair<mfile0::M_Chain, Sharing_file::Sharing_file>>sf_stop = sharing_file_matching(sf, stop);
	std::vector<std::pair<mfile0::M_Chain, Sharing_file::Sharing_file>>sf_pene = sharing_file_matching(sf, penetrate);
	std::vector<std::pair<mfile0::M_Chain, Sharing_file::Sharing_file>>sf_edgeout = sharing_file_matching(sf, edgeout);

	printf("ECC stop      %d\n", int(sf_stop.size()));
	printf("ECC penetrate %d\n", int(sf_pene.size()));
	printf("ECC edgeout   %d\n", int(sf_edgeout.size()));

}


std::vector < Sharing_file::Sharing_file> pickup_sharingfile(std::vector<Sharing_file::Sharing_file> &sf, std::vector<mfile0::M_Chain>&c,int track_type) {
	std::vector<std::pair<mfile0::M_Chain, Sharing_file::Sharing_file>> chain_sf = sharing_file_matching(sf, c);
	std::vector < Sharing_file::Sharing_file> ret;
	for (auto &p : chain_sf) {
		if (p.second.track_type == track_type) {
			ret.push_back(p.second);
		}
	}
	return ret;
}