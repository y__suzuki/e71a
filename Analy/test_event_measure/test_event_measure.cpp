#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <omp.h>
#include <set>


vxx::base_track_t read_base(std::string file_in_ECC, std::pair<int, int> rawid);
vxx::base_track_t read_muon(std::string file_in_ECC, int pl, mfile0::M_Base mu_base);
std::pair<bool, vxx::base_track_t> basetrack_apply_local_ali(vxx::base_track_t t, std::string file_in_ECC, int pl0, int pl1);
vxx::base_track_t basetrack_tans(vxx::base_track_t t, std::vector<corrmap0::Corrmap> corr);
void basetrack_inverse_trans(vxx::base_track_t &t, corrmap0::Corrmap param);
void basetrack_affine_trans(vxx::base_track_t &t, corrmap0::Corrmap param);
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> &corr);
void partner_search(vxx::base_track_t target, std::vector<vxx::base_track_t>&all, double gap, double z_range[2], double accuracy, std::vector<vxx::base_track_t>&connect);
void Set_search_z(double z_range[2], int muonPL, int searchPL);
std::vector<mfile0::M_Chain> Get_Mfile_chain(std::vector<vxx::base_track_t> &base, std::map<int, double> z_map, std::vector<corrmap0::Corrmap> &corr, mfile0::M_Chain mu);
void clac_md(vxx::base_track_t target, vxx::base_track_t partner, double gap, double z_range[2]);

int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mu-mfile in-ECC-Area-path output-mfile\n");
		exit(1);
	}


	std::string file_in_mfile = argv[1];
	std::string file_in_ECC_path = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	//gid,(pl,rawid)
	std::map<int, std::pair<int, int>> muon_map;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (itr->chain_id % 100000 == 0) {
			muon_map.insert(std::make_pair(itr->basetracks.begin()->group_id, std::make_pair(itr->basetracks.rbegin()->pos / 10, itr->basetracks.rbegin()->rawid)));
		}
	}

	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC_path << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);


	for (int i = 0; i < m.chains.size(); i++) {
		printf("chain %4d/%4d start\n", i + 1, m.chains.size());
		std::pair<int, int> up_muon = muon_map.at(m.chains[i].basetracks.begin()->group_id);
		std::pair<int, int> target;
		if (m.chains[i].pos1 / 10 == up_muon.first) {
			target = std::make_pair(m.chains[i].basetracks.rbegin()->pos / 10, m.chains[i].basetracks.rbegin()->rawid);
		}
		else if (m.chains[i].pos0 / 10 - 1 == up_muon.first) {
			target = std::make_pair(m.chains[i].basetracks.begin()->pos / 10, m.chains[i].basetracks.begin()->rawid);
		}
		else continue;

		vxx::base_track_t muon_base = read_base(file_in_ECC_path, up_muon);
		vxx::base_track_t partner_base = read_base(file_in_ECC_path, target);


		//あるPLでのattachの探索
		//nominal gap + corrmapで変換
		//mdで探索
		int stop_pl = muon_base.pl;
		int search_pl = partner_base.pl;
		printf("muon PL%03d search PL%03d\n", stop_pl, search_pl);

		std::pair<bool, vxx::base_track_t> mu_trans = basetrack_apply_local_ali(muon_base, file_in_ECC_path, stop_pl, search_pl);
		if (!mu_trans.first)continue;
		if (z_map.count(stop_pl) + z_map.count(search_pl) != 2) {
			printf("gap data not found PL%03d/PL%03d\n", stop_pl, search_pl);
			continue;
		}
		//gapはmuon z=0としたときの探索対象のz
		double gap;
		gap = z_map.at(search_pl) - z_map.at(stop_pl);
		if (stop_pl > search_pl)gap -= mu_trans.second.z;
		else if (stop_pl < search_pl)gap += mu_trans.second.z;

		double z_range[2];
		//z_rangeの設定をする
		Set_search_z(z_range, stop_pl, search_pl);
		printf("z-range (%.0lf, %.0lf) gap=%.0lf\n", z_range[0], z_range[1], gap);
		//accuracy-->md探索範囲=dz*accuracy+5
		//角度精度みたいなもん
		//if (md > 10 + 100. / 2500.*fabs(extra[0] + extra[1]) / 2)continue;
		clac_md(mu_trans.second, partner_base, gap, z_range);

	}

}
vxx::base_track_t read_base(std::string file_in_ECC, std::pair<int, int> rawid) {
	int pl = rawid.first;
	std::stringstream file_in_base;
	file_in_base << file_in_ECC << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	vxx::BvxxReader br;
	std::array<int, 2> index = { rawid.second,rawid.second + 1 };//1234<=rawid<=5678であるようなものだけを読む。
	std::vector<vxx::base_track_t >base = br.ReadAll(file_in_base.str(), pl, 0, vxx::opt::index = index);


	vxx::base_track_t ret = *base.begin();
	return ret;
}

vxx::base_track_t read_muon(std::string file_in_ECC, int pl, mfile0::M_Base mu_base) {

	std::stringstream file_in_base;
	file_in_base << file_in_ECC << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t >base = br.ReadAll(file_in_base.str(), pl, 0);

	vxx::base_track_t ret;
	bool flg = false;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->rawid == mu_base.rawid) {
			ret = *itr;
			flg = true;
			break;
		}
	}

	if (!flg) {
		printf("rawid=%d basetrack not found\n", mu_base.rawid);
		return ret;
	}
	return ret;
}
std::pair<bool, vxx::base_track_t> basetrack_apply_local_ali(vxx::base_track_t t, std::string file_in_ECC, int pl0, int pl1) {
	if (pl0 == pl1) {
		vxx::base_track_t ret = t;
		t.z = 0;
		return std::make_pair(true, ret);
	}
	//pl0の座標系-->pl1の座標系
	std::stringstream file_in_ali;
	file_in_ali << file_in_ECC << "\\0\\align\\corrmap-align-" << std::setw(3) << std::setfill('0') << std::min(pl0, pl1) << "-"
		<< std::setw(3) << std::setfill('0') << std::max(pl0, pl1) << ".lst";
	if (!std::filesystem::exists(file_in_ali.str())) {
		printf("alignment not found\n");
		printf("%s\n", file_in_ali.str().c_str());
		return std::make_pair(false, t);
	}
	//corrmapの読み込み
	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_ali.str(), corr, 0);
	vxx::base_track_t ret = t;
	ret = basetrack_tans(t, corr);
	return std::make_pair(true, ret);
}
vxx::base_track_t basetrack_tans(vxx::base_track_t t, std::vector<corrmap0::Corrmap> corr) {
	//変換するtrackのPLが基準(小さい)
	if (t.pl == corr.begin()->pos[0] / 10) {
		//corrmapの場所のパラメータで逆変換
		corrmap0::Corrmap param;
		double dist;
		double c_x, c_y;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			c_x = (itr->areax[0] + itr->areax[1]) / 2;
			c_y = (itr->areay[0] + itr->areay[1]) / 2;
			if (itr == corr.begin()) {
				dist = pow(t.x - c_x, 2) + pow(t.y - c_y, 2);
				param = *itr;
			}
			if (dist > pow(t.x - c_x, 2) + pow(t.y - c_y, 2)) {
				dist = pow(t.x - c_x, 2) + pow(t.y - c_y, 2);
				param = *itr;
			}
		}
		basetrack_inverse_trans(t, param);
		t.z = param.dz;
	}
	//変換するtrackのPLが変換先(大きい)
	else if (t.pl == corr.begin()->pos[1] / 10) {
		//corrmapの場所を逆変換
		//最近接の場所のparameterで変換
		corrmap_area_inverse(corr);
		corrmap0::Corrmap param;
		double dist;
		double c_x, c_y;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			c_x = (itr->areax[0] + itr->areax[1]) / 2;
			c_y = (itr->areay[0] + itr->areay[1]) / 2;
			if (itr == corr.begin()) {
				dist = pow(t.x - c_x, 2) + pow(t.y - c_y, 2);
				param = *itr;
			}
			if (dist > pow(t.x - c_x, 2) + pow(t.y - c_y, 2)) {
				dist = pow(t.x - c_x, 2) + pow(t.y - c_y, 2);
				param = *itr;
			}
		}
		basetrack_affine_trans(t, param);
		t.z = param.dz;
	}
	else {
		printf("exception!! corrmap not found\n");
		printf("corrmap PL: %03d - %03d\n", corr.begin()->pos[0] / 10, corr.begin()->pos[1] / 10);
		printf("basetrack PL : %03d\n", t.pl);
		exit(1);
	}
	return t;

}
void basetrack_inverse_trans(vxx::base_track_t &t, corrmap0::Corrmap param) {
	double factor;
	double tmp_x, tmp_y;
	factor = 1 / (param.position[0] * param.position[3] - param.position[1] * param.position[2]);
	tmp_x = t.x - param.position[4];
	tmp_y = t.y - param.position[5];
	t.x = factor * (tmp_x*param.position[3] - tmp_y * param.position[1]);
	t.y = factor * (tmp_y*param.position[0] - tmp_x * param.position[2]);

	factor = 1 / (param.angle[0] * param.angle[3] - param.angle[1] * param.angle[2]);
	tmp_x = t.ax - param.angle[4];
	tmp_y = t.ay - param.angle[5];
	t.ax = factor * (tmp_x*param.angle[3] - tmp_y * param.angle[1]);
	t.ay = factor * (tmp_y*param.angle[0] - tmp_x * param.angle[2]);
}
void basetrack_affine_trans(vxx::base_track_t &t, corrmap0::Corrmap param) {
	double tmp_x, tmp_y;
	tmp_x = t.x;
	tmp_y = t.y;
	t.x = tmp_x * param.position[0] + tmp_y * param.position[1] + param.position[4];
	t.y = tmp_x * param.position[2] + tmp_y * param.position[3] + param.position[5];

	tmp_x = t.ax;
	tmp_y = t.ay;
	t.ax = tmp_x * param.angle[0] + tmp_y * param.angle[1] + param.angle[4];
	t.ay = tmp_x * param.angle[2] + tmp_y * param.angle[3] + param.angle[5];
}
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> &corr) {
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		double area[4], factor;
		area[0] = itr->areax[0] - itr->position[4];
		area[1] = itr->areax[1] - itr->position[4];
		area[2] = itr->areay[0] - itr->position[5];
		area[3] = itr->areay[1] - itr->position[5];

		factor = 1 / (itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);
		itr->areax[0] = factor * (area[0] * itr->position[3] - area[2] * itr->position[1]);
		itr->areay[0] = factor * (area[2] * itr->position[0] - area[0] * itr->position[2]);
		itr->areax[1] = factor * (area[1] * itr->position[3] - area[3] * itr->position[1]);
		itr->areay[1] = factor * (area[3] * itr->position[0] - area[1] * itr->position[2]);
	}

}
void clac_md(vxx::base_track_t target, vxx::base_track_t partner, double gap, double z_range[2]) {

	matrix_3D::vector_3D pos, dir;
	pos.x = target.x;
	pos.y = target.y;
	pos.z = 0;

	dir.x = target.ax;
	dir.y = target.ay;
	dir.z = 1;

	double md, extra[2], allowance;
	matrix_3D::vector_3D pos_all, dir_all;
	std::ofstream ofs("md_data2.txt", std::ios::app);

	pos_all.x = partner.x;
	pos_all.y = partner.y;
	pos_all.z = gap;
	dir_all.x = partner.ax;
	dir_all.y = partner.ay;
	dir_all.z = 1;

	md = matrix_3D::minimum_distance(pos, pos_all, dir, dir_all, z_range, extra);
	ofs << std::right << std::fixed
		<< std::setw(4) << std::setprecision(0) << target.pl << " "
		<< std::setw(10) << std::setprecision(0) << target.rawid << " "
		<< std::setw(4) << std::setprecision(0) << partner.pl << " "
		<< std::setw(10) << std::setprecision(0) << partner.rawid << " "
		<< std::setw(7) << std::setprecision(1) << pos.z << " "
		<< std::setw(7) << std::setprecision(1) << pos_all.z << " "
		<< std::setw(7) << std::setprecision(1) << extra[0] << " "
		<< std::setw(7) << std::setprecision(1) << extra[1] << " "
		<< std::setw(10) << std::setprecision(4) << md << " "
		<< std::setw(7) << std::setprecision(1) << (pos.z + extra[0] + pos_all.z + extra[1]) / 2 << " "
		<< std::setw(7) << std::setprecision(1) << int((pos.z + extra[0] + pos_all.z + extra[1]) / 2 > -2580) << std::endl;


}



void partner_search(vxx::base_track_t target, std::vector<vxx::base_track_t>&all, double gap, double z_range[2], double accuracy, std::vector<vxx::base_track_t>&connect) {

	matrix_3D::vector_3D pos, dir;
	pos.x = target.x;
	pos.y = target.y;
	pos.z = 0;

	dir.x = target.ax;
	dir.y = target.ay;
	dir.z = 1;

	double md, extra[2], allowance;
	matrix_3D::vector_3D pos_all, dir_all;
	std::ofstream ofs("md_data.txt");
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		pos_all.x = itr->x;
		pos_all.y = itr->y;
		pos_all.z = gap;
		dir_all.x = itr->ax;
		dir_all.y = itr->ay;
		dir_all.z = 1;
		if (fabs(dir_all.x) > 4.0)continue;
		if (fabs(dir_all.y) > 4.0)continue;
		md = matrix_3D::minimum_distance(pos, pos_all, dir, dir_all, z_range, extra);
		if (false) {
			if (itr->rawid == 7262027 || itr->rawid == 5907371) {
				printf("\n----------------------------------------\n");
				printf("raw=%d md %.2lf oa %.4lf\n", itr->rawid, md, matrix_3D::opening_angle(dir, dir_all));
				printf("%5.4lf %5.4lf %5.4lf %5.4lf\n", dir_all.x, dir_all.y, dir.x, dir.y);
				printf("%8.1lf %8.1lf %8.1lf %8.1lf %8.1lf\n", pos_all.x, pos_all.y, pos.x, pos.y, pos_all.z);
				printf("%8.1lf %8.1lf \n", pos_all.x - (pos.x + dir.x*pos_all.z), pos_all.y - (pos.y + dir.y*pos_all.z));
				printf("\n----------------------------------------\n");
			}
		}
		allowance = pow(accuracy*extra[0] + 10, 2) + pow(accuracy*extra[1] + 10, 2);
		if (md*md < allowance) {
			connect.push_back(*itr);
			ofs << std::right << std::fixed
				<< std::setw(4) << std::setprecision(0) << target.pl << " "
				<< std::setw(10) << std::setprecision(0) << target.rawid << " "
				<< std::setw(4) << std::setprecision(0) << itr->pl << " "
				<< std::setw(10) << std::setprecision(0) << itr->rawid << " "
				<< std::setw(7) << std::setprecision(1) << extra[0] << " "
				<< std::setw(7) << std::setprecision(1) << extra[1] << " "
				<< std::setw(10) << std::setprecision(4) << md << std::endl;

		}
	}


}

void Set_search_z(double z_range[2], int muonPL, int searchPL) {
	bool iron = false;
	bool water = false;
	bool other = false;
	if (4 <= muonPL && muonPL <= 14)iron = true;
	else if (16 <= muonPL && muonPL % 2 == 0)iron = true;
	else if (16 <= muonPL && muonPL % 2 == 1)water = true;
	else other = true;

	//forward探索
	if (muonPL >= searchPL) {
		if (iron) {
			//iron-emulsion-water- (+a)
			z_range[0] = -500 - 350;
			z_range[1] = +200;
		}
		else if (water) {
			//water-emulsion-iron-emulsion-water- (+a)
			z_range[0] = -2300 - 350;
			z_range[1] = +200;
		}
		else {
			z_range[0] = -500 - 350;
			z_range[1] = +200;
		}
	}
	//backward探索
	else {
		if (iron) {
			//iron-emulsion-water- (+a)
			z_range[0] = -500 - 350;
			z_range[1] = +200;
		}
		else if (water) {
			//water-emulsion-iron-emulsion-water- (+a)
			z_range[0] = -2300 - 350;
			z_range[1] = +200;
		}
		else {
			z_range[0] = -500 - 350;
			z_range[1] = +200;
		}
	}

}
std::vector<mfile0::M_Chain> Get_Mfile_chain(std::vector<vxx::base_track_t> &base, std::map<int, double> z_map, std::vector<corrmap0::Corrmap> &corr, mfile0::M_Chain mu) {

	std::vector<mfile0::M_Chain> ret;
	//ret.push_back(mu);

	int64_t groupid = mu.basetracks.begin()->group_id;
	groupid++;
	//groupidはgid*100000でつけられている
	int count = 0, countmax = 99999;
	int pl = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		count++;
		if (count >= countmax) {
			fprintf(stderr, "partner cand over %d\n", countmax);
			continue;
		}

		mfile0::M_Chain c;
		mfile0::M_Base b;
		pl = itr->pl;
		auto res = z_map.find(pl);
		if (res == z_map.end()) {
			printf("gap nominal not found PL%03d\n", pl);
			continue;
		}
		b.z = res->second;
		corrmap0::Corrmap param;
		bool flg = false;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			if (itr->pos[0] / 10 == pl) {
				param = *itr;
				flg = true;
				break;
			}
		}
		if (!flg) {
			printf("corrmap abs not found PL%03d\n", pl);
			continue;
		}
		b.group_id = groupid;
		groupid++;
		b.pos = pl * 10 + 1;
		b.rawid = itr->rawid;
		b.ph = itr->m[0].ph + itr->m[1].ph;
		b.ax = itr->ax*param.angle[0] + itr->ay*param.angle[1] + param.angle[4];
		b.ay = itr->ax*param.angle[2] + itr->ay*param.angle[3] + param.angle[5];
		b.x = itr->x*param.position[0] + itr->y*param.position[1] + param.position[4];
		b.y = itr->x*param.position[2] + itr->y*param.position[3] + param.position[5];
		b.z += param.dz;
		b.flg_d[0] = 0;
		b.flg_d[1] = 0;
		b.flg_i[0] = 0;
		b.flg_i[1] = 0;
		b.flg_i[2] = 0;
		b.flg_i[3] = 0;
		c.basetracks.push_back(b);
		c.chain_id = b.group_id;
		c.nseg = c.basetracks.size();
		c.pos0 = c.basetracks.begin()->pos;
		c.pos1 = c.basetracks.rbegin()->pos;
		ret.push_back(c);
	}
	return ret;
}