#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
# include <iostream>
# include <random>
#include <set>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <list>
class stop_track {
public:
	uint64_t chainid, groupid;
	int  nseg, npl, pl0, pl1, ph, rawid;
	double ax, ay, x, y, z;
};
class track_pair {
public:
	int pl0, pl1, raw0, raw1;
	double x, y, z, md, oa;
};
class track_multi {
public:
	int eventid, pl;
	double x, y, z;
	std::vector< std::pair<double, stop_track>> trk;
	std::vector<track_pair>pair;
};
void read_vtx_file(std::string filename, std::vector<track_multi>&multi);
void wrtie_mfile(std::string file_path, std::vector<std::pair<std::pair<int, int>, mfile0::Mfile>> &mfile_all);
std::vector<std::pair<std::pair<int, int>, mfile0::Mfile>> read_mfile(std::string filename, std::multimap<uint64_t, std::pair<int, int>> &chainid);
std::multimap<uint64_t, std::pair<int, int>> vtx_chainid_pick(std::vector<track_multi>&multi);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file_in_vtx file_in_mfile file_out_mfile\n");
		exit(1);
	}
	std::string file_in_vtx = argv[1];
	std::string file_in_mfile = argv[2];
	std::string file_out_path = argv[3];

	std::vector<track_multi>multi;
	read_vtx_file(file_in_vtx, multi);


	std::multimap<uint64_t, std::pair<int, int>> chainid=vtx_chainid_pick(multi);
	std::vector<std::pair<std::pair<int, int>, mfile0::Mfile>> mfile_all=read_mfile(file_in_mfile, chainid);
	wrtie_mfile(file_out_path, mfile_all);

}

void read_vtx_file(std::string filename, std::vector<track_multi>&multi) {
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0, trk_num;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);

		track_multi m;
		m.eventid = std::stoi(str_v[0]);
		m.pl = std::stoi(str_v[1]);
		m.x = std::stod(str_v[3]);
		m.y = std::stod(str_v[4]);
		m.z = std::stod(str_v[5]);
		trk_num = std::stoi(str_v[2]);

		for (int i = 0; i < trk_num*(trk_num - 1) / 2; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			track_pair p;
			p.pl0 = std::stoi(str_v[0]);
			p.raw0 = std::stoi(str_v[1]);
			p.pl1 = std::stoi(str_v[2]);
			p.raw1 = std::stoi(str_v[3]);
			p.x = std::stod(str_v[4]);
			p.y = std::stod(str_v[5]);
			p.z = std::stod(str_v[6]);
			p.oa = std::stod(str_v[7]);
			p.md = std::stod(str_v[8]);
			m.pair.push_back(p);
		}
		for (int i = 0; i < trk_num; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			stop_track s;
			s.pl0 = std::stoi(str_v[0]);
			s.pl1 = std::stoi(str_v[1]);
			s.rawid = std::stoi(str_v[2]);
			s.chainid = std::stoll(str_v[3]);
			s.groupid = std::stoll(str_v[4]);
			s.nseg = std::stoi(str_v[5]);
			s.npl = std::stoi(str_v[6]);
			s.ph = std::stoi(str_v[7]);
			s.ax = std::stod(str_v[8]);
			s.ay = std::stod(str_v[9]);
			s.x = std::stod(str_v[10]);
			s.y = std::stod(str_v[11]);
			s.z = 0;
			double ip = std::stod(str_v[12]);

			m.trk.push_back(std::make_pair(ip, s));
		}

		multi.push_back(m);
	}

}
std::multimap<uint64_t, std::pair<int,int>> vtx_chainid_pick(std::vector<track_multi>&multi) {

	std::multimap<uint64_t, std::pair<int, int>> ret;
	for (auto itr = multi.begin(); itr != multi.end(); itr++) {
		for (auto itr2 = itr->trk.begin(); itr2 != itr->trk.end(); itr2++) {
			ret.insert(std::make_pair(itr2->second.chainid, std::make_pair(itr->pl, itr->eventid)));
		}
	}
	return ret;
}
std::vector<std::pair<std::pair<int, int>, mfile0::Mfile>> read_mfile(std::string filename, std::multimap<uint64_t, std::pair<int, int>> &chainid) {
	std::map<std::pair<int, int>, mfile1::MFile> mfile_all;


	std::ifstream ifs(filename, std::ios::binary);


	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }
	uint64_t count = 0;
	for (int64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		//一旦chainを読む
		mfile1::MFileChain1 chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }

		std::vector< mfile1::MFileBase1> basetracks;
		basetracks.reserve(chain.nseg);

		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase1 base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			basetracks.emplace_back(base);

		}
		if (chainid.count(chain.chain_id) == 0)continue;

		auto range = chainid.equal_range(chain.chain_id);
		for (auto res = range.first; res != range.second; res++) {
			if (mfile_all.count(res->second) == 0) {
				mfile1::MFile mfile_tmp;
				mfile_tmp.header = mfile.header;
				mfile_tmp.info_header = mfile.info_header;
				mfile_all.insert(std::make_pair(res->second, mfile_tmp));
			}
			auto find = mfile_all.find(res->second);
			find->second.all_basetracks.push_back(basetracks);
			find->second.chains.push_back(chain);

		}

	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

	uint64_t N_base = 0, N_Chain = 0;

	for (auto itr = mfile_all.begin(); itr != mfile_all.end(); itr++) {
		N_base = 0;
		 N_Chain = 0;
		 for (auto itr2 = itr->second.all_basetracks.begin(); itr2 != itr->second.all_basetracks.end(); itr2++) {
			 N_base += itr2->size();
			 N_Chain++;
		 }
		 itr->second.info_header.Nbasetrack = N_base;
		 itr->second.info_header.Nchain = N_Chain;
	}

	std::vector<std::pair<std::pair<int, int>, mfile0::Mfile>> ret;

	for (auto itr = mfile_all.begin(); itr != mfile_all.end(); itr++) {
		mfile0::Mfile m;
		mfile1::converter(itr->second, m);
		ret.push_back(std::make_pair(itr->first, m));

	}
	return ret;


}
void wrtie_mfile(std::string file_path, std::vector<std::pair<std::pair<int, int>, mfile0::Mfile>> &mfile_all) {


	for (auto itr = mfile_all.begin(); itr != mfile_all.end(); itr++) {
		std::stringstream file_out;
		file_out << file_path << "\\m_" << std::setw(3) << std::setfill('0') << itr->first.first << "_" << std::setw(8) << std::setfill('0') << itr->first.second << ".all";
		mfile0::write_mfile(file_out.str(), itr->second);

	}

}
