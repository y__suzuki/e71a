#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class correction_val {
public:
	int pl, face,sensor_id;
	double angle_min,angle_max,angle,mean, sigma;
};
bool sort_corr(const correction_val&left, const correction_val&right) {
	if (left.pl != right.pl)return left.pl < right.pl;
	if (fabs(left.angle - right.angle) > 0.01)return left.angle < right.angle;
	if (left.sensor_id!=right.sensor_id)return left.sensor_id < right.sensor_id;
	return left.face < right.face;
}

void Calc_mean_sigma(std::vector<Momentum_recon::Mom_basetrack> &base_v, int face, int sensor, double &mean, double &sigma);
void Calc_correction(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v);
void Adapt_correction(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v);
void output_correction(std::string filename, std::vector<correction_val> &corr_v);

int judege_sensor_id(int id);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-momch file-out-momch file-correction-file\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];
	std::string file_out_res = argv[3];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::vector<correction_val> corr_v;
	Calc_correction(momch, corr_v);
	output_correction(file_out_res, corr_v);

	Adapt_correction(momch, corr_v);

	Momentum_recon::Write_mom_chain_extension(file_out_momch, momch);

}
void Calc_correction(std::vector<Momentum_recon::Mom_chain>&mom_ch,std::vector<correction_val> &corr_v) {
	std::multimap<int, Momentum_recon::Mom_basetrack> base;
	for (auto itr = mom_ch.begin(); itr != mom_ch.end(); itr++) {
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			if (!isfinite(itr->ecc_mcs_mom))continue;
			if (itr->ecc_mcs_mom < 1000)continue;
			base.insert(std::make_pair(itr2->pl, *itr2));
		}
	}

	double angle_divide = 0.2;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr = std::next(itr, base.count(itr->first))) {
		std::multimap<int, Momentum_recon::Mom_basetrack> base_ang;
		auto range = base.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			angle = sqrt(res->second.ax*res->second.ax + res->second.ay*res->second.ay);
			base_ang.insert(std::make_pair(int(angle / angle_divide), res->second));
		}

		for (auto itr2 = base_ang.begin(); itr2 != base_ang.end(); itr2=std::next(itr2,base_ang.count(itr2->first))) {
			double angle_min = itr2->first*angle_divide;
			double angle_max = (itr2->first + 1)*angle_divide;
			std::vector<Momentum_recon::Mom_basetrack> base_v;
			auto range = base_ang.equal_range(itr2->first);
			for (auto res = range.first; res != range.second; res++) {
				base_v.push_back(res->second);
			}
			correction_val corr;
			corr.angle_min = angle_min;
			corr.angle_max = angle_max;
			corr.angle = (angle_min + angle_max )/ 2;
			corr.pl = itr->first;
			for (int face = 0; face <= 1; face++) {
				for (int sensor = 0; sensor <= 1; sensor++) {
					corr.face = face;
					corr.sensor_id = sensor;
					Calc_mean_sigma(base_v, corr.face, corr.sensor_id, corr.mean, corr.sigma);
					if (corr.mean > 0)corr_v.push_back(corr);
				}
			}
		}
	}
}
void Calc_mean_sigma(std::vector<Momentum_recon::Mom_basetrack> &base_v,int face,int sensor,double &mean,double &sigma) {
	double sum = 0, sum2 = 0;
	int count = 0;
	double val = 0;
	for (auto itr = base_v.begin(); itr != base_v.end(); itr++) {
		if (judege_sensor_id(itr->m[face].imager) != sensor)continue;
		val= itr->m[face].hitnum / sqrt(1 + itr->ax*itr->ax + itr->ay*itr->ay);
		sum += val;
		sum2 += val * val;
		count += 1;
	}
	if (count >10) {
		mean = sum / count;
		sigma = sqrt(sum2 / count - mean * mean);
	}
	else {
		mean = -1; 
		sigma = -1;
	}
}
void Adapt_correction(std::vector<Momentum_recon::Mom_chain>&mom_ch, std::vector<correction_val> &corr_v) {

	std::map<std::tuple<int,int,int>, std::map<double,correction_val>> corr_map;
	std::tuple<int, int, int>id;
	for (auto itr = corr_v.begin(); itr != corr_v.end(); itr++) {
		std::get<0>(id) = itr->pl;
		std::get<1>(id) = itr->face;
		std::get<2>(id) = itr->sensor_id;
		if (corr_map.count(id) == 0) {
			std::map<double, correction_val> corr_v;
			corr_v.insert(std::make_pair(itr->angle_max, *itr));
			corr_map.insert(std::make_pair(id, corr_v));
		}
		else {
			corr_map.at(id).insert(std::make_pair(itr->angle_max, *itr));
		}
	}

	double angle;
	for (auto itr = mom_ch.begin(); itr != mom_ch.end(); itr++) {
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			angle = sqrt(itr2->ax*itr2->ax + itr2->ay*itr2->ay);
			std::get<0>(id) = itr2->pl;
			for (int face = 0; face <= 1; face++) {
				std::get<1>(id) = face;
				std::get<2>(id) = judege_sensor_id(itr2->m[face].imager);
				if (corr_map.count(id) == 0 ) {
					fprintf(stderr, "correction map PL%03d face%d sensor%d not found\n", std::get<0>(id), std::get<1>(id), std::get<2>(id));
					exit(1);
				}
				auto corr_map_id = corr_map.at(id);
				auto range_end = corr_map_id.upper_bound(angle);
				//0-0.2
				if (range_end == corr_map_id.begin()) {
					itr2->m[face].hitnum = (itr2->m[face].hitnum / sqrt(1 + itr2->ax*itr2->ax + itr2->ay*itr2->ay)) / range_end->second.mean * 100;
				}
				//0.2-5.0
				else if (range_end != corr_map_id.end()) {
					auto range_start = std::next(range_end, -1);
					double x0 = range_start->second.angle;
					double x1 = range_end->second.angle;
					double y0 = range_start->second.mean;
					double y1 = range_end->second.mean;
					double mean_interporate = (y1 - y0) / (x1 - x0)*(angle - x0) + y0;
					itr2->m[face].hitnum = (itr2->m[face].hitnum / sqrt(1 + itr2->ax*itr2->ax + itr2->ay*itr2->ay)) / mean_interporate * 100;
				}
				//5.0-
				else {
					itr2->m[face].hitnum = (itr2->m[face].hitnum / sqrt(1 + itr2->ax*itr2->ax + itr2->ay*itr2->ay)) / corr_map_id.rend()->second.mean * 100;
				}
			}
		}
	}


}

void output_correction(std::string filename,std::vector<correction_val> &corr_v) {
	sort(corr_v.begin(), corr_v.end(), sort_corr);
	std::ofstream ofs(filename);


	for (auto itr = corr_v.begin(); itr != corr_v.end(); itr++) {
		ofs << std::fixed << std::right
			<< std::setw(5) << std::setprecision(0) << itr->pl << " "
			<< std::setw(3) << std::setprecision(0) << itr->face << " "
			<< std::setw(3) << std::setprecision(0) << itr->sensor_id << " "
			<< std::setw(7) << std::setprecision(4) << itr->angle << " "
			<< std::setw(7) << std::setprecision(4) << itr->angle_min << " "
			<< std::setw(7) << std::setprecision(4) << itr->angle_max << " "
			<< std::setw(5) << std::setprecision(1) << itr->mean << " "
			<< std::setw(7) << std::setprecision(3) << itr->sigma << std::endl;
	}

}

int judege_sensor_id(int id) {
	if ((24 <= id && id <= 35) || id == 52)return 0;
	else return 1;
}