#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Mom_chain> pickup_shorttrack(std::vector<Momentum_recon::Mom_chain> &momch, int cut_thr);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	momch = pickup_shorttrack(momch, 10);
	Momentum_recon::Write_mom_chain_extension(file_out_momch, momch);

	exit(0);



}
std::vector<Momentum_recon::Mom_chain> pickup_shorttrack(std::vector<Momentum_recon::Mom_chain> &momch, int cut_thr) {
	std::vector<Momentum_recon::Mom_chain> ret;

	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		if (itr->base.size() > cut_thr)continue;
		ret.push_back(*itr);


	}
	return ret;
}