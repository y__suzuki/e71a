#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>

class Microtrack_inf {
public:
	int pos, rawid, view, imager, ph, ph2, vph2, area;
	float ax, ay;
};
class Basetrack_inf {
public:
	uint64_t chain_id;
	int pl, zone, rawid;
	double ax, ay, mom;
	Microtrack_inf m[2];
};
class Chain_inf {
public:
	uint64_t chain_id;
	int zone;
	double ax, ay, mom, ph, vph;
};
std::multimap<uint64_t, Basetrack_inf> read_basetrack_inf(std::string filename, std::vector<corrmap0::Corrmap> &corr);
void base_trans(Basetrack_inf&base, corrmap0::Corrmap&param);

std::vector<Chain_inf> Calc_chain_inf(std::multimap<uint64_t, Basetrack_inf>&chain);
Chain_inf Calc_chain_inf(std::vector<Basetrack_inf>&base);

void out_chain_inf(std::string filename, std::vector<Chain_inf>&chain);
void out_chain_inf2(std::string filename, std::vector<Chain_inf>&chain);
void VPH_correction(std::vector<Chain_inf>&chain);
void Calc_vph_correction(Chain_inf &c);
void read_base_inf(std::string filename, std::vector<Basetrack_inf>&b);
void base_inf_corr(std::vector<Basetrack_inf>&base);
void write_base_inf(std::string filename, std::vector<Basetrack_inf>&base);

int main(int argc,char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	std::string file_in_corr_abs = argv[2];
	std::string file_out_ave = argv[3];
	std::string file_out_ave2 = argv[4];
	std::string file_out_base2 = argv[5];
	//corrrmap abs読み込み
	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corr_abs, corr);
	//読み込み&座標返還
	std::multimap<uint64_t, Basetrack_inf> base = read_basetrack_inf(file_in_base, corr);
	//chainごとに分ける average 計算
	std::vector<Chain_inf> chain_inf = Calc_chain_inf(base);
	//average 出力
	out_chain_inf(file_out_ave, chain_inf);
	VPH_correction(chain_inf);
	out_chain_inf2(file_out_ave2, chain_inf);

	std::vector<Basetrack_inf> b;
	read_base_inf(file_in_base, b);
	base_inf_corr(b);
	write_base_inf(file_out_base2, b);
	//mip/black分ける

	//mip出力

	//black出力

}
std::multimap<uint64_t, Basetrack_inf> read_basetrack_inf(std::string filename,std::vector<corrmap0::Corrmap> &corr) {
	std::map<int, corrmap0::Corrmap> corr_map;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		corr_map.insert(std::make_pair(itr->pos[0] / 10, *itr));
	}

	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	Basetrack_inf base;
	std::multimap<uint64_t, Basetrack_inf> ret;
	while (ifs.read((char*)& base, sizeof(Basetrack_inf))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		base_trans(base, corr_map.at(base.pl));
		ret.insert(std::make_pair(base.chain_id, base));
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;
}
void base_trans(Basetrack_inf&base,corrmap0::Corrmap&param) {
	double tmp_x, tmp_y;
	tmp_x = base.ax;
	tmp_y = base.ay;
	base.ax = tmp_x * param.angle[0] + tmp_y * param.angle[1] + param.angle[4];
	base.ay = tmp_x * param.angle[2] + tmp_y * param.angle[3] + param.angle[5];
}

std::vector<Chain_inf> Calc_chain_inf(std::multimap<uint64_t, Basetrack_inf>&chain) {
	std::vector<Chain_inf> ret;
	std::vector<Basetrack_inf> base_inf;
	base_inf.reserve(150);
	int count = 0;
	for (auto itr = chain.begin(); itr != chain.end();itr++) {
		count = chain.count(itr->first);
		base_inf.clear();
		auto range = chain.equal_range(itr->first);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			base_inf.push_back(itr2->second);
		}
		ret.push_back(Calc_chain_inf(base_inf));

		itr = std::next(itr, count - 1);
	}

	return ret;
}
Chain_inf Calc_chain_inf(std::vector<Basetrack_inf>&base) {
	Chain_inf ret;
	ret.chain_id = base.begin()->chain_id;
	ret.mom = base.begin()->mom;
	std::map<int,int> zone_count;
	double vph[2], ph[2], ax[2], ay[2];
	double count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ax[0] += itr->ax;
		ay[0] += itr->ay;
		ph[0] += int((itr->m[0].ph + itr->m[1].ph) / 10000);
		vph[0] += int((itr->m[0].ph + itr->m[1].ph) % 10000);

		ax[1] += itr->ax*itr->ax;
		ay[1] += itr->ay*itr->ay;
		ph[1] += int((itr->m[0].ph + itr->m[1].ph) / 10000)*int((itr->m[0].ph + itr->m[1].ph) / 10000);
		vph[1] += int((itr->m[0].ph + itr->m[1].ph) % 10000)*int((itr->m[0].ph + itr->m[1].ph) % 10000);

		auto res = zone_count.insert(std::make_pair(itr->zone, 1));
		if (!res.second) {
			res.first->second++;
		}
		count++;
	}

	ret.ax = ax[0] / count;
	ret.ay = ay[0] / count;
	ret.ph = ph[0] / count;
	ret.vph = vph[0] / count;

	int num;
	for (auto itr = zone_count.begin(); itr != zone_count.end(); itr++) {
		if (itr == zone_count.begin()) {
			num = itr->second;
			ret.zone = itr->first;
		}
		if (itr->second > num) {
			num = itr->second;
			ret.zone = itr->first;
		}
	}
	return ret;
}

void out_chain_inf(std::string filename, std::vector<Chain_inf>&chain) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (chain.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = chain.begin(); itr != chain.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write  ... %d/%d (%4.1lf%%)", count, int(chain.size()), count*100. / chain.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(20) << std::setprecision(0) << itr->chain_id << " "
				<< std::setw(8) << std::setprecision(1) << itr->mom << " "
				<< std::setw(7) << std::setprecision(4) << itr->ax << " "
				<< std::setw(7) << std::setprecision(4) << itr->ay << " "
				<< std::setw(5) << std::setprecision(1) << itr->ph << " "
				<< std::setw(6) << std::setprecision(1) << itr->vph << " "
				<< std::setw(4) << std::setprecision(0) << itr->zone << std::endl;
		}
		fprintf(stderr, "\r Write  ... %d/%d (%4.1lf%%)\n", count, int(chain.size()), count*100. / chain.size());
	}
}
void out_chain_inf2(std::string filename, std::vector<Chain_inf>&chain) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (chain.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = chain.begin(); itr != chain.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write  ... %d/%d (%4.1lf%%)", count, int(chain.size()), count*100. / chain.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(20) << std::setprecision(0) << itr->chain_id << " "
				<< std::setw(8) << std::setprecision(1) << itr->mom << " "
				<< std::setw(7) << std::setprecision(4) << itr->ax << " "
				<< std::setw(7) << std::setprecision(4) << itr->ay << " "
				<< std::setw(5) << std::setprecision(1) << itr->ph << " "
				<< std::setw(6) << std::setprecision(3) << itr->vph << " "
				<< std::setw(4) << std::setprecision(0) << itr->zone << std::endl;
		}
		fprintf(stderr, "\r Write  ... %d/%d (%4.1lf%%)\n", count, int(chain.size()), count*100. / chain.size());
	}
}

void VPH_correction(std::vector<Chain_inf>&chain) {
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		Calc_vph_correction(*itr);
	}
}
void Calc_vph_correction(Chain_inf &c) {
	double angle = sqrt(c.ax*c.ax + c.ay*c.ay);
	double a, b;
	if (angle < 1.85) {
		a = 0.0014*pow(angle, 4) - 0.0097*pow(angle, 3) + 0.022*pow(angle, 2) - 0.0183*angle + 0.0093;
		b = -1 * exp(-5.68238*angle + 0.18384) + 0.809942;
	}
	else {
		a = 0.05722;
		b = 0.809909;
	}
	c.vph = a * c.vph + b;
}
void read_base_inf(std::string filename, std::vector<Basetrack_inf>&b) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	Basetrack_inf base;
	while (ifs.read((char*)& base, sizeof(Basetrack_inf))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		b.push_back(base);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}

}
void base_inf_corr(std::vector<Basetrack_inf>&base) {
	double angle, a, b, vph0, vph1, ph0, ph1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angle < 1.85) {
			a = 0.0014*pow(angle, 4) - 0.0097*pow(angle, 3) + 0.022*pow(angle, 2) - 0.0183*angle + 0.0093;
			b = -1 * exp(-5.68238*angle + 0.18384) + 0.809942;
		}
		else {
			a = 0.05722;
			b = 0.809909;
		}
		vph0 = a * (itr->m[0].ph % 10000) + b;
		ph0 = itr->m[0].ph / 10000;
		vph1 = a * (itr->m[1].ph % 10000) + b;
		ph1 = itr->m[1].ph / 10000;
		itr->m[0].ph = (int)ph0 * 10000 + std::min(9999, int(vph0 * 100));
		itr->m[1].ph = (int)ph1 * 10000 + std::min(9999, int(vph1 * 100));
	}
}
void write_base_inf(std::string filename, std::vector<Basetrack_inf>&base) {
	std::ofstream ofs(filename, std::ios::binary);

	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (base.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = base.size();

	for (auto b : base) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& b, sizeof(Basetrack_inf));

	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}
