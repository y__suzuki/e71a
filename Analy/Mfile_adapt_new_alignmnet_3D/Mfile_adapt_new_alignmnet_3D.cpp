#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#include <omp.h>

class align_param {
public:
	int pl, id, signal, ix, iy;
	//視野中心
	double x, y, z;
	//parameter(9);
	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;

};
class align_param2 {
public:
	align_param* corr_p[3];
	//3点の視野中心の重心(回転中心)
	double x, y, z;
	//parameter(9);
	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;
public:
	//3つのparameterから計算
	void Calc_9param();

};
class align_track_inf {
public:
	mfile0::M_Base *base;
	mfile0::M_Chain *chain;
};

std::map<int, std::vector<align_param>> read_ali_param(std::string filename, bool output);
std::vector <std::pair<vxx::base_track_t*, align_param2*>>track_affineparam_correspondence(std::vector<vxx::base_track_t>&base, std::vector <align_param2> &param);
align_param2* search_param(std::vector<align_param*> &param, vxx::base_track_t&base, std::multimap<int, align_param2*>&triangles);
double select_triangle_vale(align_param2* param, vxx::base_track_t&base);
std::vector <align_param2 >DelaunayDivide(std::vector <align_param >&corr);
int use_thread(double ratio, bool output);
void basetrack_trans(std::vector<std::pair<align_param*, std::vector<align_track_inf>>> &track_param);

std::map<int, std::vector<align_track_inf>> chain_extract_pointer(std::vector<mfile0::M_Chain>*c);
std::vector <std::pair<mfile0::M_Base*, align_param2*>>track_affineparam_correspondence(std::vector<align_track_inf>&c_base, std::vector <align_param2> &param);
std::vector <align_param2 >DelaunayDivide(std::vector <align_param >&corr);
align_param2* search_param(std::vector<align_param*> &param, mfile0::M_Base&base, std::multimap<int, align_param2*>&triangles);
double select_triangle_vale(align_param2* param, mfile0::M_Base &base);
void trans_base(std::vector<mfile0::M_Base*>&base, align_param2 *param);
void trans_base_all(std::vector < std::pair<mfile0::M_Base*, align_param2*>>&track_pair);

int main(int argc, char**argv) {

	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-mfile in-corrmap file-out-mfile \n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_corr = argv[2];
	std::string file_out_mfile = argv[3];
	std::map<int, std::vector<align_param>> corr_map = read_ali_param(file_in_corr, 1);


	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::map<int, std::vector<align_track_inf>> track_inf_v = chain_extract_pointer(&(m.chains));

	int pl = 0;
	for (auto itr = corr_map.begin(); itr != corr_map.end(); itr++) {
		pl = itr->first;
		printf("adapt align PL%03d\n", pl);
		//delaunay3角形分割
		std::vector <align_param2 >corr2 = DelaunayDivide(itr->second);

		auto track = track_inf_v.find(pl);
		if (track == track_inf_v.end()) {
			printf("track not found\n");
			continue;
		}

		//trackとdelaunay3角形の対応
		std::vector < std::pair<mfile0::M_Base*, align_param2*>> track_param = track_affineparam_correspondence(track->second,corr2);
		//basetrackを変換
		trans_base_all(track_param);
	}
	//std::vector <align_param > corr = read_ali_param(file_in_align, false);

	mfile1::write_mfile_extension(file_out_mfile, m);

}
std::map<int, std::vector<align_param>> read_ali_param(std::string filename, bool output) {


	std::multimap<int, align_param> read_corr;
	align_param param_tmp;
	std::ifstream ifs(filename);
	std::string dx, dy, dz;

	std::set<std::tuple<int, int, int>> read_id;
	int count = 0;
	while (ifs >> param_tmp.pl >> param_tmp.id >> param_tmp.ix >> param_tmp.iy >> param_tmp.signal
		>> param_tmp.x >> param_tmp.y >> param_tmp.z
		>> param_tmp.x_rot >> param_tmp.y_rot >> param_tmp.z_rot
		>> param_tmp.x_shrink >> param_tmp.y_shrink >> param_tmp.z_shrink
		>> param_tmp.yx_shear >> param_tmp.zx_shear >> param_tmp.zy_shear
		>> dx >> dy >> dz) {
		if (read_id.count(std::make_tuple(param_tmp.pl, param_tmp.ix, param_tmp.iy)) == 1)continue;
		read_id.insert(std::make_tuple(param_tmp.pl, param_tmp.ix, param_tmp.iy));


		count++;
		//if (!std::all_of(dx.cbegin(), dx.cend(), isdigit))continue;
		//if (!std::all_of(dy.cbegin(), dy.cend(), isdigit))continue;
		//if (!std::all_of(dz.cbegin(), dz.cend(), isdigit))continue;
		//printf("%3d %5d %3d %3d %5d %8.1lf %8.1lf %8.1lf %6.5lf %6.5lf %6.5lf %6.5lf %6.5lf %6.5lf %6.5lf %6.5lf %6.5lf %s %s %s\n",
		//	param_tmp.pl, param_tmp.id, param_tmp.ix, param_tmp.iy, param_tmp.signal,
		//	param_tmp.x, param_tmp.y, param_tmp.z,
		//	param_tmp.x_rot, param_tmp.y_rot, param_tmp.z_rot,
		//	param_tmp.x_shrink, param_tmp.y_shrink, param_tmp.z_shrink,
		//	param_tmp.yx_shear, param_tmp.zx_shear, param_tmp.zy_shear,
		//	dx.c_str(), dy.c_str(), dz.c_str());

		param_tmp.dx = std::stod(dx);
		param_tmp.dy = std::stod(dy);
		param_tmp.dz = std::stod(dz);

		if (!isfinite(param_tmp.dx))continue;
		if (!isfinite(param_tmp.dy))continue;
		if (!isfinite(param_tmp.dz))continue;
		//printf("ture\n");
		read_corr.insert(std::make_pair(param_tmp.pl, param_tmp));

		//printf("ix %d iy%d\n", param_tmp.ix, param_tmp.iy);

	}
	if (output == 1) {
		fprintf(stderr, "%s input finish\n", filename.c_str());
	}
	if (read_corr.size() == 0) {
		fprintf(stderr, "%s alignment miss!\n", filename.c_str());
		exit(1);
	}

	printf("all param %d\n", count);
	printf("read param %d\n", read_corr.size());

	count = 0;
	std::vector<align_param> corr_v;;
	std::map<int, std::vector<align_param>> ret;
	for (auto itr = read_corr.begin(); itr != read_corr.end(); itr++) {
		count = read_corr.count(itr->first);
		corr_v.clear();

		auto range = read_corr.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			corr_v.push_back(res->second);
		}
		ret.insert(std::make_pair(itr->first, corr_v));

		itr = std::next(itr, count - 1);
	}
	return ret;

}

std::vector <align_param2 >DelaunayDivide(std::vector <align_param >&corr) {

	//delaunay分割
	std::vector<double> x, y;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		x.push_back(itr->x);
		y.push_back(itr->y);
	}

	delaunay::DelaunayTriangulation DT(x, y); // (std::vector<double> x, std::vector<double> y, uint32_t seed_)
	DT.execute(); // (double min_delta = 1e-6, double max_delta = 1e-5, int max_miss_count = 30)
	std::vector<delaunay::Edge> edge = DT.get_edges();

	std::multimap<int, int> edge_map;

	for (auto itr = edge.begin(); itr != edge.end(); itr++) {
		edge_map.insert(std::make_pair(std::min(itr->first, itr->second), std::max(itr->first, itr->second)));

	}
	std::set<std::tuple<int, int, int>>triangle;
	std::set<int> vertex;
	for (auto itr = edge_map.begin(); itr != edge_map.end(); itr++) {
		//itr->firstの点=aを通る三角形の探索
		vertex.clear();
		auto range = edge_map.equal_range(itr->first);
		//aを通りitr->secondの点=bに行く。bのsetを作成
		for (auto res = range.first; res != range.second; res++) {
			vertex.insert(res->second);
		}
		//bを通る線分の探索
		for (auto itr2 = vertex.begin(); itr2 != vertex.end(); itr2++) {
			if (edge_map.count(*itr2) == 0)continue;
			auto range2 = edge_map.equal_range(*itr2);
			//bを通る線分の中からaから始まる線分を探す
			for (auto res = range2.first; res != range2.second; res++) {
				if (vertex.count(res->second) == 1) {
					triangle.insert(std::make_tuple(itr->first, *itr2, res->second));
				}
			}

		}
	}

	std::vector <align_param2 > ret;
	for (auto itr = triangle.begin(); itr != triangle.end(); itr++) {
		//printf("delaunay triangle %d %d %d\n", std::get<0>(*itr), std::get<1>(*itr), std::get<2>(*itr));
		align_param2 param;
		param.corr_p[0] = &(corr[std::get<0>(*itr)]);
		param.corr_p[1] = &(corr[std::get<1>(*itr)]);
		param.corr_p[2] = &(corr[std::get<2>(*itr)]);
		param.x = 0;
		param.y = 0;
		param.z = 0;
		param.z_shrink = 0;
		for (int i = 0; i < 3; i++) {
			param.x += param.corr_p[i]->x;
			param.y += param.corr_p[i]->y;
			param.z += param.corr_p[i]->z;
			param.z_shrink += param.corr_p[i]->z_shrink;
		}
		param.x = param.x / 3;
		param.y = param.y / 3;
		param.z = param.z / 3;
		param.z_shrink = param.z_shrink / 3;
		param.zx_shear = 0;
		param.zy_shear = 0;

		param.Calc_9param();

		ret.push_back(param);
	}

	return ret;

}

std::vector <std::pair<mfile0::M_Base*, align_param2*>>track_affineparam_correspondence(std::vector<align_track_inf>&c_base, std::vector <align_param2> &param) {

	//local alignの視野中心を取り出して、位置でhash
	//local alignの視野中心の作るdelaunay三角形をmapで対応

	std::map<int, align_param*> view_center;
	std::multimap<int, align_param2*>triangles;
	double xmin = 999999, ymin = 999999, hash = 5000;
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		for (int i = 0; i < 3; i++) {
			view_center.insert(std::make_pair(itr->corr_p[i]->id, (itr->corr_p[i])));
			triangles.insert(std::make_pair(itr->corr_p[i]->id, &(*itr)));
			xmin = std::min(itr->corr_p[i]->x, xmin);
			ymin = std::min(itr->corr_p[i]->y, ymin);
		}
	}
	std::multimap<std::pair<int, int>, align_param*> view_center_hash;
	std::pair<int, int>id;
	for (auto itr = view_center.begin(); itr != view_center.end(); itr++) {
		id.first = int((itr->second->x - xmin) / hash);
		id.second = int((itr->second->y - ymin) / hash);
		view_center_hash.insert(std::make_pair(id, itr->second));
	}

	std::vector < std::pair<mfile0::M_Base*, align_param2*>> ret;
	std::vector<align_param*> param_cand;
	int loop = 0, ix, iy, count = 0;
	for (auto itr = c_base.begin(); itr != c_base.end(); itr++) {
		if (count % 100000 == 0) {
			printf("\r search correspond triangles %d/%d(%4.1lf%%)", count, c_base.size(), count*100. / c_base.size());
		}
		count++;
		ix = (itr->base->x - xmin) / hash;
		iy = (itr->base->y - ymin) / hash;
		loop = 1;
		while (true) {
			param_cand.clear();
			for (int iix = ix - loop; iix <= ix + loop; iix++) {
				for (int iiy = iy - loop; iiy <= iy + loop; iiy++) {
					id.first = iix;
					id.second = iiy;
					if (view_center_hash.count(id) != 0) {
						auto range = view_center_hash.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							param_cand.push_back(res->second);
						}
					}
				}
			}
			if (param_cand.size() > 2)break;
			loop++;
		}
		align_param2* param2 = search_param(param_cand, *itr->base, triangles);
		ret.push_back(std::make_pair((itr->base), param2));
	}
	printf("\r search correspond triangles %d/%d(%4.1lf%%)\n", count, c_base.size(), count*100. / c_base.size());

	return ret;
}
align_param2* search_param(std::vector<align_param*> &param, mfile0::M_Base&base, std::multimap<int, align_param2*>&triangles) {
	//三角形内部
	//最近接三角形
	double dist = 0;
	std::map<double, align_param* > dist_map;
	//align_paramを近い順にsort
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		dist = ((*itr)->x - base.x)*((*itr)->x - base.x) + ((*itr)->y - base.y)*((*itr)->y - base.y);
		dist_map.insert(std::make_pair(dist, (*itr)));
	}

	double sign[3];
	bool flg = false;
	int id;

	align_param2* ret = triangles.begin()->second;
	for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
		if (itr != dist_map.begin())continue;


		//corrmapのID
		id = itr->second->id;
		if (triangles.count(id) == 0) {
			fprintf(stderr, "alignment triangle ID=%d not found\n", id);
			exit(1);
		}
		//idの属する三角形を探索
		auto range = triangles.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			sign[0] = (itr2->second->corr_p[1]->x - itr2->second->corr_p[0]->x)*(base.y - itr2->second->corr_p[1]->y) - (itr2->second->corr_p[1]->y - itr2->second->corr_p[0]->y)*(base.x - itr2->second->corr_p[1]->x);
			sign[1] = (itr2->second->corr_p[2]->x - itr2->second->corr_p[1]->x)*(base.y - itr2->second->corr_p[2]->y) - (itr2->second->corr_p[2]->y - itr2->second->corr_p[1]->y)*(base.x - itr2->second->corr_p[2]->x);
			sign[2] = (itr2->second->corr_p[0]->x - itr2->second->corr_p[2]->x)*(base.y - itr2->second->corr_p[0]->y) - (itr2->second->corr_p[0]->y - itr2->second->corr_p[2]->y)*(base.x - itr2->second->corr_p[0]->x);
			//printf("point %.lf,%.1lf\n", base.x, base.y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[0]->x, itr2->second->corr_p[0]->y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[1]->x, itr2->second->corr_p[1]->y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[2]->x, itr2->second->corr_p[2]->y);
			//printf("sign %.1lf %1.lf %.1lf\n", sign[0], sign[1], sign[2]);
			//printf("  signbit %d %d %d\n", std::signbit(sign[0]), std::signbit(sign[1]), std::signbit(sign[2]));
			//printf("n signbit %d %d %d\n", !std::signbit(sign[0]), !std::signbit(sign[1]), !std::signbit(sign[2]));
			//printf("judge %d\n", (std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2])));
			//printf("\n");

			//符号が3つとも一致でtrue
			if ((std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2]))) {
				ret = itr2->second;
				flg = true;
				break;
			}
		}
		if (flg)break;
	}
	if (flg) {
		//printf("point in trianlge\n");
		return ret;
	}

	//distが最小になるcorrmapをとってくる
	dist = -1;
	for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
		//corrmapのID
		id = itr->second->id;
		if (triangles.count(id) == 0) {
			fprintf(stderr, "alignment triangle ID=%d not found\n", id);
			exit(1);
		}
		//idの属する三角形を探索
		auto range = triangles.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			if (dist<0 || dist>select_triangle_vale(itr2->second, base)) {
				dist = select_triangle_vale(itr2->second, base);
				ret = itr2->second;
			}
		}
	}
	//printf("point not in trianlge\n");
	return ret;
}
double select_triangle_vale(align_param2* param, mfile0::M_Base &base) {
	double x, y;
	double dist = 0;
	x = (param->corr_p[0]->x + param->corr_p[1]->x + param->corr_p[2]->x) / 3;
	y = (param->corr_p[0]->y + param->corr_p[1]->y + param->corr_p[2]->y) / 3;
	dist = (base.x - x)*(base.x - x) + (base.y - y)*(base.y - y);
	return dist;
}

//変換 zshrink補正-->9para変換
void trans_base_all(std::vector < std::pair<mfile0::M_Base*, align_param2*>>&track_pair) {
	std::map<std::tuple<int, int, int>, align_param2*> param_map;
	std::multimap<std::tuple<int, int, int>, mfile0::M_Base*>base_map;
	std::tuple<int, int, int>id;
	//三角形ごとにbasetrackをまとめる
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		std::get<0>(id) = itr->second->corr_p[0]->id;
		std::get<1>(id) = itr->second->corr_p[1]->id;
		std::get<2>(id) = itr->second->corr_p[2]->id;
		param_map.insert(std::make_pair(id, itr->second));
		base_map.insert(std::make_pair(id, itr->first));
	}


	//ここで三角形ごとに変換
	int count = 0;
	std::vector<mfile0::M_Base*> t_base;
	for (auto itr = param_map.begin(); itr != param_map.end(); itr++) {
		if (count % 1000 == 0) {
			printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)", count, param_map.size(), count*100. / param_map.size());
		}
		count++;

		t_base.clear();

		if (base_map.count(itr->first) == 0)continue;
		auto range = base_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			t_base.push_back(res->second);
		}
		trans_base(t_base, itr->second);

	}
	printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)\n", count, param_map.size(), count*100. / param_map.size());

}
void trans_base(std::vector<mfile0::M_Base*>&base, align_param2 *param) {

	matrix_3D::matrix_33 x_rot_mat(0, param->x_rot), y_rot_mat(1, param->y_rot), z_rot_mat(2, param->z_rot), all_trans(0, 0), shear_mat(0, 0), shrink_mat(0, 0);

	shrink_mat.val[0][0] *= param->x_shrink;
	shrink_mat.val[1][1] *= param->y_shrink;
	//shrink_mat.val[2][2] *= param->z_shrink;
	shear_mat.val[0][1] = param->yx_shear;
	shear_mat.val[0][2] = param->zx_shear;
	shear_mat.val[1][2] = param->zy_shear;

	matrix_3D::vector_3D shift, center;
	center.x = param->x;
	center.y = param->y;
	center.z = param->z;
	shift.x = param->dx;
	shift.y = param->dy;
	shift.z = param->dz;

	all_trans.matrix_multiplication(shear_mat);
	all_trans.matrix_multiplication(shrink_mat);
	all_trans.matrix_multiplication(z_rot_mat);
	all_trans.matrix_multiplication(y_rot_mat);
	all_trans.matrix_multiplication(x_rot_mat);

	//all_trans.Print();
	matrix_3D::vector_3D base_p0, base_p1;
	const double base_thick_nom = 210;
	double base_thick;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_p0.x = (*itr)->x;
		base_p0.y = (*itr)->y;
		base_p0.z = (*itr)->z;
		//角度shrink分はここでかける
		base_thick = 210 / param->z_shrink;
		base_p1.x = (*itr)->x + (*itr)->ax*base_thick;
		base_p1.y = (*itr)->y + (*itr)->ay*base_thick;
		base_p1.z = base_thick;

		//視野中心を原点に移動
		//base_p0 = matrix_3D::addition(base_p0, matrix_3D::const_multiple(center, -1));
		//base_p1 = matrix_3D::addition(base_p1, matrix_3D::const_multiple(center, -1));

		//変換の実行
		base_p0.matrix_multiplication(all_trans);
		base_p0 = matrix_3D::addition(base_p0, shift);
		base_p1.matrix_multiplication(all_trans);
		base_p1 = matrix_3D::addition(base_p1, shift);

		//原点をもとに戻す
		//base_p0 = matrix_3D::addition(base_p0, center);
		//base_p1 = matrix_3D::addition(base_p1, center);

		(*itr)->x = base_p0.x;
		(*itr)->y = base_p0.y;
		(*itr)->z = base_p0.z;

		//printf("ax:%.4lf --> %.4lf\n", (*itr)->ax, (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z));
		//printf("ay:%.4lf --> %.4lf\n", (*itr)->ay, (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z));

		(*itr)->ax = (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z);
		(*itr)->ay = (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z);

	}
}

void align_param2::Calc_9param() {


	double bp[3][3], ap[3][3], cos_z, sin_z;
	for (int i = 0; i < 3; i++) {
		bp[i][0] = corr_p[i]->x;
		bp[i][1] = corr_p[i]->y;
		bp[i][2] = corr_p[i]->z;

		cos_z = cos(corr_p[i]->z_rot);
		sin_z = sin(corr_p[i]->z_rot);


		ap[i][0] = corr_p[i]->x_shrink*cos_z*(corr_p[i]->x) - corr_p[i]->y_shrink*sin_z*(corr_p[i]->y) + corr_p[i]->dx;
		ap[i][1] = corr_p[i]->x_shrink*sin_z*(corr_p[i]->x) + corr_p[i]->y_shrink*cos_z*(corr_p[i]->y) + corr_p[i]->dy;
		ap[i][2] = corr_p[i]->z + corr_p[i]->dz;
		//printf("bp%d %8.1lf %8.1lf %8.1lf\n",i, bp[i][0], bp[i][1], bp[i][2]);
		//printf("ap%d %8.1lf %8.1lf %8.1lf\n", i,ap[i][0], ap[i][1], ap[i][2]);
	}
	//apの位置ずれvectorを定義
	double dp[2][3];
	for (int i = 0; i < 3; i++) {
		dp[0][i] = ap[1][i] - ap[0][i];
		dp[1][i] = ap[2][i] - ap[0][i];
	}
	//printf("0-->1 x,y,z : %.1lf %.1lf %.1lf\n", dp[0][0], dp[0][1], dp[0][2]);
	//printf("0-->2 x,y,z : %.1lf %.1lf %.1lf\n", dp[1][0], dp[1][1], dp[1][2]);
	//法線vector
	double n_v[3];
	n_v[0] = (dp[0][1] * dp[1][2] - dp[0][2] * dp[1][1]);
	n_v[1] = (dp[0][2] * dp[1][0] - dp[0][0] * dp[1][2]);
	n_v[2] = (dp[0][0] * dp[1][1] - dp[0][1] * dp[1][0]);

	//std::cout << "normal vector" << std::endl;
	//for (int i = 0; i < 3; i++) {
	//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
	//}

	x_rot = atan(n_v[1] / n_v[2]);
	n_v[1] = cos(x_rot)*n_v[1] - sin(x_rot)*n_v[2];
	n_v[2] = sin(x_rot)*n_v[1] + cos(x_rot)*n_v[2];
	//std::cout << "normal vector" << std::endl;
	//for (int i = 0; i < 3; i++) {
	//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
	//}
	y_rot = atan(-1 * n_v[0] / n_v[2]);
	n_v[0] = cos(y_rot)*n_v[0] + sin(y_rot)*n_v[2];
	n_v[2] = -1 * sin(y_rot)*n_v[0] + cos(y_rot)*n_v[2];

	//std::cout << "normal vector" << std::endl;
	//for (int i = 0; i < 3; i++) {
	//	std::cout << std::setw(14) << std::fixed << std::setprecision(10) << n_v[i] << std::endl;
	//}

	//printf("x rot:%.6lf\n", x_rot);
	//printf("y rot:%.6lf\n", y_rot);


	matrix_3D::matrix_33 x_rot_mat(0, x_rot), y_rot_mat(1, y_rot);
	matrix_3D::vector_3D ap_v[3];
	for (int i = 0; i < 3; i++) {
		ap_v[i].x = ap[i][0];
		ap_v[i].y = ap[i][1];
		ap_v[i].z = ap[i][2];
	}
	for (int i = 0; i < 3; i++) {
		ap_v[i].matrix_multiplication(x_rot_mat);
		ap_v[i].matrix_multiplication(y_rot_mat);
	}
	//for (int i = 0; i < 3; i++) {
	//	printf("point %d\n", i);
	//	printf("\t %.2lf %.2lf %.2lf\n", bp[i][0], bp[i][1], bp[i][2]);
	//	printf("\t %.2lf %.2lf %.2lf\n", ap_v[i].x, ap_v[i].y, ap_v[i].z);
	//}
	dz = (ap_v[0].z - bp[0][2] + ap_v[1].z - bp[1][2] + ap_v[2].z - bp[2][2]) / 3;
	//printf("dz=%.2lf\n", dz);
	//3元方程式を解く
	double a[2][3][3] = { { {bp[0][0],bp[0][1],1},{bp[1][0],bp[1][1],1},{bp[2][0],bp[2][1],1} },  { {bp[0][0],bp[0][1],1},{bp[1][0],bp[1][1],1},{bp[2][0],bp[2][1],1} } };
	double b[2][3] = { { ap_v[0].x,ap_v[1].x,ap_v[2].x },{ ap_v[0].y,ap_v[1].y,ap_v[2].y } };
	double c[2][3] = { {1, 1, 1},{1,1,1} };
	//gauss(a[0], b[0], c[0]);
	//gauss(a[1], b[1], c[1]);
	GaussJorden(a[0], b[0], c[0]);
	GaussJorden(a[1], b[1], c[1]);
	z_rot = atan(c[1][0] / c[0][0]);
	x_shrink = c[0][0] / cos(z_rot);
	y_shrink = (c[0][0] * c[1][1] - c[0][1] * c[1][0]) / (c[0][0] * cos(z_rot) + c[1][0] * sin(z_rot));
	yx_shear = (c[0][1] * cos(z_rot) + c[1][1] * sin(z_rot)) / (c[0][0] * cos(z_rot) + c[1][0] * sin(z_rot));

	dx = c[0][2];
	dy = c[1][2];
	matrix_3D::vector_3D dr;
	dr.x = c[0][2];
	dr.y = c[1][2];
	dr.z = dz;

	x_rot = x_rot * -1;
	y_rot = y_rot * -1;
	matrix_3D::matrix_33 x_rot_mat_inv(0, x_rot), y_rot_mat_inv(1, y_rot);


	dr.matrix_multiplication(y_rot_mat_inv);
	dr.matrix_multiplication(x_rot_mat_inv);

	dx = dr.x;
	dy = dr.y;
	dz = dr.z;

	//printf("x rot: %.6lf\n",x_rot);
	//printf("y rot: %.6lf\n",y_rot);
	//printf("z rot: %.6lf\n",z_rot);
	//printf("x shrink: %.6lf\n", x_shrink);
	//printf("y shrink: %.6lf\n", y_shrink);
	//printf("z shrink: %.6lf\n", z_shrink);
	//printf("x shift: %.5lf\n", dx);
	//printf("y shift: %.5lf\n", dy);
	//printf("z shift: %.5lf\n", dz);
	//printf("yx shear: %.6lf\n", yx_shear);
	//printf("zx shear: %.6lf\n", zx_shear);
	//printf("zy shear: %.6lf\n", zy_shear);

	//std::vector< matrix_3D::vector_3D >point,point_after;
	//for (int i = 0; i < 3; i++) {
	//	matrix_3D::vector_3D p;
	//	p.x = corr_p[i]->x;
	//	p.y = corr_p[i]->y;
	//	p.z = corr_p[i]->z;
	//	point.push_back(p);
	//	p.x = corr_p[i]->x + corr_p[i]->dx;
	//	p.y = corr_p[i]->y + corr_p[i]->dy;
	//	p.z = corr_p[i]->z + corr_p[i]->dz;
	//	point_after.push_back(p);
	//}
	//trans_9para(point, *this);
	//for (auto p : point_after) {
	//	printf("x:%10.1lf y:%10.1lf z:%10.1lf\n", p.x, p.y, p.z);
	//}
}


std::map<int, std::vector<align_track_inf>> chain_extract_pointer(std::vector<mfile0::M_Chain>*c) {
	std::map<int, std::vector<align_track_inf>> ret;
	std::multimap<int, align_track_inf> t_inf_map;
	int pl;
	align_track_inf t_inf;
	int64_t count = 0, all = c->size();
	for (auto itr = c->begin(); itr != c->end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r chain extract inf %lld/%lld(%4.1lf)", count, all, count*100. / all);
		}
		count++;


		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			pl = itr2->pos / 10;
			t_inf.base = &(*itr2);
			t_inf.chain = &(*itr);
			t_inf_map.insert(std::make_pair(pl, t_inf));
		}
	}
	printf("\r chain extract inf %d/%d(%4.1lf)\n", count, all, count*100. / all);


	count = 0;
	all = t_inf_map.size();
	for (auto itr = t_inf_map.begin(); itr != t_inf_map.end(); itr++) {
		count = t_inf_map.count(itr->first);
		std::vector<align_track_inf> buf;
		buf.reserve(count);
		pl = itr->first;

		auto range = t_inf_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			buf.push_back(res->second);
		}
		ret.insert(std::make_pair(pl, buf));
		itr = std::next(itr, count - 1);
	}
	printf("chain extract inf fin\n");
	return ret;
}

