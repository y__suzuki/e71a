#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <chrono>

#include <FILE_structure.hpp>

class output_format_micro {
public:
	int pos, view, imager, zone, isg, ph, vph, px;
};
class output_format_base {
public:
	int pl, rawid;
	double ax, ay, x, y, z;
	output_format_micro m[2];
};

class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};


bool checkFileExistence(const std::string& str);
std::vector<output_format_link> read_link_bin(std::string filename);
void write_linklet_txt(std::string filename, std::vector<output_format_link>&link);


int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "prg input-linklet(bin) output-linklet(txt)\n");
		exit(1);
	}
	std::string file_in_linklet = argv[1];
	std::string file_out_txt = argv[2];
	if (checkFileExistence(file_in_linklet) == false) {
		fprintf(stderr, "file [%s] not exist\n", file_in_linklet.c_str());
		return 0;
	}
	std::vector<output_format_link> link = read_link_bin(file_in_linklet);

	write_linklet_txt(file_out_txt, link);

}
bool checkFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}
std::vector<output_format_link> read_link_bin(std::string filename) {
	std::vector<output_format_link> link;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	output_format_link l;
	while (ifs.read((char*)& l, sizeof(output_format_link))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		link.emplace_back(l);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return link;
}
void write_linklet_txt(std::string filename, std::vector<output_format_link>&link) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open ���s
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (link.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = link.begin(); itr != link.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)", count, int(link.size()), count*100. / link.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(10) << std::setprecision(0) << itr->b[0].pl << " "
				<< std::setw(10) << std::setprecision(0) << itr->b[0].rawid << " "
				<< std::setw(10) << std::setprecision(4) << itr->b[0].ax << " "
				<< std::setw(10) << std::setprecision(4) << itr->b[0].ay << " "
				<< std::setw(10) << std::setprecision(1) << itr->b[0].x << " "
				<< std::setw(10) << std::setprecision(1) << itr->b[0].y << " "
				<< std::setw(10) << std::setprecision(1) << itr->b[0].z << " "

				<< std::setw(10) << std::setprecision(0) << itr->b[1].pl << " "
				<< std::setw(10) << std::setprecision(0) << itr->b[1].rawid << " "
				<< std::setw(10) << std::setprecision(4) << itr->b[1].ax << " "
				<< std::setw(10) << std::setprecision(4) << itr->b[1].ay << " "
				<< std::setw(10) << std::setprecision(1) << itr->b[1].x << " "
				<< std::setw(10) << std::setprecision(1) << itr->b[1].y << " "
				<< std::setw(10) << std::setprecision(1) << itr->b[1].z << " "

				<< std::setw(8) << std::setprecision(5) << itr->dax << " "
				<< std::setw(8) << std::setprecision(5) << itr->day << " "
				<< std::setw(8) << std::setprecision(5) << itr->dar << " "
				<< std::setw(8) << std::setprecision(5) << itr->dal << " "
				<< std::setw(10) << std::setprecision(1) << itr->dx << " "
				<< std::setw(10) << std::setprecision(1) << itr->dy << " "
				<< std::setw(10) << std::setprecision(1) << itr->dr << " "
				<< std::setw(10) << std::setprecision(1) << itr->dl << std::endl;

		}
		fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)\n", count, int(link.size()), count*100. /link.size());
	}
}


