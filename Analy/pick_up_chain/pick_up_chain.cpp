#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>
#include <omp.h>

std::vector<mfile0::M_Chain> chain_selection(std::vector<mfile0::M_Chain> &c);
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold);
std::vector<mfile0::M_Chain> chain_angle_selection(std::vector<mfile0::M_Chain>  &chain, double thr_ax, double thr_ay);
std::vector<mfile0::M_Chain> chain_nseg_selection(std::vector<mfile0::M_Chain> &chain, int nseg);
std::vector<mfile0::M_Chain> reject_Fe_ECC(std::vector<mfile0::M_Chain> &chain);

void calc_density(std::vector<mfile0::M_Chain> &chain);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "prg in-mfile out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];

	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	m.chains = chain_nseg_selection(m.chains, 15);
	m.chains = chain_angle_selection(m.chains, 4.0, 4.0);
	m.chains = reject_Fe_ECC(m.chains);
	m.chains = chain_dlat_selection(m.chains,0.01);

	m.chains = chain_nseg_selection(m.chains, 60);
	m.chains = chain_dlat_selection(m.chains, 0.005);

	calc_density(m.chains);

	printf("num of chain = %d \n", m.chains.size());
	printf("density = %.3lf /cm^3\n", m.chains.size()*1. / (25 * 25 * 25.));


	mfile1::write_mfile_extension(file_out_mfile, m);
}
std::vector<mfile0::M_Chain> chain_selection(std::vector<mfile0::M_Chain> &c) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if ((itr->pos0 / 10 == 3 )&& (itr->pos1 / 10 == 133)) {
			ret.push_back(*itr);
		}
	}
	printf("num of chain = %d \n", ret.size());
	printf("density = %.3lf /cm^2\n", ret.size()*1. / (25 * 25.));
	return ret;
}
std::vector<mfile0::M_Chain> chain_dlat_selection(std::vector<mfile0::M_Chain>  &chain, double threshold) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (mfile0::angle_diff_dev_lat(*itr, ax, ay) > threshold)continue;
		ret.push_back(*itr);
	}
	printf("chain lateral selection <= %5.4lf : %d --> %d (%4.1lf%%)\n", threshold, chain.size(), ret.size(), ret.size()*100. / chain.size());

	return ret;

}
std::vector<mfile0::M_Chain> chain_nseg_selection(std::vector<mfile0::M_Chain> &chain, int nseg) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr->nseg < nseg)continue;
		ret.push_back(*itr);
	}
	printf("chain nseg >= %d: %d --> %d (%4.1lf%%)\n", nseg, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::M_Chain> reject_Fe_ECC(std::vector<mfile0::M_Chain> &chain) {
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr->pos0 / 10 > 15) {
			ret.push_back(*itr);
		}
		else {
			int flg = 0;
			for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
				if (itr2->pos / 10 == 16)flg++;
				if (itr2->pos / 10 == 17)flg++;
				if (itr2->pos / 10 == 18)flg++;
				if (itr2->pos / 10 == 19)flg++;
				if (itr2->pos / 10 == 20)flg++;
				if (itr2->pos / 10 == 21)flg++;
				if (itr2->pos / 10 == 22)flg++;
				if (itr2->pos / 10 == 23)flg++;
			}
			if (flg >= 6 || itr->pos1 / 10 > 23) {
				ret.push_back(*itr);
			}
		}
	}
	printf("reject_Fe_ECC(PL1 >= 24): %d --> %d (%4.1lf%%)\n", chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}
std::vector<mfile0::M_Chain> chain_angle_selection(std::vector<mfile0::M_Chain>  &chain, double thr_ax, double thr_ay) {
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		if (thr_ax - fabs(ax) < 0)continue;
		if (thr_ay - fabs(ay) < 0)continue;
		ret.push_back(*itr);
	}
	printf("chain average angle selection |ax|<=%3.1lf |ay|<=%3.1lf : %d --> %d (%4.1lf%%)\n", thr_ax, thr_ay, chain.size(), ret.size(), ret.size()*100. / chain.size());
	return ret;
}

void calc_density(std::vector<mfile0::M_Chain> &chain) {
	std::map<int, int> count;
	int pl;
	for (auto &c : chain) {
		for (auto &b : c.basetracks) {
			pl = b.pos / 10;
			auto res=count.insert(std::make_pair(pl, 1));
			if (!res.second)res.first->second += 1;
		}
	}

	for (auto itr = count.begin(); itr != count.end(); itr++) {
		printf("PL%03d %10d density %10.3lf /cm^2\n", itr->first, itr->second, itr->second / (25 * 25.));
	}

}
