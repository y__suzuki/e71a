#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib,"FILE_structure.lib")

#include <VxxReader.h>
#include <FILE_structure.hpp>

#include <fstream>
#include <algorithm>
#include <map>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <omp.h>
#include <chrono>

std::vector<vxx::base_track_t> base_angle_cut(std::vector<vxx::base_track_t> &base, double angle_cut);

int main(int argc, char**argv) {
	if (argc != 5) {

		fprintf(stderr, "usage:prg in-bvxx pl angle out-bvxx\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	double angle_cut = std::stod(argv[3]);
	std::string file_out_bvxx = argv[4];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);
	base = base_angle_cut(base, angle_cut);
	vxx::BvxxWriter bw;
	bw.Write(file_out_bvxx, pl, 0, base);
}
std::vector<vxx::base_track_t> base_angle_cut(std::vector<vxx::base_track_t> &base, double angle_cut) {
	std::vector<vxx::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {

		if (sqrt(itr->ax*itr->ax + itr->ay*itr->ay) < angle_cut)continue;
		ret.push_back(*itr);

	}
	printf("angle cut(angle > %.1lf) %d --> %d (%4.1lf%%)\n", angle_cut,base.size(), ret.size(), ret.size()*100. / base.size());
	return ret;



}