#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

std::vector<std::vector<mfile0::M_Chain>> divide_event(mfile0::Mfile &m);
mfile0::M_Chain muon_pick_up(std::vector<mfile0::M_Chain>&c);
std::vector<mfile0::M_Chain> reject_penetrate_mip(std::vector<mfile0::M_Chain>&c);
std::vector<mfile0::M_Chain> reject_short_mip(std::vector<mfile0::M_Chain>&c);
std::vector<mfile0::M_Chain> reject_angle(std::vector<mfile0::M_Chain>&c);
bool judge_mip_short(mfile0::M_Chain c);
bool judge_mip_long(mfile0::M_Chain c);


bool sort_chain(const mfile0::M_Chain&left, const  mfile0::M_Chain&right) {
	if (left.basetracks.begin()->group_id != right.basetracks.begin()->group_id) {
		return left.basetracks.begin()->group_id < right.basetracks.begin()->group_id;
	}
	else if (left.pos0 != right.pos0) {
		return left.pos0 < right.pos0;
	}
	else{
		return left.nseg > right.nseg;
	}


}
bool sort_base(const mfile0::M_Base&left, const  mfile0::M_Base&right) {
	if (left.pos != right.pos) {
		return left.pos < right.pos;
	}
	return left.rawid < right.rawid;
}
int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mfile out-event-mfile-path out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile_path = argv[2];
	std::string file_out_mfile = argv[3];


	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	mfile0::Mfile m_out;
	m_out.header = m.header;

	//

	std::vector<std::vector<mfile0::M_Chain>> event_chain = divide_event(m);
	int64_t gid;
	for (int i = 0; i < event_chain.size(); i++) {
		gid = event_chain[i].begin()->basetracks.begin()->group_id / 100000;
		printf("\r event %d/%d (ID=%d)", i, event_chain.size(), gid);


		std::vector<mfile0::M_Chain> c;
		c = reject_short_mip(event_chain[i]);
		c = reject_penetrate_mip(c);
		c = reject_angle(c);
		//m_out�ɂ��ׂ�push back
		for (auto itr = c.begin(); itr != c.end(); itr++) {
			m_out.chains.push_back(*itr);
		}
		sort(c.begin(), c.end(), sort_chain);
		mfile0::Mfile m_out_one;
		m_out_one.header = m.header;
		m_out_one.chains = c;

		std::stringstream file_out_mfile_event;
		file_out_mfile_event << file_out_mfile_path << "\\event_" << std::setw(10) << std::setfill('0') << gid << ".all";

		mfile0::write_mfile(file_out_mfile_event.str(), m_out_one, 0);

	}
	sort(m_out.chains.begin(), m_out.chains.end(), sort_chain);
	mfile0::write_mfile(file_out_mfile, m_out);


}

std::vector<std::vector<mfile0::M_Chain>> divide_event(mfile0::Mfile &m) {
	std::vector<std::vector<mfile0::M_Chain>> ret;

	std::multimap<int64_t, mfile0::M_Chain> chain_map;
	int64_t gid;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		gid = itr->basetracks.begin()->group_id / 100000;
		chain_map.insert(std::make_pair(gid, *itr));
	}

	int count = 0;
	for (auto itr = chain_map.begin(); itr != chain_map.end(); itr++) {
		count = chain_map.count(itr->first);
		auto range = chain_map.equal_range(itr->first);
		std::vector<mfile0::M_Chain> chain_v;
		for (auto res = range.first; res != range.second; res++) {
			chain_v.push_back(res->second);
		}
		ret.push_back(chain_v);
		itr = std::next(itr, count - 1);
	}
	return ret;
}
std::vector<mfile0::M_Chain> reject_short_mip(std::vector<mfile0::M_Chain>&c) {
	mfile0::M_Chain mu = muon_pick_up(c);
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->basetracks.begin()->group_id % 100000 == 0)continue;
		if ((itr->pos1 - itr->pos0) / 10 + 1 > 10) {
			ret.push_back(*itr);
		}
		else {
			if (judge_mip_short(*itr)) { continue; }
			ret.push_back(*itr);
		}
	}

	ret.push_back(mu);
	return ret;
}
std::vector<mfile0::M_Chain> reject_penetrate_mip(std::vector<mfile0::M_Chain>&c) {
	mfile0::M_Chain mu = muon_pick_up(c);
	mfile0::M_Base mu_up = *mu.basetracks.rbegin();
	int stop_pl = mu.pos1 / 10;
	std::vector<mfile0::M_Chain> ret;
	int pl0, pl1;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->basetracks.begin()->group_id % 100000 == 0)continue;
		pl0 = itr->pos0 / 10;
		pl1 = itr->pos1 / 10;
		if (!judge_mip_long(*itr)) {
			ret.push_back(*itr);
		}
		else {
			if ((pl0 - stop_pl)*(pl1 - stop_pl) < 0)continue;
			else if ((pl0 - stop_pl) == 0) {
				double dist;
				dist = sqrt(pow(itr->basetracks.begin()->x - mu_up.x, 2) + pow(itr->basetracks.begin()->y - mu_up.y, 2));
				if (dist > 500)continue;
			}
			ret.push_back(*itr);
		}
	}

	ret.push_back(mu);
	return ret;
}
std::vector<mfile0::M_Chain> reject_angle(std::vector<mfile0::M_Chain>&c) {
	mfile0::M_Chain mu = muon_pick_up(c);
	mfile0::M_Base mu_up = *mu.basetracks.rbegin();
	int stop_pl = mu.pos1 / 10;
	std::vector<mfile0::M_Chain> ret;
	double ax, ay;
	int count = 0;
	ax = 0;
	ay = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->basetracks.begin()->group_id % 100000 == 0)continue;
		ax = 0;
		ay = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			ax += itr2->ax;
			ay += itr2->ay;
			count++;
		}
		ax = ax / count;
		ay = ay / count;
		if (fabs(ax) < 4 && fabs(ay) < 4) {
			ret.push_back(*itr);
		}
	}

	ret.push_back(mu);
	return ret;
}

bool judge_mip_short(mfile0::M_Chain c) {
	double angle = 0, vph = 0;
	double ax = 0, ay = 0;
	int count = 0;	for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
		ax += itr->ax;
		ay += itr->ay;
		vph += itr->ph % 10000;

		count++;
	}
	ax = ax / count;
	ay = ay / count;
	angle = sqrt(ax*ax + ay * ay);
	vph = vph / count;

	double param[3];

	param[0] = 3319.48;
	param[1] = -3.71618;
	param[2] = 13.4601;
	bool ret = vph < std::max(param[0] / pow(angle - param[1], 2) + param[2], 100.);

	return ret;
}
bool judge_mip_long(mfile0::M_Chain c) {
	double angle = 0, vph = 0;
	double ax = 0, ay = 0;
	int count = 0;
	for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
		ax += itr->ax;
		ay += itr->ay;
		vph += itr->ph % 10000;
		count++;
	}
	ax = ax / count;
	ay = ay / count;
	angle = sqrt(ax*ax + ay * ay);
	vph = vph / count;

	double param[3];
	param[0] = 15.1341;
	param[1] = -0.355553;
	param[2] = 80.2505;

	//sprintf(fomula, "%.4lf/(x-%.4lf)^2+%.4lf", param[0], param[1], param[2]);

	return vph < std::max(param[0] / pow(angle - param[1], 2) + param[2], 80.);
}



mfile0::M_Chain muon_pick_up(std::vector<mfile0::M_Chain>&c) {
	std::vector<mfile0::M_Chain> muon_cand;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->basetracks.begin()->group_id % 100000 == 0) {
			muon_cand.push_back(*itr);
		}
	}
	if (muon_cand.size() == 0) {
		printf("GID=%d muon not found\n", c.begin()->basetracks.begin()->group_id / 100000);
		exit(1);
	}
	else if (muon_cand.size() == 1) {
		return *muon_cand.begin();
	}
	else {
		mfile0::M_Chain ret;
		int cnt = 0;
		for (auto itr = muon_cand.begin(); itr != muon_cand.end(); itr++) {
			if (itr->pos0 / 10 > 5)continue;
			if (itr->pos1 / 10 >= 132)continue;
			if (cnt == 0) {
				ret = *itr;
				cnt++;
			}
			else {
				if (ret.nseg < itr->nseg) {
					ret = *itr;
				}
			}
		}
		if (cnt == 0) {
			printf("GID=%d muon not found\n", muon_cand.begin()->basetracks.begin()->group_id / 100000);
			exit(1);
		}
		return ret;

	}
}
