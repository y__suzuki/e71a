#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <VxxReader.h>
#include <picojson.h>
#include <omp.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <cassert>  // std::assert
#include <sstream>
#include <iomanip>
#include <string>
#include <numeric>


class EachImager_Param {
public:
	//CamareaID * 5 + SensorID = ImagerID
	//Width,Height 縦横のpixel数
	double Aff_coef[6], Aff_coef_offset[6], DZ;
	int CameraID, GridX, GridY, Height, ImagerID, SensorID, Width;
	std::string LastReportFilePath;
};
class EachView_Param {
public:
	int  LayerID, NPicThickOfLayer;
	double Stage_x, Stage_y, ThickOfLayer, Z_begin, Z_end;
};

void micro_2_pixel(int view, int imager, double &x, double &y, std::map<int, EachImager_Param>&imager_map, std::map<int, EachView_Param> &view_map);
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin);
std::vector<cv::Mat> read_spng(std::string filename);


int main(int argc, char **argv) {


	system("pause");


	if (argc != 1) {
		fprintf(stderr, "beta_path\n");
		exit(1);
	}
	int row, col;
	double x, y;

	row = 0;
	col = 0;
	x = 0;
	y = 0;



	int ViewID = 0, ImagerID = 0, NumberOfImager = 72;
	uint32_t ShotID;
	ShotID = ((uint32_t)(uint16_t)row << 16) | ((uint32_t)(uint16_t)col);
	ViewID = ShotID / NumberOfImager;
	ImagerID = ShotID % NumberOfImager;


	//コマンドライン引数の読み込み
	std::string file_in_beta_path = argv[0];

	//EachImager:1視野内での各sensorの値 センサー数(72)個
	//EachShot:全視野での各sensorの値 視野数*センサー数(72)個
	//EachView:各視野の値 視野数

	std::string file_in_Beta_EachImagerParam = file_in_beta_path + "\\Beta_EachImagerParam.json";
	std::string file_in_Beta_EachViewParam = file_in_beta_path + "\\Beta_EachViewParam.json";

	std::map<int, EachImager_Param> imager_map = read_EachImager(file_in_Beta_EachImagerParam);
	std::map<int, EachView_Param> view_map = read_EachView(file_in_Beta_EachViewParam);


	printf("x:%.1lf y:%.1lf\n", x, y);
	micro_2_pixel(ViewID, ImagerID, x, y, imager_map, view_map);
	printf("x:%.1lf y:%.1lf\n", x, y);

}

std::map<int, EachImager_Param> read_EachImager(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	std::map<int, EachImager_Param> ret;
	//mapのkey=imagerID
	int ImagerID = 0;
	picojson::array &all = v.get<picojson::array>();
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachImager_Param param;
		picojson::array &Aff_coef = obj["Aff_coef"].get<picojson::array>();
		picojson::array &Aff_coef_offset = obj["Aff_coef_offset"].get<picojson::array>();
		int i = 0;
		for (int i = 0; i < 6; i++) {
			param.Aff_coef[i] = Aff_coef[i].get<double>();
			param.Aff_coef_offset[i] = Aff_coef_offset[i].get<double>();
		}
		param.DZ = obj["DZ"].get<double>();
		param.CameraID = (int)obj["CameraID"].get<double>();
		param.GridX = (int)obj["GridX"].get<double>();
		param.GridY = (int)obj["GridY"].get<double>();
		param.Height = (int)obj["Height"].get<double>();
		param.ImagerID = (int)obj["ImagerID"].get<double>();
		param.SensorID = (int)obj["SensorID"].get<double>();
		param.Width = (int)obj["Width"].get<double>();
		param.LastReportFilePath = obj["LastReportFilePath"].get<std::string>();
		ret.insert(std::make_pair(ImagerID, param));
		ImagerID++;

		//printf("imagerID %d factor %.10lf\n", param.ImagerID, param.Aff_coef[0] * param.Aff_coef[3] - param.Aff_coef[1] * param.Aff_coef[2]);
	}
	printf("number of imager = %d\n", ImagerID);
	return ret;
}
std::map<int, EachView_Param> read_EachView(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}
	std::map<int, EachView_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	int ViewID = 0;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachView_Param param;
		param.Stage_x = obj["Stage_x"].get<double>();
		param.Stage_y = obj["Stage_y"].get<double>();
		param.ThickOfLayer = obj["ThickOfLayer"].get<double>();
		param.Z_begin = obj["Z_begin"].get<double>();
		param.Z_end = obj["Z_end"].get<double>();
		param.LayerID = (int)obj["LayerID"].get<double>();
		param.NPicThickOfLayer = (int)obj["NPicThickOfLayer"].get<double>();
		ret.insert(std::make_pair(ViewID, param));
		ViewID++;
	}
	printf("number of view = %d\n", ViewID);
	return ret;
}

void micro_2_pixel(int view, int imager, double &x, double &y, std::map<int, EachImager_Param>&imager_map, std::map<int, EachView_Param> &view_map) {

	auto imager_param = imager_map.find(imager);
	auto view_param = view_map.find(view);

	double px, py;
	x = x / 1000;
	y = y / 1000;
	x = x - imager_param->second.Aff_coef_offset[4] - imager_param->second.Aff_coef[4] - view_param->second.Stage_x;
	y = y - imager_param->second.Aff_coef_offset[5] - imager_param->second.Aff_coef[5] - view_param->second.Stage_y;

	double factor;
	factor = imager_param->second.Aff_coef[0] * imager_param->second.Aff_coef[3] - imager_param->second.Aff_coef[1] * imager_param->second.Aff_coef[2];
	factor = 1. / factor;

	px = factor * (imager_param->second.Aff_coef[3] * x - imager_param->second.Aff_coef[1] * y);
	py = factor * (imager_param->second.Aff_coef[0] * y - imager_param->second.Aff_coef[2] * x);

	x = px;
	y = py;
}

std::vector<cv::Mat> read_spng(std::string filename) {
	//画像入力
	std::vector<cv::Mat> vmat1;

	std::vector<std::vector<uchar>> vvin;
	read_vbin(filename, vvin);
	for (int j = 0; j < vvin.size(); j++) {
		cv::Mat mat1 = cv::imdecode(vvin[j], 0);
		vmat1.emplace_back(mat1);
	}
	vvin.clear();
	return vmat1;
}
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin)
{
	std::ifstream ifs(filepath, std::ios::binary);
	if (!ifs.is_open()) { std::cout << "cannot open " << filepath << std::endl; throw std::exception(); }
	while (true)
	{
		uint64_t i64;
		ifs.read((char*)&i64, sizeof(uint64_t));
		if (ifs.eof()) { break; }

		std::vector<T> vin;
		if (i64 == -1)
		{
			vvin.push_back(vin);
			continue;
		}
		if (i64 > vin.max_size()) { throw std::exception(""); }
		vin.reserve(i64);
		for (uint64_t j = 0; j < i64; j++)
		{
			T p;
			ifs.read((char*)&p, sizeof(T));
			vin.push_back(p);
		}
		vvin.push_back(vin);
	}
	ifs.close();
}

