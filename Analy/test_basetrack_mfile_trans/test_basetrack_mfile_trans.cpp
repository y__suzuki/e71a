#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip> 
#include <set>
#include <sstream>
//basetrackに含まれるmicrotrack情報のクラス
class micro_track_subset_t {
public:
	double ax, ay;
	double z;
	int ph;
	int pos, col, row, zone, isg;
	int64_t rawid;
};
//basetrackのクラス
class base_track_t {
public:
	double ax, ay;
	double x, y, z;
	int pl;
	int isg, zone;
	int dmy;    // In ROOT, you will have to add this member because CINT does not handle 8byte alignment. 
	int64_t rawid;
	micro_track_subset_t m[2];
};
//corrmapのクラス
class Corrmap {
public:
	int id, pos[2];
	double areax[2], areay[2], position[6], angle[6], dz, signal, background, SN, rms_pos[2], rms_angle[2];
	double notuse_d[5];
	int notuse_i[9];
};
std::vector<std::string> StringSplit(std::string str);
bool read_basetrack_txt(std::string filename, std::vector<base_track_t> &base, int output=1);
std::vector<Corrmap> read_corrmap(std::string filename, int output = 1);
void basetrack_trans(std::vector<base_track_t> &base, std::vector<Corrmap>corr);
void write_basetrack_mfile_format(std::string filename, std::vector<base_track_t> &base);

int main(int argc, char** argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg input-basetrack-txt corrmap-abs ouput-file\n");
		exit(1);
	}
	std::string file_in_basetrack = argv[1];
	std::string file_in_corr = argv[2];
	std::string file_out_txt = argv[3];

	std::vector<base_track_t> base;
	read_basetrack_txt(file_in_basetrack, base);
	std::vector<Corrmap>corr = read_corrmap(file_in_corr);

	basetrack_trans(base, corr);

	write_basetrack_mfile_format(file_out_txt, base);
}
std::vector<std::string> StringSplit(std::string str) {
	std::stringstream ss{ str };
	std::vector<std::string> v;
	std::string buf;
	while (std::getline(ss, buf, ' ')) {
		if (buf != "") {
			v.push_back(buf);
		}
	}
	return v;
}
bool read_basetrack_txt(std::string filename, std::vector<base_track_t> &base, int output) {

	std::ifstream ifs(filename);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (output == 1) {
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
	}

	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		base_track_t basetrack;

		basetrack.rawid = stoi(str_v[0]);
		basetrack.pl = stoi(str_v[1]);
		basetrack.isg = stoi(str_v[2]);
		basetrack.ax = stod(str_v[3]);
		basetrack.ay = stod(str_v[4]);
		basetrack.x = stod(str_v[5]);
		basetrack.y = stod(str_v[6]);

		basetrack.m[0].ph = stoi(str_v[7]);
		basetrack.m[0].ax = stod(str_v[8]);
		basetrack.m[0].ay = stod(str_v[9]);
		basetrack.m[0].z = stod(str_v[12]);

		basetrack.m[0].pos = stoi(str_v[13]);
		basetrack.m[0].col = stoi(str_v[14]);
		basetrack.m[0].row = stoi(str_v[15]);
		basetrack.m[0].zone = stoi(str_v[16]);
		basetrack.m[0].isg = stoi(str_v[17]);
		basetrack.m[0].rawid = stoi(str_v[18]);

		basetrack.m[1].ph = stoi(str_v[19]);
		basetrack.m[1].ax = stod(str_v[20]);
		basetrack.m[1].ay = stod(str_v[21]);
		basetrack.m[1].z = stod(str_v[24]);

		basetrack.m[1].pos = stoi(str_v[25]);
		basetrack.m[1].col = stoi(str_v[26]);
		basetrack.m[1].row = stoi(str_v[27]);
		basetrack.m[1].zone = stoi(str_v[28]);
		basetrack.m[1].isg = stoi(str_v[29]);
		basetrack.m[1].rawid = stoi(str_v[30]);

		basetrack.dmy = 0;
		basetrack.zone = basetrack.m[0].zone;

		base.push_back(basetrack);
		if (cnt % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			if (output == 1) {
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
		}
		cnt++;

	}
	auto size1 = eofpos - begpos;
	if (output == 1) {
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;
	}
	if (cnt == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return true;

}
std::vector<Corrmap> read_corrmap(std::string filename, int output) {
	std::vector<Corrmap> ret;
	//ファイルをテキストモードでopen
	//バイナリに比べて速度は遅い
	std::ifstream ifs(filename);
	Corrmap buffer;

	while (ifs >> buffer.id >> buffer.pos[0] >> buffer.pos[1] >> buffer.areax[0] >> buffer.areax[1] >> buffer.areay[0] >> buffer.areay[1] >>
		buffer.position[0] >> buffer.position[1] >> buffer.position[2] >> buffer.position[3] >> buffer.position[4] >> buffer.position[5] >>
		buffer.angle[0] >> buffer.angle[1] >> buffer.angle[2] >> buffer.angle[3] >> buffer.angle[4] >> buffer.angle[5] >>
		buffer.dz >> buffer.signal >> buffer.background >> buffer.SN >> buffer.rms_pos[0] >> buffer.rms_pos[1] >>
		buffer.notuse_d[0] >> buffer.notuse_d[1] >> buffer.rms_angle[0] >> buffer.rms_angle[1] >>
		buffer.notuse_i[0] >> buffer.notuse_i[1] >> buffer.notuse_i[2] >> buffer.notuse_i[3] >> buffer.notuse_i[4] >> buffer.notuse_i[5] >>
		buffer.notuse_i[6] >> buffer.notuse_i[7] >> buffer.notuse_i[8] >>
		buffer.notuse_d[2] >> buffer.notuse_d[3] >> buffer.notuse_d[4]) {
		Corrmap *cormap = new Corrmap();
		cormap = &buffer;
		ret.push_back(*cormap);
	}
	if (output == 1) {
		fprintf(stderr, "%s input finish\n", filename.c_str());
	}
	if (ret.size() == 0) {
		fprintf(stderr, "%s alignment miss!\n", filename.c_str());
		exit(1);
	}
	return ret;
}
void basetrack_trans(std::vector<base_track_t> &base, std::vector<Corrmap>corr) {

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		int pl = itr->pl;
		bool flg = false;
		Corrmap param;
		for (int i = 0; i < corr.size(); i++) {
			if(corr[i].pos[0] / 10 == pl){
				flg = true;
				param = corr[i];
				break;
			}
		}
		if (!flg) {
			fprintf(stderr, "corrmap not found PL%03d\n", pl);
			exit(1);
		}
		double tmp_x, tmp_y;
		tmp_x = itr->x;
		tmp_y = itr->y;
		itr->x = tmp_x * param.position[0] + tmp_y * param.position[1] + param.position[4];
		itr->y = tmp_x * param.position[2] + tmp_y * param.position[3] + param.position[5];
		tmp_x = itr->ax;
		tmp_y = itr->ay;
		itr->ax = tmp_x * param.angle[0] + tmp_y * param.angle[1] + param.angle[4];
		itr->ay = tmp_x * param.angle[2] + tmp_y * param.angle[3] + param.angle[5];
	}


}
void write_basetrack_mfile_format(std::string filename, std::vector<base_track_t> &base) {
	std::ofstream ofs(filename);
	int flg_i = 0;
	double flg_d = 0;
	for (int i = 0; i < base.size(); i++) {
		ofs << std::right << std::fixed
			<< std::setw(6) << base[i].pl * 10 + 1 << " "
			<< std::setw(10) << i << " "
			<< std::setw(9) << base[i].rawid << " "
			<< std::setw(9) << base[i].m[0].ph + base[i].m[1].ph << " "
			<< std::setw(8) << std::setprecision(4) << base[i].ax << " "
			<< std::setw(8) << std::setprecision(4) << base[i].ay << " "
			<< std::setw(10) << std::setprecision(1) << base[i].x << " "
			<< std::setw(10) << std::setprecision(1) << base[i].y << " "
			<< std::setw(10) << std::setprecision(0) << base[i].z << " "
			<< std::setw(2) << flg_i << " "
			<< std::setw(2) << flg_i << " "
			<< std::setw(2) << flg_i << " "
			<< std::setw(2) << flg_i << " "
			<< std::setw(5) << std::setprecision(4) << flg_d << " "
			<< std::setw(5) << std::setprecision(4) << flg_d << " "
			<< std::endl;
	}
}