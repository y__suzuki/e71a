#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Mom_chain> merge_momch(std::vector<Momentum_recon::Mom_chain>&momch0, std::vector<Momentum_recon::Mom_chain>&momch1);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-momchain file-in-momchain file-out-momchain\n");
		exit(1);
	}
	std::string file_in_momch0 = argv[1];
	std::string file_in_momch1 = argv[2];
	std::string file_out_momch = argv[3];

	std::vector<Momentum_recon::Mom_chain> momch0, momch1;
	momch0 = Momentum_recon::Read_mom_chain_extension(file_in_momch0);
	momch1 = Momentum_recon::Read_mom_chain_extension(file_in_momch1);

	std::vector<Momentum_recon::Mom_chain> m_momch = merge_momch(momch0, momch1);

	Momentum_recon::Write_mom_chain_extension(file_out_momch, m_momch);

}

std::vector<Momentum_recon::Mom_chain> merge_momch(std::vector<Momentum_recon::Mom_chain>&momch0, std::vector<Momentum_recon::Mom_chain>&momch1) {
	std::map<std::pair<int, int>, Momentum_recon::Mom_chain> m_momch;
	for (auto itr = momch0.begin(); itr != momch0.end(); itr++) {
		m_momch.insert(std::make_pair(std::make_pair(itr->groupid, itr->chainid), *itr));
	}

	for (auto itr = momch1.begin(); itr != momch1.end(); itr++) {
		auto res= m_momch.insert(std::make_pair(std::make_pair(itr->groupid, itr->chainid), *itr));
		if (!res.second) {
			fprintf(stderr, "same chain input groupid=%d chainid=%d\n", itr->groupid, itr->chainid);
		}
	}
	std::vector<Momentum_recon::Mom_chain> ret;
	for (auto itr = m_momch.begin(); itr != m_momch.end(); itr++) {
		ret.push_back(itr->second);
	}
	return ret;
	


}