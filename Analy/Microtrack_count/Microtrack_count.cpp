#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int64_t count_track(std::string filename, int pos);

int main(int argc,char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg file-in-fvxx pos area tracking_mode file-out\n");
		fprintf(stderr, "tracking_mode = 0,1 :thick\n");
		fprintf(stderr, "tracking_mode = 2,3 :thin\n");
		exit(1);
	}

	std::string file_in_fvxx = argv[1];
	int pos = std::stoi(argv[2]);
	int area = std::stoi(argv[3]);
	int tracking_mode = std::stoi(argv[4]);
	//0-3:thick
	//4-7:thin
	std::string file_out_count = argv[5];

	int64_t micro_num = count_track(file_in_fvxx, pos);


	std::ofstream ofs(file_out_count, std::ios::app);

	ofs << std::right << std::fixed
		<< std::setw(5) << std::setprecision(0) << pos << " "
		<< std::setw(2) << std::setprecision(0) << area << " "
		<< std::setw(2) << std::setprecision(0) << tracking_mode << " "
		<< std::setw(15) << std::setprecision(0) << micro_num << std::endl;




}
int64_t count_track(std::string filename,int pos) {
	//fvxx xx byte/track
	//1Mtrack=xxGB
	int64_t count = 0;
	int read_track_num = (50 * 1000 * 1000);
	int id = 0;
	int t_num = -1;
	std::vector<vxx::micro_track_t> m;
	m.reserve(read_track_num);
	vxx::FvxxReader fr;

	while (t_num != 0) {
		std::array<int, 2> index = { id*read_track_num, (id + 1)*read_track_num };//1234<=rawid<=5678であるようなものだけを読む。
		m = fr.ReadAll(filename, pos, 0, vxx::opt::index = index);
		t_num = m.size();
		count += t_num;
		id++;
	}

	return count;
}