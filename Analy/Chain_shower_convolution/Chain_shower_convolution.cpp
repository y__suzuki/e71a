//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <stack>
#include <unordered_set>
#include <omp.h>

using namespace l2c;

class Chain_baselist {
public:
	int64_t groupid, trackid;
	std::pair<int32_t, int64_t> target_track;
	std::set<std::pair<int32_t, int64_t>> btset;
	std::set<std::tuple<int, int, int, int>>ltlist;
	std::vector<int32_t> usepos;
	int chain_num;
	std::vector<Linklet> make_link_list();
	void set_usepos();
};
void Chain_baselist::set_usepos() {
	std::set<int> pos_set;
	for (auto itr = btset.begin(); itr != btset.end(); itr++) {
		pos_set.insert(itr->first);
	}
	usepos.clear();
	for (auto itr = pos_set.begin(); itr != pos_set.end(); itr++) {
		usepos.push_back(*itr);
	}
}
std::vector<Linklet> Chain_baselist::make_link_list() {
	std::vector<Linklet> ret;
	ret.reserve(ltlist.size());
	for (auto itr = ltlist.begin(); itr != ltlist.end(); itr++) {
		ret.emplace_back(std::get<0>(*itr), std::get<1>(*itr), std::get<2>(*itr), std::get<3>(*itr));
	}
	return ret;
}

std::vector < Chain_baselist > read_linklet_list2(std::string filename);
void output_group(std::string filename, std::vector<Chain_baselist >&g);
std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_filename(std::string file_in_ECC);
std::map<std::pair<int, int>, std::map<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>>>
Set_basetrack_pair(std::vector<Chain_baselist>&group, std::map<int, std::vector < vxx::base_track_t* >> &base_ptr);
int use_thread(double ratio, bool output);
std::map<int, std::map<int, vxx::base_track_t >> Read_basetrack_inf(std::string file_in_ECC, std::map<int, std::vector < vxx::base_track_t* >> base_ptr);
Chain_baselist select_good_chains(Chain_baselist&g, std::map<std::pair<int, int>, std::map<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>>>& base_local);
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t >>select_good_path(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t >> path_all);
void apply_corrmap_local(std::map<int, std::map<int, vxx::base_track_t >>&base_map, std::map<int, std::vector<corrmap_3d::align_param2>>&corr);
l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output);
std::vector<mfile0::M_Chain> make_chain(Chain_baselist &g, std::map<int, std::map<int, vxx::base_track_t >>  &input_base);
std::map<double, std::pair<double, double>> angle_sigma_distribution();

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file_in_link_list file-in-ECC file-out\n");
		exit(1);
	}
	std::string file_in_link_list = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_out = argv[3];

	std::vector<Chain_baselist> chain_list = read_linklet_list2(file_in_link_list);
	printf("group size = %d\n", chain_list.size());
	//for (int i = 0; i < chain_list.size(); i++) {
	//	std::stringstream file_out;
	//	file_out << std::setw(5) << std::setfill('0') << chain_list[i].groupid << "_" << std::setw(5) << std::setfill('0') << chain_list[i].trackid << ".txt";
	//	std::vector<Chain_baselist> chain_out;
	//	chain_out.push_back(chain_list[i]);
	//	output_group(file_out.str(), chain_out);
	//}

	std::map<int, std::vector < vxx::base_track_t* >> base_ptr;
	std::map<std::pair<int, int>, std::map<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>>> base_local = Set_basetrack_pair(chain_list, base_ptr);
	//basetrackの情報読みこみ
	std::map<int, std::map<int, vxx::base_track_t >>  input_base = Read_basetrack_inf(file_in_ECC, base_ptr);
	{
		//mfile系に変換
		std::string file_in_corrmap = file_in_ECC + "\\Area0\\0\\align\\fine\\local\\corrmap-local-abs.lst";

		std::map<int, std::vector<corrmap_3d::align_param>>corrmap = corrmap_3d::read_ali_param_abs(file_in_corrmap, 1);
		std::map<int, std::vector<corrmap_3d::align_param2>>corrmap_dd = corrmap_3d::DelaunayDivide_map(corrmap);
		apply_corrmap_local(input_base, corrmap_dd);
	}

	//alignment paramerter read
	//これはポインタで参照されるためとっておく
	std::vector < std::pair<std::pair<int, int>, std::vector <corrmap_3d::align_param >>>all_align_param;
	//std::vector<std::pair< std::pair<int, int>, std::vector <corrmap_3d::align_param2 >>>all_align_param2;
	std::map< std::pair<int, int>, std::vector <corrmap_3d::align_param2 >>all_align_param2;
	std::vector<std::pair<std::pair<int, int>, std::string> >  files_in_align = Get_alignment_filename(file_in_ECC);

	//通常alinment pl0-->pl1 読みこみ
	int count = 0, all = files_in_align.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < files_in_align.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align read %d/%d", count, all);
		}
#pragma omp atomic
		count++;

		if (base_local.count(files_in_align[i].first) == 0)continue;

		std::vector <corrmap_3d::align_param > corr = corrmap_3d::read_ali_param(files_in_align[i].second, false);
#pragma omp critical
		all_align_param.push_back(std::make_pair(files_in_align[i].first, corr));
	}
	printf("\r corrmap align read %d/%d fin\n", count, all);


	//Delaunay3角形への分割
	count = 0, all = all_align_param.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < all_align_param.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align calc delaunay %d/%d", count, all);
		}
#pragma omp atomic
		count++;

		std::vector <corrmap_3d::align_param2 >corr2 = DelaunayDivide(all_align_param[i].second);
#pragma omp critical
		all_align_param2.insert(std::make_pair(all_align_param[i].first, corr2));

	}
	printf("\r corrmap align calc delaunay %d/%d fin\n", count, all);


	//linkletの系変換
	for (auto itr = base_local.begin(); itr != base_local.end(); itr++) {
		if (all_align_param2.count(itr->first) == 0) {
			fprintf(stderr, "alignment PL%03d PL%03d not found\n", itr->first.first, itr->first.second);
			exit(1);
		}
		std::vector <corrmap_3d::align_param2 > ali_target = all_align_param2.at(itr->first);

		std::vector< vxx::base_track_t*>trans_base;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			trans_base.push_back(&(itr2->second.second));
		}

		//trackとdelaunay3角形の対応
		std::vector < std::pair<vxx::base_track_t*, corrmap_3d::align_param2*>> track_param = track_affineparam_correspondence(trans_base, ali_target);
		//basetrackを変換
		corrmap_3d::trans_base_all(track_param);
	}

	//groupに対して処理
	std::vector<Chain_baselist> good_chain_list;
	//printf("group conv start");
	for (auto &g : chain_list) {
		Chain_baselist good_group= select_good_chains(g, base_local);
		good_chain_list.push_back(good_group);
		std::vector<mfile0::M_Chain>chains = make_chain(good_group, input_base);

		//mfileテスト出力
		mfile0::Mfile m_out;
		m_out.chains = chains;
		mfile0::set_header(*good_group.usepos.begin() / 10, *good_group.usepos.rbegin() / 10, m_out);
		std::stringstream file_out_mfile;
		file_out_mfile << "m_" << std::setw(5) << std::setfill('0') << g.groupid << "_"
			<< std::setw(5) << std::setfill('0') << g.trackid << ".all";
		mfile0::write_mfile(file_out_mfile.str(), m_out);
		//std::vector<mfile0::M_Chain> 
		//mfileの系でreconstruction
		//周辺+-xxumのみ許す

	}
	output_group(file_out, good_chain_list);
}

std::vector < Chain_baselist > read_linklet_list2(std::string filename) {
	std::vector < Chain_baselist > ret;
	std::ifstream ifs(filename);
	int gid, tid, muon_num, t_pl, t_rawid;
	int64_t link_num;
	int pl, raw;
	double weight;
	std::tuple<int, int, int, int> link;
	int count = 0;

	while (ifs >> gid >> tid >> t_pl >> t_rawid >> link_num) {
		printf("\r read group %d gid=%d link=%lld", count, gid, link_num);
		count++;
		Chain_baselist c;
		c.groupid = gid;
		c.trackid = tid;
		c.target_track.first = t_pl;
		c.target_track.second = t_rawid;
		for (int64_t i = 0; i < link_num; i++) {
			ifs >> std::get<0>(link) >> std::get<1>(link) >> std::get<2>(link) >> std::get<3>(link);
			c.btset.insert(std::make_pair(std::get<0>(link), std::get<2>(link)));
			c.btset.insert(std::make_pair(std::get<1>(link), std::get<3>(link)));
			c.ltlist.insert(link);
		}
		ret.push_back(c);
	}
	printf("\r read group %d gid=%d link=%lld\n", count, gid, link_num);

	return ret;

}

void output_group(std::string filename, std::vector<Chain_baselist >&g) {
	std::ofstream ofs(filename,std::ios::app);
	int count = 0;
	for (auto itr = g.begin(); itr != g.end(); itr++) {
		if (count % 1000 == 0) {
			fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)", count, g.size(), count*100. / g.size());
		}
		count++;

		ofs << std::fixed << std::right
			<< std::setw(10) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(5) << std::setprecision(0) << itr->trackid << " "
			<< std::setw(4) << std::setprecision(0) << itr->target_track.first << " "
			<< std::setw(4) << std::setprecision(0) << itr->target_track.second<< " "
			<< std::setw(10) << std::setprecision(0) << itr->ltlist.size() << std::endl;
		if (itr->ltlist.size() == 0)continue;
		for (auto itr2 = itr->ltlist.begin(); itr2 != itr->ltlist.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(5) << std::setprecision(0) << std::get<0>(*itr2) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(*itr2) << std::endl;
		}
	}
	fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)\n", count, g.size(), count*100. / g.size());

}

std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_filename(std::string file_in_ECC) {
	std::vector<std::pair<std::pair<int, int>, std::string> > ret;
	std::map<std::pair<int, int>, std::string> ret_map;

	std::string file_in_align_path = file_in_ECC + "\\Area0\\0\\align\\fine";
	std::filesystem::directory_iterator iter(file_in_align_path), end;
	std::error_code err;

	std::vector<std::string > file_names;
	for (; iter != end && !err; iter.increment(err)) {
		const std::filesystem::directory_entry entry = *iter;

		file_names.push_back(entry.path().string());
		//printf("%s\n", file_names.back().c_str());
	}
	for (int i = 0; i < file_names.size(); i++) {
		int length = file_names[i].size();
		if (file_names[i].substr(length - 4, 4) != ".txt")continue;
		if (file_names[i].substr(length - 18, 14) != "_interpolation")continue;

		int pl0 = std::stoi(file_names[i].substr(length - 25, 3));
		int pl1 = std::stoi(file_names[i].substr(length - 21, 3));

		ret_map.insert(std::make_pair(std::make_pair(pl0, pl1), file_names[i]));
	}
	for (auto itr = ret_map.begin(); itr != ret_map.end(); itr++) {
		ret.push_back(*itr);
	}
	return ret;
}

std::map<std::pair<int, int>, std::map<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>>> 
Set_basetrack_pair(std::vector<Chain_baselist>&group, std::map<int, std::vector < vxx::base_track_t* >> &base_ptr) {
	std::map<std::pair<int, int>, std::map<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>>>ret;
	int pl0, pl1, raw0, raw1;
	std::pair<int, int> pl_pair;
	std::pair<int, int>raw_pair;
	std::pair<vxx::base_track_t, vxx::base_track_t> base_pair;
	std::set<int> pl_set;
	for (auto&g : group) {
		for (auto &l : g.ltlist) {
			pl0 = std::get<0>(l) / 10;
			pl1 = std::get<1>(l) / 10;
			raw0 = std::get<2>(l);
			raw1 = std::get<3>(l);
			//printf("%3d %3d %10d %10d\n", pl0, pl1, raw0, raw1);
			pl_pair.first = pl0;
			pl_pair.second = pl1;
			raw_pair.first = raw0;
			raw_pair.second = raw1;
			base_pair.first.pl = pl0;
			base_pair.second.pl = pl1;
			base_pair.first.rawid = raw0;
			base_pair.second.rawid = raw1;

			pl_set.insert(pl0);
			pl_set.insert(pl1);
			auto res = ret.find(pl_pair);
			if (res == ret.end()) {
				std::map<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>> id_map;
				id_map.insert(std::make_pair(raw_pair, base_pair));
				ret.insert(std::make_pair(pl_pair, id_map));
			}
			else {
				res->second.insert(std::make_pair(raw_pair, base_pair));
			}
		}
	}

	for (auto &pl:pl_set) {
		std::vector < vxx::base_track_t* > base_ptr_v;
		base_ptr.insert(std::make_pair(pl, base_ptr_v));
	}
	//system("pause");

	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		//printf("PL%03d - PL%03d %d\n", itr->first.first, itr->first.second, itr->second.size());
		auto base_pl0 = base_ptr.find(itr->first.first);
		auto base_pl1 = base_ptr.find(itr->first.second);
		if (base_pl0 == base_ptr.end()) {
			fprintf(stderr, "base ptr nmot foun\n");
			exit(1);
		}
		if (base_pl1 == base_ptr.end()) {
			fprintf(stderr, "base ptr nmot foun\n");
			exit(1);
		}
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			//printf("\t%10d %10d\n", itr2->first.first, itr2->first.second);
			base_pl0->second.push_back(&(itr2->second.first));
			base_pl1->second.push_back(&(itr2->second.second));
		}
	}


	//for (auto &base : base_ptr) {
	//	printf("PL%03d %d\n", base.first, base.second.size());
	//	for (auto &bp : base.second) {
	//		std::cout <<"\t"<< bp << std::endl;
	//	}
	//}


	return ret;
}

Chain_baselist select_good_chains(Chain_baselist&g, std::map<std::pair<int, int>, std::map<std::pair<int, int>, std::pair<vxx::base_track_t, vxx::base_track_t>>>& base_local) {
	Chain_baselist good_group;
	good_group.groupid = g.groupid;
	good_group.target_track = g.target_track;
	good_group.trackid = g.trackid;

	std::set<std::pair<std::pair<int, int>, std::pair<int, int>>> good_path;
	std::pair<int, int> target_base;

	target_base.first = g.target_track.first * 10;
	target_base.second = g.target_track.second;
	std::multimap<std::pair<int, int>, std::pair<int, int>> path_next;
	std::multimap<std::pair<int, int>, std::pair<int, int>> path_prev;
	std::pair<int, int> seg0, seg1;
	for (auto itr = g.ltlist.begin(); itr != g.ltlist.end(); itr++) {
		seg0.first = std::get<0>(*itr);
		seg1.first = std::get<1>(*itr);
		seg0.second = std::get<2>(*itr);
		seg1.second = std::get<3>(*itr);
		path_next.insert(std::make_pair(seg0, seg1));
		path_prev.insert(std::make_pair(seg1, seg0));
	}
	std::set<std::pair<int, int>> finished;
	std::set<std::pair<int, int>> now;
	std::set<std::pair<int, int>> add;
	add.insert(target_base);

	while (add.size() != 0) {
		now = add;
		add.clear();

		for (auto &add_b : now) {
			std::vector<std::pair<vxx::base_track_t, vxx::base_track_t >> path_all;
			if (path_next.count(add_b) == 0)continue;
			auto range = path_next.equal_range(add_b);
			for (auto res = range.first; res != range.second; res++) {
				auto link_map = base_local.find(std::make_pair(res->first.first / 10, res->second.first / 10));
				auto t_link = link_map->second.find(std::make_pair(res->first.second, res->second.second));
				path_all.push_back(t_link->second);
			}
			std::vector<std::pair<vxx::base_track_t, vxx::base_track_t >>path_sel = select_good_path(path_all);
			for (auto &path : path_sel) {
				target_base.first = path.second.pl * 10;
				target_base.second = path.second.rawid;
				if (finished.count(target_base) == 1)continue;
				good_path.insert(std::make_pair(std::make_pair(path.first.pl * 10, path.first.rawid), std::make_pair(path.second.pl * 10, path.second.rawid)));
				add.insert(target_base);
			}
			finished.insert(add_b);
		}
	}

	target_base.first = g.target_track.first * 10;
	target_base.second = g.target_track.second;
	add.insert(target_base);
	while (add.size() != 0) {
		now = add;
		add.clear();

		for (auto add_b : now) {
			std::vector<std::pair<vxx::base_track_t, vxx::base_track_t >> path_all;
			if (path_prev.count(add_b) == 0)continue;
			auto range = path_prev.equal_range(add_b);
			for (auto res = range.first; res != range.second; res++) {
				auto link_map = base_local.find(std::make_pair(res->second.first / 10, res->first.first / 10));
				auto t_link = link_map->second.find(std::make_pair(res->second.second, res->first.second));
				path_all.push_back(t_link->second);
			}
			std::vector<std::pair<vxx::base_track_t, vxx::base_track_t >>path_sel = select_good_path(path_all);
			for (auto &path : path_sel) {
				target_base.first = path.first.pl * 10;
				target_base.second = path.first.rawid;
				if (finished.count(target_base) == 1)continue;
				good_path.insert(std::make_pair(std::make_pair(path.first.pl * 10, path.first.rawid), std::make_pair(path.second.pl * 10, path.second.rawid)));
				add.insert(target_base);
			}
			finished.insert(add_b);
		}
	}

	for (auto itr = good_path.begin(); itr != good_path.end(); itr++) {
		good_group.btset.insert(itr->first);
		good_group.btset.insert(itr->second);
		good_group.ltlist.insert(std::make_tuple(itr->first.first, itr->second.first, itr->first.second, itr->second.second));

	}
	//for (auto itr = good_group.btset.begin(); itr != good_group.btset.end(); itr++) {
	//	printf("PL%03d %d\n", itr->first / 10, itr->second);
	//}

	return good_group;


}
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t >>select_good_path(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t >> path_all) {
	std::map<double, std::pair<double, double>> angle_sigma = angle_sigma_distribution();

	double val = DBL_MAX;
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t >>ret;
	std::pair<vxx::base_track_t, vxx::base_track_t > best;
	for (auto itr = path_all.begin(); itr != path_all.end(); itr++) {
		double val_tmp = 0;
		double ax = (itr->first.x - itr->second.x) / (itr->first.z - itr->second.z);
		double ay = (itr->first.y - itr->second.y) / (itr->first.z - itr->second.z);
		double angle = sqrt(ax*ax + ay * ay);
		double s_lat, s_rad;
		auto angle_sigma_0=angle_sigma.upper_bound(angle);
		if (angle_sigma_0 == angle_sigma.begin()) {
			s_lat = angle_sigma_0->second.first;
			s_rad = angle_sigma_0->second.second;
		}
		else if (angle_sigma_0 == angle_sigma.end()) {
			s_lat = angle_sigma.rbegin()->second.first;
			s_rad = angle_sigma.rbegin()->second.second;
		}
		else {
			auto val0 = std::next(angle_sigma_0, -1);
			auto val1 = angle_sigma_0;
			s_lat= (val1->second.first - val0->second.first) / (val1->first - val0->first)*(angle - val0->first) + val0->second.first;
			s_rad = (val1->second.second - val0->second.second) / (val1->first - val0->first)*(angle - val0->first) + val0->second.second;
		}
		//printf("ax %7.4lf ay %7.4lf\n", ax, ay);
		//printf("dax %7.4lf day %7.4lf\n", ax - itr->first.ax, ay - itr->first.ay);
		//printf("dax %7.4lf day %7.4lf\n", ax - itr->second.ax, ay - itr->second.ay);
		double dal[2], dar[2];
		if(angle <0.01){
			dal[0] = itr->first.ax - ax;
			dal[1] = itr->second.ax - ax;
			dar[0] = itr->first.ay - ay;
			dar[1] = itr->second.ay - ay;
		}
		else {
			dal[0] = ((itr->first.ax - ax)*ay - (itr->first.ay - ay)*ax) / angle;
			dal[1] = ((itr->second.ax - ax)*ay - (itr->second.ay - ay)*ax) / angle;
			dar[0] = ((itr->first.ax - ax)*ax+ (itr->first.ay - ay)*ay) / angle;
			dar[1] = ((itr->second.ax - ax)*ax+(itr->second.ay - ay)*ay) / angle;
		}
		val_tmp = pow(dal[0]/ s_lat, 2) + pow(dal[1] / s_lat, 2) + pow(dar[0] / s_rad, 2) + pow(dar[1] / s_rad, 2);
		//printf("val %.7lf\n", val_tmp);
		if (val_tmp < val) {
			val = val_tmp;
			best= *itr;
		}
	}
	//printf("best val %.7lf\n", val);

	for (auto itr = path_all.begin(); itr != path_all.end(); itr++) {
		double val_tmp = 0;
		double ax = (itr->first.x - itr->second.x) / (itr->first.z - itr->second.z);
		double ay = (itr->first.y - itr->second.y) / (itr->first.z - itr->second.z);
		double angle = sqrt(ax*ax + ay * ay);
		double s_lat, s_rad;
		auto angle_sigma_0 = angle_sigma.upper_bound(angle);
		if (angle_sigma_0 == angle_sigma.begin()) {
			s_lat = angle_sigma_0->second.first;
			s_rad = angle_sigma_0->second.second;
		}
		else if (angle_sigma_0 == angle_sigma.end()) {
			s_lat = angle_sigma.rbegin()->second.first;
			s_rad = angle_sigma.rbegin()->second.second;
		}
		else {
			auto val0 = std::next(angle_sigma_0, -1);
			auto val1 = angle_sigma_0;
			s_lat = (val1->second.first - val0->second.first) / (val1->first - val0->first)*(angle - val0->first) + val0->second.first;
			s_rad = (val1->second.second - val0->second.second) / (val1->first - val0->first)*(angle - val0->first) + val0->second.second;
		}
		//printf("ax %7.4lf ay %7.4lf\n", ax, ay);
		//printf("dax %7.4lf day %7.4lf\n", ax - itr->first.ax, ay - itr->first.ay);
		//printf("dax %7.4lf day %7.4lf\n", ax - itr->second.ax, ay - itr->second.ay);
		double dal[2], dar[2];
		if (angle < 0.01) {
			dal[0] = itr->first.ax - ax;
			dal[1] = itr->second.ax - ax;
			dar[0] = itr->first.ay - ay;
			dar[1] = itr->second.ay - ay;
		}
		else {
			dal[0] = ((itr->first.ax - ax)*ay - (itr->first.ay - ay)*ax) / angle;
			dal[1] = ((itr->second.ax - ax)*ay - (itr->second.ay - ay)*ax) / angle;
			dar[0] = ((itr->first.ax - ax)*ax + (itr->first.ay - ay)*ay) / angle;
			dar[1] = ((itr->second.ax - ax)*ax + (itr->second.ay - ay)*ay) / angle;
		}
		val_tmp = pow(dal[0] / s_lat, 2) + pow(dal[1] / s_lat, 2) + pow(dar[0] / s_rad, 2) + pow(dar[1] / s_rad, 2);
		if (val_tmp <= std::max(val+0.000001,10.)) {
			ret.push_back(*itr);
		}
	}
	return ret;
};
l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output) {
	try
	{
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		if (output) {
			l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
			return cdat;
		}
		l2c::Cdat cdat = l2c::MakeCdat_no_out(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
		return cdat;

	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}
std::vector<mfile0::M_Chain> make_chain(Chain_baselist &g, std::map<int, std::map<int, vxx::base_track_t >>  &input_base) {

	std::vector<Linklet> ltlist = g.make_link_list();
	g.set_usepos();

	l2c::Cdat cdat = l2c_x(g.btset, ltlist, g.usepos, DEFAULT_CHAIN_UPPERLIM, false);

	std::vector<mfile0::M_Chain> ret;
	size_t grsize = cdat.GetNumOfGroups();
	size_t possize = g.usepos.size();
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		size_t chsize = gr.GetNumOfChains();
		if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
		if (gr.IsOverUpperLim())
		{
			fprintf(stdout, "over upperlim.nchain:%-9lld\n", chsize);
			fprintf(stdout, "event:%d track%d\n", g.groupid, g.trackid);
			exit(1);
			//continue;
		}
		for (size_t ich = 0; ich < chsize; ++ich)
		{
			mfile0::M_Chain c;
			c.chain_id = ich;
			Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
			int64_t chid = ch.GetID();
			int32_t nseg = ch.GetNSeg();
			int32_t spl = ch.GetStartPL();
			int32_t epl = ch.GetEndPL();

			for (size_t pl = 0; pl < possize; ++pl)
			{
				//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
				//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
				BaseTrackID bt = ch.GetBaseTrack(pl);
				if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
				int32_t btpl = bt.GetPL();
				int64_t btid = bt.GetRawID();
				//btplとplは厳密に一致しなければおかしい。
				if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
				auto base_pl = input_base.find(g.usepos[bt.GetPL()] / 10);
				if (base_pl == input_base.end()) {
					fprintf(stderr, "PL%03d basetrack not found\n", g.usepos[bt.GetPL()] / 10);
				}
				auto base_id = base_pl->second.find(btid);
				if (base_id == base_pl->second.end()) {
					fprintf(stderr, "PL%03d rawid=%d basetrack not found\n", g.usepos[bt.GetPL()] / 10, btid);
				}
				mfile0::M_Base b;
				b.pos = g.usepos[bt.GetPL()] + 1;
				b.rawid = btid;
				b.group_id = g.groupid;
				b.ax = base_id->second.ax;
				b.ay = base_id->second.ay;
				b.x = base_id->second.x;
				b.y = base_id->second.y;
				b.z = base_id->second.z;
				b.ph = base_id->second.m[0].ph+ base_id->second.m[1].ph;

				b.flg_i[0] = 0;
				b.flg_i[1] = g.trackid;
				b.flg_i[2] = 0;
				b.flg_i[3] = 0;
				b.flg_d[0] = 0;
				b.flg_d[1] = 0;
				c.basetracks.push_back(b);
			}
			c.pos0 = c.basetracks.begin()->pos;
			c.pos1 = c.basetracks.rbegin()->pos;
			c.nseg = c.basetracks.size();
			ret.push_back(c);
		}
	}
	return ret;


}


void apply_corrmap_local(std::map<int, std::map<int, vxx::base_track_t >>&base_map, std::map<int, std::vector<corrmap_3d::align_param2>>&corr) {

	//ここを書く
	std::set<int> all_pl_set;
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		all_pl_set.insert(itr->first);
	}
	std::vector<int> all_pl;
	for (auto itr = all_pl_set.begin(); itr != all_pl_set.end(); itr++) {
		all_pl.push_back(*itr);
	}

	int all = all_pl.size(), count = 0;
#pragma omp parallel for num_threads(use_thread(0.4,true)) schedule(dynamic,1)
	for (int i = 0; i < all_pl.size(); i++) {
		//for (auto itr = base_map_single.begin(); itr != base_map_single.end(); itr++) {
			//count = base_map_single.count(itr->first);
		int pl = all_pl[i];
#pragma omp critical
		{
			printf("PL%03d basetrack tans %3d/%3d\n", pl, count, all);
			count++;
			if (corr.count(pl) == 0) {
				fprintf(stderr, "PL%03d corrmap not found\n", pl);
				exit(1);
			}
			if (base_map.count(pl) == 0) {
				fprintf(stderr, "PL%03d basetrack not found\n", pl);
				exit(1);
			}
		}

		std::vector<corrmap_3d::align_param2> param = corr.at(pl);
		auto basetracks = base_map.find(pl);
		std::vector<vxx::base_track_t*> base_trans;
		base_trans.reserve(basetracks->second.size());
		for (auto itr2 = basetracks->second.begin(); itr2!=basetracks->second.end();itr2++) {
			base_trans.push_back(&(itr2->second));
		}
		std::vector <std::pair<vxx::base_track_t*, corrmap_3d::align_param2*>> base_trans_map = corrmap_3d::track_affineparam_correspondence(base_trans, param);
		trans_base_all(base_trans_map);
	}

	//for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
	//	printf("PL%03d\n", itr->first);
	//	for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
	//		printf("\t %10d %10d %6.4lf %6.4lf %8.1lf %8.1lf %8.1lf\n",itr2->first,itr2->second.rawid, itr2->second.ax, itr2->second.ay, itr2->second.x, itr2->second.y, itr2->second.z);
	//	}
	//}


}

std::map<int, std::map<int, vxx::base_track_t >> Read_basetrack_inf(std::string file_in_ECC,std::map<int, std::vector < vxx::base_track_t* >> base_ptr) {
	std::map<int,std::map<int, vxx::base_track_t >> ret;

	for (auto itr = base_ptr.begin(); itr != base_ptr.end(); itr++) {
		std::stringstream file_in_base;
		file_in_base << file_in_ECC << "\\Area0\\PL"
			<< std::setw(3) << std::setfill('0') << itr->first << "\\b"
			<< std::setw(3) << std::setfill('0') << itr->first << ".sel.cor.vxx";

		std::multimap<int, vxx::base_track_t*>base_map;

		int raw_min = INT32_MAX, raw_max = 0;
		for (auto &b : itr->second) {
			base_map.insert(std::make_pair(b->rawid, b));
			raw_min = std::min(raw_min, int(b->rawid));
			raw_max = std::max(raw_max, int(b->rawid));
		}

		std::vector<vxx::base_track_t >base;
		vxx::BvxxReader br;
		std::array<int, 2> index = { raw_min,raw_max + 1 };//1234<=rawid<=5678であるようなものだけを読む。
		printf("PL%03d\n", itr->first);
		printf("rawid %10d - %10d\n", raw_min, raw_max);
		base = br.ReadAll(file_in_base.str(), itr->first, 0, vxx::opt::index = index);

		std::map<int, vxx::base_track_t > input_base;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			auto res = base_map.find(itr->rawid);
			if (res == base_map.end())continue;
			itr->z = 0;
			input_base.insert(std::make_pair(itr->rawid, *itr));

			auto range = base_map.equal_range(itr->rawid);
			for (auto res = range.first; res != range.second; res++) {
				*res->second = *itr;
			}
		}
		ret.insert(std::make_pair(itr->first, input_base));
	}
	return ret;
}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}

std::map<double, std::pair<double, double>> angle_sigma_distribution() {
	std::map<double, std::pair<double, double>> ret;
	ret.insert(std::make_pair(0.05, std::make_pair(0.00234304 ,0.00240377)));
	ret.insert(std::make_pair(0.15, std::make_pair(0.00224587 ,0.00298669)));
	ret.insert(std::make_pair(0.25, std::make_pair(0.00231961 ,0.00360052)));
	ret.insert(std::make_pair(0.35, std::make_pair(0.00232506 ,0.00474736)));
	ret.insert(std::make_pair(0.45, std::make_pair(0.00235054 ,0.00596864)));
	ret.insert(std::make_pair(0.55, std::make_pair(0.00238381 ,0.00689529)));
	ret.insert(std::make_pair(0.65, std::make_pair(0.00239777 ,0.00802516)));
	ret.insert(std::make_pair(0.75, std::make_pair(0.00242283 ,0.00922937)));
	ret.insert(std::make_pair(0.85, std::make_pair(0.0025464 ,0.0105157)));
	ret.insert(std::make_pair(0.95, std::make_pair(0.00257759, 0.0117106)));
	ret.insert(std::make_pair(1.05, std::make_pair(0.00262354, 0.0132166)));
	ret.insert(std::make_pair(1.15, std::make_pair(0.00267648, 0.0144992)));
	ret.insert(std::make_pair(1.25, std::make_pair(0.00275726, 0.0158971)));
	ret.insert(std::make_pair(1.35, std::make_pair(0.00279427, 0.017297)));
	ret.insert(std::make_pair(1.55, std::make_pair(0.00290661, 0.0201512)));
	ret.insert(std::make_pair(1.85, std::make_pair(0.0031476 ,0.0243441)));
	ret.insert(std::make_pair(2.15, std::make_pair(0.00333298, 0.028457)));
	ret.insert(std::make_pair(2.45, std::make_pair(0.00360881, 0.0331959)));
	ret.insert(std::make_pair(2.75, std::make_pair(0.00382296, 0.0394615)));
	ret.insert(std::make_pair(3.05, std::make_pair(0.0041386 ,0.0454457)));
	ret.insert(std::make_pair(3.35, std::make_pair(0.00435668, 0.0512972)));
	ret.insert(std::make_pair(3.65, std::make_pair(0.00462299, 0.0565831)));
	ret.insert(std::make_pair(3.95, std::make_pair(0.00490809, 0.0629289)));
	ret.insert(std::make_pair(4.25, std::make_pair(0.00525439, 0.0702018)));
	ret.insert(std::make_pair(4.55, std::make_pair(0.0055612 ,0.0790199)));
	ret.insert(std::make_pair(4.85, std::make_pair(0.0058107 ,0.0872187)));
	return ret;
}

