#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
class Track_Area {
public:
	double xmin, xmax, ymin, ymax;
};
bool sort_base(const mfile0::M_Base &left, const mfile0::M_Base &right) {
	return left.pos > right.pos;
}
std::map<int, Track_Area >decide_track_area(std::vector<mfile0::M_Chain>&c);
void reject_penetrate(std::vector<mfile0::M_Chain>&c, std::map<int, Track_Area >&area);
void reject_edge_out(std::vector<mfile0::M_Chain>&c, std::map<int, Track_Area >&area);
void reject_low_eff(std::vector<mfile0::M_Chain>&c);
void reject_high_enrgy(std::vector<mfile0::M_Chain>&c);

void output_energy_deposit_range_pid(std::string filename, std::vector<mfile0::M_Chain>&c, double ang_min, double ang_max);

int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:prg file_in_mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];


	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::map<int, Track_Area >area = decide_track_area(m.chains);
	reject_penetrate(m.chains, area);
	reject_edge_out(m.chains, area);
	reject_low_eff(m.chains);
	reject_high_enrgy(m.chains);

	double angle_min, angle_max;
	for (int i = 0; i < 20; i++) {
		angle_min = i * 0.2;
		angle_max = (i + 1)*0.2;
		std::stringstream filename;
		filename << "vph_"
			<< std::setw(2) << std::setfill('0') << angle_min * 10 << "_"
			<< std::setw(2) << std::setfill('0') << angle_max * 10 << ".txt";
		output_energy_deposit_range_pid(filename.str(), m.chains, angle_min, angle_max);
	}

}

std::map<int, Track_Area >decide_track_area(std::vector<mfile0::M_Chain>&c) {
	std::map<int, Track_Area > ret;

	int pl;
	double x, y;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			pl = itr2->pos / 10;
			x = itr2->x;
			y = itr2->y;
			if (ret.count(pl) == 0) {
				Track_Area area;
				area.xmax = x;
				area.xmin = x;
				area.ymax = y;
				area.ymin = y;
				ret.insert(std::make_pair(pl, area));
			}
			else {
				auto res = ret.find(pl);
				res->second.xmin = std::min(res->second.xmin, x);
				res->second.xmax = std::max(res->second.xmax, x);
				res->second.ymin = std::min(res->second.ymin, y);
				res->second.ymax = std::max(res->second.ymax, y);
			}
		}
	}

	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		printf("%03d %8.1lf %8.1lf %8.1lf %8.1lf\n", itr->first, itr->second.xmin, itr->second.xmax, itr->second.ymin, itr->second.ymax);
	}
	return ret;
}
void reject_penetrate(std::vector<mfile0::M_Chain>&c, std::map<int, Track_Area >&area) {
	int pl_thr = 4;
	std::set<int> pl_up, pl_down;
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		if (pl_down.size() < pl_thr) {
			pl_down.insert(itr->first);
		}
	}
	for (auto itr = area.rbegin(); itr != area.rend(); itr++) {
		if (pl_up.size() < pl_thr) {
			pl_up.insert(itr->first);
		}
	}

	std::vector<mfile0::M_Chain> ret;
	bool flg;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		flg = false;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (pl_down.count(itr2->pos / 10) == 1)flg = true;
			if (pl_up.count(itr2->pos / 10) == 1)flg = true;
		}
		if (flg)continue;
		ret.push_back(*itr);
	}

	c.swap(ret);
}
void reject_edge_out(std::vector<mfile0::M_Chain>&c, std::map<int, Track_Area >&area) {
	double mergin = 20000;

	std::vector<mfile0::M_Chain> ret;
	Track_Area range;
	bool flg;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		flg = false;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			range = area.at(itr2->pos / 10);
			if (itr2->x < range.xmin + mergin)flg = true;
			if (itr2->x > range.xmax - mergin)flg = true;
			if (itr2->y < range.ymin + mergin)flg = true;
			if (itr2->y > range.ymax - mergin)flg = true;
		}
		if (flg)continue;
		ret.push_back(*itr);
	}

	c.swap(ret);
}
void reject_low_eff(std::vector<mfile0::M_Chain>&c) {
	std::vector<mfile0::M_Chain> ret;

	double eff;
	int npl = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		npl = itr->pos1 / 10 - itr->pos0 / 10 + 1;
		eff = itr->nseg*1. / npl;
		if (eff < 0.6)continue;
		ret.push_back(*itr);
	}
	c.swap(ret);

}

void reject_high_enrgy(std::vector<mfile0::M_Chain>&c) {

	std::vector<mfile0::M_Chain> ret;
	double ax, ay,dlat;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		ax = mfile0::chain_ax(*itr);
		ay = mfile0::chain_ay(*itr);
		dlat = mfile0::angle_diff_dev_lat(*itr, ax, ay);
		if (dlat > 0.01) {
			ret.push_back(*itr);
		}
	}
	c.swap(ret);



}

void decide_direction(std::vector<mfile0::M_Chain>&c) {

	int count = 0;
	double de_sum0 = 0;
	double de_sum1 = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		count =itr->basetracks.size()/ 2 - 1;
		if (count < 1)continue;
		de_sum0 = 0;
		for (int i = 0; i < count; i++) {
			de_sum0 += itr->basetracks[i].ph % 10000;
		}

		de_sum1 = 0;
		for (int i = 0; i < count; i++) {
			de_sum1 += itr->basetracks[itr->basetracks.size() - 1 - i].ph % 10000;
		}

		if (de_sum0 < de_sum1) {
			sort(itr->basetracks.begin(), itr->basetracks.end(), sort_base);
		}
	}
}



void output_energy_deposit_range_pid(std::string filename, std::vector<mfile0::M_Chain>&c, double ang_min, double ang_max) {

	//x-range
	//y-de/dx
	int x_bin = 133;
	int y_bin = 700;
	double x_min = 0.5, x_max = 133.5;
	double y_min = 0, y_max = 700;

	double x_step = (x_max - x_min) / x_bin;
	double y_step = (y_max - y_min) / y_bin;

	std::map<std::pair<int, int>, double> data;


	double x, y, z;
	double range, angle;
	int npl, pl0;
	std::pair<int, int> id;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		x = itr->basetracks.begin()->x;
		y = itr->basetracks.begin()->y;
		z = itr->basetracks.begin()->z;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			npl = abs(itr->basetracks.begin()->pos/10 - itr2->pos/10) + 1;
			range = sqrt(pow(x - itr2->x, 2) + pow(y - itr2->y, 2) + pow(z - itr2->z, 2));
			angle = sqrt(itr2->ax*itr2->ax + itr2->ay*itr2->ay);
			if (angle < ang_min)continue;
			if (ang_max <= angle)continue;
			//id.first = (range - x_min) / x_step;
			id.first = (npl - x_min) / x_step;
			id.second = (itr2->ph%10000 - y_min) / y_step;
			auto res = data.insert(std::make_pair(id, 1));
			if (!res.second) {
				res.first->second += 1;
			}
		}
	}

	std::ofstream ofs(filename);
	ofs << std::right << std::fixed
		<< std::setw(8) << std::setprecision(0) << x_bin << " "
		<< std::setw(8) << std::setprecision(1) << x_min << " "
		<< std::setw(8) << std::setprecision(1) << x_max << " "
		<< std::setw(8) << std::setprecision(0) << y_bin << " "
		<< std::setw(8) << std::setprecision(3) << y_min << " "
		<< std::setw(8) << std::setprecision(3) << y_max << std::endl;
	for (auto itr = data.begin(); itr != data.end(); itr++) {
		if (itr->first.first >= x_bin)continue;
		if (itr->first.second >= y_bin)continue;
		if (itr->first.first < 0)continue;
		if (itr->first.second < 0)continue;
		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(0) << itr->first.first << " "
			<< std::setw(8) << std::setprecision(0) << itr->first.second << " "
			<< std::setw(12) << std::setprecision(4) << itr->second << std::endl;
	}


}