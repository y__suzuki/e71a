//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <stack>
#include <unordered_set>

using namespace l2c;

class output_format {
public:
	int gid;
	int num_comfirmed_path, num_cut_path, num_select_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> comfirmed_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> cut_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> select_path;
};
std::vector< output_format> read_chain_file(std::string filename);
int64_t make_chain(output_format &group);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-chain groupid\n");
		exit(1);
	}

	std::string file_in_chain = argv[1];
	int groupid = std::stoi(argv[2]);

	std::vector< output_format> group = read_chain_file(file_in_chain);
	
	for (int i = 0; i < group.size(); i++) {
		if (group[i].gid != groupid)continue;
		int64_t nchain = make_chain(group[i]);
		printf("chain num = %lld\n", nchain);
		return 0;
	}
}
l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output) {
	try
	{
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		if (output) {
			l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
			return cdat;
		}
		l2c::Cdat cdat = l2c::MakeCdat_no_out(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
		return cdat;

	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}

std::vector< output_format> read_chain_file(std::string filename) {
	std::vector< output_format> ret;
	std::ifstream ifs(filename);
	output_format out;
	std::tuple<int, int, int, int> path;
	int id;
	int pl, rawid;
	while (ifs >> out.gid >> out.num_comfirmed_path >> out.num_cut_path >> out.num_select_path) {
		for (int i = 0; i < out.num_comfirmed_path; i++) {
			ifs >> id >> std::get<0>(path) >> std::get<1>(path) >> std::get<2>(path) >> std::get<3>(path);
			out.comfirmed_path.push_back(std::make_pair(id, path));

		}
		for (int i = 0; i < out.num_cut_path; i++) {
			ifs >> id >> std::get<0>(path) >> std::get<1>(path) >> std::get<2>(path) >> std::get<3>(path);
			out.cut_path.push_back(std::make_pair(id, path));

		}
		for (int i = 0; i < out.num_select_path; i++) {
			ifs >> id >> std::get<0>(path) >> std::get<1>(path) >> std::get<2>(path) >> std::get<3>(path);
			out.select_path.push_back(std::make_pair(id, path));

		}

		ret.push_back(out);
		out.comfirmed_path.clear();
		out.cut_path.clear();
		out.select_path.clear();
	}

	return ret;
}

int64_t make_chain(output_format &group){


	std::set<std::pair<int32_t, int64_t>> btset;
	std::vector<Linklet> ltlist;
	std::set<int32_t> pos;

	ltlist.reserve(group.comfirmed_path.size() + group.cut_path.size());
	for (auto itr = group.comfirmed_path.begin(); itr != group.comfirmed_path.end(); itr++) {
		ltlist.emplace_back(std::get<0>(itr->second), std::get<1>(itr->second), std::get<2>(itr->second), std::get<3>(itr->second));
		btset.insert(std::make_pair(std::get<0>(itr->second), std::get<2>(itr->second)));
		btset.insert(std::make_pair(std::get<1>(itr->second), std::get<3>(itr->second)));
		pos.insert(std::get<0>(itr->second));
		pos.insert(std::get<1>(itr->second));
	}
	for (auto itr = group.cut_path.begin(); itr != group.cut_path.end(); itr++) {
		ltlist.emplace_back(std::get<0>(itr->second), std::get<1>(itr->second), std::get<2>(itr->second), std::get<3>(itr->second));
		btset.insert(std::make_pair(std::get<0>(itr->second), std::get<2>(itr->second)));
		btset.insert(std::make_pair(std::get<1>(itr->second), std::get<3>(itr->second)));
		pos.insert(std::get<0>(itr->second));
		pos.insert(std::get<1>(itr->second));
	}

	std::vector<int32_t> usepos(pos.begin(), pos.end());
	auto cdat = l2c_x(btset, ltlist, usepos, DEFAULT_CHAIN_UPPERLIM, false);
	const Group& gr = cdat.GetGroup(0);
	size_t chsize = gr.GetNumOfChains();

	return  chsize;
}
