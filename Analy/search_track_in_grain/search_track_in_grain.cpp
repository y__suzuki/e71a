#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <omp.h>

class GrainInformation {
public:
	int id,tid, face, ix, iy, layer, pixelnum, brightness_sum9;
	double x, y, z,color[4];
};

class TrackInformation {
public:
	double ax, ay, x[2], y[2], z[2], color_HSV[3];
	int vph,id;
};

class SearchTrackInformation {
public:
	double ax, ay, x, y, z;
	int hit;
};

bool sort_track_hit(const SearchTrackInformation&left, const SearchTrackInformation&right) {
	return left.hit > right.hit;
}
std::vector<GrainInformation> read_grain_inf(std::string filename, int &face);
std::vector<TrackInformation> read_track_inf(std::string filename);

std::vector<GrainInformation*> cut_grain(std::vector<GrainInformation>&gr, TrackInformation &tr, double radius);
void search_track(std::vector<GrainInformation*>&gr, TrackInformation &tr, double radius, int face);
void output_grain(std::string filename, std::vector<GrainInformation>&grain);
int use_thread(double ratio, bool output);
void Calc_angle(std::vector<GrainInformation*> gr, double &x, double &y, double &z, double &ax, double &ay);


int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg file_in_grain-face1 file_in_grain-face2 file-in-track output-file output-result\n");
		exit(1);
	}
	std::string file_in_grain_face1 = argv[1];
	std::string file_in_grain_face2 = argv[2];
	std::string file_in_track = argv[3];
	std::string file_out_grain = argv[4];
	std::string file_out_result = argv[5];

	int face[2];
	std::vector<GrainInformation> grain[2];
	grain[0] = read_grain_inf(file_in_grain_face1, face[0]);
	grain[1] = read_grain_inf(file_in_grain_face2, face[1]);

	std::vector<TrackInformation>tr= read_track_inf(file_in_track);
	std::vector<GrainInformation*> grain_sel;
	for (auto itr = tr.begin(); itr != tr.end(); itr++) {
		if (itr->id == 0)continue;
		//if (itr->id != 1)continue;
		grain_sel = cut_grain(grain[0], *itr, 30);
		search_track(grain_sel, *itr, 20,1);
		grain_sel = cut_grain(grain[1], *itr, 30);
		search_track(grain_sel, *itr, 20,2);
	}

	std::vector < GrainInformation>out_grain;
	for (auto itr = grain[0].begin(); itr != grain[0].end(); itr++) {
		out_grain.push_back(*itr);
	}
	for (auto itr = grain[1].begin(); itr != grain[1].end(); itr++) {
		out_grain.push_back(*itr);
	}
	output_grain(file_out_grain, out_grain);
}
std::vector<GrainInformation> read_grain_inf(std::string filename, int &face) {
	std::ifstream ifs(filename);
	std::vector<GrainInformation> ret;
	GrainInformation gr;
	int count = 0;
	while (ifs >> gr.face >> gr.ix >> gr.iy >> gr.layer >> gr.x >> gr.y >> gr.z >> gr.pixelnum >> gr.brightness_sum9) {
		if (count % 100000 == 0) {
			printf("\r grain read %d", count);
		}
		face = gr.face;
		gr.color[0] = 0;
		gr.color[1] = 0;
		gr.color[2] = 1;
		gr.color[3] = 0.5;
		gr.tid = 0;
		gr.id = count;
		count++;
		ret.push_back(gr);
	}
	printf("\r grain read fin %d\n", ret.size());
	return ret;

}
std::vector<TrackInformation> read_track_inf(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<TrackInformation> ret;
	TrackInformation tr;
	while (ifs >> tr.x[0] >> tr.y[0] >> tr.z[0] >> tr.x[1] >> tr.y[1] >> tr.z[1] >> tr.vph
		>> tr.color_HSV[0] >> tr.color_HSV[1] >> tr.color_HSV[2]>>tr.id) {
		tr.ax = (tr.x[1] - tr.x[0]) / (tr.z[1] - tr.z[0]);
		tr.ay = (tr.y[1] - tr.y[0]) / (tr.z[1] - tr.z[0]);
		ret.push_back(tr);
	}
	return ret;
}

std::vector<GrainInformation*> cut_grain(std::vector<GrainInformation>&gr, TrackInformation &tr, double radius) {
	std::vector<GrainInformation*> ret;
	double tmp_x, tmp_y;
	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		tmp_x = tr.x[0] + tr.ax*(itr->z - tr.z[0]);
		tmp_y = tr.y[0] + tr.ay*(itr->z - tr.z[0]);
		if (fabs(itr->x - tmp_x) > radius)continue;
		if (fabs(itr->y - tmp_y) > radius)continue;
		if (itr->tid <1) {
			itr->color[0] = 0.5;
			itr->color[1] = 1.0;
			itr->color[3] = 0.5;
			itr->tid = 1;
		}
		//if (pow(itr->x - tmp_x, 2) + pow(itr->y - tmp_y, 2) > radius*radius)continue;
		ret.push_back(&(*itr));
	}
	//std::ofstream ofs("out_cut_grain.txt");
	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	ofs << std::right << std::fixed
	//		<< std::setw(4) << std::setprecision(0) << itr->face << " "
	//		<< std::setw(3) << std::setprecision(0) << itr->ix << " "
	//		<< std::setw(3) << std::setprecision(0) << itr->iy << " "
	//		<< std::setw(10) << std::setprecision(0) << itr->layer << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->x << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->z << " "
	//		<< std::setw(10) << std::setprecision(0) << itr->pixelnum << " "
	//		<< std::setw(10) << std::setprecision(0) << itr->brightness_sum9 << std::endl;
	//}
	//ofs.close();
	return ret;
}
void search_track(std::vector<GrainInformation*>&gr, TrackInformation &tr, double radius, int face) {
	;
	double thick = 70;
	double xmin, xmax, ymin, ymax;
	double z_border;
	double tmp_x, tmp_y;
	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		if (itr == gr.begin()) {
			z_border = (*itr)->z;
		}
		if (face == 1) {
			z_border = std::max((*itr)->z, z_border);
		}
		else if (face == 2) {
			z_border = std::min((*itr)->z, z_border);
		}

		tmp_x = tr.x[0] + tr.ax*((*itr)->z - tr.z[0]);
		tmp_y = tr.y[0] + tr.ay*((*itr)->z - tr.z[0]);
		if (fabs((*itr)->x - tmp_x) > radius)continue;
		if (fabs((*itr)->y - tmp_y) > radius)continue;
		//if ((*itr)->tid < 2) {
		//	(*itr)->color[0] = 0.6;
		//	(*itr)->color[1] = 1.0;
		//	(*itr)->color[3] = 0.8;
		//	(*itr)->tid = 2;
		//}

	}


	xmin = tr.x[0] + tr.ax*(z_border - tr.z[0]) - radius;
	xmax = tr.x[0] + tr.ax*(z_border - tr.z[0]) + radius;
	ymin = tr.y[0] + tr.ay*(z_border - tr.z[0]) - radius;
	ymax = tr.y[0] + tr.ay*(z_border - tr.z[0]) + radius;

	double search_radius = 0.5;
	double step = search_radius;

	int x_search_num = (xmax - xmin) / step;
	int y_search_num = (ymax - ymin) / step;

	double d_tan = radius / thick;
	double ax_min = tan(atan(tr.ax) - atan(d_tan / 2 / 5));
	double ax_max = tan(atan(tr.ax) + atan(d_tan / 2 / 5));
	double ay_min = tan(atan(tr.ay) - atan(d_tan / 2 / 5));
	double ay_max = tan(atan(tr.ay) + atan(d_tan / 2 / 5));


	//double ax_step = (ax_max - ax_min) / ax_search_num;
	//double ay_step = (ay_max - ay_min) / ay_search_num;
	double ax_step = search_radius / thick / 5;
	double ay_step = search_radius / thick / 5;


	ax_max = tr.ax + 0.05;
	ax_min = tr.ax - 0.05;
	ay_max = tr.ay + 0.05;
	ay_min = tr.ay - 0.05;
	ax_step = 0.001;
	ay_step = 0.001;
	int ax_search_num = (ax_max - ax_min) / ax_step;
	int ay_search_num = (ay_max - ay_min) / ay_step;

	printf("x  step=%g num=%d\n", step, x_search_num);
	printf("y  step=%g num=%d\n", step, y_search_num);
	printf("ax step=%g num=%d\n", ax_step, ax_search_num);
	printf("ay step=%g num=%d\n", ay_step, ay_search_num);

	int64_t count = 0, all = x_search_num * y_search_num* ax_search_num*ay_search_num, hit_count = 0;
	std::vector< SearchTrackInformation> ret;

	//std::ofstream ofs("track_result.txt");


	std::vector<std::vector<std::vector<std::vector<int>>>>hit_map_v(x_search_num,
		std::vector<std::vector<std::vector<int>>>(y_search_num,
			std::vector<std::vector<int>>(ax_search_num,
				std::vector<int>(ay_search_num, 0))));

	std::vector<std::vector<std::vector<std::vector<int>>>>hit_map_v_cluster(x_search_num,
		std::vector<std::vector<std::vector<int>>>(y_search_num,
			std::vector<std::vector<int>>(ax_search_num,
				std::vector<int>(ay_search_num, 0))));
	//std::vector<std::vector<std::vector<std::vector<int>>>>hit_map_v_cluster;
	//for (int ix = 0; ix < x_search_num; ix++) {
	//	std::vector < std::vector<std::vector<int>>>hit_map_tmp0;
	//	std::vector < std::vector<std::vector<int>>>hit_map_tmp00;
	//	for (int iy = 0; iy < y_search_num; iy++) {
	//		std::vector<std::vector<int>>hit_map_tmp1;
	//		std::vector<std::vector<int>>hit_map_tmp11;
	//		for (int iax = 0; iax < ax_search_num; iax++) {
	//			std::vector<int>hit_map_tmp2;
	//			std::vector<int>hit_map_tmp22;
	//			for (int iay = 0; iay < ay_search_num; iay++) {
	//				hit_map_tmp2.push_back(0);
	//				hit_map_tmp22.push_back(0);
	//			}
	//			hit_map_tmp1.push_back(hit_map_tmp2);
	//			hit_map_tmp11.push_back(hit_map_tmp22);
	//		}
	//		hit_map_tmp0.push_back(hit_map_tmp1);
	//		hit_map_tmp00.push_back(hit_map_tmp11);
	//	}
	//	hit_map_v.push_back(hit_map_tmp0);
	//	hit_map_v_cluster.push_back(hit_map_tmp00);
	//}

#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(guided)
	for (int ix = 0; ix < x_search_num; ix++) {
		for (int iy = 0; iy < y_search_num; iy++) {
			for (int iax = 0; iax < ax_search_num; iax++) {
				for (int iay = 0; iay < ay_search_num; iay++) {

					if (count % 100000 == 0) {
						printf("\r track search %lld / %lld(%4.1lf%%)", count, all, count*100. / all);
					}
#pragma omp atomic
					count++;
					double x, y, ax, ay;
					double tmp_x, tmp_y;
					int hit_num = 0;

					hit_num = 0;
					x = xmin + step * ix;
					y = ymin + step * iy;
					ax = ax_min + ax_step * iax;
					ay = ay_min + ay_step * iay;

					for (auto itr = gr.begin(); itr != gr.end(); itr++) {
						tmp_x = x + ax * ((*itr)->z - z_border);
						tmp_y = y + ay * ((*itr)->z - z_border);
						if (fabs((*itr)->x - tmp_x) > search_radius)continue;
						if (fabs((*itr)->y - tmp_y) > search_radius)continue;
						//if (pow((*itr)->x - tmp_x, 2) + pow((*itr)->y - tmp_y, 2) > search_radius*search_radius)continue;
						hit_num++;
					}
					hit_map_v[ix][iy][iax][iay] = hit_num;
					if (hit_num < 5)continue;
					//#pragma omp critical
					//					{
					//						ofs << std::right
					//							<< std::setw(3) << ix << " "
					//							<< std::setw(3) << iy << " "
					//							<< std::setw(3) << iax << " "
					//							<< std::setw(3) << iay << " "
					//							<< std::setw(5) << hit_num << std::endl;
					//					}

				}
			}
		}
	}
	printf("\r track search %lld / %lld(%4.1lf%%)\n", count, all, count*100. / all);

	count = 0;
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(guided)
	for (int ix = 0; ix < x_search_num; ix++) {
		for (int iy = 0; iy < y_search_num; iy++) {
			for (int iax = 0; iax < ax_search_num; iax++) {
				for (int iay = 0; iay < ay_search_num; iay++) {

					if (count % 100000 == 0) {
						printf("\r hit clustering %lld / %lld(%4.1lf%%)", count, all, count*100. / all);
					}
#pragma omp atomic
					count++;
					int hit_num = 0;
					for (int iix = 0; iix <= 0; iix++) {
						if (ix + iix < 0)continue;
						if (ix + iix >= x_search_num)continue;
						for (int iiy = 0; iiy <= 0; iiy++) {
							if (iy + iiy < 0)continue;
							if (iy + iiy >= y_search_num)continue;
							/*
							for (int iiax = -7; iiax <= 7; iiax++) {
								if (iax + iiax < 0)continue;
								if (iax + iiax >= ax_search_num)continue;
								hit_map_v_cluster[ix][iy][iax][iay] += hit_map_v[ix + iix][iy + iiy][iax + iiax][iay];
							}
							for (int iiay = -7; iiay <= 7; iiay++) {
								if (iay + iiay < 0)continue;
								if (iay + iiay >= ay_search_num)continue;

								hit_map_v_cluster[ix][iy][iax][iay] += hit_map_v[ix + iix][iy + iiy][iax][iay + iiay];
							}
							*/
							for (int iiax = -4; iiax <= 4; iiax++) {
								if (iax + iiax < 0)continue;
								if (iax + iiax >= ax_search_num)continue;
								for (int iiay = -4; iiay <= 4; iiay++) {
									if (iay + iiay < 0)continue;
									if (iay + iiay >= ay_search_num)continue;
									hit_map_v_cluster[ix][iy][iax][iay] += hit_map_v[ix + iix][iy + iiy][iax + iiax][iay + iiay];
								}
							}


						}
					}
				}
			}
		}
	}
	printf("\r hit clustering %lld / %lld(%4.1lf%%)\n", count, all, count*100. / all);

	std::tuple<int, int, int, int> peak;
	int hit_max = 0;
	for (int ix = 0; ix < x_search_num; ix++) {
		for (int iy = 0; iy < y_search_num; iy++) {
			for (int iax = 0; iax < ax_search_num; iax++) {
				for (int iay = 0; iay < ay_search_num; iay++) {
					if (hit_map_v_cluster[ix][iy][iax][iay] > hit_max) {
						std::get<0>(peak) = ix;
						std::get<1>(peak) = iy;
						std::get<2>(peak) = iax;
						std::get<3>(peak) = iay;
						hit_max = hit_map_v_cluster[ix][iy][iax][iay];
					}
				}
			}
		}
	}


	//std::ofstream ofs;
	//ofs.close();
	//ofs.open("track_result_clst.txt");
	//count=0;
	//for (int ix = 0; ix < x_search_num; ix++) {
	//	for (int iy = 0; iy < y_search_num; iy++) {
	//		for (int iax = 0; iax < ax_search_num; iax++) {
	//			for (int iay = 0; iay < ay_search_num; iay++) {
	//				if (count % 100000 == 0) {
	//					printf("\r write hit clustering %lld / %lld(%4.1lf%%)", count, all, count*100. / all);
	//				}
	//				count++;

	//				if (hit_map_v_cluster[ix][iy][iax][iay] < 2*2*15)continue;
	//				ofs << std::right
	//					<< std::setw(3) << ix << " "
	//					<< std::setw(3) << iy << " "
	//					<< std::setw(3) << iax << " "
	//					<< std::setw(3) << iay << " "
	//					<< std::setw(5) << hit_map_v_cluster[ix][iy][iax][iay] << std::endl;
	//			}
	//		}
	//	}
	//}
	//printf("\r write hit clustering %lld / %lld(%4.1lf%%)\n", count, all, count*100. / all);


	//printf("hit count %lld\n", hit_count);
	//if (hit_count < 1)return;
	//peakをとるx-yの抽出
	//sort(ret.begin(), ret.end(), sort_track_hit);
	printf("peak number of grain         = %lld\n", hit_map_v[std::get<0>(peak)][std::get<1>(peak)][std::get<2>(peak)][std::get<3>(peak)]);
	printf("peak number of grain clster  = %lld\n", hit_map_v_cluster[std::get<0>(peak)][std::get<1>(peak)][std::get<2>(peak)][std::get<3>(peak)]);
	//printf("peak number of grain average = %.2lf\n", hit_map_v_cluster[std::get<0>(peak)][std::get<1>(peak)][std::get<2>(peak)][std::get<3>(peak)] / (15.*2.));
	printf("peak number of grain average = %.2lf\n", hit_map_v_cluster[std::get<0>(peak)][std::get<1>(peak)][std::get<2>(peak)][std::get<3>(peak)] / (9.*9.));

	//peakの位置角度情報
	SearchTrackInformation s_tr;
	s_tr.x = xmin + step * std::get<0>(peak);
	s_tr.y = ymin + step * std::get<1>(peak);
	s_tr.ax = ax_min + ax_step * std::get<2>(peak);
	s_tr.ay = ay_min + ay_step * std::get<3>(peak);
	s_tr.z = z_border;
	s_tr.hit = hit_max;
	//peak周辺のgrain
	std::vector<GrainInformation*>hit_gr;
	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		tmp_x = s_tr.x + s_tr.ax * ((*itr)->z - s_tr.z);
		tmp_y = s_tr.y + s_tr.ay * ((*itr)->z - s_tr.z);
		if (pow((*itr)->x - tmp_x, 2) + pow((*itr)->y - tmp_y, 2) > search_radius*search_radius)continue;
		hit_gr.push_back(*itr);
	}
	//fit
	search_radius = 0.5;
	Calc_angle(hit_gr, s_tr.x, s_tr.y, s_tr.z, s_tr.ax, s_tr.ay);
	hit_gr.clear();
	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		tmp_x = s_tr.x + s_tr.ax * ((*itr)->z - s_tr.z);
		tmp_y = s_tr.y + s_tr.ay * ((*itr)->z - s_tr.z);
		if (pow((*itr)->x - tmp_x, 2) + pow((*itr)->y - tmp_y, 2) > search_radius*search_radius)continue;
		hit_gr.push_back(*itr);
	}

	for (auto itr = hit_gr.begin(); itr != hit_gr.end(); itr++) {
		(*itr)->color[0] = 1;
		(*itr)->color[1] = 1;
		(*itr)->color[3] = 1;
		(*itr)->tid = 3;
	}
	printf("track number of grain = %lld\n", hit_gr.size());

}
void output_grain(std::string filename, std::vector<GrainInformation>&grain) {
	std::ofstream ofs(filename);
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {

		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->face << " "
			<< std::setw(3) << std::setprecision(0) << itr->ix << " "
			<< std::setw(3) << std::setprecision(0) << itr->iy << " "
			<< std::setw(10) << std::setprecision(0) << itr->layer << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(10) << std::setprecision(1) << itr->z << " "
			<< std::setw(10) << std::setprecision(0) << itr->pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->brightness_sum9 << " "
			<< std::setw(3) << std::setprecision(2) << itr->color[0] << " "
			<< std::setw(3) << std::setprecision(2) << itr->color[1] << " "
			<< std::setw(3) << std::setprecision(2) << itr->color[2] << " "
			<< std::setw(3) << std::setprecision(2) << itr->color[3] << " "
			<< std::setw(3) << std::setprecision(0) << itr->tid << std::endl;
	}

}
int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}



void Calc_angle(std::vector<GrainInformation*> gr,double &tr_x,double &tr_y,double &tr_z,double &tr_ax,double &tr_ay) {

	double x, y, n, xy, x2;
	x = 0;
	x2 = 0;
	xy = 0;
	y = 0;
	n = 0;
	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		x += (*itr)->z;
		y += (*itr)->x;
		xy += (*itr)->z*(*itr)->x;
		x2 += (*itr)->z*(*itr)->z;
		n += 1;
	}
	if (n*x2 - x * x == 0) {
		tr_ax = 999999;
	}
	tr_ax = (n*xy - x * y) / (n*x2 - x * x);
	tr_x = (x2*y - xy * x) / (n*x2 - x * x);
	tr_x = tr_x + tr_ax*(tr_z - 0);

	x = 0;
	x2 = 0;
	xy = 0;
	y = 0;
	n = 0;
	for (auto itr = gr.begin(); itr !=gr.end(); itr++) {
		x += (*itr)->z;
		y += (*itr)->y;
		xy += (*itr)->z*(*itr)->y;
		x2 += (*itr)->z*(*itr)->z;
		n += 1;
	}
	if (n*x2 - x * x == 0) {
		tr_ay = 999999;
	}
	tr_ay = (n*xy - x * y) / (n*x2 - x * x);
	tr_y = (x2*y - xy * x) / (n*x2 - x * x);
	tr_y = tr_y + tr_ay*(tr_z - 0);

	return;
}
