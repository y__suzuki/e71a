#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <picojson.h>

class t2l_param {
public:
	std::string file_in_ECC_path, file_in_align_path;
	std::string file_out_linklet_path, file_out_others_path;
	int pl0, pl1;
	double intercept_ax, intercept_ay, intercept_px, intercept_py;
	double slope_px, slope_py, slope_ax, slope_ay;
	double slope2_px, slope2_py, slope2_ax, slope2_ay;
	double intercept_ar, intercept_al, intercept_pr, intercept_pl;
	double slope_pr, slope_pl, slope_ar, slope_al;
	double slope2_pr, slope2_pl, slope2_ar, slope2_al;
	double position_hash, angle_hash;
	double angle_max;
	void Print_all();
};

std::multimap<int, Momentum_recon::Mom_chain > divide_chain(std::vector<Momentum_recon::Mom_chain> &momch);
void Calc_position_difference(Momentum_recon::Mom_basetrack &t1, Momentum_recon::Mom_basetrack &t2, double &dr, double &dl);
bool judge_connect_xy(Momentum_recon::Mom_basetrack &t1, Momentum_recon::Mom_basetrack &t2, t2l_param &param);
bool judge_connect_rl(Momentum_recon::Mom_basetrack &t1, Momentum_recon::Mom_basetrack &t2, t2l_param &param);
bool track_connection(Momentum_recon::Mom_chain &down, Momentum_recon::Mom_chain&up, t2l_param &param_fe, t2l_param &param_water);
std::vector<Momentum_recon::Mom_chain> muon_connection(std::multimap<int, Momentum_recon::Mom_chain >&chain_map, t2l_param param_fe, t2l_param param_water);
t2l_param read_param_json(std::string filename);
bool judge_connect_md_oa(Momentum_recon::Mom_basetrack &t1, Momentum_recon::Mom_basetrack &t2, t2l_param &param);



int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:fileame\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_param_fe = argv[2];
	std::string file_param_water = argv[3];
	std::string file_out_momch = argv[4];

	t2l_param param_fe = read_param_json(file_param_fe);
	t2l_param param_water = read_param_json(file_param_water);
	//param_fe.Print_all();
	//param_water.Print_all();

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::multimap<int, Momentum_recon::Mom_chain > mom_map = divide_chain(momch);
	std::vector<Momentum_recon::Mom_chain>muon_connect = muon_connection(mom_map, param_fe, param_water);

	Momentum_recon::Write_mom_chain_extension(file_out_momch, muon_connect);

}
//json fileでのparameter読み込み
t2l_param read_param_json(std::string filename) {

	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	t2l_param ret;
	picojson::object &all = v.get<picojson::object>();
	ret.file_in_ECC_path = all["file_in_ECC_path"].get<std::string>();
	ret.file_in_align_path = all["file_in_align_path"].get<std::string>();
	ret.file_out_linklet_path = all["file_out_linklet_path"].get<std::string>();
	ret.file_out_others_path = all["file_out_others"].get<std::string>();
	picojson::object &connect_param_angle = all["connect_param_angle"].get<picojson::object>();

	picojson::object &connect_param_position = all["connect_param_position"].get<picojson::object>();

	ret.intercept_ax = connect_param_angle["intercept_x"].get<double>();
	ret.intercept_ay = connect_param_angle["intercept_y"].get<double>();
	ret.intercept_ar = connect_param_angle["intercept_r"].get<double>();
	ret.intercept_al = connect_param_angle["intercept_l"].get<double>();
	ret.slope_ax = connect_param_angle["slope_x"].get<double>();
	ret.slope_ay = connect_param_angle["slope_y"].get<double>();
	ret.slope_ar = connect_param_angle["slope_r"].get<double>();
	ret.slope_al = connect_param_angle["slope_l"].get<double>();
	ret.slope2_ax = connect_param_angle["slope2_x"].get<double>();
	ret.slope2_ay = connect_param_angle["slope2_y"].get<double>();
	ret.slope2_ar = connect_param_angle["slope2_r"].get<double>();
	ret.slope2_al = connect_param_angle["slope2_l"].get<double>();


	ret.intercept_px = connect_param_position["intercept_x"].get<double>();
	ret.intercept_py = connect_param_position["intercept_y"].get<double>();
	ret.intercept_pr = connect_param_position["intercept_r"].get<double>();
	ret.intercept_pl = connect_param_position["intercept_l"].get<double>();
	ret.slope_px = connect_param_position["slope_x"].get<double>();
	ret.slope_py = connect_param_position["slope_y"].get<double>();
	ret.slope_pr = connect_param_position["slope_r"].get<double>();
	ret.slope_pl = connect_param_position["slope_l"].get<double>();
	ret.slope2_px = connect_param_position["slope2_x"].get<double>();
	ret.slope2_py = connect_param_position["slope2_y"].get<double>();
	ret.slope2_pr = connect_param_position["slope2_r"].get<double>();
	ret.slope2_pl = connect_param_position["slope2_l"].get<double>();

	ret.position_hash = all["position_hash"].get<double>();
	ret.angle_hash = all["angle_hash"].get<double>();
	ret.angle_max = all["angle_max"].get<double>();

	return ret;
}
void t2l_param::Print_all() {

	printf("file_in_ECC_path : %s\n", file_in_ECC_path.c_str());
	printf("file_in_align_path : %s\n", file_in_align_path.c_str());
	printf("file_out_linklet_path : %s\n", file_out_linklet_path.c_str());
	printf("file_out_others_path : %s\n", file_out_others_path.c_str());


	printf("PL0 : %03d \n", pl0);
	printf("PL1 : %03d \n", pl1);

	printf("position hash : %g[um]\n", position_hash);
	printf("angle hash : %\g\n", angle_hash);

	printf("dax: %.5lf * tan^2\u03B8x + %.5lf * tan\u03B8x + %.5lf\n", slope2_ax, slope_ax, intercept_ax);
	printf("day: %.5lf * tan^2\u03B8y + %.5lf * tan\u03B8y + %.5lf\n", slope2_ay, slope_ay, intercept_ay);
	printf("dar: %.5lf * tan^2\u03B8  + %.5lf * tan\u03B8  + %.5lf\n", slope2_ar, slope_ar, intercept_ar);
	printf("dal: %.5lf * tan^2\u03B8  + %.5lf * tan\u03B8  + %.5lf\n", slope2_al, slope_al, intercept_al);

	printf("dpx: %.1lf * tan^2\u03B8x + %.1lf * tan\u03B8x + %.1lf\n", slope2_px, slope_px, intercept_px);
	printf("dpy: %.1lf * tan^2\u03B8y + %.1lf * tan\u03B8y + %.1lf\n", slope2_py, slope_py, intercept_py);
	printf("dpr: %.1lf * tan^2\u03B8  + %.1lf * tan\u03B8  + %.1lf\n", slope2_pr, slope_pr, intercept_pr);
	printf("dpl: %.1lf * tan^2\u03B8  + %.1lf * tan\u03B8  + %.1lf\n", slope2_pl, slope_pl, intercept_pl);

}

std::multimap<int, Momentum_recon::Mom_chain > divide_chain(std::vector<Momentum_recon::Mom_chain> &momch) {
	std::multimap<int, Momentum_recon::Mom_chain > ret;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		ret.insert(std::make_pair(itr->groupid, *itr));
	}
	return ret;
}
std::vector<Momentum_recon::Mom_chain> muon_connection(std::multimap<int, Momentum_recon::Mom_chain >&chain_map, t2l_param param_fe, t2l_param param_water) {
	int muon_num = 0;
	std::vector<Momentum_recon::Mom_chain>ret;
	for (auto itr = chain_map.begin(); itr != chain_map.end(); itr++) {
		muon_num++;

		Momentum_recon::Mom_chain muon;
		muon.groupid = -1;
		std::vector<Momentum_recon::Mom_chain>all;
		if (chain_map.count(itr->first) == 0)continue;
		auto range = chain_map.equal_range(itr->first);
		//printf("event %d track %d\n", range.first->second.groupid, chain_map.count(itr->first));
		for (auto res = range.first; res != range.second; res++) {
			//printf("\tPID %d\n", res->second.particle_flg);

			if (res->second.particle_flg == 13) {
				muon = itr->second;
			}
			else {
				all.push_back(res->second);
			}
		}
		//printf("ev%d muon:%d , other:%d\n",muon.groupid, muon.particle_flg, all.size());
		if (muon.groupid < 0) {
			itr = std::next(itr, chain_map.count(itr->first) - 1);
			continue;
		}
		Momentum_recon::Mom_chain connected = muon;
		for (auto itr2 = all.begin(); itr2 != all.end(); itr2++) {
			if (track_connection(muon, *itr2, param_fe, param_water)) {
				connected.particle_flg += itr2->particle_flg;
				for (auto &b : itr2->base) {
					std::pair<Momentum_recon::Mom_basetrack, Momentum_recon::Mom_basetrack> pair;
					pair.first = *connected.base.rbegin();
					pair.second = b;
					connected.base.push_back(b);
					connected.base_pair.push_back(pair);
				}
				ret.push_back(connected);

				break;
			}
		}

		itr = std::next(itr, chain_map.count(itr->first) - 1);
	}

	printf("muon num %d --> %d\n", muon_num, ret.size());
	return ret;
}

bool track_connection(Momentum_recon::Mom_chain &down, Momentum_recon::Mom_chain&up, t2l_param &param_fe, t2l_param &param_water) {

	Momentum_recon::Mom_basetrack b_down,b_up;
	b_down = *down.base.rbegin();
	b_up = *up.base.begin();
	if (b_down.pl >= b_up.pl)return false;

	if (b_down.pl <= 15||b_down.pl%2==0) {
		return judge_connect_xy(b_down, b_up, param_fe) && judge_connect_rl(b_down, b_up, param_fe);
	}
	else{
		//printf("b_down %7.4lf %7.4lf %9.1lf %9.1lf %9.1lf\n", b_down.ax, b_down.ay, b_down.x, b_down.y, b_down.z);
		//printf("b_up   %7.4lf %7.4lf %9.1lf %9.1lf %9.1lf\n", b_up.ax, b_up.ay, b_up.x, b_up.y, b_up.z);
		//printf("PL%03d PL%03d gap%.1lf\n", b_down.pl, b_up.pl, b_down.z - b_up.z);

		return (judge_connect_xy(b_down, b_up, param_water) && judge_connect_rl(b_down, b_up, param_water))|| judge_connect_md_oa(b_down, b_up, param_water);
	}
	return false;
}
bool judge_connect_xy(Momentum_recon::Mom_basetrack &t1, Momentum_recon::Mom_basetrack &t2, t2l_param &param) {
	double angle, d_pos_x, d_pos_y, d_ang_x, d_ang_y;
	double all_pos_x, all_pos_y, all_ang_x, all_ang_y;

	all_ang_x = param.intercept_ax + param.slope_ax*fabs(t1.ax) + param.slope2_ax*pow(t1.ax, 2);
	all_ang_y = param.intercept_ay + param.slope_ay*fabs(t1.ay) + param.slope2_ay*pow(t1.ay, 2);

	all_pos_x = param.intercept_px + param.slope_px*fabs(t1.ax) + param.slope2_px*pow(t1.ax, 2);
	all_pos_y = param.intercept_py + param.slope_py*fabs(t1.ay) + param.slope2_py*pow(t1.ay, 2);

	d_pos_x = t2.x - t1.x - (t1.ax + t2.ax) / 2 * (t2.z - t1.z);
	d_pos_y = t2.y - t1.y - (t1.ay + t2.ay) / 2 * (t2.z - t1.z);

	d_ang_x = (t2.ax - t1.ax);
	d_ang_y = (t2.ay - t1.ay);

	if (fabs(d_ang_x) > fabs(all_ang_x))return false;
	if (fabs(d_ang_y) > fabs(all_ang_y))return false;

	if (fabs(d_pos_x) > fabs(all_pos_x))return false;
	if (fabs(d_pos_y) > fabs(all_pos_y))return false;

	return true;
}
bool judge_connect_rl(Momentum_recon::Mom_basetrack &t1, Momentum_recon::Mom_basetrack &t2, t2l_param &param) {
	double angle, d_pos_r, d_pos_l, d_ang_r, d_ang_l;
	double all_pos_r, all_pos_l, all_ang_r, all_ang_l;
	angle = sqrt(t1.ax*t1.ax + t1.ay*t1.ay);
	if (angle < 0.01)return true;

	all_ang_r = param.intercept_ar + param.slope_ar*angle + param.slope2_ar*angle*angle;
	all_ang_l = param.intercept_al + param.slope_al*angle + param.slope2_al*angle*angle;
	all_pos_r = param.intercept_pr + param.slope_pr*angle + param.slope2_pr*angle*angle;
	all_pos_l = param.intercept_pl + param.slope_pl*angle + param.slope2_pl*angle*angle;


	d_ang_r = ((t2.ax - t1.ax)*t1.ax + (t2.ay - t1.ay)*t1.ay);
	d_ang_l = ((t2.ax - t1.ax)*t1.ay - (t2.ay - t1.ay)*t1.ax);
	if (fabs(d_ang_r) > fabs(all_ang_r)*angle)return false;
	if (fabs(d_ang_l) > fabs(all_ang_l)*angle)return false;

	Calc_position_difference(t1, t2, d_pos_r, d_pos_l);
	if (fabs(d_pos_r) > fabs(all_pos_r))return false;
	if (fabs(d_pos_l) > fabs(all_pos_l))return false;

	//printf("angle %.4lf\n", angle);
	//printf("angle    radial : %.4lf %.4lf\n", d_ang_r / angle, all_ang_r);
	//printf("angle    lateral: %.4lf %.4lf\n", d_ang_l / angle, all_ang_l);
	//printf("position radial : %.4lf %.4lf\n", d_pos_r, all_pos_r);
	//printf("position lateral: %.4lf %.4lf\n", d_pos_l, all_pos_l);
	//printf("\n");

	return true;
}
bool judge_connect_md_oa(Momentum_recon::Mom_basetrack &t1, Momentum_recon::Mom_basetrack &t2, t2l_param &param) {
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = t1.x;
	pos0.y = t1.y;
	pos0.z = t1.z;
	dir0.x = t1.ax;
	dir0.y = t1.ay;
	dir0.z = 1;
	pos1.x = t2.x;
	pos1.y = t2.y;
	pos1.z = t2.z;
	dir1.x = t2.ax;
	dir1.y = t2.ay;
	dir1.z = 1;

	double oa = opening_angle(dir0, dir1);
	double z_range[2], extra[2];
	z_range[0] = t1.z;
	z_range[1] = t2.z;

	double md = minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);

	if (oa < 0.2&&md < 200)return true;
	return false;
}
void Calc_position_difference(Momentum_recon::Mom_basetrack &t1, Momentum_recon::Mom_basetrack &t2, double &dr, double &dl) {
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = t1.x;
	pos0.y = t1.y;
	pos0.z = t1.z;
	dir0.x = t1.ax;
	dir0.y = t1.ay;
	dir0.z = 1;
	pos1.x = t2.x;
	pos1.y = t2.y;
	pos1.z = t2.z;
	dir1.x = t2.ax;
	dir1.y = t2.ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);

}
