#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void read_cormap_fast(std::string file, std::vector<corrmap0::Corrmap> &cor, int output=1);
std::multimap<std::pair<int, int>, corrmap0::Corrmap> corrmap_divide(std::vector<corrmap0::Corrmap> &corr_all, std::vector<corrmap0::Corrmap> &corr_abs, double pich_x, double pich_y, int count_min);

int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:prg file-in-align file-in-corrabs x-pich y-pich output-filename count-min\n");
		exit(1);
	}
	std::string file_in_align = argv[1];
	std::string file_in_align_abs = argv[2];
	double pich_x = std::stod(argv[3]);
	double pich_y = std::stod(argv[4]);
	std::string file_out = argv[5];
	int count_min = std::stoi(argv[6]);

	std::vector<corrmap0::Corrmap> corr_all, corr_abs;
	read_cormap_fast(file_in_align, corr_all);
	corrmap0::read_cormap(file_in_align_abs, corr_abs);
	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_divide = corrmap_divide(corr_all, corr_abs, pich_x, pich_y,count_min);

	for (auto itr = corr_divide.begin(); itr != corr_divide.end(); itr++) {
		int count = corr_divide.count(itr->first);

		std::stringstream file_out_str;
		file_out_str << file_out
			<< "_" << std::setw(5) << std::setfill('0') << itr->first.first
			<< "_" << std::setw(5) << std::setfill('0') << itr->first.second
			<< ".lst";
		std::vector<corrmap0::Corrmap> corr_out;
		std::pair<int, int> id = itr->first;
		if (corr_divide.count(id) == 0)continue;
		auto range = corr_divide.equal_range(id);
		for (auto res = range.first; res != range.second; res++) {
			corr_out.push_back(res->second);
		}
		corrmap0::write_corrmap(file_out_str.str(), corr_out);


		itr = std::next(itr, count - 1);
	}

}
void read_cormap_fast(std::string file, std::vector<corrmap0::Corrmap> &cor, int output) {
	std::ifstream ifs(file);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}

	corrmap0::Corrmap buffer;
	int cnt = 0;
	while (ifs >> buffer.id >> buffer.pos[0] >> buffer.pos[1] >> buffer.areax[0] >> buffer.areax[1] >> buffer.areay[0] >> buffer.areay[1] >>
		buffer.position[0] >> buffer.position[1] >> buffer.position[2] >> buffer.position[3] >> buffer.position[4] >> buffer.position[5] >>
		buffer.angle[0] >> buffer.angle[1] >> buffer.angle[2] >> buffer.angle[3] >> buffer.angle[4] >> buffer.angle[5] >>
		buffer.dz >> buffer.signal >> buffer.background >> buffer.SN >> buffer.rms_pos[0] >> buffer.rms_pos[1] >>
		buffer.notuse_d[0] >> buffer.notuse_d[1] >> buffer.rms_angle[0] >> buffer.rms_angle[1] >> buffer.notuse_i[0] >>
		//これがix,iy
		buffer.notuse_i[1] >> buffer.notuse_i[2] >>
		buffer.notuse_i[3] >> buffer.notuse_i[4] >> buffer.notuse_i[5] >> buffer.notuse_i[6] >> buffer.notuse_i[7] >> buffer.notuse_i[8] >>
		buffer.notuse_d[2] >> buffer.notuse_d[3] >> buffer.notuse_d[4]) {

		if (output == 1) {
			if (cnt % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
		}
		cnt++;
		if (abs(buffer.pos[0] - buffer.pos[1]) / 10 > 2)continue;
		if (buffer.signal > 100 && buffer.SN > 20) {
			cor.push_back(buffer);
		}
	}
	if (output == 1) {
		auto size1 = eofpos - begpos;
		std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%"<<std::endl;
		fprintf(stderr, "%s input finish\n", file.c_str());
		//fprintf(stderr, "read corr size= %d\n",cor.size());
	}
	if (cor.size() == 0) {
		fprintf(stderr, "%s alignment miss!\n", file.c_str());
		exit(1);
	}


}

std::multimap<std::pair<int, int>, corrmap0::Corrmap> corrmap_divide(std::vector<corrmap0::Corrmap> &corr_all, std::vector<corrmap0::Corrmap> &corr_abs,double pich_x,double pich_y,int count_min) {
	std::map<int, corrmap0::Corrmap> corr_abs_map;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		corr_abs_map.insert(std::make_pair(itr->pos[0] / 10, *itr));
	}
	std::multimap<int, corrmap0::Corrmap> corr_all_map;
	for (auto itr = corr_all.begin(); itr != corr_all.end(); itr++) {
		corr_all_map.insert(std::make_pair(itr->pos[0] / 10, *itr));
	}
	printf("corr all size = %lld\n", corr_all_map.size());
	printf("corr abs size = %lld\n", corr_abs_map.size());

	double x_min= DBL_MAX, y_min= DBL_MAX;
	int pl,count;
	for (auto itr = corr_all_map.begin(); itr != corr_all_map.end(); itr++) {
		pl = itr->first;
		count = corr_all_map.count(pl);
		if (corr_abs_map.count(pl) == 0) {
			itr = std::next(itr, count - 1);
			continue;
		}
		corrmap0::Corrmap param = corr_abs_map.at(pl);
		auto range = corr_all_map.equal_range(pl);

		double x_center, y_center,x_buf,y_buf;
		for (auto res = range.first; res != range.second; res++) {
			x_buf = (res->second.areax[0] + res->second.areax[1]) / 2;
			y_buf = (res->second.areay[0] + res->second.areay[1]) / 2;
			x_center = x_buf * param.position[0] + y_buf * param.position[1] + param.position[4];
			y_center = x_buf * param.position[2] + y_buf * param.position[3] + param.position[5];
			x_min = std::min(x_center, x_min);
			y_min = std::min(y_center, y_min);
		}

		itr = std::next(itr, count - 1);
	}
	//x_min -= pich_x * 0.51;
	//y_min -= pich_y * 0.51;
	printf("xmin %.1lf ymin %.1lf\n", x_min, y_min);


	std::multimap<std::pair<int, int>, corrmap0::Corrmap> corr_divide_area;
	std::pair<int, int> id;
	for (auto itr = corr_all_map.begin(); itr != corr_all_map.end(); itr++) {
		pl = itr->first;
		count = corr_all_map.count(pl);
		if (count < count_min) {
			itr = std::next(itr, count - 1);
			continue;
		}
		if (corr_abs_map.count(pl) == 0) {
			itr = std::next(itr, count - 1);
			continue;
		}

		corrmap0::Corrmap param = corr_abs_map.at(pl);
		auto range = corr_all_map.equal_range(pl);

		double x_center, y_center, x_buf, y_buf,rem_x,rem_y;
		int ix, iy;
		for (auto res = range.first; res != range.second; res++) {
			x_buf = (res->second.areax[0] + res->second.areax[1]) / 2;
			y_buf = (res->second.areay[0] + res->second.areay[1]) / 2;
			x_center = x_buf * param.position[0] + y_buf * param.position[1] + param.position[4];
			y_center = x_buf * param.position[2] + y_buf * param.position[3] + param.position[5];
			iy = (y_center - y_min) / pich_y;
			if (iy % 2 == 0) {
				ix = (x_center - x_min) / pich_x+0.25;
			}
			else {
				ix = (x_center - x_min) / pich_x-0.25;
				if (ix < 0)ix = 0;
			}
			id.first = ix;
			id.second = iy;
			corr_divide_area.insert(std::make_pair(id, res->second));
			
		}

		itr = std::next(itr, count - 1);
	}

	//for (auto itr = corr_divide_area.begin(); itr != corr_divide_area.end(); itr++) {
	//	int count = corr_divide_area.count(itr->first);

	//	printf("%5d %5d %d\n", itr->first.first, itr->first.second, count);

	//	itr = std::next(itr, count - 1);

	//}
	//
	return corr_divide_area;

}