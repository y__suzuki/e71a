//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>
using namespace l2c;
class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};

l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output);
void read_linklet_bin(std::string filename, std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist);
std::vector<int32_t> Get_usepos(std::set<std::pair<int32_t, int64_t>> &btset);
void output_header(std::string filename, std::vector<linklet_header>&link);
std::vector<linklet_header> Linklet_convolution(l2c::Cdat&cdat, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg input-linklet-header output-linklet-header\n");
		exit(1);
	}
	std::string file_in_link = argv[1];
	std::string file_out_link = argv[2];

	std::set<std::pair<int32_t, int64_t>> btset;
	std::vector<Linklet> ltlist;
	std::vector<int32_t>usepos;
	read_linklet_bin(file_in_link, btset, ltlist);
	printf("Linklet read fin\n");
	usepos = Get_usepos(btset);
	printf("usepos set fin\n");
	l2c::Cdat cdat = l2c_x(btset, ltlist, usepos, 1, true);
	printf("l2c fin\n");

	std::vector<linklet_header>out_link = Linklet_convolution(cdat, ltlist, usepos);
}
int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}

void read_linklet_bin(std::string filename, std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist) {
		int64_t link_num = Linklet_header_num(filename);
		ltlist.reserve(link_num);
		FILE*fp_in;
		if ((fp_in = fopen(filename.c_str(), "rb")) == NULL) {
			printf("%s file not open!\n", filename.c_str());
			exit(EXIT_FAILURE);
		}

		int count = 0;
		const int read_num_max = 10000;
		//std::vector<linklet_header> link;
		//link.reserve(read_num_max);
		linklet_header link[read_num_max];
		int64_t  now = 0;
		int read_num;
		bool flg = true;
		while (flg) {
			if (link_num - now == read_num_max) {
				read_num = read_num_max;
				flg = false;
			}
			else if (link_num - now < read_num_max) {
				read_num = link_num - now;
				flg = false;
			}
			else {
				read_num = read_num_max;
			}
			fread(&link, sizeof(linklet_header), read_num, fp_in);
			for (int i = 0; i < read_num; i++) {
				//printf("%d %d %d %d\n", link[i].pos0, link[i].raw0, link[i].pos1, link[i].raw1);
				ltlist.emplace_back(link[i].pos0 + 1, link[i].pos1 + 1, link[i].raw0, link[i].raw1);
				btset.insert(std::make_pair(link[i].pos0 + 1, link[i].raw0));
				btset.insert(std::make_pair(link[i].pos1 + 1, link[i].raw1));
			}
			if (count % 100 == 0) {
				printf("\r read fin %12lld/%12lld (%4.1lf%%)", now, link_num, now*100. / link_num);
			}
			now += read_num;
			count++;
		}
		printf("\r read fin %12lld/%12lld (%4.1lf%%)\n", now, link_num, now*100. / link_num);

	}
std::vector<int32_t> Get_usepos(std::set<std::pair<int32_t, int64_t>> &btset) {
		std::set<int> pos_set;
		for (auto itr = btset.begin(); itr != btset.end(); itr++) {
			pos_set.insert(itr->first);
		}
		std::vector<int32_t> usepos;
		for (auto itr = pos_set.begin(); itr != pos_set.end(); itr++) {
			usepos.push_back(*itr);
		}
		return usepos;

	}
l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output) {
	try
	{
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		if (output) {
			l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
			return cdat;
		}
		l2c::Cdat cdat = l2c::MakeCdat_no_out(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
		return cdat;

	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}

std::vector<linklet_header> Linklet_convolution(l2c::Cdat&cdat, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos) {
	size_t grsize = cdat.GetNumOfGroups();
	size_t possize = usepos.size();
	std::vector<linklet_header> ret;
	int64_t count = 0;
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
		int32_t spl = gr.GetStartPL();
		int32_t epl = gr.GetEndPL();
		auto link_list = gr.GetLinklets();
		for (auto itr = link_list.begin(); itr != link_list.end(); itr++) {
			linklet_header link;
			link.pos0 = usepos[itr->first.GetPL()];
			link.pos1 = usepos[itr->second.GetPL()];
			link.raw0 = itr->first.GetRawID();
			link.raw1 = itr->second.GetRawID();
			ret.push_back(link);
			count++;
			if (count % 10000 == 0) {
				printf("linklet information output %lld\r", count);
			}
		}
	}
	printf("linklet information output %lld\n", count);
	return ret;
}



void output_header(std::string filename, std::vector<linklet_header>&link) {
	FILE *fp_out;
	if ((fp_out = fopen(filename.c_str(), "ab")) == NULL) {
		printf("file open error!!\n");
		exit(EXIT_FAILURE);	/* (3)エラーの場合は通常、異常終了する */
	}
	int64_t  now = 0;
	printf("now file writing\r");
	fwrite(&link, sizeof(linklet_header), link.size(), fp_out);
	printf("write fin \n");
}
