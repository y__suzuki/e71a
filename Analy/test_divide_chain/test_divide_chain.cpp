#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void Calc_divide_vph(Momentum_recon::Mom_chain  &c, int pl, double mean[2], double mean_err[2], double sd[2], int count[2]);
void Calc_divide_pixel(Momentum_recon::Mom_chain  &c, int pl, double mean[2], double mean_err[2], double sd[2], int count[2]);
void output_file(std::string filename, std::vector<Momentum_recon::Mom_chain>&mom);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-momch file-out-momch\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);

	output_file(file_out_momch, momch);


}
void output_file(std::string filename, std::vector<Momentum_recon::Mom_chain>&mom) {
	std::ofstream ofs(filename);

	double dax, day, dar, dal,angle;
	int count[2];
	for (auto &c : mom) {
		for (auto itr = c.base_pair.begin(); itr != c.base_pair.end(); itr++) {
			angle = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);
			dax = itr->second.ax - itr->first.ax;
			day = itr->second.ay - itr->first.ay;
			if (angle < 0.01) {
				dal = dax;
				dar = day;
			}
			else {
				dal = (dax*itr->first.ay - day * itr->first.ax) / angle;
				dar = (dax*itr->first.ax + day * itr->first.ay) / angle;
			}
			ofs << std::fixed << std::right
				<< std::setw(10) << std::setprecision(0) << c.groupid << " "
				<< std::setw(10) << std::setprecision(0) << c.chainid << " "
				<< std::setw(4) << std::setprecision(0) << itr->first.pl << " "
				<< std::setw(4) << std::setprecision(0) << itr->second.pl << " "
				<< std::setw(10) << std::setprecision(4) << itr->first.ax << " "
				<< std::setw(10) << std::setprecision(4) << itr->first.ay << " "
				<< std::setw(10) << std::setprecision(4) << dax << " "
				<< std::setw(10) << std::setprecision(4) << day << " "
				<< std::setw(10) << std::setprecision(4) << dar << " "
				<< std::setw(10) << std::setprecision(4) << dal << std::endl;
		}


	}
}
