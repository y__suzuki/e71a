#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

bool lateral_cut(vxx::base_track_t&b, double allowance);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in_bvxx pos zone out_bvxx allowance\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_bvxx = argv[4];
	double allowance = std::stod(argv[5]);

	std::vector<vxx::base_track_t> base;
	//ReadAll関数はbvxx中のBaseTrackを一度にすべて読みだす。
	//引数にはbvxxへのパス、pl、zoneを渡す。
	//ファイルを開けなかった場合などは空のvectorが返ってくる。
	int64_t count = 0;
	vxx::BvxxReader br;
	if (br.Begin(file_in_bvxx, pl, zone))
	{
		vxx::HashEntry h;
			vxx::base_track_t b;
			while (br.NextHashEntry(h))
			{
				while (br.NextBaseTrack(b))
				{
					if (count % 100000 == 0) {
						fprintf(stderr, "\r process... %lld", count);
					}
					count++;
					if (lateral_cut(b, allowance)) {
						base.push_back(b);
					}
				}
			}
		br.End();
	}
	fprintf(stderr, "\r process... %lld fin\n", count);
	fprintf(stderr, "lateral cut %lld -->%lld(%4.1lf%)\n", count, base.size(), base.size()*100. / count);

	vxx::BvxxWriter w;
	w.Write(file_out_bvxx, pl, zone, base);

}
bool lateral_cut(vxx::base_track_t&b, double allowance) {
	if (((b.ax - b.m[0].ax)*b.ay - (b.ay - b.m[0].ay)*b.ax) / (sqrt(b.ax*b.ax + b.ay*b.ay)) > allowance)return 0;
	if (((b.ax - b.m[1].ax)*b.ay - (b.ay - b.m[1].ay)*b.ax) / (sqrt(b.ax*b.ax + b.ay*b.ay)) > allowance)return 0;
	return 1;
}