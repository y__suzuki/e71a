#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
std::vector<netscan::base_track_t> basetrack_matching(std::vector<netscan::base_track_t> base_pred, std::vector<netscan::base_track_t> base_all);

int main(int argc, char**argv) {
	if (argc!=5) {
		fprintf(stderr, "prg:in-bvxx all-bvxx out-bvxx pl\n");
		exit(1);
	}
	std::string file_in_bvxx_pred = argv[1];
	std::string file_in_bvxx_all = argv[2];
	std::string file_out_bvxx = argv[3];
	int pl = std::stoi(argv[4]);

	std::vector<netscan::base_track_t> base_pred;
	std::vector<netscan::base_track_t> base_all;

	netscan::read_basetrack_extension(file_in_bvxx_pred, base_pred, pl, 0);
	netscan::read_basetrack_extension(file_in_bvxx_all, base_all, pl, 0);

	std::vector<netscan::base_track_t> base_sel = basetrack_matching(base_pred, base_all);
	netscan::write_basetrack_vxx(file_out_bvxx, base_sel, pl, 0);
}
std::vector<netscan::base_track_t> basetrack_matching(std::vector<netscan::base_track_t> base_pred, std::vector<netscan::base_track_t> base_all) {
	std::vector<netscan::base_track_t> ret;

	int i = 0;
	for (i = 0; i < base_pred.size(); i++) {
		printf("\r base search %d/%d", i, base_pred.size());
		double angle = sqrt(base_pred[i].ax*base_pred[i].ax + base_pred[i].ay*base_pred[i].ay);
		double all_ang_r = 0.3*angle+0.3;
		double all_ang_l = 0.3;
		double all_pos_r = 100 * angle+100;
		double all_pos_l = 100;
		double diff_ang_r, diff_ang_l, diff_pos_r, diff_pos_l;

		for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
			if (fabs(itr->x - base_pred[i].x) > 10000)continue;
			if (fabs(itr->y - base_pred[i].y) > 10000)continue;
			diff_ang_r = ((itr->ax - base_pred[i].ax)*base_pred[i].ax + (itr->ay - base_pred[i].ay)*base_pred[i].ay) / angle;
			diff_ang_l = ((itr->ax - base_pred[i].ax)*base_pred[i].ay - (itr->ay - base_pred[i].ay)*base_pred[i].ax) / angle;
			diff_pos_r = ((itr->x - base_pred[i].x)*base_pred[i].ax + (itr->y - base_pred[i].y)*base_pred[i].ay) / angle;
			diff_pos_l = ((itr->x - base_pred[i].x)*base_pred[i].ay - (itr->y - base_pred[i].y)*base_pred[i].ax) / angle;
			if (fabs(diff_pos_l) > all_pos_l)continue;
			if (fabs(diff_pos_r) > all_pos_r)continue;
			if (fabs(diff_ang_l) > all_ang_l)continue;
			if (fabs(diff_ang_r) > all_ang_r)continue;
			ret.push_back(*itr);
		}


	}
	printf("\r base search %d/%d\n", i, base_pred.size());
	printf("matched base num=%d\n", ret.size());

	return ret;
}