#include <iostream>
#include <set>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <iomanip>
void read_list(std::string filename, int pl, std::set<std::pair<int, int>> &list);
void write_list(std::string filename, std::set<std::pair<int, int>> &list);
int main(int argc, char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg start-list pl stop-list pl out-txt\n");
		exit(1);
	}
	std::string file_in_start = argv[1];
	int start_pl = std::stoi(argv[2]);

	std::string file_in_stop = argv[3];
	int stop_pl = std::stoi(argv[4]);
	std::string file_out_txt = argv[5];

	std::set<std::pair<int, int>> list;
	read_list(file_in_start, start_pl, list);
	read_list(file_in_stop, stop_pl, list);
	write_list(file_out_txt, list);
}

void read_list(std::string filename,  int pl, std::set<std::pair<int, int>> &list) {
	std::ifstream ifs(filename);
	int _pl, _rawid;
	while (ifs >> _pl >> _rawid) {
		if (_pl != pl)continue;
		list.insert(std::make_pair(_pl, _rawid));
	}
}
void write_list(std::string filename, std::set<std::pair<int, int>> &list) {
	std::ofstream ofs(filename);
	for (auto itr = list.begin(); itr != list.end(); itr++) {
		ofs << std::right << std::fixed << std::setfill(' ')
			<< std::setw(4) << std::setprecision(0) << itr->first << " "
			<< std::setw(12) << std::setprecision(0) << itr->second << std::endl;
	}
}
