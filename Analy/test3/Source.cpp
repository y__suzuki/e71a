#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <set>
#include <list>
#include <algorithm>

std::vector<vxx::base_track_t> micro_unique_face0_ver1(std::vector<vxx::base_track_t> &base);

bool sort_id0(const vxx::base_track_t& left, const vxx::base_track_t& right) {
	return left.m[0].rawid < right.m[0].rawid;
}

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg input-bvxx output-bvxx pl\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	std::string file_out_bvxx = argv[2];
	int pl = std::stoi(argv[3]);

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);

	base = micro_unique_face0_ver1(base);

}
std::vector<vxx::base_track_t> micro_unique_face0_ver2(std::vector<vxx::base_track_t> &base) {
	std::vector<vxx::base_track_t> ret;
	sort(base.begin(), base.end(), sort_id0);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::vector<vxx::base_track_t> base_same_micro;
		base_same_micro.push_back(*itr);

		while (itr + 1 != base.end()&&itr->m[0].rawid==(itr+1)->m[0].rawid) {
			itr++;
			base_same_micro.push_back(*itr);
		}
		vxx::base_track_t selected = select_best_base(base_same_micro);
		ret.push_back(selected);

	}

	return ret;

}
std::vector<vxx::base_track_t> micro_unique_face0_ver3(std::vector<vxx::base_track_t> &base) {
	//sort(base.begin(), base.end(), sort_id0);
	std::vector<vxx::base_track_t> ret;
	std::set<int> finish_id;
	for (auto itr0 = base.begin(); itr0 != base.end(); itr0++) {
		if (finish_id.count(itr0->m[0].rawid) == 1)continue;
		finish_id.insert(itr0->m[0].rawid);


		std::vector<vxx::base_track_t> base_same_micro;
		for (auto itr1 = base.begin(); itr1 != base.end(); itr1++) {
			if (itr0->m[0].rawid == itr1->m[0].rawid) {
				base_same_micro.push_back(*itr1);
			}
		}

		vxx::base_track_t selected = select_best_base(base_same_micro);
		ret.push_back(selected);

	}

	return ret;
}
std::vector<vxx::base_track_t> micro_unique_face0_ver1(std::vector<vxx::base_track_t> &base) {

	std::vector<vxx::base_track_t> ret;

	std::multimap<int, vxx::base_track_t*> face0_micro;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		face0_micro.insert(std::make_pair(itr->m[0].rawid, &(*itr)));
	}

	int rawid,count = 0;
	for (auto itr = face0_micro.begin(); itr != face0_micro.end(); itr++) {
		rawid = itr->first;
		count = face0_micro.count(rawid);

		auto range = face0_micro.equal_range(rawid);
		std::vector<vxx::base_track_t> base_group;
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			base_group.push_back(*itr2->second);
		}
		vxx::base_track_t selected = select_best_base(base_group);
		ret.push_back(selected);

		itr = std::next(itr, count - 1);

	}

	return ret;
}
vxx::base_track_t select_best_base(std::vector<vxx::base_track_t> &base) {
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		int ph = itr->m[0].ph + itr->m[1].ph;
	}
	return *(base.begin());
}