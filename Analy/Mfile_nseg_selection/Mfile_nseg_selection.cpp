#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>

class ECC_area {
public:
	std::map <int, double> xmin, ymin, xmax, ymax, z;
};
class Chain_classification {
public:
	mfile0::M_Chain *c;
	//flg
	//上16bit --> upstream
	//0000 0000 0000 0000
	//下16bit --> downstream
	unsigned int flg;

	void Show_all_flg() {
		const int BitSize = sizeof(int) * 8; // 整数型のビットサイズを算出
		int bit = 1, i;
		char c[BitSize];
		for (i = 0; i < BitSize; i++) {
			if (flg & bit)
				c[i] = '1';
			else
				c[i] = '0';
			bit <<= 1;
		}
		// 計算結果の表示
		for (i = BitSize - 1; i >= 0; i--) {
			putchar(c[i]);
			if (i % 4 == 0) {
				printf(" ");
			}
		}
		printf("\n");
	}
	//下1桁 penetrate flg
	bool Get_penetrate(bool up) {
		return flg & (1 << (16 * up + 0));
	}
	void Set_penetrate(bool up) {
		flg |= (1 << (16 * up + 0));
	}
	//下2桁 upstream(downstream)
	bool Get_zout(bool up) {
		return flg & (1 << (16 * up + 1));
	}
	void Set_zout(bool up) {
		flg |= (1 << (16 * up + 1));
	}
	//下3桁 side out
	bool Get_sideout(bool up) {
		return flg & (1 << (16 * up + 2));
	}
	void Set_sideout(bool up) {
		flg |= (1 << (16 * up + 2));
	}
	//下4桁 side out(x)
	bool Get_sideout_x(bool up) {
		return flg & (1 << (16 * up + 3));
	}
	void Set_sideout_x(bool up) {
		flg |= (1 << (16 * up + 3));
	}
	//下5桁 side out(y)
	bool Get_sideout_y(bool up) {
		return flg & (1 << (16 * up + 4));
	}
	void Set_sideout_y(bool up) {
		flg |= (1 << (16 * up + 4));
	}

};

void each_pl_range(std::vector<mfile0::M_Chain> &c, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax);
void set_z(std::vector<mfile0::M_Chain> &c, std::map <int, double> &z);
void SetArea_min(std::map <int, double> &min, double cut);
void SetArea_max(std::map <int, double> &max, double cut);
double GetVal(std::map <int, double> &value, int pl);
void chain_angle_cut(std::vector<mfile0::M_Chain> &c, double thr);

std::vector<Chain_classification> Set_Chain_classification(std::vector<mfile0::M_Chain> &c);
void Set_upstream_flg(std::vector<Chain_classification>&c, ECC_area &area);
void Set_downstream_flg(std::vector<Chain_classification>&c, ECC_area &area);

void SetChainStartStop(std::vector<Chain_classification>&c, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map);
void Show_classification(std::vector<Chain_classification>&c);

//ここまで
std::vector<mfile0::M_Chain>chain_nseg_selection(std::multimap <int, mfile0::M_Chain*> &stop_pl_map, int nseg_cut);
	bool connect_judge(mfile0::M_Chain &c0, mfile0::M_Chain &c1);
void SetChainStartStop(std::vector<mfile0::M_Chain>& c, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax, std::map <int, double> &z);
std::vector<mfile0::Mfile> Chain_reconnect(int pl, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map);
void connect_judge(mfile0::M_Chain &c0, mfile0::M_Chain &c1, std::ofstream &ofs);
void Chain_reconnect(int pl, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map, std::string filename);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mfile out-mfile nseg_cut\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	//std::string file_out_mfile = argv[2];
	std::string file_out_mfile = argv[2];
	int nseg_cut = std::stoi(argv[3]);

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	ECC_area area;
	each_pl_range(m.chains, area.xmin, area.ymin, area.xmax, area.ymax);
	set_z(m.chains, area.z);
	SetArea_min(area.xmin, 1000);
	SetArea_min(area.ymin, 1000);
	SetArea_max(area.xmax, 1000);
	SetArea_max(area.ymax, 1000);
	printf("area %8.1lf - %8.1lf, %8.1lf - %8.1lf\n", area.xmin.begin()->second, area.xmax.begin()->second, area.ymin.begin()->second, area.ymax.begin()->second);
	printf("%8.1lf cm^2\n", (area.xmax.begin()->second - area.xmin.begin()->second)*(area.ymax.begin()->second - area.ymin.begin()->second) / 1000 / 1000 / 10 / 10);


	std::vector<Chain_classification> chain_cls = Set_Chain_classification(m.chains);
	//for (int i = 1; i <= 51; i++) {
	//	if (area.xmax.count(i) + area.ymax.count(i) + area.xmin.count(i) + area.ymin.count(i) + area.z.count(i) == 5) {
	//		printf("PL%03d %8.1lf %8.1lf %8.1lf %8.1lf %8.1lf\n", i,area.xmin[i], area.xmax[i], area.ymin[i], area.ymax[i], area.z[i]);
	//	}
	//}
	Set_upstream_flg(chain_cls, area);
	Set_downstream_flg(chain_cls, area);


	//表示
	Show_classification(chain_cls);


	std::multimap<int, mfile0::M_Chain *>  stop_pl, start_pl;
	SetChainStartStop(chain_cls, stop_pl, start_pl);
	m.chains = chain_nseg_selection(stop_pl, nseg_cut);
	chain_angle_cut(m.chains, 4);

	mfile0::write_mfile(file_out_mfile, m);

}
//area関連
void set_z(std::vector<mfile0::M_Chain> &c, std::map <int, double> &z) {
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			z.insert(std::make_pair(itr->pos / 10, itr->z));
		}
	}

}
void each_pl_range(std::vector<mfile0::M_Chain> &c, std::map <int, double> &xmin, std::map <int, double> &ymin, std::map <int, double> &xmax, std::map <int, double>&ymax) {
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		for (auto itr = itr_c->basetracks.begin(); itr != itr_c->basetracks.end(); itr++) {
			auto res1 = xmin.insert(std::make_pair(itr->pos / 10, itr->x));
			auto res2 = xmax.insert(std::make_pair(itr->pos / 10, itr->x));
			auto res3 = ymin.insert(std::make_pair(itr->pos / 10, itr->y));
			auto res4 = ymax.insert(std::make_pair(itr->pos / 10, itr->y));
			if (!res1.second)res1.first->second = std::min(res1.first->second, itr->x);
			if (!res2.second)res2.first->second = std::max(res2.first->second, itr->x);
			if (!res3.second)res3.first->second = std::min(res3.first->second, itr->y);
			if (!res4.second)res4.first->second = std::max(res4.first->second, itr->y);
		}
	}

	auto itr1 = xmin.begin();
	auto itr2 = xmax.begin();
	auto itr3 = ymin.begin();
	auto itr4 = ymax.begin();
	for (auto itr = xmin.begin(); itr != xmin.end(); itr++) {
		if (itr->first != itr1->first || itr->first != itr2->first || itr->first != itr3->first || itr->first != itr4->first) {
			fprintf(stderr, "exception different PL\n");
			printf("xmin:PL%03d xmax:PL%03d ymin:PL%03d ymax:PL%03d\n", itr1->first, itr2->first, itr3->first, itr4->first);
			exit(1);
		}
		printf("PL%03d (%8.1lf, %8.1lf) (%8.1lf, %8.1lf)\n", itr->first, itr1->second, itr2->second, itr3->second, itr4->second);
		itr1++;
		itr2++;
		itr3++;
		itr4++;
	}
	return;
}
void SetArea_min(std::map <int, double> &min, double cut)
{
	double set_val;
	for (auto itr = min.begin(); itr != min.end(); itr++) {
		if (itr == min.begin()) {
			set_val = itr->second;
		}
		set_val = std::max(set_val, itr->second);
	}
	set_val = set_val + cut;
	for (auto itr = min.begin(); itr != min.end(); itr++) {
		itr->second = set_val;
	}
}
void SetArea_max(std::map <int, double> &max, double cut)
{
	double set_val;
	for (auto itr = max.begin(); itr != max.end(); itr++) {
		if (itr == max.begin()) {
			set_val = itr->second;
		}
		set_val = std::min(set_val, itr->second);
	}
	set_val = set_val - cut;
	for (auto itr = max.begin(); itr != max.end(); itr++) {
		itr->second = set_val;
	}
}
double GetVal(std::map <int, double> &value, int pl) {
	if (value.count(pl) == 0) {
		fprintf(stderr, "PL%03d not found\n");
		return -1;
	}
	return value[pl];
}

//chain
std::vector<Chain_classification> Set_Chain_classification(std::vector<mfile0::M_Chain> &c) {
	std::vector<Chain_classification> chain_cls;
	chain_cls.reserve(c.size());
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		Chain_classification cls;
		cls.c = &(*itr);
		cls.flg = 0;
		chain_cls.push_back(cls);
	}
	return chain_cls;
}
void Set_upstream_flg(std::vector<Chain_classification>&c, ECC_area &area) {
	int pl_min = area.z.begin()->first;
	int pl_max = area.z.rbegin()->first;
	//printf("PL%03d - PL%03d\n", pl_min, pl_max);
	for (int i = 0; i < c.size(); i++) {
		mfile0::M_Chain chain = *(c[i].c);
		//最上流の貫通チェック
		if (chain.pos1 / 10 == pl_max || chain.pos1 / 10 == pl_max - 1) {
			c[i].Set_penetrate(1);
			c[i].Set_zout(1);
		}
		else {
			//最上流貫通しない-->side outのチェック
			int pl = chain.pos1 / 10;
			int ex_pl = chain.pos1 / 10 + 1;
			double 	gap = area.z[ex_pl] - area.z[pl];

			double ex_x = chain.basetracks.rbegin()->x + chain.basetracks.rbegin()->ax *gap;
			double ex_y = chain.basetracks.rbegin()->y + chain.basetracks.rbegin()->ay *gap;
			//x edge out
			if (ex_x < area.xmin[ex_pl] || area.xmax[ex_pl] < ex_x) {
				c[i].Set_penetrate(1);
				c[i].Set_sideout(1);
				c[i].Set_sideout_x(1);
			}
			//y edge out
			if (ex_y < area.ymin[ex_pl] || area.ymax[ex_pl] < ex_y) {
				c[i].Set_penetrate(1);
				c[i].Set_sideout(1);
				c[i].Set_sideout_y(1);
			}
		}
	}
}
void Set_downstream_flg(std::vector<Chain_classification>&c, ECC_area &area) {
	int pl_min = area.z.begin()->first;
	int pl_max = area.z.rbegin()->first;
	//printf("PL%03d - PL%03d\n", pl_min, pl_max);

	for (int i = 0; i < c.size(); i++) {
		mfile0::M_Chain chain = *(c[i].c);
		//最下流の貫通チェック
		if (chain.pos0 / 10 == pl_min || chain.pos0 / 10 == pl_min + 1) {
			c[i].Set_penetrate(0);
			c[i].Set_zout(0);
		}
		else {
			//最下流貫通しない-->side outのチェック
			int pl = chain.pos0 / 10;
			int ex_pl = chain.pos0 / 10 - 1;
			double 	gap = area.z[ex_pl] - area.z[pl];

			double ex_x = chain.basetracks.begin()->x + chain.basetracks.begin()->ax *gap;
			double ex_y = chain.basetracks.begin()->y + chain.basetracks.begin()->ay *gap;
			//x edge out
			if (ex_x < area.xmin[ex_pl] || area.xmax[ex_pl] < ex_x) {
				c[i].Set_penetrate(0);
				c[i].Set_sideout(0);
				c[i].Set_sideout_x(0);
			}
			//y edge out
			if (ex_y < area.ymin[ex_pl] || area.ymax[ex_pl] < ex_y) {
				c[i].Set_penetrate(0);
				c[i].Set_sideout(0);
				c[i].Set_sideout_y(0);
			}
		}
	}
}
//分類後種類分け
void Show_classification(std::vector<Chain_classification>&c) {
	int penetrate = 0, full_contained = 0, half_contained = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		if (itr->Get_penetrate(0) + itr->Get_penetrate(1) == 2) {
			penetrate++;
		}
		else if (itr->Get_penetrate(0) + itr->Get_penetrate(1) == 1) {
			half_contained++;
		}
		else if (itr->Get_penetrate(0) + itr->Get_penetrate(1) == 0) {
			full_contained++;
		}
	}
	printf("all track:%d\n", c.size());
	printf("\tpenetrate     :%d\n", penetrate);
	printf("\thalf_contained:%d\n", half_contained);
	printf("\tfull_contained:%d\n", full_contained);
}

//上流stop 下流stopの分類
void SetChainStartStop(std::vector<Chain_classification>&c, std::multimap <int, mfile0::M_Chain*> &stop_pl_map, std::multimap <int, mfile0::M_Chain*> &start_pl_map) {
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		//下流stop
		//start track(最下流)
		if (itr->Get_penetrate(0) == 0) {
			start_pl_map.insert(std::make_pair(itr->c->pos0 / 10, itr->c));
		}
		//上流stop
		//stop track(最上流)
		if (itr->Get_penetrate(1) == 0) {
			stop_pl_map.insert(std::make_pair(itr->c->pos1 / 10, itr->c));
		}
	}
}

std::vector<mfile0::M_Chain>chain_nseg_selection(std::multimap <int, mfile0::M_Chain*> &stop_pl_map, int nseg_cut) {
	std::vector<mfile0::M_Chain> ret;
	int nseg, stopPL;
	for (auto itr = stop_pl_map.begin(); itr != stop_pl_map.end(); itr++) {
		nseg = itr->second->nseg;
		stopPL = itr->second->pos1 / 10;
		if (stopPL < 16)continue;
		if (nseg == nseg_cut) {
			ret.push_back(*itr->second);
		}
	}

	fprintf(stderr, "nseg cut(nseg=%d):%d --> %d(%4.1lf%%)\n", nseg_cut, stop_pl_map.size(), ret.size(), ret.size()*100. / stop_pl_map.size());

	return ret;
}
void chain_angle_cut(std::vector<mfile0::M_Chain> &c, double thr) {
	std::vector<mfile0::M_Chain> sel;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		double ax = mfile0::chain_ax(*itr);
		double ay = mfile0::chain_ay(*itr);
		if (fabs(ax) > thr)continue;
		if (fabs(ay) > thr)continue;
		sel.push_back(*itr);
	}
	printf("chain angle cut %d --> %d\n", c.size(), sel.size());
	c.clear();
	c = sel;
	return;
}
