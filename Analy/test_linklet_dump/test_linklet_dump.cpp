//数字の小さいPLから大きいPLにbasetrackを外挿する。
//下流-->上流
#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib,"FILE_structure.lib")

#include <VxxReader.h>
#include <FILE_structure.hpp>
std::vector<netscan::linklet_t> link_area_cut(std::vector<netscan::linklet_t> &link);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg input output\n");
		exit(1);
	}
	std::string file_in_link = argv[1];
	std::string file_out_link = argv[2];

	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_bin(file_in_link, link);
	link = link_area_cut(link);
	netscan::write_linklet_txt(file_out_link, link);

}
std::vector<netscan::linklet_t> link_area_cut(std::vector<netscan::linklet_t> &link) {
	std::vector<netscan::linklet_t> ret;
	double x_tmp = 50000;
	double y_tmp = 50000;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (fabs(itr->b[0].x - x_tmp) > 1000)continue;
		if (fabs(itr->b[0].y - y_tmp) > 1000)continue;
		ret.push_back(*itr);
	}
	return ret;
}
