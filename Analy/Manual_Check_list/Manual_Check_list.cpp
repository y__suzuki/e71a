#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

class extra_track {
public:
	vxx::base_track_t base;
	int64_t gid;
	int id;
};

bool sort_extra(const extra_track&left,const extra_track&right) {
	if (left.base.pl != right.base.pl) {
		return left.base.pl < right.base.pl;
	}
	if (abs(left.id) != abs(right.id)) {
		return abs(left.id) < abs(right.id);
	}
	return left.base.rawid < right.base.rawid;
}

void pick_up_muon(mfile0::Mfile &m, std::multimap<int, extra_track>&extra);

std::multimap<int, extra_track> pick_up_extra_base(std::vector<mfile0::M_Chain>&chains, std::map<int64_t, int> muon_pl);
void pick_up_basetrack_inf(std::multimap<int, extra_track> &extra, std::string file_in_ECC);
std::map<int64_t, int> pick_up_event_pl(mfile0::Mfile &m);

void basetrack_extra_upstream(std::multimap<int, extra_track> &extra, std::string file_in_ECC, std::map<int, double> z_map);
void basetrack_extra_upstream(std::vector<extra_track>&base, std::vector<corrmap0::Corrmap> &corr, const double gap, std::vector<extra_track>&extra_end);
void basetrack_extra_downstream(std::multimap<int, extra_track> &extra, std::string file_in_ECC, std::map<int, double> z_map);
void basetrack_extra_downstream(std::vector<extra_track>&base, std::vector<corrmap0::Corrmap> &corr,const double gap, std::vector<extra_track>&extra_end);

void corrmap_area_inverse(std::vector<corrmap0::Corrmap> &corr);
void basetrack_trans(vxx::base_track_t& base, corrmap0::Corrmap param);
void basetrack_trans_inv(vxx::base_track_t& base, corrmap0::Corrmap param);

void base_angle_correction(std::multimap<int, extra_track> &extra);

void write_base_extra(std::multimap<int, extra_track> &extra, std::string fileout_path);


int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file-in-mfile file-in-check_muon file-in-ECC(Area) output-path\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_mu = argv[2];
	std::string file_in_ECC = argv[3];
	std::string file_out_path = argv[4];

	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);



	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	//eventのPL情報の抽出
	std::map<int64_t, int> muon_pl = pick_up_event_pl(m);
	//外挿したいbasetrackを抜き出し
	std::multimap<int, extra_track> extra = pick_up_extra_base(m.chains, muon_pl);
	//alignment確認用muon track抜き出し
	mfile0::Mfile mu;
	mfile0::read_mfile(file_in_mu, mu);
	pick_up_muon(mu, extra);

	//外挿すべきbasetrackの情報抽出
	pick_up_basetrack_inf(extra, file_in_ECC);

	//上流へ外挿
	basetrack_extra_upstream(extra, file_in_ECC, z_map);
	//下流へ外挿
	basetrack_extra_downstream(extra, file_in_ECC, z_map);

	//corrの角度補正(逆)
	base_angle_correction(extra);
	//bvxx,listの書き出し
	write_base_extra(extra, file_out_path);
}
void pick_up_muon(mfile0::Mfile &m, std::multimap<int, extra_track>&extra) {
	std::multimap<int, extra_track>ret;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			extra_track ex;
			ex.base.pl = itr2->pos / 10;
			ex.base.rawid = itr2->rawid;
			ex.id = 0;
			ex.gid = itr2->group_id;
			extra.insert(std::make_pair(ex.base.pl,ex));
			//extra.insert(std::make_pair(down.base.pl, down));

		}

	}

}

std::map<int64_t, int> pick_up_event_pl(mfile0::Mfile &m) {
	std::map<int64_t, int> ret;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (itr->basetracks.begin()->group_id % 100000 == 0) {
			ret.insert(std::make_pair(itr->basetracks.begin()->group_id / 100000, itr->pos1 / 10));
		}
	}
	return ret;
}
std::multimap<int, extra_track> pick_up_extra_base(std::vector<mfile0::M_Chain>&chains, std::map<int64_t, int> muon_pl) {
	std::multimap<int, extra_track> extra;
	int64_t gid;
	int pl;
	for (auto itr = chains.begin(); itr != chains.end(); itr++) {
		if (itr->basetracks.begin()->group_id % 100000 != 0)continue;
		gid = itr->basetracks.begin()->group_id / 100000;
		pl = muon_pl.at(gid);
		extra_track up, down;
		if (itr->pos0 / 10 <= pl) {
			auto up_track = itr->basetracks.rbegin();
			if ((up_track->pos - std::next(up_track, 1)->pos) / 10 > 1) {
				up_track= std::next(up_track, 1);
			}
			up.base.pl = up_track->pos / 10;
			up.base.rawid = up_track->rawid;
			down.base.pl = up_track->pos / 10;
			down.base.rawid = up_track->rawid;
			up.gid = up_track->group_id;
			down.gid = up_track->group_id;
			up.id = 1;
			down.id = -1;
			extra.insert(std::make_pair(up.base.pl, up));
			//extra.insert(std::make_pair(down.base.pl, down));
		}
		else {
			up.base.pl = itr->basetracks.rbegin()->pos / 10;
			up.base.rawid = itr->basetracks.rbegin()->rawid;
			down.base.pl = itr->basetracks.begin()->pos / 10;
			down.base.rawid = itr->basetracks.begin()->rawid;
			up.gid = itr->basetracks.begin()->group_id;
			down.gid = itr->basetracks.begin()->group_id;
			up.id = 1;
			down.id = -1;
			//extra.insert(std::make_pair(up.base.pl, up));
			extra.insert(std::make_pair(down.base.pl, down));
		}
	}

	return extra;
}

void pick_up_basetrack_inf(std::multimap<int, extra_track> &extra, std::string file_in_ECC) {
	std::multimap<int, extra_track> ex2;
	vxx::BvxxReader br;
	bool flg = true;
	for (auto itr = extra.begin(); itr != extra.end(); itr++) {
		int pl = itr->first;
		int count = extra.count(pl);
		if (count == 0)continue;
		flg = false;
		auto range = extra.equal_range(pl);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.id != 0)flg = true;
		}
		if (!flg)continue;

		printf("now PL%03d\n", pl);
		std::stringstream file_in_base;
		file_in_base << file_in_ECC << "\\PL" << std::setw(3) << std::setfill('0') << pl
			<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
		if (!std::filesystem::exists(file_in_base.str())) {
			fprintf(stderr, "%s not found\n", file_in_base.str().c_str());
			exit(1);
		}

		std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), pl, 0);
		std::map<int, vxx::base_track_t> base_map;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_map.insert(std::make_pair(itr->rawid, (*itr)));
		}

		range = extra.equal_range(pl);
		for (auto res = range.first; res != range.second; res++) {
			auto find_base = base_map.find(res->second.base.rawid);
			if (find_base == base_map.end()) {
				fprintf(stderr, "PL%03d rawid=%d not found\n", pl, res->second.base.rawid);
				exit(1);
			}
			res->second.base = (find_base->second);
			ex2.insert(*res);
		}
		itr = std::next(itr, count - 1);
	}
	extra = ex2;

}

void basetrack_extra_upstream(std::multimap<int, extra_track> &extra, std::string file_in_ECC, std::map<int, double> z_map) {
	std::vector<extra_track> extra_end;

	for (auto itr = extra.begin(); itr != extra.end(); itr++) {
		int pl = itr->first;

		int count = extra.count(pl);
		if (count == 0)continue;
		std::vector<extra_track> extra_base;
		auto range = extra.equal_range(pl);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.id == 1) {
				extra_base.push_back(res->second);
			}
		}
		if (extra_base.size() != 0) {
			int extra_pl = pl + 1;
			while (true) {
				std::stringstream file_in_corr;
				file_in_corr << file_in_ECC << "\\0\\align\\corrmap-align-"
					<< std::setw(3) << std::setfill('0') << pl << "-"
					<< std::setw(3) << std::setfill('0') << extra_pl << ".lst";
				if (!std::filesystem::exists(file_in_corr.str())) {
					//fprintf(stderr, "%s not found\n", file_in_corr.str().c_str());
					break;
				}
				if (z_map.count(pl) + z_map.count(extra_pl) != 2) {
					fprintf(stderr, "z nominal not found PL%03d/PL%03d\n", pl, extra_pl);
					break;
				}
				std::vector<corrmap0::Corrmap> corr;
				corrmap0::read_cormap(file_in_corr.str(), corr, 0);
				printf("extrapolate PL%03d --> PL%03d\n", pl, extra_pl);

				//ここで外挿
				basetrack_extra_upstream(extra_base, corr, z_map.at(extra_pl) - z_map.at(pl), extra_end);

				extra_pl++;
			}
		}
		itr = std::next(itr, count - 1);
	}

	for (auto itr = extra_end.begin(); itr != extra_end.end(); itr++) {
		extra.insert(std::make_pair(itr->base.pl, *itr));
	}
}
void basetrack_extra_upstream(std::vector<extra_track>&base, std::vector<corrmap0::Corrmap> &corr,const double gap, std::vector<extra_track>&extra_end) {

	int ex_id = (corr.begin()->pos[1] - corr.begin()->pos[0]) / 10 + 1;
	int ex_pl = corr.begin()->pos[1] / 10;


	double dz ,ex_x,ex_y,dist;
	int id ,id_prev,loop;
	corrmap0::Corrmap param;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		dz = 0;
		id = 0;
		id_prev = -1;
		loop = 0;
		while (loop < 10) {
			if (loop == 0) {
				dz = 0;
			}
			else {
				dz = param.dz;
			}
			ex_x = itr->base.x + itr->base.ax*(gap + dz);
			ex_y = itr->base.y + itr->base.ay*(gap + dz);
			for (int i = 0; i < corr.size(); i++) {
				if (i == 0) {
					id = i;
					dist = pow((corr[i].areax[0] + corr[i].areax[1]) / 2 - ex_x, 2) + pow((corr[i].areay[0] + corr[i].areay[1]) / 2 - ex_y, 2);
				}
				if (dist > pow((corr[i].areax[0] + corr[i].areax[1]) / 2 - ex_x, 2) + pow((corr[i].areay[0] + corr[i].areay[1]) / 2 - ex_y, 2)) {
					id = i;
					dist = pow((corr[i].areax[0] + corr[i].areax[1]) / 2 - ex_x, 2) + pow((corr[i].areay[0] + corr[i].areay[1]) / 2 - ex_y, 2);
				}
			}
			param = corr[id];
			if (id == id_prev) {
				break;
			}
			id_prev = id;
			loop++;
		}
		extra_track ex;
		ex.base = itr->base;
		ex.gid = itr->gid;
		ex.id = ex_id;
		ex.base.pl = ex_pl;
		ex.base.m[0].pos = ex_pl * 10;
		ex.base.m[1].pos = ex_pl * 10 + 1;
		ex.base.x = ex.base.x + ex.base.ax*(gap + param.dz);
		ex.base.y = ex.base.y + ex.base.ay*(gap + param.dz);
		basetrack_trans_inv(ex.base, param);
		extra_end.push_back(ex);
	}
}

void basetrack_extra_downstream(std::multimap<int, extra_track> &extra, std::string file_in_ECC, std::map<int, double> z_map) {
	std::vector<extra_track> extra_end;

	for (auto itr = extra.begin(); itr != extra.end(); itr++) {
		int pl = itr->first;

		int count = extra.count(pl);
		if (count == 0)continue;
		std::vector<extra_track> extra_base;
		auto range = extra.equal_range(pl);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.id == -1) {
				extra_base.push_back(res->second);
			}
		}
		if (extra_base.size() != 0) {
			int extra_pl = pl - 1;
			while (true) {
				std::stringstream file_in_corr;
				file_in_corr << file_in_ECC << "\\0\\align\\corrmap-align-"
					<< std::setw(3) << std::setfill('0') << extra_pl << "-"
					<< std::setw(3) << std::setfill('0') << pl << ".lst";
				if (!std::filesystem::exists(file_in_corr.str())) {
					//fprintf(stderr, "%s not found\n", file_in_corr.str().c_str());
					break;
				}
				if (z_map.count(pl) + z_map.count(extra_pl) != 2) {
					fprintf(stderr, "z nominal not found PL%03d/PL%03d\n", pl, extra_pl);
					break;
				}
				std::vector<corrmap0::Corrmap> corr;
				corrmap0::read_cormap(file_in_corr.str(), corr, 0);
				printf("extrapolate PL%03d --> PL%03d\n", pl, extra_pl);

				//ここで外挿
				basetrack_extra_downstream(extra_base, corr, z_map.at(extra_pl) - z_map.at(pl), extra_end);

				extra_pl--;
			}
		}
		itr = std::next(itr, count - 1);
	}
	for (auto itr = extra_end.begin(); itr != extra_end.end(); itr++) {
		extra.insert(std::make_pair(itr->base.pl, *itr));
	}

}
void basetrack_extra_downstream(std::vector<extra_track>&base, std::vector<corrmap0::Corrmap> &corr,const double gap, std::vector<extra_track>&extra_end) {

	int ex_id = ((corr.begin()->pos[1] - corr.begin()->pos[0]) / 10 + 1)*-1;
	int ex_pl = corr.begin()->pos[0] / 10;
	corrmap_area_inverse(corr);

	double dist;
	int id;
	corrmap0::Corrmap param;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		for (int i = 0; i < corr.size(); i++) {
			if (i == 0) {
				id = i;
				dist = pow((corr[i].areax[0] + corr[i].areax[1]) / 2 - itr->base.x, 2) + pow((corr[i].areay[0] + corr[i].areay[1]) / 2 - itr->base.y, 2);
			}
			if (dist > pow((corr[i].areax[0] + corr[i].areax[1]) / 2 - itr->base.x, 2) + pow((corr[i].areay[0] + corr[i].areay[1]) / 2 - itr->base.y, 2)) {
				id = i;
				dist = pow((corr[i].areax[0] + corr[i].areax[1]) / 2 - itr->base.x, 2) + pow((corr[i].areay[0] + corr[i].areay[1]) / 2 - itr->base.y, 2);
			}
		}
		param = corr[id];
		extra_track ex;
		ex.base = itr->base;
		ex.gid = itr->gid;
		ex.id = ex_id;
		ex.base.pl = ex_pl;
		ex.base.m[0].pos = ex_pl * 10;
		ex.base.m[1].pos = ex_pl * 10+1;
		basetrack_trans(ex.base, param);
		ex.base.x = ex.base.x + ex.base.ax*(gap - param.dz);
		ex.base.y = ex.base.y + ex.base.ay*(gap - param.dz);
		extra_end.push_back(ex);
	}
}
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> &corr) {
	//hash_sizeはcorrmapのサイズより大きくする
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		double area[4], factor;
		area[0] = itr->areax[0] - itr->position[4];
		area[1] = itr->areax[1] - itr->position[4];
		area[2] = itr->areay[0] - itr->position[5];
		area[3] = itr->areay[1] - itr->position[5];

		factor = 1 / (itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);
		itr->areax[0] = factor * (area[0] * itr->position[3] - area[2] * itr->position[1]);
		itr->areay[0] = factor * (area[2] * itr->position[0] - area[0] * itr->position[2]);
		itr->areax[1] = factor * (area[1] * itr->position[3] - area[3] * itr->position[1]);
		itr->areay[1] = factor * (area[3] * itr->position[0] - area[1] * itr->position[2]);
	}
}


void basetrack_trans(vxx::base_track_t& base, corrmap0::Corrmap param) {
	double tmp_x, tmp_y;
	tmp_x = base.x;
	tmp_y = base.y;
	base.x = tmp_x * param.position[0] + tmp_y * param.position[1] + param.position[4];
	base.y = tmp_x * param.position[2] + tmp_y * param.position[3] + param.position[5];
	tmp_x = base.ax;
	tmp_y = base.ay;
	base.ax = tmp_x * param.angle[0] + tmp_y * param.angle[1] + param.angle[4];
	base.ay = tmp_x * param.angle[2] + tmp_y * param.angle[3] + param.angle[5];
}
void basetrack_trans_inv(vxx::base_track_t& base, corrmap0::Corrmap param) {
	double tmp_x, tmp_y,factor;
	tmp_x = base.x - param.position[4];
	tmp_y = base.y - param.position[5];
	factor = 1. / (param.position[0] * param.position[3] - param.position[1] * param.position[2]);
	base.x = factor * (tmp_x*param.position[3] - tmp_y * param.position[1]);
	base.y = factor * (tmp_y*param.position[0] - tmp_x * param.position[2]);

	tmp_x = base.ax - param.angle[4];
	tmp_y = base.ay - param.angle[5];
	factor = 1. / (param.angle[0] * param.angle[3] - param.angle[1] * param.angle[2]);
	base.ax = factor * (tmp_x*param.angle[3] - tmp_y * param.angle[1]);
	base.ay = factor * (tmp_y*param.angle[0] - tmp_x * param.angle[2]);
}

void base_angle_correction(std::multimap<int, extra_track> &extra) {

	for (auto itr = extra.begin(); itr != extra.end(); itr++) {
		itr->second.base.ax = itr->second.base.ax*1. / 0.951;
		itr->second.base.ay = itr->second.base.ay*1. / 0.951;
	}

}
void write_base_extra(std::multimap<int, extra_track> &extra, std::string fileout_path) {

	for (auto itr = extra.begin(); itr != extra.end(); itr++) {
		int pl = itr->first;
		int count = extra.count(pl);
		if (count == 0)continue;
		std::vector<vxx::base_track_t> extra_base;
		auto range = extra.equal_range(pl);
		for (auto res = range.first; res != range.second; res++) {
			extra_base.push_back(res->second.base);
		}
		std::stringstream file_out_bvxx;
		file_out_bvxx << fileout_path << "\\b" << std::setw(3) << std::setfill('0') << pl << ".vxx";
		{
			vxx::BvxxWriter bw;
			bw.Write(file_out_bvxx.str(), pl, 0, extra_base);
		}
		{
			vxx::BvxxReader br;
			extra_base = br.ReadAll(file_out_bvxx.str(), pl, 0);
		}
		std::map<std::tuple<int, int,int,int, int, int>, vxx::base_track_t> base_map;
		for (auto itr = extra_base.begin(); itr != extra_base.end(); itr++) {
			base_map.insert(std::make_pair(std::make_tuple(itr->m[0].pos, itr->m[0].zone, itr->m[0].rawid, itr->m[1].pos, itr->m[1].zone, itr->m[1].rawid), *itr));
		}
		for (auto res = range.first; res != range.second; res++) {
			auto res_find = base_map.find(std::make_tuple(res->second.base.m[0].pos, res->second.base.m[0].zone, res->second.base.m[0].rawid, res->second.base.m[1].pos, res->second.base.m[1].zone, res->second.base.m[1].rawid));
			if (res_find == base_map.end()) {
				printf("base not found\n");
			}
			res->second.base = res_find->second;
		}

		itr = std::next(itr, count - 1);
	}

	std::vector< extra_track> ex_out;
	for (auto itr = extra.begin(); itr != extra.end(); itr++) {
		ex_out.push_back(itr->second);
	}
	sort(ex_out.begin(), ex_out.end(), sort_extra);
	std::stringstream file_out_list;
	file_out_list << fileout_path << "\\extra_base.lst";
	std::ofstream ofs(file_out_list.str());
	for (auto itr = ex_out.begin(); itr != ex_out.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->base.pl << " "
			<< std::setw(10) << std::setprecision(0) << itr->base.rawid << " "
			<< std::setw(10) << std::setprecision(0) << itr->gid << " "
			<< std::setw(3) << std::setprecision(0) << itr->id << " "
			<< std::setw(7) << std::setprecision(4) << itr->base.ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->base.ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->base.x << " "
			<< std::setw(8) << std::setprecision(1) << itr->base.y << " "
			<< std::setw(7) << std::setprecision(0) << itr->base.m[0].ph + itr->base.m[1].ph << std::endl;
	}
}