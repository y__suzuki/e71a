#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <stack>
#include <unordered_set>

mfile0::Mfile select_multi_chain(mfile0::Mfile &m);


int main(int argc,char**argv) {
	if (argc != 3) {
		fprintf(stderr, "prg file-in-mfile file-out-mfile\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];
	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	mfile0::Mfile out=select_multi_chain(m);

	mfile0::write_mfile(file_out_mfile, out);

}
mfile0::Mfile select_multi_chain(mfile0::Mfile &m) {
	mfile0::Mfile ret;
	ret.header = m.header;


	std::multimap<int, mfile0::M_Chain> group;
	for (auto &c : m.chains) {
		group.insert(std::make_pair(c.basetracks.begin()->group_id, c));
	}

	for (auto itr = group.begin(); itr != group.end(); itr++) {
		int count = group.count(itr->first);
		if (count > 1) {
			auto range = group.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				ret.chains.push_back(res->second);
			}
		}

		itr = std::next(itr, count - 1);
	}
	return ret;
}
