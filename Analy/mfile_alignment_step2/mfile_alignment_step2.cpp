#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#include <omp.h>

class align_param {
public:
	int pl, id, signal, ix, iy;
	//視野中心
	double x, y, z;
	//parameter(9);
	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;

};
class align_param2 {
public:
	align_param* corr_p[3];
	//3点の視野中心の重心(回転中心)
	double x, y, z;
	//parameter(9);
	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;
public:
	//3つのparameterから計算
	void Calc_9param();

};
class align_track_inf {
public:
	mfile0::M_Base *base;
	mfile0::M_Chain *chain;
};
class Remain_difference {
public:
	double deltax_ax, deltay_ay, ax, deltax, ay, deltay, sigx, sigy, ax2, ay2;
	double dx, dy, dz;
	double dx_sum, dx2_sum, dy_sum, dy2_sum, dz_sum, dz2_sum;
	double dx_w, dy_w, dz_w;
	int num;
};
class Remain_shift_val {
public:
	double dx_sum, dy_sum, dz_sum;
	double dx_sig, dy_sig, dz_sig;
};
std::map<int, std::vector<align_param>> read_ali_param(std::string filename, bool output);
std::vector <std::pair<vxx::base_track_t*, align_param2*>>track_affineparam_correspondence(std::vector<vxx::base_track_t>&base, std::vector <align_param2> &param);
align_param2* search_param(std::vector<align_param*> &param, vxx::base_track_t&base, std::multimap<int, align_param2*>&triangles);
double select_triangle_vale(align_param2* param, vxx::base_track_t&base);
std::vector <align_param2 >DelaunayDivide(std::vector <align_param >&corr);
int use_thread(double ratio, bool output);

std::map<int, std::vector<align_track_inf>> chain_extract_pointer(std::vector<mfile0::M_Chain>*c);
std::vector<std::pair<align_param*, std::vector<align_track_inf>>> track_affineparam_correspondence(std::vector<align_param> *param_v, std::vector<align_track_inf>&track);

void Calc_track_difference(std::map<std::tuple<int, int, int, int, int, int>, Remain_difference> &ret, std::vector<std::pair<align_param*, std::vector<align_track_inf>>> &track_param);
std::vector<align_param>  Calc_shift_value(std::map<std::tuple<int, int, int, int, int, int>, Remain_difference> &diff_param, std::map<int, std::vector<align_param>> &corr_map);
void output_corrmap(std::string filename, std::vector<align_param> &corr);
void basetrack_trans(std::vector<std::pair<align_param*, std::vector<align_track_inf>>> &track_param);

double position_acc(double angle, double dz);
double angle_acc(double angle);
std::map<std::tuple<int, int, int, int, int, int>, Remain_difference> Use_Combination(std::vector<std::pair<align_param*, std::vector<align_track_inf>>> &track_param, int diff_pl_max, int count_thr);

int main(int argc, char**argv) {

	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-mfile in-corrmap out-corrmap\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_corr = argv[2];
	std::string file_out_corr = argv[3];
	std::map<int, std::vector<align_param>> corr_map = read_ali_param(file_in_corr, 1);

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::map<int, std::vector<align_track_inf>> track_inf_v = chain_extract_pointer(&(m.chains));

	std::vector<std::pair<align_param*, std::vector<align_track_inf>>> track_param_v_all;
	int pl = 0;
	for (auto itr = corr_map.begin(); itr != corr_map.end(); itr++) {
		pl = itr->first;
		printf("track allign map correspond PL%03d\n", pl);
		std::vector<std::pair<align_param*, std::vector<align_track_inf>>> track_param_v = track_affineparam_correspondence(&itr->second, track_inf_v.at(pl));
		basetrack_trans(track_param_v);
		for (auto itr2 = track_param_v.begin(); itr2 != track_param_v.end();itr2++) {
			track_param_v_all.push_back(*itr2);
		}
	}
	std::map<std::tuple<int, int, int, int, int, int>, Remain_difference> diff_param = Use_Combination(track_param_v_all, 5, 100);
	Calc_track_difference(diff_param, track_param_v_all);

	
	std::vector<align_param> out_corr= Calc_shift_value(diff_param, corr_map);
	for (auto itr = corr_map.begin(); itr != corr_map.end(); itr++) {
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			out_corr.push_back(*itr2);
		}
	}

	output_corrmap(file_out_corr, out_corr);
	//mfile1::write_mfile_extension(m, file_out_mfile);

}
std::map<int, std::vector<align_param>> read_ali_param(std::string filename, bool output) {


	std::multimap<int, align_param> read_corr;
	align_param param_tmp;
	std::ifstream ifs(filename);

	while (ifs >> param_tmp.pl >> param_tmp.id >> param_tmp.ix >> param_tmp.iy >> param_tmp.signal
		>> param_tmp.x >> param_tmp.y >> param_tmp.z
		>> param_tmp.x_rot >> param_tmp.y_rot >> param_tmp.z_rot
		>> param_tmp.x_shrink >> param_tmp.y_shrink >> param_tmp.z_shrink
		>> param_tmp.yx_shear >> param_tmp.zx_shear >> param_tmp.zy_shear
		>> param_tmp.dx >> param_tmp.dy >> param_tmp.dz) {
		read_corr.insert(std::make_pair(param_tmp.pl, param_tmp));
		//printf("ix %d iy%d\n", param_tmp.ix, param_tmp.iy);

	}
	if (output == 1) {
		fprintf(stderr, "%s input finish\n", filename.c_str());
	}
	if (read_corr.size() == 0) {
		fprintf(stderr, "%s alignment miss!\n", filename.c_str());
		exit(1);
	}

	int count = 0;
	std::vector<align_param> corr_v;;
	std::map<int, std::vector<align_param>> ret;
	for (auto itr = read_corr.begin(); itr != read_corr.end(); itr++) {
		count = read_corr.count(itr->first);
		corr_v.clear();

		auto range = read_corr.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			corr_v.push_back(res->second);
		}
		ret.insert(std::make_pair(itr->first, corr_v));

		itr = std::next(itr, count - 1);
	}
	return ret;

}


std::map<int, std::vector<align_track_inf>> chain_extract_pointer(std::vector<mfile0::M_Chain>*c) {
	std::map<int, std::vector<align_track_inf>> ret;
	std::multimap<int, align_track_inf> t_inf_map;
	int pl;
	align_track_inf t_inf;
	int64_t count = 0,all=c->size();
	for (auto itr = c->begin(); itr != c->end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r chain extract inf %lld/%lld(%4.1lf)", count, all, count*100. / all);
		}
		count++;


		for (auto itr2 = itr->basetracks.begin(); itr2!=itr->basetracks.end(); itr2++) {
			pl = itr2->pos / 10;
			t_inf.base = &(*itr2);
			t_inf.chain = &(*itr);
			t_inf_map.insert(std::make_pair(pl, t_inf));
		}
	}
	printf("\r chain extract inf %d/%d(%4.1lf)\n", count, all, count*100. / all);


	count = 0;
	all = t_inf_map.size();
	for (auto itr = t_inf_map.begin(); itr != t_inf_map.end(); itr++) {
		count = t_inf_map.count(itr->first);
		std::vector<align_track_inf> buf;
		buf.reserve(count);
		pl = itr->first;

		auto range = t_inf_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			buf.push_back(res->second);
		}
		ret.insert(std::make_pair(pl, buf));
		itr = std::next(itr, count - 1);
	}
	printf("chain extract inf fin\n");
	return ret;
}


std::vector<std::pair<align_param*, std::vector<align_track_inf>>> track_affineparam_correspondence(std::vector<align_param> *param_v, std::vector<align_track_inf>&track) {

	std::vector<std::pair<align_param*, std::vector<align_track_inf>>> ret;

	//align param hash
	double hash_size = 1000;
	std::multimap<std::pair<int, int>, align_param*> alignmap;
	double xmin = 0, ymin = 0;
	for (auto itr = param_v->begin(); itr != param_v->end(); itr++) {
		if (itr == param_v->begin()) {
			xmin = itr->x;
			ymin = itr->y;
		}
		xmin = std::min(itr->x, xmin);
		ymin = std::min(itr->y, ymin);
	}
	std::pair<int, int>id;

	for (auto itr = param_v->begin(); itr != param_v->end(); itr++) {
		id.first = (itr->x - xmin) / hash_size;
		id.second = (itr->y - ymin) / hash_size;
		alignmap.insert(std::make_pair(id, &(*itr)));
	}
	//////////////////////////////////


	//basetrackの位置に一番近い align paramを対応させる
	std::multimap<int,std::pair<align_param*, align_track_inf>> correspond_map;
	int64_t all = track.size(), count = 0;
	//private
	int ix, iy,loop_num=0;
	double dist;
	align_param* param =0;
	std::map<int, align_param* >p_map;

#pragma omp parallel for num_threads(use_thread(0.8,false)) schedule(guided) private(ix,iy,loop_num,dist,param,p_map,id)
	for (int i = 0; i < track.size(); i++) {

		//	for (auto itr = track.begin(); itr != track.end(); itr++) {
		if (count % 100000 == 0) {
			printf("\r search closest param %10d/%10d(%4.1lf)", count, all, count*100. / all);
		}
#pragma omp atomic
		count++;

		p_map.clear();
		loop_num = 0;
		ix = track[i].base->x / hash_size;
		iy = track[i].base->y / hash_size;
		while (p_map.size() < 5 && loop_num < 100) {
			loop_num++;
			for (int iix = -1 * loop_num; iix <= loop_num; iix++) {
				for (int iiy = -1 * loop_num; iiy <= loop_num; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (alignmap.count(id) == 0)continue;
					auto range = alignmap.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						p_map.insert(std::make_pair(res->second->id, res->second));
					}
				}
			}
		}
		if (p_map.size() < 5) {
			fprintf(stderr, "align map not found\n");
			track[i].base->flg_i[0] = -9999;
			track[i].base->flg_i[1] = -9999;
			continue;
		}
		dist = 9999999;
		for (auto itr_p = p_map.begin(); itr_p != p_map.end(); itr_p++) {
			if (itr_p == p_map.begin() || dist > pow(itr_p->second->x - track[i].base->x, 2) + pow(itr_p->second->y - track[i].base->y, 2)) {
				dist = pow(itr_p->second->x - track[i].base->x, 2) + pow(itr_p->second->y - track[i].base->y, 2);
				param = itr_p->second;
			}
		}
		//basetrackにIDを振る
		//track[i].base->flg_i[0] = param->ix;
		//track[i].base->flg_i[1] = param->iy;
#pragma omp critical
		correspond_map.insert(std::make_pair(param->id, std::make_pair(param, track[i])));
	}
	printf("\r search closest param %10d/%10d(%4.1lf)\n", count, all, count*100. / all);
	/////////////////////////////

	//対応させたtrackをvectorにまとめる
	std::vector<align_track_inf> track_vec;
	for (auto itr = correspond_map.begin(); itr != correspond_map.end(); itr++) {
		count = correspond_map.count(itr->first);
		track_vec.clear();
		track_vec.reserve(count);
		auto range = correspond_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			//basetrackにIDを振る
			res->second.second.base->flg_i[0] = res->second.first->ix;
			res->second.second.base->flg_i[1] = res->second.first->iy;

			track_vec.push_back(res->second.second);
		}
		ret.push_back(std::make_pair(itr->second.first, track_vec));
		itr = std::next(itr, count - 1);
	}

	return ret;
}

void basetrack_trans(std::vector<std::pair<align_param*, std::vector<align_track_inf>>> &track_param) {
	mfile0::M_Base* b;
	align_param* p;
	double x_tmp, y_tmp;
	for (auto itr = track_param.begin(); itr != track_param.end(); itr++) {
		p = itr->first;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			b = itr2->base;
			x_tmp = b->x;
			y_tmp = b->y;
			b->x = p->x_shrink*cos(p->z_rot)*x_tmp - p->y_shrink*sin(p->z_rot)*y_tmp + p->dx;
			b->y = p->x_shrink*sin(p->z_rot)*x_tmp + p->y_shrink*cos(p->z_rot)*y_tmp + p->dy;
			b->z = b->z + p->dz;

			x_tmp = b->ax;
			y_tmp = b->ay;
			b->ax = p->z_shrink*cos(p->z_rot)*x_tmp - p->z_shrink*sin(p->z_rot)*y_tmp + p->zx_shear;
			b->ay = p->z_shrink*sin(p->z_rot)*x_tmp + p->z_shrink*cos(p->z_rot)*y_tmp + p->zy_shear;

		}
	}
}

std::map<std::tuple<int, int, int, int, int, int>, Remain_difference> Use_Combination(std::vector<std::pair<align_param*, std::vector<align_track_inf>>> &track_param, int diff_pl_max, int count_thr) {
	std::map<std::tuple<int, int, int, int, int, int>, Remain_difference> ret;
	std::map < std::pair<int, int>, int>count_combination;
	std::pair<int, int> id;
	int pl0, pl1, ix, iy;
	int64_t count = 0, all = track_param.size();
	for (auto itr = track_param.begin(); itr != track_param.end(); itr++) {
		if (count % 10 == 0) {
			printf("\r calc difference %10d/%10d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		for (auto itr_t = itr->second.begin(); itr_t != itr->second.end(); itr_t++) {

			for (auto itr_b = itr_t->chain->basetracks.begin(); itr_b != itr_t->chain->basetracks.end(); itr_b++) {
				//PL小 --> PL大 のみ計算
				if (itr_t->base->pos >= itr_b->pos)continue;

				pl0 = itr_t->base->pos / 10;
				ix = itr_t->base->flg_i[0];
				iy = itr_t->base->flg_i[1];
				if (ix < -100)continue;
				if (iy < -100)continue;
				id.first = pl0 * 10000 + ix * 100 + iy;

				pl1 = itr_b->pos / 10;
				ix = itr_b->flg_i[0];
				iy = itr_b->flg_i[1];
				if (ix < -100)continue;
				if (iy < -100)continue;
				id.second = pl1 * 10000 + ix * 100 + iy;

				//PLが離れているところは無視
				if (pl1 - pl0 > diff_pl_max)continue;

				if (count_combination.count(id) == 0) {
					count_combination.insert(std::make_pair(id, 1));
				}
				else {
					count_combination.at(id) += 1;
				}
			}
		}
	}
	printf("\r calc difference %10d/%10d(%4.1lf%%)\n", count, all, count*100. / all);

	std::tuple<int, int, int, int, int, int>id2;
	//debug 
	std::ofstream ofs("use_conbination.txt");
	for (auto itr = count_combination.begin(); itr != count_combination.end(); itr++) {
		if (itr->second < count_thr)continue;
		std::get<0>(id2) = itr->first.first / 10000;
		std::get<1>(id2) = (itr->first.first % 10000) / 100;
		std::get<2>(id2) = itr->first.first % 100;
		std::get<3>(id2) = itr->first.second / 10000;
		std::get<4>(id2) = (itr->first.second % 10000) / 100;
		std::get<5>(id2) = itr->first.second % 100;
		Remain_difference diff;
		diff.deltax_ax = 0;
		diff.deltay_ay = 0;
		diff.ax = 0;
		diff.deltax = 0;
		diff.ay = 0;
		diff.deltay = 0;
		diff.sigx = 0;
		diff.sigy = 0;
		diff.ax2 = 0;
		diff.ay2 = 0;
		diff.dx_sum = 0;
		diff.dx2_sum = 0;
		diff.dy_sum = 0;
		diff.dy2_sum = 0;
		diff.dz_sum = 0;
		diff.dz2_sum = 0;
		diff.num = 0;
		ret.insert(std::make_pair(id2, diff));
		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << std::get < 0>(id2) << " "
			<< std::setw(3) << std::setprecision(0) << std::get < 1>(id2) << " "
			<< std::setw(3) << std::setprecision(0) << std::get < 2>(id2) << " "
			<< std::setw(3) << std::setprecision(0) << std::get < 3>(id2) << " "
			<< std::setw(3) << std::setprecision(0) << std::get < 4>(id2) << " "
			<< std::setw(3) << std::setprecision(0) << std::get < 5>(id2) << " "
			<< std::setw(10) << std::setprecision(0) << std::get < 5>(id2) << itr->second << std::endl;
	}
	printf("all param combination %lld\n", ret.size());

	return ret;
}

void Calc_track_difference(std::map<std::tuple<int, int, int, int, int, int>, Remain_difference> &ret,std::vector<std::pair<align_param*, std::vector<align_track_inf>>> &track_param) {
	
	std::tuple<int, int, int, int, int, int> id;
	//mfile0::M_Base *base0=0,*base1=0;
	int64_t count = 0,all=track_param.size();
	double ax, ay, sig_x, sig_y,dz;
	//ずれ量の計算
	for (auto itr = track_param.begin(); itr != track_param.end(); itr++) {
		if (count % 10 == 0) {
			printf("\r calc difference %10d/%10d(%4.1lf%%)",count,all,count*100./all);
		}
		count++;

		for (auto itr_t = itr->second.begin(); itr_t != itr->second.end(); itr_t++) {
			//itr_b -> align_track_inf
			//base = itr_t->base;
			ax = mfile0::chain_ax(*(itr_t->chain));
			ay = mfile0::chain_ay(*(itr_t->chain));

			for (auto itr_b = itr_t->chain->basetracks.begin(); itr_b != itr_t->chain->basetracks.end(); itr_b++) {
				//PL小 --> PL大 のみ計算
				if (itr_t->base->pos >= itr_b->pos)continue;
				std::get<0>(id) = itr_t->base->pos / 10;
				std::get<1>(id) = itr_t->base->flg_i[0];
				std::get<2>(id) = itr_t->base->flg_i[1];

				std::get<3>(id) = itr_b->pos / 10;
				std::get<4>(id) = itr_b->flg_i[0];
				std::get<5>(id) = itr_b->flg_i[1];

				//PLが離れているところは無視
				//if (std::get<3>(id) - std::get<0>(id) > diff_pl_max)continue;
				//ix/iyが対応していないbasetrackはid=-999が入る
				if (std::get<1>(id) < -100)continue;
				if (std::get<2>(id) < -100)continue;
				if (std::get<4>(id) < -100)continue;
				if (std::get<5>(id) < -100)continue;

				if (ret.count(id) == 0) continue;

				auto diff = ret.find(id);
				if (diff == ret.end()) {
					fprintf(stderr, "diff PL%03d %3d %3d PL%03d %3d %3d not found\n"
						, std::get < 0>(id), std::get<1>(id), std::get<2>(id)
						, std::get < 3>(id), std::get<4>(id), std::get<5>(id));
				}
				dz = itr_t->base->z - itr_b->z;

				sig_x = 1. / pow(position_acc(ax, dz), 2);
				sig_y = 1. / pow(position_acc(ay, dz), 2);

				diff->second.ax += ax * sig_x;
				diff->second.ax2 += ax * ax*sig_x;
				diff->second.ay += ay * sig_y;
				diff->second.ay2 += ay * ay * sig_y;
				diff->second.deltax += (itr_b->x - itr_t->base->x + ax * dz)*sig_x;
				diff->second.deltax_ax += (itr_b->x - itr_t->base->x + ax * dz)*ax*sig_x;
				diff->second.deltay += (itr_b->y - itr_t->base->y + ay * dz)*sig_y;
				diff->second.deltay_ay += (itr_b->y - itr_t->base->y + ay * dz)*ay*sig_y;
				diff->second.sigx += sig_x;
				diff->second.sigy += sig_y;
				diff->second.num += 1;

			}
		}
	}
	printf("\r calc difference %10d/%10d(%4.1lf%%)\n", count, all, count*100. / all);

	//parameterの計算
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {

		itr->second.dz =
			(itr->second.deltax_ax + itr->second.deltay_ay
				- (itr->second.ax*itr->second.deltax) / itr->second.sigx
				- (itr->second.ay*itr->second.deltay) / itr->second.sigy)
			/
			(itr->second.ax2 + itr->second.ay2
				- itr->second.ax*itr->second.ax / itr->second.sigx
				- itr->second.ay*itr->second.ay / itr->second.sigy);

		itr->second.dx =
			itr->second.ax / itr->second.sigx*itr->second.dz
			- itr->second.deltax / itr->second.sigx;

		itr->second.dy =
			itr->second.ay / itr->second.sigy*itr->second.dz
			- itr->second.deltay / itr->second.sigy;
		//debug
		//printf("PL%03d %3d %3d PL%03d %3d %3d %g %g %g\n"
		//	, std::get<0>(itr->first), std::get<1>(itr->first), std::get<2>(itr->first)
		//	, std::get<3>(itr->first), std::get<4>(itr->first), std::get<5>(itr->first)
		//	, itr->second.dx, itr->second.dy, itr->second.dz);
	}

	double d_dx, d_dy, d_dz;
	//補正後のずれ量の計算
	count = 0;
	for (auto itr = track_param.begin(); itr != track_param.end(); itr++) {
		if (count % 10 == 0) {
			printf("\r calc difference after correction %10d/%10d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		for (auto itr_t = itr->second.begin(); itr_t != itr->second.end(); itr_t++) {
			//itr_b -> align_track_inf
			//base = itr_t->base;
			ax = mfile0::chain_ax(*(itr_t->chain));
			ay = mfile0::chain_ay(*(itr_t->chain));

			for (auto itr_b = itr_t->chain->basetracks.begin(); itr_b != itr_t->chain->basetracks.end(); itr_b++) {
				//PL小 --> PL大 のみ計算
				if (itr_t->base->pos >= itr_b->pos)continue;

				std::get<0>(id) = itr_t->base->pos / 10;
				std::get<1>(id) = itr_t->base->flg_i[0];
				std::get<2>(id) = itr_t->base->flg_i[1];

				std::get<3>(id) = itr_b->pos / 10;
				std::get<4>(id) = itr_b->flg_i[0];
				std::get<5>(id) = itr_b->flg_i[1];
				if (ret.count(id) == 0) {
					continue;
				}

				auto diff = ret.find(id);
				dz = itr_t->base->z - itr_b->z;
				sig_x = 1. / position_acc(ax, dz);
				sig_y = 1. / position_acc(ay, dz);

				d_dx = itr_b->x + diff->second.dx - itr_t->base->x + ax * dz - ax * diff->second.dz;
				d_dy = itr_b->y + diff->second.dy - itr_t->base->y+ ay * dz - ay * diff->second.dz;
				d_dx = d_dx * sig_x;
				d_dy = d_dy * sig_y;

				d_dz = (ax*d_dx + ay * d_dy) / sqrt(ax*ax + ay * ay);

				diff->second.dx_sum += d_dx;
				diff->second.dy_sum += d_dy;
				diff->second.dz_sum += d_dz;

				diff->second.dx2_sum += d_dx * d_dx;
				diff->second.dy2_sum += d_dy * d_dy;
				diff->second.dz2_sum += d_dz * d_dz;

				diff->second.num++;

			}
		}
	}
	printf("\r calc difference after correction %10d/%10d(%4.1lf%%)\n", count, all, count*100. / all);

	//ずれ分布の幅を計算
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		if (itr->second.num < 3)continue;
		itr->second.dx_w = itr->second.dx2_sum / itr->second.num - pow(itr->second.dx_sum / itr->second.num, 2);
		itr->second.dy_w = itr->second.dy2_sum / itr->second.num - pow(itr->second.dy_sum / itr->second.num, 2);
		itr->second.dz_w = itr->second.dz2_sum / itr->second.num - pow(itr->second.dz_sum / itr->second.num, 2);

		itr->second.dx_w = sqrt(itr->second.dx_w);
		itr->second.dy_w = sqrt(itr->second.dy_w);
		itr->second.dz_w = sqrt(itr->second.dz_w);

		itr->second.dx_w = sqrt(itr->second.num / (itr->second.num - 3))*itr->second.dx_w;
		itr->second.dy_w = sqrt(itr->second.num / (itr->second.num - 3))*itr->second.dy_w;
		itr->second.dz_w = sqrt(itr->second.num / (itr->second.num - 3))*itr->second.dz_w;

		itr->second.dx_w = itr->second.dx_w / sqrt(itr->second.num);
		itr->second.dy_w = itr->second.dy_w / sqrt(itr->second.num);
		itr->second.dz_w = itr->second.dz_w / sqrt(itr->second.num);

		if (itr->second.dx_w <= 0)itr->second.num = -1;
		else itr->second.dx_w = 1 / itr->second.dx_w;
		if (itr->second.dy_w <= 0)itr->second.num = -1;
		else itr->second.dy_w = 1 / itr->second.dy_w;
		if (itr->second.dz_w <= 0)itr->second.num = -1;
		else itr->second.dz_w = 1 / itr->second.dz_w;

		//debug
		//printf("PL%03d %3d %3d PL%03d %3d %3d %g %g %g\n"
		//	, std::get<0>(itr->first), std::get<1>(itr->first), std::get<2>(itr->first)
		//	, std::get<3>(itr->first), std::get<4>(itr->first), std::get<5>(itr->first)
		//	, itr->second.dx_w, itr->second.dy_w, itr->second.dz_w);

	}

	
}

std::vector<align_param> Calc_shift_value(std::map<std::tuple<int, int, int, int, int, int>, Remain_difference> &diff_param, std::map<int, std::vector<align_param>> &corr_map) {
	std::vector<align_param>ret;

	std::set<int>pl,ix,iy;
	for (auto itr = diff_param.begin(); itr != diff_param.end(); itr++) {
		pl.insert(std::get<0>(itr->first));
		ix.insert(std::get<1>(itr->first));
		iy.insert(std::get<2>(itr->first));
		pl.insert(std::get<3>(itr->first));
		ix.insert(std::get<4>(itr->first));
		iy.insert(std::get<5>(itr->first));
	}
	std::tuple<int, int, int> id;
	Remain_shift_val shift_val_tmp;

	std::tuple<int, int, int,int,int,int> search_id;
	for (auto itr = corr_map.begin(); itr != corr_map.end(); itr++) {
		printf("Calc Shift Value PL%03d\n", itr->first);
		std::get<0>(id) = itr->first;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			std::get<1>(id) = itr2->ix;
			std::get<2>(id) = itr2->iy;

			shift_val_tmp.dx_sig = 0;
			shift_val_tmp.dy_sig = 0;
			shift_val_tmp.dz_sig = 0;
			shift_val_tmp.dx_sum = 0;
			shift_val_tmp.dy_sum = 0;
			shift_val_tmp.dz_sum = 0;

			for (auto t_pl = pl.begin(); t_pl != pl.end(); t_pl++) {
				if (std::get<0>(id) == *t_pl)continue;
				for (auto t_ix = ix.begin(); t_ix != ix.end(); t_ix++) {
					for (auto t_iy = iy.begin(); t_iy != iy.end(); t_iy++) {
						if (std::get<0>(id) < *t_pl) {
							std::get<0>(search_id) = itr2->pl;
							std::get<1>(search_id) = itr2->ix;
							std::get<2>(search_id) = itr2->iy;
							std::get<3>(search_id) = *t_pl;
							std::get<4>(search_id) = *t_ix;
							std::get<5>(search_id) = *t_iy;

							if (diff_param.count(search_id) == 0)continue;
							auto res = diff_param.find(search_id);

							shift_val_tmp.dx_sum += -1 * res->second.dx * res->second.dx_w;
							shift_val_tmp.dy_sum += -1 * res->second.dy * res->second.dy_w;
							shift_val_tmp.dz_sum += -1 * res->second.dz * res->second.dz_w;

							shift_val_tmp.dx_sig += res->second.dx_w;
							shift_val_tmp.dy_sig += res->second.dy_w;
							shift_val_tmp.dz_sig += res->second.dz_w;
						}
						else {
							std::get<3>(search_id) = itr2->pl;
							std::get<4>(search_id) = itr2->ix;
							std::get<5>(search_id) = itr2->iy;
							std::get<0>(search_id) = *t_pl;
							std::get<1>(search_id) = *t_ix;
							std::get<2>(search_id) = *t_iy;

							if (diff_param.count(search_id) == 0)continue;
							auto res = diff_param.find(search_id);

							shift_val_tmp.dx_sum +=  res->second.dx * res->second.dx_w;
							shift_val_tmp.dy_sum +=  res->second.dy * res->second.dy_w;
							shift_val_tmp.dz_sum +=  res->second.dz * res->second.dz_w;

							shift_val_tmp.dx_sig += res->second.dx_w;
							shift_val_tmp.dy_sig += res->second.dy_w;
							shift_val_tmp.dz_sig += res->second.dz_w;
						}

					}
				}
			}

			itr2->dx = shift_val_tmp.dx_sum / shift_val_tmp.dx_sig;
			itr2->dy = shift_val_tmp.dy_sum / shift_val_tmp.dy_sig;
			itr2->dz = shift_val_tmp.dz_sum / shift_val_tmp.dz_sig;

			ret.push_back(*itr2);
		}
	}

	return ret;

}

double position_acc(double angle, double gap) {
	double dx = 0.32229;
	double dz = 2.29754;
	double acc_single = 1 / 210.*sqrt(dx*dx + angle * angle*dz*dz);

	return acc_single * fabs(gap);
}
double angle_acc(double angle) {
	double dx = 0.32229;
	double dz = 2.29754;
	double acc_single = 1 / 210.*sqrt(dx*dx + angle * angle*dz*dz);

	return acc_single;
}


void output_corrmap(std::string filename, std::vector<align_param> &corr) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (corr.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)", count, int(corr.size()), count*100. / corr.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(0) << itr->pl << " "
				<< std::setw(8) << std::setprecision(0) << itr->id << " "
				<< std::setw(4) << std::setprecision(0) << itr->ix << " "
				<< std::setw(4) << std::setprecision(0) << itr->iy << " "
				<< std::setw(4) << std::setprecision(0) << itr->signal << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << " "
				<< std::setw(8) << std::setprecision(1) << itr->z << " "
				<< std::setw(9) << std::setprecision(7) << itr->x_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->y_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->z_rot << " "
				<< std::setw(8) << std::setprecision(6) << itr->x_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->y_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->z_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->yx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zy_shear << " "
				<< std::setw(8) << std::setprecision(2) << itr->dx << " "
				<< std::setw(8) << std::setprecision(2) << itr->dy << " "
				<< std::setw(8) << std::setprecision(2) << itr->dz << std::endl;
		}
		fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)\n", count, int(corr.size()), count*100. / corr.size());

	}



}


int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
