#define _CRT_SECURE_NO_WARNINGS
//#pragma comment(lib, "VxxReader.lib")
//#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <filesystem>

std::set<int> pick_groupid(std::vector<mfile0::M_Chain> &chain);
std::vector<Sharing_file::Sharing_file> sf_pickup(std::vector<Sharing_file::Sharing_file>&sf, std::set<int> &group);
std::set<int> pick_groupid(std::vector<Momentum_recon::Event_information>&momch);
std::set<int> read_gropfile(std::string filename);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_sf = argv[2];
	std::string file_out_sf = argv[3];

	//mfile0::Mfile m;
	//mfile1::read_mfile_extension(file_in_mfile, m);
	//std::set<int> group = pick_groupid(m.chains);

	//std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_mfile);
	//std::set<int> group = pick_groupid(momch);

	std::set<int> group = read_gropfile(file_in_mfile);

	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_extension(file_in_sf);

	std::vector<Sharing_file::Sharing_file>  sf_sel = sf_pickup(sf, group);

	Sharing_file::Write_sharing_file_txt(file_out_sf, sf_sel);

}
std::set<int> pick_groupid(std::vector<mfile0::M_Chain> &chain) {
	std::set<int> ret;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (ret.count(itr->basetracks.begin()->group_id) == 1) {
			ret.insert(itr->basetracks.begin()->group_id);
		}
	}
	printf("event num=%d\n", ret.size());
	return ret;
}
std::set<int> pick_groupid(std::vector<Momentum_recon::Event_information>&momch) {
	std::set<int> ret;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		if (ret.count(itr->groupid) == 1) {
			ret.insert(itr->groupid);
		}
	}
	printf("event num=%d\n", ret.size());
	return ret;


}
std::set<int> read_gropfile(std::string filename) {
	std::ifstream ifs(filename);
	std::set<int> ret;
	int gid;
	while (ifs >> gid) {
		ret.insert(gid);
	}
	printf("event num=%d\n", ret.size());
	return ret;

}

std::vector<Sharing_file::Sharing_file> sf_pickup(std::vector<Sharing_file::Sharing_file>&sf, std::set<int> &group) {
	std::vector<Sharing_file::Sharing_file> ret;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		if (group.count(itr->eventid) == 1) {
			ret.push_back(*itr);
		}
	}
	printf("event num=%d\n", ret.size());
	return ret;

}