#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
# include <iostream>
# include <random>
#include <set>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <list>
class stop_track {
public:
	uint64_t chainid, groupid;
	int  nseg, npl, pl0, pl1, ph, rawid;
	double ax, ay, x, y, z;
};
class track_pair {
public:
	int pl0, pl1, raw0, raw1;
	double x, y, z, md, oa;
};
class track_multi {
public:
	int eventid, pl;
	double x, y, z;
	std::vector< std::pair<double, stop_track>> trk;
	std::vector<track_pair>pair;
};
void read_vtx_file(std::string filename, std::vector<track_multi>&multi);
std::map<std::pair<int, int>, int>  vtx_count(std::vector<track_multi>&multi);

int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:prg file_in_vtx\n");
		exit(1);
	}
	std::string file_in_vtx = argv[1];
	std::vector<track_multi>multi;
	read_vtx_file(file_in_vtx, multi);

	std::map<std::pair<int, int>, int>count = vtx_count(multi);
	for (auto itr = count.begin(); itr != count.end(); itr++) {
		printf("%d -> %d : %d\n", itr->first.first, itr->first.second, itr->second);
	}
}

void read_vtx_file(std::string filename, std::vector<track_multi>&multi) {
	std::ifstream ifs(filename);
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0, trk_num;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);

		track_multi m;
		m.eventid = std::stoi(str_v[0]);
		m.pl = std::stoi(str_v[1]);
		m.x = std::stod(str_v[3]);
		m.y = std::stod(str_v[4]);
		m.z = std::stod(str_v[5]);
		trk_num = std::stoi(str_v[2]);

		for (int i = 0; i < trk_num*(trk_num - 1) / 2; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			track_pair p;
			p.pl0 = std::stoi(str_v[0]);
			p.raw0 = std::stoi(str_v[1]);
			p.pl1 = std::stoi(str_v[2]);
			p.raw1 = std::stoi(str_v[3]);
			p.x = std::stod(str_v[4]);
			p.y = std::stod(str_v[5]);
			p.z = std::stod(str_v[6]);
			p.oa = std::stod(str_v[7]);
			p.md = std::stod(str_v[8]);
			m.pair.push_back(p);
		}
		for (int i = 0; i < trk_num; i++) {
			std::getline(ifs, str);
			str_v.clear();
			str_v = StringSplit(str);

			stop_track s;
			s.pl0 = std::stoi(str_v[0]);
			s.pl1 = std::stoi(str_v[1]);
			s.rawid = std::stoi(str_v[2]);
			s.chainid = std::stoll(str_v[3]);
			s.groupid = std::stoll(str_v[4]);
			s.nseg = std::stoi(str_v[5]);
			s.npl = std::stoi(str_v[6]);
			s.ph = std::stoi(str_v[7]);
			s.ax = std::stod(str_v[8]);
			s.ay = std::stod(str_v[9]);
			s.x = std::stod(str_v[10]);
			s.y = std::stod(str_v[11]);
			s.z = 0;
			double ip = std::stod(str_v[12]);
			m.pl = s.pl1;

			m.trk.push_back(std::make_pair(ip, s));
		}

		multi.push_back(m);
	}

}
std::map<std::pair<int, int>,int>  vtx_count(std::vector<track_multi>&multi) {
	std::map<std::pair<int, int>, int> ret;
	int num0, num1,pl;
	for (auto itr = multi.begin(); itr != multi.end(); itr++) {
		num0 = 0; 
		num1 = 0;
		pl = itr->pl;
		for (auto itr2 = itr->trk.begin(); itr2 != itr->trk.end(); itr2++) {
			if (itr2->second.pl1 <= pl) {
				num0++;
			}
			else {
				num1++;
			}
		}
		auto res = ret.insert(std::make_pair(std::make_pair(num0, num1), 1));
		if (!res.second)res.first->second++;
	}
	return ret;
}
