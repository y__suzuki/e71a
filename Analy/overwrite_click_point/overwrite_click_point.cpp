#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

//#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <filesystem>
class Parameters {
public:
	double size, xmin, ymin;
	//150 - 944.1 - 245635.1
};
class ClickPionts {
public:
	int circle_id, edge_id;
	double x, y;

};
class Basetrack {
public:
	double x, y, ax, ay;
};

Parameters read_param(std::string filename);
std::map<std::pair<int, int>, ClickPionts> read_click_point(std::string filename);
cv::Mat write_click_point_base(cv::Mat &image, Parameters&param, std::map<std::pair<int, int>, ClickPionts>&p, std::vector<Basetrack>&b);
std::vector<Basetrack> read_event_track(std::string file_path, int pl);

int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "usgae:filename\n");
		exit(1);
	}

	std::string file_input_image = argv[1];
	std::string file_input_param = argv[2];
	std::string file_input_click = argv[3];
	std::string file_input_event = argv[4];
	int event_pl = std::stoi(argv[5]);
	std::string file_output = argv[6];

	//K:\NINJA\E71a\work\suzuki\ManualCheck\check\pl
	if (!std::filesystem::exists(file_input_image)) {
		printf("image not found %s\n", file_input_image.c_str());
		return 1;
	}
	if (!std::filesystem::exists(file_input_param)) {
		printf("param not found %s\n", file_input_param.c_str());
		return 1;
	}
	if (!std::filesystem::exists(file_input_event)) {
		printf("param not found %s\n", file_input_event.c_str());
		return 1;
	}
	if (!std::filesystem::exists(file_input_click)) {
		printf("click data not found %s\n", file_input_click.c_str());
		return 1;
	}


	Parameters param = read_param(file_input_param);
	std::map<std::pair<int, int>, ClickPionts> points = read_click_point(file_input_click);
	std::vector<Basetrack> event_base = read_event_track(file_input_event, event_pl);

	cv::Mat	image = cv::imread(file_input_image, cv::IMREAD_UNCHANGED);
	cv::Mat image_out = write_click_point_base(image, param, points,event_base);

	cv::imwrite(file_output, image_out);
}
Parameters read_param(std::string filename) {
	std::ifstream ifs(filename);
	Parameters param;
	ifs >> param.size >> param.xmin >> param.ymin;
	return param;
}
std::map<std::pair<int, int>, ClickPionts> read_click_point(std::string filename) {
	std::map<std::pair<int, int>, ClickPionts> ret;
	std::ifstream ifs(filename);
	ClickPionts p;
	while (ifs >> p.circle_id >> p.edge_id >> p.x >> p.y) {
		ret.insert(std::make_pair(std::make_pair(p.circle_id,p.edge_id), p));
	}
	return ret;
}


bool getFileNames(std::string folderPath, std::vector<std::string> &file_names)
{
	using namespace std::filesystem;
	directory_iterator iter(folderPath), end;
	std::error_code err;

	for (; iter != end && !err; iter.increment(err)) {
		const directory_entry entry = *iter;
		file_names.push_back(entry.path().string());
	}

	/* �G���[���� */
	if (err) {
		std::cout << err.value() << std::endl;
		std::cout << err.message() << std::endl;
		return false;
	}
	return true;
}
std::vector<std::string> get_eventid(std::string file_path, int pl) {
	std::vector<std::string> filenames;
	getFileNames(file_path, filenames);
	std::vector<std::string> filenames_pl;
	for (auto itr = filenames.begin(); itr != filenames.end(); itr++) {
		if (std::stoi(itr->substr(itr->size() - 29, 3)) == pl) {
			filenames_pl.push_back(*itr);
		}
	}
	return filenames_pl;
}

Basetrack read_basetrack(std::string filename) {

	std::ifstream ifs(filename);
	Basetrack base;
	int pl, rawid, ph0, ph1;
	ifs >> pl >> rawid >> ph0 >> ph1 >> base.ax >> base.ay >> base.x >> base.y;

	return base;
}

std::vector<Basetrack> read_event_track(std::string file_path,int pl) {

	std::vector<std::string> filenames = get_eventid(file_path, pl);

	std::vector<Basetrack> base;
	for (auto itr = filenames.begin(); itr != filenames.end(); itr++) {
		base.push_back(read_basetrack(*itr));
	}
	return base;
}
cv::Mat write_click_point_base(cv::Mat &image, Parameters&param, std::map<std::pair<int, int>, ClickPionts>&p, std::vector<Basetrack>&b) {
	//100x100�̉摜�@CV_8UC3(8bit 3�`�����l��)
	//x
	int cols = image.cols;
	//y
	int rows = image.rows;

	cv::Mat image_out = cv::Mat::zeros(rows, cols, CV_8UC3);

	for (int j = 0; j < rows; j++) {
		for (int i = 0; i < cols; i++) {
			if (image.at<cv::Vec3b>(j, i)[0] != 0) {
				image_out.at<cv::Vec3b>(j, i)[0] = 0xff; //��
				image_out.at<cv::Vec3b>(j, i)[1] = 0xff; //��
				image_out.at<cv::Vec3b>(j, i)[2] = 0xff; //��
			}
		}
	}

	int closs_length = 3;
	int line_width = 3;
	int ix, iy;
	int px, py;
	for (auto itr = p.begin(); itr != p.end(); itr++) {
		px = (itr->second.x - param.xmin) / param.size;
		py = (-1 * itr->second.y - param.ymin) / param.size;
		//pixel --> basetrack
		//(ix+0.5)*size+x_min
		//(iy+0.5)*size+y_min

		for (int i = (closs_length + line_width / 2) * -1; i <= (closs_length + line_width / 2); i++) {
			for (int j = (closs_length + line_width / 2) * -1; j <= (closs_length + line_width / 2); j++) {
				ix = px + i;
				iy = py + j;
				if (ix < 0)continue;
				if (cols <= ix)continue;
				if (iy < 0)continue;
				if (rows <= iy)continue;
				if (abs(ix - px) > line_width / 2 && abs(iy - py) > line_width / 2)continue;
				image_out.at<cv::Vec3b>(iy, ix)[0] = 0x00; //��
				image_out.at<cv::Vec3b>(iy, ix)[1] = 0x00; //��
				image_out.at<cv::Vec3b>(iy, ix)[2] = 0xff; //��


			}
		}


	}

	closs_length = 20;
	line_width = 20;
	for (auto itr = b.begin(); itr != b.end(); itr++) {
		px = (itr->x - param.xmin) / param.size;
		py = (-1 * itr->y - param.ymin) / param.size;
		//pixel --> basetrack
		//(ix+0.5)*size+x_min
		//(iy+0.5)*size+y_min

		for (int i = (closs_length + line_width / 2) * -1; i <= (closs_length + line_width / 2); i++) {
			for (int j = (closs_length + line_width / 2) * -1; j <= (closs_length + line_width / 2); j++) {
				ix = px + i;
				iy = py + j;
				if (ix < 0)continue;
				if (cols <= ix)continue;
				if (iy < 0)continue;
				if (rows <= iy)continue;
				if (abs(ix - px) > line_width / 2 && abs(iy - py) > line_width / 2)continue;
				image_out.at<cv::Vec3b>(iy, ix)[0] = 0xff; //��
				image_out.at<cv::Vec3b>(iy, ix)[1] = 0x00; //��
				image_out.at<cv::Vec3b>(iy, ix)[2] = 0x00; //��


			}
		}


	}

	return image_out;
}
