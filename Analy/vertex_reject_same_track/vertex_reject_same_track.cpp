#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_format {
public:
	int eventid, trackid, pl, rawid, mu_pl, mu_rawid;
	double ax, ay, vph, md, oa, x, y;
};
bool sort_chain_base(const mfile0::M_Base&left, const mfile0::M_Base&right) {
	if (left.pos == right.pos)return left.rawid < right.rawid;
	return left.pos < right.pos;
}

std::vector<mfile0::M_Chain> reject_same_chain(std::vector<mfile0::M_Chain>&chain);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage: prg in-mfile out-mfile\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);
	printf("mfile read fin\n");
	m.chains = reject_same_chain(m.chains);

	mfile0::write_mfile(file_out_mfile, m);
}

mfile0::M_Chain select_best_chain(std::vector<mfile0::M_Chain>&chains) {
	mfile0::M_Chain ret;
	if (chains.size() == 1) {
		ret = chains[0];
	}
	else {
		int nseg_max = 0;
		for (auto &c : chains) {
			nseg_max = std::max(nseg_max, c.nseg);
		}

		std::vector<mfile0::M_Chain>chains2;
		for (auto &c : chains) {
			if (nseg_max == c.nseg) {
				chains2.push_back(c);
			}
		}

		double d_lat_min = -1;
		for (auto &c : chains2) {
			double d_lat, d_rad, ax, ay;
			ax = mfile0::chain_ax(c);
			ay = mfile0::chain_ay(c);
			
			mfile0::angle_diff_dev_theta(c, ax, ay,d_rad, d_lat);
			if (d_lat_min < 0 || d_lat < d_lat_min) {
				d_lat_min = d_lat;
				ret = c;
			}
		}
	}
	return ret;
}

std::vector<mfile0::M_Chain> reject_same_chain(std::vector<mfile0::M_Chain>&chain) {

	std::vector<mfile0::M_Chain> ret;

	std::multimap<int, mfile0::M_Chain> group;
	for (auto &c : chain) {
		group.insert(std::make_pair(c.basetracks.begin()->group_id, c));
	}

	for (auto itr = group.begin(); itr != group.end(); itr++) {
		int g_count = 0;
		g_count = group.count(itr->first);
		int muon_pl;
		
		std::vector<mfile0::M_Chain> event_chian;
		auto range = group.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			event_chian.push_back(res->second);
		}

		for (auto &c : event_chian) {
			if (c.chain_id == 0) {
				ret.push_back(c);
				muon_pl = c.pos1 / 10;
				break;
			}
		}

		std::multimap<std::pair<int, int>, mfile0::M_Chain> chain_base;
		for (auto &c : event_chian) {
			if (c.chain_id == 0)continue;
			if (c.pos1 / 10 <= muon_pl) {
				int up_pl = c.basetracks.rbegin()-> pos/ 10;
				int up_rawid = c.basetracks.rbegin()->rawid;
				chain_base.insert(std::make_pair(std::make_pair(up_pl, up_rawid), c));
			}
			else {
				int down_pl = c.basetracks.begin()->pos / 10;
				int down_rawid = c.basetracks.begin()->rawid;
				chain_base.insert(std::make_pair(std::make_pair(down_pl, down_rawid), c));
			}
		}

		for (auto itr2 = chain_base.begin(); itr2 != chain_base.end(); itr2++) {
			std::vector<mfile0::M_Chain> same_chain;
			auto range = chain_base.equal_range(itr2->first);
			for (auto res = range.first; res != range.second; res++) {
				same_chain.push_back(res->second);
			}
			ret.push_back(select_best_chain(same_chain));


			itr2=std::next(itr2, chain_base.count(itr2->first) - 1);
		}


		itr=std::next(itr, g_count - 1);
	}



	return ret;

}

