#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<vxx::base_track_t> cut_edge_base(std::vector<vxx::base_track_t>&base, double &x_min, double &x_max, double &y_min, double &y_max);
int count_base(double angle, double angle_width, std::vector<vxx::base_track_t>&base);
void output_file(std::string filename, std::vector<vxx::base_track_t>&base, int pl, double area);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-bvxx pl out-file\n");
		exit(1);
	}

	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_out = argv[3];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);
	double x_min, x_max, y_min, y_max;
	base=cut_edge_base(base, x_min, x_max, y_min, y_max);
	//cm2
	double area = (x_max - x_min) / 10000 * (y_max - y_min) / 10000;
	output_file(file_out, base, pl, area);

}
std::vector<vxx::base_track_t> cut_edge_base(std::vector<vxx::base_track_t>&base, double &x_min, double &x_max, double &y_min, double &y_max) {
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			x_min = itr->x;
			x_max = itr->x;
			y_min = itr->y;
			y_max = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		x_max = std::max(itr->x, x_max);
		y_min = std::min(itr->y, y_min);
		y_max = std::max(itr->y, y_max);
	}
	double mergin = 5000;
	x_min = x_min + mergin;
	x_max = x_max - mergin;
	y_min = y_min + mergin;
	y_max = y_max - mergin;
	std::vector<vxx::base_track_t>ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->x < x_min)continue;
		if (itr->x > x_max)continue;
		if (itr->y < y_min)continue;
		if (itr->y > y_max)continue;
		ret.push_back(*itr);
	}
	printf("x:%.1lf cm y:%.1lf cm\n", (x_max - x_min) / 10000, (y_max - y_min) / 10000);
	printf("area %.2lf cm2\n", (x_max - x_min) / 10000 * (y_max - y_min) / 10000);
	return ret;
}
void output_file(std::string filename, std::vector<vxx::base_track_t>&base, int pl, double area) {
	std::ofstream ofs(filename, std::ios::app);

	int count = 0;
	double angle, angle_width;

	angle = 2;
	angle_width = 2;
	ofs << std::right << std::fixed << std::setfill(' ')
		<< std::setw(3) << std::setprecision(0) << pl << " "
		<< std::setw(4) << std::setprecision(2) << angle << " "
		<< std::setw(4) << std::setprecision(2) << angle_width << " "
		<< std::setw(8) << std::setprecision(2) << area << " "
		<< std::setw(10) << std::setprecision(0) << count_base(angle, angle_width, base) << std::endl;
	for (int i = 0; i < 20; i++) {
		angle = i * 0.2 + 0.1;
		angle_width = 0.1;
		ofs << std::right << std::fixed << std::setfill(' ')
			<< std::setw(3) << std::setprecision(0) << pl << " "
			<< std::setw(4) << std::setprecision(2) << angle << " "
			<< std::setw(4) << std::setprecision(2) << angle_width << " "
			<< std::setw(8) << std::setprecision(2) << area << " "
			<< std::setw(10) << std::setprecision(0) << count_base(angle, angle_width, base) << std::endl;

	}


}
int count_base(double angle, double angle_width, std::vector<vxx::base_track_t>&base) {
	int count = 0;
	double ang = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ang = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angle - angle_width <= ang && ang < angle + angle_width)count++;
	}
	return count;
}