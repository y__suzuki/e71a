#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <filesystem>
#include <set>
#include <omp.h>

class manual_check_muon {
public:
	int eventid, pl, rawid, ph0, ph1;
	double ax, ay, x, y;
};

std::vector<manual_check_muon> read_file(std::string filename);
void check(std::string filename, std::vector<mfile0::M_Chain>&c, std::vector<manual_check_muon>&mu);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string in_file_muon = argv[1];
	std::string in_file_base = argv[2];
	std::string out_file = argv[3];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(in_file_muon, m);
	std::vector<manual_check_muon> muon = read_file(in_file_base);
	
	check(out_file, m.chains, muon);

}

std::vector<manual_check_muon> read_file(std::string filename) {
	std::vector<manual_check_muon> ret;
	std::ifstream ifs(filename);
	manual_check_muon t;
	while (ifs >> t.eventid >> t.pl >> t.rawid >> t.ph0 >> t.ph1 >> t.ax >> t.ay >> t.x >> t.y) {
		ret.push_back(t);
	}
	printf("muon size=%d\n", ret.size());
	return ret;
}

void check(std::string filename,std::vector<mfile0::M_Chain>&c, std::vector<manual_check_muon>&mu) {
	std::ofstream ofs(filename);

	for (auto itr = c.begin(); itr != c.end(); itr++) {
		mfile0::M_Base b = *itr->basetracks.rbegin();

		bool flg = false;
		for (auto &base : mu) {
			if (b.pos / 10 == base.pl&&b.rawid == base.rawid) {
				flg = true;
				ofs << b.group_id << " " << base.eventid << " " << b.pos / 10 << " " << b.rawid << std::endl;
			}
		}
		if (flg)continue;
		ofs << b.group_id << " -1 " << b.pos / 10 << " " << b.rawid << std::endl;


	}

}