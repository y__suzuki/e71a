#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output {
public:
	int groupid,chainid,pid;
	double pb, vph,angle;
};
std::vector<output> Calc_inf(std::vector<Momentum_recon::Event_information> &momch);
void outputfile(std::string filename, std::vector<output> &out);
bool judege_iron_penetrate(int pl0, int pl1);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-momch file-out\n");
		exit(1);
	}

	std::string file_in_momch = argv[1];
	std::string file_out = argv[2];
	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);

	std::vector<output> out = Calc_inf(momch);

	outputfile(file_out, out);

}
std::vector<output> Calc_inf(std::vector<Momentum_recon::Event_information> &momch) {
	std::vector<output> ret;
	for (auto &ev : momch) {
		for (auto &c : ev.chains) {
			output out;
			out.chainid = c.chainid;
			out.groupid = ev.groupid;
			out.pb = c.Get_muon_mcs_pb();
			//out.pb = c.ecc_mcs_mom[0];
			if (!isfinite(out.pb))out.pb = -1;
			out.pid = 0;
			out.vph = 0;
			if (c.chainid == 0) {
				out.pid = c.particle_flg;
			}
			int count = 0;
			for (auto &b : c.base) {
				out.vph += b.m[0].ph % 10000;
				out.vph += b.m[1].ph % 10000;
				count += 2;
			}
			out.vph /= count;
			if (c.base.rbegin()->pl <= ev.vertex_pl) {
				out.angle = sqrt(pow(c.base.rbegin()->ax, 2) + pow(c.base.rbegin()->ay, 2));
			}
			else {
				out.angle = sqrt(pow(c.base.begin()->ax, 2) + pow(c.base.begin()->ay, 2));
			}
			if (!judege_iron_penetrate(c.base.begin()->pl, c.base.rbegin()->pl))continue;
			ret.push_back(out);
		}
	}
	return ret;
}
bool judege_iron_penetrate(int pl0,int pl1) {
	int num_film = 0;
	int num_iron = 0;
	int num_water = 0;
	for (int pl = pl0; pl < pl1;pl++){
		num_film++;
		if (pl == 3)continue;
		else if (pl <= 14)num_iron++;
		else if (pl == 15)continue;
		else if (pl % 2 == 0)num_iron++;
		else num_water++;
	}
	return num_iron > 0;

}
void outputfile(std::string filename, std::vector<output> &out) {
	std::ofstream ofs(filename);
	for (auto itr = out.begin(); itr!= out.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(5) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(5) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(5) << std::setprecision(0) << itr->pid << " "
			<< std::setw(7) << std::setprecision(4) << itr->angle << " "
			<< std::setw(8) << std::setprecision(1) << itr->pb << " "
			<< std::setw(6) << std::setprecision(1) << itr->vph << std::endl;
	}


}