#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
class parameter_format {
public:
	double angle_min, angle_max, parameter[2];
};

int judege_sensor_id(int id);
std::map < std::pair<int, int>, std::map<double, parameter_format>> ReadParam(std::string filename);
void vph_correction(std::vector<Momentum_recon::Mom_chain> &momch, std::map < std::pair<int, int>, std::map<double, parameter_format>> &param);


int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-momch file-in-corr file-out-momch\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_corr = argv[2];
	std::string file_out_momch = argv[3];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::map < std::pair<int, int>, std::map<double, parameter_format>> param = ReadParam(file_corr);
	vph_correction(momch, param);
	Momentum_recon::Write_mom_chain_extension(file_out_momch, momch);
	exit(0);
}
std::map < std::pair<int, int>, std::map<double, parameter_format>> ReadParam(std::string filename) {
	std::map < std::pair<int,int>, std::map<double, parameter_format>> ret;
	parameter_format p;
	std::ifstream ifs(filename);
	int pl, face;
	std::pair<int, int> pl_face;
	double mom_min, mom_max, angle_min, angle_max, param[2], error[2],chi2,ndf,chi2_ndf;
	while (ifs >> pl_face.first>>pl_face.second>>mom_min >> mom_max >> angle_min >> angle_max
		>> param[0] >> error[0] >> param[1] >> error[1]>> chi2>> ndf>> chi2_ndf) {
		p.angle_min = angle_min;
		p.angle_max = angle_max;
		p.parameter[0] = param[0];
		p.parameter[1] = param[1];
		auto res = ret.find(pl_face);
		if (res == ret.end()) {
			std::map<double, parameter_format> map_tmp;
			map_tmp.insert(std::make_pair(p.angle_max, p));
			ret.insert(std::make_pair(pl_face, map_tmp));
		}
		else {
			res->second.insert(std::make_pair(p.angle_max, p));
		}
	}
	return ret;

}
void vph_correction(std::vector<Momentum_recon::Mom_chain> &momch, std::map < std::pair<int, int>, std::map<double, parameter_format>> &param) {
	int vph, ph;
	double angle, par[2];
	int count = 0, all = momch.size();
	std::pair<int, int> pl_face;
	for (auto &c : momch) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r VPH correction %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		std::map<std::tuple<int, int, int>, int>after_corr_val;
		for (auto &b : c.base) {
			for (int i = 0; i < 2; i++) {
				pl_face.first = b.pl;
				pl_face.second = i;
				auto res = param.find(pl_face);
				if (res == param.end()) {
					fprintf(stderr, "PL%03d face%d param not found\n", pl_face.first, pl_face.second);
					exit(1);
				}

				vph = b.m[i].ph % 10000;
				ph = b.m[i].ph / 10000;
				angle = sqrt(b.ax*b.ax + b.ay*b.ay);
				auto p = res->second.lower_bound(angle);
				if (p == res->second.end()) {
					p = std::prev(p, 1);
				}
				par[0] = p->second.parameter[0];
				par[1] = p->second.parameter[1];
				//vph = par[0] + vph * par[1];
				vph =  vph * par[1];
				//vphがphのけたをつぶさないように
				vph = std::min(vph, 9999);
				//vphが負の値にならないように
				vph = std::max(vph, 2);
				b.m[i].ph = ph * 10000 + vph;
				after_corr_val.insert(std::make_pair(std::make_tuple(b.pl, b.rawid, i), b.m[i].ph));

			}
		}
		for (auto &pair : c.base_pair) {
			for (int i = 0; i < 2; i++) {
				auto res = after_corr_val.find(std::make_tuple(pair.first.pl, pair.first.rawid, i));
				if (res != after_corr_val.end()) {
					pair.first.m[i].ph = res->second;
				}
				res = after_corr_val.find(std::make_tuple(pair.second.pl, pair.second.rawid, i));
				if (res != after_corr_val.end()) {
					pair.second.m[i].ph = res->second;
				}
			}
		}
	}
	fprintf(stderr, "\r VPH correction %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

}
