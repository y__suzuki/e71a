#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <sstream>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <set>
#include <fstream>

#include <filesystem>
class Film_hole {
public:
	int circle_id, edge_id;
	double x, y;
};
class align_param {
public:
	double a, b, c, d, p, q;
};
class UTS_base {
public:
	double x, y, ax, ay;
};
std::map<std::pair<int, int>, Film_hole > read_click_point(std::string filename);

align_param Cacl_param(std::pair<Film_hole, Film_hole> pair0, std::pair<Film_hole, Film_hole>pair1);
align_param Cacl_param(std::vector<std::pair<Film_hole, Film_hole>> pair); 
void GaussJorden(double in[4][4], double b[4], double c[4]);
bool getFileNames(std::string folderPath, std::vector<std::string> &file_names);
std::set<int> get_eventid(std::string file_path, int pl);
std::string get_filepath(std::string file_path, int pl, int eventid);
void align_param_log(std::string filename, align_param &param);
std::pair<double, double> read_track_position(std::string filename);
void Print_position_take_picutures(int pl, int eventid, std::pair<double, double>&position, align_param &param, double z_face1, double z_face2);
bool output_film_edge_point(std::map<std::pair<int, int>, Film_hole >&film_hole, align_param&param, Film_hole &hole);

int main(int argc, char**argv) {

	align_param param;
	int mode;
	int pl;

	printf("please input film PL\n");
	std::cin >> pl;
	std::cin.clear();
	std::cin.ignore(10000, '\n');

	printf("param input(0) or calculation(1)\n");
	std::cin >> mode;
	std::cin.clear();
	std::cin.ignore(10000, '\n');
	printf("-----------------------------------------------------\n\n");

	std::map<std::pair<int, int>, Film_hole> film_hole;

	if (mode == 0) {
		printf("please input parameter\n");

		std::cin >> param.a >> param.b >> param.c >> param.d >> param.p >> param.q;
		std::cin.clear();
		std::cin.ignore(10000, '\n');

	}
	if (mode == 1) {
		bool flg = true;

		while (flg) {
			//hole edgeの座標読み込み
			std::stringstream file_film_edge;
			file_film_edge << "K:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\FilmEdge\\PL"
				<<std::setw(3) << std::setfill('0') << pl << "_edge.txt";
			if (!std::filesystem::exists(file_film_edge.str())) {
				printf("file not found %s\n", file_film_edge.str().c_str());
				continue;
			}
			film_hole = read_click_point(file_film_edge.str());
			if (film_hole.size() >= 2) {
				flg = false;
			}
			else {
				printf("click point data low\n");
				printf("data size = %d\n", film_hole.size());
			}

		}

		std::vector<std::pair<Film_hole, Film_hole>> film_hole_pair;
		std::map<std::pair<int, int>, Film_hole> film_hole_uts;
		flg = true;
		while (flg) {
			//測定値の入力
			Film_hole hole;
			double z;
			printf("please input film edge hole position measured by UTS\n");
			printf("circle_id, edge_id x y z(not use)\n");
			printf("circle_id\n");
			printf("id=0 left-top / id=1 left-bottom / id=2 right-top / id=3 right-bottom\n");
			printf("edge_id\n");
			printf("id=0 top / id=1 left / id=2 bottom / id=3 right\n");
			std::cin >> hole.circle_id >> hole.edge_id >> hole.x >> hole.y >> z;
			std::cin.clear();
			std::cin.ignore(10000, '\n');
			printf("-----------------------------------------------------\n\n");

			//対応する穴の位置を探索
			if (film_hole.count(std::make_pair(hole.circle_id, hole.edge_id)) == 0) {
				printf("this point not measured\n");
				continue;
			}
			auto res = film_hole_uts.insert(std::make_pair(std::make_pair(hole.circle_id, hole.edge_id), hole));
			if (!res.second) {
				res.first->second.x = hole.x;
				res.first->second.y = hole.y;
			}

			//パラメータの計算
			film_hole_pair.clear();
			int count = 0;
			for (auto itr = film_hole_uts.begin(); itr != film_hole_uts.end(); itr++) {
				if (film_hole.count(itr->first) == 0)continue;
				auto res = film_hole.find(itr->first);
				film_hole_pair.push_back(std::make_pair(itr->second, res->second));
				count++;
			}
			if (count >= 2) {
				if (count == 2) {
					//入力値2個
					//パラメータは一意に決定
					param = Cacl_param(film_hole_pair[0], film_hole_pair[1]);

				}
				else {
					//入力値3個以上
					//ベストなパラメータを計算
					param = Cacl_param(film_hole_pair);
				}
				printf("now parameter\n");
				printf("%g\n", param.a);
				printf("%g\n", param.b);
				printf("%g\n", param.c);
				printf("%g\n", param.d);
				printf("%g\n", param.p);
				printf("%g\n", param.q);
				printf("\n");
				printf("add point?\n");
				printf("yes = 1 / no = 0\n");
				bool continue_flg;
				std::cin >> continue_flg;
				std::cin.clear();
				std::cin.ignore(10000, '\n');
				printf("-----------------------------------------------------\n\n");

				if (!continue_flg) {
					flg = false;
					break;
				}
				continue_flg = output_film_edge_point(film_hole, param, hole);
			}
		}
	}
	printf("%g\n", param.a);
	printf("%g\n", param.b);
	printf("%g\n", param.c);
	printf("%g\n", param.d);
	printf("%g\n", param.p);
	printf("%g\n", param.q);
	printf("\n");

	bool flg = true;
	while (flg) {
		printf("please input mode\n");
		printf("mode =-1: colse program\n");
		printf("mode = 0: ECC --> UTS\n");
		printf("mode = 1: UTS --> ECC\n");
		printf("mode = 2: parameter fine tune\n");
		printf("mode = 3: hole position output(alignment check)\n");
		printf("mode = 4: scan position output\n");
		std::cin >> mode;
		std::cin.clear();
		std::cin.ignore(10000, '\n');
		printf("-----------------------------------------------------\n\n");

		if (mode == -1)flg = false;
		else if (mode == 0) {
			double x, y;
			double tmp_x, tmp_y;

			printf("input ECC coordiatie\n");
			printf("x y\n");
			std::cin >> tmp_x >> tmp_y;
			std::cin.clear();
			std::cin.ignore(10000, '\n');

			x = tmp_x * param.a + tmp_y * param.b + param.p;
			y = tmp_x * param.c + tmp_y * param.d + param.q;

			printf("UTS coordiate\n");
			printf("%g %g\n", x, y);
		}
		else if (mode == 1) {
			double x, y;
			double tmp_x, tmp_y;

			printf("input UTS coordiatie\n");
			printf("x y\n");
			std::cin >> tmp_x >> tmp_y;
			std::cin.clear();
			std::cin.ignore(10000, '\n');

			tmp_x = tmp_x - param.p;
			tmp_y = tmp_y - param.q;
			double factor = param.a*param.d - param.b*param.c;
			factor = 1 / factor;

			x = factor * (param.d*tmp_x - param.c*tmp_y);
			y = factor * (-1*param.b*tmp_x + param.a*tmp_y);

			printf("ECC coordiate\n");
			printf("%g %g\n", x, y);
		}
		else if (mode == 2) {
			printf("Unimplemented\n");
		}
		else if (mode == 3) {
			printf("please input film edge hole id\n");
			printf("circle_id, edge_id\n");
			printf("circle_id\n");
			printf("id=0 left-top / id=1 left-bottom / id=2 right-top / id=3 right-bottom x y\n");
			printf("edge_id\n");
			printf("id=0 top / id=1 left / id=2 bottom / id=3 right\n");
			int circle_id, edge_id;
			std::cin >> circle_id >> edge_id;
			std::cin.clear();
			std::cin.ignore(10000, '\n');
			printf("-----------------------------------------------------\n\n");

			auto res = film_hole.find(std::make_pair(circle_id, edge_id));
			if (res == film_hole.end()) {
				printf("%d %d not found\n", circle_id, edge_id);
				continue;
			}
			double x, y;

			x = res->second.x * param.a + res->second.y* param.b + param.p;
			y = res->second.x * param.c + res->second.y* param.d + param.q;

			printf("UTS coordiate\n");
			printf("%.4lf %.4lf\n", x, y);

		}
		else if (mode == 4) {
			

			std::string file_path = "K:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\check\\pl";
			std::set<int> eventidlist = get_eventid(file_path, pl);
			printf("------------------\n");
			printf("eventid list\n");
			for (auto itr = eventidlist.begin(); itr != eventidlist.end(); itr++) {
				printf("\t %d\n", *itr);
			}
			printf("------------------\n");

			int eventid;
			printf("input eventid\n");
			std::cin >> eventid;
			std::cin.clear();
			std::cin.ignore(10000, '\n');
			printf("-----------------------------------------------------\n\n");

			if (eventidlist.count(eventid) == 0) {
				printf("event id =%d not found\n", eventid);
				continue;
			}
			//event fileの読みこみ
			std::string file_in_event = get_filepath(file_path, pl, eventid);
			std::pair<double, double> position = read_track_position(file_in_event);

			//変換parameterなどのログ
			std::stringstream file_param_log;
			file_param_log << "K:\\NINJA\\E71a\\work\\suzuki\\ManualCheck\\image\\"
				<< std::setw(3) << std::setfill('0') << pl << "_"
				<< std::setw(5) << std::setfill('0') << eventid << "_param.txt";
			printf("%s\n", file_param_log.str().c_str());
			if (std::filesystem::exists(file_param_log.str())) {
				printf("PL%03d event %d already exist\n", pl, eventid);
				printf("overwrite file?\n");
				printf("yes 1 / no 0\n");
				bool flg_file_out = false;
				std::cin >> flg_file_out;
				std::cin.clear();
				std::cin.ignore(10000, '\n');
				printf("-----------------------------------------------------\n\n");

				if (!flg_file_out) {
					continue;
				}
			}
			//parameter の記録
			align_param_log(file_param_log.str(), param);

			printf("PL%03d event %d view center\n", pl, eventid);
			printf("%.4lf %.4lf\n\n",
				position.first*param.a + position.second*param.b + param.p,
				position.first*param.c + position.second*param.d + param.q
				);

			double z_face1_bottom,z_face1_top, z_face2_bottom, z_face2_top;
			printf("input face1-z(bottom) face1-z(top) face2-z(bottom) face2-z(top)\n");
			std::cin >> z_face1_bottom >> z_face1_top >> z_face2_bottom >> z_face2_top;
			std::cin.clear();
			std::cin.ignore(10000, '\n');
			printf("-----------------------------------------------------\n\n");
			printf("small z value\n");
			printf("Z   |====================================|<-- %9.4lf\n", z_face2_top);
			printf("↓  |************************************|\n");
			printf("↓  |********* emulsion  gel *********+**|\n");
			printf("↓  |*********************************+**|\n");
			printf("↓  |------------------------------------|<-- %9.4lf\n", z_face2_bottom);
			printf("↓  |                                    |\n");
			printf("↓  |                                    |\n");
			printf("↓  |           base                     |\n");
			printf("↓  |                                    |\n");
			printf("↓  |                                    |\n");
			printf("↓  |------------------------------------|<-- %9.4lf\n", z_face1_top);
			printf("↓  |************************************|\n");
			printf("↓  |********* emulsion  gel ************|\n");
			printf("↓  |************************************|\n");
			printf("↓  |====================================|<-- %9.4lf\n", z_face1_bottom);
			printf("large z value\n");
			if (z_face2_top > z_face2_bottom || z_face2_top > z_face1_bottom || z_face2_top > z_face1_top
				|| z_face2_bottom > z_face1_top || z_face2_bottom > z_face1_bottom || z_face1_top > z_face1_bottom) {
				printf("input value error\n");
			}
			else {
				double z_face1, z_face2;
				z_face1 = (z_face1_top + z_face1_bottom) / 2 + 0.050;
				z_face2 = (z_face2_top + z_face2_bottom) / 2 + 0.050;
				Print_position_take_picutures(pl, eventid, position, param, z_face1, z_face2);
			}
		}
		else {

		}
	}
}



std::map<std::pair<int, int>, Film_hole > read_click_point(std::string filename) {
	std::map<std::pair<int, int>, Film_hole > ret;
	std::ifstream ifs(filename);
	Film_hole  p;
	while (ifs >> p.circle_id >> p.edge_id >> p.x >> p.y) {
		ret.insert(std::make_pair(std::make_pair(p.circle_id, p.edge_id), p));
	}
	return ret;
}


align_param Cacl_param(std::pair<Film_hole, Film_hole> pair0, std::pair<Film_hole, Film_hole>pair1) {
	std::pair<double, double> a0, a1, b0, b1;
	b0.first = pair0.first.x;
	b0.second = pair0.first.y;
	b1.first = pair1.first.x;
	b1.second = pair1.first.y;

	a0.first = pair0.second.x;
	a0.second = pair0.second.y;
	a1.first = pair1.second.x;
	a1.second = pair1.second.y;

	double in[4][4] = {
		{a0.first, -1 * a0.second,1,0},
		{a0.second,  a0.first,0,1},
		{a1.first, -1 * a1.second,1,0},
		{a1.second,  a1.first,0,1}
	};
	double b[4] = { b0.first,b0.second,b1.first,b1.second };
	double c[4];

	GaussJorden(in, b, c);
	align_param ret;
	ret.a = c[0];
	ret.b = -1 * c[1];
	ret.c = c[1];
	ret.d = c[0];
	ret.p = c[2];
	ret.q = c[3];
	return ret;

}

align_param Cacl_param(std::vector<std::pair<Film_hole, Film_hole>> pair) {
	double n, xa2, ya2, xa, ya, xaxb, yayb, xayb, xbya, xb, yb;
	n = 0;
	xa2 = 0;
	ya2 = 0;
	xa = 0;
	ya = 0;
	xaxb = 0;
	yayb = 0;
	xayb = 0;
	xbya = 0;
	xb = 0;
	yb = 0;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		auto pa = itr->second;
		auto pb = itr->first;

		xa += pa.x;
		ya += pa.y;
		xa2 += pa.x*pa.x;
		ya2 += pa.y*pa.y;
		xaxb += pa.x*pb.x;
		yayb += pa.y*pb.y;
		xayb += pa.x*pb.y;
		xbya += pb.x*pa.y;
		xb += pb.x;
		yb += pb.y;
		n += 1;
	}
	double in[4][4] = {
		{xa2 + ya2,0,xa,ya},
		{0,xa2 + ya2,-1 * ya,xa},
		{xa,-1 * ya,n,0},
		{ya,xa,0,n	}
	};
	double b[4] = {
		xaxb + yayb,xayb - xbya,xb,yb
	};
	double res[4] = {};
	GaussJorden(in, b, res);
	align_param ret;
	ret.a = res[0];
	ret.b = -1 * res[1];
	ret.c = res[1];
	ret.d = res[0];
	ret.p = res[2];
	ret.q = res[3];

	int count = 0;
	double dx_mean = 0, dx_sigma = 0, dy_mean = 0, dy_sigma = 0, dx, dy;

	printf("\n***************************************\n");
	printf("output result\n");
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		auto pa = itr->second;
		auto pb = itr->first;
		dx = pb.x - (pa.x*ret.a + pa.y*ret.b + ret.p);
		dy = pb.y - (pa.x*ret.c + pa.y*ret.d + ret.q);
		dx_mean += dx;
		dy_mean += dy;
		dx_sigma += dx * dx;
		dy_sigma += dy * dy;
		count++;
		printf("%d %d dx = %7.1lf[μm] dy = %7.1lf[μm]\n", pa.circle_id, pa.edge_id, dx * 1000, dy * 1000);
	}
	dx_mean = dx_mean / count;
	dy_mean = dy_mean / count;
	dx_sigma = sqrt(dx_sigma / count - dx_mean * dx_mean);
	dy_sigma = sqrt(dy_sigma / count - dy_mean * dy_mean);
	printf("diff mean   x = %7.1lf[μm], y = %7.1lf[μm]\n", dx_mean * 1000, dy_mean * 1000);
	printf("diff sigma  x = %7.1lf[μm], y = %7.1lf[μm]\n", dx_sigma * 1000, dy_sigma * 1000);
	printf("accuracy    x = %7.1lf[μm], y = %7.1lf[μm]\n", dx_sigma / sqrt(count - 2) * 1000, dy_sigma / sqrt(count - 2) * 1000);
	printf("\n***************************************\n");

	return ret;
}

bool output_film_edge_point(std::map<std::pair<int, int>, Film_hole >&film_hole, align_param&param, Film_hole &hole) {
	;
	printf("please input film edge hole ID\n");
	printf("circle_id, edge_id\n");
	printf("circle_id\n");
	printf("id=0 left-top / id=1 left-bottom / id=2 right-top / id=3 right-bottom\n");
	printf("edge_id\n");
	printf("id=0 top / id=1 left / id=2 bottom / id=3 right\n");
	std::cin >> hole.circle_id >> hole.edge_id;
	std::cin.clear();
	std::cin.ignore(10000, '\n');
	printf("-----------------------------------------------------\n\n");

	//対応する穴の位置を探索
	if (film_hole.count(std::make_pair(hole.circle_id, hole.edge_id)) == 0) {
		printf("this point not measured\n");
		return false;
	}
	double x, y;
	auto res = film_hole.find(std::make_pair(hole.circle_id, hole.edge_id));
	x = param.a*res->second.x + param.b*res->second.y + param.p;
	y = param.c*res->second.x + param.d*res->second.y + param.q;
	printf("\n");
	printf("::::::::::::::::::\n\n");
	printf("%d %d %.4lf %.4lf\n\n", hole.circle_id, hole.edge_id, x, y);
	printf("::::::::::::::::::\n\n");

	return true;
}


void GaussJorden(double in[4][4], double b[4], double c[4]) {


	double a[4][5];
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 5; j++) {
			if (j < 4) {
				a[i][j] = in[i][j];
			}
			else {
				a[i][j] = b[i];
			}
		}
	}
	int N = 4;
	double p, d;         // ピボット係数、ピボット行ｘ係数
	double max, dummy;   // 最大絶対値、入れ替え時ダミー
	int s;

	//元の連立方程式をコンソール出力
   //for (int i = 0; i < N; i++) {
   //	for (int j = 0; j < N; j++)
   //		printf("%+fx%d ", a[i][j], j + 1);
   //	printf("= %+f\n", a[i][N]);
   //}

	for (int k = 0; k < N; k++) {
		// 行入れ替え
		max = 0; s = k;
		for (int j = k; j < N; j++) {
			if (fabs(a[j][k]) > max) {
				max = fabs(a[j][k]);
				s = j;
			}
		}
		if (max == 0) {
			printf("解けない！");
			exit(1);
		}
		for (int j = 0; j <= N; j++) {
			dummy = a[k][j];
			a[k][j] = a[s][j];
			a[s][j] = dummy;
		}

		// ピボット係数
		p = a[k][k];

		// ピボット行を p で除算
		for (int j = k; j < N + 1; j++)
			a[k][j] /= p;

		// ピボット列の掃き出し
		for (int i = 0; i < N; i++) {
			if (i != k) {
				d = a[i][k];
				for (int j = k; j < N + 1; j++)
					a[i][j] -= d * a[k][j];
			}
		}
	}

	// 結果出力
	for (int k = 0; k < N; k++) {
		c[k] = a[k][N];
		//printf("x%d = %f\n", k + 1, a[k][N]);
	}
}

std::string get_filepath(std::string file_path, int pl, int eventid) {
	std::set<int> ret;
	std::vector<std::string> filenames;
	getFileNames(file_path, filenames);
	std::vector<std::string> filenames_pl;
	std::string filename_event;
	for (auto itr = filenames.begin(); itr != filenames.end(); itr++) {
		if (std::stoi(itr->substr(itr->size() - 29, 3)) == pl) {
			filenames_pl.push_back(*itr);
		}
	}
	for (auto itr = filenames_pl.begin(); itr != filenames_pl.end(); itr++) {
		if (std::stoi(itr->substr(itr->size() - 23, 5)) == eventid) {
			filename_event = *itr;
		}
	}
	return filename_event;

}
std::set<int> get_eventid(std::string file_path,int pl) {
	std::set<int> ret;
	std::vector<std::string> filenames;
	getFileNames(file_path, filenames);
	std::vector<std::string> filenames_pl;
	for (auto itr = filenames.begin(); itr != filenames.end(); itr++) {
		if (std::stoi(itr->substr(itr->size() - 29, 3)) == pl) {
			filenames_pl.push_back(*itr);
		}
	}
	for (auto itr = filenames_pl.begin(); itr != filenames_pl.end(); itr++) {
		ret.insert(std::stoi(itr->substr(itr->size() - 23, 5)));
	}
	return ret;
}
bool getFileNames(std::string folderPath, std::vector<std::string> &file_names)
{
	using namespace std::filesystem;
	directory_iterator iter(folderPath), end;
	std::error_code err;

	for (; iter != end && !err; iter.increment(err)) {
		const directory_entry entry = *iter;
		file_names.push_back(entry.path().string());
	}

	/* エラー処理 */
	if (err) {
		std::cout << err.value() << std::endl;
		std::cout << err.message() << std::endl;
		return false;
	}
	return true;
}

void align_param_log(std::string filename, align_param &param) {
	std::ofstream ofs(filename);
	ofs << std::right << std::fixed
		<< std::setw(14) << std::setprecision(10) << param.a << " "
		<< std::setw(14) << std::setprecision(10) << param.b << " "
		<< std::setw(14) << std::setprecision(10) << param.c << " "
		<< std::setw(14) << std::setprecision(10) << param.d << " "
		<< std::setw(14) << std::setprecision(4) << param.p << " "
		<< std::setw(14) << std::setprecision(4) << param.q << std::endl;

}

std::pair<double, double> read_track_position(std::string filename) {
	std::ifstream ifs(filename);
	int pl, rawid, ph0, ph1;
	double ax, ay, x, y;

	ifs >> pl >> rawid >> ph0 >> ph1 >> ax >> ay >> x >> y;
	return std::make_pair(x, y);
	//4    4391350   90011   90005 - 0.0376 - 0.6286 181200.0 110141.2

}
void Print_position_take_picutures(int pl, int eventid, std::pair<double, double>&position, align_param &param, double z_face1, double z_face2) {
	double x, y;
	x = position.first*param.a + position.second*param.b + param.p;
	y = position.first*param.c + position.second*param.d + param.q;

	std::vector<std::pair<int, int>> pos_id;
	for (int iy = 0; iy < 5; iy++) {
		for (int ix = 0; ix < 5; ix++) {
			pos_id.push_back(std::make_pair(ix, iy));
		}
	}
	//駆動:回転
	/*
	pos_id.push_back(std::make_pair(2, 2));
	pos_id.push_back(std::make_pair(2, 1));
	pos_id.push_back(std::make_pair(1, 1));
	pos_id.push_back(std::make_pair(1, 2));
	pos_id.push_back(std::make_pair(1, 3));
	pos_id.push_back(std::make_pair(2, 3));
	pos_id.push_back(std::make_pair(3, 3));
	pos_id.push_back(std::make_pair(3, 2));
	pos_id.push_back(std::make_pair(3, 1));
	pos_id.push_back(std::make_pair(3, 0));
	pos_id.push_back(std::make_pair(2, 0));
	pos_id.push_back(std::make_pair(1, 0));
	pos_id.push_back(std::make_pair(0, 0));
	pos_id.push_back(std::make_pair(0, 1));
	pos_id.push_back(std::make_pair(0, 2));
	pos_id.push_back(std::make_pair(0, 3));
	pos_id.push_back(std::make_pair(0, 4));
	pos_id.push_back(std::make_pair(1, 4));
	pos_id.push_back(std::make_pair(2, 4));
	pos_id.push_back(std::make_pair(3, 4));
	pos_id.push_back(std::make_pair(4, 4));
	pos_id.push_back(std::make_pair(4, 3));
	pos_id.push_back(std::make_pair(4, 2));
	pos_id.push_back(std::make_pair(4, 1));
	pos_id.push_back(std::make_pair(4, 0));
	*/
	char output_name_tmp[1024];
	std::vector<std::string> output_string;
	for (int i = 0; i < pos_id.size(); i++) {
		//xのid=0
		if (pos_id[i].first == 0) {
			//yのid=0
			if (pos_id[i].second == 0) {
				std::stringstream filename;
				filename << "ev" << std::setw(5) << std::setfill('0') << eventid << "\\PL"
					<< std::setw(3) << std::setfill('0') << pl << "\\face1__1__1"
					<< std::setw(1) << std::setfill('0') << pos_id[i].second;

				sprintf(output_name_tmp, "%.4lf %.4lf %.4lf %s",
					x + (pos_id[i].first - 3)*0.2,
					y + (pos_id[i].second - 3)*0.2,
					z_face1,
					filename.str().c_str()
				);
			}
			std::stringstream filename;
			filename << "ev" << std::setw(5) << std::setfill('0') << eventid << "\\PL"
				<< std::setw(3) << std::setfill('0') << pl << "\\face1__1_"
				<< std::setw(1) << std::setfill('0') << pos_id[i].second;

			sprintf(output_name_tmp, "%.4lf %.4lf %.4lf %s",
				x + (pos_id[i].first - 3)*0.2,
				y + (pos_id[i].second - 2)*0.2,
				z_face1,
				filename.str().c_str()
			);

		}

		std::stringstream filename;
		filename << "ev" << std::setw(5) << std::setfill('0') << eventid << "\\PL"
			<< std::setw(3) << std::setfill('0') << pl << "\\face1_"
			<< std::setw(1) << std::setfill('0') << pos_id[i].first << "_"
			<< std::setw(1) << std::setfill('0') << pos_id[i].second;
		
		sprintf(output_name_tmp,"%.4lf %.4lf %.4lf %s",
			x + (pos_id[i].first - 2)*0.2,
			y + (pos_id[i].second - 2)*0.2,
			z_face1,
			filename.str().c_str()
		);
		output_string.push_back(output_name_tmp);
	}
	for (int i = 0; i < pos_id.size(); i++) {
		//xのid=0
		if (pos_id[i].first == 0) {
			//yのid = 0
				if (pos_id[i].second == 0) {
					std::stringstream filename;
					filename << "ev" << std::setw(5) << std::setfill('0') << eventid << "\\PL"
						<< std::setw(3) << std::setfill('0') << pl << "\\face2__1__1"
						<< std::setw(1) << std::setfill('0') << pos_id[i].second;

					sprintf(output_name_tmp, "%.4lf %.4lf %.4lf %s",
						x + (pos_id[i].first - 3)*0.2,
						y + (pos_id[i].second - 3)*0.2,
						z_face2,
						filename.str().c_str()
					);
				}
			std::stringstream filename;
			filename << "ev" << std::setw(5) << std::setfill('0') << eventid << "\\PL"
				<< std::setw(3) << std::setfill('0') << pl << "\\face2__1_"
				<< std::setw(1) << std::setfill('0') << pos_id[i].second;

			sprintf(output_name_tmp, "%.4lf %.4lf %.4lf %s",
				x + (pos_id[i].first - 3)*0.2,
				y + (pos_id[i].second - 2)*0.2,
				z_face2,
				filename.str().c_str()
			);

		}

		std::stringstream filename;
		filename << "ev" << std::setw(5) << std::setfill('0') << eventid << "\\PL"
			<< std::setw(3) << std::setfill('0') << pl << "\\face2_"
			<< std::setw(1) << std::setfill('0') << pos_id[i].first << "_"
			<< std::setw(1) << std::setfill('0') << pos_id[i].second;

		sprintf(output_name_tmp,"%.4lf %.4lf %.4lf %s",
			x + (pos_id[i].first - 2)*0.2,
			y + (pos_id[i].second - 2)*0.2,
			z_face2,
			filename.str().c_str()
		);
		output_string.push_back(output_name_tmp);
	}

	for (int i = 0; i < output_string.size(); i++) {
		if (i % 25 == 0) {
			printf("\n");
		}
		printf("%s\n", output_string[i].c_str());
	}
}