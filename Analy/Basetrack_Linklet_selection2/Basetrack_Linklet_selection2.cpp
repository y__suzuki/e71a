#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib,"functions.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"
#include <functions.hpp>

#include <set>
#include <filesystem>
class linklet_cut {
public:
	double radial_angle_intercept, radial_angle_slope, radial_position_intercept, radial_position_slope;
	double lateral_angle, lateral_position;
};

class Linklet_rl {
public:
	netscan::linklet_t link;
	double dr, dl, md, oa;
};

std::vector<vxx::base_track_t> basetrack_selection(std::vector<vxx::base_track_t>&base, std::vector<Linklet_rl> &link, int pl);
std::vector<Linklet_rl> Radial_Laterai_information_addition(std::vector<netscan::linklet_t> link);
void position_difference(netscan::linklet_t link, double &dr, double &dl);
void Calc_MD_OA(netscan::linklet_t link, double &md, double &oa);
std::vector<Linklet_rl> linklet_selection(std::vector<Linklet_rl> &link);
linklet_cut linklet_cut_param(int pl0, int pl1);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg in-ECC-folder out-bvxx-folder\n");
		exit(1);
	}



	std::string file_in_ECC = argv[1];
	std::string file_out_bvxx_path = argv[2];

	for (int PL = 1; PL <= 133; PL++) {

		//if (PL != 30)continue;


		std::stringstream file_in_bvxx, file_in_link0, file_in_link1;
		file_in_bvxx << file_in_ECC << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << PL
			<< "\\b" << std::setw(3) << std::setfill('0') << PL << ".sel.cor.vxx";
		file_in_link0 << file_in_ECC << "\\Area0\\0\\linklet\\l-" << std::setw(3) << std::setfill('0') << PL - 1
			<< "-" << std::setw(3) << std::setfill('0') << PL << ".sel.bll";
		file_in_link1 << file_in_ECC << "\\Area0\\0\\linklet\\l-" << std::setw(3) << std::setfill('0') << PL
			<< "-" << std::setw(3) << std::setfill('0') << PL + 1 << ".sel.bll";
		if (!std::filesystem::exists(file_in_bvxx.str()) || !std::filesystem::exists(file_in_link0.str()) || !std::filesystem::exists(file_in_link1.str())) {
			printf("PL%03d file not found\n", PL);
			continue;
		}
		vxx::BvxxReader br;
		std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx.str(), PL, 0);
		std::vector<netscan::linklet_t> link0, link1;
		netscan::read_linklet_bin(file_in_link0.str(), link0);
		netscan::read_linklet_bin(file_in_link1.str(), link1);
		std::vector<Linklet_rl> link0_rl = Radial_Laterai_information_addition(link0);
		std::vector<Linklet_rl> link1_rl = Radial_Laterai_information_addition(link1);

		link0_rl = linklet_selection(link0_rl);
		link1_rl = linklet_selection(link1_rl);

		base = basetrack_selection(base, link0_rl, PL);
		base = basetrack_selection(base, link1_rl, PL);

		std::stringstream file_out_bvxx;
		file_out_bvxx<<file_out_bvxx_path << "\\b" << std::setw(3) << std::setfill('0') << PL << ".sel.cor.vxx";
		vxx::BvxxWriter bw;
		bw.Write(file_out_bvxx.str(), PL, 0, base);
	}
}


std::vector<vxx::base_track_t> basetrack_selection(std::vector<vxx::base_track_t>&base, std::vector<Linklet_rl> &link, int pl) {
	int id = -1;
	if (link.begin()->link.b[0].pl == pl)id = 0;
	else if (link.begin()->link.b[1].pl == pl)id = 1;
	else {
		fprintf(stderr, "linklet PL%03d not found\n", pl);
		exit(1);
	}

	std::vector<vxx::base_track_t> ret;
	std::set<int> select;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		select.insert(itr->link.b[id].rawid);
	}
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (select.count(itr->rawid) == 1) {
			ret.push_back(*itr);
		}
	}
	fprintf(stderr, "selected basetrack %d --> %d\n", base.size(), ret.size());
	return ret;

}
std::vector<Linklet_rl> linklet_selection(std::vector<Linklet_rl> &link) {

	int pl0 = link.begin()->link.b[0].pl;
	int pl1 = link.begin()->link.b[1].pl;
	linklet_cut cut_param = linklet_cut_param(pl0, pl1);
	std::vector<Linklet_rl>ret;
	double angle, d_ang_r, d_ang_l;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		angle = sqrt(itr->link.b[0].ax*itr->link.b[0].ax + itr->link.b[0].ay*itr->link.b[0].ay);
		if (fabs(itr->dl) > cut_param.lateral_position)continue;
		if (fabs(itr->dr) > cut_param.radial_position_intercept+cut_param.radial_position_slope*angle)continue;

		d_ang_r = (itr->link.b[1].ax - itr->link.b[0].ax)*itr->link.b[0].ax + (itr->link.b[1].ay - itr->link.b[0].ay)*itr->link.b[0].ay;
		d_ang_l = (itr->link.b[1].ax - itr->link.b[0].ax)*itr->link.b[0].ay - (itr->link.b[1].ay - itr->link.b[0].ay)*itr->link.b[0].ax;
		if (fabs(d_ang_l) > cut_param.lateral_angle*angle)continue;
		if (fabs(d_ang_r) > (cut_param.radial_angle_intercept + cut_param.radial_angle_slope*angle)*angle)continue;

		ret.push_back(*itr);
	}
	fprintf(stderr, "selected linklet %d --> %d\n", link.size(), ret.size());

	return ret;
}
std::vector<Linklet_rl> Radial_Laterai_information_addition(std::vector<netscan::linklet_t> link) {
	std::vector<Linklet_rl> ret;
	int count = 0;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r calculation ... %d/%d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;
		Linklet_rl tmp_link;
		tmp_link.link = *itr;
		position_difference(*itr, tmp_link.dr, tmp_link.dl);
		Calc_MD_OA(*itr, tmp_link.md, tmp_link.oa);
		ret.push_back(tmp_link);
	}
	fprintf(stderr, "\r calculation ... %d/%d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

	return ret;

}
void position_difference(netscan::linklet_t link, double &dr, double &dl) {
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = link.b[0].x;
	pos0.y = link.b[0].y;
	pos0.z = link.b[0].z;
	dir0.x = link.b[0].ax;
	dir0.y = link.b[0].ay;
	dir0.z = 1;
	pos1.x = link.b[1].x;
	pos1.y = link.b[1].y;
	pos1.z = link.b[1].z;
	dir1.x = link.b[1].ax;
	dir1.y = link.b[1].ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);

}
void Calc_MD_OA(netscan::linklet_t link, double &md, double &oa) {
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = link.b[0].x;
	pos0.y = link.b[0].y;
	pos0.z = link.b[0].z;
	dir0.x = link.b[0].ax;
	dir0.y = link.b[0].ay;
	dir0.z = 1;
	pos1.x = link.b[1].x;
	pos1.y = link.b[1].y;
	pos1.z = link.b[1].z;
	dir1.x = link.b[1].ax;
	dir1.y = link.b[1].ay;
	dir1.z = 1;

	oa = opening_angle(dir0, dir1);
	double z_range[2], extra[2];
	z_range[0] = pos0.z;
	z_range[1] = pos1.z;
	md = minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);

}

linklet_cut linklet_cut_param(int pl0, int pl1) {
	linklet_cut ret;
	if (pl0 <= 15||pl0%2==0) {

		ret.radial_angle_intercept = 0.05;
		ret.radial_angle_slope = 0.05;
		ret.radial_position_intercept = 5;
		ret.radial_position_slope = 5;
		ret.lateral_angle = 0.01;
		ret.lateral_position = 10;
	}
	else {
		ret.radial_angle_intercept = 0.05;
		ret.radial_angle_slope = 0.05;
		ret.radial_position_intercept = 20;
		ret.radial_position_slope = 20;
		ret.lateral_angle = 0.015;
		ret.lateral_position = 30;
	}
	return ret;
}