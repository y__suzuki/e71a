#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

class Sharing_file {
public:
	int32_t pl, ecc_id, fixedwall_id, trackerwall_id, spotid, zone[2], rawid[2], unix_time, tracker_track_id;
	//spotid:spotA * 100 + spotB
};

class Chain_baselist {
public:
	int64_t groupid;
	std::set<std::pair<int, int>>rawid;
	Sharing_file sf;
};
std::vector<Chain_baselist> read_sharing_file(std::string filename, int mode, std::string file_in_ECC, int link_sel_mode);
std::vector<std::pair<Sharing_file, vxx::base_track_t>> sharing2base(std::vector<Sharing_file> &track, std::string file_in_ECC, std::vector<netscan::linklet_t> &link);
std::map<std::tuple<int, int, int, int>, Sharing_file> pl4_track_multidelete(std::map<std::tuple<int, int, int, int>, Sharing_file>&pl4_zone_rawid, std::map<std::tuple<int, int, int, int>, Sharing_file>&pl3_zone_rawid, std::map<std::tuple<int, int, int, int>, std::tuple<int, int, int, int>>&pl3_exist);

std::vector<Sharing_file>  Read_sharing_file_bin(std::string filename);
std::vector<Sharing_file>  Read_sharing_file_txt(std::string filename);

void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs);

std::vector<Chain_baselist> read_chain_list(std::string filename);
void read_mfile(std::string filename, std::vector<Chain_baselist>&chain, std::map<int, double>&z_map);
void read_linklet_1to133(std::string file_path, std::vector<Chain_baselist>&chain, int link_sel_mode);
void read_linklet_133to1(std::string file_path, std::vector<Chain_baselist>&chain, int link_sel_mode);
int max_pl(std::vector<Chain_baselist>&chain);
int min_pl(std::vector<Chain_baselist>&chain);

std::multimap<int, mfile0::M_Base> Chain_base(std::vector<Chain_baselist> &chain_list, std::map<int, double> &z_map);

void basetrack_pick_up(std::string file_path, int pl, std::vector<mfile0::M_Base*>&base_p);
void basetrack_mfile_trans(std::vector<mfile0::M_Base*>&base_p, int pl, std::vector<corrmap0::Corrmap>&corr_abs);

mfile0::Mfile basetrack2chian(std::multimap<int, mfile0::M_Base> &chains);
int64_t Linklet_track_num(std::string filename);
void output_Saringfile_w_gid(std::string filename, std::vector<Chain_baselist>&gsf);

int main(int argc, char**argv) {
	if (argc != 8) {
		fprintf(stderr, "usage:prg sharing-file file-mode ECC-Area-path mode output-mfile link-sel-flg output-sharing-file\n");
		exit(1);
	}
	std::string file_in_sharing = argv[1];
	int file_mode = std::stoi(argv[2]);
	std::string file_in_ECC = argv[3];
	int mode = std::stoi(argv[4]);
	std::string file_out_mfile = argv[5];
	int link_sel_mode = std::stoi(argv[6]);
	std::string file_out_gsf = argv[7];

	std::string file_path_liklet = file_in_ECC + "\\0\\linklet";
	std::string file_in_corrabs = file_in_ECC + "\\0\\align\\corrmap-abs.lst";

	if (file_mode != 0 && file_mode != 1) {
		fprintf(stderr, "filemode exception\n");
		fprintf(stderr, "mode=0 binary sharing file\n");
		fprintf(stderr, "mode=1 txt    sharing file\n");
		exit(1);
	}
	if (mode != -1 && mode != 1) {
		fprintf(stderr, "mode exception\n");
		fprintf(stderr, "mode=-1 PL133 --> PL001 follow down\n");
		fprintf(stderr, "mode= 1 PL001 --> PL133 scan back\n");
		exit(1);
	}
	if (link_sel_mode != 0 && link_sel_mode != 1) {
		fprintf(stderr, "link sel flg exception\n");
		fprintf(stderr, "flg=0 use no selection linklet\n");
		fprintf(stderr, "flg=1 use    selected  linklet\n");
		exit(1);

	}
	//corrmap absの読み込み
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrabs, corr_abs);

	//sharing fileの読み込み
	std::vector<Chain_baselist> chain_list = read_sharing_file(file_in_sharing,file_mode, file_in_ECC,link_sel_mode);
	output_Saringfile_w_gid(file_out_gsf, chain_list);
	printf("num of track %d\n", chain_list.size());
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	z_map_correction(z_map, corr_abs);

	//全linkletからchainを再作成
	if (mode == 1) {
		read_linklet_1to133(file_path_liklet, chain_list, link_sel_mode);
	}
	else if (mode == -1) {
		read_linklet_133to1(file_path_liklet, chain_list, link_sel_mode);
	}

	//chainを構成するbasetrackを読み出し-->mfile座標系に変換
	std::map<int, int64_t> gid_cid;
	std::multimap<int, mfile0::M_Base> basetracks = Chain_base(chain_list, z_map);
	int count = 0;
	for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
		count = basetracks.count(itr->first);
		std::vector<mfile0::M_Base*> base_p;
		auto range = basetracks.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_p.push_back(&(res->second));
		}
		basetrack_pick_up(file_in_ECC, itr->first, base_p);
		basetrack_mfile_trans(base_p, itr->first, corr_abs);

		itr = std::next(itr, count - 1);
	}

	std::multimap<int, mfile0::M_Base> chains;
	for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
		chains.insert(std::make_pair(itr->second.group_id, itr->second));
	}

	mfile0::Mfile m = basetrack2chian(chains);

	mfile0::write_mfile(file_out_mfile, m);

}

std::vector<Chain_baselist> read_sharing_file(std::string filename,int mode,std::string file_in_ECC,int link_sel_mode) {
	std::vector<Sharing_file> mu_cand;
	if (mode == 0) {
		mu_cand = Read_sharing_file_bin(filename);
	}
	else if (mode == 1) {
		mu_cand = Read_sharing_file_txt(filename);
	}	

	//linklet読み込み
	std::stringstream file_in_link;
	if (link_sel_mode == 1) {
		file_in_link << file_in_ECC << "\\0\\linklet\\l-003-004.sel.bll";
	}
	else if (link_sel_mode == 0) {
		file_in_link << file_in_ECC << "\\0\\linklet\\l-003-004.sel0.bll";
	}
	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_bin( file_in_link.str(),link);

	std::vector<std::pair<Sharing_file, vxx::base_track_t>> base = sharing2base(mu_cand, file_in_ECC,link);
	std::vector<Chain_baselist> ret;

	int64_t gid = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {

		Chain_baselist c;
		//下5桁には関連飛跡のidを入れる
		c.groupid = gid*100000;
		c.sf = itr->first;
		c.rawid.insert(std::make_pair(itr->second.pl, itr->second.rawid));
		ret.push_back(c);
		gid++;
	}

	//for (auto itr = mu_cand.begin(); itr != mu_cand.end(); itr++) {
	//	Chain_baselist c;
	//	c.chainid = (int64_t)itr->unix_time * 1000 + (int64_t)itr->tracker_track_id;
	//	//printf("chainid %lld\n", c.chainid);
	//	c.rawid.insert(std::make_pair(itr->pl, itr->ecc_id));
	//	ret.push_back(c);
	//}

	return ret;
}
std::vector<std::pair<Sharing_file, vxx::base_track_t>> sharing2base(std::vector<Sharing_file> &track, std::string file_in_ECC, std::vector<netscan::linklet_t> &link) {
	std::map<std::tuple<int, int, int, int>, Sharing_file> pl3_zone_rawid;
	std::map<std::tuple<int, int, int, int>, Sharing_file> pl4_zone_rawid;

	std::map<std::tuple<int, int, int, int>, std::tuple<int, int, int, int>> pl3_exist;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		pl3_exist.insert(std::make_pair(std::make_tuple(itr->b[1].m[0].zone, itr->b[1].m[0].rawid, itr->b[1].m[1].zone, itr->b[1].m[1].rawid), std::make_tuple(itr->b[0].m[0].zone, itr->b[0].m[0].rawid, itr->b[0].m[1].zone, itr->b[0].m[1].rawid)));
	}
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		if (itr->pl == 3)pl3_zone_rawid.insert(std::make_pair(std::make_tuple(itr->zone[0], itr->rawid[0], itr->zone[1], itr->rawid[1]), *itr));
		else if (itr->pl == 4)pl4_zone_rawid.insert(std::make_pair(std::make_tuple(itr->zone[0], itr->rawid[0], itr->zone[1], itr->rawid[1]), *itr));
		else {
			fprintf(stderr, "shaeing file PL exception PL%03d\n", itr->pl);
		}
	}
	printf("PL003 sharing file =%d\n", pl3_zone_rawid.size());
	printf("PL004 sharing file =%d\n", pl4_zone_rawid.size());
	pl4_zone_rawid = pl4_track_multidelete(pl4_zone_rawid, pl3_zone_rawid, pl3_exist);
	printf("PL004 same delete --> %d\n", pl4_zone_rawid.size());

	std::vector<std::pair<Sharing_file, vxx::base_track_t>> ret;
	vxx::BvxxReader br;
	std::tuple<int, int, int, int> id;
	if (pl3_zone_rawid.size() != 0) {
		std::stringstream file_in_bvxx;
		file_in_bvxx << file_in_ECC << "\\PL003\\b003.sel.cor.vxx";
		std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx.str(), 3, 0);
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			std::get<0>(id) = itr->m[0].zone;
			std::get<1>(id) = itr->m[0].rawid;
			std::get<2>(id) = itr->m[1].zone;
			std::get<3>(id) = itr->m[1].rawid;
			if (pl3_zone_rawid.count(id) == 0)continue;
			auto res = pl3_zone_rawid.find(id);
			ret.push_back(std::make_pair(res->second, *itr));
		}
	}
	if (pl4_zone_rawid.size() != 0) {
		std::stringstream file_in_bvxx;
		file_in_bvxx << file_in_ECC << "\\PL004\\b004.sel.cor.vxx";
		std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx.str(), 4, 0);
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			std::get<0>(id) = itr->m[0].zone;
			std::get<1>(id) = itr->m[0].rawid;
			std::get<2>(id) = itr->m[1].zone;
			std::get<3>(id) = itr->m[1].rawid;
			if (pl4_zone_rawid.count(id) == 0)continue;
			auto res = pl4_zone_rawid.find(id);
			ret.push_back(std::make_pair(res->second, *itr));
		}

	}
	return ret;

}
std::map<std::tuple<int, int, int, int>, Sharing_file> pl4_track_multidelete(std::map<std::tuple<int, int, int, int>, Sharing_file>&pl4_zone_rawid, std::map<std::tuple<int, int, int, int>, Sharing_file>&pl3_zone_rawid, std::map<std::tuple<int, int, int, int>, std::tuple<int, int, int, int>> &pl3_exist) {
	std::map<std::tuple<int, int, int, int>, Sharing_file> ret;
	for (auto itr = pl4_zone_rawid.begin(); itr != pl4_zone_rawid.end(); itr++) {
		//PL003-PL004のlinkletが存在しない
		if (pl3_exist.count(itr->first) == 0) {
			ret.insert(*itr);
			continue;
		}
		auto pl3id = pl3_exist.at(itr->first);
		//PL003のsharing fileが存在しない
		if (pl3_zone_rawid.count(pl3id) == 0) {
			ret.insert(*itr);
			continue;
		}
		auto pl3_share = pl3_zone_rawid.at(pl3id);
		//ここだけinsertしない
		//PL003のsharing file内の時間等が一致
		if (pl3_share.unix_time == itr->second.unix_time&&pl3_share.tracker_track_id==itr->second.tracker_track_id) {
			continue;
		}
		ret.insert(*itr);
	}
	return ret;
}

std::vector<Sharing_file>  Read_sharing_file_bin(std::string filename) {
	std::vector<Sharing_file> ret;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	Sharing_file t;
	while (ifs.read((char*)& t, sizeof(Sharing_file))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(t);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no data!\n", filename.c_str());
		exit(1);
	}
	return ret;
}
std::vector<Sharing_file>  Read_sharing_file_txt(std::string filename) {
	std::vector<Sharing_file> ret;
	std::ifstream ifs(filename);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	Sharing_file t;
	//while (ifs.read((char*)& t, sizeof(Sharing_file))) {
	while (ifs >> t.pl >> t.ecc_id >> t.fixedwall_id >> t.trackerwall_id >> t.spotid >> t.zone[0] >> t.rawid[0] >> t.zone[1] >> t.rawid[1] >> t.unix_time >> t.tracker_track_id) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(t);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no data!\n", filename.c_str());
		exit(1);
	}
	return ret;
}

void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs) {

	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		auto res = z_map.find(pl);
		if (res == z_map.end())continue;
		res->second += itr->dz;
	}

}

void read_linklet_1to133(std::string file_path, std::vector<Chain_baselist>&chain,int link_sel_mode) {

	int peke = 0;
	for (int pl = 1; pl <= 133; pl++) {
		printf("now PL%03d maxPL=%03d\n", pl, max_pl(chain));
		if (pl > max_pl(chain))continue;
		//if (pl > 6)continue;

		peke = 0;
		while (true) {
			if (peke == 3)break;
			else if (peke == 2) {
				if (pl + peke + 1 == 18)break;
				if (pl >= 16 && pl % 2 == 1)break;
			}


			std::stringstream filename;
			if (link_sel_mode == 0) {
				filename << file_path << "\\l-" << std::setw(3) << std::setfill('0') << pl << "-"
					<< std::setw(3) << std::setfill('0') << pl + peke + 1 << ".sel0.bll";
			}
			else if (link_sel_mode == 1) {
				filename << file_path << "\\l-" << std::setw(3) << std::setfill('0') << pl << "-"
					<< std::setw(3) << std::setfill('0') << pl + peke + 1 << ".sel.bll";
			}
			if (std::filesystem::exists(filename.str()) == false)break;

			int64_t link_num = Linklet_track_num(filename.str());
			std::map<std::pair<int, int>, std::pair<int, int>> linked_id;
			FILE*fp_in;
			if ((fp_in = fopen(filename.str().c_str(), "rb")) == NULL) {
				printf("%s file not open!\n", filename.str().c_str());
				exit(EXIT_FAILURE);
			}

			const int read_num_max=1000;
			netscan::linklet_t link[read_num_max];
			int64_t  now = 0;
			int read_num;
			bool flg = true;
			while (flg) {
				if (link_num - now == read_num_max) {
					read_num = read_num_max;
					flg = false;
				}
				else if (link_num - now < read_num_max) {
					read_num = link_num - now;
					flg = false;
				}
				else {
					read_num = read_num_max;
				}
				fread(&link, sizeof(netscan::linklet_t), read_num, fp_in);
				for (int i = 0; i < read_num; i++) {
					linked_id.insert(std::make_pair(std::make_pair(link[i].b[0].pl, link[i].b[0].rawid), std::make_pair(link[i].b[1].pl, link[i].b[1].rawid)));
				}
				now += read_num;
				 //printf("\r read fin %lld/%lld", now, link_num);
			}
			//printf("\r read fin %lld/%lld\n\n", now, link_num);


			for (auto itr = chain.begin(); itr != chain.end(); itr++) {
				for (auto itr2 = itr->rawid.begin(); itr2 != itr->rawid.end(); itr2++) {

					if (linked_id.count(*itr2) == 0)continue;
					auto res = linked_id.find(*itr2);
					itr->rawid.insert(res->second);

				}
			}

			peke++;
		}


	}

}
int max_pl(std::vector<Chain_baselist>&chain) {
	int max_pl=-1;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		for (auto itr2 = itr->rawid.begin(); itr2 != itr->rawid.end(); itr2++) {
			max_pl = std::max(max_pl, itr2->first);
		}
	}
	return max_pl;
}
void read_linklet_133to1(std::string file_path, std::vector<Chain_baselist>&chain, int link_sel_mode) {

	int peke = 0;
	std::vector<netscan::linklet_t >link;
	std::map<std::pair<int, int>, std::pair<int, int>> linked_id;
	link.reserve(10000000);
	for (int pl = 133; pl >= 1; pl--) {
		printf("now PL%03d minPL=%03d\n", pl, min_pl(chain));
		if (pl < min_pl(chain))continue;

		peke = 0;
		while (true) {
			std::stringstream filename;
			if (link_sel_mode == 0) {
				filename << file_path << "\\l-" << std::setw(3) << std::setfill('0') << pl - 1 - peke << "-"
					<< std::setw(3) << std::setfill('0') << pl << ".sel0.bll";
			}
			else if (link_sel_mode == 1) {
				filename << file_path << "\\l-" << std::setw(3) << std::setfill('0') << pl - 1 - peke << "-"
					<< std::setw(3) << std::setfill('0') << pl << ".sel.bll";

			}
			if (std::filesystem::exists(filename.str()) == false)break;
			link.clear();
			linked_id.clear();
			netscan::read_linklet_bin(filename.str(), link);
			for (auto itr = link.begin(); itr != link.end(); itr++) {
				linked_id.insert(std::make_pair(std::make_pair(itr->b[1].pl, itr->b[1].rawid), std::make_pair(itr->b[0].pl, itr->b[0].rawid)));
			}
			for (auto itr = chain.begin(); itr != chain.end(); itr++) {
				for (auto itr2 = itr->rawid.begin(); itr2 != itr->rawid.end(); itr2++) {

					if (linked_id.count(*itr2) == 0)continue;
					auto res = linked_id.find(*itr2);
					itr->rawid.insert(res->second);

				}
			}

			peke++;
		}


	}

}
int min_pl(std::vector<Chain_baselist>&chain) {
	int min_pl = 999;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		for (auto itr2 = itr->rawid.begin(); itr2 != itr->rawid.end(); itr2++) {
			min_pl = std::min(min_pl, itr2->first);
		}
	}
	return min_pl;
}


std::multimap<int, mfile0::M_Base> Chain_base(std::vector<Chain_baselist> &chain_list, std::map<int, double> &z_map) {
	std::multimap<int, mfile0::M_Base> ret;

	mfile0::M_Base base;
	int gid = 0;
	for (auto itr = chain_list.begin(); itr != chain_list.end(); itr++) {
		gid = itr->groupid;
		for (auto b : itr->rawid) {
			base.group_id = gid;
			base.pos = b.first * 10 + 1;
			base.rawid = b.second;
			if (z_map.count(b.first) == 0) base.z = 0;
			else base.z = z_map.at(b.first);

			base.flg_d[0] = 0;
			base.flg_d[1] = 0;
			base.flg_i[0] = 0;
			base.flg_i[1] = 0;
			base.flg_i[2] = 0;
			base.flg_i[3] = 0;

			ret.insert(std::make_pair(b.first, base));
		}
		gid++;
	}
	return ret;
}

void basetrack_pick_up(std::string file_path, int pl, std::vector<mfile0::M_Base*>&base_p) {

	std::stringstream file_in_bvxx;
	file_in_bvxx << file_path << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	if (!std::filesystem::exists(file_in_bvxx.str())) {
		fprintf(stderr, "file[%s] not exist\n", file_in_bvxx.str().c_str());
		exit(1);
	}
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t>base = br.ReadAll(file_in_bvxx.str(), pl, 0);
	std::map<int, vxx::base_track_t> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_map.insert(std::make_pair(itr->rawid, (*itr)));
	}

	for (auto itr = base_p.begin(); itr != base_p.end(); itr++) {
		auto res = base_map.find((*itr)->rawid);
		if (res == base_map.end()) {
			fprintf(stderr, "PL%03d rawid=%d basetrack not found\n", pl, (*itr)->rawid);
			continue;
		}
		(*itr)->ax = res->second.ax;
		(*itr)->ay = res->second.ay;
		(*itr)->x = res->second.x;
		(*itr)->y = res->second.y;

		(*itr)->ph = res->second.m[0].ph + res->second.m[1].ph;
	}

}
void basetrack_mfile_trans(std::vector<mfile0::M_Base*>&base_p, int pl, std::vector<corrmap0::Corrmap>&corr_abs) {

	corrmap0::Corrmap param;
	bool flg = false;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		if (itr->pos[0] / 10 == pl) {
			flg = true;
			param = *itr;
			break;
		}
	}
	if (!flg) {
		fprintf(stderr, "PL%03d corrmap abs not found\n", pl);
		return;
	}

	double tmp_x, tmp_y;
	for (auto itr = base_p.begin(); itr != base_p.end(); itr++) {
		tmp_x = (*itr)->x;
		tmp_y = (*itr)->y;
		(*itr)->x = tmp_x * param.position[0] + tmp_y * param.position[1] + param.position[4];
		(*itr)->y = tmp_x * param.position[2] + tmp_y * param.position[3] + param.position[5];
		tmp_x = (*itr)->ax;
		tmp_y = (*itr)->ay;
		(*itr)->ax = tmp_x * param.angle[0] + tmp_y * param.angle[1] + param.angle[4];
		(*itr)->ay = tmp_x * param.angle[2] + tmp_y * param.angle[3] + param.angle[5];
	}


}

mfile0::Mfile basetrack2chian(std::multimap<int, mfile0::M_Base> &chains) {
	//chains(gid,M_Base)
	//gid_cid(gid,chainid)

	mfile0::Mfile ret;
	int  num = 0, count = 0;
	int64_t gid;
	for (auto itr = chains.begin(); itr != chains.end(); itr++) {
		count = chains.count(itr->first);
		auto range = chains.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			mfile0::M_Chain c;
			c.basetracks.push_back(res->second);
			c.chain_id = num;
			c.nseg = c.basetracks.size();
			c.pos0 = c.basetracks.begin()->pos;
			c.pos1 = c.basetracks.rbegin()->pos;
			ret.chains.push_back(c);
			num++;
		}
		itr = std::next(itr, count - 1);
	}
	//chainを構成するbasetrackをlistにつめる

	ret.header.head[0] = "% Created by mkmf";
	ret.header.head[1] = "% on 2007 / 12 / 25 23:28 : 09 + 09:00 (JST)";
	ret.header.head[2] = "	0       0   3   0      0.0   0.0000";
	std::set<int> pos;
	for (int i = 0; i < ret.chains.size(); i++) {
		for (int j = 0; j < ret.chains[i].basetracks.size(); j++) {
			pos.insert(ret.chains[i].basetracks[j].pos);
		}
	}

	ret.header.num_all_plate = int(pos.size());

	for (auto itr = pos.begin(); itr != pos.end(); itr++) {
		ret.header.all_pos.push_back(*itr);
	}

	return ret;

}
int64_t Linklet_track_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(netscan::linklet_t);
}

void output_Saringfile_w_gid(std::string filename, std::vector<Chain_baselist>&gsf) {
	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (gsf.size() == 0) {
		fprintf(stderr, "target data ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int64_t count = 0;
		int64_t max = gsf.size();

		for (int i = 0; i < max; i++) {
			if (count % 10000 == 0) {
				std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / gsf.size() << "%%";
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(3) << std::setprecision(0) << gsf[i].sf.pl << " "
				<< std::setw(10) << std::setprecision(0) << gsf[i].sf.ecc_id << " "
				<< std::setw(10) << std::setprecision(0) << gsf[i].sf.fixedwall_id << " "
				<< std::setw(10) << std::setprecision(0) << gsf[i].sf.trackerwall_id << " "
				<< std::setw(4) << std::setprecision(0) << gsf[i].sf.spotid << " "
				<< std::setw(2) << std::setprecision(0) << gsf[i].sf.zone[0] << " "
				<< std::setw(10) << std::setprecision(0) << gsf[i].sf.rawid[0] << " "
				<< std::setw(2) << std::setprecision(0) << gsf[i].sf.zone[1] << " "
				<< std::setw(10) << std::setprecision(0) << gsf[i].sf.rawid[1] << " "
				<< std::setw(10) << std::setprecision(0) << gsf[i].sf.unix_time << " "
				<< std::setw(3) << std::setprecision(0) << gsf[i].sf.tracker_track_id << " "
				<< std::setw(10) << std::setprecision(0) << gsf[i].groupid << std::endl;

		}
		std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / gsf.size() << "%%" << std::endl;
	}
}

