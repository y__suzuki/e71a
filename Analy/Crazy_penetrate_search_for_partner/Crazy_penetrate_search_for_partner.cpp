#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#include <chrono>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

namespace mfile0 {
	bool operator<(const mfile0::M_Chain&left, const mfile0::M_Chain&right) {
		if (left.basetracks.begin()->group_id == right.basetracks.begin()->group_id)return left.chain_id < right.chain_id;
		return left.basetracks.begin()->group_id < right.basetracks.begin()->group_id;
	}
}

class basetrack_minimum {
public:
	int pl, rawid, ph;
	float ax, ay, x, y;
};
class basetrack_mfile :public basetrack_minimum {
public:
	float z;
};

class penetrate_check_area {
	int pl;
	double x_min, x_max, y_min, y_max;
	double ax_min, ax_max, ay_min, ay_max;
public:
	void Set_area_position(double in_xmin, double in_xmax, double in_ymin, double in_ymax);
	void Set_area_angle(double in_xmin, double in_xmax, double in_ymin, double in_ymax);
	void Set_pl(int in_pl);
	bool judage_inside(int pl, double x, double y, double ax, double ay);
};
void penetrate_check_area::Set_area_position(double in_xmin, double in_xmax, double in_ymin, double in_ymax) {
	x_min = in_xmin;
	x_max = in_xmax;
	y_min = in_ymin;
	y_max = in_ymax;
}
void penetrate_check_area::Set_area_angle(double in_xmin, double in_xmax, double in_ymin, double in_ymax) {
	ax_min = in_xmin;
	ax_max = in_xmax;
	ay_min = in_ymin;
	ay_max = in_ymax;
}
void penetrate_check_area::Set_pl(int in_pl) {
	pl = in_pl;
}
bool penetrate_check_area::judage_inside(int in_pl, double x, double y, double ax, double ay) {
	if (in_pl != pl)return false;

	if (x < x_min)return false;
	if (x_max < x)return false;
	if (y < y_min)return false;
	if (y_max < y)return false;

	if (ax < ax_min)return false;
	if (ax_max < ax)return false;
	if (ay < ay_min)return false;
	if (ay_max < ay)return false;

	return true;
}



bool sort_basetrack_pl(const basetrack_minimum&left, const basetrack_minimum &right) {
	if (left.pl == right.pl)return left.rawid < right.rawid;
	return left.pl < right.pl;
}

std::vector< basetrack_minimum> read_base(std::string filename);
std::map<int, corrmap0::Corrmap> read_corrmap_abs(std::string file_in_ECC);
void z_map_corr(std::map<int, double> &z_map, std::map<int, corrmap0::Corrmap>&corr);

std::map<mfile0::M_Chain, std::vector<penetrate_check_area >>  Set_check_area(std::vector<mfile0::M_Chain>&chain, std::map<int, double> &z_map, double thr_oa, double thr_md);
std::map<mfile0::M_Chain, std::vector<basetrack_mfile >>Search_penetrate_cand(std::map<mfile0::M_Chain, std::vector<penetrate_check_area >>&muon_area, std::vector< basetrack_minimum>&base, std::map<int, double> &z_map, std::map<int, corrmap0::Corrmap>&corr);

std::vector<basetrack_mfile> judege_penetrate(mfile0::M_Chain c, std::vector<basetrack_mfile >&pene_cnad, std::map<int, int> &event_pl, double threshold_oa, double threshold_md);
void output(std::string filename, std::map<mfile0::M_Chain, std::vector<basetrack_mfile >>&pene_cand);
std::map<int, int> event_muonpl(std::vector<mfile0::M_Chain>&chain);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage\n file_in_mfile file_in_base file_in_ECC file_out\n");
		exit(1);
	}

	std::string file_in_muon_mfile = argv[1];
	std::string file_in_all_base = argv[2];
	std::string file_in_ECC = argv[3];
	std::string file_out = argv[4];

	double hash_size_angle = 0.1;
	double hash_size_position = 2000;


	mfile0::Mfile m;
	mfile0::read_mfile(file_in_muon_mfile, m);
	std::map<int, int> event_pl=event_muonpl(m.chains);

	std::vector< basetrack_minimum> base = read_base(file_in_all_base);

	std::map<int, corrmap0::Corrmap> corrmap = read_corrmap_abs(file_in_ECC);
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\st\\st.dat";
	printf("%s\n", structure_path.str().c_str());
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	//gapをmfileの値に変換
	z_map_corr(z_map, corrmap);

	//oaとmdの閾値の決定
	double thr_oa = 0.2, thr_md = 200;
	//muon飛跡から探索すべき領域の決定
	std::map<mfile0::M_Chain, std::vector<penetrate_check_area >>muon_area = Set_check_area(m.chains, z_map, thr_oa, thr_md);
	//領域内の飛跡をpickup
	std::map<mfile0::M_Chain, std::vector<basetrack_mfile >>pene_cand = Search_penetrate_cand(muon_area, base, z_map, corrmap);

	//md,oaで探索
	for (auto itr = pene_cand.begin(); itr != pene_cand.end(); itr++) {
		itr->second = judege_penetrate(itr->first, itr->second, event_pl, thr_oa, thr_md);
	}

	output(file_out, pene_cand);

}

std::map<int, int> event_muonpl(std::vector<mfile0::M_Chain>&chain) {
	std::map<int, int>  ret;
	int event_id, muon_pl;
	for (auto &c : chain) {
		if (c.chain_id == 0) {
			event_id = c.basetracks.begin()->group_id;
			muon_pl = c.basetracks.rbegin()->pos / 10;
			ret.insert(std::make_pair(event_id, muon_pl));
		}
	}
	return ret;
}

int64_t Basetrack_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(basetrack_minimum);
}
std::vector< basetrack_minimum> read_base(std::string filename) {
	std::vector< basetrack_minimum> ret;
	int64_t track_num = Basetrack_num(filename);
	ret.reserve(track_num);

	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	basetrack_minimum b;
	while (ifs.read((char*)& b, sizeof(basetrack_minimum))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		//ここ
		ret.emplace_back(b);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no basetrack!\n", filename.c_str());
		exit(1);
	}
	return ret;

}

std::map<int, corrmap0::Corrmap> read_corrmap_abs(std::string file_in_ECC) {
	std::stringstream file_in_corr;
	file_in_corr << file_in_ECC << "\\Area0\\0\\align\\corrmap-abs.lst";

	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corr.str(), corr);

	std::map<int, corrmap0::Corrmap> ret;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		ret.insert(std::make_pair(itr->pos[0] / 10, *itr));
	}
	return ret;
}

void z_map_corr(std::map<int, double> &z_map, std::map<int, corrmap0::Corrmap>&corr) {
	for (auto itr = z_map.begin(); itr != z_map.end(); itr++) {
		auto res = corr.find(itr->first);
		if (res == corr.end()) {
			fprintf(stderr, "PL%03d corrmap abs not found\n", itr->first);
			//exit(1);
		}
		itr->second = itr->second + res->second.dz;
	}
}


std::map<mfile0::M_Chain, std::vector<penetrate_check_area >>  Set_check_area(std::vector<mfile0::M_Chain>&chain, std::map<int, double> &z_map, double thr_oa, double thr_md) {
	std::map<mfile0::M_Chain, std::vector<penetrate_check_area >> ret;
	int muon_pl = 0;
	for (auto &c : chain) {
		if (c.chain_id == 0) {
			muon_pl = c.basetracks.rbegin()->pos / 10;
			continue;
		}
		if (c.pos1/10<=muon_pl) {
			//最上流basetrack pickup
			int stop_pl = c.basetracks.rbegin()->pos / 10;
			double stop_z = c.basetracks.rbegin()->z;
			double stop_x = c.basetracks.rbegin()->x;
			double stop_y = c.basetracks.rbegin()->y;

			//muonの角度は上流の平均
			double stop_ax = 0, stop_ay = 0;
			int count = 0;
			for (int i = c.basetracks.size() - 1; i >= std::max((int)c.basetracks.size() - 6, 0); i--) {
				//printf("%d %d ax=%.4lf ay=%.4lf\n", stop_pl, c.basetracks[i].pos/10, c.basetracks[i].ax, c.basetracks[i].ay);
				stop_ax += c.basetracks[i].ax;
				stop_ay += c.basetracks[i].ay;
				count++;
			}
			stop_ax = stop_ax / count;
			stop_ay = stop_ay / count;
			//printf("ave ax=%.4lf ay=%.4lf\n", stop_ax, stop_ay);

			std::vector<penetrate_check_area >p_v;

			//PL番号 -1〜4の間で探索
			for (int i_pl = -1; i_pl <= 4; i_pl++) {
				int t_pl = stop_pl + i_pl;
				auto z_val = z_map.find(t_pl);
				//PL3-PL133の範囲外
				if (z_val == z_map.end())continue;


				penetrate_check_area p;
				p.Set_pl(t_pl);
				double ax_min = tan(std::max(atan(stop_ax) - thr_oa * sqrt(2), -1.48));
				double ax_max = tan(std::min(atan(stop_ax) + thr_oa * sqrt(2), 1.48));
				double ay_min = tan(std::max(atan(stop_ay) - thr_oa * sqrt(2), -1.48));
				double ay_max = tan(std::min(atan(stop_ay) + thr_oa * sqrt(2), 1.48));
				p.Set_area_angle(ax_min, ax_max, ay_min, ay_max);
				//printf("%4.1lf %4.1lf %4.1lf %4.1lf\n", ax_min, ax_max, ay_min, ay_max);

				double x_min = stop_x + ax_min * (z_val->second - stop_z);
				double x_max = stop_x + ax_max * (z_val->second - stop_z);
				double y_min = stop_y + ay_min * (z_val->second - stop_z);
				double y_max = stop_y + ay_max * (z_val->second - stop_z);
				if (x_min > x_max)std::swap(x_min, x_max);
				if (y_min > y_max)std::swap(y_min, y_max);
				x_min -= 200 * sqrt(2);
				x_max += 200 * sqrt(2);
				y_min -= 200 * sqrt(2);
				y_max += 200 * sqrt(2);
				p.Set_area_position(x_min, x_max, y_min, y_max);

				p_v.push_back(p);
			}

			ret.insert(std::make_pair(c, p_v));
		}
		else if (c.pos0 / 10 > muon_pl) {
			//最下流basetrack pickup
			int stop_pl = c.basetracks.begin()->pos / 10;
			double stop_z = c.basetracks.begin()->z;
			double stop_x = c.basetracks.begin()->x;
			double stop_y = c.basetracks.begin()->y;

			//muonの角度は上流の平均
			double stop_ax = 0, stop_ay = 0;
			int count = 0;
			for (int i = 0; i < std::min((int)c.basetracks.size(), 6); i++) {
				stop_ax += c.basetracks[i].ax;
				stop_ay += c.basetracks[i].ay;
				count++;
			}
			stop_ax = stop_ax / count;
			stop_ay = stop_ay / count;
			std::vector<penetrate_check_area >p_v;

			//PL番号 -4〜1の間で探索
			for (int i_pl = -4; i_pl <= 1; i_pl++) {
				int t_pl = stop_pl + i_pl;
				auto z_val = z_map.find(t_pl);
				//PL3-PL133の範囲外
				if (z_val == z_map.end())continue;


				penetrate_check_area p;
				p.Set_pl(t_pl);
				double ax_min = tan(std::max(atan(stop_ax) - thr_oa * sqrt(2), -1.48));
				double ax_max = tan(std::min(atan(stop_ax) + thr_oa * sqrt(2), 1.48));
				double ay_min = tan(std::max(atan(stop_ay) - thr_oa * sqrt(2), -1.48));
				double ay_max = tan(std::min(atan(stop_ay) + thr_oa * sqrt(2), 1.48));
				p.Set_area_angle(ax_min, ax_max, ay_min, ay_max);

				double x_min = stop_x + ax_min * (z_val->second - stop_z);
				double x_max = stop_x + ax_max * (z_val->second - stop_z);
				double y_min = stop_y + ay_min * (z_val->second - stop_z);
				double y_max = stop_y + ay_max * (z_val->second - stop_z);
				if (x_min > x_max)std::swap(x_min, x_max);
				if (y_min > y_max)std::swap(y_min, y_max);
				x_min -= 200 * sqrt(2);
				x_max += 200 * sqrt(2);
				y_min -= 200 * sqrt(2);
				y_max += 200 * sqrt(2);
				p.Set_area_position(x_min, x_max, y_min, y_max);

				p_v.push_back(p);
			}

			ret.insert(std::make_pair(c, p_v));
		}
		else {
			fprintf(stderr, "gid=%d cid=%d tid=%d flg[0]==1 not found\n", c.basetracks.begin()->group_id, c.chain_id, c.basetracks.begin()->flg_i[1]);
			exit(1);

		}
	}
	return ret;
}

std::map<mfile0::M_Chain, std::vector<basetrack_mfile >>Search_penetrate_cand(std::map<mfile0::M_Chain, std::vector<penetrate_check_area >>&muon_area, std::vector< basetrack_minimum>&base, std::map<int, double> &z_map, std::map<int, corrmap0::Corrmap>&corr) {

	int64_t count = 0, max = base.size();
	std::vector<std::pair<mfile0::M_Chain, basetrack_mfile>> connect_cand_v;

	std::chrono::system_clock::time_point  start, end; // 型は auto で可
	start = std::chrono::system_clock::now(); // 計測開始時間

	for (int64_t i = 0; i < max; i++) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r penetrate cand search %10lld/%10lld (%4.1lf%%)", count, max, count*100. / max);
		}
		count++;

		auto res0 = z_map.find(base[i].pl);
		auto res1 = corr.find(base[i].pl);
		if (res0 == z_map.end()) {
			fprintf(stderr, "PL%03d z not found\n", base[i].pl);
			exit(1);
		}
		if (res1 == corr.end()) {
			fprintf(stderr, "PL%03d corrmap-abs not found\n", base[i].pl);
			exit(1);
		}

		basetrack_mfile b_m;
		b_m.ph = base[i].ph;
		b_m.pl = base[i].pl;
		b_m.rawid = base[i].rawid;
		b_m.z = res0->second;
		b_m.x = base[i].x*res1->second.position[0] + base[i].y*res1->second.position[1] + res1->second.position[4];
		b_m.y = base[i].x*res1->second.position[2] + base[i].y*res1->second.position[3] + res1->second.position[5];
		b_m.ax = base[i].ax*res1->second.angle[0] + base[i].ay*res1->second.angle[1] + res1->second.angle[4];
		b_m.ay = base[i].ax*res1->second.angle[2] + base[i].ay*res1->second.angle[3] + res1->second.angle[5];

		for (auto itr = muon_area.begin(); itr != muon_area.end(); itr++) {
			for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
				if (itr2->judage_inside(b_m.pl, b_m.x, b_m.y, b_m.ax, b_m.ay)) {
#pragma omp critical
					connect_cand_v.push_back(std::make_pair(itr->first, b_m));
				}
			}
		}
	}
	fprintf(stderr, "\r penetrate cand search %10lld/%10lld (%4.1lf%%)\n", count, max, count*100. / max);

	end = std::chrono::system_clock::now();  // 計測終了時間
	double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count(); //処理に要した時間をミリ秒に変換
	printf("time %.1lf[s]\n", elapsed / 1000);

	printf("hit base num = %lld\n", connect_cand_v.size());

	std::multimap<mfile0::M_Chain, basetrack_mfile > connect_cand_multimap;
	for (auto itr = connect_cand_v.begin(); itr != connect_cand_v.end(); itr++) {
		connect_cand_multimap.insert(*itr);
	}
	std::map<mfile0::M_Chain, std::vector<basetrack_mfile >> ret;
	for (auto itr = connect_cand_multimap.begin(); itr != connect_cand_multimap.end(); itr++) {
		int count = connect_cand_multimap.count(itr->first);
		std::vector<basetrack_mfile > base_v;
		base_v.reserve(count);
		auto range = connect_cand_multimap.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_v.push_back(res->second);
		}
		ret.insert(std::make_pair(itr->first, base_v));
		itr = std::next(itr, count - 1);
	}

	return ret;
}

std::vector<basetrack_mfile> judege_penetrate(mfile0::M_Chain c, std::vector<basetrack_mfile >&pene_cnad, std::map<int, int> &event_pl, double threshold_oa, double threshold_md) {

	if (event_pl.count(c.basetracks.begin()->group_id) == 0) {
		fprintf(stderr, "event id =%d not found\n", c.basetracks.begin()->group_id);
		exit(1);
	}
	int muon_pl = event_pl.at(c.basetracks.begin()->group_id);

	double stop_x, stop_y, stop_z, stop_ax, stop_ay;
	if (c.pos1 / 10 <= muon_pl) {
		//最上流basetrack pickup
		stop_z = c.basetracks.rbegin()->z;
		stop_x = c.basetracks.rbegin()->x;
		stop_y = c.basetracks.rbegin()->y;

		//muonの角度は上流の平均
		stop_ax = 0;
		stop_ay = 0;
		int count = 0;
		for (int i = c.basetracks.size() - 1; i >= std::max((int)c.basetracks.size() - 6, 0); i--) {
			stop_ax += c.basetracks[i].ax;
			stop_ay += c.basetracks[i].ay;
			count++;
		}
		stop_ax = stop_ax / count;
		stop_ay = stop_ay / count;
	}
	else if (c.pos0 / 10 > muon_pl) {
		//最下流basetrack pickup
		stop_z = c.basetracks.begin()->z;
		stop_x = c.basetracks.begin()->x;
		stop_y = c.basetracks.begin()->y;

		//muonの角度は上流の平均
		stop_ax = 0;
		stop_ay = 0;
		int count = 0;
		for (int i = 0; i < std::min((int)c.basetracks.size(), 6); i++) {
			stop_ax += c.basetracks[i].ax;
			stop_ay += c.basetracks[i].ay;
			count++;
		}
		stop_ax = stop_ax / count;
		stop_ay = stop_ay / count;
	}
	else {
		fprintf(stderr, "gid=%d cid=%d tid=%d flg[0]==1 not found\n", c.basetracks.begin()->group_id, c.chain_id, c.basetracks.begin()->flg_i[1]);
		exit(1);
	}


	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	pos0.x = stop_x;
	pos0.y = stop_y;
	pos0.z = stop_z;
	dir0.x = stop_ax;
	dir0.y = stop_ay;
	dir0.z = 1;

	std::vector<basetrack_mfile> ret;
	for (int i = 0; i < pene_cnad.size(); i++) {
		pos1.x = pene_cnad[i].x;
		pos1.y = pene_cnad[i].y;
		pos1.z = pene_cnad[i].z;
		dir1.x = pene_cnad[i].ax;
		dir1.y = pene_cnad[i].ay;
		dir1.z = 1;

		double extra[2], md, oa, z_range[2] = { pos1.z,pos0.z };
		oa = matrix_3D::opening_angle(dir0, dir1);
		md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		if (oa > threshold_oa)continue;
		if (md > threshold_md)continue;
		ret.push_back(pene_cnad[i]);
	}
	return ret;
}

void output(std::string filename, std::map<mfile0::M_Chain, std::vector<basetrack_mfile >>&pene_cand) {

	std::ofstream ofs(filename);
	int all = pene_cand.size(), count = 0;

	for (auto itr = pene_cand.begin(); itr != pene_cand.end(); itr++) {
		if (count % 100 == 0) {
			fprintf(stderr, "\r write penetrate candidate %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		for (int i = 0; i < itr->second.size(); i++) {
			ofs << std::right << std::fixed
				<< std::setw(10) << std::setprecision(0) << itr->first.basetracks.begin()->group_id << " "
				<< std::setw(10) << std::setprecision(0) << itr->first.chain_id * 100000 + i + 1 << " "
				<< std::setw(10) << std::setprecision(0) << itr->second[i].pl << " "
				<< std::setw(10) << std::setprecision(0) << itr->second[i].rawid << std::endl;
		}
	}
	fprintf(stderr, "\r write penetrate candidate %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

}