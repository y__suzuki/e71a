#pragma comment(lib, "VxxReader.lib")
#include <VxxReader.h>

std::vector<vxx::base_track_t> edge_cut(std::vector<vxx::base_track_t> &base, int area, double cut);

int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-bvxx pl area\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	int area = std::stoi(argv[3]);

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_bvxx, pl, 0);
	base = edge_cut(base, area, 2000);

	vxx::BvxxWriter bw;
	bw.Write(file_in_bvxx, pl, 0, base);
}
std::vector<vxx::base_track_t> edge_cut(std::vector<vxx::base_track_t> &base, int area, double cut) {
	double xmin, xmax, ymin, ymax;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			xmin = itr->x;
			xmax = itr->x;
			ymin = itr->y;
			ymax = itr->y;
		}
		xmin = std::min(itr->x, xmin);
		xmax = std::max(itr->x, xmax);
		ymin = std::min(itr->y, xmin);
		ymax = std::max(itr->y, xmax);
	}
	std::vector<vxx::base_track_t> ret;
	bool flg = false;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		flg = true;
		if (area % 2 == 1) {
			//area�̍���cut
			if (xmin + cut > itr->x) {
				flg = false;
			}
		}
		if (area % 2 == 0) {
			//area�̉E��cut
			if (itr->x > xmax - cut) {
				flg = false;
			}
		}
		if (area == 5 || area == 6) {
			if (itr->y > ymax - cut) {
				flg = false;
			}
		}
		if (flg) {
			ret.push_back(*itr);
		}
	}

	printf("track num %d --> %d\n", base.size(), ret.size());
	return ret;
}