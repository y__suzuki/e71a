#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void Read_merge_momch(std::string file_in_momch, int i, std::vector<Momentum_recon::Mom_chain> &momch);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:filename");
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch = argv[2];
	std::vector<Momentum_recon::Mom_chain> momch;

	for (int i = 0; i < 999; i++) {
		Read_merge_momch(file_in_momch, i, momch);
	}

	Momentum_recon::Write_mom_chain_extension(file_out_momch, momch);

}
void Read_merge_momch(std::string file_in_momch, int i, std::vector<Momentum_recon::Mom_chain> &momch) {
	std::stringstream filename;
	filename << file_in_momch << i << ".momch";
	std::vector<Momentum_recon::Mom_chain> mom_buf = Momentum_recon::Read_mom_chain_extension(filename.str());
	for (auto &c: mom_buf) {
		c.groupid += i * 1000000;
		momch.push_back(c);
	}
}
