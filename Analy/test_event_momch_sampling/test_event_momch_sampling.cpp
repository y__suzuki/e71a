#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>


class Difference {
public:
	int groupid, chainid, vertex_pl, target_pl[2], direction, add_nseg, edge_pl[2];
	double ax, ay, dax, day, dx, dy, dar, dal, dr, dl;
	double  s_dar, s_dal, s_dr, s_dl;
};
class Sigma_list {
public:
	int peke;
	double angle;
	double dal, dar, dr, dl;
};
std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch);
std::vector<Momentum_recon::Mom_chain > format_change(std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&mom_ev);
void Check_fill_factor_forward_water(std::string filename, std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&momch_ev);
void Calc_difference(Difference&diff, std::pair<Momentum_recon::Mom_basetrack, Momentum_recon::Mom_basetrack>&pair);
std::vector<Difference> Calc_difference(std::vector<Momentum_recon::Event_information>&ev_v);
	void output_difference(std::string filename, std::vector<Difference>&diff);
std::map<int, std::map<double, Sigma_list>> Read_sigma_list(std::string filename);
void Calc_sigma(std::vector<Difference>&diff, std::map<int, std::map<double, Sigma_list>> &sigma_list);
std::set<std::pair<int, int>> Get_penetrate_list(std::vector<Difference>&diff);
std::map<double, std::pair<double, double>> vph_mip_distribution();
bool judege_Black(Momentum_recon::Mom_chain &chain, std::map<double, std::pair<double, double>> &vph_mip, double thr_sigma);
std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> cut_penetrate(std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&momch_ev, std::set<std::pair<int, int>>&id_list);
mfile0::M_Chain momch_to_chain(Momentum_recon::Mom_chain&momch, int groupid);
mfile0::Mfile Momch_to_mfile(std::vector<Momentum_recon::Event_information> &ev_v, std::vector<Difference>&diff);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-momch sigma_list file_out_mfile\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_in_sigma = argv[2];
	std::string file_out_mfile = argv[3];

	std::map<int, std::map<double, Sigma_list>> sigma_list = Read_sigma_list(file_in_sigma);

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);

	std::vector<Difference>diff = Calc_difference(momch);
	Calc_sigma(diff, sigma_list);

	mfile0::Mfile m = Momch_to_mfile(momch, diff);
	mfile1::write_mfile_extension(file_out_mfile, m);


}
std::map<int, std::map<double, Sigma_list>> Read_sigma_list(std::string filename) {

	std::map<int, std::map<double, Sigma_list>> ret;
	std::ifstream ifs(filename);
	int peke, num;
	while (ifs >> peke >> num) {
		std::map<double, Sigma_list> sigma_map;
		Sigma_list sig;
		sig.peke = peke;
		double angle_min, angle_max;
		for (int i = 0; i < num; i++) {
			ifs >> sig.angle >> sig.dal >> sig.dar >> sig.dl >> sig.dr;
			sigma_map.insert(std::make_pair(sig.angle, sig));
		}
		ret.insert(std::make_pair(sig.peke, sigma_map));
	}
	return ret;

	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {
	//	printf("peke:%d\n", itr->first);
	//	for (auto s : itr->second) {
	//		printf("%.1lf %.5lf %.5lf %.2lf %.2lf\n", s.second.angle, s.second.dal, s.second.dar, s.second.dl, s.second.dr);
	//	}
	//}

}

std::vector<Difference> Calc_difference(std::vector<Momentum_recon::Event_information>&ev_v) {
	std::vector<Difference> ret;

	for (auto &ev : ev_v) {
		Difference diff;
		diff.groupid = ev.groupid;

		for (auto &c : ev.chains) {
			if (c.chainid == 0)continue;
			diff.vertex_pl = ev.vertex_pl;
			diff.direction = c.direction;
			diff.chainid = c.chainid;
			diff.edge_pl[0] = c.base.begin()->pl;
			diff.edge_pl[1] = c.base.rbegin()->pl;

			diff.add_nseg = 0;
			diff.target_pl[0] = -1;
			diff.target_pl[1] = -1;
			//forward
			if (diff.direction == 1) {
				diff.target_pl[0] = diff.vertex_pl;

				for (auto &b : c.base) {
					if (b.pl > diff.vertex_pl) {
						if (diff.target_pl[1] < 0) {
							diff.target_pl[1] = b.pl;
						}
						diff.add_nseg += 1;
					}
				}
				if (diff.target_pl[1] < 0)continue;
				for (auto &pair : c.base_pair) {
					if (pair.first.pl == diff.target_pl[0] && pair.second.pl == diff.target_pl[1]) {
						Calc_difference(diff, pair);
						ret.push_back(diff);
						break;
					}
				}

			}
			//backward
			else if (diff.direction == -1) {
				for (auto b = c.base.rbegin(); b != c.base.rend(); b++) {
					if (b->pl <= diff.vertex_pl) {
						if (diff.target_pl[1] < 0) {
							diff.target_pl[0] = b->pl;
							diff.target_pl[1] = std::next(b, -1)->pl;
						}
						diff.add_nseg += 1;
					}
				}
				if (diff.target_pl[1] < 0)continue;
				for (auto &pair : c.base_pair) {
					if (pair.first.pl == diff.target_pl[0] && pair.second.pl == diff.target_pl[1]) {
						Calc_difference(diff, pair);
						ret.push_back(diff);
						break;
					}
				}

			}
		}
	}
	return ret;

}
void Calc_sigma(std::vector<Difference>&diff, std::map<int, std::map<double, Sigma_list>> &sigma_list) {

	int peke;
	double angle, sigma_dal, sigma_dar, sigma_dr, sigma_dl;
	Sigma_list sigma[2];
	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		peke = itr->target_pl[1] - itr->target_pl[0] - 1;
		angle = sqrt(pow(itr->ax, 2) + pow(itr->ay, 2));

		auto sigma_peke = sigma_list.find(peke);
		if (sigma_peke == sigma_list.end()) {
			fprintf(stderr, "peke=%d not found\n", peke);
			exit(1);
		}
		auto sigma_angle = sigma_peke->second.upper_bound(angle);
		if (sigma_angle == sigma_peke->second.begin()) {
			sigma[0] = sigma_angle->second;
			sigma[1] = sigma_angle->second;
			sigma_dal = sigma[0].dal;
			sigma_dar = sigma[0].dar;
			sigma_dl = sigma[0].dl;
			sigma_dr = sigma[0].dr;
		}
		else if (sigma_angle == sigma_peke->second.end()) {
			sigma[0] = sigma_peke->second.rbegin()->second;
			sigma[1] = sigma_peke->second.rbegin()->second;
			sigma_dal = sigma[0].dal;
			sigma_dar = sigma[0].dar;
			sigma_dl = sigma[0].dl;
			sigma_dr = sigma[0].dr;
		}
		else {
			sigma[0] = std::next(sigma_angle, -1)->second;
			sigma[1] = sigma_angle->second;
			sigma_dal = (sigma[1].dal - sigma[0].dal) / (sigma[1].angle - sigma[0].angle)*(angle - sigma[0].angle) + sigma[0].dal;
			sigma_dar = (sigma[1].dar - sigma[0].dar) / (sigma[1].angle - sigma[0].angle)*(angle - sigma[0].angle) + sigma[0].dar;
			sigma_dl = (sigma[1].dl - sigma[0].dl) / (sigma[1].angle - sigma[0].angle)*(angle - sigma[0].angle) + sigma[0].dl;
			sigma_dr = (sigma[1].dr - sigma[0].dr) / (sigma[1].angle - sigma[0].angle)*(angle - sigma[0].angle) + sigma[0].dr;

		}


		itr->s_dal = itr->dal / sigma_dal;
		itr->s_dar = itr->dar / sigma_dar;
		itr->s_dl = itr->dl / sigma_dl;
		itr->s_dr = itr->dr / sigma_dr;
	}


}

void Calc_difference(Difference&diff, std::pair<Momentum_recon::Mom_basetrack, Momentum_recon::Mom_basetrack>&pair) {
	diff.ax = pair.first.ax;
	diff.ay = pair.first.ay;
	diff.dax = pair.second.ax - pair.first.ax;
	diff.day = pair.second.ay - pair.first.ay;

	diff.dx = pair.second.x - pair.first.x - (pair.first.ax + pair.second.ax) / 2 * (pair.second.z - pair.first.z);
	diff.dy = pair.second.y - pair.first.y - (pair.first.ay + pair.second.ay) / 2 * (pair.second.z - pair.first.z);
	diff.dar = (diff.dax*pair.first.ax + diff.day * pair.first.ay) / sqrt(pair.first.ax*pair.first.ax + pair.first.ay*pair.first.ay);
	diff.dal = (diff.dax*pair.first.ay - diff.day * pair.first.ax) / sqrt(pair.first.ax*pair.first.ax + pair.first.ay*pair.first.ay);

	//dr,dlの計算
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = pair.first.x;
	pos0.y = pair.first.y;
	pos0.z = pair.first.z;
	dir0.x = pair.first.ax;
	dir0.y = pair.first.ay;
	dir0.z = 1;
	pos1.x = pair.second.x;
	pos1.y = pair.second.y;
	pos1.z = pair.second.z;
	dir1.x = pair.second.ax;
	dir1.y = pair.second.ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	diff.dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	diff.dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);

	if (sqrt(pair.first.ax*pair.first.ax + pair.first.ay*pair.first.ay) < 0.001) {
		diff.dar = diff.day;
		diff.dal = diff.dax;
		diff.dr = diff.dy;
		diff.dl = diff.dx;
	}

}
void output_difference(std::string filename, std::vector<Difference>&diff) {

	std::ofstream ofs(filename);
	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(8) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(8) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(2) << std::setprecision(0) << itr->direction << " "
			<< std::setw(4) << std::setprecision(0) << itr->vertex_pl << " "
			<< std::setw(4) << std::setprecision(0) << itr->target_pl[0] << " "
			<< std::setw(4) << std::setprecision(0) << itr->target_pl[1] << " "
			<< std::setw(4) << std::setprecision(0) << itr->edge_pl[0] << " "
			<< std::setw(4) << std::setprecision(0) << itr->edge_pl[1] << " "
			<< std::setw(4) << std::setprecision(0) << itr->add_nseg << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(7) << std::setprecision(4) << itr->dax << " "
			<< std::setw(7) << std::setprecision(4) << itr->day << " "
			<< std::setw(8) << std::setprecision(1) << itr->dx << " "
			<< std::setw(8) << std::setprecision(1) << itr->dy << " "
			<< std::setw(7) << std::setprecision(4) << itr->dar << " "
			<< std::setw(7) << std::setprecision(4) << itr->dal << " "
			<< std::setw(8) << std::setprecision(1) << itr->dr << " "
			<< std::setw(8) << std::setprecision(1) << itr->dl << " "
			<< std::setw(7) << std::setprecision(4) << itr->s_dar << " "
			<< std::setw(7) << std::setprecision(4) << itr->s_dal << " "
			<< std::setw(7) << std::setprecision(4) << itr->s_dr << " "
			<< std::setw(7) << std::setprecision(4) << itr->s_dl << std::endl;
	}
}
std::vector<Momentum_recon::Mom_chain > format_change(std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&mom_ev) {
	std::vector<Momentum_recon::Mom_chain > ret;
	for (auto itr = mom_ev.begin(); itr != mom_ev.end(); itr++) {
		ret.push_back(itr->first);
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (itr2->base.size() > 0) {
				ret.push_back(*itr2);
			}
		}
	}
	return ret;

}

std::set<std::pair<int, int>> Get_penetrate_list(std::vector<Difference>&diff) {
	std::set<std::pair<int, int>> ret;
	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		double chi2 = pow(itr->s_dal, 2) + pow(itr->s_dar, 2) + pow(itr->s_dl, 2) + pow(itr->s_dr, 2);
		if (chi2 > 15)continue;
		ret.insert(std::make_pair(itr->groupid, itr->chainid));
	}
	return ret;
}
bool judege_Black(Momentum_recon::Mom_chain &chain, std::map<double, std::pair<double, double>> &vph_mip, double thr_sigma) {
	double ax = 0, ay = 0, vph = 0;
	int count = 0, count2 = 0;
	for (auto itr = chain.base.begin(); itr != chain.base.end(); itr++) {
		ax += itr->ax;
		ay += itr->ay;
		count++;
		vph += itr->m[0].ph % 10000;
		count2++;
		vph += itr->m[1].ph % 10000;
		count2++;
	}
	ax /= count;
	ay /= count;
	vph /= count2;

	double angle = sqrt(ax*ax + ay * ay);

	double sigma = 0;
	auto vph_angle = vph_mip.upper_bound(angle);
	if (vph_angle == vph_mip.begin()) {
		sigma = (vph - vph_angle->second.first) / vph_angle->second.second;
	}
	else if (vph_angle == vph_mip.end()) {
		sigma = (vph - vph_mip.rbegin()->second.first) / vph_mip.rbegin()->second.second;
	}
	else {
		double vph_mean, vph_sigma;
		auto val0 = std::next(vph_angle, -1);
		auto val1 = vph_angle;
		vph_mean = (val1->second.first - val0->second.first) / (val1->first - val0->first)*(angle - val0->first) + val0->second.first;
		vph_sigma = (val1->second.second - val0->second.second) / (val1->first - val0->first)*(angle - val0->first) + val0->second.second;
		sigma = (vph - vph_mean) / vph_sigma;
	}
	if (sigma > thr_sigma)return true;
	return false;
}
std::map<double, std::pair<double, double>> vph_mip_distribution() {
	std::map<double, std::pair<double, double>> ret;
	ret.insert(std::make_pair(0.05, std::make_pair(61.86, 6.8)));
	ret.insert(std::make_pair(0.15, std::make_pair(49.68, 7.6)));
	ret.insert(std::make_pair(0.25, std::make_pair(36.38, 5.0)));
	ret.insert(std::make_pair(0.35, std::make_pair(30.85, 4.0)));
	ret.insert(std::make_pair(0.45, std::make_pair(29.20, 3.8)));
	ret.insert(std::make_pair(0.55, std::make_pair(27.17, 3.9)));
	ret.insert(std::make_pair(0.65, std::make_pair(25.14, 3.8)));
	ret.insert(std::make_pair(0.80, std::make_pair(22.26, 3.6)));
	ret.insert(std::make_pair(1.00, std::make_pair(19.83, 3.3)));
	ret.insert(std::make_pair(1.20, std::make_pair(18.39, 3.2)));
	ret.insert(std::make_pair(1.40, std::make_pair(17.24, 3.1)));
	ret.insert(std::make_pair(1.70, std::make_pair(16.02, 3.0)));
	ret.insert(std::make_pair(2.10, std::make_pair(15.29, 3.0)));
	ret.insert(std::make_pair(2.50, std::make_pair(14.19, 3.0)));
	return ret;
}

mfile0::Mfile Momch_to_mfile(std::vector<Momentum_recon::Event_information> &ev_v, std::vector<Difference>&diff) {
		std::map<std::pair<int, int>, Difference> diff_map;
		std::pair<int, int>id;
	for (auto itr = diff.begin(); itr != diff.end(); itr++) {
		id.first = itr->groupid;
		id.second = itr->chainid;
		diff_map.insert(std::make_pair(id, *itr));
	}

	int gid = 0;
	std::vector<mfile0::M_Chain> chains;
	for (auto &ev : ev_v) {
		
		mfile0::M_Chain mu;
		for (auto &c : ev.chains) {
			if (c.chainid == 0) {
			mu	= momch_to_chain(c,ev.groupid);
			}
		}
		mfile0::M_Chain partner;

		for (auto &c : ev.chains) {
			if (c.base.size() <= 1)continue;
			if (c.chainid == 0) continue;
				id.first = ev.groupid;
			id.second = c.chainid;
			partner = momch_to_chain(c,ev.groupid);
			gid = c.chainid + ev.groupid * 100000;

			//stop like
			if (diff_map.count(id) == 0) {
				for (auto &b : mu.basetracks) {
					b.group_id = gid;
				}
				for (auto &b : partner.basetracks) {
					b.group_id = gid;
					b.flg_d[1] = c.direction;
				}
			}
			//penetrate like
			else {
				auto diff_param = diff_map.find(id);

				for (auto &b : mu.basetracks) {
					b.group_id = gid;
				}
				for (auto &b : partner.basetracks) {
					b.group_id = gid;
					b.flg_i[0] = diff_param->second.s_dal * 10;
					b.flg_i[1] = diff_param->second.s_dar * 10;
					b.flg_i[2] = diff_param->second.s_dl * 10;
					b.flg_i[3] = diff_param->second.s_dr * 10;
					b.flg_d[0] = (pow(diff_param->second.s_dal, 2) + pow(diff_param->second.s_dar, 2) + pow(diff_param->second.s_dl, 2) + pow(diff_param->second.s_dr, 2));
					b.flg_d[1] = c.direction;
				}
			}
			chains.push_back(mu);
			chains.push_back(partner);
		}
	}
	mfile0::Mfile ret;
	mfile0::set_header(3, 133, ret);
	ret.chains = chains;
	return ret;

}
mfile0::M_Chain momch_to_chain(Momentum_recon::Mom_chain&momch,int groupid) {
	mfile0::M_Chain ret;
	for (auto &b : momch.base) {
		mfile0::M_Base base;
		base.group_id = groupid;
		base.ph = b.m[0].ph + b.m[1].ph;
		base.pos = b.pl * 10 + 1;
		base.rawid = b.rawid;
		base.ax = b.ax;
		base.ay = b.ay;
		base.x = b.x;
		base.y = b.y;
		base.z = b.z;
		base.flg_i[0] = 0;
		base.flg_i[1] = 0;
		base.flg_i[2] = 0;
		base.flg_i[3] = 0;
		base.flg_d[0] = 0;
		base.flg_d[1] = 0;
		ret.basetracks.push_back(base);
	}
	ret.chain_id = momch.chainid;
	ret.nseg = ret.basetracks.size();
	ret.pos0 = ret.basetracks.begin()->pos;
	ret.pos1 = ret.basetracks.rbegin()->pos;
	return ret;
}