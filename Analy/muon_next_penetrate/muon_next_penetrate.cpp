#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#include <chrono>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

class Check_result {
public:
	int eventid, chainid;
	std::string str;
};
bool sort_chain_base(const mfile0::M_Base&left, const mfile0::M_Base&right) {
	return left.pos < right.pos;
}
bool sort_chain(const mfile0::M_Chain &left, const mfile0::M_Chain &right) {
	int gid_l = left.basetracks.begin()->group_id;
	int gid_r = right.basetracks.begin()->group_id;
	if (gid_l != gid_r)return gid_l < gid_r;

	int tid_l = left.chain_id;
	int tid_r = right.chain_id;
	if (tid_l != tid_r)return tid_l < tid_r;

	int tid2_l = left.basetracks.begin()->flg_i[1];
	int tid2_r = right.basetracks.begin()->flg_i[1];
	return tid2_l < tid2_r;
}

std::map<int, Check_result> read_file(std::string filename);
std::multimap<int, mfile0::M_Chain> chain_divide_event(std::vector<mfile0::M_Chain> &c);
mfile0::M_Chain chain_merge(mfile0::M_Chain&c0, mfile0::M_Chain&c1);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr,"usage:prg file-in-mfile file-in-result file-add-mfile file-all-mfile\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_result = argv[2];
	std::string file_out_mfile_add = argv[3];
	std::string file_out_mfile_all = argv[4];

	mfile0::Mfile m, out_add, out_all;
	mfile0::read_mfile(file_in_mfile, m);
	out_add.header = m.header;
	out_all.header = m.header;

	std::map<int,Check_result> check_res = read_file(file_in_result);

	std::multimap<int, mfile0::M_Chain> chains = chain_divide_event(m.chains);

	std::vector<mfile0::M_Chain>connect_chain;
	for (auto itr = check_res.begin(); itr != check_res.end(); itr++) {
		if (chains.count(itr->first) == 0) {
			fprintf(stderr, "%d %d %s not found 1\n", itr->second.eventid, itr->second.chainid, itr->second.str.c_str());
			continue;
		}
		if (itr->second.str != "connect")continue;

		int count = 0;
		mfile0::M_Chain c0, c1;

		auto range = chains.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.chain_id == 0) {
				c0 = res->second;
				count++;
			}
			if (res->second.chain_id ==itr->second.chainid) {
				c1 = res->second;
				count++;
			}
		}
		if (count != 2) {
			fprintf(stderr, "%d %d %s not found 2\n", itr->second.eventid, itr->second.chainid, itr->second.str.c_str());
			continue;
		}
		connect_chain.push_back(chain_merge(c0, c1));

	}

	printf("connect size = %d\n", connect_chain.size());

	out_add.chains = connect_chain;
	out_all.chains = connect_chain;
	std::set<int> input_eventid;
	for (auto itr = out_all.chains.begin(); itr != out_all.chains.end(); itr++) {
		input_eventid.insert(itr->basetracks.begin()->group_id);
	}
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (input_eventid.count(itr->basetracks.begin()->group_id) == 1)continue;
		if (itr->basetracks.begin()->flg_i[1] != 0)continue;
		out_all.chains.push_back(*itr);
		input_eventid.insert(itr->basetracks.begin()->group_id);
	}
	sort(out_add.chains.begin(), out_add.chains.end(), sort_chain);
	sort(out_all.chains.begin(), out_all.chains.end(), sort_chain);

	mfile0::write_mfile(file_out_mfile_add, out_add);
	mfile0::write_mfile(file_out_mfile_all, out_all);

}
std::map<int, Check_result> read_file(std::string filename) {
	std::ifstream ifs(filename);
	std::map<int, Check_result> ret;
	Check_result res;
	while (ifs >> res.eventid >> res.chainid >> res.str) {
		if (res.str != "connect"&&res.str != "kink"&&res.str != "divide") {
			printf("%d %d %s\n", res.eventid, res.chainid, res.str.c_str());
		}
		ret.insert(std::make_pair(res.eventid, res));
	}
	return ret;
}

std::multimap<int, mfile0::M_Chain> chain_divide_event(std::vector<mfile0::M_Chain> &c) {
	std::multimap<int, mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		ret.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}

	return ret;
}
mfile0::M_Chain chain_merge(mfile0::M_Chain&c0, mfile0::M_Chain&c1) {


	mfile0::M_Chain ret;
	std::set<int> input_pl;
	for (auto itr = c0.basetracks.begin(); itr != c0.basetracks.end(); itr++) {
		input_pl.insert(itr->pos / 10);
		ret.basetracks.push_back(*itr);
	}

	for (auto itr = c1.basetracks.begin(); itr != c1.basetracks.end(); itr++) {
		itr->flg_i[0] = 0;
		itr->flg_i[1] = 0;
		if (input_pl.count(itr->pos / 10) == 01)continue;
		ret.basetracks.push_back(*itr);
	}

	sort(ret.basetracks.begin(), ret.basetracks.end(), sort_chain_base);
	ret.chain_id = c0.chain_id;
	ret.nseg = ret.basetracks.size();
	ret.pos0 = c0.basetracks.begin()->pos;
	ret.pos1 = c1.basetracks.rbegin()->pos;
	return ret;
}
