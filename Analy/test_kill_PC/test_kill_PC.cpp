#include <iostream>
#include <vector>


int main() {
	int64_t INTERVAL = 10000000;

	std::vector<int64_t> num_v;
	int64_t cnt = 0;
	while (cnt >= 0) {
		if (cnt%INTERVAL == 0) {
			fprintf(stderr,"\r now cnt=%lld Memory %.1lf[GB]", cnt,cnt*sizeof(int64_t)*1./1000/1000/1000);
		}
		num_v.push_back(cnt);
		cnt++;
	}
	fprintf(stderr, "\r now cnt=%lld Memory %.1lf[GB]\n", cnt, cnt * sizeof(int64_t)*1. / 1000 / 1000 / 1000);
	fprintf(stderr, "prg finish\n", cnt, cnt * sizeof(int64_t)*1. / 1000 / 1000 / 1000);


}