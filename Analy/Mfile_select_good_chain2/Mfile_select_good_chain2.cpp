#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include<vector>


void select_best_chain(std::vector<mfile1::MFileChain> &c, std::vector< std::vector< mfile1::MFileBase>>&base, mfile1::MFileChain &sel_c, std::vector< mfile1::MFileBase>&sel_base);
void mfile_read_write_bin(std::string file_in_mfile, std::string file_out_mfile);
bool unique_base_to_chain(std::map<std::pair<int, int>, mfile1::MFileBase>&base_all, std::vector< mfile1::MFileBase>&c);
bool merge_chains(std::map<std::pair<int, int>, mfile1::MFileBase>&base_all, std::vector< mfile1::MFileBase>&c);
std::map<std::pair<int, int>, mfile1::MFileBase> divide_base(std::vector< std::vector< mfile1::MFileBase>>&base);

bool sort_base(const mfile1::MFileBase& left, mfile1::MFileBase&right) {
	return left.pos < right.pos;
}
void main(int argc, char **argv)
{
	if (argc != 3) {
		fprintf(stderr, "usage : prg_name [input m-file-bin] [output m-file-bin]\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];

	mfile_read_write_bin(file_in_mfile, file_out_mfile);

}
void mfile_read_write_bin(std::string file_in_mfile, std::string file_out_mfile) {

	std::ofstream ofs(file_out_mfile, std::ios::binary);
	std::ifstream ifs(file_in_mfile, std::ios::binary);


	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }
	//Mfile headerの書き込み
	//mfile info headerの書き込み
	ofs.write((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	ofs.write((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));

	std::vector< mfile1::MFileChain> group;
	std::vector< std::vector< mfile1::MFileBase>> all_basetracks;
	mfile1::MFileChain w_chain;
	std::vector< mfile1::MFileBase> w_base;
	uint64_t count = 0,r_base_num=0,r_chain_num=0,w_base_num=0,w_chain_num=0,r_group_num=0;
	int64_t gid = -1;
	for (int64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		//一旦chainを読む
		mfile1::MFileChain chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }
		r_chain_num++;

		std::vector< mfile1::MFileBase> basetracks;
		basetracks.reserve(chain.nseg);

		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			basetracks.emplace_back(base);
			r_base_num++;

		}

		//読んだchainが前のchainと同じgroupか確認
		if (c + 1 == mfile.info_header.Nchain) {
			if (gid != basetracks.begin()->group_id) {
				////////////////////////////
				//今までのgroupの書き出し
				r_group_num++;
				gid = basetracks.begin()->group_id;
				//gorupからbestなchainを選択
				if (group.size() == 1) {
					w_chain = group[0];
					w_base = all_basetracks[0];
				}
				else {
					select_best_chain(group, all_basetracks, w_chain, w_base);
				}
				//書き出し
				ofs.write((char*)& w_chain, sizeof(mfile1::MFileChain));
				w_chain_num++;
				assert(w_chain.nseg == w_base.size());
				for (int b = 0; b < w_chain.nseg; b++) {
					ofs.write((char*)& w_base[b], sizeof(mfile1::MFileBase));
					w_base_num++;
				}
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();
				////////////////////////////
				//最後のgroupの書き出し
				w_chain = chain;
				w_base = basetracks;
				r_group_num++;
				gid = basetracks.begin()->group_id;
				//書き出し
				ofs.write((char*)& w_chain, sizeof(mfile1::MFileChain));
				w_chain_num++;
				assert(w_chain.nseg == w_base.size());
				for (int b = 0; b < w_chain.nseg; b++) {
					ofs.write((char*)& w_base[b], sizeof(mfile1::MFileBase));
					w_base_num++;
				}
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();
			}
			else {
				//今のchainをpush back
				all_basetracks.emplace_back(basetracks);
				group.emplace_back(chain);
				r_group_num++;
				gid = basetracks.begin()->group_id;
				//gorupからbestなchainを選択
				if (group.size() == 1) {
					w_chain = group[0];
					w_base = all_basetracks[0];
				}
				else {
					select_best_chain(group, all_basetracks, w_chain, w_base);
				}
				//書き出し
				ofs.write((char*)& w_chain, sizeof(mfile1::MFileChain));
				w_chain_num++;
				assert(w_chain.nseg == w_base.size());
				for (int b = 0; b < w_chain.nseg; b++) {
					ofs.write((char*)& w_base[b], sizeof(mfile1::MFileBase));
					w_base_num++;
				}
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();

			}
		}
		else if (group.size() != 0 && gid != basetracks.begin()->group_id) {
			r_group_num++;
			gid = basetracks.begin()->group_id;
			//gorupからbestなchainを選択
			if (group.size() == 1) {
				w_chain = group[0];
				w_base = all_basetracks[0];
			}
			else {
				select_best_chain(group, all_basetracks, w_chain, w_base);
			}
			//書き出し
			ofs.write((char*)& w_chain, sizeof(mfile1::MFileChain));
			w_chain_num++;
			assert(w_chain.nseg == w_base.size());
			for (int b = 0; b < w_chain.nseg; b++) {
				ofs.write((char*)& w_base[b], sizeof(mfile1::MFileBase));
				w_base_num++;
			}
			group.clear();
			for (int64_t i = 0; i < all_basetracks.size(); i++) {
				all_basetracks[i].clear();
			}
			all_basetracks.clear();
		}
		else if (c == 0) {
			gid = basetracks.begin()->group_id;
		}
		all_basetracks.emplace_back(basetracks);
		group.emplace_back(chain);

	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

	if (r_chain_num != mfile.info_header.Nchain) { throw std::exception("Nchain is wrong."); }
	if (r_base_num != mfile.info_header.Nbasetrack) { throw std::exception("Nbasetrack is wrong."); }
	mfile.info_header.Nchain = w_chain_num;
	mfile.info_header.Nbasetrack = w_base_num;
	//file pointerを最初に戻してヘッダを書く
	ofs.clear();
	ofs.seekp(0, std::ios::beg);

	ofs.write((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	ofs.write((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));

	printf("mfile info\n");
	printf("group num =%lld\n", r_group_num);
	printf("chain num =%lld --> %lld\n", r_chain_num, w_chain_num);
	printf("base  num =%lld --> %lld\n", r_base_num, w_base_num);
}

void select_best_chain(std::vector<mfile1::MFileChain> &c, std::vector< std::vector< mfile1::MFileBase>>&base,mfile1::MFileChain &sel_c, std::vector< mfile1::MFileBase>&sel_base){
	
	std::map<std::pair<int, int>, mfile1::MFileBase> base_all = divide_base(base);
	if (unique_base_to_chain(base_all, sel_base)) {
		sort(sel_base.begin(), sel_base.end(), sort_base);
		sel_c.chain_id = c.begin()->chain_id;
		sel_c.nseg = sel_base.size();
		sel_c.pos0 = sel_base.begin()->pos;
		sel_c.pos1 = sel_base.rbegin()->pos;
		return;
	}
	if (merge_chains(base_all, sel_base)) {
		sort(sel_base.begin(), sel_base.end(), sort_base);
		sel_c.chain_id = c.begin()->chain_id;
		sel_c.nseg = sel_base.size();
		sel_c.pos0 = sel_base.begin()->pos;
		sel_c.pos1 = sel_base.rbegin()->pos;
		return;
	}

	std::vector<int> id;
	std::vector<mfile0::M_Chain> sel0;
	int nseg = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		nseg = std::max(nseg, itr->nseg);
	}
	for (int i = 0; i < c.size();i++) {
		if (nseg == c[i].nseg) {
			id.push_back(i);
		}
	}

	double d_lat = 1000000000;
	int sel_id;
	for (auto itr = id.begin(); itr != id.end();itr++) {
		if (itr == id.begin()) {
			d_lat = mfile1::angle_diff_dev_lat(base[*itr]);
			sel_id = *itr;
		}
		if (d_lat > mfile1::angle_diff_dev_lat(base[*itr])) {
			d_lat = mfile1::angle_diff_dev_lat(base[*itr]);
			sel_id = *itr;
		}
	}

	sel_c = c[sel_id];
	sel_base = base[sel_id];
}
std::map<std::pair<int,int>, mfile1::MFileBase> divide_base(std::vector< std::vector< mfile1::MFileBase>>&base) {
	std::map<std::pair<int, int>, mfile1::MFileBase> ret;
	for (int i = 0; i < base.size(); i++) {
		for (int j = 0; j < base[i].size(); j++) {
			ret.insert(std::make_pair(std::make_pair(base[i][j].pos, base[i][j].rawid), base[i][j]));
		}
	}
	return ret;

}
bool unique_base_to_chain(std::map<std::pair<int, int>, mfile1::MFileBase>&base_all,std::vector< mfile1::MFileBase>&c) {
	std::map<int, int> unique_count;
	c.clear();

	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		auto res = unique_count.insert(std::make_pair(itr->first.first, 1));
		if (!res.second)return false;
		c.push_back(itr->second);
	}
	
	return true;
}
bool merge_chains(std::map<std::pair<int, int>, mfile1::MFileBase>&base_all, std::vector< mfile1::MFileBase>&c) {
	std::map<int, int> unique_count;
	std::multimap<int, mfile1::MFileBase> base_map;
	c.clear();

	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		auto res = unique_count.insert(std::make_pair(itr->first.first, 1));
		if (!res.second) {
			res.first->second++;
		}
		base_map.insert(std::make_pair(itr->second.pos , itr->second));
	}
	int start_pl = -1;
	int double_count = 0;
	for (auto itr = unique_count.begin(); itr != unique_count.end(); itr++) {
		if (itr->second == 1) {
			start_pl = itr->first;
			break;
		}
		if (itr->second == 2) {
			double_count++;
		}
		if (double_count > 2)return false;
		if (itr->second > 2)return false;
	}
	if (start_pl < 0)return false;

	//開始basetrackの決定
	int count = 0;
	auto start = base_map.begin();
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		if (itr->second.pos == start_pl) {
			start = itr;
			c.push_back(itr->second);
			break;
		}
	}
	for (auto itr = std::next(start, 1); itr != base_map.end(); itr++) {
		count = base_map.count(itr->first);
		if (count == 1) {
			c.push_back(itr->second);
		}
		//2track以上ある場合
		else {
			//距離を比較。離れていたらmutli-->flgをtrue
			std::vector<mfile1::MFileBase> multi_base;
			mfile1::MFileBase cand;
			auto range = base_map.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				multi_base.push_back(res->second);
			}
			//近接なら良いほうをselにpush back
			double ex_x, ex_y, gap, dist;
			gap = multi_base.begin()->z - itr->second.z;
			ex_x = itr->second.x + itr->second.ax*(gap);
			ex_y = itr->second.y + itr->second.ay*(gap);
			for (int i = 0; i < multi_base.size(); i++) {
				if (i == 0) {
					dist = sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2));
					cand = multi_base[i];
				}
				if (dist > sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2))) {
					dist = sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2));
					cand = multi_base[i];
				}
			}
			c.push_back(cand);
		}

		itr = std::next(itr, count - 1);
	}

	auto start_r = base_map.rbegin();
	for (auto itr = base_map.rbegin(); itr != base_map.rend(); itr++) {
		if (itr->second.pos == start_pl) {
			start_r = itr;
		}
	}
	for (auto itr = std::next(start_r, 1); itr != base_map.rend(); itr++) {
		count = base_map.count(itr->first);
		if (count == 1) {
			c.push_back(itr->second);
		}
		//2track以上ある場合
		else {
			//距離を比較。離れていたらmutli-->flgをtrue
			std::vector<mfile1::MFileBase> multi_base;
			mfile1::MFileBase cand;
			auto range = base_map.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				multi_base.push_back(res->second);
			}
			//近接なら良いほうをselにpush back
			double ex_x, ex_y, gap, dist;
			gap = multi_base.begin()->z - itr->second.z;
			ex_x = itr->second.x + itr->second.ax*(gap);
			ex_y = itr->second.y + itr->second.ay*(gap);
			for (int i = 0; i < multi_base.size(); i++) {
				if (i == 0) {
					dist = sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2));
					cand = multi_base[i];
				}
				if (dist > sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2))) {
					dist = sqrt(pow(multi_base[i].x - ex_x, 2) + pow(multi_base[i].y - ex_y, 2));
					cand = multi_base[i];
				}
			}
			c.push_back(cand);
		}

		itr = std::next(itr, count - 1);
	}

	return true;

}
