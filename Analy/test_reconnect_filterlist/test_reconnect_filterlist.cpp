#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mfile-path out-start-list stop-list\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_txt0 = argv[2];
	std::string file_out_txt1= argv[3];
	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::set<std::pair<int, int>> stop_list,start_list;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		if (itr->pos0 / 10 > *m.header.all_pos.begin() / 10) {
			start_list.insert(std::make_pair(itr->pos0 / 10, itr->basetracks.begin()->rawid));
		}
		if (itr->pos1 / 10 < *m.header.all_pos.rbegin() / 10) {
			stop_list.insert(std::make_pair(itr->pos1 / 10, itr->basetracks.rbegin()->rawid));
		}
	}
	{
		std::ofstream ofs(file_out_txt0);
		for (auto itr = start_list.begin(); itr != start_list.end(); itr++) {
			ofs << std::right << std::fixed << std::setfill(' ')
				<< std::setw(4) << std::setprecision(0) << itr->first << " "
				<< std::setw(12) << std::setprecision(0) << itr->second << std::endl;
		}
	}
	{
		std::ofstream ofs(file_out_txt1);
		for (auto itr = stop_list.begin(); itr != stop_list.end(); itr++) {
			ofs << std::right << std::fixed << std::setfill(' ')
				<< std::setw(4) << std::setprecision(0) << itr->first << " "
				<< std::setw(12) << std::setprecision(0) << itr->second << std::endl;
		}
	}
}

