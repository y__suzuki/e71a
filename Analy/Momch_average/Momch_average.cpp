#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void output_file(std::string filename, std::vector<Momentum_recon::Mom_chain> &momch);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg file-in-momch file-out\n");
		fprintf(stderr, "mode=0:vph\n");
		fprintf(stderr, "mode=1:pixel\n");

		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_txt = argv[2];
	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);

	 output_file(file_out_txt, momch);

}
void output_file(std::string filename, std::vector<Momentum_recon::Mom_chain> &momch) {
	std::ofstream ofs(filename);

	int count[2] = { 0,0 };
	double sum[2] = { 0,0 };
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		count[0] = 0;
		count[1] = 0;
		sum[0] = 0;
		sum[1] = 0;
		if (!isfinite(itr->ecc_mcs_mom))continue;
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			sum[0] += itr2->m[0].ph;
			count[0]++;
			sum[0] += itr2->m[1].ph;
			count[0]++;
			if (itr2->m[0].hitnum > 0) {
				sum[1] += itr2->m[0].hitnum;
				count[1]++;
			}
			if (itr2->m[1].hitnum > 0) {
				sum[1] += itr2->m[1].hitnum;
				count[1]++;
			}
		}
		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(3) << std::setprecision(0) << itr->chainid << " "
			<< std::setw(10) << std::setprecision(0) << itr->unix_time << " "
			<< std::setw(3) << std::setprecision(0) << itr->tracker_track_id << " "
			<< std::setw(5) << std::setprecision(0) << itr->entry_in_daily_file << " "
			<< std::setw(3) << std::setprecision(0) << itr->stop_flg << " "
			<< std::setw(3) << std::setprecision(0) << itr->particle_flg << " "
			<< std::setw(7) << std::setprecision(1) << itr->ecc_range_mom << " "
			<< std::setw(7) << std::setprecision(1) << itr->ecc_mcs_mom << " "
			<< std::setw(7) << std::setprecision(1) << itr->bm_range_mom << " "
			<< std::setw(7) << std::setprecision(1) << itr->bm_curvature_mom << " "

			<< std::setw(10) << std::setprecision(2) << sum[0] * 1. / count[0] << " "
			<< std::setw(10) << std::setprecision(2) << sum[1] * 1. / count[1] << std::endl;
	}
}