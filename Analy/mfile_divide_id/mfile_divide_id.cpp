#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>

#include <set>

void divide_chain(mfile0::Mfile &m_in, mfile0::Mfile &m_sel, mfile0::Mfile &m_remain, std::set<int64_t> gid_sel);
std::set<int64_t> read_gid(std::string filename);



int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:in-mfile gid-list out-mfile-sel out-mfile-remain\n");
		exit(1);
	}
	std::string file_in_mfile0 = argv[1];
	std::string file_in_list = argv[2];
	std::string file_out_mfile0 = argv[3];
	std::string file_out_mfile1 = argv[4];

	mfile0::Mfile m_in, m_sel,m_remain;
	mfile0::read_mfile(file_in_mfile0, m_in);

	std::set<int64_t> gid = read_gid(file_in_list);
	divide_chain(m_in, m_sel, m_remain, gid);

	mfile0::write_mfile(file_out_mfile0, m_sel);
	mfile0::write_mfile(file_out_mfile1, m_remain);

}

std::set<int64_t> read_gid(std::string filename) {
	std::set<int64_t>ret;
	std::ifstream ifs(filename);
	int64_t gid;
	while (ifs >> gid) {
		ret.insert(gid);
	}
	return ret;
}

void divide_chain(mfile0::Mfile &m_in, mfile0::Mfile &m_sel, mfile0::Mfile &m_remain, std::set<int64_t> gid_sel) {
	m_sel.header = m_in.header;
	m_remain.header = m_in.header;
	int64_t gid;
	for (auto itr = m_in.chains.begin(); itr != m_in.chains.end(); itr++) {
		gid = itr->basetracks.begin()->group_id / 100000;
		if (gid_sel.count(gid) == 1) {
			m_sel.chains.push_back(*itr);
		}
		else {
			m_remain.chains.push_back(*itr);
		}


	}


}