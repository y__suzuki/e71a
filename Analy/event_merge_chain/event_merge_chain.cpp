#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void add_momch(std::vector<Momentum_recon::Event_information>& momch, std::vector<Momentum_recon::Event_information>& momch_add, int add_event, int add_chain);

int main(int argc,char **argv) {
	if (argc != 6) {
		fprintf(stderr, "usage\n");
		exit(1);
	}
	std::string file_in_momch_ori = argv[1];
	std::string file_in_momch_add = argv[2];
	int add_event = std::stoi(argv[3]);
	int add_chain = std::stoi(argv[4]);
	std::string file_out_momch=argv[5];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch_ori);
	std::vector<Momentum_recon::Event_information> momch_add = Momentum_recon::Read_Event_information_extension(file_in_momch_add);

	add_momch(momch, momch_add, add_event, add_chain);

	Momentum_recon::Write_Event_information_extension(file_out_momch, momch);
}
void add_momch(std::vector<Momentum_recon::Event_information>& momch, std::vector<Momentum_recon::Event_information>& momch_add, int add_event, int add_chain) {
	Momentum_recon::Mom_chain chain;
	for (auto &ev : momch_add) {
		if (ev.groupid != add_event)continue;
		for (auto &c : ev.chains) {
			if (c.chainid != add_chain)continue;
			chain = c;
		}
	}

	for (auto &ev : momch) {
		if (ev.groupid != add_event)continue;
		ev.chains.push_back(chain);
	}

}