#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

mfile0::Mfile mfile_converter(std::vector<Momentum_recon::Mom_chain>&momch);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:fileame\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_mfile = argv[2];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);

	mfile0::Mfile m=mfile_converter(momch);

	mfile1::write_mfile_extension(file_out_mfile, m);

}
mfile0::Mfile mfile_converter(std::vector<Momentum_recon::Mom_chain>&momch) {
	mfile0::Mfile ret;
	std::set<int> pos_set;
	int chainnum = 0;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		mfile0::M_Chain c;
		c.chain_id = itr->chainid;
		chainnum++;
		//if (chainnum > 1000)continue;
		for (auto itr2 = itr->base.begin(); itr2 != itr->base.end(); itr2++) {
			mfile0::M_Base b;
			b.ax = itr2->ax;
			b.ay = itr2->ay;
			b.x = itr2->x;
			b.y = itr2->y;
			b.z = itr2->z;
			b.group_id= itr->groupid;
			b.pos = itr2->pl * 10 + 1;
			b.rawid = itr2->rawid;
			b.ph = std::min(9999, (itr2->m[0].ph + itr2->m[1].ph) % 10000) + std::min(32, (itr2->m[0].ph + itr2->m[1].ph) / 100) * 10000;

			b.flg_i[0] = itr->particle_flg;
			b.flg_i[1] = 0;
			b.flg_i[2] = 0;
			b.flg_i[3] = 0;
			b.flg_d[0] = 0;
			b.flg_d[1] = 0;
			pos_set.insert(b.pos);
			c.basetracks.push_back(b);
		}
		c.nseg = c.basetracks.size();
		c.pos0 = c.basetracks.begin()->pos;
		c.pos1 = c.basetracks.rbegin()->pos;
		ret.chains.push_back(c);

	}
	mfile0::set_header(*pos_set.begin() / 10, *pos_set.rbegin() / 10, ret);
	return ret;
}