#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

void ShotID_trans(std::vector<vxx::micro_track_t>&micro, int NumberOfImager, int scanNUM, int scanID);
inline void ShotID_trans(vxx::micro_track_t&micro, int NumberOfImager, int scanNUM, int scanID) {
	if (scanNUM <= scanID) {
		printf("scanNum(%d)<=ScanID(%d)\n", scanNUM, scanID);
		exit(1);
	}
	uint32_t ShotID;
	int ViewID = 0, ImagerID = 0;
	ShotID = ((uint32_t)(uint16_t)micro.row << 16) | ((uint32_t)(uint16_t)micro.col);
	ViewID = ShotID / NumberOfImager;
	ImagerID = ShotID % NumberOfImager;

	ShotID = ViewID * (NumberOfImager*scanNUM) + (NumberOfImager*scanID) + ImagerID;
	micro.col = (int16_t)(ShotID & 0x0000ffff);
	micro.row = (int16_t)((ShotID & 0xffff0000) >> 16);
}

int main(int argc, char**argv) {
	if (argc != 8) {
		fprintf(stderr, "usage:prg in_fvxx pos zone out_fvxx NumberOfImager scanNUM scanID\n");
		exit(1);
	}
	std::string file_in_fvxx = argv[1];
	int pos = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_fvxx = argv[4];
	int NumberOfImager = std::stoi(argv[5]);
	int scanNUM = std::stoi(argv[6]);
	int scanID = std::stoi(argv[7]);
	//とりあえず 1G本
	std::vector<vxx::micro_track_t> micro;

	micro.reserve(1000 * 1000 * 1000 * 1);
	int64_t count = 0;
	vxx::FvxxReader fr;
	//ReadAll関数はbvxx中のBaseTrackを一度にすべて読みだす。
	//引数にはbvxxへのパス、pl、zoneを渡す。
	//ファイルを開けなかった場合などは空のvectorが返ってくる。
	if (fr.Begin(file_in_fvxx, pos, zone))
	{
		vxx::HashEntry h;
		vxx::micro_track_t m;
		while (fr.NextHashEntry(h))
		{
			while (fr.NextMicroTrack(m))
			{
				if (count % 1000000 == 0) {
					fprintf(stderr, "\r fvxx read ...%d", count);
				}
				count++;
				ShotID_trans(m, NumberOfImager, scanNUM, scanID);
				micro.push_back(m);
			}
		}
		fr.End();
	}
	fprintf(stderr, "\r fvxx read ...%d\n", count);

	vxx::FvxxWriter w;
	w.Write(file_out_fvxx, pos, zone, micro);
}
void ShotID_trans(std::vector<vxx::micro_track_t>&micro, int NumberOfImager, int scanNUM, int scanID) {
	if (scanNUM <= scanID) {
		printf("scanNum(%d)<=ScanID(%d)\n", scanNUM, scanID);
		exit(1);
	}
	uint32_t ShotID;
	int ViewID = 0, ImagerID = 0;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		ShotID = ((uint32_t)(uint16_t)itr->row << 16) | ((uint32_t)(uint16_t)itr->col);
		ViewID = ShotID / NumberOfImager;
		ImagerID = ShotID % NumberOfImager;

		ShotID = ViewID * (NumberOfImager*scanNUM) + (NumberOfImager*scanID) + ImagerID;
		itr->col = (int16_t)(ShotID & 0x0000ffff);
		itr->row = (int16_t)((ShotID & 0xffff0000) >> 16);
	}
}