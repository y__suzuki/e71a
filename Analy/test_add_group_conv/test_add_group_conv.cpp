#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class output_format {
public:
	int groupid, trackid;
	int num_comfirmed_path, num_cut_path, num_select_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> comfirmed_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> cut_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> select_path;
};

std::vector< output_format> read_chain_file_bin(std::string filename);
void Add_group(std::string filename, std::vector<output_format> &g);
bool checkFileExistence(const std::string& str);
void Write_group(std::string filename, std::vector<output_format> &g);


int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-origin-group file-add-group\n");
		exit(1);
	}
	std::string file_origin_group = argv[1];
	std::string file_add_group = argv[2];

	std::vector<output_format> ori, add;
	if (checkFileExistence(file_origin_group)) {
		ori = read_chain_file_bin(file_origin_group);
	}
	add = read_chain_file_bin(file_add_group);

	for (auto &g : add) {
		ori.push_back(g);
	}
	Write_group(file_origin_group, ori);
}

bool checkFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}

std::vector< output_format> read_chain_file_bin(std::string filename) {
	std::vector< output_format> ret;
	std::ifstream ifs(filename, std::ios::binary);
	output_format out;
	std::tuple<int, int, int, int> path;
	int id;
	int pl, rawid;
	int cnt = 0;
	while (ifs.read((char*)&out.groupid, sizeof(int))) {
		if (cnt % 10000==0) {
			fprintf(stderr, "\r read file... %d", cnt);
		}
		cnt++;

		ifs.read((char*)&out.trackid, sizeof(int));
		ifs.read((char*)&out.num_comfirmed_path, sizeof(int));
		ifs.read((char*)&out.num_cut_path, sizeof(int));
		ifs.read((char*)&out.num_select_path, sizeof(int));

		for (int i = 0; i < out.num_comfirmed_path; i++) {
			ifs.read((char*)&id, sizeof(int));
			ifs.read((char*)&std::get<0>(path), sizeof(int));
			ifs.read((char*)&std::get<1>(path), sizeof(int));
			ifs.read((char*)&std::get<2>(path), sizeof(int));
			ifs.read((char*)&std::get<3>(path), sizeof(int));
			out.comfirmed_path.push_back(std::make_pair(id, path));
		}
		for (int i = 0; i < out.num_cut_path; i++) {
			ifs.read((char*)&id, sizeof(int));
			ifs.read((char*)&std::get<0>(path), sizeof(int));
			ifs.read((char*)&std::get<1>(path), sizeof(int));
			ifs.read((char*)&std::get<2>(path), sizeof(int));
			ifs.read((char*)&std::get<3>(path), sizeof(int));
			out.cut_path.push_back(std::make_pair(id, path));
		}
		for (int i = 0; i < out.num_select_path; i++) {
			ifs.read((char*)&id, sizeof(int));
			ifs.read((char*)&std::get<0>(path), sizeof(int));
			ifs.read((char*)&std::get<1>(path), sizeof(int));
			ifs.read((char*)&std::get<2>(path), sizeof(int));
			ifs.read((char*)&std::get<3>(path), sizeof(int));
			out.select_path.push_back(std::make_pair(id, path));
		}

		ret.push_back(out);
		out.comfirmed_path.clear();
		out.cut_path.clear();
		out.select_path.clear();
	}
	fprintf(stderr, "\r read file... %d\n", cnt);

	return ret;
}


void Add_group(std::string filename, std::vector<output_format> &g) {
	std::ofstream ofs(filename, std::ios::app || std::ios::binary);

	double weight = 1;
	int link_num = 0;
	int all = g.size();
	int count = 0;
	for (auto itr = g.begin(); itr != g.end(); itr++) {

		if (count % 10000 == 0) {
			printf("\r write path %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		ofs.write((char*)& itr->groupid, sizeof(int));
		ofs.write((char*)& itr->trackid, sizeof(int));
		ofs.write((char*)& itr->num_comfirmed_path, sizeof(int));
		ofs.write((char*)& itr->num_cut_path, sizeof(int));
		ofs.write((char*)& itr->num_select_path, sizeof(int));

		for (auto itr2 = itr->comfirmed_path.begin(); itr2 != itr->comfirmed_path.end(); itr2++) {
			ofs.write((char*)& itr2->first, sizeof(int));
			ofs.write((char*)& std::get<0>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<1>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<2>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<3>(itr2->second), sizeof(int));
		}

		for (auto itr2 = itr->cut_path.begin(); itr2 != itr->cut_path.end(); itr2++) {
			ofs.write((char*)& itr2->first, sizeof(int));
			ofs.write((char*)& std::get<0>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<1>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<2>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<3>(itr2->second), sizeof(int));
		}

		for (auto itr2 = itr->select_path.begin(); itr2 != itr->select_path.end(); itr2++) {
			ofs.write((char*)& itr2->first, sizeof(int));
			ofs.write((char*)& std::get<0>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<1>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<2>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<3>(itr2->second), sizeof(int));
		}
	}
	printf("\r write path %d/%d(%4.1lf%%)\n", count, all, count*100. / all);


}
void Write_group(std::string filename, std::vector<output_format> &g) {
	std::ofstream ofs(filename,  std::ios::binary);

	double weight = 1;
	int link_num = 0;
	int all = g.size();
	int count = 0;
	for (auto itr = g.begin(); itr != g.end(); itr++) {

		if (count % 10000 == 0) {
			printf("\r write path %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		ofs.write((char*)& itr->groupid, sizeof(int));
		ofs.write((char*)& itr->trackid, sizeof(int));
		ofs.write((char*)& itr->num_comfirmed_path, sizeof(int));
		ofs.write((char*)& itr->num_cut_path, sizeof(int));
		ofs.write((char*)& itr->num_select_path, sizeof(int));

		for (auto itr2 = itr->comfirmed_path.begin(); itr2 != itr->comfirmed_path.end(); itr2++) {
			ofs.write((char*)& itr2->first, sizeof(int));
			ofs.write((char*)& std::get<0>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<1>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<2>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<3>(itr2->second), sizeof(int));
		}

		for (auto itr2 = itr->cut_path.begin(); itr2 != itr->cut_path.end(); itr2++) {
			ofs.write((char*)& itr2->first, sizeof(int));
			ofs.write((char*)& std::get<0>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<1>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<2>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<3>(itr2->second), sizeof(int));
		}

		for (auto itr2 = itr->select_path.begin(); itr2 != itr->select_path.end(); itr2++) {
			ofs.write((char*)& itr2->first, sizeof(int));
			ofs.write((char*)& std::get<0>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<1>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<2>(itr2->second), sizeof(int));
			ofs.write((char*)& std::get<3>(itr2->second), sizeof(int));
		}
	}
	printf("\r write path %d/%d(%4.1lf%%)\n", count, all, count*100. / all);


}