#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Event_information>extract_forward(std::vector<Momentum_recon::Event_information>&momch_ev);
std::vector<Momentum_recon::Event_information>extract_backward(std::vector<Momentum_recon::Event_information>&momch_ev);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-momch file-out\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch_name = argv[2];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	std::vector<Momentum_recon::Event_information>momch_forward = extract_forward(momch);
	std::vector<Momentum_recon::Event_information>momch_backward = extract_backward(momch);
	Momentum_recon::Write_Event_information_extension(file_out_momch_name + "_forward.momch", momch_forward);
	Momentum_recon::Write_Event_information_extension(file_out_momch_name + "_backward.momch", momch_backward);

}
std::vector<Momentum_recon::Event_information>extract_forward(std::vector<Momentum_recon::Event_information>&momch_ev) {
	std::vector<Momentum_recon::Event_information> ret;
	int all = 0, sel = 0;
	int vertex_pl;
	for (auto &ev : momch_ev) {
		Momentum_recon::Event_information ev_inf = ev;
		ev_inf.chains.clear();
		ev_inf.true_chains.clear();

		for (auto &c : ev.chains) {
			if (c.particle_flg == 13) {
				ev_inf.chains.push_back(c);
				continue;
			}
			all++;
			if (c.direction == 1) {
				ev_inf.chains.push_back(c);
				sel++;
			}
		}
		ret.push_back(ev_inf);
	}
	printf("forward attach %d --> %d\n", all, sel);
	return ret;
}
std::vector<Momentum_recon::Event_information>extract_backward(std::vector<Momentum_recon::Event_information>&momch_ev) {
	std::vector<Momentum_recon::Event_information> ret;
	int all = 0, sel = 0;
	int vertex_pl;
	for (auto &ev : momch_ev) {
		Momentum_recon::Event_information ev_inf = ev;
		ev_inf.chains.clear();
		ev_inf.true_chains.clear();

		for (auto &c : ev.chains) {
			if (c.particle_flg == 13) {
				ev_inf.chains.push_back(c);
				continue;
			}
			all++;
			if (c.direction == -1) {
				ev_inf.chains.push_back(c);
				sel++;
			}
		}
		ret.push_back(ev_inf);
	}
	printf("backward attach %d --> %d\n", all, sel);
	return ret;
}
