#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>



class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};
std::vector<linklet_header> read_linklet(std::string filename);
void write_linklet(std::string filename, std::vector<linklet_header>&link);

bool operator<(const linklet_header &left, const linklet_header &right) {
	if (left.pos0 != right.pos0)return left.pos0 < right.pos0;
	else if (left.pos1 != right.pos1)return left.pos1 < right.pos1;
	else if (left.raw0 != right.raw0)return left.raw0 < right.raw0;
	else return left.raw1 < right.raw1;
}

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg file-in file-out\n");
		exit(1);
	}
	std::string file_in_link = argv[1];
	std::string file_out_link = argv[2];
	std::vector<linklet_header>link= read_linklet(file_in_link);
	write_linklet(file_out_link, link);

}
std::vector<linklet_header> read_linklet(std::string filename) {
	std::ifstream ifs(filename);
	std::set<linklet_header> ret;
	int groupid, trackid, pl, rawid, linknum;
	while (ifs >> groupid >> trackid >> pl >> rawid >> linknum) {
		for (int i = 0; i < linknum; i++) {
			linklet_header link;
			ifs >> link.pos0 >> link.pos1 >> link.raw0 >> link.raw1;
			ret.insert(link);


		}
	}
	printf("read fin %d\n", ret.size());
	std::vector<linklet_header> ret_v(ret.begin(), ret.end());
	return ret_v;

}
void write_linklet(std::string filename, std::vector<linklet_header>&link) {

	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open ���s
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (link.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = link.size();
	linklet_header l;

	for (int i = 0; i < link.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& link[i], sizeof(linklet_header));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}
