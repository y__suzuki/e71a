#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "functions.lib")
#include <functions.hpp>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <chrono>
#include <filesystem>
#include <set>

class output_format_base {
public:
	int pl, rawid, ph0, ph1;
	double ax, ay, x, y, z;
};
class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};
class align_param {
public:
	int id,signal,ix,iy;
	//視野中心
	double x, y, z;
	//parameter(9);
	double dx, dy, dz, x_rot, y_rot, z_rot, x_shrink, y_shrink, z_shrink, yx_shear, zx_shear, zy_shear;

};
std::string Set_file_read_bvxx_path(std::string file_ECC_path, int pl, int area);
std::string Set_file_read_ali_path(std::string file_ECC_path, int pl[2], int area);
double nominal_gap(std::string file_in_ECC, int pl[2]);
std::multimap<int, int> Read_linklet(std::string filename);
std::vector<align_param> align_change_format(std::vector<corrmap0::Corrmap> &corr, double nominal_gap);
std::vector<vxx::base_track_t>select_track(std::vector<vxx::base_track_t> &base, double &x, double &y, double dist);
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t*>> make_base_pair(std::vector<vxx::base_track_t> &base1, std::map<int, vxx::base_track_t*> &base0_map, std::multimap<int, int> &connect_list);
void base_pair_trans(std::pair<vxx::base_track_t, vxx::base_track_t*>&base_pair, align_param&corr);
void do_fine_alignment(std::vector<align_param> &corr, std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1, std::multimap<int, int> &connect_list);
vxx::base_track_t basetrack_trans(vxx::base_track_t &base, align_param&corr);
void write_output_link(std::string filename, std::vector<output_format_link>&link);
output_format_link output_format(vxx::base_track_t &t1, vxx::base_track_t &t2);
void output_corrmap(std::string filename, std::vector<align_param> &corr);
int signal_count(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t*>>&pair);

void add_corrmap_edge(std::vector<align_param> &corr, std::vector<vxx::base_track_t> &base);
int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "prg ECC_path pl0 pl1 link-pl0-pl1 output-file\n");
		exit(1);
	}
	int area = 0;
	int pl0 = std::stoi(argv[2]);
	int pl1 = std::stoi(argv[3]);
	int pl[2];
	pl[0] = std::min(pl0, pl1);
	pl[1] = std::max(pl0, pl1);

	std::string file_ECC_path = argv[1];
	std::string file_Linklet_path = argv[4];
	std::string file_output = argv[5];

	double gap = nominal_gap(file_ECC_path, pl);
	//読み込みfile名設定
	std::string file_in_base[2];
	for (int i = 0; i < 2; i++) {
		file_in_base[i] = Set_file_read_bvxx_path(file_ECC_path, pl[i], area);
	}
	std::string file_in_ali = Set_file_read_ali_path(file_ECC_path, pl, area);


	//fileの存在確認・読み込み
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base[2];
	for (int i = 0; i < 2; i++) {
		if (!std::filesystem::exists(file_in_base[i])) {
			fprintf(stderr, "%s not exist\n", file_in_base[i].c_str());
			exit(1);
		}
		base[i] = br.ReadAll(file_in_base[i], pl[i], 0);
	}
	std::vector<corrmap0::Corrmap> corr;
	if (!std::filesystem::exists(file_in_ali)) {
		fprintf(stderr, "%s not exist\n", file_in_ali.c_str());
		exit(1);
	}
	corrmap0::read_cormap(file_in_ali, corr);

	std::multimap<int, int>base_pair;
	if (!std::filesystem::exists(file_Linklet_path)) {
		fprintf(stderr, "%s not exist\n", file_Linklet_path.c_str());
		exit(1);
	}
	base_pair = Read_linklet(file_Linklet_path);

	std::vector<align_param> ali_params = align_change_format(corr, gap);
	add_corrmap_edge(ali_params, base[1]);

	do_fine_alignment(ali_params, base[0], base[1], base_pair);
	output_corrmap(file_output, ali_params);

}

std::string Set_file_read_bvxx_path(std::string file_ECC_path, int pl, int area) {
	std::stringstream file_in_base;
	file_in_base << file_ECC_path << "\\Area" << area << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	return file_in_base.str();
}
std::string Set_file_read_ali_path(std::string file_ECC_path, int pl[2], int area) {
	std::stringstream file_in_ali;
	file_in_ali << file_ECC_path << "\\Area" << area << "\\0\\align\\corrmap-align-"
		<< std::setw(3) << std::setfill('0') << pl[0] << "-"
		<< std::setw(3) << std::setfill('0') << pl[1] << ".lst";
	return file_in_ali.str();

}
double nominal_gap(std::string file_in_ECC, int pl[2]) {

	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	if (z_map.count(pl[0]) + z_map.count(pl[1]) != 2) {
		fprintf(stderr, "exception nominal gap not found\n");
		exit(1);
	}
	//pl[0]のz=0の座標系
	return z_map[pl[1]] - z_map[pl[0]];
}

std::multimap<int, int> Read_linklet(std::string filename) {
	std::multimap<int, int> base_pair;

	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	output_format_link l;
	while (ifs.read((char*)& l, sizeof(output_format_link))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		base_pair.insert(std::make_pair(l.b[1].rawid, l.b[0].rawid));
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return base_pair;

}

std::vector<align_param> align_change_format(std::vector<corrmap0::Corrmap> &corr,double nominal_gap) {
	std::vector<align_param> ret;
	for (int i = 0; i < corr.size(); i++) {
		align_param param;
		param.id = i;
		param.ix = corr[i].notuse_i[1];
		param.iy = corr[i].notuse_i[2];
		param.dx = corr[i].position[4];
		param.dy = corr[i].position[5];
		param.dz = corr[i].dz;
		double factor = corr[i].position[0] * corr[i].position[3] - corr[i].position[1] * corr[i].position[2];
		param.x_shrink = sqrt(factor);
		param.y_shrink = sqrt(factor);
		param.z_shrink = 1;

		param.z_rot = atan((corr[i].position[2] / corr[i].position[0]));

		double area[4];
		area[0] = corr[i].areax[0] - corr[i].position[4];
		area[1] = corr[i].areax[1] - corr[i].position[4];
		area[2] = corr[i].areay[0] - corr[i].position[5];
		area[3] = corr[i].areay[1] - corr[i].position[5];

		corr[i].areax[0] = 1. / factor * (area[0] * corr[i].position[3] - area[2] * corr[i].position[1]);
		corr[i].areay[0] = 1. / factor * (area[2] * corr[i].position[0] - area[0] * corr[i].position[2]);
		corr[i].areax[1] = 1. / factor * (area[1] * corr[i].position[3] - area[3] * corr[i].position[1]);
		corr[i].areay[1] = 1. / factor * (area[3] * corr[i].position[0] - area[1] * corr[i].position[2]);

		param.x = (corr[i].areax[0] + corr[i].areax[1]) / 2;
		param.y = (corr[i].areay[0] + corr[i].areay[1]) / 2;
		param.z = nominal_gap;

		param.x_rot = 0;
		param.y_rot = 0;
		param.yx_shear = 0;
		param.zx_shear = 0;
		param.zy_shear = 0;

		ret.push_back(param);
	}
	return ret;
}

void add_corrmap_edge(std::vector<align_param> &corr, std::vector<vxx::base_track_t> &base) {
	double xmin, ymin, xmax, ymax;
	for(auto itr=base.begin();itr!=base.end();itr++){
		if (itr == base.begin()) {
			xmin = itr->x;
			ymin = itr->y;
			xmax = itr->x;
			ymax = itr->y;
		}
		xmin = std::min(itr->x, xmin);
		ymin = std::min(itr->y, ymin);
		xmax = std::max(itr->x, xmax);
		ymax = std::max(itr->y, ymax);
	}
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		xmin = std::min(itr->x, xmin);
		ymin = std::min(itr->y, ymin);
		xmax = std::max(itr->x, xmax);
		ymax = std::max(itr->y, ymax);
	}
	xmin -= 500;
	ymin -= 500;
	xmax += 500;
	ymax += 500;
	std::vector<std::pair<double, double>> edge_point;
	for (double x = xmin; x <= xmax + 2000; x += 2000) {
		for (double y = ymin; y <= ymax + 2000; y += 2000) {
			edge_point.push_back(std::make_pair(x, y));
		}
	}


	double dist = 0,cx,cy;
	int id = corr.size(),count=0;
	align_param param;
	std::vector<align_param> add_cand;
	for (auto itr = edge_point.begin(); itr != edge_point.end(); itr++) {
		if (count % 100 == 0) {
			printf("\r add align param %d/%d", count, edge_point.size());
		}
		count++;

		dist = 0;
		for (auto itr2 = corr.begin(); itr2 != corr.end(); itr2++) {
			cx = itr2->x;
			cy = itr2->y;
			if (itr2 == corr.begin()) {
				dist = (itr->first - cx)*(itr->first - cx) + (itr->second - cy)*(itr->second - cy);
				param = *itr2;
			}
			if (dist > (itr->first - cx)*(itr->first - cx) + (itr->second - cy)*(itr->second - cy)) {
				dist = (itr->first - cx)*(itr->first - cx) + (itr->second - cy)*(itr->second - cy);
				param = *itr2;
			}
		}
		if (sqrt(dist) < 1500)continue;
		param.x = itr->first;
		param.y = itr->second;
		param.id = id;
		id++;
		add_cand.push_back(param);
	}
	printf("\r add align param %d/%d\n", count, edge_point.size());

	for (auto itr = add_cand.begin(); itr != add_cand.end(); itr++) {
		corr.push_back(*itr);
	}

}

void do_fine_alignment(std::vector<align_param> &corr, std::vector<vxx::base_track_t> &base0, std::vector<vxx::base_track_t> &base1, std::multimap<int, int> &connect_list) {
	std::vector<vxx::base_track_t>base_sel;
	std::vector<output_format_link> out;
	std::map<int, vxx::base_track_t*> base0_map;
	for (auto itr = base0.begin(); itr!=base0.end(); itr++) {
		base0_map.insert(std::make_pair(itr->rawid, &(*itr)));
	}
	std::multimap<std::pair<int, int>, vxx::base_track_t*> base1_hash;
	double xmin, ymin, hash = 2000;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (itr == base1.begin()) {
			xmin = itr->x;
			ymin = itr->y;
		}
		xmin = std::min(itr->x, xmin);
		ymin = std::min(itr->y, ymin);
	}
	std::pair<int, int>id;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		id.first = (itr->x - xmin) / hash;
		id.second = (itr->y - ymin) / hash;
		base1_hash.insert(std::make_pair(id, &(*itr)));
	}

	std::vector<vxx::base_track_t> base1_cand;
	int count = 0,ix,iy;
	for (int i = 0; i < corr.size(); i++) {
		if (count % 100 == 0) {
			printf("\r do fine align %d/%d", count, corr.size());
		}
		count++;
		//if (count != 5000)continue;
		base1_cand.clear();
		ix = (corr[i].x - xmin) / hash;
		iy = (corr[i].y - ymin) / hash;
		for (int iix = ix - 1; iix <= ix + 1; iix++) {
			for (int iiy = iy - 1; iiy <= iy + 1; iiy++) {
				id.first = iix;
				id.second = iiy;
				if (base1_hash.count(id) == 0)continue;
				auto range = base1_hash.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					base1_cand.push_back(*(res->second));
				}
			}
		}

		base_sel = select_track(base1_cand, corr[i].x, corr[i].y, 2000);
		out.clear();
		auto base_pair = make_base_pair(base_sel, base0_map, connect_list);
		for (auto itr = base_pair.begin(); itr != base_pair.end(); itr++) {
			base_pair_trans(*itr, corr[i]);
			out.push_back(output_format(*itr->second, itr->first));

		}
		corr[i].signal = signal_count(base_pair);
		//write_output_link("out_link.bin", out);
	}
	printf("\r do fine align %d/%d\n", count, corr.size());

}

//ここのbaseはhash化予定
//変換するからcopy で渡す
std::vector<vxx::base_track_t>select_track(std::vector<vxx::base_track_t> &base, double &x, double &y, double dist) {
	std::vector<vxx::base_track_t> ret;
	double dx, dy;
	double dist_2 = dist * dist;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		dx = itr->x - x;
		dy = itr->y - y;
		if (dist_2 > dx*dx + dy * dy) {
			ret.push_back(*itr);
		}
	}
	return ret;

}
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t*>> make_base_pair(std::vector<vxx::base_track_t> &base1, std::map<int, vxx::base_track_t*> &base0_map, std::multimap<int, int> &connect_list) {

	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t*>> ret;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (connect_list.count(itr->rawid) == 0)continue;
		//rawidで一致する飛跡を探す
		auto range = connect_list.equal_range(itr->rawid);
		for (auto res = range.first; res != range.second; res++) {
			//base0_mapからrawidで探索
			if (base0_map.count(res->second) == 0)continue;
			auto connect_base = base0_map.at(res->second);
			ret.push_back(std::make_pair((*itr), connect_base));
		}
	}
	return ret;
}
void base_pair_trans(std::pair<vxx::base_track_t, vxx::base_track_t*>&base_pair, align_param&corr) {
	base_pair.second->z = 0;
	base_pair.first.z = corr.z;

	base_pair.first = basetrack_trans(base_pair.first, corr);
}
int signal_count(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t*>>&pair) {

	std::set<int> num;
	for (auto p : pair) {
		num.insert(p.second->rawid);
	}
	return num.size();
}
vxx::base_track_t basetrack_trans(vxx::base_track_t &base, align_param&corr) {
	vxx::base_track_t ret = base;
	matrix_3D::vector_3D pos0,pos1, shift;
	matrix_3D::matrix_33 x_rot(0, corr.x_rot), y_rot(1, corr.y_rot), z_rot(2, corr.z_rot);
	matrix_3D::matrix_33 expansion, shear,trans_mat;

	pos0.x = base.x;
	pos0.y = base.y;
	pos0.z = base.z;
	pos1.x = base.x + base.ax*(base.m[1].z - base.m[0].z);
	pos1.y = base.y + base.ay*(base.m[1].z - base.m[0].z);
	pos1.z = base.z + (base.m[1].z - base.m[0].z);

	shift.x = corr.dx;
	shift.y = corr.dy;
	shift.z = corr.dz;
	expansion.val[0][0] = corr.x_shrink;
	expansion.val[1][1] = corr.y_shrink;
	expansion.val[2][2] = corr.z_shrink;
	shear.val[0][0] = 1;
	shear.val[1][1] = 1;
	shear.val[2][2] = 1;
	shear.val[0][1] = corr.yx_shear;
	shear.val[0][2] = corr.zx_shear;
	shear.val[1][2] = corr.zy_shear;
	trans_mat.val[0][0] = 1;
	trans_mat.val[1][1] = 1;
	trans_mat.val[2][2] = 1;
	trans_mat.matrix_multiplication(shear);
	trans_mat.matrix_multiplication(expansion);
	trans_mat.matrix_multiplication(x_rot);
	trans_mat.matrix_multiplication(y_rot);
	trans_mat.matrix_multiplication(z_rot);

	pos0.matrix_multiplication(trans_mat);
	pos1.matrix_multiplication(trans_mat);
	pos0 = matrix_3D::addition(pos0, shift);
	pos1 = matrix_3D::addition(pos1, shift);
	//printf("x rot\n");
	//x_rot.Print();
	//printf("\ny rot\n");
	//y_rot.Print();
	//printf("\nz rot\n");
	//z_rot.Print();
	//printf("\nexpansion\n");
	//expansion.Print();
	//printf("\nshear\n");
	//shear.Print();
	ret.x = pos0.x;
	ret.y = pos0.y;
	ret.z = pos0.z;
	ret.ax = (pos0.x - pos1.x) / (pos0.z - pos1.z);
	ret.ay = (pos0.y - pos1.y) / (pos0.z - pos1.z);

	//printf("x %8.1lf --> %8.1lf\n", base.x, ret.x);
	//printf("y %8.1lf --> %8.1lf\n", base.y, ret.y);
	//printf("z %8.1lf --> %8.1lf\n", base.z, ret.z);
	//printf("ax %7.4lf --> %7.4lf\n", base.ax, ret.ax);
	//printf("ay %7.4lf --> %7.4lf\n", base.ay, ret.ay);
	//printf("\n");

	return ret;
}

output_format_link output_format(vxx::base_track_t &t1, vxx::base_track_t &t2) {
	output_format_link l;
	l.b[0].pl = t1.pl;
	l.b[0].rawid = t1.rawid;
	l.b[0].ph0 = t1.m[0].ph;
	l.b[0].ph1 = t1.m[1].ph;
	l.b[0].ax = t1.ax;
	l.b[0].ay = t1.ay;
	l.b[0].x = t1.x;
	l.b[0].y = t1.y;
	l.b[0].z = t1.z;

	l.b[1].pl = t2.pl;
	l.b[1].rawid = t2.rawid;
	l.b[1].ph0 = t2.m[0].ph;
	l.b[1].ph1 = t2.m[1].ph;
	l.b[1].ax = t2.ax;
	l.b[1].ay = t2.ay;
	l.b[1].x = t2.x;
	l.b[1].y = t2.y;
	l.b[1].z = t2.z;

	l.Calc_difference();
	return l;
}

void output_format_link::Calc_difference() {

	dax = b[1].ax - b[0].ax;
	day = b[1].ay - b[0].ay;
	dx = b[1].x - b[0].x - (b[0].ax + b[1].ax) / 2 * (b[1].z - b[0].z);
	dy = b[1].y - b[0].y - (b[0].ay + b[1].ay) / 2 * (b[1].z - b[0].z);
	dar = (dax*b[0].ax + day * b[0].ay) / sqrt(b[0].ax*b[0].ax + b[1].ay*b[1].ay);
	dal = (dax*b[0].ay - day * b[0].ax) / sqrt(b[0].ax*b[0].ax + b[1].ay*b[1].ay);
	//dr,dlの計算
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = b[0].x;
	pos0.y = b[0].y;
	pos0.z = b[0].z;
	dir0.x = b[0].ax;
	dir0.y = b[0].ay;
	dir0.z = 1;
	pos1.x = b[1].x;
	pos1.y = b[1].y;
	pos1.z = b[1].z;
	dir1.x = b[1].ax;
	dir1.y = b[1].ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);

}

void write_output_link(std::string filename, std::vector<output_format_link>&link) {
	std::ofstream ofs(filename, std::ios::binary);
	int count = 0;
	for (auto l : link) {
		if (count % 10000 == 0) {
			printf("\r write linklet %10d/%10d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;
		ofs.write((char*)& l, sizeof(output_format_link));
	}

	printf("\r write linklet %10d/%10d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

}

void output_corrmap(std::string filename, std::vector<align_param> &corr) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (corr.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)", count, int(corr.size()), count*100. / corr.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(0) << itr->id << " "
				<< std::setw(4) << std::setprecision(0) << itr->ix << " "
				<< std::setw(4) << std::setprecision(0) << itr->iy << " "
				<< std::setw(4) << std::setprecision(0) << itr->signal << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << " "
				<< std::setw(8) << std::setprecision(1) << itr->z << " "
				<< std::setw(9) << std::setprecision(7) << itr->x_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->y_rot << " "
				<< std::setw(9) << std::setprecision(7) << itr->z_rot << " "
				<< std::setw(8) << std::setprecision(6) << itr->x_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->y_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->z_shrink << " "
				<< std::setw(8) << std::setprecision(6) << itr->yx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zx_shear << " "
				<< std::setw(8) << std::setprecision(6) << itr->zy_shear << " "
				<< std::setw(8) << std::setprecision(2) << itr->dx << " "
				<< std::setw(8) << std::setprecision(2) << itr->dy << " "
				<< std::setw(8) << std::setprecision(2) << itr->dz << std::endl;
		}
		fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)\n", count, int(corr.size()), count*100. / corr.size());

	}



}