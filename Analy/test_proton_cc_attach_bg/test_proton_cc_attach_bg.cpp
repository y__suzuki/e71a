#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <omp.h>

std::vector<Momentum_recon::Event_information> proton_selection(std::vector<Momentum_recon::Event_information>&momch);
std::vector<Momentum_recon::Event_information> muon_shift(std::vector<Momentum_recon::Event_information>&momch, double shift_x, double shift_y);

bool judge_proton_attach(Momentum_recon::Mom_basetrack &b_muon, Momentum_recon::Mom_chain&c_proton);
std::vector<Momentum_recon::Event_information> attach_search(std::vector<Momentum_recon::Event_information> &momch_muon, std::vector<Momentum_recon::Event_information> &momch_proton, int ix, int iy);
int use_thread(double ratio, bool output);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "uasge:input-proton input-muon output\n");
		exit(1);
	}
	std::string file_in_momch_proton = argv[1];
	std::string file_in_momch_muon = argv[2];
	std::string file_out_momch= argv[3];
	std::vector<Momentum_recon::Event_information> momch_proton = Momentum_recon::Read_Event_information_extension(file_in_momch_proton);
	std::vector<Momentum_recon::Event_information> momch_muon = Momentum_recon::Read_Event_information_extension(file_in_momch_muon);
	std::vector<Momentum_recon::Event_information> momch_out;
	//protonをvector momchに変換

	//muonをx-y shiftのloop
	int all = 51 * 51, count = 0;
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < all; i++) {
		int ix = i / 51;
		int iy = i % 51;
		//1cm pich でシフト
		double shift_x = (ix - 25) * 10000;
		//1cm pich でシフト
		double shift_y = (iy - 25) * 10000;
#pragma omp critical
		printf("\r BG search %d/%d(%4.1lf%%)", count, all, count*100. / all);
#pragma omp atomic
		count++;
		if (ix == 25 && iy == 25)continue;

		std::vector<Momentum_recon::Event_information> momch_muon_shift = muon_shift(momch_muon, shift_x, shift_y);
		if (momch_muon_shift.size() < 1)continue;

		//muon最上流とのattach search
		//-->event_information化
		std::vector<Momentum_recon::Event_information> ev_cand = attach_search(momch_muon_shift, momch_proton, ix, iy);
#pragma omp critical
		for (int j = 0; j < ev_cand.size(); j++) {
			momch_out.push_back(ev_cand[j]);
		}

	}
	printf("\r BG search %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

	printf("detect bg = %d\n", momch_out.size());
	if (momch_out.size() > 0) {
		Momentum_recon::Write_Event_information_extension(file_out_momch, momch_out);
	}
	//出力
}

std::vector<Momentum_recon::Event_information> muon_shift(std::vector<Momentum_recon::Event_information>&momch,double shift_x,double shift_y) {
	std::vector<Momentum_recon::Event_information> ret;
	for (auto &ev : momch) {
		Momentum_recon::Event_information ev_tmp;
		ev_tmp = ev;
		ev_tmp.chains.clear();
		ev_tmp.chains.push_back(ev.chains[0]);
		for (auto &b : ev_tmp.chains[0].base) {
			b.x += shift_x;
			b.y += shift_y;
		}
		double up_x, up_y;
		up_x = ev_tmp.chains[0].base.rbegin()->x;
		up_y = ev_tmp.chains[0].base.rbegin()->y;
		if (up_x < 30000)continue;
		if (220000 < up_x)continue;
		if (up_y < 30000)continue;
		if (220000 < up_y)continue;
		ret.push_back(ev_tmp);
	}
	return ret;
}

std::vector<Momentum_recon::Event_information> attach_search(std::vector<Momentum_recon::Event_information> &momch_muon, std::vector<Momentum_recon::Event_information> &momch_proton,int ix,int iy) {
	std::vector<Momentum_recon::Event_information> ret;
	for (auto &mu : momch_muon) {
		Momentum_recon::Event_information ev_tmp;
		ev_tmp = mu;
		ev_tmp.groupid = ev_tmp.groupid * 10000+ ix * 100 + iy;

		Momentum_recon::Mom_basetrack b_muon = *mu.chains[0].base.rbegin();

		for (auto &p : momch_proton) {
			if (!judge_proton_attach(b_muon, p.chains[0]))continue;
			ev_tmp.chains.push_back(p.chains[0]);
		}
		if (ev_tmp.chains.size() > 1) {
			ret.push_back(ev_tmp);
		}
	}
	return ret;

}

void Set_search_z(double z_range[2], int muonPL) {
	bool iron = false;
	bool water = false;
	bool other = false;
	if (muonPL <= 15)iron = true;
	else if (16 <= muonPL && muonPL % 2 == 0)iron = true;
	else if (16 <= muonPL && muonPL % 2 == 1)water = true;
	else other = true;

	if (water) {
		//water-emulsion-iron-emulsion-water- (+a)
		z_range[0] = -2300 - 350 - 110 * 2 ;
		//z_range[1] = +210 + 70 + 30;
		z_range[1] = 0;
	}
	else {
		//iron-emulsion-water- (+a)
		z_range[0] = -500 - 350 - 70;
		//z_range[1] = +210 + 70 + 30;
		z_range[1] = 0;
	}
}

bool judge_proton_attach(Momentum_recon::Mom_basetrack &b_muon, Momentum_recon::Mom_chain&c_proton) {
	double angle_accuracy_intercept_mu = 0.04;
	double angle_accuracy_slope_mu = 0.04;
	double angle_accuracy_intercept = 0.04;
	double angle_accuracy_slope = 0.04;

	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	pos0.x = b_muon.x;
	pos0.y = b_muon.y;
	pos0.z = 0;

	dir0.x = b_muon.ax;
	dir0.y = b_muon.ay;
	dir0.z = 1;

	double z_range[2];
	Set_search_z(z_range, b_muon.pl);

	for (int i = 0; i < c_proton.base.size(); i++) {
		if (c_proton.base[i].pl != b_muon.pl)continue;
		pos1.x = c_proton.base[i].x;
		pos1.y = c_proton.base[i].y;
		pos1.z = 0;

		dir1.x = c_proton.base[i].ax;
		dir1.y = c_proton.base[i].ay;
		dir1.z = 1;

		double muon_angle_acc = angle_accuracy_intercept_mu + angle_accuracy_slope_mu * sqrt(dir0.x*dir0.x + dir0.y*dir0.y);
		double partner_angle_acc;


		double md, extra[2], allowance, angle;
		matrix_3D::vector_3D pos_all, dir_all;

		md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
		partner_angle_acc = angle_accuracy_intercept + angle_accuracy_slope * sqrt(dir1.x*dir1.x + dir1.y*dir1.y);

		allowance = pow(fabs(muon_angle_acc*extra[0]) + 5, 2) + pow(fabs(partner_angle_acc*extra[1]) + 5, 2);
		return md * md < allowance;
	}
	return false;
}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
