#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
#pragma comment(lib, "VxxReader.lib")

#include <set>
void base_corr_inverse(std::vector<netscan::base_track_t> &base);
void base_base_matching(netscan::base_track_t &target, std::vector<netscan::base_track_t> pred, std::vector<netscan::base_track_t> base, double &shift_x, double &shift_y, double &gap);
void search_microtrack(netscan::base_track_t base, std::string file_in_fvxx, int pos, double shift_x, double shift_y, double gap, double &z, std::vector<netscan::micro_track_t> &m, std::vector<corrmap0::CorrmapDC> corr);
	void fit_line(std::vector<double>x, std::vector<double>y, double &slope, double &intercept);
	void fit_line_intercept0(std::vector<double>x, std::vector<double>y, double &slope);
	void micro_format(std::vector<vxx::micro_track_t>m0, std::vector < netscan::micro_track_t> &m1);
	void micro_dc_correction(std::vector<vxx::micro_track_t> &m, std::vector<corrmap0::CorrmapDC>corr, double x, double y, int pos);

int main(int argc, char **argv) {
	if (argc != 9) {
		fprintf(stderr, "usage:prg prediction-bvxx pl input-fvxx pos input-fvxx pos input-bvxx dc-map\n");
		exit(1);
	}
	std::string file_in_pred = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_fvxx1 = argv[3];
	int pos1 = std::stoi(argv[4]);
	std::string file_in_fvxx2 = argv[5];
	int pos2 = std::stoi(argv[6]);
	std::string file_in_bvxx = argv[7];
	std::string file_in_dc = argv[8];

	std::vector<netscan::base_track_t> pred;
	netscan::read_basetrack_extension(file_in_pred, pred, pl, 0);
	base_corr_inverse(pred);

	std::vector<netscan::base_track_t> base;
	netscan::read_basetrack_extension(file_in_bvxx, base, pl, 0);

	std::vector<netscan::micro_track_t> micro;
	std::vector < netscan::base_track_t >out_base;
	std::multimap<int, int>micro_id;
	std::vector<corrmap0::CorrmapDC> corr;
	corrmap0::read_cormap_dc(file_in_dc, corr,0);

	std::set<int> id_list;
	std::ifstream ifs("F:\\NINJA\\E71a\\ECC5\\Layer32\\work\\Efficiency\\ManualCheck\\PL004\\200415\\b004_nohit2.txt");
	double buf_d[3];
	int buf_i[3];
	while (ifs >> buf_i[0] >> buf_i[1] >> buf_d[0] >> buf_d[1] >> buf_d[2] >> buf_i[2]) {
		id_list.insert(buf_i[1]);
	}
	ifs.close();

	//std::map<int,std::pair<int,int>> detect_map;
	//ifs.open("detect.txt");
	//while (ifs >> buf_i[0] >> buf_i[1] >> buf_i[2]) {
	//	detect_map.insert(std::make_pair(buf_i[0], std::make_pair(buf_i[1], buf_i[2])));
	//}
	//ifs.close();

	//とりあえず1本で実装
	netscan::base_track_t target;
	double shift_x, shift_y, gap;
	double z1=0, z2=0;
	int cont = 0;
	for (int i = 0; i < pred.size(); i++)
	{
		//if (i != 388)continue;
		//if (sqrt(pow(pred[i].ax,2)+ pow(pred[i].ay, 2))<1.0)continue;
		//cont++;
		//if (cont > 5)continue;
		//if (id_list.count(pred[i].m[0].isg) == 0)continue;
		//auto res = detect_map.find(pred[i].m[0].isg);
		//if (res == detect_map.end())continue;

		micro.clear();
		out_base.clear();
		target = pred[i];
		//alignmentとる
		base_base_matching(target, pred, base, shift_x, shift_y, gap);
		//pos1探す
		search_microtrack(target, file_in_fvxx1, pos1, shift_x, shift_y, gap, z1, micro,corr);
		//pos2探す
		search_microtrack(target, file_in_fvxx2, pos2, shift_x, shift_y, gap, z2, micro,corr);
		//zの設定
		if (isnan(z1) && isnan(z2)) {
			//z1,z2ともにnan
			z1 = 0;
			z2 = 210;
		}
		else if (isnan(z1)) {
			//z1のみnan
			z1 = z2 - 210;
		}
		else if (isnan(z2)) {
			z2=z1+210;
		}
		target.m[0].z = z1;
		target.m[1].z = z2;
		out_base.push_back(target);

		//int m_num = 0;
		//for (auto itr = micro.begin(); itr != micro.end();itr++) {
		//	if (itr->pos % 10 == 1) {
		//		if (itr->rawid == res->second.first)m_num++;
		//	}
		//	if (itr->pos % 10 == 2) {
		//		if (itr->rawid == res->second.second)m_num++;
		//	}
		//}
		//if (m_num != 2) {
		//	printf("m num = %d\n");
		//	printf("fname = %05d\n",i);
		//	printf("isg = %d, raw1=%d, raw2=%d\n", res->second.first, res->second.second);
		//	system("pause");
		//}
		//else {
		//	printf("m num = %d --> success\n",m_num);

		//}
		char file_out_micro[256];
		char file_out_base[256];
		sprintf(file_out_micro, "pred_%05d.bmt", i);
		sprintf(file_out_base, "pred_%05d.bbt", i);
		//netscan::write_basetrack_bin(file_out_base, out_base);
		//netscan::write_microtrack_bin(file_out_micro, micro);
		for (auto itr = micro.begin(); itr != micro.end(); itr++) {
			micro_id.insert(std::make_pair(itr->pos, itr->rawid));
		}
	}
	std::ofstream ofs("output_micro.lst");
	for (auto itr = micro_id.begin(); itr != micro_id.end(); itr++) {
		ofs << itr->first << " " << itr->second << std::endl;
	}

}
void base_corr_inverse(std::vector<netscan::base_track_t> &base) {
	//correction values
	double anlge_corr = 0.951;
	double slope = -1.53*pow(10, -9);
	double intercept = 1.0;

	double Area_y[2], Area_x[2], x_center;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			Area_y[0] = itr->y;
			Area_y[1] = itr->y;
		}
		if (Area_y[0] > itr->y) {
			Area_y[0] = itr->y;
		}
		if (Area_y[1] < itr->y) {
			Area_y[1] = itr->y;
		}
	}
	int count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		//yの下限から1cmのトラックのみ使用
		//xのcenterの決定
		if (Area_y[0] + 10000 < itr->y)continue;
		if (count == 0) {
			Area_x[0] = itr->x;
			Area_x[1] = itr->x;
			count++;
		}
		if (Area_x[0] > itr->x) {
			Area_x[0] = itr->x;
		}
		if (Area_x[1] < itr->x) {
			Area_x[1] = itr->x;
		}
	}
	x_center = (Area_x[1] - Area_x[0]) / 2;
	double shrink_x;
	double tmp_x;

	for (auto itr = base.begin(); itr != base.end(); itr++) {

		shrink_x = slope * itr->y + intercept;
		tmp_x = itr->x;
		//printf("y shrinkx %8.1lf %8.7lf\n",itr->y,shrink_x);
		itr->x = (tmp_x - x_center)*(shrink_x)+x_center;

	}
	printf("trapezoid correction\n");
	printf("shrink value= 1+y_position*%12.11lf\n", slope);
	printf("calc: x-->(x-x_center)*shrink+x_center\n");

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		itr->ax = itr->ax*(1. / anlge_corr);
		itr->ay = itr->ay*(1. / anlge_corr);
	}
	printf("basetrack angle trans *%5.4lf\n", 1.0 / anlge_corr);
}
void base_base_matching(netscan::base_track_t &target, std::vector<netscan::base_track_t> pred, std::vector<netscan::base_track_t> base, double &shift_x, double &shift_y, double &gap) {

	std::vector<netscan::base_track_t> pred_sel;
	std::vector<netscan::base_track_t> base_sel;

	double area = 5000;
	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		if (fabs(itr->x - target.x) > area)continue;
		if (fabs(itr->y - target.y) > area)continue;
		pred_sel.push_back(*itr);
	}
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (fabs(itr->x - target.x) > area)continue;
		if (fabs(itr->y - target.y) > area)continue;
		base_sel.push_back(*itr);
	}
	std::vector<std::pair<netscan::base_track_t, netscan::base_track_t>> match;
	double diff_pos[2], diff_ang[2], angle;
	for (auto itr = pred_sel.begin(); itr != pred_sel.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		for (auto itr2 = base_sel.begin(); itr2 != base_sel.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.05 * angle + 0.02)continue;
			if (fabs(diff_ang[1]) > 0.02)continue;
			if (fabs(diff_pos[0]) > 10 * angle + 15)continue;
			if (fabs(diff_pos[1]) > 15)continue;
			match.push_back(std::make_pair(*itr, *itr2));
		}
	}

	std::vector<double>dx, ax, dy, ay;
	std::vector<double>angle_v, dr;
	double tmp[2];

	for (auto itr = match.begin(); itr != match.end(); itr++) {
		ax.push_back(itr->first.ax);
		ay.push_back(itr->first.ay);
		dx.push_back(itr->first.x - itr->second.x);
		dy.push_back(itr->first.y - itr->second.y);
		tmp[0] = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);
		tmp[1] = ((itr->first.x - itr->second.x)*itr->first.ax + (itr->first.y - itr->second.y)*itr->first.ay) / tmp[0];
		angle_v.push_back(tmp[0]);
		dr.push_back(tmp[1]);

	}
	double slope;
	fit_line(ax, dx, slope, shift_x);
	fit_line(ay, dy, slope, shift_y);
	fit_line_intercept0(angle_v, dr, gap);

	printf("shift x=%lf\n", shift_x);
	printf("shift y=%lf\n", shift_y);
	printf("gap=%lf\n", gap);

	target.x = target.x - shift_x - target.ax*gap;
	target.y = target.y - shift_y - target.ay*gap;
	//for (auto itr = pred_sel.begin(); itr != pred_sel.end(); itr++) {
	//	itr->x -= shift_x;
	//	itr->y -= shift_y;
	//	itr->x = itr->x - itr->ax*gap;
	//	itr->y = itr->y - itr->ay*gap;
	//}

}
void fit_line(std::vector<double>x, std::vector<double>y, double &slope, double &intercept) {
	if (x.size() != y.size()) {
		printf("exeption\n");
		exit(1);
	}
	double x1, y1, x2, y2, xy, n;
	x1 = 0;
	y1 = 0;
	x2 = 0;
	y2 = 0;
	xy = 0;
	n = 0;
	for (int i = 0; i < x.size(); i++) {
		x1 += x[i];
		y1 += y[i];
		x2 += x[i] * x[i];
		xy += x[i] * y[i];
		n += 1;
	}
	slope = (n*xy - x1 * y1) / (n*x2 - x1 * x1);
	intercept = (x2*y1 - xy * x1) / (n*x2 - x1 * x1);
}
void fit_line_intercept0(std::vector<double>x, std::vector<double>y, double &slope){
	if (x.size() != y.size()) {
		printf("exeption\n");
		exit(1);
	}
	double x2, xy;
	x2 = 0;
	xy = 0;
	for (int i = 0; i < x.size(); i++) {
		x2 += x[i] * x[i];
		xy += x[i] * y[i];
	}
	slope = xy / x2;
}
void search_microtrack(netscan::base_track_t base, std::string file_in_fvxx, int pos, double shift_x, double shift_y, double gap, double &z, std::vector<netscan::micro_track_t> &m, std::vector<corrmap0::CorrmapDC> corr) {

	double area_cut = 100;

	if (pos % 10 == 2) {
		base.x = base.x + base.ax * 210;
		base.y = base.y + base.ay * 210;
	}
	vxx::FvxxReader fr;
	std::vector<vxx::CutArea> area;
	area.push_back(vxx::CutArea(base.x - area_cut, base.x + area_cut, base.y - area_cut, base.y + area_cut));//xmin, xmax, ymin, ymax
	std::vector<vxx::micro_track_t> micro = fr.ReadAll(file_in_fvxx, pos, 0, vxx::opt::a = area);
	printf("micro num=%d-->", micro.size());

	//micro_correction(dc)

	micro_dc_correction(micro, corr, base.x, base.y, pos);




	//char out_micro_name[256];
	//sprintf(out_micro_name, "out_f%04d.vxx", pos);
	//netscan::write_microtrack_vxx(out_micro_name, out_micro, pos, 0);

	z = 0;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		if (pos % 10 == 2) {
			z += itr->z1;
		}
		else {
			z += itr->z2;
		}
	}
	z = z / micro.size();





	//radial lateralで絞込
	std::vector<vxx::micro_track_t> micro_sel;
	double diff_pos[2], diff_ang[2], angle;
	double x, y;
	angle = sqrt(base.ax * base.ax + base.ay*base.ay);
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		if (itr->pos % 10 == 1) {
			x = itr->x + (itr->z2 - itr->z1)/2*itr->ax;
			y = itr->y + (itr->z2 - itr->z1)/2*itr->ay;
		}
		else {
			x = itr->x - (itr->z2 - itr->z1)/2*itr->ax;
			y = itr->y - (itr->z2 - itr->z1)/2*itr->ay;
		}

		if (angle > 0.2) {
			diff_ang[0] = ((itr->ax - base.ax)*base.ax + (itr->ay - base.ay)*base.ay) / angle;
			diff_ang[1] = ((itr->ax - base.ax)*base.ay - (itr->ay - base.ay)*base.ax) / angle;
			diff_pos[0] = ((x - base.x)*base.ax + (y - base.y)*base.ay) / angle;
			diff_pos[1] = ((x - base.x)*base.ay - (y - base.y)*base.ax) / angle;

			if (fabs(diff_ang[0]) > 0.3 * angle + 0.1 )continue;
			if (fabs(diff_ang[1]) > 0.1)continue;
			if (fabs(diff_pos[0]) > 10 * angle + 30)continue;
			if (fabs(diff_pos[1]) > 30)continue;
			//printf("%5.3lf %5.3lf %5.3lf %5.3lf\n", diff_ang[0], diff_ang[1], diff_pos[0], diff_pos[1]);
			micro_sel.push_back(*itr);
		}
		else {
			diff_ang[0] = (itr->ax - base.ax);
			diff_ang[1] = (itr->ay - base.ay);
			diff_pos[0] = (x - base.x);
			diff_pos[1] = (y - base.y);
			if (fabs(diff_ang[0]) > 0.1)continue;
			if (fabs(diff_ang[1]) > 0.1)continue;
			if (fabs(diff_pos[0]) > 30)continue;
			if (fabs(diff_pos[1]) > 30)continue;
			micro_sel.push_back(*itr);

		}
	}
	printf("micro num=%d\n", micro_sel.size());


	std::vector < netscan::micro_track_t >out_micro;
	micro_format(micro_sel, out_micro);
	for (auto itr = out_micro.begin(); itr != out_micro.end(); itr++) {
		m.push_back(*itr);
	}

	//
	//
	//}
}
void micro_format(std::vector<vxx::micro_track_t>m0, std::vector < netscan::micro_track_t> &m1) {
	for (auto itr = m0.begin(); itr != m0.end(); itr++) {
		//pos zone rawid isg ph ax ay x y z z1 z2 px py col row f
		netscan::micro_track_t tmp;
		tmp.pos = itr->pos;
		tmp.zone = itr->zone;
		tmp.rawid = itr->rawid;
		tmp.isg = itr->isg;
		tmp.ph = itr->ph;
		tmp.ax = itr->ax;
		tmp.ay = itr->ay;
		tmp.x = itr->x;
		tmp.y = itr->y;
		tmp.z = itr->z;
		tmp.z1 = itr->z1;
		tmp.z2 = itr->z2;
		tmp.px = itr->px;
		tmp.py = itr->py;
		tmp.col = itr->col;
		tmp.row = itr->row;
		m1.push_back(tmp);
	}
}
void micro_dc_correction(std::vector<vxx::micro_track_t> &m, std::vector<corrmap0::CorrmapDC>corr, double x, double y, int pos) {
	corrmap0::CorrmapDC param;
	double dist = 0;
	bool flg = true;;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (itr->pos[0] != pos)continue;
		if (flg) {
			param = *itr;
			dist = pow(x - (itr->areax[0] + itr->areax[1]) / 2, 2) + pow(y - (itr->areay[0] + itr->areay[1]) / 2, 2);
			flg = false;
		}
		if (dist > pow(x - (itr->areax[0] + itr->areax[1]) / 2, 2) + pow(y - (itr->areay[0] + itr->areay[1]) / 2, 2)) {
			param = *itr;
			dist = pow(x - (itr->areax[0] + itr->areax[1]) / 2, 2) + pow(y - (itr->areay[0] + itr->areay[1]) / 2, 2);
		}
	}
	//printf("corr dist = %lf\n", sqrt(dist));

	double emulsion_thick = 70;
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		itr->ax = itr->ax*param.shr + param.dax;
		itr->ay = itr->ay*param.shr + param.day;
		if (itr->pos % 10 == 2) {
			itr->x = itr->x - (-1*emulsion_thick / 2 + param.ddz)*param.dax;
			itr->y = itr->y - (-1*emulsion_thick / 2 + param.ddz)*param.day;
		}
		else if (itr->pos % 10 == 1) {
			itr->x = itr->x - (emulsion_thick / 2 + param.ddz)*param.dax;
			itr->y = itr->y - (emulsion_thick / 2 + param.ddz)*param.day;
		}
		else {
			fprintf(stderr, "exception\n");
			exit(1);
		}
		itr->z = itr->z + param.dz;
	}

}
