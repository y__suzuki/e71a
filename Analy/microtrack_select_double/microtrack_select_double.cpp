#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <set>
#include <omp.h>
void Read_fvxx_area(std::string filename, int pos, int zone, std::vector<vxx::micro_track_t> &micro, double x_min, double x_max, double y_min, double y_max);
void microtrack_matching(std::vector<vxx::micro_track_t> &micro1, std::vector<vxx::micro_track_t> &micro2, std::vector<vxx::micro_track_t> &match);
void fvxx_area(std::string filename, int pos, int zone, double &x_min, double &x_max, double &y_min, double &y_max);
void ShotID_trans(std::vector<vxx::micro_track_t>&micro, int NumberOfImager, int scanNUM, int scanID);
void ShotID_trans(vxx::micro_track_t &micro, int NumberOfImager, int scanNUM, int scanID);
std::vector<vxx::micro_track_t>  output2micro(std::vector<vxx::micro_track_t>&match);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in_fvxx1 in_fvxx2 pos zone out_fvxx\n");
		exit(1);
	}
	std::string file_in_fvxx1 = argv[1];
	std::string file_in_fvxx2 = argv[2];
	int pos = std::stoi(argv[3]);
	int zone = std::stoi(argv[4]);
	std::string file_out_fvxx = argv[5];
	std::vector<vxx::micro_track_t> output;

	double x_min, x_max, y_min, y_max;
	//fvxx_area(file_in_fvxx1, pos, zone, x_min, x_max, y_min, y_max);
	x_min = 0;
	y_min = 0;
	x_max = 100000;
	y_max = 100000;
	int divide = 5;
	int step = (x_max - x_min) / divide;
	int overrap = 500;
	double x, y;
	int count = 0;
	for (x = x_min; x <= x_max+overrap;x+=step) {
		for (y = y_min; y <= y_max + overrap; y+=step) {
			printf("count %d \n", count);
			//printf("x: %8.1lf %8.1lf  y: %8.1lf %8.1lf \n", x - overrap, x + step + overrap, y - overrap, y + step + overrap);
			count++;
			if (count != 16)continue;

			std::vector<vxx::micro_track_t> micro1;
			std::vector<vxx::micro_track_t> micro2;

			Read_fvxx_area(file_in_fvxx1, pos, zone, micro1, x - overrap, x + step + overrap, y - overrap, y + step + overrap);
			if (micro1.size() == 0)continue;
			Read_fvxx_area(file_in_fvxx2, pos, zone, micro2, x - overrap * 2, x + step + overrap * 2, y - overrap * 2, y + step + overrap * 2);
			ShotID_trans(micro1, 72, 2, 0);
			ShotID_trans(micro2, 72, 2, 1);
			microtrack_matching(micro1, micro2, output);
		}
	}
	return 0;
	printf("%lld\n", output.size());
	std::vector<vxx::micro_track_t>  micro = output2micro(output);
	vxx::FvxxWriter w;
	w.Write(file_out_fvxx, pos, zone, micro);

}
void fvxx_area(std::string filename, int pos, int zone, double &x_min, double &x_max, double &y_min, double &y_max) {
	int64_t count = 0;
	vxx::FvxxReader fr;
	std::vector<vxx::CutArea> area;
	if (fr.Begin(filename, pos, zone))
	{
		vxx::HashEntry h;
		vxx::micro_track_t m;
		while (fr.NextHashEntry(h))
		{
			while (fr.NextMicroTrack(m))
			{
				if (count % 1000000 == 0) {
					fprintf(stderr, "\r count %lld", count);
				}
				count++;
				if (count == 0) {
					x_min = m.x;
					x_max = m.x;
					y_min = m.y;
					y_max = m.y;
				}
				x_min = std::min(x_min, m.x);
				x_max = std::max(x_max, m.x);
				y_min = std::min(y_min, m.y);
				y_max = std::max(y_max, m.y);
			}
		}
		fr.End();
		fprintf(stderr, "\r count %lld\n", count);
	}
}
void Read_fvxx_area(std::string filename, int pos, int zone, std::vector<vxx::micro_track_t> &micro, double x_min, double x_max, double y_min, double y_max) {
	micro.clear();
	vxx::FvxxReader fr;
	std::vector<vxx::CutArea> area;
	area.push_back(vxx::CutArea(x_min, x_max, y_min, y_max));//xmin, xmax, ymin, ymax
	micro = fr.ReadAll(filename, pos, zone, vxx::opt::a = area);
}
void ShotID_trans(std::vector<vxx::micro_track_t>&micro, int NumberOfImager, int scanNUM, int scanID) {
	if (scanNUM <= scanID) {
		printf("scanNum(%d)<=ScanID(%d)\n", scanNUM, scanID);
		exit(1);
	}
	uint32_t ShotID;
	int ViewID = 0, ImagerID = 0;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		ShotID = ((uint32_t)(uint16_t)itr->row << 16) | ((uint32_t)(uint16_t)itr->col);
		ViewID = ShotID / NumberOfImager;
		ImagerID = ShotID % NumberOfImager;

		ShotID = ViewID * (NumberOfImager*scanNUM) + (NumberOfImager*scanID) + ImagerID;
		itr->col = (int16_t)(ShotID & 0x0000ffff);
		itr->row = (int16_t)((ShotID & 0xffff0000) >> 16);
	}
}
void ShotID_trans(vxx::micro_track_t &micro, int NumberOfImager, int scanNUM, int scanID) {
	if (scanNUM <= scanID) {
		printf("scanNum(%d)<=ScanID(%d)\n", scanNUM, scanID);
		exit(1);
	}
	uint32_t ShotID;
	int ViewID = 0, ImagerID = 0;
	ShotID = ((uint32_t)(uint16_t)micro.row << 16) | ((uint32_t)(uint16_t)micro.col);
	ViewID = ShotID / NumberOfImager;
	ImagerID = ShotID % NumberOfImager;

	ShotID = ViewID * (NumberOfImager*scanNUM) + (NumberOfImager*scanID) + ImagerID;
	micro.col = (int16_t)(ShotID & 0x0000ffff);
	micro.row = (int16_t)((ShotID & 0xffff0000) >> 16);

}

void microtrack_matching(std::vector<vxx::micro_track_t> &micro1, std::vector<vxx::micro_track_t> &micro2, std::vector<vxx::micro_track_t>&match) {
	bool match_output_flg = true;
	double hash_area = 50;
	double hash_angle = 0.1;
	double x_min, y_min, ax_min, ay_min;
	for (auto itr = micro2.begin(); itr != micro2.end(); itr++) {
		//補正
		itr->x = itr->x - 2 * itr->ax;
		itr->y = itr->y - 2 * itr->ay;
		if (itr == micro2.begin()) {
			x_min = itr->x;
			y_min = itr->y;
			ax_min = itr->ax;
			ay_min = itr->ay;
		}
		x_min = std::min(x_min, itr->x);
		y_min = std::min(y_min, itr->y);
		ax_min = std::min(ax_min, itr->ax);
		ay_min = std::min(ay_min, itr->ay);
	}
	std::ofstream ofs("out.txt", std::ios::app);
	std::multimap<std::tuple<int, int, int, int>, vxx::micro_track_t*> micro_map;
	int ix, iy, iax, iay;
	std::tuple <int, int, int, int> id;
	for (auto itr = micro2.begin(); itr != micro2.end(); itr++) {
		ix = (itr->x - x_min) / hash_area;
		iy = (itr->y - y_min) / hash_area;
		iax = (itr->ax - ax_min) / hash_angle;
		iay = (itr->ay - ay_min) / hash_angle;
		std::get<0>(id) = ix;
		std::get<1>(id) = iy;
		std::get<2>(id) = iax;
		std::get<3>(id) = iay;
		micro_map.insert(std::make_pair(id, &(*itr)));
	}
	int count = 0;
	int64_t cnt = 0;
#pragma omp parallel for private(id)
	for (int i = 0; i < micro1.size(); i++) {
#pragma omp atomic
		cnt++;
		if (cnt % 100000 == 0) {
			fprintf(stderr, "\r calc %d/%d(%4.1lf%%)", cnt, micro1.size(), cnt*100. / micro1.size());
		}
		int iix_range[2], iiy_range[2], iiax_range[2], iiay_range[2];
		double angle;
		angle = sqrt(micro1[i].ax*micro1[i].ax + micro1[i].ay*micro1[i].ay);
		double dr_pos = angle * 10 + 2;
		double dr_ang = angle * 0.05 + 0.1;
		double dl_pos = 1.5;
		double dl_ang = 0.07;
		vxx::micro_track_t micro_out = micro1[i];
		bool double_flg = false;

		iix_range[0] = ((micro1[i].x - x_min) - dr_pos) / hash_area;
		iix_range[1] = ((micro1[i].x - x_min) + dr_pos) / hash_area;
		iiy_range[0] = ((micro1[i].y - y_min) - dr_pos) / hash_area;
		iiy_range[1] = ((micro1[i].y - y_min) + dr_pos) / hash_area;
		iiax_range[0] = ((micro1[i].ax - ax_min) - dr_ang) / hash_angle;
		iiax_range[1] = ((micro1[i].ax - ax_min) + dr_ang) / hash_angle;
		iiay_range[0] = ((micro1[i].ay - ay_min) - dr_ang) / hash_angle;
		iiay_range[1] = ((micro1[i].ay - ay_min) + dr_ang) / hash_angle;
		//printf("%d %d %d %d %d %d %d %d\n", iix_range[0], iix_range[1], iiy_range[0], iiy_range[1], iiax_range[0], iiax_range[1], iiay_range[0], iiay_range[1]);
		for (int iix = iix_range[0]; iix <= iix_range[1]; iix++) {
			for (int iiy = iiy_range[0]; iiy <= iiy_range[1]; iiy++) {
				for (int iiax = iiax_range[0]; iiax <= iiax_range[1]; iiax++) {
					for (int iiay = iiay_range[0]; iiay <= iiay_range[1]; iiay++) {
						std::get<0>(id) = iix;
						std::get<1>(id) = iiy;
						std::get<2>(id) = iiax;
						std::get<3>(id) = iiay;
						if (micro_map.count(id) == 0)continue;
						else if (micro_map.count(id) == 1) {
							auto res = micro_map.find(id);
							if (fabs((res->second->ax - micro1[i].ax)*micro1[i].ay - (res->second->ay - micro1[i].ay)*micro1[i].ax) > dl_ang*angle)continue;
							if (fabs((res->second->x - micro1[i].x)*micro1[i].ay - (res->second->y - micro1[i].y)*micro1[i].ax)> dl_pos*angle)continue;
							if (fabs((res->second->ax - micro1[i].ax)*micro1[i].ax + (res->second->ay - micro1[i].ay)*micro1[i].ay)> dr_ang*angle)continue;
							if (fabs((res->second->x - micro1[i].x)*micro1[i].ax + (res->second->y - micro1[i].y)*micro1[i].ay)  > dr_pos*angle)continue;
							if (double_flg == false) {
								double_flg = true;
							}
							if (micro_out.ph < res->second->ph) {
								micro_out = *(res->second);
							}
							//一致したtrackを出力
							if (match_output_flg) {
#pragma omp critical
								{
									ofs << std::right << std::fixed
										<< std::setw(10) << std::setprecision(0) << micro1[i].rawid << " "
										<< std::setw(10) << std::setprecision(0) << res->second->rawid << " "
										<< std::setw(8) << std::setprecision(0) << micro1[i].ph << " "
										<< std::setw(7) << std::setprecision(4) << micro1[i].ax << " "
										<< std::setw(7) << std::setprecision(4) << micro1[i].ay << " "
										<< std::setw(8) << std::setprecision(1) << micro1[i].x << " "
										<< std::setw(8) << std::setprecision(1) << micro1[i].y << " "
										<< std::setw(8) << std::setprecision(0) << res->second->ph << " "
										<< std::setw(7) << std::setprecision(4) << res->second->ax << " "
										<< std::setw(7) << std::setprecision(4) << res->second->ay << " "
										<< std::setw(8) << std::setprecision(1) << res->second->x << " "
										<< std::setw(8) << std::setprecision(1) << res->second->y << std::endl;
								}
							}
						}
						else {
							auto range = micro_map.equal_range(id);
							for (auto res = range.first; res != range.second; res++) {
								if (fabs((res->second->ax - micro1[i].ax)*micro1[i].ay - (res->second->ay - micro1[i].ay)*micro1[i].ax) > dl_ang*angle)continue;
								if (fabs((res->second->x - micro1[i].x)*micro1[i].ay - (res->second->y - micro1[i].y)*micro1[i].ax) > dl_pos*angle)continue;
								if (fabs((res->second->ax - micro1[i].ax)*micro1[i].ax + (res->second->ay - micro1[i].ay)*micro1[i].ay) > dr_ang*angle)continue;
								if (fabs((res->second->x - micro1[i].x)*micro1[i].ax + (res->second->y - micro1[i].y)*micro1[i].ay) > dr_pos*angle)continue;
								if (double_flg == false) {
									double_flg = true;
								}
								if (micro_out.ph < res->second->ph) {
									micro_out = *(res->second);
								}
								//一致したtrackを出力
								if (match_output_flg) {
#pragma omp critical
									{
										ofs << std::right << std::fixed
											<< std::setw(10) << std::setprecision(0) << micro1[i].rawid << " "
											<< std::setw(10) << std::setprecision(0) << res->second->rawid << " "
											<< std::setw(8) << std::setprecision(0) << micro1[i].ph << " "
											<< std::setw(7) << std::setprecision(4) << micro1[i].ax << " "
											<< std::setw(7) << std::setprecision(4) << micro1[i].ay << " "
											<< std::setw(8) << std::setprecision(1) << micro1[i].x << " "
											<< std::setw(8) << std::setprecision(1) << micro1[i].y << " "
											<< std::setw(8) << std::setprecision(0) << res->second->ph << " "
											<< std::setw(7) << std::setprecision(4) << res->second->ax << " "
											<< std::setw(7) << std::setprecision(4) << res->second->ay << " "
											<< std::setw(8) << std::setprecision(1) << res->second->x << " "
											<< std::setw(8) << std::setprecision(1) << res->second->y << std::endl;
									}
								}
							}
						}
					}
				}
			}
		}
		//matchする飛跡があったものに対して
		if (double_flg) {
#pragma omp atomic
			count++;
#pragma omp critical
			match.push_back(micro_out);
		}

	}
	fprintf(stderr, "\r calc %d/%d(%4.1lf%%)\n", cnt, micro1.size(), cnt*100. / micro1.size());
	fprintf(stderr, " matching %lld --> %d(%4.1lf%%)\n", micro1.size(), count, count*100. / micro1.size());
}
std::vector<vxx::micro_track_t>  output2micro(std::vector<vxx::micro_track_t>&match) {
	//idをユニークにしてる
	std::vector<vxx::micro_track_t> ret;
	std::tuple<int, int, int> id;
	int64_t count = 0;
	std::map<std::tuple<int, int, int>, vxx::micro_track_t*>out_map;
	for (auto itr = match.begin(); itr != match.end(); itr++) {
		std::get<0>(id) = itr->col;
		std::get<1>(id) = itr->row;
		std::get<2>(id) = itr->isg;

		out_map.insert(std::make_pair(id, &(*itr)));
	}
	for (auto itr = out_map.begin(); itr != out_map.end(); itr++) {
		ret.push_back(*itr->second);
	}
	
	fprintf(stderr, "selected microtrack num=%d --> %d\n",match.size(),ret.size());
	return ret;
}