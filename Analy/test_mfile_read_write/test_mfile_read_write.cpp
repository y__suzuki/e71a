#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include<vector>


void select_best_chain(std::vector<mfile1::MFileChain> &c, std::vector< std::vector< mfile1::MFileBase>>&base, mfile1::MFileChain &sel_c, std::vector< mfile1::MFileBase>&sel_base);
void mfile_read_write_bin(std::string file_in_mfile, std::string file_out_mfile);
bool unique_base_to_chain(std::map<std::pair<int, int>, mfile1::MFileBase>&base_all, std::vector< mfile1::MFileBase>&c);
bool merge_chains(std::map<std::pair<int, int>, mfile1::MFileBase>&base_all, std::vector< mfile1::MFileBase>&c);
std::map<std::pair<int, int>, mfile1::MFileBase> divide_base(std::vector< std::vector< mfile1::MFileBase>>&base);
bool select_best_chain(std::vector<mfile1::MFileChain> &c, std::vector< std::vector< mfile1::MFileBase>>&base);

bool sort_base(const mfile1::MFileBase& left, mfile1::MFileBase&right) {
	return left.pos < right.pos;
}
void main(int argc, char **argv)
{
	if (argc != 3) {
		fprintf(stderr, "usage : prg_name [input m-file-bin] [output m-file-bin]\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];
	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	mfile1::write_mfile_extension(file_out_mfile, m);


}
