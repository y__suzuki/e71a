#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <set>
#include <list>
#include <filesystem>
#include <omp.h>

std::set<std::tuple<int, int, int>> base_id_pickup(std::string filename, int pl);
std::vector<vxx::base_track_t> select_matched_base(std::set<std::tuple<int, int, int>>&trackid, std::string filename, int pl);
std::map<int, std::vector<vxx::base_track_t>> read_all_tracking_base(std::string file_path, int pl, int area, std::vector<vxx::base_track_t> &base);
vxx::base_track_t select_highVPH_track(vxx::base_track_t &origin, std::vector<vxx::base_track_t>&base);
int use_thread(double ratio, bool output);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg input_bvxx_all pl input_bvxx_path output_bvxx_path\n");
		exit(1);
	}

	std::string file_in_bvxx_all = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_bvxx_path = argv[3];
	std::string file_out_bvxx_path = argv[4];

	std::set<std::tuple<int, int, int>> all_base_id = base_id_pickup(file_in_bvxx_all, pl);

#pragma omp parallel for num_threads(std::min(6,use_thread(0.4,false))) schedule(dynamic,1)
	for (int area = 1; area <= 6; area++) {
		//if (area != 1)continue;
		std::stringstream file_in_bvxx;
		file_in_bvxx << file_in_bvxx_path << "\\Area" << std::setw(1) << area << "\\PL" << std::setw(3) << std::setfill('0') << pl
			<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.vxx";
		if (!std::filesystem::exists(file_in_bvxx.str())) {
			printf("file[%s]not exist\n", file_in_bvxx.str().c_str());
			continue;
		}
		std::vector<vxx::base_track_t> base_area = select_matched_base(all_base_id, file_in_bvxx.str(), pl);

		std::map<int, std::vector<vxx::base_track_t>> base_area_tracking = read_all_tracking_base(file_in_bvxx_path, pl, area, base_area);

		vxx::BvxxWriter bw;
		for (int i = 0; i < 4; i++) {
			std::stringstream filename0, filename1;
			filename0 << file_out_bvxx_path << "\\b" << std::setw(3) << std::setfill('0') << pl << "_area" << std::setw(1) << area << "_thick_" << std::setw(1) << i << ".sel.vxx";
			filename1 << file_out_bvxx_path << "\\b" << std::setw(3) << std::setfill('0') << pl << "_area" << std::setw(1) << area << "_thin_" << std::setw(1) << i << ".sel.vxx";
			bw.Write(filename0.str(), pl, 0, base_area_tracking[i * 2]);
			bw.Write(filename1.str(), pl, 0, base_area_tracking[i * 2 + 1]);
		}
		std::stringstream filename;
		filename << file_out_bvxx_path << "\\b" << std::setw(3) << std::setfill('0') << pl << "_area" << std::setw(1) << area << ".sel.vxx";
		bw.Write(filename.str(), pl, 0, base_area);

	}
}

std::set<std::tuple<int, int, int>> base_id_pickup(std::string filename, int pl) {
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base= br.ReadAll(filename, pl, 0);
	std::set<std::tuple<int, int, int>> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ret.insert(std::make_tuple(itr->m[0].zone, itr->m[0].rawid, itr->m[1].rawid));
	}
	return ret;
}
std::vector<vxx::base_track_t> select_matched_base(std::set<std::tuple<int, int, int>>&trackid, std::string filename, int pl) {
	std::vector<vxx::base_track_t> ret;
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> all = br.ReadAll(filename, pl, 0);
	std::tuple<int, int, int> id;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		std::get<0>(id) = itr->m[0].zone;
		std::get<1>(id) = itr->m[0].rawid;
		std::get<2>(id) = itr->m[1].rawid;
		if (trackid.count(id) == 0)continue;
		ret.push_back(*itr);
	}
	return ret;
}
std::map<int, std::vector<vxx::base_track_t>> read_all_tracking_base(std::string file_path, int pl, int area, std::vector<vxx::base_track_t> &base) {
	std::map<int, std::vector<vxx::base_track_t>> ret;

	std::stringstream file_path2;
	file_path2 << file_path << "\\Area" << std::setw(1) << area
		<< "\\PL" << std::setw(3) << std::setfill('0') << pl;
	std::vector<std::string> filename;
	for (int i = 0; i < 4; i++) {
		std::stringstream filename0, filename1;
		filename0 << file_path2.str() << "\\b" << std::setw(3) << std::setfill('0') << pl << "_thick_" << std::setw(1) << i << ".sel.vxx";
		filename1 << file_path2.str() << "\\b" << std::setw(3) << std::setfill('0') << pl << "_thin_" << std::setw(1) << i << ".sel.vxx";
		filename.push_back(filename0.str());
		filename.push_back(filename1.str());
	}

	for (int i = 0; i < 8; i++) {
		vxx::BvxxReader br;
		std::vector<vxx::base_track_t> base_tracking = br.ReadAll(filename[i], pl, 0);

		//hash
		double xmin, ymin;
		for (auto itr = base_tracking.begin(); itr != base_tracking.end(); itr++) {
			if (itr == base_tracking.begin()) {
				xmin = itr->x;
				ymin = itr->y;
			}
			xmin = std::min(itr->x, xmin);
			ymin = std::min(itr->y, ymin);
		}

		double hash_size = 1000;
		std::multimap<std::pair<int, int>, vxx::base_track_t*> base_hash;
		std::pair<int, int> id;
		for (auto itr = base_tracking.begin(); itr != base_tracking.end(); itr++) {
			id.first = (itr->x - xmin) / hash_size;
			id.second = (itr->y - ymin) / hash_size;
			base_hash.insert(std::make_pair(id, &(*itr)));
		}
		std::vector < vxx::base_track_t>base_tmp;
		std::vector < vxx::base_track_t>matched;

		int ix, iy;
		double angle, all_pos[2], all_ang[2], diff_pos[2], diff_ang[2];
		int count = 0;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			//if (count % 1000 == 0) {
			//	fprintf(stderr, "\r track matching %d/%d(%4.1lf%%))", count, base.size(), count*100. / base.size());
			//}
			count++;

			ix = (itr->x - xmin) / hash_size;
			iy = (itr->y - ymin) / hash_size;
			angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
			//��v��allowance
			all_pos[0] = 5;
			all_pos[1] = 5 * angle + 5;
			all_ang[0] = 0.01;
			all_ang[1] = 0.03*angle + 0.01;

			base_tmp.clear();
			for (int iix = -1; iix <= 1; iix++) {
				for (int iiy = -1; iiy <= 1; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (base_hash.count(id) == 0)continue;
					auto range = base_hash.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						diff_pos[0] = (res->second->x - itr->x)*itr->ay - (res->second->y - itr->y)*itr->ax;
						if (fabs(diff_pos[0]) > all_pos[0] * angle)continue;
						diff_pos[1] = (res->second->x - itr->x)*itr->ax + (res->second->y - itr->y)*itr->ay;
						if (fabs(diff_pos[1]) > all_pos[1] * angle)continue;

						diff_ang[0] = (res->second->ax - itr->ax)*itr->ay - (res->second->ay - itr->ay)*itr->ax;
						if (fabs(diff_ang[0]) > all_ang[0] * angle)continue;
						diff_ang[1] = (res->second->ax - itr->ax)*itr->ax + (res->second->ay - itr->ay)*itr->ay;
						if (fabs(diff_ang[1]) > all_ang[1] * angle)continue;

						base_tmp.push_back(*(res->second));
					}
				}
			}
			if (base_tmp.size() == 0)continue;
			matched.push_back(select_highVPH_track(*itr, base_tmp));
		}
		ret.insert(std::make_pair(i, matched));
	}
	return ret;
}
vxx::base_track_t select_highVPH_track(vxx::base_track_t &origin, std::vector<vxx::base_track_t>&base) {

	vxx::base_track_t sel;
	if (base.size() == 1)sel = *base.begin();
	else {
		int vph;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			if (itr->m[0].zone == origin.m[0].zone&&itr->m[0].rawid == origin.m[0].rawid&&itr->m[1].rawid == origin.m[1].rawid) {
				sel = *itr;
				break;
			}
			if (itr == base.begin()) {
				vph = (itr->m[0].ph + itr->m[1].ph);
				sel = *itr;
			}
			if (vph < (itr->m[0].ph + itr->m[1].ph)) {
				vph = (itr->m[0].ph + itr->m[1].ph);
				sel = *itr;
			}
		}
	}
	return sel;
}
int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
