#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<netscan::base_track_t> angle_cut(std::vector<netscan::base_track_t>base, double angle);

int main(int argc, char **argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-bvxx pl zone out-bvxx\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	int zone = std::stoi(argv[3]);
	std::string file_out_base = argv[4];

	std::vector<netscan::base_track_t>base;
	netscan::read_basetrack_extension(file_in_base, base, pl, zone);

	base = angle_cut(base, 0.3);

	netscan::write_basetrack_vxx(file_out_base, base, pl, zone);

}
std::vector<netscan::base_track_t> angle_cut(std::vector<netscan::base_track_t>base, double angle) {
	std::vector<netscan::base_track_t> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (sqrt(itr->ax*itr->ax + itr->ay*itr->ay) < angle)continue;
		ret.push_back(*itr);
	}
	printf("angle cut <%.1lf :%d-->%d(%4.1lf%%)\n", angle, base.size(), ret.size(), ret.size()*100. / base.size());
	return ret;
}
