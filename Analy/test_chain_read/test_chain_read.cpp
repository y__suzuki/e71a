#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include<vector>

int64_t max_groupid(chain::Chain_file &c);
int64_t max_chainid(chain::Chain_file &c);

int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:prg input-chain\n");
		exit(1);
	}
	std::string file_in_chain = argv[1];
	chain::Chain_file c;
	chain::read_chain(file_in_chain, c);
	printf("read fin\n");

	int64_t max_group = max_groupid(c);
	printf("max group:%lld\n", max_group);

	int64_t max_chain = max_chainid(c);
	printf("max chain:%lld\n", max_chain);

	std::map<std::pair<int, int>, uint64_t> group_base;
	std::map<std::pair<int, int>, uint64_t> base_group;
}
int64_t max_groupid(chain::Chain_file &c) {
	int64_t ret = 0;
	for (auto itr = c.groups.begin(); itr != c.groups.end(); itr++) {
		ret = std::max(ret, itr->group_id);
	}
	return ret;
}
int64_t max_chainid(chain::Chain_file &c) {
	int64_t ret = 0;
	for (int i = 0; i < c.chains.size(); i++) {
		for (int j = 0; j < c.chains[i].size(); j++) {
			ret = std::max(ret,c.chains[i][j].chain_id);
		}
	}
	return ret;
}
