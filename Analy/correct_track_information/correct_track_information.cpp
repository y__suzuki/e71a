#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"PID_correct.lib")
#include <PID_correct.h>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <iostream>
#include <set>
#include <omp.h>
#include <filesystem>

class microtrack_inf {
public:
	int zone, pos, col, row, isg, ph, pixelnum, hitnum;
};

std::vector<Chain_base> read_mfile(std::string filename);
std::vector<std::pair<uint64_t, vxx::base_track_t>> read_base_area0(std::string file_path, int pl, std::vector<Chain_base>&chain_inf);
std::vector<std::pair<uint64_t, vxx::base_track_t>> read_base_area_divide(std::string file_path, int pl, int area, std::vector<std::pair<uint64_t, vxx::base_track_t>> &base);
std::vector<PID_track> read_all_tracking_base(std::string file_path, int pl, int area, std::vector<std::pair<uint64_t, vxx::base_track_t>>  &base);
void base_trackinf_convert(std::pair<uint64_t, vxx::base_track_t>&chain, std::vector<vxx::base_track_t>&base, PID_track t[2]);
short get_sensorid(int col, int row);
short get_trackingid(int zone, int pos);
std::vector<PID_track> same_track_delete(std::vector<PID_track>&track);
std::vector<microtrack_inf> read_microtrack_inf(std::string filename,bool output);
void add_pixelnum_inf(std::string file_path, int pl, int area, std::vector<PID_track>&track);
int use_thread(double ratio, bool output);

bool sort_chainid(const PID_track&left, const PID_track&right) {
	if (left.chainid == right.chainid)return left.trackingid < right.trackingid;
	return left.chainid < right.chainid;
}
int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usagae: mfile-bin output-filename ECC_path\n");
	}
	std::string file_in_mfile = argv[1];
	std::string file_out = argv[2];
	std::string file_path_ECC = argv[3];
	//mfileを構成するchainのID+baseのpl+rawidを読み込む
	std::vector<Chain_base> chain_inf = read_mfile(file_in_mfile);
	std::vector<PID_track> track_all;
	//
	for (int pl = 4; pl <= 133; pl++) {
		std::stringstream filename_bin, filename_txt;
		filename_bin << file_out << "_" << std::setw(3) << std::setfill('0') << pl << ".bin";
		filename_txt << file_out << "_" << std::setw(3) << std::setfill('0') << pl << ".txt";
		if (std::filesystem::exists(filename_bin.str()))continue;
		//if (pl != 4)continue;
		if (pl == 59 ||(109<=pl&&pl<=115))continue;
		track_all.clear();
		printf("PL%03d start\n", pl);
		std::vector<std::pair<uint64_t,vxx::base_track_t>> base;
		base = read_base_area0(file_path_ECC, pl, chain_inf);
		printf("\t Area0 base read fin num=%d\n", base.size());
#pragma omp parallel for num_threads(std::min(6,use_thread(0.4,false))) schedule(dynamic,1)
		for (int area = 1; area <= 6; area++) {
			//if (area != 1)continue;
			std::vector<std::pair<uint64_t, vxx::base_track_t>> base_area;
			base_area = read_base_area_divide(file_path_ECC, pl, area, base);
			printf("\t Area%d base read fin num=%d\n", area, base_area.size());
			std::vector<PID_track>pid_base = read_all_tracking_base(file_path_ECC, pl, area, base_area);
			//add pixel num information
			add_pixelnum_inf(file_path_ECC, pl, area, pid_base);
#pragma omp critical
			{
				for (auto itr = pid_base.begin(); itr != pid_base.end(); itr++) {
					track_all.push_back(*itr);
				}
				printf("\t Area%d base add pixel information fin\n", area);
			}
		}
		if (track_all.size() == 0)continue;
		PID_track::wrtie_pid_track(filename_bin.str(), track_all);
		//PID_track::wrtie_pid_track_txt(filename_txt.str(), track_all);
	}

}
std::vector<Chain_base> read_mfile(std::string filename) {
	std::vector<Chain_base> ret;
	mfile1::MFile mfile;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }
	
	ret.reserve(mfile.info_header.Nbasetrack);

	uint64_t count = 0;
	mfile1::MFileChain1 chain;
	mfile1::MFileBase1 base;
	for (uint64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }

		for (int b = 0; b < chain.nseg; b++) {
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			//ここ
			Chain_base c_b;
			c_b.chainid = chain.chain_id;
			c_b.pl = base.pos / 10;
			c_b.rawid = base.rawid;
			ret.push_back(c_b);
		}
	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

	if (ret.size() != mfile.info_header.Nbasetrack) { throw std::exception("Nbasetrack is wrong."); }
	return ret;

}

std::vector<std::pair<uint64_t, vxx::base_track_t>> read_base_area0(std::string file_path, int pl, std::vector<Chain_base>&chain_inf) {
	std::stringstream filename;
	filename << file_path << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	std::vector<vxx::base_track_t> base;
	vxx::BvxxReader br;
	base = br.ReadAll(filename.str(), pl, 0);

	std::map<int,Chain_base> rawid_list;
	for (auto c : chain_inf) {
		if (c.pl != pl)continue;
		rawid_list.insert(std::make_pair(c.rawid, c));
	}

	std::vector<std::pair<uint64_t, vxx::base_track_t>> ret;
	for (auto itr=base.begin(); itr != base.end(); itr++) {
		if (rawid_list.count(itr->rawid) == 0)continue;
		auto res = rawid_list.find(itr->rawid);
		ret.push_back(std::make_pair(res->second.chainid,*itr));
	}
	return ret;
}
std::vector<std::pair<uint64_t, vxx::base_track_t>> read_base_area_divide(std::string file_path, int pl, int area, std::vector<std::pair<uint64_t, vxx::base_track_t>> &base) {
	std::stringstream filename;
	filename << file_path << "\\Area" << std::setw(1) << area
		<< "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.vxx";
	std::vector<vxx::base_track_t> base_all;
	vxx::BvxxReader br;
	base_all = br.ReadAll(filename.str(), pl, 0);

	std::map<std::tuple<int, int, int, int>,uint64_t> rawid_list;
	for (auto b : base) {
		if ((b.second.m[0].zone - 1) % 6 != area - 1)continue;
		rawid_list.insert(std::make_pair(std::make_tuple(b.second.m[0].zone, b.second.m[0].rawid, b.second.m[1].zone, b.second.m[1].rawid), b.first));
	}
	std::tuple<int, int, int, int> id;
	std::vector<std::pair<uint64_t, vxx::base_track_t>> ret;
	for (auto b : base_all) {
		std::get<0>(id) = b.m[0].zone;
		std::get<1>(id) = b.m[0].rawid;
		std::get<2>(id) = b.m[1].zone;
		std::get<3>(id) = b.m[1].rawid;
		if (rawid_list.count(id) == 0)continue;
		auto res = rawid_list.find(id);
		ret.push_back(std::make_pair(res->second, b));
	}
	return ret;
}
std::vector<PID_track> read_all_tracking_base(std::string file_path, int pl, int area, std::vector<std::pair<uint64_t, vxx::base_track_t>> &base) {
	std::stringstream file_path2;
	file_path2 << file_path << "\\Area" << std::setw(1) << area
		<< "\\PL" << std::setw(3) << std::setfill('0') << pl;
	std::vector<std::string> filename;
	if (pl == 59) {
		for (int i = 0; i < 4; i++) {
			std::stringstream filename0, filename1;
			filename0 << file_path2.str() << "\\b" << std::setw(3) << std::setfill('0') << pl << "_thick_" << std::setw(1) << i << ".vxx";
			filename1 << file_path2.str() << "\\b" << std::setw(3) << std::setfill('0') << pl << "_thin_" << std::setw(1) << i << ".vxx";
			filename.push_back(filename0.str());
			filename.push_back(filename1.str());
		}
	}
	else {
		for (int i = 0; i < 4; i++) {
			std::stringstream filename0, filename1;
			filename0 << file_path2.str() << "\\b" << std::setw(3) << std::setfill('0') << pl << "_thick_" << std::setw(1) << i << ".sel.vxx";
			filename1 << file_path2.str() << "\\b" << std::setw(3) << std::setfill('0') << pl << "_thin_" << std::setw(1) << i << ".sel.vxx";
			filename.push_back(filename0.str());
			filename.push_back(filename1.str());
		}
	}

	std::vector<vxx::base_track_t> base_tracking[8];
	vxx::BvxxReader br;
	base_tracking[0] = br.ReadAll(filename[0], pl, 0);
	base_tracking[1] = br.ReadAll(filename[2], pl, 0);
	base_tracking[2] = br.ReadAll(filename[4], pl, 0);
	base_tracking[3] = br.ReadAll(filename[6], pl, 0);
	base_tracking[4] = br.ReadAll(filename[1], pl, 0);
	base_tracking[5] = br.ReadAll(filename[3], pl, 0);
	base_tracking[6] = br.ReadAll(filename[5], pl, 0);
	base_tracking[7] = br.ReadAll(filename[7], pl, 0);

	//hash
	double xmin=999999, ymin=999999;
	for (int i = 0; i < 8; i++) {
		for (auto itr = base_tracking[i].begin(); itr != base_tracking[i].end(); itr++) {
			if (i == 0 && itr == base_tracking[i].begin()) {
				xmin = itr->x;
				ymin = itr->y;
			}
			xmin = std::min(itr->x, xmin);
			ymin = std::min(itr->y, ymin);
		}
	}

	double hash_size = 1000;
	std::multimap<std::pair<int, int>, vxx::base_track_t*> base_hash[8];
	std::pair<int, int> id;
	for (int i = 0; i < 8; i++) {
		for (auto itr = base_tracking[i].begin(); itr != base_tracking[i].end(); itr++) {
			id.first = (itr->x - xmin) / hash_size;
			id.second = (itr->y - ymin) / hash_size;
			base_hash[i].insert(std::make_pair(id, &(*itr)));
		}
	}
	//同一track 探索
	std::vector<PID_track> ret;
	PID_track t[2];

	std::vector < vxx::base_track_t>base_tmp;
	int ix, iy;
	double angle, all_pos[2], all_ang[2], diff_pos[2], diff_ang[2];
	int count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		//if (count % 1000 == 0) {
		//	fprintf(stderr, "\r track matching %d/%d(%4.1lf%%))", count, base.size(), count*100. / base.size());
		//}
		count++;

		ix = (itr->second.x - xmin) / hash_size;
		iy = (itr->second.y - ymin) / hash_size;
		angle = sqrt(itr->second.ax*itr->second.ax + itr->second.ay*itr->second.ay);
		//一致のallowance
		all_pos[0] = 5;
		all_pos[1] = 5 * angle + 5;
		all_ang[0] = 0.01;
		all_ang[1] = 0.03*angle+0.01;

		for (int i = 0; i < 8; i++) {
			base_tmp.clear();
			for (int iix = -1; iix <= 1; iix++) {
				for (int iiy = -1; iiy <= 1; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (base_hash[i].count(id) == 0)continue;
					auto range = base_hash[i].equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						diff_pos[0] = (res->second->x - itr->second.x)*itr->second.ay - (res->second->y - itr->second.y)*itr->second.ax;
						if (fabs(diff_pos[0]) > all_pos[0] * angle)continue;
						diff_pos[1] = (res->second->x - itr->second.x)*itr->second.ax + (res->second->y - itr->second.y)*itr->second.ay;
						if (fabs(diff_pos[1]) > all_pos[1] * angle)continue;

						diff_ang[0] = (res->second->ax - itr->second.ax)*itr->second.ay - (res->second->ay - itr->second.ay)*itr->second.ax;
						if (fabs(diff_ang[0]) > all_ang[0] * angle)continue;
						diff_ang[1] = (res->second->ax - itr->second.ax)*itr->second.ax + (res->second->ay - itr->second.ay)*itr->second.ay;
						if (fabs(diff_ang[1]) > all_ang[1] * angle)continue;

						base_tmp.push_back(*(res->second));
					}

				}
			}
			if (base_tmp.size() == 0)continue;
			base_trackinf_convert(*itr, base_tmp,t);
			ret.push_back(t[0]);
			ret.push_back(t[1]);
		}
	}
	//fprintf(stderr, "\r track matching %d/%d(%4.1lf%%))\n", count, base.size(), count*100. / base.size());
	ret = same_track_delete(ret);
	return ret;
}
void base_trackinf_convert(std::pair<uint64_t, vxx::base_track_t>&chain, std::vector<vxx::base_track_t>&base, PID_track t[2]) {

	vxx::base_track_t sel;
	if (base.size() == 1)sel = *base.begin();
	else {
		int vph;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			if (itr->m[0].zone == chain.second.m[0].zone&&itr->m[0].rawid == chain.second.m[0].rawid&&itr->m[1].zone == chain.second.m[1].zone&&itr->m[1].rawid == chain.second.m[1].rawid) {
				sel = *itr;
				break;
			}
			if (itr == base.begin()) {
				vph = (itr->m[0].ph + itr->m[1].ph);
				sel = *itr;
			}
			if (vph < (itr->m[0].ph + itr->m[1].ph)) {
				vph = (itr->m[0].ph + itr->m[1].ph);
				sel = *itr;
			}
		}
	}
	/*
	else {
	double diff;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->m[0].zone == chain.second.m[0].zone&&itr->m[0].rawid == chain.second.m[0].rawid&&itr->m[1].zone == chain.second.m[1].zone&&itr->m[1].rawid == chain.second.m[1].rawid) {
			sel = *itr;
			break;
		}
		if (itr == base.begin()) {
			diff = pow(itr->x - chain.second.x, 2) + pow(itr->y - chain.second.y, 2);
			sel = *itr;
		}
		if (diff > pow(itr->x - chain.second.x, 2) + pow(itr->y - chain.second.y, 2)) {
			diff = pow(itr->x - chain.second.x, 2) + pow(itr->y - chain.second.y, 2);
			sel = *itr;
		}
	}
	*/

	for (int i = 0; i < 2; i++) {
		t[i].chainid = chain.first;
		t[i].rawid = sel.m[i].rawid;
		t[i].col = sel.m[i].col;
		t[i].row = sel.m[i].row;
		t[i].isg = sel.m[i].isg;
		t[i].pid = -1;
		t[i].pb = -1;
		t[i].angle = sqrt(sel.ax*sel.ax + sel.ay*sel.ay);
		t[i].pos = sel.m[i].pos;
		t[i].vph = sel.m[i].ph % 10000;
		t[i].sensorid = get_sensorid(sel.m[i].col, sel.m[i].row);
		t[i].trackingid = get_trackingid(sel.m[i].zone, sel.m[i].pos);
		t[i].pixelnum = -1;
	}
}
short get_sensorid(int col, int row) {
	uint32_t ShotID;
	int NumberOfImager = 72;
	ShotID = ((uint32_t)(uint16_t)row << 16) | ((uint32_t)(uint16_t)col);
	int ViewID = ShotID / NumberOfImager;
	int ImagerID = ShotID % NumberOfImager;
	return (short)ImagerID;
}
short get_trackingid(int zone,int pos) {
	//0: thick0
	//1: thick1
	//2: thin0(base side)
	//3: thin1(outer side)
	short ret;
	if (pos % 10 == 1) {
		if ((zone - 1) / 6 == 0) ret = 0;
		else if ((zone - 1) / 6 == 1)ret = 0;
		else if ((zone - 1) / 6 == 2)ret = 1;
		else if ((zone - 1) / 6 == 3)ret = 1;
		else if ((zone - 1) / 6 == 4)ret = 2;
		else if ((zone - 1) / 6 == 5)ret = 2;
		else if ((zone - 1) / 6 == 6)ret = 3;
		else if ((zone - 1) / 6 == 7)ret = 3;
		else {
			fprintf(stderr, "zone exception :%d\n", zone);
			exit(1);
		}
	}
	else if (pos % 10 == 2) {
		if ((zone - 1) / 6 == 0) ret = 0;
		else if ((zone - 1) / 6 == 1)ret = 1;
		else if ((zone - 1) / 6 == 2)ret = 0;
		else if ((zone - 1) / 6 == 3)ret = 1;
		else if ((zone - 1) / 6 == 4)ret = 2;
		else if ((zone - 1) / 6 == 5)ret = 3;
		else if ((zone - 1) / 6 == 6)ret = 2;
		else if ((zone - 1) / 6 == 7)ret = 3;
		else {
			fprintf(stderr, "zone exception :%d\n", zone);
			exit(1);
		}
	}
	else {
		fprintf(stderr, "pos exception :%d\n", pos);
		exit(1);

	}
	return ret;
}
std::vector<PID_track> same_track_delete(std::vector<PID_track>&track) {
	//pos,trackingid,rawidが同一なら同一microtrack
	std::map<std::tuple<short, short, int>, PID_track *> track_unique;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		track_unique.insert(std::make_pair(std::make_tuple(itr->pos, itr->trackingid, itr->rawid), &(*itr)));
	}
	std::vector<PID_track> ret;
	for (auto itr = track_unique.begin(); itr != track_unique.end(); itr++) {
		ret.push_back(*(itr->second));
	}
	sort(ret.begin(),ret.end(), sort_chainid);
	return ret;
}
void add_pixelnum_inf(std::string file_path, int pl, int area, std::vector<PID_track>&track) {


	std::stringstream file_path2;
	file_path2 << file_path << "\\Area" << std::setw(1) << area
		<< "\\PL" << std::setw(3) << std::setfill('0') << pl;
	std::vector<std::string> filename;
	for (int i = 0; i < 4; i++) {
		std::stringstream filename0, filename1;
		filename0 << file_path2.str() << "\\micro_inf_thick_" << std::setw(1) << i;
		filename1 << file_path2.str() << "\\micro_inf_thin_" << std::setw(1) << i;
		filename.push_back(filename0.str());
		filename.push_back(filename1.str());
	}
	//pos,trackingid,col,row,isgでunique
	std::map<std::tuple<short, short, int, int, int>, microtrack_inf> m_map;
	std::vector<microtrack_inf> m_inf;
	short pos,tracking_id;
	for (int i = 0; i < filename.size(); i++) {
		m_inf = read_microtrack_inf(filename[i],false);
		for (auto itr = m_inf.begin(); itr != m_inf.end(); itr++) {
			pos = itr->pos;
			tracking_id = get_trackingid(itr->zone, itr->pos);
			m_map.insert(std::make_pair(std::make_tuple(pos, tracking_id, itr->col, itr->row, itr->isg), *itr));
		}
	}
	std::tuple<short, short, int, int, int> id;
	for (auto itr = track.begin(); itr != track.end(); itr++) {
		std::get<0>(id) = itr->pos;
		std::get<1>(id) = itr->trackingid;
		std::get<2>(id) = itr->col;
		std::get<3>(id) = itr->row;
		std::get<4>(id) = itr->isg;
		if (m_map.count(id) == 0) {
			//printf("not found\n");
			continue;
		}
		auto res = m_map.at(id);
		itr->pixelnum = res.hitnum;
	}
	return;

}
std::vector<microtrack_inf> read_microtrack_inf(std::string filename, bool output) {
	std::vector<microtrack_inf> ret;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (output) {
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
	}
	int64_t count = 0;
	microtrack_inf m;
	while (ifs.read((char*)& m, sizeof(microtrack_inf))) {
		if (output) {
			if (count % 10000 == 0) {
				nowpos = ifs.tellg();
				auto size1 = nowpos - begpos;
				std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
			}
		}
		count++;

		ret.emplace_back(m);
	}
	auto size1 = eofpos - begpos;
	if (output) {
		std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	}
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;
}
int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
