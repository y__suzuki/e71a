#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <omp.h>
struct Efficiency {
	int pl, ph, bin, zfilter, all, hit;
	double eff, eff_err, ang_min, ang_max;
};

void Basetrack_matching(std::vector<netscan::base_track_t> base_pred, std::vector<netscan::base_track_t>  &base_all, double ang_min, double ang_max, std::vector<Efficiency> &eff, std::vector<netscan::linklet_t> &link);
void Basetrack_matching_BG(std::vector<netscan::base_track_t> base_pred, std::vector<netscan::base_track_t>  &base_all, double ang_min, double ang_max, std::vector<Efficiency> &eff);
std::vector<std::vector<netscan::base_track_t>> Base_Area_Cut_Hash(std::vector<netscan::base_track_t> base);
void output_eff(std::string filename, std::vector<Efficiency> eff);
std::vector <netscan::base_track_t> base_alignment(std::vector <netscan::base_track_t>&base0, std::vector <netscan::base_track_t> &base1);
void fit_line(std::vector<double>x, std::vector<double>y, double &slope, double &intercept);
void fit_line_intercept0(std::vector<double>x, std::vector<double>y, double &slope);

int main(int argc, char **argv) {
	if (argc != 10) {
		fprintf(stderr, "usage:prg in-bvxx(prediction) in-bvxx pl out-link out-txt-sig(app) out-txt-bg(app) ph bin zfilter\n");
		exit(1);
	}
	std::string file_in_base_pred = argv[1];
	std::string file_in_base = argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_out_link = argv[4];
	std::string file_out_txt_sig = argv[5];
	std::string file_out_txt_bg = argv[6];
	int ph = std::stoi(argv[7]);
	int bin = std::stoi(argv[8]);
	int zfilter = std::stoi(argv[9]);

	std::vector<netscan::base_track_t> base_pred;
	std::vector<netscan::base_track_t> base;

	netscan::read_basetrack_extension(file_in_base_pred, base_pred, pl, 0);
	netscan::read_basetrack_extension(file_in_base, base, pl, 0);
	std::vector<std::vector<netscan::base_track_t>>base_pred_hash = Base_Area_Cut_Hash(base_pred);

	std::vector<Efficiency> eff;
	std::vector<Efficiency> eff_bg;
	std::vector<netscan::linklet_t> link;
	link.reserve(10000000);

	double ang_min, ang_max;
	int i = 0;
	for (i = 0; i < base_pred_hash.size(); i++) {
		printf("\r %d/%d(%4.1lf%%)", i, base_pred_hash.size(), i*100. / base_pred_hash.size());
		std::vector <netscan::base_track_t> base_ali = base_alignment(base_pred_hash[i], base);
		for (int j = 0; j < 15; j++) {
			ang_min = j * 0.1;
			ang_max = (j + 1) * 0.1;
			Basetrack_matching(base_pred_hash[i], base_ali, ang_min, ang_max, eff, link);
			Basetrack_matching_BG(base_pred_hash[i], base_ali, ang_min, ang_max, eff_bg);
		}
	}
	printf("\r %d/%d(%4.1lf%%)\n", i, base_pred_hash.size(), i*100. / base_pred_hash.size());
	for (auto itr = eff.begin(); itr != eff.end(); itr++) {
		itr->pl = pl;
		itr->bin = bin;
		itr->ph = ph;
		itr->zfilter = zfilter;
	}
	for (auto itr = eff_bg.begin(); itr != eff_bg.end(); itr++) {
		itr->pl = pl;
		itr->bin = bin;
		itr->ph = ph;
		itr->zfilter = zfilter;
	}
	netscan::write_linklet_txt(file_out_link, link);
	output_eff(file_out_txt_sig, eff);
	output_eff(file_out_txt_bg, eff_bg);
}
std::vector<std::vector<netscan::base_track_t>> Base_Area_Cut_Hash(std::vector<netscan::base_track_t> base) {
	std::vector<std::vector<netscan::base_track_t>> ret;
	double area[4];
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr == base.begin()) {
			area[0] = itr->x;
			area[1] = itr->x;
			area[2] = itr->y;
			area[3] = itr->y;
		}
		area[0] = std::min(area[0], itr->x);
		area[1] = std::max(area[1], itr->x);
		area[2] = std::min(area[2], itr->y);
		area[3] = std::max(area[3], itr->y);
	}
	double cut = 5000;
	area[0] += cut;
	area[1] -= cut;
	area[2] += cut;
	area[3] -= cut;

	std::multimap<std::pair<int,int>, netscan::base_track_t>  base_map;
	double hash = 7500;
	int ix, iy;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->x < area[0])continue;
		if (itr->x > area[1])continue;
		if (itr->y < area[2])continue;
		if (itr->y > area[3])continue;
		ix = (itr->x - area[0]) / hash;
		iy = (itr->y - area[2]) / hash;
		base_map.insert(std::make_pair(std::make_pair(ix, iy), *itr));

	}
	std::vector<netscan::base_track_t> base_vec;
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		if (itr->first == std::next(itr, 1)->first) {
			base_vec.push_back(itr->second);
		}
		else {
			base_vec.push_back(itr->second);
			ret.push_back(base_vec);
			base_vec.clear();
		}
	}
	return ret;
}
std::vector <netscan::base_track_t> base_alignment(std::vector <netscan::base_track_t>&base0, std::vector <netscan::base_track_t> &base1) {
	double x_min, x_max, y_min, y_max;
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		if (itr == base0.begin()) {
			x_min = itr->x;
			x_max = itr->x;
			y_min = itr->y;
			y_max = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		x_max = std::max(itr->x, x_max);
		y_min = std::min(itr->y, y_min);
		y_max = std::max(itr->y, y_max);
	}
	double overrap = 2000;
	x_min -= overrap;
	x_max += overrap;
	y_min -= overrap;
	y_max += overrap;
	std::vector<netscan::base_track_t> ret;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (itr->x < x_min)continue;
		if (itr->x > x_max)continue;
		if (itr->y < y_min)continue;
		if (itr->y > y_max)continue;
		ret.push_back(*itr);
	}



	//base0とretのaignmen-->retを変換
	double shift_x, shift_y, gap;
	std::vector<std::pair<netscan::base_track_t, netscan::base_track_t>> match;
	double diff_pos[2], diff_ang[2], angle;
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		for (auto itr2 = ret.begin(); itr2 != ret.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.05 * angle + 0.02)continue;
			if (fabs(diff_ang[1]) > 0.02)continue;
			if (fabs(diff_pos[0]) > 10 * angle + 15)continue;
			if (fabs(diff_pos[1]) > 15)continue;
			match.push_back(std::make_pair(*itr, *itr2));
		}
	}
	std::vector<double>dx, ax, dy, ay;
	std::vector<double>angle_v, dr;
	double tmp[2];

	for (auto itr = match.begin(); itr != match.end(); itr++) {
		ax.push_back(itr->first.ax);
		ay.push_back(itr->first.ay);
		dx.push_back(itr->first.x - itr->second.x);
		dy.push_back(itr->first.y - itr->second.y);
		tmp[0] = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);
		tmp[1] = ((itr->first.x - itr->second.x)*itr->first.ax + (itr->first.y - itr->second.y)*itr->first.ay) / tmp[0];
		angle_v.push_back(tmp[0]);
		dr.push_back(tmp[1]);

	}
	double slope;
	fit_line(ax, dx, slope, shift_x);
	fit_line(ay, dy, slope, shift_y);
	fit_line_intercept0(angle_v, dr, gap);

	//printf("shift x=%lf\n", shift_x);
	//printf("shift y=%lf\n", shift_y);
	//printf("gap=%lf\n", gap);
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		itr->x = itr->x + shift_x + itr->ax*gap;
		itr->y = itr->y + shift_y + itr->ay*gap;
	}


	//以下確認
	match.clear();
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		for (auto itr2 = ret.begin(); itr2 != ret.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.05 * angle + 0.02)continue;
			if (fabs(diff_ang[1]) > 0.02)continue;
			if (fabs(diff_pos[0]) > 10 * angle + 15)continue;
			if (fabs(diff_pos[1]) > 15)continue;
			match.push_back(std::make_pair(*itr, *itr2));
		}
	}
	//	printf("match num=%d\n", match.size());

	dx.clear();
	ax.clear();
	dy.clear();
	ay.clear();
	for (auto itr = match.begin(); itr != match.end(); itr++) {
		ax.push_back(itr->first.ax);
		ay.push_back(itr->first.ay);
		dx.push_back(itr->first.x - itr->second.x);
		dy.push_back(itr->first.y - itr->second.y);
		tmp[0] = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);
		tmp[1] = ((itr->first.x - itr->second.x)*itr->first.ax + (itr->first.y - itr->second.y)*itr->first.ay) / tmp[0];
		angle_v.push_back(tmp[0]);
		dr.push_back(tmp[1]);

	}
	fit_line(ax, dx, slope, shift_x);
	fit_line(ay, dy, slope, shift_y);
	fit_line_intercept0(angle_v, dr, gap);

	//printf("shift x=%lf\n", shift_x);
	//printf("shift y=%lf\n", shift_y);
	//printf("gap=%lf\n", gap);
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		itr->x = itr->x + itr->ax*gap;
		itr->y = itr->y + itr->ay*gap;
	}
	return ret;
}
void fit_line(std::vector<double>x, std::vector<double>y, double &slope, double &intercept) {
	if (x.size() != y.size()) {
		printf("exeption\n");
		exit(1);
	}
	double x1, y1, x2, y2, xy, n;
	x1 = 0;
	y1 = 0;
	x2 = 0;
	y2 = 0;
	xy = 0;
	n = 0;
	for (int i = 0; i < x.size(); i++) {
		x1 += x[i];
		y1 += y[i];
		x2 += x[i] * x[i];
		xy += x[i] * y[i];
		n += 1;
	}
	slope = (n*xy - x1 * y1) / (n*x2 - x1 * x1);
	intercept = (x2*y1 - xy * x1) / (n*x2 - x1 * x1);
}
void fit_line_intercept0(std::vector<double>x, std::vector<double>y, double &slope) {
	if (x.size() != y.size()) {
		printf("exeption\n");
		exit(1);
	}
	double x2, xy;
	x2 = 0;
	xy = 0;
	for (int i = 0; i < x.size(); i++) {
		x2 += x[i] * x[i];
		xy += x[i] * y[i];
	}
	slope = xy / x2;
}

void Basetrack_matching(std::vector<netscan::base_track_t> base_pred,  std::vector<netscan::base_track_t>  &base_all, double ang_min, double ang_max, std::vector<Efficiency> &eff,std::vector<netscan::linklet_t> &link) {
	
	double angle_eff = (ang_min + ang_max) / 2;
	bool angle_flg = false;
	Efficiency* eff_tmp = nullptr;


	for (auto itr = eff.begin(); itr != eff.end(); itr++) {
		if (itr->ang_min < angle_eff&&angle_eff < itr->ang_max) {
			eff_tmp = &(*itr);
			angle_flg = true;
		}
	}
	if (angle_flg == false) {
		eff_tmp = new Efficiency();
		eff_tmp->ang_max = ang_max;
		eff_tmp->ang_min = ang_min;
		eff_tmp->all = 0;
		eff_tmp->hit = 0;
	}

	//int count = 0;

#pragma omp parallel for
	for (int i = 0; i < base_pred.size(); i++) {
		double diff_pos[2], diff_ang[2];
		auto itr = &(base_pred[i]);
		bool hit_flg = false;
		double angle;
		int ix, iy;
		int flg;
		std::pair<int, int> id;
		netscan::base_track_t base_hit;

		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angle < eff_tmp->ang_min)continue;
		if (angle >= eff_tmp->ang_max)continue;
#pragma omp atomic
		eff_tmp->all++;

		for (auto itr2 = base_all.begin(); itr2 != base_all.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.10 * angle + 0.08)continue;
			if (fabs(diff_ang[1]) > 0.08)continue;
			if (fabs(diff_pos[0]) > 20 * angle + 30)continue;
			if (fabs(diff_pos[1]) > 30)continue;
			base_hit = *(itr2);
			hit_flg = true;
		}
		if (hit_flg) {
#pragma omp atomic
			eff_tmp->hit++;
			netscan::linklet_t link_tmp;
			link_tmp.b[0] = *itr;
			link_tmp.b[1] = base_hit;
			link_tmp.pos[0] = link_tmp.b[0].pl * 10 + 1;
			link_tmp.pos[1] = link_tmp.b[1].pl * 10 + 1;
			link_tmp.xc = itr->x;
			link_tmp.yc = itr->y;
			link_tmp.zproj = (link_tmp.b[0].z - link_tmp.b[1].z) / 2;
			link_tmp.dx = link_tmp.b[0].x - link_tmp.b[1].x;
			link_tmp.dy = link_tmp.b[0].y - link_tmp.b[1].y;
#pragma omp critical
			link.push_back(link_tmp);
		}
	}
	//fprintf(stderr, "ang:%4.1lf\n", (ang_min + ang_max) / 2);
	//ここまででmtaching終了
	if (eff_tmp->all == 0) {
		eff_tmp->eff = 0;
		eff_tmp->eff_err = 0;
	}
	else {
		eff_tmp->eff = eff_tmp->hit*1.0 / eff_tmp->all;
		eff_tmp->eff_err = sqrt(eff_tmp->all*eff_tmp->eff*(1 - eff_tmp->eff)) / eff_tmp->all;
	}
	if (angle_flg == false) {
		eff.push_back(*eff_tmp);
	}
}
void Basetrack_matching_BG(std::vector<netscan::base_track_t> base_pred, std::vector<netscan::base_track_t>  &base_all, double ang_min, double ang_max, std::vector<Efficiency> &eff) {
	
	double angle_eff = (ang_min + ang_max) / 2;
	bool angle_flg = false;
	Efficiency* eff_tmp = nullptr;


	for (auto itr = eff.begin(); itr != eff.end(); itr++) {
		if (itr->ang_min < angle_eff&&angle_eff < itr->ang_max) {
			eff_tmp = &(*itr);
			angle_flg = true;
		}
	}
	if (angle_flg == false) {
		eff_tmp = new Efficiency();
		eff_tmp->ang_max = ang_max;
		eff_tmp->ang_min = ang_min;
		eff_tmp->all = 0;
		eff_tmp->hit = 0;
	}

	for (auto itr = base_pred.begin(); itr != base_pred.end(); itr++) {
		//位置をシフト
		itr->x += 1000;
		itr->y += 1000;
	}


	//int count = 0;

#pragma omp parallel for
	for (int i = 0; i < base_pred.size(); i++) {
		double diff_pos[2], diff_ang[2];
		auto itr = &(base_pred[i]);
		bool hit_flg = false;
		double angle;
		int ix, iy;
		int flg;
		std::pair<int, int> id;
		netscan::base_track_t base_hit;

		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (angle < eff_tmp->ang_min)continue;
		if (angle >= eff_tmp->ang_max)continue;
#pragma omp atomic
		eff_tmp->all++;

		for (auto itr2 = base_all.begin(); itr2 != base_all.end(); itr2++) {
			diff_ang[0] = ((itr2->ax - itr->ax)*itr->ax + (itr2->ay - itr->ay)*itr->ay) / angle;
			diff_ang[1] = ((itr2->ax - itr->ax)*itr->ay - (itr2->ay - itr->ay)*itr->ax) / angle;
			diff_pos[0] = ((itr2->x - itr->x)*itr->ax + (itr2->y - itr->y)*itr->ay) / angle;
			diff_pos[1] = ((itr2->x - itr->x)*itr->ay - (itr2->y - itr->y)*itr->ax) / angle;

			if (fabs(diff_ang[0]) > 0.10 * angle + 0.08)continue;
			if (fabs(diff_ang[1]) > 0.08)continue;
			if (fabs(diff_pos[0]) > 20 * angle + 30)continue;
			if (fabs(diff_pos[1]) > 30)continue;
			base_hit = *(itr2);
			hit_flg = true;
		}
		if (hit_flg) {
#pragma omp atomic
			eff_tmp->hit++;
		}
	}
	//fprintf(stderr, "ang:%4.1lf\n", (ang_min + ang_max) / 2);
	//ここまででmtaching終了
	if (eff_tmp->all == 0) {
		eff_tmp->eff = 0;
		eff_tmp->eff_err = 0;
	}
	else {
		eff_tmp->eff = eff_tmp->hit*1.0 / eff_tmp->all;
		eff_tmp->eff_err = sqrt(eff_tmp->all*eff_tmp->eff*(1 - eff_tmp->eff)) / eff_tmp->all;
	}
	if (angle_flg == false) {
		eff.push_back(*eff_tmp);
	}

	

}
void output_eff(std::string filename, std::vector<Efficiency> eff) {
	std::ofstream ofs(filename, std::ios::app);
	for (auto itr = eff.begin(); itr != eff.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << itr->pl << " "
			<< std::setw(3) << std::setprecision(0) << itr->ph << " "
			<< std::setw(3) << std::setprecision(0) << itr->bin << " "
			<< std::setw(3) << std::setprecision(0) << itr->zfilter << " "
			<< std::setw(3) << std::setprecision(1) << itr->ang_min << " "
			<< std::setw(3) << std::setprecision(1) << itr->ang_max << " "
			<< std::setw(10) << std::setprecision(0) << itr->hit << " "
			<< std::setw(10) << std::setprecision(0) << itr->all << " "
			<< std::setw(5) << std::setprecision(4) << itr->eff << " "
			<< std::setw(5) << std::setprecision(4) << itr->eff_err << std::endl;
	}
}