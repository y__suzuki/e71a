#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>
#include <omp.h>

class chain_inf {
public:
	int chainid, groupid;
	int nseg,pl0,pl1;
	float ax, ay, dlat;
};
chain_inf Calc_chain_inf(mfile0::M_Chain &c);
void write_inf(std::string filename, std::vector<chain_inf> &chain);


int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "prg in-mfile out-data\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];

	std::string file_out_txt = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::vector<chain_inf> chain_inf;
	chain_inf.reserve(m.chains.size());

	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		chain_inf.push_back(Calc_chain_inf(*itr));
	}

	write_inf(file_out_txt,chain_inf);
}

chain_inf Calc_chain_inf(mfile0::M_Chain &c) {
	chain_inf ret;
	ret.chainid = c.chain_id;
	ret.groupid = c.basetracks.begin()->group_id;
	ret.nseg = c.nseg;
	ret.pl0 = c.pos0 / 10;
	ret.pl1 = c.pos1 / 10;

	ret.ax = mfile0::chain_ax(c);
	ret.ay = mfile0::chain_ay(c);

	ret.dlat = mfile0::angle_diff_dev_lat(c, ret.ax, ret.ay);

	return ret;
}

void write_inf(std::string filename,std::vector<chain_inf> &chain) {

	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open ���s
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (chain.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = chain.size();
	for (int i = 0; i < chain.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& chain[i], sizeof(chain_inf));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();

}