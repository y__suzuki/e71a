
#define _CRT_SECURE_NO_WARNINGS
//#pragma comment(lib, "VxxReader.lib")
//#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <filesystem>
#include <omp.h>

void add_time(std::vector<Momentum_recon::Event_information> &ev_v, std::vector<Sharing_file::Sharing_file> &sf);

bool sort_unix_time(const Momentum_recon::Event_information&left, const Momentum_recon::Event_information&right) {
	if (left.unix_time == right.unix_time)return left.tracker_track_id < right.tracker_track_id;
	return left.unix_time < right.unix_time;
}
int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-momentum file-in-sharingfile file-out-momentum\n");
		exit(1);
	}
	std::string file_in_mom = argv[1];
	std::string file_in_sf = argv[2];
	std::string file_out_mom = argv[3];

	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_bin(file_in_sf);
	std::vector<Momentum_recon::Event_information> mom = Momentum_recon::Read_Event_information_extension(file_in_mom);

	add_time(mom, sf);
	Momentum_recon::Write_Event_information_extension(file_out_mom, mom);

}
void add_time(std::vector<Momentum_recon::Event_information> &ev_v, std::vector<Sharing_file::Sharing_file> &sf) {
	std::map<int, Sharing_file::Sharing_file> sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(itr->eventid, *itr));
	}

	for (auto itr = ev_v.begin(); itr != ev_v.end(); itr++) {
		if (sf_map.count(itr->groupid) == 0)continue;
		auto sf_sel = sf_map.at(itr->groupid);
		itr->entry_in_daily_file = sf_sel.entry_in_daily_file;
		itr->unix_time = sf_sel.unix_time;
		itr->tracker_track_id = sf_sel.tracker_track_id;
	}

	sort(ev_v.begin(), ev_v.end(), sort_unix_time);
}