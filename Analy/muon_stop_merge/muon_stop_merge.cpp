#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>

#include <set>

mfile0::Mfile merge(mfile0::Mfile &m, mfile0::Mfile &m2);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:in-mfile in-mfile2 out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile0 = argv[1];
	std::string file_in_mfile1 = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m_in, m_in2, m_out;
	mfile0::read_mfile(file_in_mfile0, m_in);
	mfile0::read_mfile(file_in_mfile1, m_in2);

	m_out = merge(m_in, m_in2);
	mfile0::write_mfile(file_out_mfile, m_out);
}
mfile0::Mfile merge(mfile0::Mfile &m, mfile0::Mfile &m2) {

	std::set<int> id{ 232,236,621,1083,1205,1287,1558,1850,1867,1897 ,
	1906,1968,2216,2427,2428,2679,2943,2967,2990,3030,3327,3411,3572,3710,3763,3835 };
	printf("input size=%d\n", id.size());
	int count = 0;
	int gid;
	for (auto itr = m2.chains.begin(); itr != m2.chains.end(); itr++) {
		gid = itr->basetracks.begin()->group_id / 100000;
		if (id.count(gid) == 1) {
			m.chains.push_back(*itr);
			count++;
		}
	}
	printf("input size(done)=%d\n", count);
	return m;
}