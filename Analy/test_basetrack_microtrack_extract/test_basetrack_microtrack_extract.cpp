#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <set>
#include <map>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <ios>
#include <iomanip> 

class microtrack_inf {
public:
	int zone, pos, col, row, isg, ph, pixelnum, hitnum;
};
std::vector<microtrack_inf> read_microtrack_inf(std::string filename);

void match_fvxx(std::string filename, int pos, std::vector < vxx::base_track_t > &base);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-fvxx-inf pos in-bvxx out-fvxx\n");
		exit(1);
	}
	std::string file_in_fvxx = argv[1];
	std::string file_in_bvxx = argv[3];
	int pos = std::stoi(argv[2]);
	int pl = pos / 10;
	std::string file_out_fvxx = argv[4];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> b = br.ReadAll(file_in_bvxx, pl, 0);
	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> m = fr.ReadAll(file_in_fvxx, pos, 0);

	match_fvxx(file_in_fvxx, pos, b);

}

std::vector<microtrack_inf> read_microtrack_inf(std::string filename) {
	std::vector<microtrack_inf> ret;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize�擾
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	microtrack_inf m;
	while (ifs.read((char*)& m, sizeof(microtrack_inf))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(m);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;
}
void match_fvxx(std::string filename, int pos, std::vector < vxx::base_track_t > &base) {

	std::vector<microtrack_inf> m = read_microtrack_inf(filename);
	std::map<std::tuple<int, int, int>, microtrack_inf*> m_map;
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		m_map.insert(std::make_pair(std::make_tuple(itr->col, itr->row, itr->isg), &(*itr)));
	}
	int count = 0;
	int m_id = pos % 10 - 1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::tuple<int, int, int> id = std::make_tuple(itr->m[m_id].col, itr->m[m_id].row, itr->m[m_id].isg);
		auto res = m_map.find(id);
		if (res != m_map.end()) {
			microtrack_inf m_inf = *res->second;
			vxx::micro_track_subset_t b_m = itr->m[m_id];
			if (b_m.ph / 10000 > 14) {
				printf("%8d %5d %5d %5d \n", b_m.ph, m_inf.ph, m_inf.hitnum, m_inf.pixelnum);
			}
			count++;
		}
	}
	printf("base  %d\n", base.size());
	printf("mciro %d\n", count);


}
