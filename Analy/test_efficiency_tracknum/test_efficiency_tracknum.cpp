#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#include <chrono>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

class efficiency {
public:
	double angle, angle_error,eff,eff_error,bg;
	int hit0, hit1, hit2, all;

};

int64_t count_basetrack_num(std::string filename, int pl);
efficiency read_efficiency(std::string filename);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file-in-eff file-in-base pl file-out\n");
		exit(1);
	}

	std::string file_in_eff = argv[1];
	std::string file_in_base = argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_out = argv[4];

	efficiency eff =read_efficiency(file_in_eff);
	int64_t num = count_basetrack_num(file_in_base, pl);

	std::ofstream ofs(file_out, std::ios::app);
	ofs << std::fixed << std::right
		<< std::setw(7) << std::setprecision(5) << eff.eff << " "
		<< std::setw(7) << std::setprecision(5) << eff.eff_error << " "
		<< std::setw(10) << std::setprecision(0) << num << std::endl;
}
efficiency read_efficiency(std::string filename) {

	efficiency eff;
	eff.angle = 2;
	eff.angle_error = 2;
	eff.all = 0;
	eff.bg = 0;
	eff.eff = 0;
	eff.eff_error = 0;
	eff.hit0 = 0;
	eff.hit1 = 0;
	eff.hit2 = 0;
	std::ifstream ifs(filename);
	efficiency eff_read;
	while (ifs >> eff_read.angle >> eff_read.angle_error >> eff_read.eff >> eff_read.eff_error >> eff_read.bg
		>> eff_read.all >> eff_read.hit0 >> eff_read.hit1 >> eff_read.hit2) {
		eff.all += eff_read.all;
		eff.hit0 += eff_read.hit0;
		eff.hit1 += eff_read.hit1;
		eff.hit2 += eff_read.hit2;
	}

	double p0, p1, p2;
	p0 = double(eff.hit0) / eff.all;
	p1 = double(eff.hit1) / eff.all;
	p2 = double(eff.hit2) / eff.all;

	double efficiency = 1. / 2 * ((p2 - p0 + 1) + sqrt(pow(p2 - p0 + 1, 2) - 4 * p2));
	double bg = 1. / 2 * ((p2 - p0 + 1) - sqrt(pow(p2 - p0 + 1, 2) - 4 * p2));

	double eff_error = sqrt(efficiency*eff.all*(1 - efficiency)) / eff.all;

	eff.eff = efficiency;
	eff.eff_error = eff_error;
	eff.bg = bg;

	return eff;
}
int64_t count_basetrack_num(std::string filename, int pl) {
	vxx::BvxxReader br;

	std::vector<vxx::base_track_t> base = br.ReadAll(filename, pl, 0);

	int64_t count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->x < 10000)continue;
		if (itr->y < 10000)continue;
		if (itr->x > 100000)continue;
		if (itr->y > 80000)continue;

		if (sqrt(itr->ax*itr->ax + itr->ay*itr->ay) > 5.0)continue;
		count++;
	}
	return count;



}