#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#include <chrono>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

class efficiency{
public:
	double angle, angle_error;
	int hit0, hit1, hit2,all;

};

int angle_id(double angle);
void id_angle(int id, double &angle, double &angle_error);

std::vector<efficiency> calc_efficiecny(std::vector<netscan::linklet_t> &link, std::vector<vxx::base_track_t> &base);
void output_efficiency(std::string filename, std::vector<efficiency>&eff);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file_in_link file_in_base pl file_out");
		exit(1);
	}

	std::string file_in_link = argv[1];
	std::string file_in_base = argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_out = argv[4];

	std::vector<netscan::linklet_t> link;
	netscan::read_linklet_bin(file_in_link, link);

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base, pl, 0);

	std::vector<efficiency> eff = calc_efficiecny(link, base);
	output_efficiency(file_out, eff);
}
std::vector<efficiency> calc_efficiecny(std::vector<netscan::linklet_t> &link, std::vector<vxx::base_track_t> &base) {



	std::multimap<int, int> linklet_id;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		linklet_id.insert(std::make_pair(itr->b[0].rawid, itr->b[1].rawid));
	}

	std::vector<efficiency> ret;
	for (int i = 0; i < 30; i++) {
		efficiency eff;
		double angle, angle_error;
		id_angle(i, angle, angle_error);
		eff.all = 0;
		eff.angle = angle;
		eff.angle_error = angle_error;
		eff.hit0 = 0;
		eff.hit1 = 0;
		eff.hit2 = 0;
		ret.push_back(eff);
	}

	for (auto itr = base.begin(); itr != base.end(); itr++) {
		double angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);

		int id = angle_id(angle);
		if (id < 0)continue;

		int count = linklet_id.count(itr->rawid);
		ret[id].all += 1;
		if (count == 0)ret[id].hit0 += 1;
		else if (count == 1)ret[id].hit1 += 1;
		else ret[id].hit2 += 1;
	}
	return ret;

}
int angle_id(double angle) {
	if (angle < 0.1)return 0;
	else if (angle < 0.2)return 1;
	else if (angle < 0.3)return 2;
	else if (angle < 0.4)return 3;
	else if (angle < 0.5)return 4;
	else if (angle < 0.6)return 5;
	else if (angle < 0.7)return 6;
	else if (angle < 0.8)return 7;
	else if (angle < 0.9)return 8;
	else if (angle < 1.0)return 9;
	else if (angle < 1.2)return 10;
	else if (angle < 1.4)return 11;
	else if (angle < 1.6)return 12;
	else if (angle < 1.8)return 13;
	else if (angle < 2.0)return 14;
	else if (angle < 2.2)return 15;
	else if (angle < 2.4)return 16;
	else if (angle < 2.6)return 17;
	else if (angle < 2.8)return 18;
	else if (angle < 3.0)return 19;
	else if (angle < 3.2)return 20;
	else if (angle < 3.4)return 21;
	else if (angle < 3.6)return 22;
	else if (angle < 3.8)return 23;
	else if (angle < 4.0)return 24;
	else if (angle < 4.2)return 25;
	else if (angle < 4.4)return 26;
	else if (angle < 4.6)return 27;
	else if (angle < 4.8)return 28;
	else if (angle < 5.0)return 29;

	return -1;
}
void id_angle(int id,double &angle,double &angle_error) {
	if (id == 0) { angle_error = 0.05; angle = 0.05; }
	else if (id == 1) { angle_error = 0.05; angle = 0.15; }
	else if (id == 2) { angle_error = 0.05; angle = 0.25; }
	else if (id == 3) { angle_error = 0.05; angle = 0.35; }
	else if (id == 4) { angle_error = 0.05; angle = 0.45; }
	else if (id == 5) { angle_error = 0.05; angle = 0.55; }
	else if (id == 6) { angle_error = 0.05; angle = 0.65; }
	else if (id == 7) { angle_error = 0.05; angle = 0.75; }
	else if (id == 8) { angle_error = 0.05; angle = 0.85; }
	else if (id == 9) { angle_error = 0.05; angle = 0.95; }
	else if (id == 10) { angle_error = 0.10; angle = 1.10; }
	else if (id == 11) { angle_error = 0.10; angle = 1.30; }
	else if (id == 12) { angle_error = 0.10; angle = 1.50; }
	else if (id == 13) { angle_error = 0.10; angle = 1.70; }
	else if (id == 14) { angle_error = 0.10; angle = 1.90; }
	else if (id == 15) { angle_error = 0.10; angle = 2.10; }
	else if (id == 16) { angle_error = 0.10; angle = 2.30; }
	else if (id == 17) { angle_error = 0.10; angle = 2.50; }
	else if (id == 18) { angle_error = 0.10; angle = 2.70; }
	else if (id == 19) { angle_error = 0.10; angle = 2.90; }
	else if (id == 20) { angle_error = 0.10; angle = 3.10; }
	else if (id == 21) { angle_error = 0.10; angle = 3.30; }
	else if (id == 22) { angle_error = 0.10; angle = 3.50; }
	else if (id == 23) { angle_error = 0.10; angle = 3.70; }
	else if (id == 24) { angle_error = 0.10; angle = 3.90; }
	else if (id == 25) { angle_error = 0.10; angle = 4.10; }
	else if (id == 26) { angle_error = 0.10; angle = 4.30; }
	else if (id == 27) { angle_error = 0.10; angle = 4.50; }
	else if (id == 28) { angle_error = 0.10; angle = 4.70; }
	else if (id == 29) { angle_error = 0.10; angle = 4.90; }

}

void output_efficiency(std::string filename, std::vector<efficiency>&eff) {
	std::ofstream ofs(filename);

	for (auto &e : eff) {
		double p0, p1, p2;
		p0 = double(e.hit0) / e.all;
		p1 = double(e.hit1) / e.all;
		p2 = double(e.hit2) / e.all;

		double eff = 1. / 2 * ((p2 - p0 + 1) + sqrt(pow(p2 - p0 + 1, 2) - 4 * p2));
		double bg = 1. / 2 * ((p2 - p0 + 1) - sqrt(pow(p2 - p0 + 1, 2) - 4 * p2));

		double eff_error = sqrt(eff*e.all*(1 - eff)) / e.all;

		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(2) << e.angle << " "
			<< std::setw(4) << std::setprecision(2) << e.angle_error << " "
			<< std::setw(7) << std::setprecision(5) << eff << " "
			<< std::setw(7) << std::setprecision(5) << eff_error << " "
			<< std::setw(7) << std::setprecision(5) << bg << " "
			<< std::setw(7) << std::setprecision(0) << e.all << " "
			<< std::setw(7) << std::setprecision(0) << e.hit0 << " "
			<< std::setw(7) << std::setprecision(0) << e.hit1 << " "
			<< std::setw(7) << std::setprecision(0) << e.hit2 << std::endl;

	}
}
