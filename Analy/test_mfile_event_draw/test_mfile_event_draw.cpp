#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int main(int argc,char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out = argv[2];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);
	std::ofstream ofs(file_out);
	for (auto &chain : m.chains) {
		int chainid = chain.chain_id;
		int groupid = chain.basetracks.begin()->group_id;
		for (auto &b : chain.basetracks) {
			ofs << std::right << std::fixed
				<< std::setw(10) << std::setprecision(0) << chainid << " "
				<< std::setw(10) << std::setprecision(0) << groupid << " "
				<< std::setw(3) << std::setprecision(0) << b.pos/10 << " "
				<< std::setw(7) << std::setprecision(4) << b.ax << " "
				<< std::setw(7) << std::setprecision(4) << b.ay << " "
				<< std::setw(8) << std::setprecision(1) << b.x << " "
				<< std::setw(8) << std::setprecision(1) << b.y << " "
				<< std::setw(8) << std::setprecision(1) << b.z << std::endl;
		}
	}
}
