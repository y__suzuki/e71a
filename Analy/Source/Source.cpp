#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <map>
#include <ios>     // std::left, std::right
#include <iomanip> 

struct Corrmap {
	int id, pos[2];
	double areax[2], areay[2], position[6], angle[6], dz, signal, background, SN, rms_pos[2], rms_angle[2];
	double notuse_d[5];
	int notuse_i[9];
};

void read_cormap(std::string file, std::vector<struct Corrmap> &cor, int output = 1);
vxx::base_track_t rawid_pickup(std::vector<vxx::base_track_t>&base, int rawid);
std::vector<vxx::base_track_t> base_area_cut(std::vector<vxx::base_track_t> &base, double x, double y, double width);
void trans_base(std::vector<vxx::base_track_t>&base, std::vector<Corrmap> &corr, int pl);
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> track_matching(std::vector<vxx::base_track_t>&base0, std::vector<vxx::base_track_t>&base1, double dz);
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> track_matching2(std::vector<vxx::base_track_t>&base0, std::vector<vxx::base_track_t>&base1, double dz);
void output_track(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> &base_pair, double z0, double z1, std::string filename);


int main(int argc, char** argv) {
	if (argc != 12) {
		fprintf(stderr, "usage:input-bvxx pl zone input-bvxx muon-rawid pl zone corrmap-abs z0 z1 output\n");
		exit(1);

	}
	std::string file_in_bvxx[2];
	int pl[2], zone[2];

	file_in_bvxx[0] = argv[1];
	pl[0] = std::stoi(argv[2]);
	zone[0] = std::stoi(argv[3]);
	file_in_bvxx[1] = argv[4];
	int mu_rawid = std::stoi(argv[5]);
	pl[1] = std::stoi(argv[6]);
	zone[1] = std::stoi(argv[7]);

	std::string file_in_corrmap = argv[8];
	double z[2];
	z[0] = std::stod(argv[9]);
	z[1] = std::stod(argv[10]);

	std::string file_out_txt = argv[11];

	//corrmap読み出し
	std::vector<Corrmap> corr;
	read_cormap(file_in_corrmap, corr);

	//読み込むbasetrackの情報
	std::vector<vxx::base_track_t> base[2];
	for (int i = 0; i < 2; i++) {
		vxx::BvxxReader br;
		//読み出し
		base[i] = br.ReadAll(file_in_bvxx[i], pl[i], zone[i]);
		//mfile座標系に変換
		trans_base(base[i], corr, pl[i]);
	}

	//muonのtrackを読み出す
	vxx::base_track_t muon = rawid_pickup(base[0], mu_rawid);
	std::vector<vxx::base_track_t> base_cut[2];
	//muon trackの5000um周囲を切り出す
	base_cut[0] = base_area_cut(base[0], muon.x, muon.y, 5000);

	//muontrackをgap分外挿
	muon.x = muon.x + muon.ax*(z[1] - z[0]);
	muon.y = muon.y + muon.ay*(z[1] - z[0]);

	//(外挿)muon trackの10000um周囲を切り出す
	base_cut[1] = base_area_cut(base[1], muon.x, muon.y, 10000);

	//飛跡のマッチング
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> base_pair = track_matching(base[0], base[1], z[1] - z[0]);
	//高速化版。結果が一致するかは不明。
	//std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> base_pair = track_matching2(base[0], base[1], z[1] - z[0]);

	//ずれ量の出力
	output_track(base_pair, z[0], z[1], file_out_txt);
}

vxx::base_track_t rawid_pickup(std::vector<vxx::base_track_t>&base, int rawid) {
	bool flg = true;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->rawid == rawid) {
			return *itr;
		}
	}
	fprintf(stderr, "rawid=%d not found\n", rawid);
	exit(1);
}


//corrmap読み込み
void read_cormap(std::string file, std::vector<struct Corrmap> &cor, int output) {
	std::ifstream ifs(file);
	struct Corrmap buffer;

	while (ifs >> buffer.id >> buffer.pos[0] >> buffer.pos[1] >> buffer.areax[0] >> buffer.areax[1] >> buffer.areay[0] >> buffer.areay[1] >>
		buffer.position[0] >> buffer.position[1] >> buffer.position[2] >> buffer.position[3] >> buffer.position[4] >> buffer.position[5] >>
		buffer.angle[0] >> buffer.angle[1] >> buffer.angle[2] >> buffer.angle[3] >> buffer.angle[4] >> buffer.angle[5] >>
		buffer.dz >> buffer.signal >> buffer.background >> buffer.SN >> buffer.rms_pos[0] >> buffer.rms_pos[1] >>
		buffer.notuse_d[0] >> buffer.notuse_d[1] >> buffer.rms_angle[0] >> buffer.rms_angle[1] >>
		buffer.notuse_i[0] >> buffer.notuse_i[1] >> buffer.notuse_i[2] >> buffer.notuse_i[3] >> buffer.notuse_i[4] >> buffer.notuse_i[5] >>
		buffer.notuse_i[6] >> buffer.notuse_i[7] >> buffer.notuse_i[8] >>
		buffer.notuse_d[2] >> buffer.notuse_d[3] >> buffer.notuse_d[4]) {
		struct Corrmap *cormap = new Corrmap();
		cormap = &buffer;
		cor.push_back(*cormap);
	}
	if (output == 1) {
		fprintf(stderr, "%s input finish\n", file.c_str());
	}
	if (cor.size() == 0) {
		fprintf(stderr, "%s alignment miss!\n", file.c_str());
		exit(1);
	}

}
//basetrack座標変換
void trans_base(std::vector<vxx::base_track_t>&base, std::vector<Corrmap> &corr, int pl) {
	Corrmap param;
	bool flg = false;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (itr->pos[0] / 10 == pl) {
			param = *itr;
			flg = false;
			break;
		}
	}
	if (flg) {
		fprintf(stderr, "corrmap PL=%03d not found\n", pl);
	}

	double tmp_x, tmp_y;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		tmp_x = itr->x;
		tmp_y = itr->y;
		itr->x = param.position[0] * tmp_x + param.position[1] * tmp_y + param.position[4];
		itr->y = param.position[2] * tmp_x + param.position[3] * tmp_y + param.position[5];

		tmp_x = itr->ax;
		tmp_y = itr->ay;
		itr->ax = param.angle[0] * tmp_x + param.angle[1] * tmp_y + param.angle[4];
		itr->ay = param.angle[2] * tmp_x + param.angle[3] * tmp_y + param.angle[5];
	}
	return;
}
//basetrack area cut
std::vector<vxx::base_track_t> base_area_cut(std::vector<vxx::base_track_t> &base, double x, double y, double width) {
	std::vector<vxx::base_track_t> ret;

	
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (fabs(itr->x - x) > width)continue;
		if (fabs(itr->y - y) > width)continue;
		ret.push_back(*itr);
	}
	return ret;
}

//飛跡のマッチング
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> track_matching(std::vector<vxx::base_track_t>&base0, std::vector<vxx::base_track_t>&base1, double dz) {
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> ret;
	double dx, dy, dax, day;
	int all = base0.size(), count = 0;
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		if (count % 1000 == 0) {
			fprintf(stderr, "\r now matching ... %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		for (auto itr2 = base1.begin(); itr2 != base1.end(); itr2++) {
			//dz=z1-z0
			//位置の外挿計算は間違ってる可能性あり。要確認。
			dx = itr->x - itr2->x + (itr->ax + itr2->ax)*dz *0.5;
			dy = itr->y - itr2->y + (itr->ay + itr2->ay)*dz *0.5;
			//位置差50um以上はcontinue
			if (fabs(dx) > 50)continue;
			if (fabs(dy) > 50)continue;
			dax = itr->ax - itr2->ax;
			day = itr->ay - itr2->ay;
			//角度差0.1以上はcontinue
			if (fabs(dax) > 0.1)continue;
			if (fabs(day) > 0.1)continue;
			//飛跡のペアをretに保存
			ret.push_back(std::make_pair(*itr, *itr2));
		}
	}
	fprintf(stderr, "\r now matching ... %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

	//返り値でret を返す。
	return ret;
}
//飛跡のマッチング(hash化による高速化)今回はあんまり効果ないかも
std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> track_matching2(std::vector<vxx::base_track_t>&base0, std::vector<vxx::base_track_t>&base1, double dz) {
	double x_min, y_min;
	//2mmでhash
	double hash = 2000;
	int ix, iy;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (itr == base1.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(itr->x, x_min);
		y_min = std::min(itr->y, y_min);
	}
	
	std::multimap<std::pair<int, int>, vxx::base_track_t> base_hash;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		ix = (itr->x - x_min) / hash;
		iy = (itr->y - y_min) / hash;
		base_hash.insert(std::make_pair(std::make_pair(ix, iy), *itr));
	}
	
		
	std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> ret;
	double dx, dy, dax, day;
	std::pair<int, int>id;
	double ex_x, ex_y;
	int all = base0.size(), count = 0;
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		if (count % 1000 == 0) {
			fprintf(stderr, "\r now matching ... %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;

		ex_x = itr->x + itr->ax*dz;
		ex_y = itr->y + itr->ay*dz;
		ix = (ex_x - x_min) / hash;
		iy = (ex_y - y_min) / hash;
		for (int iix = -1; iix <= 1; iix++) {
			for (int iiy = -1; iiy <= 1; iiy++) {
				id.first = ix + iix;
				id.second = iy + iiy;
				if (base_hash.count(id) == 0)continue;
				auto range = base_hash.equal_range(id);
				for (auto res = range.first; res != range.second; res++) {
					auto itr2 = &(res->second);
					//dz=z1-z0
					//位置の外挿計算は間違ってる可能性あり。要確認。
					dx = itr->x - itr2->x + (itr->ax + itr2->ax)*dz *0.5;
					dy = itr->y - itr2->y + (itr->ay + itr2->ay)*dz *0.5;
					//位置差50um以上はcontinue
					if (fabs(dx) > 50)continue;
					if (fabs(dy) > 50)continue;
					dax = itr->ax - itr2->ax;
					day = itr->ay - itr2->ay;
					//角度差0.1以上はcontinue
					if (fabs(dax) > 0.1)continue;
					if (fabs(day) > 0.1)continue;
					//飛跡のペアをretに保存
					ret.push_back(std::make_pair(*itr, *itr2));

				}
			}
		}
	}
	fprintf(stderr, "\r now matching ... %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

	//返り値でret を返す。
	return ret;
}
//マッチした飛跡をtxtで出力
void output_track(std::vector<std::pair<vxx::base_track_t, vxx::base_track_t>> &base_pair, double z0, double z1, std::string filename) {
	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (base_pair.size() == 0) {
		fprintf(stderr, "pair not exist\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		for (auto itr = base_pair.begin(); itr != base_pair.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write base pair ... %d/%d (%4.1lf%%)", count, int(base_pair.size()), count*100. / base_pair.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(12) << std::setprecision(0) << itr->first.rawid << " "
				<< std::setw(8) << std::setprecision(0) << itr->first.m[0].ph + itr->first.m[1].ph << " "
				<< std::setw(8) << std::setprecision(4) << itr->first.ax << " "
				<< std::setw(8) << std::setprecision(4) << itr->first.ay << " "
				<< std::setw(10) << std::setprecision(1) << itr->first.x << " "
				<< std::setw(10) << std::setprecision(1) << itr->first.y << " "
				<< std::setw(10) << std::setprecision(1) << z0 << " "

				<< std::setw(12) << std::setprecision(0) << itr->second.rawid << " "
				<< std::setw(8) << std::setprecision(0) << itr->second.m[0].ph + itr->first.m[1].ph << " "
				<< std::setw(8) << std::setprecision(4) << itr->second.ax << " "
				<< std::setw(8) << std::setprecision(4) << itr->second.ay << " "
				<< std::setw(10) << std::setprecision(1) << itr->second.x << " "
				<< std::setw(10) << std::setprecision(1) << itr->second.y << " "
				<< std::setw(10) << std::setprecision(1) << z1 << std::endl;
		}
		fprintf(stderr, "\r Write base pair ... %d/%d (%4.1lf%%)\n", count, int(base_pair.size()), count*100. / base_pair.size());
	}
}