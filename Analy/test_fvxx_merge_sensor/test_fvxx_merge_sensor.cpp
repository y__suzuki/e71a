#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

void Read_fvxx(std::string filename, int pos, int view, int num, std::vector<vxx::micro_track_t> &m);

int main(int argc, char**argv) {
	if (argc < 4) {
		fprintf(stderr, " usage\n");
		exit(1);
	}
	std::vector<std::string >file_in_fvxx;
	std::string file_out_fvxx;
	int pos,view;

	file_out_fvxx = argv[1];
	pos = std::stoi(argv[2]);
	view = std::stoi(argv[3]);
	for (int i = 4; i < argc; i++) {
		file_in_fvxx.push_back(argv[i]);
	}

	std::vector<vxx::micro_track_t> micro;
	for (int i = 0; i < file_in_fvxx.size(); i++) {
		Read_fvxx(file_in_fvxx[i], pos,view, i, micro);
	}

	printf("micro all size = %d\n", micro.size());

	vxx::FvxxWriter fw;
	fw.Write(file_out_fvxx, pos, 0, micro);

}
void Read_fvxx(std::string filename, int pos,int view,int num, std::vector<vxx::micro_track_t> &m) {
	vxx::FvxxReader fr;

	std::vector<vxx::micro_track_t> micro = fr.ReadAll(filename, pos, 0);
	uint32_t ShotID;
	int ViewID = 0, ImagerID = 0;
	int NumberOfImager = 72;

	std::set<int> view_set;
	for (auto itr = micro.begin(); itr != micro.end(); itr++) {
		ShotID = ((uint32_t)(uint16_t)itr->row << 16) | ((uint32_t)(uint16_t)itr->col);
		ViewID = ShotID / NumberOfImager;
		ImagerID = ShotID % NumberOfImager;
		view_set.insert(ViewID);
		ViewID += view * num;
		ShotID = ViewID * NumberOfImager + ImagerID;
		itr->col = (int16_t)(ShotID & 0x0000ffff);
		itr->row = (int16_t)((ShotID & 0xffff0000) >> 16);
		m.push_back(*itr);
	}
	printf("micro %d size = %d\n", num, micro.size());
}