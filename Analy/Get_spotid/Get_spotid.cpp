#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg file-in-sf pl rawid ECC-path\n");
		exit(1);
	}
	std::string file_in_sf = argv[1];
	int pl = std::stoi(argv[2]);
	int rawid = std::stoi(argv[3]);
	std::string ECC_path = argv[4];

	vxx::BvxxReader br;
	std::stringstream file_in_base;
	file_in_base << ECC_path << "\\Area0\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') <<pl<< ".sel.cor.vxx";
	std::array<int, 2> index = { rawid, rawid+1 };//1234<=rawid<=5678であるようなものだけを読む。

	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), pl, 0, vxx::opt::index = index);
	if (base.size() == 0) {
		fprintf(stderr, "PL%03d rawid=%d not found\n", pl, rawid);
		exit(1);
	}
	vxx::base_track_t b = base[0];
	//fprintf(stderr, "%.4lf %.4lf %d\n", b.ax, b.ay, b.m[0].ph + b.m[1].ph);
	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_extension(file_in_sf);
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		if (itr->zone[0] != b.m[0].zone)continue;
		if (itr->zone[1] != b.m[1].zone)continue;
		if (itr->rawid[0] != b.m[0].rawid)continue;
		if (itr->rawid[1] != b.m[1].rawid)continue;
		//printf("PL%03d rawid=%d : spot id = %d eventid=%d\n", pl, rawid, itr->spotid, itr->eventid);
		printf("spot id = %d eventid=%d\n", itr->spotid, itr->eventid);
	}
	
}