#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::set<std::pair<int64_t, int>> read_result(std::string filename);
std::vector<mfile0::M_Chain> Sel_chain(std::vector<mfile0::M_Chain>&chains, std::set<std::pair<int64_t, int>>&event_list);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_res = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);
	std::set<std::pair<int64_t, int>> proton_list = read_result(file_in_res);
	m.chains = Sel_chain(m.chains, proton_list);

	mfile0::write_mfile(file_out_mfile, m);


}
std::set<std::pair<int64_t, int>> read_result(std::string filename) {
	std::ifstream ifs(filename);
	std::set<std::pair<int64_t, int>> ret;
	std::pair<int64_t, int>id;
	while (ifs >> id.first >> id.second) {
		ret.insert(id);
		id.second = 0;
		ret.insert(id);

	}
	return ret;

}

std::vector<mfile0::M_Chain> Sel_chain(std::vector<mfile0::M_Chain>&chains, std::set<std::pair<int64_t, int>>&event_list) {
	std::vector<mfile0::M_Chain>ret;
	std::pair<int64_t, int>id;
	for (auto &c : chains) {
		id.first = c.basetracks.begin()->group_id;
		id.second = c.chain_id;
		if (event_list.count(id) == 0)continue;
		mfile0::M_Chain c_tmp = c;
		if (id.second != 0) {
			c_tmp.chain_id = (id.first % 100000) / 10;
		}
		else {
			c_tmp.chain_id = 0;
		}

		for (auto &b : c_tmp.basetracks) {
			b.group_id = id.first / 100000;
		}

		ret.push_back(c_tmp);
	}
	return ret;
}
