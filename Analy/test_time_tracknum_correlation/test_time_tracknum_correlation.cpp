#include <vector>
#include <string>
#include <fstream>
#include <iostream >
#include <ios>
#include <iomanip>

#include <map>
class Ttrack_num {
public:
	int pos, area, trackingid;
	int64_t tracknum;
};
std::vector<Ttrack_num> read_track_num(std::string filename);
std::map<std::pair<int, int>, double> track_num_multi_del(std::vector<Ttrack_num>&t_num);
std::map<std::pair<int, int>, double>read_track_time(std::string filename);

void output(std::string filename, std::map<std::pair<int, int>, double> &t_num, std::map<std::pair<int, int>, double>&t_time);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in-time file-in-tracknum file-out\n");
		exit(1);
	}
	std::string file_in_time = argv[1];
	std::string file_in_num = argv[2];
	std::string file_out = argv[3];


	std::vector<Ttrack_num> t_num = read_track_num(file_in_num);
	std::map<std::pair<int, int>, double> t_num_dens = track_num_multi_del(t_num);
	printf("read fin\n");
	std::map<std::pair<int, int>, double>t_time = read_track_time(file_in_time);
	printf("read fin\n");

	//for (auto itr = t_num_dens.begin(); itr != t_num_dens.end(); itr++) {
	//	printf("%d %d %lf\n", itr->first.first, itr->first.second, itr->second);
	//}
	//printf("------------------------------\n");
	//for (auto itr = t_time.begin(); itr != t_time.end(); itr++) {
	//	printf("%d %d %lf\n", itr->first.first, itr->first.second, itr->second);
	//}



	output(file_out, t_num_dens, t_time);

}

	
std::vector<Ttrack_num> read_track_num(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<Ttrack_num> ret;
	Ttrack_num t;
	while (ifs >> t.pos >> t.area >> t.trackingid >> t.tracknum) {
		ret.push_back(t);
	}
	return ret;
}

std::map<std::pair<int, int>, double> track_num_multi_del(std::vector<Ttrack_num>&t_num) {
	//multiの削除
	std::vector<Ttrack_num>sel;
	for (auto &t : t_num) {
		bool flg = true;
		for (auto &s : sel) {
			if (t.area == s.area&&t.pos == s.pos&&t.trackingid == s.trackingid) {
				s.tracknum = t.tracknum;
				flg = false;
			}
		}
		if (flg) {
			sel.push_back(t);
		}
	}
	t_num = sel;
	sel.clear();


	std::map<std::pair<int, int>, int>t_count;
	for (auto itr = t_num.begin(); itr != t_num.end(); itr++) {

		auto res = t_count.insert(std::make_pair(std::make_pair(itr->pos / 10, itr->area), 1));
		if (!res.second) {
			res.first->second++;
		}
	}


	std::map<std::pair<int, int>, int64_t>t_num_sum;

	for (auto itr = t_num.begin(); itr != t_num.end(); itr++) {
		std::pair<int, int> id = std::make_pair(itr->pos / 10, itr->area);
		if (t_count.at(id) != 8)continue;
		auto res = t_num_sum.insert(std::make_pair(id, itr->tracknum));
		if (!res.second) {
			res.first->second += itr->tracknum;
		}
	}

	//printf("t_num size=%d\n", t_num_sum.size());
	//100cm^2あたり
	std::map<std::pair<int, int>, double>t_num_dens;
	for (auto itr = t_num_sum.begin(); itr != t_num_sum.end(); itr++) {
		t_num_dens.insert(std::make_pair(itr->first, itr->second *100. / (9.*13.)));
	}
	//printf("t_num size=%d\n", t_num_dens.size());

	return t_num_dens;
}

std::map<std::pair<int, int>, double>read_track_time(std::string filename) {

	std::map<std::pair<int, int>, double> ret;
	std::ifstream ifs(filename);
	int pl, area, year, month, day, hour, min, sec;
	double time;
	//100cm^2あたり
	while (ifs >>pl>>area>>year>>month>>day>>hour>>min>>sec>>time){
		ret.insert(std::make_pair(std::make_pair(pl, area), time*100. / (9 * 13)));
	}
	return ret;

}

void output(std::string filename,std::map<std::pair<int, int>, double> &t_num, std::map<std::pair<int, int>, double>&t_time) {

	std::ofstream ofs(filename);

	for (auto itr = t_num.begin(); itr != t_num.end(); itr++) {
		auto res = t_time.find(itr->first);
		if (res == t_time.end())continue;

		double num, time;
		num = itr->second;
		time = res->second;
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->first.first << " "
			<< std::setw(4) << std::setprecision(0) << itr->first.second << " "
			<< std::setw(10) << std::setprecision(1) << num << " "
			<< std::setw(10) << std::setprecision(1) << time << std::endl;
	}

}