#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"

#include <fstream>
#include <algorithm>
#include <map>
#include <ios>     // std::left, std::right
#include <iomanip> 
#include <omp.h>
#include <chrono>
class Cut_param {
public:
	double ang_min, ang_max;
	std::pair<double, double> p[3];
	double eff;
	int count;
};
struct BaseInformation {
	double angle, x, y;
	std::pair<bool, vxx::base_track_t> *ptr;
};

bool sort_cut_param_angle(const Cut_param &left, const Cut_param & right) {
	return left.ang_min < right.ang_min;
}

std::vector<Cut_param>  read_cut_param(std::string filename);
std::pair<double, double> get_angle_acc(std::string filename, int pl, int area);
std::vector<std::pair<bool, vxx::base_track_t>> read_bvxx_id(std::string filename, int pl, int &id);
std::vector<BaseInformation> clac_chi2(std::vector<std::pair<bool, vxx::base_track_t>> &base, std::pair<double, double>&param);

bool judege_hit(BaseInformation &t, double x0, double y0, double x1, double y1, double x2, double y2);

std::vector<vxx::base_track_t> angle_cut(std::vector<vxx::base_track_t>&base, double threshold);


int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:prg in-bvxx pl area out-bvxx angle_acc_file cut-param-file\n");
		fprintf(stderr, "cut-param-file:ang_min ang_max x1 y1 x2 y2\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	int area = std::stoi(argv[3]);
	std::string file_out_base = argv[4];
	std::string file_in_acc = argv[5];
	std::string file_in_cut = argv[6];

	std::vector<Cut_param> cut_param = read_cut_param(file_in_cut);
	sort(cut_param.begin(), cut_param.end(), sort_cut_param_angle);

	std::pair<double, double> acc_param = get_angle_acc(file_in_acc, pl, area);

	std::vector<vxx::base_track_t> base_out;
	int id = 0;
	int64_t count = 0;
	while (true) {
		std::vector<std::pair<bool, vxx::base_track_t>>base = read_bvxx_id(file_in_base, pl, id);
		if (base.size() == 0)break;
		

		std::vector<BaseInformation>ranking = clac_chi2(base, acc_param);
		for (int i = 0; i < ranking.size();i++){
			for (auto itr2 = cut_param.begin(); itr2 != cut_param.end(); itr2++) {
				if (std::next(itr2,1)!=cut_param.end()&&ranking[i].angle > itr2->ang_max)continue;
				ranking[i].ptr->first = judege_hit(ranking[i], itr2->p[0].first, itr2->p[0].second, itr2->p[1].first, itr2->p[1].second, itr2->p[2].first, itr2->p[2].second);
				break;
			}
		}
		for (auto itr = ranking.begin(); itr != ranking.end(); itr++) {
			if (itr->ptr->first) {
				base_out.push_back(itr->ptr->second);
			}
		}
	}

	//角度が大きすぎるものはcut
	base_out = angle_cut(base_out, 10 * sqrt(2));
	vxx::BvxxWriter bw;
	bw.Write(file_out_base, pl, 0, base_out);
}
std::vector<Cut_param>  read_cut_param(std::string filename) {
	std::vector<Cut_param> ret;
	Cut_param param;
	std::ifstream ifs(filename);
	while (ifs >> param.ang_min>>param.ang_max>> param.p[0].first>> param.p[0].second>> param.p[1].first >> param.p[1].second >> param.p[2].first >> param.p[2].second >>param.eff>>param.eff ){
		ret.push_back(param);
	}
	return ret;
}
std::pair<double, double> get_angle_acc(std::string filename, int pl, int area) {
	std::ifstream ifs(filename.c_str());
	int pl_tmp, area_tmp;
	double dx, dz;
	std::pair<double, double>param;
	bool flg = false;
	while (ifs >> pl_tmp >> area_tmp >> dx >> dz) {
		if (pl_tmp == pl && area_tmp == area) {
			flg = true;
			param.first = dx;
			param.second = dz;
			break;
		}
	}
	if (!flg) {
		fprintf(stderr, "angle acc parameter not found\n");
		exit(1);
	}
	return param;

}
std::vector<std::pair<bool, vxx::base_track_t>> read_bvxx_id(std::string filename, int pl,  int &id) {
	//bvxx 176 byte/track
	//50Mtrack=8.8GB
	int read_track_num = 50 * 1000 * 1000;

	std::vector<vxx::base_track_t> b;
	vxx::BvxxReader br;
	std::array<int, 2> index = { id*read_track_num, (id + 1)*read_track_num };//1234<=rawid<=5678であるようなものだけを読む。
	b = br.ReadAll(filename, pl,0, vxx::opt::index = index);

	std::vector<std::pair<bool, vxx::base_track_t>> ret;
	ret.reserve(read_track_num);
	for (auto itr = b.begin(); itr != b.end(); itr++) {
		ret.push_back(std::make_pair(true, *itr));
	}
	id++;
	return ret;

}

std::vector<BaseInformation> clac_chi2(std::vector<std::pair<bool, vxx::base_track_t>> &base, std::pair<double, double>&param) {

	std::vector<BaseInformation> ret;
	BaseInformation chi2_tmp;
	double angle, dlat0, dlat1, drad0, drad1, sigma_lat, sigma_rad, chi;
	int vph;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		angle = sqrt(itr->second.ax*itr->second.ax + itr->second.ay*itr->second.ay);
		dlat0 = ((itr->second.m[0].ax - itr->second.ax)*itr->second.ay - (itr->second.m[0].ay - itr->second.ay)*itr->second.ax) / angle;
		dlat1 = ((itr->second.m[1].ax - itr->second.ax)*itr->second.ay - (itr->second.m[1].ay - itr->second.ay)*itr->second.ax) / angle;

		drad0 = ((itr->second.m[0].ax - itr->second.ax)*itr->second.ax + (itr->second.m[0].ay - itr->second.ay)*itr->second.ay) / angle;
		drad1 = ((itr->second.m[1].ax - itr->second.ax)*itr->second.ax + (itr->second.m[1].ay - itr->second.ay)*itr->second.ay) / angle;

		sigma_lat = sqrt(2) / 60 * param.first;
		sigma_rad = sqrt(2) / 60 * sqrt(param.first*param.first + angle * angle*param.second*param.second);

		chi2_tmp.x= pow(dlat0 / sigma_lat, 2) + pow(dlat1 / sigma_lat, 2) + pow(drad0 / sigma_rad, 2) + pow(drad1 / sigma_rad, 2);
		chi2_tmp.y= (itr->second.m[0].ph + itr->second.m[1].ph) % 10000;

		chi2_tmp.angle = angle;
		chi2_tmp.ptr = &(*itr);

		ret.push_back(chi2_tmp);

	}
	return ret;
}

bool judege_hit(BaseInformation &t, double x0, double y0, double x1, double y1, double x2, double y2) {
	if (t.x <= x0)return t.y >= y0;
	else if (t.x < x1) return t.y >= (y1 - y0) / (x1 - x0)*(t.x - x0) + y0;
	else if (t.x < x2) return t.y >= (y2 - y1) / (x2 - x1)*(t.x - x1) + y1;
	else return t.y >= y2;
}

std::vector<vxx::base_track_t> angle_cut(std::vector<vxx::base_track_t>&base, double threshold) {
	std::vector<vxx::base_track_t> ret;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		angle = itr->ax*itr->ax + itr->ay*itr->ay;
		if (angle > threshold*threshold)continue;
		ret.push_back(*itr);
	}
	return ret;
}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
