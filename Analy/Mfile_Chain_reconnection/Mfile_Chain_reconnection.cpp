#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>

class Mfile_Area {
public:
	std::map<int, double> x_min, y_min, x_max, y_max, z;
};
class Chain_inf {
public:
	uint64_t chainid;
	int pl0, pl1, nseg;
	double ax_up, ay_up, ax_down, ay_down, x_up, y_up, z_up, x_down, y_down, z_down;
	int flg;
};
uint64_t mfile_size(const mfile1::MFile_minimum &m_all);
Mfile_Area Mfile_area(mfile1::MFile_minimum &m);
std::vector<uint64_t> chain_id_list(const mfile1::MFile_minimum &m_all);
std::vector<uint64_t> divide_downstream_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, int PL_thr);
std::vector<uint64_t> divide_upstream_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, int PL_thr);

std::vector<uint64_t> divide_up_edgeout_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, const Mfile_Area &area, double edge_thr);
bool judge_up_edgeout(const std::vector<mfile1::MFileBase> &base, const Mfile_Area &area, double edge_cut);
std::vector<uint64_t> divide_down_edgeout_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, const Mfile_Area &area, double edge_thr);
bool judge_down_edgeout(const std::vector<mfile1::MFileBase> &base, const Mfile_Area &area, double edge_cut);

std::vector<uint64_t> short_thin_track_selection(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all);
bool VPH_cut(const std::vector<mfile1::MFileBase> &base);

void mfile_wrtie(std::string filename, mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &veto0, const std::vector<uint64_t> &veto1, const std::vector<uint64_t> &veto2);

Chain_inf Calc_Chain_inf(const mfile1::MFile_minimum &m_all, uint64_t num, std::set<uint64_t> &pene_up, std::set<uint64_t> &pene_down, std::set<uint64_t> &edge_up, std::set<uint64_t> &edge_down, std::set<uint64_t> &short_thin);
void Write_Chain_inf(std::string filename, std::vector<Chain_inf> &chain);


int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:prg in-mfile\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];

	mfile1::MFile_minimum m;
	mfile1::read_mfile(file_in_mfile, m);

	printf("read mfile fin\n");
	printf("size:%.3lf[GB]\n", mfile_size(m) / 1000. / 1000. / 1000);

	Mfile_Area area = Mfile_area(m);
	//for (auto itr = area.x_max.begin(); itr != area.x_max.end();itr++) {
	//	int pl = itr->first;
	//	auto res0 = area.x_min.find(pl);
	//	auto res1 = area.x_max.find(pl);
	//	auto res2 = area.y_min.find(pl);
	//	auto res3 = area.y_max.find(pl);
	//	printf("%03d %10.1lf %10.1lf %10.1lf %10.1lf\n", pl, res0->second, res1->second, res2->second, res3->second);
	//}

	std::vector<uint64_t>chain_all, chain_up_pene, chain_down_pene, chain_up_edgeout, chain_down_edgeout,short_thin;
	chain_all = chain_id_list(m);

	int max_pl = area.z.rbegin()->first;
	printf("max PL = %d:UP penetrate PL%03d -- PL%03d\n", max_pl, max_pl - 3,  max_pl );
	//max PL=133の場合 130,131,132,133を選択
	chain_up_pene = divide_upstream_chain(m, chain_all, max_pl - 3);

	int min_pl = area.z.begin()->first;
	printf("min PL = %d:DOWN penetrate PL%03d -- PL%03d\n", min_pl, min_pl , min_pl+3);
	//max PL=4の場合 4,5,6,7を選択
	chain_down_pene = divide_downstream_chain(m, chain_all, min_pl + 3);

	chain_up_edgeout = divide_up_edgeout_chain(m, chain_all, area, 5000);
	chain_down_edgeout = divide_down_edgeout_chain(m, chain_all, area, 5000);

	short_thin = short_thin_track_selection(m, chain_all);

	printf("all chain      :%llu\n", chain_all.size());
	printf("penetrate up   :%llu\n", chain_up_pene.size());
	printf("penetrate down :%llu\n", chain_down_pene.size());
	printf("edge out up    :%llu\n", chain_up_edgeout.size());
	printf("edge out down  :%llu\n", chain_down_edgeout.size());
	printf("short thin     :%llu\n", short_thin.size());

	//mfile_wrtie("m_up_stop.bmf", m, chain_up_pene, chain_up_edgeout, short_thin);
	//mfile_wrtie("m_down_stop.bmf", m, chain_down_pene, chain_down_edgeout, short_thin);
	std::vector<Chain_inf> chain;
	chain.reserve(m.chains.size());
	std::set<uint64_t>chain_up_pene_set(chain_up_pene.begin(), chain_up_pene.end());
	std::set<uint64_t>chain_down_pene_set(chain_down_pene.begin(), chain_down_pene.end());
	std::set<uint64_t>chain_up_edge_set(chain_up_edgeout.begin(), chain_up_edgeout.end());
	std::set<uint64_t>chain_down_edge_set(chain_down_edgeout.begin(), chain_down_edgeout.end());
	std::set<uint64_t>short_thin_set(short_thin.begin(), short_thin.end());

	
	for (uint64_t i = 0; i < m.chains.size(); i++) {
		chain.push_back(Calc_Chain_inf(m, i, chain_up_pene_set, chain_down_pene_set, chain_up_edge_set, chain_down_edge_set, short_thin_set));
	}
	Write_Chain_inf("chain_inf.dat", chain);
}
uint64_t mfile_size(const mfile1::MFile_minimum &m_all) {
	uint64_t size = 0;
	size += sizeof(mfile1::MFileHeader);
	size += sizeof(mfile1::MFileInfoHeader);
	uint64_t chain_size = sizeof(mfile1::MFileChain);
	uint64_t base_size = sizeof(mfile1::MFileBase);
	size += chain_size * m_all.info_header.Nchain;
	size += base_size * m_all.info_header.Nbasetrack;
	return size;
}
Mfile_Area Mfile_area(mfile1::MFile_minimum &m) {
	Mfile_Area area;
	int pl;
	for (int i = 0; i < m.all_basetracks.size(); i++) {
		for (int j = 0; j < m.all_basetracks[i].size(); j++) {
			pl = m.all_basetracks[i][j].pos / 10;
			if (area.x_max.count(pl) == 0) {
				area.x_min.insert(std::make_pair(pl, m.all_basetracks[i][j].x));
				area.x_max.insert(std::make_pair(pl, m.all_basetracks[i][j].x));
				area.y_min.insert(std::make_pair(pl, m.all_basetracks[i][j].y));
				area.y_max.insert(std::make_pair(pl, m.all_basetracks[i][j].y));
				area.z.insert(std::make_pair(pl, m.all_basetracks[i][j].z));
			}
			auto res0 = area.x_min.find(pl);
			auto res1 = area.x_max.find(pl);
			auto res2 = area.y_min.find(pl);
			auto res3 = area.y_max.find(pl);
			res0->second = std::min(res0->second, m.all_basetracks[i][j].x);
			res1->second = std::max(res1->second, m.all_basetracks[i][j].x);
			res2->second = std::min(res2->second, m.all_basetracks[i][j].y);
			res3->second = std::max(res3->second, m.all_basetracks[i][j].y);
		}
	}
	return area;
}
std::vector<uint64_t> chain_id_list(const mfile1::MFile_minimum &m_all) {
	std::vector<uint64_t> ret;
	ret.reserve(m_all.chains.size());
	for (int i = 0; i < m_all.chains.size(); i++) {
		ret.push_back(i);
	}
	return ret;
}
std::vector<uint64_t> divide_upstream_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, int PL_thr) {
	std::vector<uint64_t> ret;
	for (int i = 0; i < all.size(); i++) {
		//PL_thr以上の飛跡をup stream VETO
		if (m_all.chains[all[i]].pos1 / 10 >= PL_thr) {
			ret.push_back(all[i]);
		}
	}
	//printf("all chain %lld\n", all.size());
	//printf("\tup  stream track %lld\n", chain_up.size());
	//printf("\tdownstream track %lld\n", chain_down.size());
	return ret;
}
std::vector<uint64_t> divide_downstream_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, int PL_thr) {
	std::vector<uint64_t> ret;
	for (int i = 0; i < all.size(); i++) {
		//PL_thr以上の飛跡をup stream VETO
		if (m_all.chains[all[i]].pos0 / 10 <= PL_thr) {
			ret.push_back(all[i]);
		}
	}
	//printf("all chain %lld\n", all.size());
	//printf("\tup  stream track %lld\n", chain_up.size());
	//printf("\tdownstream track %lld\n", chain_down.size());
	return ret;
}


std::vector<uint64_t> divide_up_edgeout_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, const Mfile_Area &area, double edge_thr) {
	std::vector<uint64_t> ret;
	//3PL外挿して1つでもout of range ならedege out
	for (int i = 0; i < all.size(); i++) {
		if (judge_up_edgeout(m_all.all_basetracks[all[i]], area, edge_thr)) {
			ret.push_back(all[i]);
		}
	}
	return ret;
}
bool judge_up_edgeout(const std::vector<mfile1::MFileBase> &base, const Mfile_Area &area, double edge_cut) {
	int pl = base.rbegin()->pos / 10;
	double z = area.z.at(pl);
	bool flg = false;
	double x_up, y_up;
	//0-3PL分外挿
	for (int i_pl = 0; i_pl <= 3; i_pl++) {
		auto res = area.z.find(pl + i_pl);
		if (res == area.z.end()) {
			//fprintf(stderr, "pl out of range %d\n", pl + i_pl);
			continue;
		}
		x_up = base.rbegin()->x + base.rbegin()->ax*(res->second - z);
		y_up = base.rbegin()->y + base.rbegin()->ay*(res->second - z);
		if (area.x_min.at(pl + i_pl) + edge_cut > x_up)flg = true;
		if (area.x_max.at(pl + i_pl) - edge_cut < x_up)flg = true;
		if (area.y_min.at(pl + i_pl) + edge_cut > y_up)flg = true;
		if (area.y_max.at(pl + i_pl) - edge_cut < y_up)flg = true;
	}
	return flg;
}

std::vector<uint64_t> divide_down_edgeout_chain(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all, const Mfile_Area &area, double edge_thr) {
	std::vector<uint64_t> ret;
	//3PL外挿して1つでもout of range ならedege out
	for (int i = 0; i < all.size(); i++) {
		if (judge_down_edgeout(m_all.all_basetracks[all[i]], area, edge_thr)) {
			ret.push_back(all[i]);
		}
	}
	return ret;
}
bool judge_down_edgeout(const std::vector<mfile1::MFileBase> &base, const Mfile_Area &area, double edge_cut) {
	int pl = base.begin()->pos / 10;
	double z = area.z.at(pl);
	bool flg = false;
	double x_up, y_up;
	//0-3PL分外挿
	for (int i_pl = 0; i_pl <= 3; i_pl++) {
		auto res = area.z.find(pl - i_pl);
		if (res == area.z.end()) {
			//fprintf(stderr, "pl out of range %d\n", pl + i_pl);
			continue;
		}
		x_up = base.begin()->x + base.begin()->ax*(res->second - z);
		y_up = base.begin()->y + base.begin()->ay*(res->second - z);
		if (area.x_min.at(pl - i_pl) + edge_cut > x_up)flg = true;
		if (area.x_max.at(pl - i_pl) - edge_cut < x_up)flg = true;
		if (area.y_min.at(pl - i_pl) + edge_cut > y_up)flg = true;
		if (area.y_max.at(pl - i_pl) - edge_cut < y_up)flg = true;
	}
	return flg;
}

std::vector<uint64_t> short_thin_track_selection(const mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &all) {

	std::vector<uint64_t> ret;

	bool flg = false;
	int nPL = 0;
	double eff;
	for (auto &i : all) {
		flg = false;
		nPL = (m_all.chains[i].pos1 - m_all.chains[i].pos0) / 10 + 1;
		eff = m_all.chains[i].nseg*1.0 / nPL;
		if (eff < 0.500000000000000001) {
			ret.push_back(i);
			continue;
		}
		if (m_all.chains[i].nseg <= 2) {
			flg = false;
		}
		else if (m_all.chains[i].nseg <= 5) {
			flg = VPH_cut(m_all.all_basetracks[i]);
		}
		else {
			flg = true;
		}
		if (!flg) {
			ret.push_back(i);
		}
	}
	return ret;

}
bool VPH_cut(const std::vector<mfile1::MFileBase> &base) {
	double ax = 0, ay = 0, VPH = 0;
	int count = 0;
	for (const auto& b : base) {
		ax += b.ax;
		ay += b.ay;
		VPH += b.ph % 10000;
		count++;
	}
	ax = ax / count;
	ay = ay / count;
	VPH = VPH / count;
	double angle = sqrt(ax*ax + ay * ay);

	if (angle < 1.0) {
		return VPH > 190 - 70 * angle;
	}
	else if (1.0 <= angle && angle < 2.0) {
		return VPH > 150 - 30 * angle;
	}
	else if (2.0 <= angle && angle < 3.0) {
		return VPH > 110 - 10 * angle;
	}
	else if (3.0 <= angle) {
		return VPH > 95 - 5 * angle;
	}
	return false;
}

void mfile_wrtie(std::string filename, mfile1::MFile_minimum &m_all, const std::vector<uint64_t> &veto0, const std::vector<uint64_t> &veto1, const std::vector<uint64_t> &veto2) {

	std::set<uint64_t> veto0_set(veto0.begin(), veto0.end());
	std::set<uint64_t> veto1_set(veto1.begin(), veto1.end());
	std::set<uint64_t> veto2_set(veto2.begin(), veto2.end());
	std::ofstream ofs(filename, std::ios::binary);


	size_t Nchain = 0;
	size_t Nbasetrack = 0;
	for (uint64_t i = 0; i < m_all.chains.size();i++) {
		if (veto0_set.count(i) == 1)continue;
		if (veto1_set.count(i) == 1)continue;
		if (veto2_set.count(i) == 1)continue;
		Nbasetrack += m_all.all_basetracks[i].size();
		Nchain++;
	}
	m_all.info_header.Nchain = Nchain;
	m_all.info_header.Nbasetrack = Nbasetrack;

	ofs.write((char*)& m_all.header, sizeof(mfile1::MFileHeader));
	ofs.write((char*)& m_all.info_header, sizeof(mfile1::MFileInfoHeader));

	int count = 0;
	int64_t max = m_all.info_header.Nchain;

	for (uint64_t i = 0; i < m_all.chains.size(); i++) {
		if (veto0_set.count(i) == 1)continue;
		if (veto1_set.count(i) == 1)continue;
		if (veto2_set.count(i) == 1)continue;

		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		ofs.write((char*)& m_all.chains[i], sizeof(mfile1::MFileChain));
		assert(m_all.chains[i].nseg == m_all.all_basetracks[i].size());
		for (int b = 0; b < m_all.chains[i].nseg; b++) {
			ofs.write((char*)& m_all.all_basetracks[i][b], sizeof(mfile1::MFileBase));
		}

	}

	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;

}

Chain_inf Calc_Chain_inf(const mfile1::MFile_minimum &m_all,uint64_t num, std::set<uint64_t> &pene_up, std::set<uint64_t> &pene_down, std::set<uint64_t> &edge_up, std::set<uint64_t> &edge_down, std::set<uint64_t> &short_thin){
	Chain_inf ret;
	ret.chainid = m_all.chains[num].chain_id;
	ret.pl0 = m_all.chains[num].pos0/10;
	ret.pl1 = m_all.chains[num].pos1/10;
	ret.nseg = m_all.chains[num].nseg;

	ret.ax_up = m_all.all_basetracks[num].rbegin()->ax;
	ret.ay_up = m_all.all_basetracks[num].rbegin()->ay;
	ret.x_up = m_all.all_basetracks[num].rbegin()->x;
	ret.y_up = m_all.all_basetracks[num].rbegin()->y;
	ret.z_up = m_all.all_basetracks[num].rbegin()->z;

	ret.ax_down = m_all.all_basetracks[num].begin()->ax;
	ret.ay_down = m_all.all_basetracks[num].begin()->ay;
	ret.x_down = m_all.all_basetracks[num].begin()->x;
	ret.y_down = m_all.all_basetracks[num].begin()->y;
	ret.z_down = m_all.all_basetracks[num].begin()->z;

	ret.flg = pene_up.count(num) * 1
		+ edge_up.count(num) * 10
		+ pene_down.count(num) * 100
		+ edge_down.count(num) * 1000
		+ short_thin.count(num) * 10000;

	return ret;

}

void Write_Chain_inf(std::string filename, std::vector<Chain_inf> &chain) {

	std::ofstream ofs(filename, std::ios::binary);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (chain.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	int64_t count = 0;
	int64_t max = chain.size();
	for (int i = 0; i < chain.size(); i++) {
		if (count % 10000 == 0) {
			std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%";
		}
		count++;
		//short thinのveto
		if (chain[i].flg / 10000 == 1)continue;
		//penetrateのveto
		if (chain[i].flg % 10 + (chain[i].flg % 100) / 10 >= 1 && (chain[i].flg % 1000) / 100 + (chain[i].flg % 10000) / 1000 >= 1)continue;
		ofs.write((char*)& chain[i], sizeof(Chain_inf));
	}
	std::cerr << std::right << std::fixed << "\r now writing ..." << std::setw(4) << std::setprecision(1) << count * 100. / max << "%" << std::endl;
	ofs.close();
}


