#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <chrono>

#include <FILE_structure.hpp>

class output_format_micro {
public:
	int pos, view, imager, zone, isg, ph, vph, px;
};
class output_format_base {
public:
	int pl, rawid;
	double ax, ay, x, y, z;
	output_format_micro m[2];
};

class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};
class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};


bool checkFileExistence(const std::string& str);
void input_output_header(FILE* fp_in, FILE* fp_out, int64_t link_num);
int64_t file_size(std::string filename);

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "prg input-linklet(bin) output-header(bin)\n");
		exit(1);
	}
	std::string file_in_linklet = argv[1];
	std::string file_out_txt = argv[2];
	if (checkFileExistence(file_in_linklet) == false) {
		fprintf(stderr, "file [%s] not exist\n", file_in_linklet.c_str());
		return 0;
	}

	int64_t link_num = file_size(file_in_linklet) / sizeof(output_format_link);
	FILE*fp_in, *fp_out;
	if ((fp_out = fopen(file_out_txt.c_str(), "ab")) == NULL) {
		printf("file open error!!\n");
		exit(EXIT_FAILURE);	/* (3)エラーの場合は通常、異常終了する */
	}
	if ((fp_in = fopen(file_in_linklet.c_str(), "rb")) == NULL) {
		printf("%s file not open!\n", file_in_linklet.c_str());
		exit(EXIT_FAILURE);
	}
	auto start = std::chrono::system_clock::now(); // 計測開始時間
	input_output_header(fp_in, fp_out, link_num);
	auto end = std::chrono::system_clock::now();  // 計測終了時間
	double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count(); //処理に要した時間をミリ秒に変換
	printf("elapsed %.1lf [s]\n", elapsed / 1000);
	fclose(fp_in);
	fclose(fp_out);

	return 0;

}
bool checkFileExistence(const std::string& str)
{
	std::ifstream ifs(str);
	return ifs.is_open();
}

int64_t file_size(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2;
}

void input_output_header(FILE* fp_in, FILE* fp_out, int64_t link_num) {
	output_format_link link[100];
	linklet_header link_head[100];
	int64_t  now = 0;
	int read_num;
	bool flg = true;
	while (flg) {
		if (link_num - now == 100) {
			read_num = link_num - now;
			flg = false;
		}
		else if (link_num - now < 100) {
			read_num = link_num - now;
			flg = false;
		}
		else {
			read_num = 100;
		}
		fread(&link, sizeof(output_format_link), read_num, fp_in);
		for (int i = 0; i < read_num; i++) {
			link_head[i].pos0 = link[i].b[0].pl * 10;
			link_head[i].raw0 = link[i].b[0].rawid;
			link_head[i].pos1 = link[i].b[1].pl * 10;
			link_head[i].raw1 = link[i].b[1].rawid;
			//printf("%d %d %d %d\n", link_head[i].pos0, link_head[i].raw0, link_head[i].pos1, link_head[i].raw1);
		}
		fwrite(link_head, sizeof(linklet_header), read_num, fp_out);

		now += read_num;
	}
	printf("write fin %lld\n", now);
}
