#include <filesystem>
#include <fstream>
#include <map>
#include <sstream>
#include <picojson.h>
#include <set>

class GrainInformation {
public:
	int id, face, ix, iy, layer, pixelnum, brightness_sum9;
	double x, y, z;
};
class AffineParam {
public:
	double rotation, shrink, shift[2];
};

std::vector<GrainInformation> read_grain_inf(std::string filename, int &face);
void read_param_dc(std::string filename, AffineParam&param, AffineParam&param_dc);
void apply_param(std::vector<GrainInformation>&grain, AffineParam&param, AffineParam&param_dc, int face);
void output_grain(std::string filename, std::vector< GrainInformation > &grain);
std::pair<double, double> get_view_center(std::vector<GrainInformation>&grain);
void output_shift(std::string filename, std::pair<double, double>&center, AffineParam&param, int pl, int face);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "prg file-in-grain file-param pl output-file\n");
		exit(1);
	}
	std::string file_in_grain = argv[1];
	std::string file_in_param = argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_out = argv[4];

	int face;
	std::vector<GrainInformation> grain_all = read_grain_inf(file_in_grain, face);
	std::pair<double, double> view_center = get_view_center(grain_all);
	AffineParam param, param_dc;
	read_param_dc(file_in_param, param, param_dc);
	output_shift(file_out, view_center, param, pl,face);

}
std::vector<GrainInformation> read_grain_inf(std::string filename, int &face) {
	std::ifstream ifs(filename);
	std::vector<GrainInformation> ret;
	GrainInformation gr;
	int count = 0;
	while (ifs >> gr.face >> gr.ix >> gr.iy >> gr.layer >> gr.x >> gr.y >> gr.z >> gr.pixelnum >> gr.brightness_sum9) {
		if (count % 100000 == 0) {
			printf("\r grain read %d", count);
		}
		face = gr.face;
		gr.id = count;
		count++;
		ret.push_back(gr);
	}
	printf("\r grain read fin %d\n", ret.size());
	return ret;

}
void read_param_dc(std::string filename, AffineParam&param, AffineParam&param_dc) {

	std::ifstream ifs(filename);
	ifs >> param.rotation >> param.shrink >> param.shift[0] >> param.shift[1];
	ifs >> param_dc.rotation >> param_dc.shrink >> param_dc.shift[0] >> param_dc.shift[1];



}
std::pair<double, double> get_view_center(std::vector<GrainInformation>&grain) {
	double xmin, xmax, ymin, ymax;
	for (auto itr = grain.begin(); itr != grain.end(); itr++) {
		if (itr == grain.begin()) {
			xmin = itr->x;
			xmax = itr->x;
			ymin = itr->y;
			ymax = itr->y;
		}

		xmin = std::min(xmin, itr->x);
		xmax = std::max(xmax, itr->x);
		ymin = std::min(ymin, itr->y);
		ymax = std::max(ymax, itr->y);
	}
	std::pair<double, double> ret;
	ret.first = (xmin + xmax) / 2;
	ret.second = (ymin + ymax) / 2;
	return ret;
}
void output_shift(std::string filename,std::pair<double, double>&center, AffineParam&param,int pl,int face) {
	std::pair<double, double> after;
	after.first = param.shrink*(cos(param.rotation)*center.first - sin(param.rotation)*center.second) + param.shift[0];
	after.second = param.shrink*(sin(param.rotation)*center.first + cos(param.rotation)*center.second) + param.shift[1];


	double shift_x = after.first - center.first;
	double shift_y = after.second - center.second;

	std::ofstream ofs(filename, std::ios::app);
	ofs << std::right << std::fixed
		<< std::setw(3) << std::setprecision(0) << pl << " "
		<< std::setw(3) << std::setprecision(0) << face << " "
		<< std::setw(8) << std::setprecision(1) << after.first << " "
		<< std::setw(8) << std::setprecision(1) << after.second << " "
		<< std::setw(6) << std::setprecision(1) << shift_x << " "
		<< std::setw(6) << std::setprecision(1) << shift_y << " "
		<< std::setw(7) << std::setprecision(5) << param.shrink << " "
		<< std::setw(7) << std::setprecision(5) << param.rotation << std::endl;

}
