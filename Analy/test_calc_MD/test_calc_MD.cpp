#pragma comment(lib,"functions.lib")
#include <functions.hpp>

int main(int argc, char**argv) {


	matrix_3D::vector_3D		dir0, dir1, pos0, pos1;

	pos0.x = 38665.7;
	pos0.y = 47151.7;
	pos0.z = -35927.0;
	dir0.x = -0.01473;
	dir0.y = -0.0217;
	dir0.z = 1;

	pos1.x = 38595.9;
	pos1.y = 46989.5;
	pos1.z = -29313;
	dir1.x = -0.0158;
	dir1.y = -0.0324;
	dir1.z = 1;

	//pos1.x = 38558.7;
	//pos1.y = 46881.5;
	//pos1.z = -25191;
	//dir1.x = -0.0006;
	//dir1.y = -0.0259;
	//dir1.z = 1;

	double z_range[2] = {pos0.z,pos1.z };
	double extra[2];

	double md = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
	double oa = matrix_3D::opening_angle(dir0, dir1);

	double depth = (extra[0] + extra[1]) / 2;
	printf("md     =%lf\n", md);
	printf("depth  =%lf\n", depth);
	printf("extra z0= %lf\n", extra[0]);
	printf("extra z1= %lf\n", extra[1]);
	printf("dz= %lf\n",pos0.z-pos1.z);
	printf("oa     =%lf\n", oa);
	system("pause");
}