#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

int main(int argc, char**argv) {
	if (argc != 2) {
		fprintf(stderr, "usage:file-in-sf\n");
		exit(1);
	}

	std::string file_in_sf = argv[1];

	std::vector < Sharing_file::Sharing_file > sf = Sharing_file::Read_sharing_file_extension(file_in_sf);

	std::multimap<int, Sharing_file::Sharing_file> sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(itr->unix_time, *itr));
	}
	for (auto itr = sf_map.begin(); itr != sf_map.end(); itr = std::next(itr, sf_map.count(itr->second.unix_time))) {
		std::vector<Sharing_file::Sharing_file> sf_v;
		auto range = sf_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			sf_v.push_back(res->second);
		}

		bool flg = false;
		for (int i = 0; i < sf_v.size(); i++) {
			for (int j = i + 1; j < sf_v.size(); j++) {
				if (sf_v[i].tracker_track_id != sf_v[j].tracker_track_id)flg = true;
			}
		}

		if (flg) {
			for (auto itr = sf_v.begin(); itr != sf_v.end(); itr++) {
				printf("%d %d %d\n", itr->eventid, itr->unix_time, itr->tracker_track_id);
			}
		}

	}


}