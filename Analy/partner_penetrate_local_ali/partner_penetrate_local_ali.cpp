#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <set>
#include <map>
#include <chrono>
#include <filesystem>
#include <omp.h>

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
//mfileの反応店付近をlocalaliに乗せる

std::multimap<std::pair<int, int>, mfile0::M_Base*> Trans_base_list(std::vector<mfile0::M_Chain>&chain, std::set<std::pair<int, int>> &target_pl_pair);
void basetrack_trans_abs(std::vector< mfile0::M_Base*>&base_v, corrmap0::Corrmap &param);
void basetrack_trans_abs_inv(std::vector< mfile0::M_Base*>&base_v, corrmap0::Corrmap &param);
std::map<int, corrmap0::Corrmap> read_corrmap_abs(std::string file_in_ECC);
std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_filename(std::string file_in_ECC);
std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_inv_filename(std::string file_in_ECC);
std::vector <std::pair<mfile0::M_Base*, corrmap_3d::align_param2*>>track_affineparam_correspondence(std::vector< mfile0::M_Base*>&base, std::vector <corrmap_3d::align_param2> &param);
void trans_base_all(std::vector < std::pair<mfile0::M_Base*, corrmap_3d::align_param2*>>&track_pair);
int use_thread(double ratio, bool output);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg file-in-mfile file-in-ECC file-out-mfile\n");
		exit(1);

	}
	std::string file_in_mfile = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_out_mfile = argv[3];

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);
	 
	std::set<std::pair<int, int>> target_pl_pair;
	std::multimap<std::pair<int, int>, mfile0::M_Base*> trans_base = Trans_base_list(m.chains, target_pl_pair);



	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);

	//corrmap abs read
	std::map<int, corrmap0::Corrmap> corrmap_abs = read_corrmap_abs(file_in_ECC);

	//alignment paramerter read
	//これはポインタで参照されるためとっておく
	std::vector < std::pair<std::pair<int, int>, std::vector <corrmap_3d::align_param >>>all_align_param;
	//std::vector<std::pair< std::pair<int, int>, std::vector <corrmap_3d::align_param2 >>>all_align_param2;
	std::map< std::pair<int, int>, std::vector <corrmap_3d::align_param2 >>all_align_param2;
	std::vector<std::pair<std::pair<int, int>, std::string> >  files_in_align = Get_alignment_filename(file_in_ECC);
	std::vector<std::pair<std::pair<int, int>, std::string> >  files_in_align_inv = Get_alignment_inv_filename(file_in_ECC);

	//通常alinment pl0-->pl1 読みこみ
	int count = 0, all = files_in_align.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < files_in_align.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align read %d/%d", count, all);
		}
#pragma omp atomic
		count++;

		if (target_pl_pair.count(std::make_pair(files_in_align[i].first.first, files_in_align[i].first.second)) == 0)continue;

		std::vector <corrmap_3d::align_param > corr = corrmap_3d::read_ali_param(files_in_align[i].second, false);
#pragma omp critical
		all_align_param.push_back(std::make_pair(files_in_align[i].first, corr));
	}
	printf("\r corrmap align read %d/%d fin\n", count, all);

	//alinment inv pl1-->pl0 読みこみ
	count = 0, all = files_in_align_inv.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < files_in_align_inv.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align inv read %d/%d", count, all);
		}
#pragma omp atomic
		count++;
		//隣接だけ読む
		if (target_pl_pair.count(std::make_pair(files_in_align_inv[i].first.first, files_in_align_inv[i].first.second)) == 0)continue;

		std::vector <corrmap_3d::align_param > corr = corrmap_3d::read_ali_param(files_in_align_inv[i].second, false);
#pragma omp critical
		all_align_param.push_back(std::make_pair(files_in_align_inv[i].first, corr));
	}
	printf("\r corrmap align inv read %d/%d fin\n", count, all);

	//Delaunay3角形への分割
	count = 0, all = all_align_param.size();
#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(dynamic,1)
	for (int i = 0; i < all_align_param.size(); i++) {
		if (count % 10 == 0) {
#pragma omp critical
			printf("\r corrmap align calc delaunay %d/%d", count, all);
		}
#pragma omp atomic
		count++;

		std::vector <corrmap_3d::align_param2 >corr2 = DelaunayDivide(all_align_param[i].second);
#pragma omp critical
		all_align_param2.insert(std::make_pair(all_align_param[i].first, corr2));

	}
	printf("\r corrmap align calc delaunay %d/%d fin\n", count, all);



	for (auto itr = trans_base.begin(); itr != trans_base.end(); itr++) {
		if (all_align_param2.count(itr->first) == 0) {
			int count = trans_base.count(itr->first);
			itr = std::next(itr, count - 1);
			continue;
		}
		int pl0 = itr->first.first;
		int pl1 = itr->first.second;
		if (corrmap_abs.count(pl0) == 0) {
			fprintf(stderr, "PL%03d corrmap abs not found\n", pl0);
			exit(1);
		}
		if (corrmap_abs.count(pl1) == 0) {
			fprintf(stderr, "PL%03d corrmap abs not found\n", pl1);
			exit(1);
		}
		auto param_pl0 = corrmap_abs.at(pl0);
		auto param_pl1 = corrmap_abs.at(pl1);
		printf("pl0=%03d pl1=%03d\n", pl0, pl1);

		if (z_map.count(pl0) == 0) {
			fprintf(stderr, "PL%03d zval not found\n", pl0);
			exit(1);
		}
		double z_pl0 = z_map.at(pl0);

		//変換対象のbastrackをリスト
		std::vector< mfile0::M_Base*>base_v;
		auto range = trans_base.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_v.push_back(res->second);
		}
		//pl1で逆変換(basetrack系に直す)
		basetrack_trans_abs_inv(base_v, param_pl1);

		//local ali(pl0のlocal ali)
		std::vector <corrmap_3d::align_param2 > ali_target = all_align_param2.at(itr->first);
		//trackとdelaunay3角形の対応
		std::vector <std::pair< mfile0::M_Base*, corrmap_3d::align_param2*>>track_param_link = track_affineparam_correspondence(base_v, ali_target);
		//basetrackを変換
		trans_base_all(track_param_link);

		for (auto itr2 = base_v.begin(); itr2 != base_v.end(); itr2++) {
			(*itr2)->z += z_pl0;
		}
		//pl0で準変換
		basetrack_trans_abs(base_v, param_pl0);

		int count = trans_base.count(itr->first);
		itr = std::next(itr, count - 1);
	}


	mfile0::write_mfile(file_out_mfile, m);

}


std::multimap<std::pair<int, int>, mfile0::M_Base*> Trans_base_list(std::vector<mfile0::M_Chain>&chain, std::set<std::pair<int, int>> &target_pl_pair) {
	std::multimap<std::pair<int, int>, mfile0::M_Base*> ret;

	std::multimap<int, mfile0::M_Chain*> chain_map;
	for (int i = 0; i < chain.size(); i++) {
		chain_map.insert(std::make_pair(chain[i].basetracks.begin()->group_id, &(chain[i])));
	}

	for (auto itr = chain_map.begin(); itr != chain_map.end(); itr++) {
		int count = chain_map.count(itr->first);
		auto range = chain_map.equal_range(itr->first);

		int muon_pl = 0;
		for (auto res = range.first; res != range.second; res++) {
			if (res->second->basetracks.begin()->flg_i[1] == 0) {
				muon_pl = res->second->pos1 / 10;
			}
		}

		if (itr->second->basetracks.begin()->group_id / 100000 != 121&& itr->second->basetracks.begin()->group_id / 100000 != 134)continue;

		for (auto res = range.first; res != range.second; res++) {
			for (int i = 0; i < res->second->basetracks.size(); i++) {
				int now_pl = res->second->basetracks[i].pos / 10;
				if (fabs(now_pl - muon_pl) > 5)continue;
				ret.insert(std::make_pair(std::make_pair(muon_pl, now_pl), &res->second->basetracks[i]));
				target_pl_pair.insert(std::make_pair(muon_pl, now_pl));
			}
		}
		itr = std::next(itr, count - 1);
	}
	return ret;
}

std::map<int, corrmap0::Corrmap> read_corrmap_abs(std::string file_in_ECC) {
	std::stringstream file_in_corr;
	file_in_corr << file_in_ECC << "\\Area0\\0\\align\\corrmap-abs.lst";

	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corr.str(), corr);

	std::map<int, corrmap0::Corrmap> ret;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		ret.insert(std::make_pair(itr->pos[0] / 10, *itr));
	}
	return ret;
}

std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_filename(std::string file_in_ECC) {
	std::vector<std::pair<std::pair<int, int>, std::string> > ret;
	std::map<std::pair<int, int>, std::string> ret_map;

	std::string file_in_align_path = file_in_ECC + "\\Area0\\0\\align\\fine";
	std::filesystem::directory_iterator iter(file_in_align_path), end;
	std::error_code err;

	std::vector<std::string > file_names;
	for (; iter != end && !err; iter.increment(err)) {
		const std::filesystem::directory_entry entry = *iter;

		file_names.push_back(entry.path().string());
		//printf("%s\n", file_names.back().c_str());
	}
	for (int i = 0; i < file_names.size(); i++) {
		int length = file_names[i].size();
		if (file_names[i].substr(length - 4, 4) != ".txt")continue;
		if (file_names[i].substr(length - 18, 14) != "_interpolation")continue;

		int pl0 = std::stoi(file_names[i].substr(length - 25, 3));
		int pl1 = std::stoi(file_names[i].substr(length - 21, 3));

		ret_map.insert(std::make_pair(std::make_pair(pl0, pl1), file_names[i]));
	}
	for (auto itr = ret_map.begin(); itr != ret_map.end(); itr++) {
		ret.push_back(*itr);
	}
	return ret;
}

std::vector<std::pair<std::pair<int, int>, std::string> >Get_alignment_inv_filename(std::string file_in_ECC) {
	std::vector<std::pair<std::pair<int, int>, std::string> > ret;
	std::map<std::pair<int, int>, std::string> ret_map;

	std::string file_in_align_path = file_in_ECC + "\\Area0\\0\\align_inv\\fine";
	std::filesystem::directory_iterator iter(file_in_align_path), end;
	std::error_code err;

	std::vector<std::string > file_names;
	for (; iter != end && !err; iter.increment(err)) {
		const std::filesystem::directory_entry entry = *iter;

		file_names.push_back(entry.path().string());
		//printf("%s\n", file_names.back().c_str());
	}
	for (int i = 0; i < file_names.size(); i++) {
		int length = file_names[i].size();
		if (file_names[i].substr(length - 4, 4) != ".txt")continue;
		if (file_names[i].substr(length - 18, 14) != "_interpolation")continue;

		int pl0 = std::stoi(file_names[i].substr(length - 25, 3));
		int pl1 = std::stoi(file_names[i].substr(length - 21, 3));

		ret_map.insert(std::make_pair(std::make_pair(pl0, pl1), file_names[i]));
	}
	for (auto itr = ret_map.begin(); itr != ret_map.end(); itr++) {
		ret.push_back(*itr);
	}
	return ret;
}

void basetrack_trans_abs_inv(std::vector< mfile0::M_Base*>&base_v, corrmap0::Corrmap &param) {
	double delta = param.position[0] * param.position[3] - param.position[1] * param.position[2];
	double x, y;
	for (auto &b : base_v) {
		x = b->x - param.position[4];
		y = b->y - param.position[5];
		b->x = (param.position[3] * x - param.position[1] * y) / delta;
		b->y = (param.position[0] * y - param.position[2] * x) / delta;

		delta = param.angle[0] * param.angle[3] - param.angle[1] * param.angle[2];
		x = b->ax - param.angle[4];
		y = b->ay - param.angle[5];
		b->ax = (param.angle[3] * x - param.angle[1] * y) / delta;
		b->ay = (param.angle[0] * y - param.angle[2] * x) / delta;

		b->z = 0;
	}
}
void basetrack_trans_abs(std::vector< mfile0::M_Base*>&base_v, corrmap0::Corrmap &param) {
	double x, y;
	for (auto &b : base_v) {
		x = b->x;
		y = b->y;
		b->x = param.position[0] * x + param.position[1] * y + param.position[4];
		b->y = param.position[2] * x + param.position[3] * y + param.position[5];

		x = b->ax;
		y = b->ay;
		b->ax = param.angle[0] * x + param.angle[1] * y + param.angle[4];
		b->ay = param.angle[2] * x + param.angle[3] * y + param.angle[5];

		b->z = b->z + param.dz;
	}

}




//basetrack-alignment mapの対応
double select_triangle_vale(corrmap_3d::align_param2* param, mfile0::M_Base&base) {
	double x, y;
	double dist = 0;
	x = (param->corr_p[0]->x + param->corr_p[1]->x + param->corr_p[2]->x) / 3;
	y = (param->corr_p[0]->y + param->corr_p[1]->y + param->corr_p[2]->y) / 3;
	dist = (base.x - x)*(base.x - x) + (base.y - y)*(base.y - y);
	return dist;
}
corrmap_3d::align_param2* search_param(std::vector<corrmap_3d::align_param*> &param, mfile0::M_Base&base, std::multimap<int, corrmap_3d::align_param2*>&triangles) {
	//三角形内部
	//最近接三角形
	double dist = 0;
	std::map<double, corrmap_3d::align_param* > dist_map;
	//align_paramを近い順にsort
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		dist = ((*itr)->x - base.x)*((*itr)->x - base.x) + ((*itr)->y - base.y)*((*itr)->y - base.y);
		dist_map.insert(std::make_pair(dist, (*itr)));
	}

	double sign[3];
	bool flg = false;
	int id;

	corrmap_3d::align_param2* ret = triangles.begin()->second;
	for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
		if (itr != dist_map.begin())continue;


		//corrmapのID
		id = itr->second->id;
		if (triangles.count(id) == 0) {
			fprintf(stderr, "alignment triangle ID=%d not found\n", id);
			exit(1);
		}
		//idの属する三角形を探索
		auto range = triangles.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			sign[0] = (itr2->second->corr_p[1]->x - itr2->second->corr_p[0]->x)*(base.y - itr2->second->corr_p[1]->y) - (itr2->second->corr_p[1]->y - itr2->second->corr_p[0]->y)*(base.x - itr2->second->corr_p[1]->x);
			sign[1] = (itr2->second->corr_p[2]->x - itr2->second->corr_p[1]->x)*(base.y - itr2->second->corr_p[2]->y) - (itr2->second->corr_p[2]->y - itr2->second->corr_p[1]->y)*(base.x - itr2->second->corr_p[2]->x);
			sign[2] = (itr2->second->corr_p[0]->x - itr2->second->corr_p[2]->x)*(base.y - itr2->second->corr_p[0]->y) - (itr2->second->corr_p[0]->y - itr2->second->corr_p[2]->y)*(base.x - itr2->second->corr_p[0]->x);
			//printf("point %.lf,%.1lf\n", base.x, base.y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[0]->x, itr2->second->corr_p[0]->y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[1]->x, itr2->second->corr_p[1]->y);
			//printf("triangle %.1lf %.1lf\n", itr2->second->corr_p[2]->x, itr2->second->corr_p[2]->y);
			//printf("sign %.1lf %1.lf %.1lf\n", sign[0], sign[1], sign[2]);
			//printf("  signbit %d %d %d\n", std::signbit(sign[0]), std::signbit(sign[1]), std::signbit(sign[2]));
			//printf("n signbit %d %d %d\n", !std::signbit(sign[0]), !std::signbit(sign[1]), !std::signbit(sign[2]));
			//printf("judge %d\n", (std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2])));
			//printf("\n");

			//符号が3つとも一致でtrue
			if ((std::signbit(sign[0]) && std::signbit(sign[1]) && std::signbit(sign[2])) || (!std::signbit(sign[0]) && !std::signbit(sign[1]) && !std::signbit(sign[2]))) {
				ret = itr2->second;
				flg = true;
				break;
			}
		}
		if (flg)break;
	}
	if (flg) {
		//printf("point in trianlge\n");
		return ret;
	}

	//distが最小になるcorrmapをとってくる
	dist = -1;
	for (auto itr = dist_map.begin(); itr != dist_map.end(); itr++) {
		//corrmapのID
		id = itr->second->id;
		if (triangles.count(id) == 0) {
			fprintf(stderr, "alignment triangle ID=%d not found\n", id);
			exit(1);
		}
		//idの属する三角形を探索
		auto range = triangles.equal_range(id);
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			if (dist<0 || dist>select_triangle_vale(itr2->second, base)) {
				dist = select_triangle_vale(itr2->second, base);
				ret = itr2->second;
			}
		}
	}
	//printf("point not in trianlge\n");
	return ret;
}
std::vector <std::pair<mfile0::M_Base*, corrmap_3d::align_param2*>>track_affineparam_correspondence(std::vector< mfile0::M_Base*>&base, std::vector <corrmap_3d::align_param2> &param) {

	//local alignの視野中心を取り出して、位置でhash
	//local alignの視野中心の作るdelaunay三角形をmapで対応

	std::map<int, corrmap_3d::align_param*> view_center;
	std::multimap<int, corrmap_3d::align_param2*>triangles;
	double xmin = 999999, ymin = 999999, hash = 2000;
	for (auto itr = param.begin(); itr != param.end(); itr++) {
		for (int i = 0; i < 3; i++) {
			view_center.insert(std::make_pair(itr->corr_p[i]->id, (itr->corr_p[i])));
			triangles.insert(std::make_pair(itr->corr_p[i]->id, &(*itr)));
			xmin = std::min(itr->corr_p[i]->x, xmin);
			ymin = std::min(itr->corr_p[i]->y, ymin);
		}
	}
	std::multimap<std::pair<int, int>, corrmap_3d::align_param*> view_center_hash;
	std::pair<int, int>id;
	for (auto itr = view_center.begin(); itr != view_center.end(); itr++) {
		id.first = int((itr->second->x - xmin) / hash);
		id.second = int((itr->second->y - ymin) / hash);
		view_center_hash.insert(std::make_pair(id, itr->second));
	}

	std::vector < std::pair<mfile0::M_Base*, corrmap_3d::align_param2*>> ret;
	std::vector<corrmap_3d::align_param*> param_cand;
	int loop = 0, ix, iy, count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		//if (count % 100000 == 0) {
		//	printf("\r search correspond triangles %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
		//}
		count++;
		ix = ((*itr)->x - xmin) / hash;
		iy = ((*itr)->y - ymin) / hash;
		loop = 1;
		while (true) {
			param_cand.clear();
			for (int iix = ix - loop; iix <= ix + loop; iix++) {
				for (int iiy = iy - loop; iiy <= iy + loop; iiy++) {
					id.first = iix;
					id.second = iiy;
					if (view_center_hash.count(id) != 0) {
						auto range = view_center_hash.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							param_cand.push_back(res->second);
						}
					}
				}
			}
			if (param_cand.size() > 2)break;
			loop++;
		}
		corrmap_3d::align_param2* param2 = search_param(param_cand, *(*itr), triangles);
		ret.push_back(std::make_pair((*itr), param2));
	}
	//printf("\r search correspond triangles %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

	return ret;
}


//変換 zshrink補正-->9para変換
void trans_base(std::vector<mfile0::M_Base*>&base, corrmap_3d::align_param2 *param) {

	matrix_3D::matrix_33 x_rot_mat(0, param->x_rot), y_rot_mat(1, param->y_rot), z_rot_mat(2, param->z_rot), all_trans(0, 0), shear_mat(0, 0), shrink_mat(0, 0);

	shrink_mat.val[0][0] *= param->x_shrink;
	shrink_mat.val[1][1] *= param->y_shrink;
	//shrink_mat.val[2][2] *= param->z_shrink;
	shear_mat.val[0][1] = param->yx_shear;
	//shear_mat.val[0][2] = param->zx_shear;
	//shear_mat.val[1][2] = param->zy_shear;

	matrix_3D::vector_3D shift, center;
	center.x = param->x;
	center.y = param->y;
	center.z = param->z;
	shift.x = param->dx;
	shift.y = param->dy;
	shift.z = param->dz;

	all_trans.matrix_multiplication(shear_mat);
	all_trans.matrix_multiplication(shrink_mat);
	all_trans.matrix_multiplication(z_rot_mat);
	all_trans.matrix_multiplication(y_rot_mat);
	all_trans.matrix_multiplication(x_rot_mat);

	//all_trans.Print();
	matrix_3D::vector_3D base_p0, base_p1;
	double base_thick = 210;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_p0.x = (*itr)->x;
		base_p0.y = (*itr)->y;
		base_p0.z = param->z;

		base_p1.x = (*itr)->x + (*itr)->ax*(base_thick);
		base_p1.y = (*itr)->y + (*itr)->ay*(base_thick);
		//角度shrink分はここでかける
		base_p1.z = param->z + (base_thick) / param->z_shrink;

		//視野中心を原点に移動
		//base_p0 = matrix_3D::addition(base_p0, matrix_3D::const_multiple(center, -1));
		//base_p1 = matrix_3D::addition(base_p1, matrix_3D::const_multiple(center, -1));

		//変換の実行
		base_p0.matrix_multiplication(all_trans);
		base_p0 = matrix_3D::addition(base_p0, shift);
		base_p1.matrix_multiplication(all_trans);
		base_p1 = matrix_3D::addition(base_p1, shift);

		//原点をもとに戻す
		//base_p0 = matrix_3D::addition(base_p0, center);
		//base_p1 = matrix_3D::addition(base_p1, center);

		(*itr)->x = base_p0.x;
		(*itr)->y = base_p0.y;
		(*itr)->z = base_p0.z;

		//printf("ax:%.4lf --> %.4lf\n", (*itr)->ax, (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z));
		//printf("ay:%.4lf --> %.4lf\n", (*itr)->ay, (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z));

		(*itr)->ax = (base_p0.x - base_p1.x) / (base_p0.z - base_p1.z) + param->zx_shear;
		(*itr)->ay = (base_p0.y - base_p1.y) / (base_p0.z - base_p1.z) + param->zy_shear;
	}
}
void trans_base_all(std::vector < std::pair<mfile0::M_Base*, corrmap_3d::align_param2*>>&track_pair) {
	std::map<std::tuple<int, int, int>, corrmap_3d::align_param2*> param_map;
	std::multimap<std::tuple<int, int, int>, mfile0::M_Base*>base_map;
	std::tuple<int, int, int>id;
	//三角形ごとにbasetrackをまとめる
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		std::get<0>(id) = itr->second->corr_p[0]->id;
		std::get<1>(id) = itr->second->corr_p[1]->id;
		std::get<2>(id) = itr->second->corr_p[2]->id;
		param_map.insert(std::make_pair(id, itr->second));
		base_map.insert(std::make_pair(id, itr->first));
	}


	//ここで三角形ごとに変換
	int count = 0;
	std::vector<mfile0::M_Base*> t_base;
	for (auto itr = param_map.begin(); itr != param_map.end(); itr++) {
		//if (count % 1000 == 0) {
		//	printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)", count, param_map.size(), count*100. / param_map.size());
		//}
		count++;

		t_base.clear();

		if (base_map.count(itr->first) == 0)continue;
		auto range = base_map.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			t_base.push_back(res->second);
		}
		trans_base(t_base, itr->second);

	}
	//printf("\r basetrack trans num of triangles %d/%d(%4.1lf%%)\n", count, param_map.size(), count*100. / param_map.size());

}


int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
