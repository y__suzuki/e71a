#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>


std::map<int, bool> read_eventid(std::string filename);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "filename origin-mfile add-mfile event-id output\n");
		exit(1);
	}



	std::string file_in_mfile_ori = argv[1];
	std::string file_in_mfile_add = argv[2];
	std::string file_in_eventid = argv[3];
	std::string file_out_mfile = argv[4];

	mfile0::Mfile m,m_add;
	mfile0::read_mfile(file_in_mfile_ori, m);
	mfile0::read_mfile(file_in_mfile_add, m_add);


	//追加で入れたいeventid
	std::map<int, bool> eventid = read_eventid(file_in_eventid);

	//もうidがあるところはtrue
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		auto res = eventid.find(itr->basetracks.begin()->group_id/100000);
		if (res != eventid.end()) {
			res->second = true;
		}
	}

	int id;
	std::set<int> input_id;
	for (auto itr = m_add.chains.begin(); itr != m_add.chains.end(); itr++) {
		id = itr->basetracks.begin()->group_id / 100000;
		auto res = eventid.find(itr->basetracks.begin()->group_id / 100000);
		if (res != eventid.end()) {
			//mにそのイベントのchainがない場合追加
			if (!res->second||input_id.count(id)==1) {
				res->second = true;
				input_id.insert(id);
				m.chains.push_back(*itr);
			}
		}
	}
	
	std::set<int> muon_num;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			muon_num.insert(itr2->group_id / 100000);
		}
	}
	fprintf(stderr,"muon num = %d\n", muon_num.size());


	mfile0::write_mfile(file_out_mfile, m);
	//ないidは出力
	for (auto itr = eventid.begin(); itr != eventid.end(); itr++) {
		if (!itr->second) {
			printf("%d\n", itr->first);
		}
	}





}
std::map<int, bool> read_eventid(std::string filename){
	std::ifstream ifs(filename);

	std::map<int, bool> ret;
	int id;
	while (ifs >> id) {
		ret.insert(std::make_pair(id, false));
	}

	return ret;
}