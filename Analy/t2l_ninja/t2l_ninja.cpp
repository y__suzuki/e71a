#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "functions.lib")
#include <functions.hpp>
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"


#include <filesystem>

class output_format_base{
public:
	int pl,rawid, ph0,ph1;
	double ax, ay, x, y, z;
};
class output_format_link {
public:
	output_format_base b[2];
	double dax, day, dx, dy, dar, dal, dr, dl;
	void Calc_difference();

};

double nominal_gap(std::string file_in_ECC, int pl[2]);
std::string Set_file_read_bvxx_path(std::string file_ECC_path, int pl, int area);
std::string Set_file_read_ali_path(std::string file_ECC_path, int pl[2], int area);

std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>> Make_base_corrmap_pair(std::vector<vxx::base_track_t>&base, std::vector<corrmap0::Corrmap> &corr);
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> &corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap*> &corr_map, double hash_size);
void basetrack_trans_affine(std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>>&base_pairs, double nominal_z);

std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> connect_track_all(std::vector<vxx::base_track_t>&base0, std::vector<vxx::base_track_t>&base1);
std::vector<vxx::base_track_t*> connect_track(vxx::base_track_t &t, std::vector<vxx::base_track_t*> &connect_cand);
bool judge_connect_xy(vxx::base_track_t &t1, vxx::base_track_t &t2);
bool judge_connect_rl(vxx::base_track_t &t1, vxx::base_track_t &t2);
void Calc_position_difference(vxx::base_track_t &t1, vxx::base_track_t &t2, double &dr, double &dl);

output_format_link output_format(vxx::base_track_t &t1, vxx::base_track_t &t2);
void output_pair(std::string filename, std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&track_pair);
void output_pair_bin(std::string filename, std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&track_pair);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "prg ECC_path pl0 pl1 output-file\n");
		exit(1);
	}
	int area = 0;
	int pl0 = std::stoi(argv[2]);
	int pl1 = std::stoi(argv[3]);
	int pl[2];
	pl[0] = std::min(pl0, pl1);
	pl[1] = std::max(pl0, pl1);

	std::string file_ECC_path = argv[1];
	std::string file_output = argv[4];

	double gap = nominal_gap(file_ECC_path, pl);
	//読み込みfile名設定
	std::string file_in_base[2];
	for (int i = 0; i < 2; i++) {
		file_in_base[i] = Set_file_read_bvxx_path(file_ECC_path, pl[i], area);
	}
	std::string file_in_ali = Set_file_read_ali_path(file_ECC_path, pl, area);


	//fileの存在確認・読み込み
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base[2];
	for (int i = 0; i < 2; i++) {
		if (!std::filesystem::exists(file_in_base[i]) ){
			fprintf(stderr,"%s not exist\n",file_in_base[i].c_str());
			exit(1);
		}
		base[i] = br.ReadAll(file_in_base[i], pl[i], 0);
	}
	std::vector<corrmap0::Corrmap> corr;
	if (!std::filesystem::exists(file_in_ali)) {
		fprintf(stderr, "%s not exist\n", file_in_ali.c_str());
		exit(1);
	}
	corrmap0::read_cormap(file_in_ali, corr);

	//for (int i = 0; i < 10; i++) {
	//	printf("%d %5.4lf %5.4lf %8.1lf %8.1lf %8.1lf\n", base[1][i].rawid, base[1][i].ax, base[1][i].ay, base[1][i].x, base[1][i].y, base[1][i].z);
	//}

	//
	std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>> base_pairs = Make_base_corrmap_pair(base[1], corr);
	basetrack_trans_affine(base_pairs, gap);

	//for (int i = 0; i < 10; i++) {
	//	printf("%d %5.4lf %5.4lf %8.1lf %8.1lf %8.1lf\n", base[1][i].rawid, base[1][i].ax, base[1][i].ay, base[1][i].x, base[1][i].y, base[1][i].z);
	//}
	std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> connect_track_pair = connect_track_all(base[0], base[1]);
	
	//output_pair(file_output, connect_track_pair);
	output_pair_bin(file_output, connect_track_pair);

}


std::string Set_file_read_bvxx_path(std::string file_ECC_path, int pl, int area) {
	std::stringstream file_in_base;
	file_in_base<< file_ECC_path << "\\Area" << area << "\\PL" << std::setw(3) << std::setfill('0') << pl
		<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
	return file_in_base.str();

}
std::string Set_file_read_ali_path(std::string file_ECC_path, int pl[2], int area) {
	std::stringstream file_in_ali;
	file_in_ali <<file_ECC_path << "\\Area" << area << "\\0\\align\\corrmap-align-"
		<< std::setw(3) << std::setfill('0') << pl[0] << "-"
		<< std::setw(3) << std::setfill('0') << pl[1] << ".lst";
	return file_in_ali.str();

}
double nominal_gap(std::string file_in_ECC,int pl[2]) {

	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	if (z_map.count(pl[0]) + z_map.count(pl[1]) != 2) {
		fprintf(stderr, "exception nominal gap not found\n");
		exit(1);
	}
	//pl[0]のz=0の座標系
	return z_map[pl[1]] - z_map[pl[0]];
}

std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>> Make_base_corrmap_pair(std::vector<vxx::base_track_t>&base, std::vector<corrmap0::Corrmap> &corr) {
	double hash_size = 4000;
	std::multimap<std::pair<int, int>, corrmap0::Corrmap*> corr_map;
	corrmap_area_inverse(corr, corr_map, hash_size);

	std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>> base_pairs;

	int ix, iy;
	int count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (count % 100000 == 0) {
			printf("\r search corrmap %d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
		}
		count++;
		std::pair<int, int> id;
		ix = (itr)->x / hash_size;
		iy = (itr)->y / hash_size;
		std::vector<corrmap0::Corrmap*> corr_list;
		int loop = 0;
		while (corr_list.size() <= 3) {
			loop++;
			for (int iix = -1 * loop; iix <= 1 * loop; iix++) {
				for (int iiy = -1 * loop; iiy <= 1 * loop; iiy++) {
					id.first = ix + iix;
					id.second = iy + iiy;
					if (corr_map.count(id) == 0)continue;
					auto range = corr_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						corr_list.push_back(res->second);
					}
				}
			}
		}
		double dist;
		corrmap0::Corrmap* param;
		for (auto itr2 = corr_list.begin(); itr2 != corr_list.end(); itr2++) {
			double cx, cy;
			cx = ((*itr2)->areax[0] + (*itr2)->areax[1]) / 2;
			cy = ((*itr2)->areay[0] + (*itr2)->areay[1]) / 2;
			if (itr2 == corr_list.begin()) {
				dist = (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy);
				param = (*itr2);
			}
			if (dist > (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy)) {
				dist = (itr->x - cx)*(itr->x - cx) + (itr->y - cy)*(itr->y - cy);
				param = (*itr2);
			}

		}
		base_pairs.push_back(std::make_pair(&(*itr), param));
	}
	printf("\r search corrmap %d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

	return base_pairs;
}
void corrmap_area_inverse(std::vector<corrmap0::Corrmap> &corr, std::multimap<std::pair<int, int>, corrmap0::Corrmap*> &corr_map, double hash_size) {

	//hash_sizeはcorrmapのサイズより大きくする
	double cx, cy;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		double area[4], factor;
		area[0] = itr->areax[0] - itr->position[4];
		area[1] = itr->areax[1] - itr->position[4];
		area[2] = itr->areay[0] - itr->position[5];
		area[3] = itr->areay[1] - itr->position[5];

		factor = 1 / (itr->position[0] * itr->position[3] - itr->position[1] * itr->position[2]);
		itr->areax[0] = factor * (area[0] * itr->position[3] - area[2] * itr->position[1]);
		itr->areay[0] = factor * (area[2] * itr->position[0] - area[0] * itr->position[2]);
		itr->areax[1] = factor * (area[1] * itr->position[3] - area[3] * itr->position[1]);
		itr->areay[1] = factor * (area[3] * itr->position[0] - area[1] * itr->position[2]);

		cx = (itr->areax[0] + itr->areax[1]) / 2;
		cy = (itr->areay[0] + itr->areay[1]) / 2;

		std::pair<int, int>id;
		id.first = cx / hash_size;
		id.second = cy / hash_size;
		corr_map.insert(std::make_pair(id, &(*itr)));
	}

}
void basetrack_trans_affine(std::vector<std::pair<vxx::base_track_t*, corrmap0::Corrmap*>>&base_pairs, double nominal_z) {
	//position rot+shift+拡大
	//angle rot
	double x_tmp, y_tmp;
	double sqrt_det;
	double cos, sin;
	for (auto p : base_pairs) {
		sqrt_det = sqrt(p.second->position[0] * p.second->position[3] - p.second->position[1] * p.second->position[2]);
		cos = p.second->position[0] / sqrt_det;
		sin = -1 * p.second->position[1] / sqrt_det;
		x_tmp = p.first->x;
		y_tmp = p.first->y;
		p.first->x = x_tmp * p.second->position[0] + y_tmp * p.second->position[1] + p.second->position[4];
		p.first->y = x_tmp * p.second->position[2] + y_tmp * p.second->position[3] + p.second->position[5];
		x_tmp = p.first->ax;
		y_tmp = p.first->ay;
		p.first->ax = x_tmp * cos + y_tmp * -1 * sin;
		p.first->ay = x_tmp * sin + y_tmp * cos;

		p.first->z = nominal_z + p.second->dz;
	}
}

std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> connect_track_all(std::vector<vxx::base_track_t>&base0, std::vector<vxx::base_track_t>&base1) {

	std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>> connected;
	double hash_pos_size = 2000;
	double hash_ang_size = 0.5;
	double min[4];
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		if (itr == base0.begin()) {
			min[0] = itr->x;
			min[1] = itr->y;
			min[2] = itr->ax;
			min[3] = itr->ay;
		}
		min[0] = std::min(min[0], itr->x);
		min[1] = std::min(min[1], itr->y);
		min[2] = std::min(min[2], itr->ax);
		min[3] = std::min(min[3], itr->ay);
		itr->z = 0;
	}

	std::multimap< std::tuple<int, int, int, int>, vxx::base_track_t*>base_map;
	std::tuple<int, int, int, int> id;
	for (auto itr = base0.begin(); itr != base0.end(); itr++) {
		std::get<0>(id) = (itr->x - min[0]) / hash_pos_size;
		std::get<1>(id) = (itr->y - min[1]) / hash_pos_size;
		std::get<2>(id) = (itr->ax - min[2]) / hash_ang_size;
		std::get<3>(id) = (itr->ay - min[3]) / hash_ang_size;
		base_map.insert(std::make_pair(id, &(*itr)));
	}

	//base1
	double ex_x, ex_y;
	double x_min, x_max, y_min, y_max;
	double ax_min, ax_max, ay_min, ay_max;
	double angle_acc_x, angle_acc_y;
	int i_ax_min, i_ax_max, i_ay_min, i_ay_max;
	int i_x_min, i_x_max, i_y_min, i_y_max;
	std::vector<vxx::base_track_t*> connect_cand;
	int count = 0;
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		if (count % 10000 == 0) {
			printf("\r connect track %10d/%10d (%4.1lf%%)", count, base1.size(), count*100. / base1.size());
		}
		count++;

		connect_cand.clear();
		//角度精度
		angle_acc_x = 0.05 + 0.15*fabs(itr->ax);
		angle_acc_y = 0.05 + 0.15*fabs(itr->ay);
		//z=0に外挿
		ex_x = itr->ax*(-1)*itr->z + itr->x;
		ex_y = itr->ay*(-1)*itr->z + itr->y;
		//pos_center+-(angle_acc*gap)*sigma
		x_min = ex_x - angle_acc_x * fabs(itr->z);
		x_max = ex_x + angle_acc_x * fabs(itr->z);
		y_min = ex_y - angle_acc_y * fabs(itr->z);
		y_max = ex_y + angle_acc_y * fabs(itr->z);
		//ang_center+-(angle_acc)*sigma
		ax_min = itr->ax - angle_acc_x;
		ax_max = itr->ax + angle_acc_x;
		ay_min = itr->ay - angle_acc_y;
		ay_max = itr->ay + angle_acc_y;

		//hash idに変換
		i_x_min = (x_min - min[0]) / hash_pos_size;
		i_x_max = (x_max - min[0]) / hash_pos_size;
		i_y_min = (y_min - min[1]) / hash_pos_size;
		i_y_max = (y_max - min[1]) / hash_pos_size;
		i_ax_min = (ax_min - min[2]) / hash_ang_size;
		i_ax_max = (ax_max - min[2]) / hash_ang_size;
		i_ay_min = (ay_min - min[3]) / hash_ang_size;
		i_ay_max = (ay_max - min[3]) / hash_ang_size;

		//hash mapの中から該当trackを探す
		for (int ix = i_x_min; ix <= i_x_max; ix++) {
			for (int iy = i_y_min; iy <= i_y_max; iy++) {
				for (int iax = i_ax_min; iax <= i_ax_max; iax++) {
					for (int iay = i_ay_min; iay <= i_ay_max; iay++) {
						std::get<0>(id) = ix;
						std::get<1>(id) = iy;
						std::get<2>(id) = iax;
						std::get<3>(id) = iay;
						if (base_map.count(id) == 0)continue;
						auto range = base_map.equal_range(id);
						for (auto res = range.first; res != range.second; res++) {
							connect_cand.push_back(res->second);
						}
					}
				}
			}
		}
		connect_cand = connect_track(*itr, connect_cand);
		if (connect_cand.size() == 0)continue;
		for (auto itr2 = connect_cand.begin(); itr2 != connect_cand.end(); itr2++) {
			connected.push_back(std::make_pair((*itr2), &(*itr)));
		}
	}
	printf("\r connect track %10d/%10d (%4.1lf%%)\n", count, base1.size(), count*100. / base1.size());

	return connected;

}
std::vector<vxx::base_track_t*> connect_track(vxx::base_track_t &t, std::vector<vxx::base_track_t*> &connect_cand) {
	std::vector<vxx::base_track_t*> ret;
	for (auto itr = connect_cand.begin(); itr != connect_cand.end(); itr++) {
		if (judge_connect_xy(t, *(*itr))&& judge_connect_rl(t, *(*itr))) {
			ret.push_back(*itr);
		}
	}
	return ret;
}
bool judge_connect_xy(vxx::base_track_t &t1, vxx::base_track_t &t2) {
	double angle, d_pos_x, d_pos_y, d_ang_x, d_ang_y;
	double all_pos_x, all_pos_y, all_ang_x, all_ang_y;

	all_ang_x = 0.15*fabs(t2.ax + t1.ax) / 2 + 0.1;
	all_ang_y = 0.15*fabs(t2.ay + t1.ay) / 2 + 0.1;

	all_pos_x = (0.1*fabs(t2.ax + t1.ax) / 2 + 0.15)*(t1.z - t2.z);
	all_pos_y = (0.1*fabs(t2.ay + t1.ay) / 2 + 0.15)*(t1.z - t2.z);

	d_pos_x = t2.x -t1.x - (t1.ax + t2.ax) / 2 * (t2.z - t1.z);
	d_pos_y = t2.y - t1.y - (t1.ay + t2.ay) / 2 * (t2.z - t1.z);

	d_ang_x = (t2.ax - t1.ax);
	d_ang_y = (t2.ay - t1.ay);

	if (fabs(d_ang_x) > fabs(all_ang_x))return false;
	if (fabs(d_ang_y) > fabs(all_ang_y))return false;

	if (fabs(d_pos_x) > fabs(all_pos_x))return false;
	if (fabs(d_pos_y) > fabs(all_pos_y))return false;

	return true;
}
bool judge_connect_rl(vxx::base_track_t &t1, vxx::base_track_t &t2) {
	double angle, d_pos_r, d_pos_l, d_ang_r, d_ang_l;
	double all_pos_r, all_pos_l, all_ang_r, all_ang_l;
	angle = sqrt(t1.ax*t1.ax + t1.ay*t1.ay);

	all_ang_r = 0.15*angle + 0.1;
	all_ang_l = 0.1;
	all_pos_r = (0.1*angle + 0.15)*(t1.z - t2.z);
	all_pos_l = 0.1*(t1.z - t2.z);


	d_ang_r = ((t2.ax - t1.ax)*t1.ax + (t2.ay - t1.ay)*t1.ay);
	d_ang_l = ((t2.ax - t1.ax)*t1.ay - (t2.ay - t1.ay)*t1.ax);
	if (fabs(d_ang_r) > fabs(all_ang_r)*angle)return false;
	if (fabs(d_ang_l) > fabs(all_ang_l)*angle)return false;

	Calc_position_difference(t1, t2, d_pos_r, d_pos_l);
	if (fabs(d_pos_r) > fabs(all_pos_r))return false;
	if (fabs(d_pos_l) > fabs(all_pos_l))return false;

	return true;
}
void Calc_position_difference(vxx::base_track_t &t1, vxx::base_track_t &t2, double &dr, double &dl) {
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = t1.x;
	pos0.y = t1.y;
	pos0.z = t1.z;
	dir0.x = t1.ax;
	dir0.y = t1.ay;
	dir0.z = 1;
	pos1.x = t2.x;
	pos1.y = t2.y;
	pos1.z = t2.z;
	dir1.x = t2.ax;
	dir1.y = t2.ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);

}

void output_pair(std::string filename, std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&track_pair) {
	std::vector<output_format_link> link;
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		link.push_back(output_format(*(itr->first), *(itr->second)));
	}

	std::ofstream ofs(filename);
	int count = 0;
	for (auto l : link) {
		if (count % 10000 == 0) {
			printf("\r write linklet %10d/%10d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;

		ofs << std::right << std::fixed;
		for (int i = 0; i < 2; i++) {
			ofs << std::setw(4) << std::setprecision(0) << l.b[i].pl << " "
				<< std::setw(10) << std::setprecision(0) << l.b[i].rawid << " "
				<< std::setw(6) << std::setprecision(0) << l.b[i].ph0 << " "
				<< std::setw(6) << std::setprecision(0) << l.b[i].ph1 << " "
				<< std::setw(7) << std::setprecision(4) << l.b[i].ax << " "
				<< std::setw(7) << std::setprecision(4) << l.b[i].ay << " "
				<< std::setw(8) << std::setprecision(1) << l.b[i].x << " "
				<< std::setw(8) << std::setprecision(1) << l.b[i].y << " "
				<< std::setw(8) << std::setprecision(1) << l.b[i].z << " ";
		}
		ofs << std::setw(7) << std::setprecision(4) << l.dax << " "
			<< std::setw(7) << std::setprecision(4) << l.day << " "
			<< std::setw(6) << std::setprecision(1) << l.dx << " "
			<< std::setw(6) << std::setprecision(1) << l.dy << " "
			<< std::setw(7) << std::setprecision(4) << l.dar << " "
			<< std::setw(7) << std::setprecision(4) << l.dal << " "
			<< std::setw(6) << std::setprecision(1) << l.dr << " "
			<< std::setw(6) << std::setprecision(1) << l.dl << std::endl;
	}
	printf("\r write linklet %10d/%10d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

}
void output_pair_bin(std::string filename, std::vector<std::pair<vxx::base_track_t*, vxx::base_track_t*>>&track_pair) {
	std::vector<output_format_link> link;
	for (auto itr = track_pair.begin(); itr != track_pair.end(); itr++) {
		link.push_back(output_format(*(itr->first), *(itr->second)));
	}

	std::ofstream ofs(filename,std::ios::binary);
	int count = 0;
	for (auto l : link) {
		if (count % 10000 == 0) {
			printf("\r write linklet %10d/%10d(%4.1lf%%)", count, link.size(), count*100. / link.size());
		}
		count++;
		ofs.write((char*)& l, sizeof(output_format_link));
	}

	printf("\r write linklet %10d/%10d(%4.1lf%%)\n", count, link.size(), count*100. / link.size());

}
output_format_link output_format(vxx::base_track_t &t1, vxx::base_track_t &t2) {
	output_format_link l;
	l.b[0].pl = t1.pl;
	l.b[0].rawid = t1.rawid;
	l.b[0].ph0 = t1.m[0].ph;
	l.b[0].ph1 = t1.m[1].ph;
	l.b[0].ax = t1.ax;
	l.b[0].ay = t1.ay;
	l.b[0].x = t1.x;
	l.b[0].y = t1.y;
	l.b[0].z = t1.z;

	l.b[1].pl = t2.pl;
	l.b[1].rawid = t2.rawid;
	l.b[1].ph0 = t2.m[0].ph;
	l.b[1].ph1 = t2.m[1].ph;
	l.b[1].ax = t2.ax;
	l.b[1].ay = t2.ay;
	l.b[1].x = t2.x;
	l.b[1].y = t2.y;
	l.b[1].z = t2.z;

	l.Calc_difference();
	return l;
}
void output_format_link::Calc_difference() {

	dax = b[1].ax - b[0].ax;
	day = b[1].ay - b[0].ay;
	dx = b[1].x - b[0].x - (b[0].ax + b[1].ax) / 2 * (b[1].z - b[0].z);
	dy = b[1].y - b[0].y - (b[0].ay + b[1].ay) / 2 * (b[1].z - b[0].z);
	dar = (dax*b[0].ax + day * b[0].ay) / sqrt(b[0].ax*b[0].ax + b[1].ay*b[1].ay);
	dal = (dax*b[0].ay - day * b[0].ax) / sqrt(b[0].ax*b[0].ax + b[1].ay*b[1].ay);
	

	//dr,dlの計算
	using namespace matrix_3D;
	vector_3D pos0, pos1, dir0, dir1;
	pos0.x = b[0].x;
	pos0.y = b[0].y;
	pos0.z = b[0].z;
	dir0.x = b[0].ax;
	dir0.y = b[0].ay;
	dir0.z = 1;
	pos1.x = b[1].x;
	pos1.y = b[1].y;
	pos1.z = b[1].z;
	dir1.x = b[1].ax;
	dir1.y = b[1].ay;
	dir1.z = 1;

	vector_3D base_point, difference;
	//外挿基準点を1:1に内分した点に設定
	base_point = addition(const_multiple(pos0, 0.5), const_multiple(pos1, 0.5));
	difference = addition(const_multiple(pos0, -1), pos1);

	vector_3D extra0, extra1;
	double ratio0, ratio1;
	ratio0 = -1 * dot(addition(pos0, const_multiple(base_point, -1)), difference) / dot(dir0, difference);
	ratio1 = -1 * dot(addition(pos1, const_multiple(base_point, -1)), difference) / dot(dir1, difference);
	extra0 = addition(pos0, const_multiple(dir0, ratio0));
	extra1 = addition(pos1, const_multiple(dir1, ratio1));

	vector_3D unit_r, unit_l;
	unit_l.x = -1 + difference.y;
	unit_l.y = difference.x;
	unit_l.z = 0;
	unit_r.x = -1 * difference.x*difference.z;
	unit_r.y = -1 * difference.y*difference.z;
	unit_r.z = pow(difference.x, 2) + pow(difference.y, 2);

	double constant;
	constant = sqrt(pow(difference.x, 2) + pow(difference.y, 2));
	unit_l.x = unit_l.x / constant;
	unit_l.y = unit_l.y / constant;
	constant = sqrt((pow(difference.x, 2) + pow(difference.y, 2))*(pow(difference.x, 2) + pow(difference.y, 2) + pow(difference.z, 2)));
	unit_r.x = unit_r.x / constant;
	unit_r.y = unit_r.y / constant;
	unit_r.z = unit_r.z / constant;

	dr = dot(addition(extra1, const_multiple(extra0, -1)), unit_r);
	dl = dot(addition(extra1, const_multiple(extra0, -1)), unit_l);


}
