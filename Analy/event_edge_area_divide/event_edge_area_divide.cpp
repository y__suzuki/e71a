#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

std::vector<std::pair<mfile0::M_Chain, std::vector<mfile0::M_Chain>>>event_divide(std::vector<mfile0::M_Chain>&chain);
void area_divide(std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> &area, std::vector<std::pair<mfile0::M_Chain, std::vector<mfile0::M_Chain>>>&event, std::vector<mfile0::M_Chain>&chain_inner, std::vector<mfile0::M_Chain>&chain_outer);

int main(int argc, char**argv) {
	if (argc != 6) {
		printf("argc %d\n", argc);
		fprintf(stderr, "usage:file-in-momch  file-in-ECC fa.txt file_out_momch\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_in_area = argv[3];
	std::string file_out_mfile_in = argv[4];
	std::string file_out_mfile_out = argv[5];

	//corrmap absの読み込み
	std::string file_in_corrmap = file_in_ECC + "\\Area0\\0\\align\\fine\\local\\corrmap-local-abs.lst";
	std::map<int, std::vector<corrmap_3d::align_param>>corrmap = corrmap_3d::read_ali_param_abs(file_in_corrmap, 1);
	std::map<int, std::vector<corrmap_3d::align_param2>>corrmap_dd = corrmap_3d::DelaunayDivide_map(corrmap);

	//fiducial areaの読み込み
	std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> area = Fiducial_Area::read_fiducial_Area(file_in_area);
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		if (corrmap_dd.count(itr->first) == 0) {
			fprintf(stderr, "corrmap local abs PL%03d not found\n", itr->first);
			exit(1);
		}
		std::vector<corrmap_3d::align_param2> param = corrmap_dd.at(itr->first);
		trans_mfile_cordinate(param, itr->second);
	}



	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	printf("chain size=%d\n", m.chains.size());

	std::vector<std::pair<mfile0::M_Chain, std::vector<mfile0::M_Chain>>>ev = event_divide(m.chains);

	printf("event size=%d\n", ev.size());
	std::vector<mfile0::M_Chain> chain_inner, chain_outer;
	area_divide(area, ev, chain_inner, chain_outer);

	m.chains = chain_inner;
	mfile0::write_mfile(file_out_mfile_in, m);
	m.chains = chain_outer;
	mfile0::write_mfile(file_out_mfile_out, m);


}
std::vector<std::pair<mfile0::M_Chain, std::vector<mfile0::M_Chain>>>event_divide(std::vector<mfile0::M_Chain>&chain) {
	std::vector<std::pair<mfile0::M_Chain, std::vector<mfile0::M_Chain>>> ret;
	std::map<int, std::vector<mfile0::M_Chain>> event;

	for (auto &c : chain) {
		int eventid = c.basetracks.begin()->group_id;
		auto res = event.find(eventid);
		if (res == event.end()) {
			std::vector < mfile0::M_Chain> c_v;
			c_v.push_back(c);

			event.insert(std::make_pair(eventid, c_v));
		}
		else {
			res->second.push_back(c);
		}
	}
	for (auto itr = event.begin(); itr != event.end(); itr++) {
		mfile0::M_Chain mu;
		bool flg = false;
		for (auto &c : itr->second) {
			if (c.chain_id == 0) {
				flg = true;
				mu = c;
			}
		}
		if (!flg) {
			fprintf(stderr, "event %d muon not found\n",itr->first);
			exit(1);
		}
		ret.push_back(std::make_pair(mu, itr->second));
	}
	return ret;
}

void area_divide(std::map<int, std::vector<Fiducial_Area::Fiducial_Area>> &area,std::vector<std::pair<mfile0::M_Chain, std::vector<mfile0::M_Chain>>>&event, std::vector<mfile0::M_Chain>&chain_inner, std::vector<mfile0::M_Chain>&chain_outer) {
	double judge_distance = 2500;
	for (auto &ev : event) {
		mfile0::M_Chain mu = ev.first;
		mfile0::M_Base mu_up = *mu.basetracks.rbegin();
		int pl = mu_up.pos / 10;
		if (area.count(pl) == 0) {
			fprintf(stderr, "PL%03d ali map not found\n", pl);
			exit(1);
		}
		auto area_pl = area.at(pl);
		bool flg = Fiducial_Area::judge_inner_track(area_pl, mu_up, judge_distance);
		for (auto chain : ev.second) {
			if (flg) {
				chain_inner.push_back(chain);
			}
			else {
				chain_outer.push_back(chain);
			}
		}
	}
}