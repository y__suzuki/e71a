#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

class Fiducial_Area {
public:
	int pl;
	double x0, y0, z0, x1, y1, z1;
};

bool sort_chain_base(const mfile0::M_Base &left, const mfile0::M_Base &right) {
	if (left.pos != right.pos) {
		return left.pos < right.pos;
	}
	else {
		return left.rawid < right.rawid;
	}
}

std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename);
void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map);
std::map<int, int> read_muon_inf(std::string filename);
void connect_chain(std::vector<mfile0::M_Chain>&all, std::map<int, int>&add_chain, std::vector<mfile0::M_Chain>&connect, std::vector<mfile0::M_Chain>&not_connect);
std::vector<mfile0::M_Chain> divide_penetrate(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&penetrate, int veto_pl);
std::vector<mfile0::M_Chain> divide_edge_out(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&edge_out, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map, double edge_cut, int ex_pl_max);
bool judge_fiducial_area(std::vector<Fiducial_Area>&area, double x, double y);


int main(int argc, char** argv) {
	if (argc != 6) {
		fprintf(stderr, "usage: prg file-in-mfile(txt) file-in-ECC fa.txt file-in-memo output-path\n");
		exit(1);
	}

	std::string file_in_mfile = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_in_area = argv[3];
	std::string file_in_txt = argv[4];
	std::string file_out_mfile_path = argv[5];


	//corrmap absの読み込み
	std::string file_in_corrabs = file_in_ECC + "\\0\\align\\corrmap-abs.lst";
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrabs, corr_abs);
	printf("size = %d\n", corr_abs.size());
	//fiducial areaの読み込み
	std::map<int, std::vector<Fiducial_Area>> area = read_fiducial_Area(file_in_area);
	printf("size = %d\n", area.size());

	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);

	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	printf("size = %d\n", z_map.size());

	//corrmap absの適用
	trans_mfile_cordinate(corr_abs, area, z_map);


	std::map<int, int> connect_pair = read_muon_inf(file_in_txt);

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);
	std::vector<mfile0::M_Chain>connect, not_connect;
	printf("fin\n");

	connect_chain(m.chains, connect_pair, connect, not_connect);
	printf("fin\n");

	std::string file_out_pene = file_out_mfile_path + "\\pene.all";
	std::string file_out_stop = file_out_mfile_path + "\\stop.all";
	std::string file_out_stop2 = file_out_mfile_path + "\\stop2.all";
	std::string file_out_eo = file_out_mfile_path + "\\eo.all";
	//つながらない-->出力
	m.chains = not_connect;
	mfile0::write_mfile(file_out_stop, m);


	//penetrate check
	std::vector<mfile0::M_Chain> penetrate;
	//最上流がPL132以上
	connect = divide_penetrate(connect, penetrate, 132);

	//edgeout check
	std::vector<mfile0::M_Chain> edge_out;
	//最上流から4PL外挿,edgeから5mm以内に入ったらedge out
	connect = divide_edge_out(connect, edge_out, area, z_map, 0, 4);


	m.chains = penetrate;
	mfile0::write_mfile(file_out_pene, m);
	m.chains = edge_out;
	mfile0::write_mfile(file_out_eo, m);
	m.chains = connect;
	mfile0::write_mfile(file_out_stop2, m);


}
std::map<int,int> read_muon_inf(std::string filename) {
	std::ifstream ifs(filename);
	std::map<int, int> ret;

	int eventid, chainid;
	while (ifs >> eventid >> chainid) {
		ret.insert(std::make_pair(eventid, chainid));
	}
	return ret;


}

void connect_chain(std::vector<mfile0::M_Chain>&all, std::map<int, int>&add_chain, std::vector<mfile0::M_Chain>&connect, std::vector<mfile0::M_Chain>&not_connect) {
	std::multimap<int, mfile0::M_Chain> event_group;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		event_group.insert(std::make_pair(itr->basetracks.begin()->group_id / 100000, *itr));
	}

	int count = 0;
	for (auto itr = event_group.begin(); itr != event_group.end(); itr++) {
		count = event_group.count(itr->first);
		mfile0::M_Chain muon;
		if (add_chain.count(itr->first) != 0) {
			int partner_cid = add_chain.at(itr->first);
			std::vector<mfile0::M_Base> add_base;

			auto range = event_group.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				if (res->second.pos0 / 10 <= 4) {
					muon = res->second;
				}
				if (res->second.chain_id == partner_cid) {
					for (auto itr2 = res->second.basetracks.begin(); itr2 != res->second.basetracks.end(); itr2++) {
						add_base.push_back(*itr2);
					}
				}
			}
			if (add_base.size() == 0) {
				fprintf(stderr, "eventid = %d  chainid = %d not found\n", itr->first, partner_cid);
				exit(1);
			}
			for (int i = 0; i < add_base.size(); i++) {
				muon.basetracks.push_back(add_base[i]);
			}

			sort(muon.basetracks.begin(), muon.basetracks.end(), sort_chain_base);
			muon.nseg = muon.basetracks.size();
			muon.pos0 = muon.basetracks.begin()->pos;
			muon.pos1 = muon.basetracks.rbegin()->pos;
			connect.push_back(muon);
		}
		else {
			//muonchainの探索
			auto range = event_group.equal_range(itr->first);
			for (auto res = range.first; res != range.second; res++) {
				if (res->second.pos0 / 10 <= 4) {
					muon = res->second;
				}
			}

			sort(muon.basetracks.begin(), muon.basetracks.end(), sort_chain_base);
			muon.nseg = muon.basetracks.size();
			muon.pos0 = muon.basetracks.begin()->pos;
			muon.pos1 = muon.basetracks.rbegin()->pos;
			not_connect.push_back(muon);
		}


		itr = std::next(itr, count - 1);


	}



}

std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename) {

	std::ifstream ifs(filename);
	std::multimap<int, Fiducial_Area> fa_multi;
	std::map<int, std::vector<Fiducial_Area>> ret;
	Fiducial_Area fa;
	while (ifs >> fa.pl >> fa.x0 >> fa.y0 >> fa.z0 >> fa.x1 >> fa.y1 >> fa.z1) {
		fa_multi.insert(std::make_pair(fa.pl, fa));
	}

	int count = 0;
	for (auto itr = fa_multi.begin(); itr != fa_multi.end(); itr++) {
		count = fa_multi.count(itr->first);
		auto range = fa_multi.equal_range(itr->first);
		std::vector<Fiducial_Area> fa_vec;
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			fa_vec.push_back(itr2->second);
		}
		ret.insert(std::make_pair(itr->first, fa_vec));
	}

	return ret;

}

void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map) {
	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		if (area.count(pl) != 0) {
			auto vec = area.find(pl);
			double tmp_x, tmp_y;
			for (auto itr2 = vec->second.begin(); itr2 != vec->second.end(); itr2++) {
				tmp_x = itr2->x0;
				tmp_y = itr2->y0;
				itr2->x0 = tmp_x * itr->position[0] + tmp_y * itr->position[1] + itr->position[4];
				itr2->y0 = tmp_x * itr->position[2] + tmp_y * itr->position[3] + itr->position[5];
				tmp_x = itr2->x1;
				tmp_y = itr2->y1;
				itr2->x1 = tmp_x * itr->position[0] + tmp_y * itr->position[1] + itr->position[4];
				itr2->y1 = tmp_x * itr->position[2] + tmp_y * itr->position[3] + itr->position[5];
			}
		}
		if (z_map.count(pl) != 0) {
			auto z = z_map.find(pl);
			z->second = z->second + itr->dz;
			//printf("PL%03d z:%.1lf\n", pl, z->second);
		}
	}
}


std::vector<mfile0::M_Chain> divide_penetrate(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&penetrate, int veto_pl) {
	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		if (up_pl >= veto_pl) {
			penetrate.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("penetrate = %d\n", penetrate.size());
	return ret;
}

std::vector<mfile0::M_Chain> divide_IronECC(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&iron, int veto_pl) {
	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		if (up_pl <= veto_pl) {
			iron.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("IronECC = %d\n", iron.size());
	return ret;
}

std::vector<mfile0::M_Chain> divide_edge_out(std::vector<mfile0::M_Chain>&all, std::vector<mfile0::M_Chain>&edge_out, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map, double edge_cut, int ex_pl_max) {


	double xmin, xmax, ymin, ymax;


	std::vector<mfile0::M_Chain> ret;
	int up_pl;
	double up_z, up_x, up_y, up_ax, up_ay, ex_z, ex_x, ex_y;
	bool flg = false;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		up_pl = itr->pos1 / 10;
		flg = false;
		up_z = z_map.at(up_pl);
		up_x = itr->basetracks.rbegin()->x;
		up_y = itr->basetracks.rbegin()->y;
		up_ax = itr->basetracks.rbegin()->ax;
		up_ay = itr->basetracks.rbegin()->ay;
		for (int ex_pl = 0; ex_pl <= ex_pl_max; ex_pl++) {
			if (z_map.count(up_pl + ex_pl) == 0)continue;
			ex_z = z_map.at(up_pl + ex_pl);
			ex_x = up_x + up_ax * (ex_z - up_z);
			ex_y = up_y + up_ay * (ex_z - up_z);

			if (!judge_fiducial_area(area.at(up_pl + ex_pl), ex_x, ex_y)) {
				flg = true;
			}
		}
		if (flg) {
			edge_out.push_back(*itr);
		}
		else {
			ret.push_back(*itr);
		}
	}
	printf("edge out = %d\n", edge_out.size());
	return ret;
}

bool judge_fiducial_area(std::vector<Fiducial_Area>&area, double x, double y) {
	//true でArea内　falseでarea外

	//点(x,y)からx軸性の方向に直線を引き、その直線と多角形の辺が何回交わるか。
	//下から上に交わったときwn+1
	//上から下に交わったときwn-1
	int wn = 0;
	double vt;
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		// 上向きの辺、下向きの辺によって処理が分かれる。
	// 上向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、終点は含まない。(ルール1)
		if (itr->y0 <= y && itr->y1 > y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->y0) / (itr->y1 - itr->y0);
			if (x < itr->x0 + vt * (itr->x1 - itr->x0)) {
				++wn;  //ここが重要。上向きの辺と交差した場合は+1
			}
		}
		// 下向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、始点は含まない。(ルール2)
		else if (itr->y0 > y && itr->y1 <= y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->y0) / (itr->y1 - itr->y0);
			if (x < itr->x0 + vt * (itr->x1 - itr->x0)) {
				--wn;  //ここが重要。下向きの辺と交差した場合は-1
			}
		}
	}
	if (wn >= 1)return true;
	return false;
}
