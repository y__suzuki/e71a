#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <set>
#include<vector>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>

class Track_file {
public:
	int eventid, trackid, pl, rawid;
};
class Group_file :public Track_file {
public:
	int link_num;
	std::vector<std::tuple<int, int, int, int>> linklet;
};
struct Segment {
	int16_t pos;
	int32_t rawid;
	bool operator==(const Segment& rhs) const
	{
		return pos == rhs.pos && rawid == rhs.rawid;
	}
	bool operator<(const Segment& rhs) const {
		if (pos == rhs.pos) {
			return rawid < rhs.rawid;
		}
		return pos < rhs.pos;
	}
};
size_t hash_value(const Segment& d)
{
	// 複数の値のハッシュ値を組み合わせてハッシュ値を計算するには、
	// boost::hash_combine を使います。
	size_t h = 0;
	boost::hash_combine(h, d.pos);
	boost::hash_combine(h, d.rawid);
	return h;
}

Group_file group_file_covert(std::vector< mfile1::MFileChain> &group, std::vector< std::vector< mfile1::MFileBase>> &all_basetracks);
std::vector<Group_file> mfile_to_group(std::string file_in_mfile);
void output_group(std::string filename, std::vector<Group_file>&g);
void output_group_bin(std::string filename, std::vector<Group_file>&g);
bool judge_shower(Group_file &g, bool flg_multi);

bool sort_base(const mfile1::MFileBase& left, mfile1::MFileBase&right) {
	return left.pos < right.pos;
}
void main(int argc, char **argv)
{
	if (argc != 4) {
		fprintf(stderr, "usage : prg_name [input m-file-bin] [output m-file-bin]\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_group = argv[2];
	std::string file_out_group_shower = argv[3];

	//mfile_read_write_bin(file_in_mfile, file_out_mfile);
	std::vector<Group_file> group=mfile_to_group( file_in_mfile);
	//output_group(file_out_group, group);
	std::vector<Group_file> group_sel, group_shower;
	bool flg_multi = false;
	for (auto &g : group) {
		if (judge_shower(g, flg_multi)) {
			//shower like pick up
			group_shower.push_back(g);
			//printf("group%5d base %d link %d ratio %.3lf\n", merge.eventid, num_base, num_link, num_base*1. / num_link);
			//merge = make_group_next_one(track[i], link_next, link_prev);
		}
		else {
			group_sel.push_back(g);
		}
	}

	output_group_bin(file_out_group, group_sel);
	if (group_shower.size() > 0) {
		output_group_bin(file_out_group_shower, group_shower);
	}
}
std::vector<Group_file> mfile_to_group(std::string file_in_mfile) {
	std::vector<Group_file> ret;
	std::ifstream ifs(file_in_mfile, std::ios::binary);


	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }
	//Mfile headerの書き込み
	//mfile info headerの書き込み

	std::vector< mfile1::MFileChain> group;
	std::vector< std::vector< mfile1::MFileBase>> all_basetracks;
	mfile1::MFileChain w_chain;
	std::vector< mfile1::MFileBase> w_base;
	uint64_t count = 0, r_base_num = 0, r_chain_num = 0, w_base_num = 0, w_chain_num = 0, r_group_num = 0;
	int64_t gid = -1;
	for (int64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		//一旦chainを読む
		mfile1::MFileChain chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }
		r_chain_num++;

		std::vector< mfile1::MFileBase> basetracks;
		basetracks.reserve(chain.nseg);

		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			basetracks.emplace_back(base);
			r_base_num++;

		}

		//読んだchainが前のchainと同じgroupか確認
		if (c + 1 == mfile.info_header.Nchain) {
			if (gid != basetracks.begin()->group_id) {
				////////////////////////////
				//今までのgroupの書き出し
				r_group_num++;
				gid = basetracks.begin()->group_id;

				ret.push_back(group_file_covert(group, all_basetracks));

				w_chain_num++;

				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();
				////////////////////////////
				//最後のgroupの書き出し
				group.push_back(chain);
				all_basetracks.push_back(basetracks);

				r_group_num++;
				gid = basetracks.begin()->group_id;
				ret.push_back(group_file_covert(group, all_basetracks));

				w_chain_num++;
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();
			}
			else {
				//今のchainをpush back
				all_basetracks.emplace_back(basetracks);
				group.emplace_back(chain);
				r_group_num++;
				gid = basetracks.begin()->group_id;

				ret.push_back(group_file_covert(group, all_basetracks));

				//書き出し
				w_chain_num++;
				group.clear();
				for (int64_t i = 0; i < all_basetracks.size(); i++) {
					all_basetracks[i].clear();
				}
				all_basetracks.clear();

			}
		}
		else if (group.size() != 0 && gid != basetracks.begin()->group_id) {
			r_group_num++;
			gid = basetracks.begin()->group_id;
			ret.push_back(group_file_covert(group, all_basetracks));
			//書き出し
			w_chain_num++;
			group.clear();
			for (int64_t i = 0; i < all_basetracks.size(); i++) {
				all_basetracks[i].clear();
			}
			all_basetracks.clear();
		}
		else if (c == 0) {
			gid = basetracks.begin()->group_id;
		}
		all_basetracks.emplace_back(basetracks);
		group.emplace_back(chain);

	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;

	if (r_chain_num != mfile.info_header.Nchain) { throw std::exception("Nchain is wrong."); }
	if (r_base_num != mfile.info_header.Nbasetrack) { throw std::exception("Nbasetrack is wrong."); }
	mfile.info_header.Nchain = w_chain_num;
	mfile.info_header.Nbasetrack = w_base_num;

	printf("mfile info\n");
	printf("group num =%lld\n", r_group_num);
	//printf("chain num =%lld --> %lld\n", r_chain_num, w_chain_num);
	//printf("base  num =%lld --> %lld\n", r_base_num, w_base_num);

	return ret;
}
Group_file group_file_covert(std::vector< mfile1::MFileChain> &group, std::vector< std::vector< mfile1::MFileBase>> &all_basetracks) {
	if (group.size() == 0 || all_basetracks.size() == 0 || all_basetracks[0].size() == 0) {
		fprintf(stderr, "group exception\n");
	}
	Group_file ret;
	ret.eventid = all_basetracks[0][0].group_id;
	//以下は不要(初期値を入れておく)
	ret.trackid = 0;
	ret.pl = all_basetracks[0][0].pos / 10;
	ret.rawid = all_basetracks[0][0].rawid;
	
	std::set<std::tuple<int, int, int, int>> link_set;
	for (auto &c : all_basetracks) {
		for (int i = 0; i < c.size() - 1; i++) {
			link_set.insert(std::make_tuple(
				(c[i].pos / 10) * 10 + 1,
				(c[i + 1].pos / 10) * 10 + 1,
				c[i].rawid,
				c[i + 1].rawid));
		}
	}

	std::vector<std::tuple<int, int, int, int>> link_vec(link_set.begin(), link_set.end());
	ret.linklet = link_vec;
	ret.link_num = ret.linklet.size();

	bool judge_shower(Group_file &g, bool flg_multi);

	return ret;
}

void output_group(std::string filename, std::vector<Group_file>&g) {
	std::ofstream ofs(filename, std::ios::binary);
	int count = 0;
	for (auto itr = g.begin(); itr != g.end(); itr++) {
		if (count % 1000 == 0) {
			fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)", count, g.size(), count*100. / g.size());
		}
		count++;
		ofs << std::fixed << std::right
			<< std::setw(10) << std::setprecision(0) << itr->eventid << " "
			<< std::setw(5) << std::setprecision(0) << itr->trackid << " "
			<< std::setw(4) << std::setprecision(0) << itr->pl << " "
			<< std::setw(4) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(10) << std::setprecision(0) << itr->link_num << std::endl;
		if (itr->linklet.size() == 0)continue;
		for (auto itr2 = itr->linklet.begin(); itr2 != itr->linklet.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(5) << std::setprecision(0) << std::get<0>(*itr2) << " "
				<< std::setw(5) << std::setprecision(0) << std::get<1>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<2>(*itr2) << " "
				<< std::setw(10) << std::setprecision(0) << std::get<3>(*itr2) << std::endl;
		}
	}
	fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)\n", count, g.size(), count*100. / g.size());

}

void output_group_bin(std::string filename, std::vector<Group_file>&g) {
	std::ofstream ofs(filename, std::ios::binary);
	int count = 0;
	for (auto itr = g.begin(); itr != g.end(); itr++) {
		if (count % 1000 == 0) {
			fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)", count, g.size(), count*100. / g.size());
		}
		count++;
		ofs.write((char*)& itr->eventid, sizeof(int));
		ofs.write((char*)& itr->trackid, sizeof(int));
		ofs.write((char*)& itr->pl, sizeof(int));
		ofs.write((char*)& itr->rawid, sizeof(int));
		ofs.write((char*)& itr->link_num, sizeof(int));
		if (itr->linklet.size() == 0)continue;
		for (auto itr2 = itr->linklet.begin(); itr2 != itr->linklet.end(); itr2++) {
			ofs.write((char*)& std::get<0>(*itr2), sizeof(int));
			ofs.write((char*)& std::get<1>(*itr2), sizeof(int));
			ofs.write((char*)& std::get<2>(*itr2), sizeof(int));
			ofs.write((char*)& std::get<3>(*itr2), sizeof(int));
		}
	}
	fprintf(stderr, "\r wrtie file %d/%d(%4.1lf%%)\n", count, g.size(), count*100. / g.size());

}



//必要があればshowerの判定
int64_t Count_path(Group_file &g) {
	boost::unordered_multimap<Segment, Segment> link_next;
	boost::unordered_multimap<Segment, Segment> link_prev;
	Segment seg0, seg1;
	std::map<Segment, int64_t*> seg_count;
	std::multimap<int, std::pair<Segment, int64_t>>seg_map;
	for (auto itr = g.linklet.begin(); itr != g.linklet.end(); itr++) {
		seg0.pos = std::get<0>(*itr);
		seg1.pos = std::get<1>(*itr);
		seg0.rawid = std::get<2>(*itr);
		seg1.rawid = std::get<3>(*itr);
		link_next.insert(std::make_pair(seg0, seg1));
		link_prev.insert(std::make_pair(seg1, seg0));
		seg_map.insert(std::make_pair(seg0.pos, std::make_pair(seg0, 0)));
		seg_map.insert(std::make_pair(seg1.pos, std::make_pair(seg1, 0)));
		//seg_map.insert(std::make_pair(seg1, 0));
	}
	for (auto itr = seg_map.begin(); itr != seg_map.end(); itr++) {
		if (link_prev.count(itr->second.first) == 0) {
			itr->second.second = 1;
		}
		seg_count.insert(std::make_pair(itr->second.first, &(itr->second.second)));
	}
	for (auto itr = seg_map.begin(); itr != seg_map.end(); itr++) {
		if (link_prev.count(itr->second.first) == 0) continue;
		auto range = link_prev.equal_range(itr->second.first);
		for (auto res = range.first; res != range.second; res++) {
			itr->second.second += *(seg_count.at(res->second));
		}
	}

	int64_t all_path_num = 0;
	for (auto itr = seg_map.begin(); itr != seg_map.end(); itr++) {
		if (link_next.count(itr->second.first) == 0) {
			if (INT64_MAX - all_path_num > itr->second.second) {
				all_path_num += itr->second.second;
			}
			else {
				all_path_num = -1;
				break;
			}
		}
	}
	return all_path_num;
}
bool Judge_bipartite_graph(Segment target, boost::unordered_multimap <Segment, Segment> &path_prev, boost::unordered_multimap <Segment, Segment> &path_next, std::set<Segment>&up, std::set<Segment>&down) {

	up.insert(target);
	int sum = 0, sum_p = -1;
	while (sum != sum_p) {
		sum_p = sum;
		for (auto itr = up.begin(); itr != up.end(); itr++) {
			if (path_next.count(*itr) == 0)continue;
			auto range = path_next.equal_range(*itr);
			for (auto res = range.first; res != range.second; res++) {
				down.insert(res->second);
			}
		}
		for (auto itr = down.begin(); itr != down.end(); itr++) {
			if (path_prev.count(*itr) == 0)continue;
			auto range = path_prev.equal_range(*itr);
			for (auto res = range.first; res != range.second; res++) {
				up.insert(res->second);
			}
		}
		sum = up.size() + down.size();
	}

	//2部グラフの判定
	bool flg = true;
	for (auto itr = up.begin(); itr != up.end(); itr++) {
		if (down.count(*itr) == 1)flg = false;
	}
	for (auto itr = down.begin(); itr != down.end(); itr++) {
		if (up.count(*itr) == 1)flg = false;
	}

	return flg;
}
double max_link_base_ratio(Group_file &g, int&max_base, int &max_link) {
	std::vector < std::pair<std::set<Segment>, std::set<Segment>>>bipartite_graph_v;

	boost::unordered_multimap<Segment, Segment> link_next;
	boost::unordered_multimap<Segment, Segment> link_prev;
	Segment seg0, seg1;
	std::set<Segment> seg_set;
	for (auto itr = g.linklet.begin(); itr != g.linklet.end(); itr++) {
		seg0.pos = std::get<0>(*itr);
		seg1.pos = std::get<1>(*itr);
		seg0.rawid = std::get<2>(*itr);
		seg1.rawid = std::get<3>(*itr);
		//隣接のみ抜き出し
		if ((seg1.pos - seg0.pos) / 10 != 1)continue;
		link_next.insert(std::make_pair(seg0, seg1));
		link_prev.insert(std::make_pair(seg1, seg0));
		//seg_map.insert(std::make_pair(seg1, 0));
		seg_set.insert(seg0);
		seg_set.insert(seg1);
	}

	std::set<Segment> finished;
	for (auto itr = seg_set.begin(); itr != seg_set.end(); itr++) {
		auto target = *itr;
		if (finished.count(target) == 1)continue;
		std::set<Segment>up, down;
		//2部グラフだった場合
		if (Judge_bipartite_graph(target, link_prev, link_next, up, down)) {
			//下端の場合
			if (down.size() == 0) {
				finished.insert(target);
				continue;
			}
			std::pair<std::set<Segment>, std::set<Segment>> bipartite_graph;
			bipartite_graph.first = up;
			bipartite_graph.second = down;

			for (auto itr = up.begin(); itr != up.end(); itr++) {
				finished.insert(*itr);
			}
			bipartite_graph_v.push_back(bipartite_graph);
		}
	}
	double max_ratio = 0;
	//printf("bg size=%d\n", bipartite_graph_v.size());
	for (auto &bg : bipartite_graph_v) {
		int all_link = 0;
		int all_base = 0;
		double ratio;
		for (auto itr = bg.first.begin(); itr != bg.first.end(); itr++) {
			all_link += link_next.count(*itr);
		}
		for (auto itr = bg.second.begin(); itr != bg.second.end(); itr++) {
			all_link += link_prev.count(*itr);
		}
		all_base = bg.first.size() + bg.second.size();

		ratio = all_link * 1. / all_base;
		if (ratio > max_ratio) {
			max_base = all_base;
			max_link = all_link;
			max_ratio = ratio;
		}
	}
	return max_ratio;

}
bool judge_shower(Group_file &g, bool flg_multi) {

	std::set<std::pair<int, int>> basetrack;
	for (auto itr = g.linklet.begin(); itr != g.linklet.end(); itr++) {
		basetrack.insert(std::make_pair(std::get<0>(*itr), std::get<2>(*itr)));
		basetrack.insert(std::make_pair(std::get<1>(*itr), std::get<3>(*itr)));
	}
	int num_base = basetrack.size(), num_link = g.linklet.size();

	std::map<int, int> base_num;
	for (auto itr = basetrack.begin(); itr != basetrack.end(); itr++) {
		if (base_num.count(itr->first) == 0) {
			base_num.insert(std::make_pair(itr->first, 1));
		}
		else {
			base_num.at(itr->first) += 1;
		}
	}
	int max_base = 0;
	for (auto itr = base_num.begin(); itr != base_num.end(); itr++) {
		max_base = std::max(itr->second, max_base);
	}
	double rms, ratio;
	if (max_base == 0) {
		ratio = 1;
		rms = 0;
	}
	else {
		double sum = 0, sum2 = 0, count = 0;
		for (auto itr = base_num.begin(); itr != base_num.end(); itr++) {
			sum += itr->second;
			sum2 += itr->second*itr->second;
			count++;
		}
		if (count == 0) {
		}
		else {
			//1basetrackから平均何本linkletが出るか
			//1本のchainでは2に漸近
			ratio = num_link * 2. / num_base;
			if (sum2 / count - pow(sum / count, 2) < 0)rms = 0;
			else rms = sqrt(sum2 / count - pow(sum / count, 2));
		}
	}

	int max_ratio_base = 0, max_ratio_link = 0;
	int64_t all_path_num = Count_path(g);
	double max_ratio = max_link_base_ratio(g, max_ratio_base, max_ratio_link);


	bool ret = false;
	//multi消しなし
	if (flg_multi) {
		ret = (all_path_num > 1e8 || all_path_num < 0 || max_ratio>5);
	}
	//multi消しあり
	else {
		ret = (all_path_num > 1e8 || all_path_num < 0 || max_ratio>5);
	}

	printf("%5d %5d %2d %5d %6d %4.3lf %5.2lf %4d %4d %4d %7.3lf %lld\n"
		, g.eventid, g.trackid, ret, num_base, num_link, ratio, rms, max_base, max_ratio_base, max_ratio_link, max_ratio, all_path_num);

	return ret;

}
