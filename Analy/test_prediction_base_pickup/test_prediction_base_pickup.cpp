#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class Prediction {
public:
	int pl, rawid, hit;
	double ax, ay, x, y, z;
};

void chain_nseg_selection(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, const int nseg);
void reject_Fe_ECC(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel);
void chain_dlat_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double threshold);
void chain_angle_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double thr_ax, double thr_ay);

std::vector<Prediction> make_prediction(mfile1::MFile_minimum &m, std::vector<uint64_t> &index, int pl, std::map<int, double> &zmap);
void prediction_base_output(std::string filename, std::vector<Prediction>&pre, int pl);
void prediction_output(std::string filename, std::vector<Prediction>&pre);
void Prediction_base_inf(std::vector<Prediction>&pred, std::string ECC_path, int pl);


int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage prg mfile pl ECC-path output-base output-prediction\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_ECC = argv[3];
	std::string file_out_bvxx = argv[4];
	std::string file_out_prediction = argv[5];

	mfile1::MFile_minimum m;
	mfile1::read_mfile(file_in_mfile, m, 15);
	//zmap
	std::map<int, double> zmap;
	for (auto c : m.all_basetracks) {
		for (auto b : c) {
			zmap.insert(std::make_pair(b.pos / 10, b.z));
		}
	}
	std::vector<uint64_t> all, sel;
	for (int i = 0; i < m.chains.size(); i++) {
		all.push_back(i);
	}

	reject_Fe_ECC(m, all, sel);
	all = sel;
	sel.clear();

	chain_angle_selection(m, all, sel, 4.0, 4.0);
	all = sel;
	sel.clear();

	chain_dlat_selection(m, all, sel, 0.02);

	std::vector<Prediction> prediction = make_prediction(m, sel, pl, zmap);
	Prediction_base_inf(prediction, file_in_ECC, pl);


	prediction_base_output(file_out_bvxx, prediction, pl);
	prediction_output(file_out_prediction, prediction);


}
void chain_nseg_selection(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, const int nseg) {
	for (auto i : all) {
		if (m.chains[i].nseg < nseg)continue;
		sel.push_back(i);
	}
	printf("chain nseg >= %lld: %lld --> %lld (%4.1lf%%)\n", nseg, all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}
void reject_Fe_ECC(const mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel) {
	for (auto i : all) {
		if (m.chains[i].pos0 / 10 > 15) {
			sel.push_back(i);
		}
		else {
			int flg = 0;
			for (auto b : m.all_basetracks[i]) {
				if (b.pos / 10 == 16)flg++;
				if (b.pos / 10 == 17)flg++;
				if (b.pos / 10 == 18)flg++;
				if (b.pos / 10 == 19)flg++;
				if (b.pos / 10 == 20)flg++;
				if (b.pos / 10 == 21)flg++;
				if (b.pos / 10 == 22)flg++;
				if (b.pos / 10 == 23)flg++;
			}
			if (flg >= 6 || m.chains[i].pos1 / 10 > 23) {
				sel.push_back(i);
			}
		}
	}
	printf("reject_Fe_ECC(PL1 >= 24): %lld --> %lld (%4.1lf%%)\n", all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}
void chain_dlat_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double threshold) {
	double ax, ay, div_rad, div_lat;
	std::ofstream ofs("out.txt");

	for (auto i : all) {
		ax = mfile1::chain_ax(m.all_basetracks[i]);
		ay = mfile1::chain_ay(m.all_basetracks[i]);
		mfile1::angle_diff_dev_theta(m.all_basetracks[i], ax, ay, div_rad, div_lat);
		ofs << std::right << std::fixed
			<< std::setw(5) << std::setprecision(0) << m.all_basetracks[i].size()
			<< std::setw(10) << std::setprecision(6) << div_rad
			<< std::setw(10) << std::setprecision(6) << div_lat << std::endl;
		if (mfile1::angle_diff_dev_lat(m.all_basetracks[i]) > threshold)continue;
		sel.push_back(i);
	}
	printf("chain lateral selection <= %5.4lf : %lld --> %lld (%4.1lf%%)\n", threshold, all.size(), sel.size(), sel.size()*100. / all.size());
	return;

}
void chain_angle_selection(mfile1::MFile_minimum &m, std::vector<uint64_t> &all, std::vector<uint64_t> &sel, double thr_ax, double thr_ay) {
	double ax, ay;
	for (auto i : all) {
		ax = mfile1::chain_ax(m.all_basetracks[i]);
		ay = mfile1::chain_ay(m.all_basetracks[i]);
		if (thr_ax - fabs(ax) < 0)continue;
		if (thr_ay - fabs(ay) < 0)continue;
		sel.push_back(i);

	}
	printf("chain average angle selection |ax|<=%3.1lf |ay|<=%3.1lf : %lld --> %lld (%4.1lf%%)\n", thr_ax, thr_ay, all.size(), sel.size(), sel.size()*100. / all.size());
	return;
}

std::vector<Prediction> make_prediction(mfile1::MFile_minimum &m, std::vector<uint64_t> &index,int pl, std::map<int, double> &zmap){
	std::vector<Prediction> ret;
	int hit_num = 0;
	double tmp_x, tmp_y,tmp_z;
	int rawid = 0;
	double pre_z = zmap.at(pl);
	for (auto i : index) {
		hit_num = 0;
		for (auto b : m.all_basetracks[i]) {
			if (abs(b.pos / 10 - pl)<= 2)hit_num++;

			if (abs(b.pos / 10 - pl) == 0) {
				tmp_x = b.x;
				tmp_y = b.y;
				tmp_z = b.z;
				rawid = b.rawid;
			}
		}
		if (hit_num == 5) {
			Prediction pre;
			pre.ax = mfile1::chain_ax(m.all_basetracks[i]);
			pre.ay = mfile1::chain_ay(m.all_basetracks[i]);
			pre.pl = pl;
			pre.rawid = rawid;
			pre.hit = 0;
			pre.x = tmp_x + pre.ax*(pre_z - tmp_z);
			pre.y = tmp_y + pre.ay*(pre_z - tmp_z);
			pre.z = tmp_z;

			ret.push_back(pre);
		}
	}
	printf("all chain %d selected(PL%03d) %d (%4.1lf%%)\n", index.size(),pl, ret.size(), ret.size()*100. / index.size());
	return ret;
}

void Prediction_base_inf(std::vector<Prediction>&pred, std::string ECC_path, int pl) {
	std::stringstream file_in_base;
	file_in_base<< ECC_path << "\\Area0\\PL" 
		<< std::setw(3) << std::setfill('0') << pl << "\\b"
		<< std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), pl, 0);
	std::map<int, vxx::base_track_t> base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		base_map.insert(std::make_pair(itr->rawid, *itr));
	}


	for (auto itr = pred.begin(); itr != pred.end(); itr++) {
		auto res = base_map.find(itr->rawid);
		itr->ax = res->second.ax;
		itr->ay = res->second.ay;
		itr->x = res->second.x;
		itr->y = res->second.y;
		itr->z = 0;
		itr->hit = 1;
		itr->pl = pl;
	}
}




void prediction_output(std::string filename, std::vector<Prediction>&pre) {
	std::ofstream ofs(filename);
	for (auto itr = pre.begin(); itr != pre.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(3) << std::setprecision(0) << itr->pl << " "
			<< std::setw(12) << std::setprecision(0) << itr->rawid << " "
			<< std::setw(8) << std::setprecision(4) << itr->ax << " "
			<< std::setw(8) << std::setprecision(4) << itr->ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(10) << std::setprecision(1) << itr->z << " "
			<< std::setw(3) << std::setprecision(0) << itr->hit << std::endl;
	}
}void prediction_base_output(std::string filename, std::vector<Prediction>&pre,int pl) {
	std::vector<vxx::base_track_t> base;
	int count = 0;
	for (auto p : pre) {
		vxx::base_track_t b;
		b.pl = pl;
		b.rawid = p.rawid;
		b.ax = p.ax;
		b.ay = p.ay;
		b.x = p.x;
		b.y = p.y;
		b.z = 0;
		b.zone = 0;
		for (int i = 0; i < 2; i++) {
			b.m[i].ax = p.ax;
			b.m[i].ay = p.ay;
			b.m[i].col = 0;
			b.m[i].row = 0;
			b.m[i].isg = count;
			b.m[i].rawid = count;
			b.m[i].ph = 990999;
			b.m[i].pos = pl * 10 + i + 1;
			b.m[i].zone = 0;
			b.m[i].z = i * 210;
		}
		base.push_back(b);
		count++;
	}
	vxx::BvxxWriter bw;
	bw.Write(filename, pl, 0, base);
}