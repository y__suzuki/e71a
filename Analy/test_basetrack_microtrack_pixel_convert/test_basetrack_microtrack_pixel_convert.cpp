#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>
#include <omp.h>

struct output_tracks {
	vxx::base_track_t b;
	std::vector<vxx::micro_track_t> m;
	
	
};
void output(std::string filename, std::vector<output_tracks> &out);
std::vector<vxx::micro_track_t> read_fvxx(std::string filename, int pos, int zone);
void read_bvxx(std::string filename, std::vector<vxx::base_track_t> &base, int pl, int zone);

int main(int argc, char**argv) {
	if (6 > argc || argc % 3 != 0) {
		fprintf(stderr, "usage:prg bvxx pl fvxx pos zone fvxx pos zone ...\n");
		exit(1);
	}
	std::string file_in_bvxx = argv[1];
	int pl = std::stoi(argv[2]);
	int fvxx_num = (argc - 2) / 3;

	std::vector<std::string> file_in_fvxx;
	std::vector<int> pos;
	std::vector<int> zone;
	for (int i = 0; i < fvxx_num; i++) {
		file_in_fvxx.push_back(argv[i * 3 + 3]);
		pos.push_back(std::stoi(argv[i * 3 + 4]));
		zone.push_back(std::stoi(argv[i * 3 + 5]));
	}
	std::vector<vxx::base_track_t> base;
	read_bvxx(file_in_bvxx, base, pl, 0);
	std::vector<output_tracks> out;
	int cnt = 0;
	double angle;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		angle = sqrt(itr->ax*itr->ax + itr->ay*itr->ay);
		if (cnt > 100)continue;
		if (1.0<angle&&angle<1.5) {
			output_tracks out_tmp;
			out_tmp.b = *itr;
			out.push_back(out_tmp);
			cnt++;
		}
	}
	printf("cnt=%d out size = %d\n", cnt, out.size());
	for (int i = 0; i < fvxx_num; i++) {
		std::vector<vxx::micro_track_t> micro = read_fvxx(file_in_fvxx[i], pos[i], zone[i]);
		std::map<int, vxx::micro_track_t*> micro_map;
		for (auto itr = micro.begin(); itr != micro.end(); itr++) {
			micro_map.insert(std::make_pair(itr->rawid, &(*itr)));
		}
		for (auto itr = out.begin(); itr != out.end(); itr++) {
			if (pos[i] == itr->b.m[0].pos) {
				itr->m.push_back(*(micro_map.find(itr->b.m[0].rawid)->second));
			}
			else if (pos[i] == itr->b.m[1].pos) {
				itr->m.push_back(*(micro_map.find(itr->b.m[1].rawid)->second));
			}
		}
	}
	output("out.txt", out);
	
}
void read_bvxx(std::string filename, std::vector<vxx::base_track_t> &base, int pl, int zone) {
	vxx::BvxxReader br;
	base = br.ReadAll(filename, pl, zone);
	return;
}

std::vector<vxx::micro_track_t> read_fvxx(std::string filename,  int pos, int zone) {
	std::vector<vxx::micro_track_t> micro;
	vxx::FvxxReader fr;
	micro = fr.ReadAll(filename, pos, zone);
	return micro;
}
void output(std::string filename, std::vector<output_tracks> &out) {
	std::ofstream ofs(filename);
	for (auto itr = out.begin(); itr != out.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr->b.rawid << " "
			<< std::setw(7) << std::setprecision(4) << itr->b.ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->b.ay << " "
			<< std::setw(10) << std::setprecision(1) << itr->b.x << " "
			<< std::setw(10) << std::setprecision(1) << itr->b.y << " "
			<< std::setw(10) << std::setprecision(1) << itr->b.z << " ";
		for (auto itr2 = itr->m.begin(); itr2 != itr->m.end(); itr2++) {
			ofs << std::setw(12) << std::setprecision(0) << itr2->rawid << " "
				<< std::setw(7) << std::setprecision(0) << itr2->ph << " "
				<< std::setw(7) << std::setprecision(4) << itr2->ax << " "
				<< std::setw(7) << std::setprecision(4) << itr2->ay << " "
				<< std::setw(10) << std::setprecision(1) << itr2->x << " "
				<< std::setw(10) << std::setprecision(1) << itr2->y << " "
				<< std::setw(10) << std::setprecision(1) << itr2->z << std::endl;
		}
	}
}