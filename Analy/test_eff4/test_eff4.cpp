#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
#include <set>

std::vector<std::pair<vxx::base_track_t, int>> Hit_check(std::vector<vxx::base_track_t> &base, std::vector<netscan::linklet_t>&link);
void Calc_multi_late(std::vector<std::pair<vxx::base_track_t, int>>&pair, double ang_min, double ang_max);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-base pl in-link output-path\n");
		exit(1);
	}
	std::string file_in_base = argv[1];
	int pl = std::stoi(argv[2]);
	std::string file_in_link = argv[3];
	std::string file_out_path = argv[4];

	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base, pl, 0);
	printf("prediction %d\n", base.size());
	std::vector<netscan::linklet_t>link;
	netscan::read_linklet_bin(file_in_link, link);
	std::vector<std::pair<vxx::base_track_t, int>> pair = Hit_check(base, link);

	Calc_multi_late(pair, 0, 0.1);
	Calc_multi_late(pair, 0.1, 0.2);
	Calc_multi_late(pair, 0.2, 0.3);
	Calc_multi_late(pair, 0.3, 0.4);
	Calc_multi_late(pair, 0.4, 0.5);
	Calc_multi_late(pair, 0.5, 0.6);
	Calc_multi_late(pair, 0.6, 0.7);
	Calc_multi_late(pair, 0.7, 0.8);
	Calc_multi_late(pair, 0.8, 0.9);
	Calc_multi_late(pair, 0.9, 1.0);
	Calc_multi_late(pair, 1.0, 1.2);
	Calc_multi_late(pair, 1.2, 1.4);
	Calc_multi_late(pair, 1.4, 1.6);
	Calc_multi_late(pair, 1.6, 1.8);
	Calc_multi_late(pair, 1.8, 2.0);
	Calc_multi_late(pair, 2.0, 2.2);
	Calc_multi_late(pair, 2.2, 2.4);
	Calc_multi_late(pair, 2.4, 2.6);
	Calc_multi_late(pair, 2.6, 2.8);
	Calc_multi_late(pair, 2.8, 3.0);
	Calc_multi_late(pair, 3.0, 3.2);
	Calc_multi_late(pair, 3.2, 3.4);
	Calc_multi_late(pair, 3.4, 3.6);
	Calc_multi_late(pair, 3.6, 3.8);
	Calc_multi_late(pair, 3.8, 4.0);

}
std::vector<std::pair<vxx::base_track_t, int>> Hit_check(std::vector<vxx::base_track_t> &base, std::vector<netscan::linklet_t>&link) {


	std::multimap<int, netscan::linklet_t> link_map;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		link_map.insert(std::make_pair(itr->b[0].rawid, *itr));
	}


	std::vector<std::pair<vxx::base_track_t, int>> ret;
	std::map<int, int> multi_count;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::pair<vxx::base_track_t, int> p;
		p.first = *itr;
		p.second = link_map.count(itr->rawid);
		auto res = multi_count.insert(std::make_pair(p.second, 1));
		if (!res.second)res.first->second++;
		ret.push_back(p);
	}

	for (auto itr = multi_count.begin(); itr != multi_count.end(); itr++) {
		printf("%d %d\n", itr->first, itr->second);


	}
	return ret;


}
void Calc_multi_late(std::vector<std::pair<vxx::base_track_t, int>>&pair,double ang_min,double ang_max) {
	int all=0, num0=0, num1=0, num2=0;
	double angle;
	for (auto itr = pair.begin(); itr != pair.end(); itr++) {
		angle = sqrt(itr->first.ax*itr->first.ax + itr->first.ay*itr->first.ay);
		if (angle < ang_min)continue;
		if (angle >=ang_max)continue;
		all++;
		if (itr->second == 0)num0++;
		else if (itr->second == 1)num1++;
		else num2++;

	}

	double p0, p1, p2;
	p0 = num0 * 1. / all;
	p1 = num1 * 1. / all;
	p2 = num2 * 1. / all;

	double p = ((p2 - p0 + 1) + sqrt(pow(p2 - p0 + 1, 2) - 4 * p2)) / 2;
	double e = ((p2 - p0 + 1) - sqrt(pow(p2 - p0 + 1, 2) - 4 * p2)) / 2;

	printf("%.2lf %.2lf %.5lf %.5lf %d %d %d\n", ang_min, ang_max, p, e, num0, num1, num2);

}