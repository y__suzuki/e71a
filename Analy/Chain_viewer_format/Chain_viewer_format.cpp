//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS
#define DEFAULT_CHAIN_UPPERLIM 1000000000000000

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <stack>
#include <unordered_set>

using namespace l2c;

class output_format {
public:
	int gid;
	int num_comfirmed_path, num_cut_path, num_select_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> comfirmed_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> cut_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> select_path;
};
std::vector< output_format> read_chain_file(std::string filename, std::multimap<int, mfile0::M_Base>&base_map);
std::map<int, corrmap0::Corrmap> read_corrmap_abs(std::string file_in_ECC);
void read_basetrack(std::string file_in_ECC, std::multimap<int, mfile0::M_Base>&base_map);
void read_basetrack_id(std::string file_in_ECC, std::multimap<int, mfile0::M_Base>&base_map);
void apply_corrmap(std::multimap<int, mfile0::M_Base>&base_map, std::map<int, corrmap0::Corrmap>&corr_abs, std::map<int, double> &z_map);

std::vector<mfile0::M_Chain> make_chain(output_format &group, std::map<std::pair<int, int>, mfile0::M_Base>&base_map_id);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:file-in-chain file-in-ECC file-out-mfile\n");
		exit(1);
	}

	std::string file_in_chain = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_out = argv[3];

	std::multimap<int, mfile0::M_Base>base_map;
	std::vector< output_format> group = read_chain_file(file_in_chain, base_map);

	std::map<int, corrmap0::Corrmap> corrmap = read_corrmap_abs(file_in_ECC);
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\st\\st.dat";
	printf("%s\n", structure_path.str().c_str());
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	//basetrack情報のpick up
	read_basetrack(file_in_ECC, base_map);
	//read_basetrack_id(file_in_ECC, base_map);

	//corrmapの適用
	apply_corrmap(base_map, corrmap, z_map);
	std::map<std::pair<int, int>, mfile0::M_Base>base_map_id;
	for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
		base_map_id.insert(std::make_pair(std::make_pair(itr->first, itr->second.rawid), itr->second));
	}

	mfile0::Mfile m;
	for (int i = 0; i < group.size(); i++) {
		std::vector<mfile0::M_Chain>chains = make_chain(group[i], base_map_id);
		for (auto itr = chains.begin(); itr != chains.end(); itr++) {
			m.chains.push_back(*itr);
		}
	}
	mfile0::set_header(base_map.begin()->first / 10, base_map.rbegin()->first / 10, m);

	mfile0::write_mfile(file_out, m);
}
l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos, int64_t upperlim, bool output) {
		try
		{
			size_t possize = usepos.size();

			//l2c関数本体を呼び出す。
			//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
			//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
			//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
			//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
			//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
			if (output) {
				l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
				return cdat;
			}
			l2c::Cdat cdat = l2c::MakeCdat_no_out(ltlist, opt::usepos = usepos, opt::upperlim = upperlim, opt::output_isolated_linklet = true);
			return cdat;

		}
		catch (const std::exception& e)
		{
			fprintf(stderr, "%s\n", e.what());
		}

	}

std::vector< output_format> read_chain_file(std::string filename, std::multimap<int, mfile0::M_Base>&base_map) {
		std::vector< output_format> ret;
		std::map<int, std::map<int, mfile0::M_Base>>base_map_pl;
		std::ifstream ifs(filename);
		output_format out;
		std::tuple<int, int, int, int> path;
		int id;
		int pl, rawid;
		while (ifs >> out.gid >> out.num_comfirmed_path >> out.num_cut_path >> out.num_select_path) {
			for (int i = 0; i < out.num_comfirmed_path; i++) {
				ifs >> id >> std::get<0>(path) >> std::get<1>(path) >> std::get<2>(path) >> std::get<3>(path);
				out.comfirmed_path.push_back(std::make_pair(id, path));

				pl = std::get<0>(path) / 10;
				rawid = std::get<2>(path);
				auto res = base_map_pl.find(pl);
				mfile0::M_Base b;
				b.pos = pl * 10 + 1;
				b.rawid = rawid;
				if (res == base_map_pl.end()) {
					std::map<int, mfile0::M_Base> base_map_tmp;
					base_map_tmp.insert(std::make_pair(rawid, b));
					base_map_pl.insert(std::make_pair(pl, base_map_tmp));
				}
				else {
					res->second.insert(std::make_pair(rawid, b));
				}

				pl = std::get<1>(path) / 10;
				rawid = std::get<3>(path);
				res = base_map_pl.find(pl);
				b.pos = pl * 10 + 1;
				b.rawid = rawid;
				if (res == base_map_pl.end()) {
					std::map<int, mfile0::M_Base> base_map_tmp;
					base_map_tmp.insert(std::make_pair(rawid, b));
					base_map_pl.insert(std::make_pair(pl, base_map_tmp));
				}
				else {
					res->second.insert(std::make_pair(rawid, b));
				}

			}
			for (int i = 0; i < out.num_cut_path; i++) {
				ifs >> id >> std::get<0>(path) >> std::get<1>(path) >> std::get<2>(path) >> std::get<3>(path);
				out.cut_path.push_back(std::make_pair(id, path));

				pl = std::get<0>(path) / 10;
				rawid = std::get<2>(path);
				auto res = base_map_pl.find(pl);
				mfile0::M_Base b;
				b.pos = pl * 10 + 1;
				b.rawid = rawid;
				if (res == base_map_pl.end()) {
					std::map<int, mfile0::M_Base> base_map_tmp;
					base_map_tmp.insert(std::make_pair(rawid, b));
					base_map_pl.insert(std::make_pair(pl, base_map_tmp));
				}
				else {
					res->second.insert(std::make_pair(rawid, b));
				}

				pl = std::get<1>(path) / 10;
				rawid = std::get<3>(path);
				res = base_map_pl.find(pl);
				b.pos = pl * 10 + 1;
				b.rawid = rawid;
				if (res == base_map_pl.end()) {
					std::map<int, mfile0::M_Base> base_map_tmp;
					base_map_tmp.insert(std::make_pair(rawid, b));
					base_map_pl.insert(std::make_pair(pl, base_map_tmp));
				}
				else {
					res->second.insert(std::make_pair(rawid, b));
				}

			}
			for (int i = 0; i < out.num_select_path; i++) {
				ifs >> id >> std::get<0>(path) >> std::get<1>(path) >> std::get<2>(path) >> std::get<3>(path);
				out.select_path.push_back(std::make_pair(id, path));

				pl = std::get<0>(path) / 10;
				rawid = std::get<2>(path);
				auto res = base_map_pl.find(pl);
				mfile0::M_Base b;
				b.pos = pl * 10 + 1;
				b.rawid = rawid;
				if (res == base_map_pl.end()) {
					std::map<int, mfile0::M_Base> base_map_tmp;
					base_map_tmp.insert(std::make_pair(rawid, b));
					base_map_pl.insert(std::make_pair(pl, base_map_tmp));
				}
				else {
					res->second.insert(std::make_pair(rawid, b));
				}

				pl = std::get<1>(path) / 10;
				rawid = std::get<3>(path);
				res = base_map_pl.find(pl);
				b.pos = pl * 10 + 1;
				b.rawid = rawid;
				if (res == base_map_pl.end()) {
					std::map<int, mfile0::M_Base> base_map_tmp;
					base_map_tmp.insert(std::make_pair(rawid, b));
					base_map_pl.insert(std::make_pair(pl, base_map_tmp));
				}
				else {
					res->second.insert(std::make_pair(rawid, b));
				}

			}

			ret.push_back(out);
			out.comfirmed_path.clear();
			out.cut_path.clear();
			out.select_path.clear();
		}

		for (auto itr = base_map_pl.begin(); itr != base_map_pl.end(); itr++) {
			for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
				base_map.insert(std::make_pair(itr->first, itr2->second));
				//printf("%d %d\n", itr->first, itr2->first);
			}
		}
		return ret;
	}

std::map<int, corrmap0::Corrmap> read_corrmap_abs(std::string file_in_ECC) {
		std::stringstream file_in_corr;
		file_in_corr << file_in_ECC << "\\Area0\\0\\align\\corrmap-abs.lst";

		std::vector<corrmap0::Corrmap> corr;
		corrmap0::read_cormap(file_in_corr.str(), corr);

		std::map<int, corrmap0::Corrmap> ret;
		for (auto itr = corr.begin(); itr != corr.end(); itr++) {
			ret.insert(std::make_pair(itr->pos[0] / 10, *itr));
		}
		return ret;
	}

void read_basetrack(std::string file_in_ECC, std::multimap<int, mfile0::M_Base>&base_map) {
		std::vector<int> read_pl;
		for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
			int count = base_map.count(itr->first);
			read_pl.push_back(itr->first);
			itr = std::next(itr, count - 1);
		}

#pragma omp parallel for num_threads(10) schedule(dynamic,1)
		for (int i = 0; i < read_pl.size(); i++) {

			std::stringstream file_in_base;
			file_in_base << file_in_ECC << "\\Area0\\PL"
				<< std::setw(3) << std::setfill('0') << read_pl[i] << "\\b"
				<< std::setw(3) << std::setfill('0') << read_pl[i] << ".sel.cor.vxx";
			std::vector<vxx::base_track_t >base;
			vxx::BvxxReader br;
			base = br.ReadAll(file_in_base.str(), read_pl[i], 0);
			std::map<int, vxx::base_track_t> base_rawmap;
			for (auto itr = base.begin(); itr != base.end(); itr++) {
				base_rawmap.insert(std::make_pair(itr->rawid, *itr));
			}

			auto range = base_map.equal_range(read_pl[i]);
			for (auto res = range.first; res != range.second; res++) {
				auto b = base_rawmap.find(res->second.rawid);
				if (b == base_rawmap.end()) {
					fprintf(stderr, "exception PL%03d rawid=%d not found\n", read_pl[i], res->second.rawid);
				}
				res->second.ax = b->second.ax;
				res->second.ay = b->second.ay;
				res->second.x = b->second.x;
				res->second.y = b->second.y;
				res->second.ph = b->second.m[0].ph + b->second.m[1].ph;
				res->second.flg_d[0] = 0;
				res->second.flg_d[1] = 0;
				res->second.flg_i[0] = 0;
				res->second.flg_i[1] = 0;
				res->second.flg_i[2] = 0;
				res->second.flg_i[3] = 0;
				res->second.ax = b->second.ax;
			}
		}


	}

void read_basetrack_id(std::string file_in_ECC, std::multimap<int, mfile0::M_Base>&base_map) {


		std::vector< mfile0::M_Base*>base_p;
		std::vector<int> read_pl;
		for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
			base_p.push_back(&(itr->second));
		}

#pragma omp parallel for  num_threads(10) 
		for (int i = 0; i < base_p.size(); i++) {
			int pl = base_p[i]->pos / 10;
			int rawid = base_p[i]->rawid;
			std::stringstream file_in_base;
			file_in_base << file_in_ECC << "\\Area0\\PL"
				<< std::setw(3) << std::setfill('0') << pl << "\\b"
				<< std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
			std::vector<vxx::base_track_t >base;
			vxx::BvxxReader br;
			std::array<int, 2> index = { rawid,rawid + 1 };//1234<=rawid<=5678であるようなものだけを読む。
			base = br.ReadAll(file_in_base.str(), pl, 0, vxx::opt::index = index);

			base_p[i]->ax = base[0].ax;
			base_p[i]->ay = base[0].ay;
			base_p[i]->x = base[0].x;
			base_p[i]->y = base[0].y;
			base_p[i]->ph = base[0].m[0].ph + base[0].m[1].ph;
			base_p[i]->flg_d[0] = 0;
			base_p[i]->flg_d[1] = 0;
			base_p[i]->flg_i[0] = 0;
			base_p[i]->flg_i[1] = 0;
			base_p[i]->flg_i[2] = 0;
			base_p[i]->flg_i[3] = 0;
		}



	}

void apply_corrmap(std::multimap<int, mfile0::M_Base>&base_map, std::map<int, corrmap0::Corrmap>&corr_abs, std::map<int, double> &z_map) {


		double tmp_x, tmp_y;

		for (auto itr = base_map.begin(); itr != base_map.end(); itr++) {
			int count = base_map.count(itr->first);
			auto range = base_map.equal_range(itr->first);
			auto z = z_map.find(itr->first);
			auto param = corr_abs.find(itr->first);
			if (z == z_map.end()) {
				printf("exception PL%03d z not found\n", itr->first);
			}
			if (param == corr_abs.end()) {
				printf("exception PL%03d corr abs not found\n", itr->first);
			}

			for (auto res = range.first; res != range.second; res++) {
				tmp_x = res->second.x;
				tmp_y = res->second.y;
				res->second.x = param->second.position[0] * tmp_x + param->second.position[1] * tmp_y + param->second.position[4];
				res->second.y = param->second.position[2] * tmp_x + param->second.position[3] * tmp_y + param->second.position[5];
				tmp_x = res->second.ax;
				tmp_y = res->second.ay;
				res->second.ax = param->second.angle[0] * tmp_x + param->second.angle[1] * tmp_y + param->second.angle[4];
				res->second.ay = param->second.angle[2] * tmp_x + param->second.angle[3] * tmp_y + param->second.angle[5];

				res->second.z = z->second + param->second.dz;

			}
			itr = std::next(itr, count - 1);
		}
	}

std::vector<mfile0::M_Chain> make_chain(output_format &group, std::map<std::pair<int, int>, mfile0::M_Base>&base_map_id) {


	std::set<std::pair<int32_t, int64_t>> btset;
	std::vector<Linklet> ltlist;
	std::set<int32_t> pos;

	ltlist.reserve(group.comfirmed_path.size() + group.cut_path.size());
	for (auto itr = group.comfirmed_path.begin(); itr != group.comfirmed_path.end(); itr++) {
		ltlist.emplace_back(std::get<0>(itr->second), std::get<1>(itr->second), std::get<2>(itr->second), std::get<3>(itr->second));
		btset.insert(std::make_pair(std::get<0>(itr->second), std::get<2>(itr->second)));
		btset.insert(std::make_pair(std::get<1>(itr->second), std::get<3>(itr->second)));
		pos.insert(std::get<0>(itr->second));
		pos.insert(std::get<1>(itr->second));
	}
	for (auto itr = group.cut_path.begin(); itr != group.cut_path.end(); itr++) {
		ltlist.emplace_back(std::get<0>(itr->second), std::get<1>(itr->second), std::get<2>(itr->second), std::get<3>(itr->second));
		btset.insert(std::make_pair(std::get<0>(itr->second), std::get<2>(itr->second)));
		btset.insert(std::make_pair(std::get<1>(itr->second), std::get<3>(itr->second)));
		pos.insert(std::get<0>(itr->second));
		pos.insert(std::get<1>(itr->second));
	}

	std::vector<int32_t> usepos(pos.begin(),pos.end());
	auto cdat = l2c_x(btset, ltlist, usepos, DEFAULT_CHAIN_UPPERLIM, false);

	std::vector<mfile0::M_Chain> ret;
	size_t grsize = cdat.GetNumOfGroups();
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		size_t chsize = gr.GetNumOfChains();
		if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。

		if (gr.IsOverUpperLim())
		{
			//upperlimを超過している場合、chainの情報はない。
			//ただしchainの本数はGetNumOfChainsで正しく取得できる。
			//また属すBaseTrackの情報は保持しているので、GetBaseTracksで全BaseTrackを取得できる。
			fprintf(stdout, "over upperlim. nchain:%-9lld\n", chsize);
			continue;
		}
		for (size_t ich = 0; ich < chsize; ++ich)
		{
			mfile0::M_Chain chain;
			Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
			chain.chain_id = ch.GetID();
			chain.nseg = ch.GetNSeg();

			for (size_t pl = 0; pl < usepos.size(); ++pl)
			{
				//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
				//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
				BaseTrackID bt = ch.GetBaseTrack(pl);
				if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
				int32_t btpl = bt.GetPL();
				int64_t btid = bt.GetRawID();
				//btplとplは厳密に一致しなければおかしい。
				if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
				if (btset.find(std::make_pair(usepos[btpl], btid)) == btset.end())
				{
					//エラーチェック。
					//Chain内の全てのBaseTrackはbtset（ファイルから読み込まれた全BaseTrack）に必ず含まれているはず。
					//含まれていなければエラー。
					throw std::exception("BaseTrack is not found in btset.");
				}

				int b_pos = usepos[bt.GetPL()];
				int rawid = bt.GetRawID();
				auto res = base_map_id.find(std::make_pair(b_pos/10, rawid));
				if (res == base_map_id.end()) {
					printf("PL%03d rawid=%d not found\n", b_pos / 10,rawid);
					exit(1);
				}
				chain.basetracks.push_back(res->second);
			}
			chain.pos0 = chain.basetracks.begin()->pos;
			chain.pos1 = chain.basetracks.rbegin()->pos;
			for (auto itr = chain.basetracks.begin(); itr != chain.basetracks.end(); itr++) {
				itr->group_id = group.gid;
			}
			ret.push_back(chain);
		}
	}
	return ret;

}
