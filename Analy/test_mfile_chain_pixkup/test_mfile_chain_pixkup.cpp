#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

mfile0::Mfile Chain_selection(mfile0::Mfile &m, int chainid);
mfile0::Mfile Group_selection(mfile0::Mfile &m, int groupid);

int main(int argc,char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out_mfile = argv[2];
	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	//mfile0::Mfile sel = Chain_selection(m, 33606427);
	mfile0::Mfile sel = Group_selection(m, 24309163);

	mfile1::write_mfile_extension(file_out_mfile, sel);

}
mfile0::Mfile Chain_selection(mfile0::Mfile &m, int chainid) {
	mfile0::Mfile ret;
	ret.header = m.header;
	int64_t all, cnt = 0;
	all = m.chains.size();
	for (auto &c : m.chains) {
		if (cnt % 100000 == 0) {
			printf("\r now %d/%d(%4.1lf%%)", cnt, all, cnt*100. / all);
		}
		cnt++;
		if (c.chain_id != chainid)continue;
		ret.chains.push_back(c);
	}
	printf("\r now %d/%d(%4.1lf%%)\n", cnt, all, cnt*100. / all);

	return ret;
}
mfile0::Mfile Group_selection(mfile0::Mfile &m, int groupid) {
	mfile0::Mfile ret;
	ret.header = m.header;
	int64_t all, cnt = 0;
	all = m.chains.size();
	for (auto &c : m.chains) {
		if (cnt % 100000 == 0) {
			printf("\r now %d/%d(%4.1lf%%)", cnt, all, cnt*100. / all);
		}
		cnt++;
		if (c.basetracks[0].group_id != groupid)continue;
		ret.chains.push_back(c);
	}
	printf("\r now %d/%d(%4.1lf%%)\n", cnt, all, cnt*100. / all);

	return ret;
}