#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <map>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <stack>
#include <unordered_set>
class Track_file {
public:
	int eventid, trackid, pl, rawid;
};
class Group_file :public Track_file {
public:
	int link_num;
	std::vector<std::tuple<int, int, int, int>> linklet;
};


class output_format {
public:
	int groupid,trackid;
	int num_comfirmed_path, num_cut_path, num_select_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> comfirmed_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> cut_path;
	std::vector<std::pair<int, std::tuple<int, int, int, int>>> select_path;
};

std::vector <  Group_file > read_group_file(std::string filename);
std::vector< output_format> read_group_conv_file(std::string filename);
Group_file  linklet_merge(Group_file&g, output_format&g_conv);
void output_group(std::string filename, std::vector<Group_file>&g);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "prg file-in-group file-in-convolution-group file-out-group\n");
	}

	std::string file_in_group = argv[1];
	std::string file_in_group_conv = argv[2];
	std::string file_out_group = argv[3];

	std::vector <  Group_file >group = read_group_file(file_in_group);
	std::vector< output_format>group_conv = read_group_conv_file(file_in_group_conv);

	std::map<std::pair<int, int>, output_format> group_conv_map;
	for (auto itr = group_conv.begin(); itr != group_conv.end(); itr++) {
		group_conv_map.insert(std::make_pair(std::make_pair(itr->groupid, itr->trackid), *itr));
	}


	std::vector <  Group_file > out_group;
	for (int i = 0; i < group.size(); i++) {
		auto res = group_conv_map.find(std::make_pair(group[i].eventid, group[i].trackid));
		if (res == group_conv_map.end()) {
			fprintf(stderr, "event=%d track=%d not found\n", group[i].eventid, group[i].trackid);
			continue;
		}
		out_group.push_back(linklet_merge(group[i], res->second));
	}

	output_group(file_out_group, out_group);

}
std::vector <  Group_file > read_group_file(std::string filename) {
	std::vector <  Group_file > ret;
	std::ifstream ifs(filename);
	int gid, tid, muon_num, link_num, t_pl, t_rawid;
	int pl, raw;
	double weight;
	std::tuple<int, int, int, int> link;
	int count = 0;

	while (ifs >> gid >> tid >> t_pl >> t_rawid >> link_num) {
		printf("\r read group %d", count);
		count++;
		Group_file g;
		g.eventid = gid;
		g.trackid = tid;
		g.pl = t_pl;
		g.rawid = t_rawid;
		for (int i = 0; i < link_num; i++) {
			ifs >> std::get<0>(link) >> std::get<1>(link) >> std::get<2>(link) >> std::get<3>(link) ;
			g.linklet.push_back(link);
		}
		g.link_num = g.linklet.size();
		ret.push_back(g);
	}
	printf("\r read group %d\n", count);

	return ret;

}
std::vector< output_format> read_group_conv_file(std::string filename) {
	std::vector< output_format> ret;
	std::ifstream ifs(filename);
	output_format out;
	std::tuple<int, int, int, int> path;
	int id;
	int pl, rawid;
	while (ifs >> out.groupid >> out.trackid >> out.num_comfirmed_path >> out.num_cut_path >> out.num_select_path) {
		for (int i = 0; i < out.num_comfirmed_path; i++) {
			ifs >> id >> std::get<0>(path) >> std::get<1>(path) >> std::get<2>(path) >> std::get<3>(path);
			out.comfirmed_path.push_back(std::make_pair(id, path));
		}
		for (int i = 0; i < out.num_cut_path; i++) {
			ifs >> id >> std::get<0>(path) >> std::get<1>(path) >> std::get<2>(path) >> std::get<3>(path);
			out.cut_path.push_back(std::make_pair(id, path));

		}
		for (int i = 0; i < out.num_select_path; i++) {
			ifs >> id >> std::get<0>(path) >> std::get<1>(path) >> std::get<2>(path) >> std::get<3>(path);
			out.select_path.push_back(std::make_pair(id, path));
		}

		ret.push_back(out);
		out.comfirmed_path.clear();
		out.cut_path.clear();
		out.select_path.clear();
	}

	return ret;
}

Group_file  linklet_merge(Group_file&g, output_format&g_conv) {

	Group_file ret;
	ret.eventid = g.eventid;
	ret.trackid = g.trackid;
	ret.pl = g.pl;
	ret.rawid = g.rawid;

	std::set<std::tuple<int, int, int, int>> link;
	std::set<std::pair<int, int>> bt;

	for (auto itr = g_conv.comfirmed_path.begin(); itr != g_conv.comfirmed_path.end(); itr++) {
		link.insert(itr->second);
		bt.insert(std::make_pair(std::get<0>(itr->second) / 10, std::get<2>(itr->second)));
		bt.insert(std::make_pair(std::get<1>(itr->second) / 10, std::get<3>(itr->second)));
	}
	for (auto itr = g_conv.cut_path.begin(); itr != g_conv.cut_path.end(); itr++) {
		link.insert(itr->second);
		bt.insert(std::make_pair(std::get<0>(itr->second) / 10, std::get<2>(itr->second)));
		bt.insert(std::make_pair(std::get<1>(itr->second) / 10, std::get<3>(itr->second)));
	}
	for (auto itr = g_conv.select_path.begin(); itr != g_conv.select_path.end(); itr++) {
		link.insert(itr->second);
		bt.insert(std::make_pair(std::get<0>(itr->second) / 10, std::get<2>(itr->second)));
		bt.insert(std::make_pair(std::get<1>(itr->second) / 10, std::get<3>(itr->second)));
	}

	if (bt.count(std::make_pair(g.pl, g.rawid)) == 0) {
		std::pair<int, int> b0, b1;
		for (auto itr = g.linklet.begin(); itr != g.linklet.end(); itr++) {
			b0.first = std::get<0>(*itr) / 10;
			b1.first = std::get<1>(*itr) / 10;
			b0.second = std::get<2>(*itr);
			b1.second = std::get<3>(*itr);
			if ((b0.first == g.pl&&b0.second == g.rawid) || (b1.first == g.pl&&b1.second == g.rawid)) {
				link.insert(*itr);
			}
		}
	}

	for (auto itr = link.begin(); itr != link.end(); itr++) {
		ret.linklet.push_back(*itr);
	}
	ret.link_num = ret.linklet.size();
	return ret;
}

 void output_group(std::string filename, std::vector<Group_file>&g) {
	 std::ofstream ofs(filename);
	 int count = 0;
	 for (auto itr = g.begin(); itr != g.end(); itr++) {
		 if (count % 1000 == 0) {
			 printf("\r wrtie file %d/%d(%4.1lf%%)", count, g.size(), count*100. / g.size());
		 }
		 count++;

		 ofs << std::fixed << std::right
			 << std::setw(10) << std::setprecision(0) << itr->eventid << " "
			 << std::setw(5) << std::setprecision(0) << itr->trackid << " "
			 << std::setw(4) << std::setprecision(0) << itr->pl << " "
			 << std::setw(4) << std::setprecision(0) << itr->rawid << " "
			 << std::setw(10) << std::setprecision(0) << itr->link_num << std::endl;
		 for (auto itr2 = itr->linklet.begin(); itr2 != itr->linklet.end(); itr2++) {
			 ofs << std::fixed << std::right
				 << std::setw(5) << std::setprecision(0) << std::get<0>(*itr2) << " "
				 << std::setw(5) << std::setprecision(0) << std::get<1>(*itr2) << " "
				 << std::setw(10) << std::setprecision(0) << std::get<2>(*itr2) << " "
				 << std::setw(10) << std::setprecision(0) << std::get<3>(*itr2) << std::endl;
		 }
	 }
	 printf("\r wrtie file %d/%d(%4.1lf%%)\n", count, g.size(), count*100. / g.size());

 }