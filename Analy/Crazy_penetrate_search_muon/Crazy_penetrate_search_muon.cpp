#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib,"functions.lib")
#include <FILE_structure.hpp>
#include <functions.hpp>
#include <omp.h>
#include <set>

class Fiducial_Area {
public:
	int pl;
	double x0, y0, z0, x1, y1, z1;
};
class connect_difference_val {
public:
	double md, oa,dz;
	int dpl,pl,rawid,nseg;
	uint64_t chainid;
};
std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename);
void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map);
bool judge_fiducial_area(std::vector<Fiducial_Area>&area, double x, double y);
mfile1::MFile_minimum mfile_read(std::string file_in_mfile, std::map<int, std::vector<Fiducial_Area>>&area);
bool pene_edgeout_flg(std::vector<mfile1::MFileBase>&chain, std::map<int, std::vector<Fiducial_Area>>&area);
std::multimap <std::pair<int, int>, std::pair<mfile1::MFileChain*, std::vector<mfile1::MFileBase>*> > chain_hash(mfile1::MFile_minimum&m, double hash_size);
mfile1::MFile_minimum select_chains(mfile0::M_Chain &mu, double hash_size, double thr_oa, std::multimap <std::pair<int, int>, std::pair<mfile1::MFileChain*, std::vector<mfile1::MFileBase>*>> &m_hash);
std::vector<mfile0::M_Chain> penetrate_check(mfile0::M_Chain c, mfile1::MFile_minimum&m, double thr_oa, double thr_md);
mfile0::M_Base base_converter(mfile1::MFileBase &base);
int use_thread(double ratio, bool output);
void output_difference(std::string filename, std::map<uint64_t, connect_difference_val> cand, int eventid);


int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:file-in-mfile-muon file-in-mfile-all file-in-ECC file-fa out-mfile\n");
		exit(1);
	}
	std::string file_in_mfile_mu = argv[1];
	std::string file_in_mfile_all = argv[2];
	std::string file_in_ECC = argv[3];
	std::string file_in_area = argv[4];
	std::string file_out_mfile = argv[5];

	//muon の読み込み
	mfile0::Mfile mu;
	mfile0::read_mfile(file_in_mfile_mu, mu);


	//corrmap absの読み込み
	std::string file_in_corrabs = file_in_ECC + "\\0\\align\\corrmap-abs.lst";
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrabs, corr_abs);
	//fiducial areaの読み込み
	std::map<int, std::vector<Fiducial_Area>> area = read_fiducial_Area(file_in_area);
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);

	//corrmap absの適用
	trans_mfile_cordinate(corr_abs, area, z_map);

	//全mfileの読み込み
	mfile1::MFile_minimum  m_all = mfile_read(file_in_mfile_all, area);

	//全mfileを角度でhash
	std::multimap <std::pair<int, int>, std::pair<mfile1::MFileChain*, std::vector<mfile1::MFileBase>*> >all_chains;
	double hash_size = 0.1;

	all_chains = chain_hash(m_all, hash_size);

	mfile0::Mfile m_out;
	m_out.header = mu.header;
	//oaとmdの閾値の決定
	double thr_oa = 0.3, thr_md = 200;
	int count = 0, all = mu.chains.size();
	for (int64_t i = 0; i < mu.chains.size(); i++) {
		printf("\r now ... %5d/%5d", count, all);
		count++;
		mfile1::MFile_minimum m_sel = select_chains(mu.chains[i], hash_size, thr_oa, all_chains);
		std::vector<mfile0::M_Chain> connect_cand = penetrate_check(mu.chains[i], m_sel, thr_oa, thr_md);
		m_out.chains.push_back(mu.chains[i]);
		for (auto itr2 = connect_cand.begin(); itr2 != connect_cand.end(); itr2++) {
			m_out.chains.push_back(*itr2);
		}
	}
	printf("\r now ... %5d/%5d\n", count, all);

	mfile0::write_mfile(file_out_mfile, m_out);
}

std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename) {

	std::ifstream ifs(filename);
	std::multimap<int, Fiducial_Area> fa_multi;
	std::map<int, std::vector<Fiducial_Area>> ret;
	Fiducial_Area fa;
	while (ifs >> fa.pl >> fa.x0 >> fa.y0 >> fa.z0 >> fa.x1 >> fa.y1 >> fa.z1) {
		fa_multi.insert(std::make_pair(fa.pl, fa));
	}

	int count = 0;
	for (auto itr = fa_multi.begin(); itr != fa_multi.end(); itr++) {
		count = fa_multi.count(itr->first);
		auto range = fa_multi.equal_range(itr->first);
		std::vector<Fiducial_Area> fa_vec;
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			fa_vec.push_back(itr2->second);
		}
		ret.insert(std::make_pair(itr->first, fa_vec));
	}

	return ret;

}

void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map) {
	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		//printf("PL%03d %d %d\n", pl,area.count(pl), z_map.count(pl));
		if (area.count(pl) + z_map.count(pl) == 2) {
			auto z = z_map.find(pl);

			auto vec = area.find(pl);
			double tmp_x, tmp_y;
			for (auto itr2 = vec->second.begin(); itr2 != vec->second.end(); itr2++) {
				tmp_x = itr2->x0;
				tmp_y = itr2->y0;
				itr2->x0 = tmp_x * itr->position[0] + tmp_y * itr->position[1] + itr->position[4];
				itr2->y0 = tmp_x * itr->position[2] + tmp_y * itr->position[3] + itr->position[5];
				tmp_x = itr2->x1;
				tmp_y = itr2->y1;
				itr2->x1 = tmp_x * itr->position[0] + tmp_y * itr->position[1] + itr->position[4];
				itr2->y1 = tmp_x * itr->position[2] + tmp_y * itr->position[3] + itr->position[5];

				itr2->z0 = z->second + itr->dz;
				itr2->z1 = z->second + itr->dz;
			}
		}
	}
}

bool judge_fiducial_area(std::vector<Fiducial_Area>&area, double x, double y) {
	//true でArea内　falseでarea外

	//点(x,y)からx軸性の方向に直線を引き、その直線と多角形の辺が何回交わるか。
	//下から上に交わったときwn+1
	//上から下に交わったときwn-1
	int wn = 0;
	double vt;
	for (auto itr = area.begin(); itr != area.end(); itr++) {
		// 上向きの辺、下向きの辺によって処理が分かれる。
	// 上向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、終点は含まない。(ルール1)
		if (itr->y0 <= y && itr->y1 > y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->y0) / (itr->y1 - itr->y0);
			if (x < itr->x0 + vt * (itr->x1 - itr->x0)) {
				++wn;  //ここが重要。上向きの辺と交差した場合は+1
			}
		}
		// 下向きの辺。点Pがy軸方向について、始点と終点の間にある。ただし、始点は含まない。(ルール2)
		else if (itr->y0 > y && itr->y1 <= y) {
			// 辺は点pよりも右側にある。ただし、重ならない。(ルール4)
			// 辺が点pと同じ高さになる位置を特定し、その時のxの値と点pのxの値を比較する。
			vt = (y - itr->y0) / (itr->y1 - itr->y0);
			if (x < itr->x0 + vt * (itr->x1 - itr->x0)) {
				--wn;  //ここが重要。下向きの辺と交差した場合は-1
			}
		}
	}
	if (wn >= 1)return true;
	return false;
}

mfile1::MFile_minimum mfile_read(std::string file_in_mfile, std::map<int, std::vector<Fiducial_Area>>&area) {
	std::ifstream ifs(file_in_mfile, std::ios::binary);


	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << std::setw(3) << std::setfill('0') << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << std::setw(3) << std::setfill('0') << KB << " [MB]" << std::endl;
	}
	mfile1::MFile_minimum mfile;
	//mfile1::MFile mfile;
	//Mfile headerの読み込み
	ifs.read((char*)& mfile.header, sizeof(mfile1::MFileHeader));
	if (ifs.eof()) { throw std::exception(); }
	std::string  filetype = "mfile-a0";
	memcpy((char*)filetype.data(), (char*)& mfile.header.filetype, filetype.size());
	if (filetype != "mfile-a0") { throw std::exception("File format is not mfile-a0."); }

	//mfile info headerの読み込み
	ifs.read((char*)& mfile.info_header, sizeof(mfile1::MFileInfoHeader));
	if (ifs.eof()) { throw std::exception(); }

	if (sizeof(mfile1::MFileChain) != mfile.info_header.classsize1) { throw std::exception("Classsize1 is wrong."); }
	if (sizeof(mfile1::MFileBase) != mfile.info_header.classsize2) { throw std::exception("Classsize2 is wrong."); }
	//Mfile headerの書き込み
	//mfile info headerの書き込み

	uint64_t count = 0;
	int64_t gid = -1;
	for (int64_t c = 0; c < mfile.info_header.Nchain; c++) {
		if (count % 100000 == 0) {
			auto nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		//一旦chainを読む
		mfile1::MFileChain chain;
		ifs.read((char*)& chain, sizeof(mfile1::MFileChain));
		if (ifs.eof()) { throw std::exception(); }

		std::vector< mfile1::MFileBase> basetracks;
		basetracks.reserve(chain.nseg);

		for (int b = 0; b < chain.nseg; b++) {
			mfile1::MFileBase base;
			ifs.read((char*)& base, sizeof(mfile1::MFileBase));
			if (ifs.eof()) { throw std::exception(); }
			basetracks.emplace_back(base);

		}
		//edgeout or penetrate していない かつ short track
		if (chain.nseg < 7 && !pene_edgeout_flg(basetracks,area))continue;

		mfile.chains.push_back(chain);
		mfile.all_basetracks.push_back(basetracks);
	}
	auto nowpos = ifs.tellg();
	auto size1 = nowpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;


	int64_t NChain = 0;
	int64_t NBase = 0;
	for (int64_t i = 0; i < mfile.chains.size(); i++) {
		NChain++;
		NBase += mfile.all_basetracks[i].size();
	}
	mfile.info_header.Nbasetrack = NBase;
	mfile.info_header.Nchain = NChain;
	return mfile;
}
bool pene_edgeout_flg(std::vector<mfile1::MFileBase>&chain, std::map<int, std::vector<Fiducial_Area>>&area) {
	//penetrate or edgeout ならtrue
	int pl0 = chain.begin()->pos / 10;
	int pl1 = chain.rbegin()->pos / 10;

	//penetrateの判定
	if (pl0 == 3 || pl0 == 4 || pl0 == 5 || pl0 == 6)return true;
	if (pl1 == 133 || pl1 == 132 || pl1 == 131 || pl1 == 130)return true;

	double edge_out = 20000;
	int pl;
	double z, ex_x, ex_y,ex_z;
	for (int ex_pl = 1; ex_pl < 4; ex_pl++) {
		//下流側
		pl = pl0 - ex_pl;
		if (area.count(pl) + area.count(pl0) == 2) {
			z = area.at(pl0).begin()->z0;
			ex_z = area.at(pl).begin()->z0;

			ex_x = chain.begin()->x + chain.begin()->ax*(ex_z - z);
			ex_y = chain.begin()->y + chain.begin()->ay*(ex_z - z);
			//edge outしていた場合 return ture
			if (!judge_fiducial_area(area.at(pl), ex_x, ex_y))return true;
		}
		//上流側
		pl = pl1 + ex_pl;
		if (area.count(pl) + area.count(pl1) == 2) {
			z = area.at(pl1).begin()->z0;
			ex_z = area.at(pl).begin()->z0;

			ex_x = chain.rbegin()->x + chain.rbegin()->ax*(ex_z - z);
			ex_y = chain.rbegin()->y + chain.rbegin()->ay*(ex_z - z);
			//edge outしていた場合 return ture
			if (!judge_fiducial_area(area.at(pl), ex_x, ex_y))return true;
		}
	}

	return false;
}

std::multimap <std::pair<int, int>, std::pair<mfile1::MFileChain*, std::vector<mfile1::MFileBase>*> > chain_hash(mfile1::MFile_minimum&m, double hash_size) {

	std::multimap <std::pair<int, int>, std::pair<mfile1::MFileChain*, std::vector<mfile1::MFileBase>*> > ret;

	std::pair<int, int> id_x,id_y;
	for (int64_t num = 0; num < m.chains.size(); num++) {
		for (auto itr = m.all_basetracks[num].begin(); itr != m.all_basetracks[num].end(); itr++) {
			if (itr == m.all_basetracks[num].begin()) {
				id_x.first = itr->ax / hash_size;
				id_x.second = itr->ax / hash_size;
				id_y.first = itr->ay / hash_size;
				id_y.second = itr->ay / hash_size;
			}
			id_x.first = std::min(id_x.first, int(itr->ax / hash_size));
			id_x.second = std::max(id_x.second, int(itr->ax / hash_size));
			id_y.first = std::min(id_y.first, int(itr->ay / hash_size));
			id_y.second = std::max(id_y.second, int(itr->ay / hash_size));
		}
		for (int ix = id_x.first; ix <= id_x.second; ix++) {
			for (int iy = id_y.first; iy <= id_y.second; iy++) {
				ret.insert(std::make_pair(std::make_pair(ix, iy), std::make_pair(&(m.chains[num]), &(m.all_basetracks[num]))));
			}
		}
	}

	return ret;
}

//hashから探索対象のchainを選ぶ
mfile1::MFile_minimum select_chains(mfile0::M_Chain &mu,  double hash_size,double thr_oa, std::multimap <std::pair<int, int>, std::pair<mfile1::MFileChain*, std::vector<mfile1::MFileBase>*>> &m_hash) {
	//探索がoaならmuon chainのax/ayの最大最小-->oaの領域でtanシータの最大最小を決めるか
	mfile1::MFile_minimum ret;
	
	std::tuple<double, double, double, double> angle_range;
	for (auto itr = mu.basetracks.begin(); itr != mu.basetracks.end(); itr++) {
		if (itr == mu.basetracks.begin()) {
			std::get<0>(angle_range) = itr->ax;
			std::get<1>(angle_range) = itr->ax;
			std::get<2>(angle_range) = itr->ay;
			std::get<3>(angle_range) = itr->ay;
		}
		if (std::get<0>(angle_range) > itr->ax)std::get<0>(angle_range) = itr->ax;
		if (std::get<1>(angle_range) < itr->ax)std::get<1>(angle_range) = itr->ax;
		if (std::get<2>(angle_range) > itr->ay)std::get<2>(angle_range) = itr->ay;
		if (std::get<3>(angle_range) < itr->ay)std::get<3>(angle_range) = itr->ay;
	}

	double angle_tmp;
	angle_tmp = atan(std::get<0>(angle_range)) - thr_oa;
	std::get<0>(angle_range) = tan(angle_tmp);
	angle_tmp = atan(std::get<1>(angle_range)) + thr_oa;
	std::get<1>(angle_range) = tan(angle_tmp);

	angle_tmp = atan(std::get<2>(angle_range)) - thr_oa;
	std::get<2>(angle_range) = tan(angle_tmp);
	angle_tmp = atan(std::get<3>(angle_range)) + thr_oa;
	std::get<3>(angle_range) = tan(angle_tmp);

	int ix[2] = { int(std::get<0>(angle_range) / hash_size),int(std::get<1>(angle_range) / hash_size) };
	int iy[2] = { int(std::get<2>(angle_range) / hash_size),int(std::get<3>(angle_range) / hash_size) };

	std::pair<int, int> id;
	std::vector<mfile1::MFileChain> chain_buf;
	std::vector<std::vector<mfile1::MFileBase>*>base_buf;
	for (int iix = ix[0]; iix <= ix[1]; iix++) {
		for (int iiy = iy[0]; iiy <= iy[1]; iiy++) {
			id.first = iix;
			id.second = iiy;
			if (m_hash.count(id) == 0)continue;
			auto range = m_hash.equal_range(id);
			for (auto res = range.first; res != range.second; res++) {
				chain_buf.push_back(*res->second.first);
				base_buf.push_back(res->second.second);
			}
		}
	}

	//printf("all chain size=%d\n", chain_buf.size());

	std::set<uint64_t> chainid;
	for (int i = 0; i < chain_buf.size(); i++) {
		auto res = chainid.insert(chain_buf[i].chain_id);
		if (!res.second)continue;
		ret.chains.push_back(chain_buf[i]);
		ret.all_basetracks.push_back(*base_buf[i]);
	}


	//printf("unique chain size=%d\n", ret.chains.size());

	return ret;
}


std::vector<mfile0::M_Chain> penetrate_check(mfile0::M_Chain c, mfile1::MFile_minimum&m,double thr_oa,double thr_md) {
	int64_t gid = c.basetracks.begin()->group_id;
	std::vector<mfile0::M_Chain> ret;

	std::map<uint64_t, connect_difference_val> cand;
	int dpl_max = 3;
	//#pragma omp parallel for num_threads(use_thread(0.4,false)) schedule(guided)
	for (int i = std::max(0,int(c.basetracks.size()-1- dpl_max)); i < c.basetracks.size(); i++) {
		matrix_3D::vector_3D pos, dir;
		int pl0;
		pl0 = c.basetracks[i].pos / 10;
		pos.x = c.basetracks[i].x;
		pos.y = c.basetracks[i].y;
		pos.z = c.basetracks[i].z;
		dir.x = c.basetracks[i].ax;
		dir.y = c.basetracks[i].ay;
		dir.z = 1;

		for (int64_t j = 0; j < m.all_basetracks.size(); j++) {
			double oa, md;
			double extra[2], z_range[2];
			int t_pl;
			matrix_3D::vector_3D t_pos, t_dir;
			t_pl = m.all_basetracks[j].begin()->pos / 10;
			if (pl0 < t_pl&&t_pl - pl0 <= 10) {
				t_pos.x = m.all_basetracks[j].begin()->x;
				t_pos.y = m.all_basetracks[j].begin()->y;
				t_pos.z = m.all_basetracks[j].begin()->z;
				t_dir.x = m.all_basetracks[j].begin()->ax;
				t_dir.y = m.all_basetracks[j].begin()->ay;
				t_dir.z = 1;
				z_range[1] = pos.z;
				z_range[0] = t_pos.z;
				oa = matrix_3D::opening_angle(dir, t_dir);
				if (oa > thr_oa)continue;
				md = matrix_3D::minimum_distance(pos, t_pos, dir, t_dir, z_range, extra);
				if (md > thr_md)continue;
#pragma omp critical
				{
					connect_difference_val tmp_val;
					tmp_val.md = md;
					tmp_val.chainid = m.chains[j].chain_id;
					tmp_val.oa = oa;
					tmp_val.dpl = t_pl - pl0 + 1;
					tmp_val.pl = t_pl;
					tmp_val.nseg = m.all_basetracks[j].size();
					tmp_val.rawid = m.all_basetracks[j].begin()->rawid;
					tmp_val.dz = pos.z - t_pos.z;
					auto res=cand.insert(std::make_pair(m.chains[j].chain_id, tmp_val));
					if (!res.second) {
						if (res.first->second.dz > tmp_val.dz)res.first->second = tmp_val;
					}
				}
			}
		}
	}


	output_difference("out_chain_diff.txt", cand, gid);

	bool flg = false;
	for (int i = 0; i < m.all_basetracks.size(); i++) {
		if (cand.count(m.chains[i].chain_id) == 0)continue;
		auto res = cand.find(m.chains[i].chain_id);

		mfile0::M_Chain chain;
		for (auto itr2 = m.all_basetracks[i].begin(); itr2 != m.all_basetracks[i].end(); itr2++) {
			mfile0::M_Base base = base_converter(*itr2);
			base.group_id = gid;
			chain.basetracks.push_back(base);
		}
		chain.nseg = chain.basetracks.size();
		chain.pos0 = chain.basetracks.begin()->pos;
		chain.pos1 = chain.basetracks.rbegin()->pos;
		chain.chain_id = m.chains[i].chain_id;

		ret.push_back(chain);
	}
	return ret;
}
mfile0::M_Base base_converter(mfile1::MFileBase &base) {
	mfile0::M_Base ret;
	ret.pos = base.pos;
	ret.rawid = base.rawid;
	ret.ph = base.ph;
	ret.ax = base.ax;
	ret.ay = base.ay;
	ret.x = base.x;
	ret.y = base.y;
	ret.z = base.z;
	ret.group_id = 0;
	ret.flg_d[0] = 0;
	ret.flg_d[1] = 0;
	ret.flg_i[0] = 0;
	ret.flg_i[1] = 0;
	ret.flg_i[2] = 0;
	ret.flg_i[3] = 0;
	return ret;
}

void output_difference(std::string filename, std::map<uint64_t, connect_difference_val> cand,int eventid) {
	std::ofstream ofs(filename, std::ios::app);
	for (auto itr = cand.begin(); itr != cand.end(); itr++) {

		ofs << std::right << std::fixed
			<< std::setw(10) << std::setprecision(0) << eventid << " "
			<< std::setw(10) << std::setprecision(0) << itr->second.chainid << " "
			<< std::setw(4) << std::setprecision(0) << itr->second.pl << " "
			<< std::setw(10) << std::setprecision(0) << itr->second.rawid << " "
			<< std::setw(4) << std::setprecision(0) << itr->second.dpl << " "
			<< std::setw(4) << std::setprecision(0) << itr->second.nseg << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.dz << " "
			<< std::setw(6) << std::setprecision(2) << itr->second.md << " "
			<< std::setw(8) << std::setprecision(6) << itr->second.oa << std::endl;


	}


}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
