#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::map<int, int> Calc_multi(std::vector<mfile0::M_Chain>&chain);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_out = argv[2];
	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);

	std::map<int, int> multi = Calc_multi(m.chains);
	
	std::ofstream ofs(file_out);
	for (auto itr = multi.begin(); itr != multi.end(); itr++) {
		ofs << itr->first << " " << itr->second << std::endl;
	}


}
std::map<int, int> Calc_multi(std::vector<mfile0::M_Chain>&chain) {
	std::map<int, int> group_multi;
	for (auto&c : chain) {
		int gid = c.basetracks.begin()->group_id;

		auto res=group_multi.insert(std::make_pair(gid, 0));
		if (!res.second) {
			res.first->second++;
		}
	}
	std::map<int, int> ret;
	for (auto itr = group_multi.begin(); itr != group_multi.end(); itr++) {
		int multi = itr->second;
		auto res = ret.insert(std::make_pair(multi,1));
		if (!res.second) {
			res.first->second++;
		}
	}
	return ret;

}