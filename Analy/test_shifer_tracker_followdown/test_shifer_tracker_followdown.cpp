#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class link_inf {
public:
	int pl0, pl1, rawid0, rawid1;
};
class linksel_inf {
public:
	int pl0, pl1, rawid0, rawid1;
	double chis_da_r, chis_da_l, chis_dp_r, chis_dp_l;
	double chis_sum() {
		return chis_da_r + chis_da_l + chis_dp_r + chis_dp_l;
	}
};
class sfnt_file {
public:
	int id, entry_in_daily_file, unix_time, tracker_track_id;
	double bt_ax, bt_ay, bt_x, bt_y, nt_ax, nt_ay, nt_x, nt_y;
	double dx, dy, dax, day;
};

std::vector<link_inf> Read_file_link(std::string filename);
std::vector<linksel_inf> Read_file_link_sel(std::string filename);
std::vector<sfnt_file> Read_file_sfnt_file(std::string filename); 

std::set<std::pair<int, int>> search_basetrack(std::vector<link_inf>&link, std::pair<int, int> tid);
std::vector<sfnt_file> search_nt(std::vector<sfnt_file> &sfnt, int id);
std::vector<sfnt_file> sfnt_file_unique(std::vector<sfnt_file>&sfnt);
std::vector<linksel_inf> Search_chi2(std::vector<linksel_inf> &link, int pl0, int raw0, int pl1, int raw1);
double get_chis(std::vector<linksel_inf> &link, int pl0, int raw0, int pl1, int raw1);


int main(int argc,char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:\n");
		exit(1);
	}
	int pl = std::stoi(argv[1]);
	int rawid = std::stoi(argv[2]);
	std::string file_out_shifter = argv[3];

	std::pair<int, int> tid;
	tid.first = pl;
	tid.second = rawid;

	std::string file_link = "link_dump.bin";
	std::string file_link_sel = "link_dump_sel.bin";
	std::string file_sfnt_0 = "matching.bin";
	std::string file_sfnt_1 = "matching.sig.bin";
	std::string file_sfnt_2 = "matching.sig.cor.bin";
	std::string file_sfnt_3 = "matching.sig.cor.cll.bin";

	std::vector<link_inf> link = Read_file_link(file_link);
	std::vector<linksel_inf> link_sel = Read_file_link_sel(file_link_sel);
	std::vector<sfnt_file> sfnt_file_0 = Read_file_sfnt_file(file_sfnt_0);
	std::vector<sfnt_file> sfnt_file_1 = Read_file_sfnt_file(file_sfnt_1);
	std::vector<sfnt_file> sfnt_file_2 = Read_file_sfnt_file(file_sfnt_2);
	std::vector<sfnt_file> sfnt_file_3 = Read_file_sfnt_file(file_sfnt_3);


	printf("ISS-OSS-Fixedwall-TSS connection search\n");
	//bt11_bt41_bt61
	std::map < std::pair<int, int >, std::map < std::pair<int, int >, std::set<std::pair<int, int >>>> follow_down_result;
	std::set < std::pair<int, int>> bt_61_track;
	std::map<std::tuple<int, int, int, int,int>,double> all_link;
	std::set<std::pair<int, int>> bt_11=search_basetrack(link, tid);
	for (auto &t1 : bt_11) {
		all_link.insert(std::make_pair(std::make_tuple(tid.first, tid.second, t1.first, t1.second,0), get_chis(link_sel, tid.first, tid.second, t1.first, t1.second)));
		std::map < std::pair<int, int >, std::set<std::pair<int, int >>> tmp1;
		std::set<std::pair<int, int>> bt_41 = search_basetrack(link, t1);
		for (auto &t2 : bt_41) {
			all_link.insert(std::make_pair(std::make_tuple(t1.first, t1.second, t2.first, t2.second,0), get_chis(link_sel, t1.first, t1.second, t2.first, t2.second)));
			std::set<std::pair<int, int>> bt_61 = search_basetrack(link, t2);
			tmp1.insert(std::make_pair(t2, bt_61));
			for (auto &t3 : bt_61) {
				all_link.insert(std::make_pair(std::make_tuple(t2.first, t2.second, t3.first, t3.second,0), get_chis(link_sel, t2.first, t2.second, t3.first, t3.second)));
				bt_61_track.insert(t3);
			}
		}
		follow_down_result.insert(std::make_pair(t1, tmp1));
	}

	printf("(%3d,%10d)\n", tid.first, tid.second);
	for (auto &t1 : follow_down_result) {
		printf("        |--(%3d,%10d)\n", t1.first.first, t1.first.second);
		for (auto &t2 : t1.second) {
			printf("                        |--(%3d,%10d)\n", t2.first.first, t2.first.second);
			for (auto &t3 : t2.second) {
				printf("                                        |--(%3d,%10d)\n", t3.first, t3.second);
				std::vector<linksel_inf> chi2 = Search_chi2(link_sel, t2.first.first, t2.first.second, t3.first, t3.second);
				for (auto &l : chi2) {
					printf("                                          (%10d,%10d)[%6.3lf %6.3lf %6.3lf %6.3lf %6.3lf]\n", l.rawid0,l.rawid1,l.chis_da_r, l.chis_da_l, l.chis_dp_r, l.chis_dp_l, l.chis_sum());
				}

			}
		}
	}

	printf("TSS-NT connection search\n");
	std::multimap<std::tuple<int, int, int, int,int>, double> all_connect;
	for (auto &al : all_link) {
		all_connect.insert(al);
	}

	for (auto &t : bt_61_track) {
		std::vector<sfnt_file> sfnt_0 = search_nt(sfnt_file_0, t.second);
		std::vector<sfnt_file> sfnt_1 = search_nt(sfnt_file_1, t.second);
		std::vector<sfnt_file> sfnt_2 = search_nt(sfnt_file_2, t.second);
		std::vector<sfnt_file> sfnt_3 = search_nt(sfnt_file_3, t.second);

		printf("(%3d,%10d)\n", t.first, t.second);

		printf("first connection\n");
		for (auto &sfnt : sfnt_0) {
			auto res = all_connect.find(std::make_tuple(t.first, t.second, sfnt.unix_time, sfnt.tracker_track_id,sfnt.entry_in_daily_file));
			if (res==all_connect.end())all_connect.insert(std::make_pair(std::make_tuple(t.first, t.second, sfnt.unix_time, sfnt.tracker_track_id, sfnt.entry_in_daily_file), 0));
			else res->second = 0;

			printf("\t %5d %10d %3d %7.4lf %7.4lf %7.4lf %7.4lf\n", sfnt.entry_in_daily_file, sfnt.unix_time, sfnt.tracker_track_id, sfnt.dx, sfnt.dy, sfnt.dax, sfnt.day);
		}
		printf("\n");

		printf("rough cut\n");
		for (auto &sfnt : sfnt_1) {
			auto res = all_connect.find(std::make_tuple(t.first, t.second, sfnt.unix_time, sfnt.tracker_track_id, sfnt.entry_in_daily_file));
			if (res == all_connect.end())all_connect.insert(std::make_pair(std::make_tuple(t.first, t.second, sfnt.unix_time, sfnt.tracker_track_id, sfnt.entry_in_daily_file), 1));
			else res->second = 1;

			printf("\t %5d %10d %3d %7.4lf %7.4lf %7.4lf %7.4lf\n", sfnt.entry_in_daily_file, sfnt.unix_time, sfnt.tracker_track_id, sfnt.dx, sfnt.dy, sfnt.dax, sfnt.day);
		}
		printf("\n");

		printf("alignment correction\n");
		for (auto &sfnt : sfnt_2) {
			auto res = all_connect.find(std::make_tuple(t.first, t.second, sfnt.unix_time, sfnt.tracker_track_id, sfnt.entry_in_daily_file));
			if (res == all_connect.end())all_connect.insert(std::make_pair(std::make_tuple(t.first, t.second, sfnt.unix_time, sfnt.tracker_track_id, sfnt.entry_in_daily_file), 2));
			else res->second = 2;

			printf("\t %5d %10d %3d %7.4lf %7.4lf %7.4lf %7.4lf\n", sfnt.entry_in_daily_file, sfnt.unix_time, sfnt.tracker_track_id, sfnt.dx, sfnt.dy, sfnt.dax, sfnt.day);
		}
		printf("\n");

		printf("chi2 cut\n");
		for (auto &sfnt : sfnt_3) {
			auto res = all_connect.find(std::make_tuple(t.first, t.second, sfnt.unix_time, sfnt.tracker_track_id, sfnt.entry_in_daily_file));
			if (res == all_connect.end())all_connect.insert(std::make_pair(std::make_tuple(t.first, t.second, sfnt.unix_time, sfnt.tracker_track_id, sfnt.entry_in_daily_file), 3));
			else res->second = 3;

			printf("\t %5d %10d %3d %7.4lf %7.4lf %7.4lf %7.4lf\n", sfnt.entry_in_daily_file, sfnt.unix_time, sfnt.tracker_track_id, sfnt.dx, sfnt.dy, sfnt.dax, sfnt.day);
		}
		printf("\n");
	}

	std::ofstream ofs(file_out_shifter);
	for (auto &al : all_connect) {
		ofs << std::get<0>(al.first) << " " << std::get<1>(al.first) << " " << std::get<2>(al.first) << " " << std::get<3>(al.first) << " " << al.second << " " << std::get<4>(al.first) << std::endl;
	}
	printf("\n");

}

std::vector<link_inf> Read_file_link(std::string filename) {
	std::vector<link_inf> ret;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cerr << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cerr << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	link_inf l;
	while (ifs.read((char*)& l, sizeof(link_inf))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(l);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;

}
std::vector<linksel_inf> Read_file_link_sel(std::string filename) {
	std::vector<linksel_inf> ret;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cerr << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cerr << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	linksel_inf l;
	while (ifs.read((char*)& l, sizeof(linksel_inf))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(l);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	return ret;

}
std::vector<sfnt_file> Read_file_sfnt_file(std::string filename) {
	std::vector<sfnt_file> ret;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cerr << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cerr << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	sfnt_file l;
	while (ifs.read((char*)& l, sizeof(sfnt_file))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(l);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no linklet!\n", filename.c_str());
		exit(1);
	}
	ret = sfnt_file_unique(ret);

	return ret;

}

std::vector<sfnt_file> sfnt_file_unique(std::vector<sfnt_file>&sfnt) {
	std::vector<sfnt_file> ret;
	std::map<std::tuple<int, int, int>, sfnt_file> sfnt_map;
	std::tuple<int, int, int> id;
	for (auto &t:sfnt) {
		if (t.tracker_track_id < 0)continue;
		std::get<0>(id) = t.id;
		std::get<1>(id) = t.unix_time;
		std::get<2>(id) = t.tracker_track_id;
		sfnt_map.insert(std::make_pair(id, t));

	}
	for (auto &t : sfnt_map) {
		ret.push_back(t.second);
	}
	fprintf(stderr,"multi del %d --> %d(%4.1lf%%)\n", sfnt.size(), ret.size(), ret.size()*100. / sfnt.size());
	return ret;

}

std::set<std::pair<int, int>> search_basetrack(std::vector<link_inf>&link, std::pair<int, int> tid) {
	std::set<std::pair<int, int>> ret;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		if (itr->pl0 == tid.first&&itr->rawid0 == tid.second) {
			ret.insert(std::make_pair(itr->pl1, itr->rawid1));
		}
	}
	return ret;
}
std::vector<sfnt_file> search_nt(std::vector<sfnt_file> &sfnt, int id) {
	std::vector<sfnt_file> ret;

	for (auto itr = sfnt.begin(); itr != sfnt.end(); itr++) {
		if (itr->id == id) {
			ret.push_back(*itr);
		}
	}

	return ret;
}

std::vector<linksel_inf> Search_chi2(std::vector<linksel_inf> &link,int pl0,int raw0, int pl1, int raw1) {
	std::vector<linksel_inf> ret;
	for (auto l : link) {
		if (l.pl0==pl0&&l.pl1==pl1&&l.rawid0 == raw0 && l.rawid1 == raw1) {
			ret.push_back(l);
		}
	}
	return ret;
}
double get_chis(std::vector<linksel_inf> &link, int pl0, int raw0, int pl1, int raw1) {
	for (auto l : link) {
		if (l.pl0 == pl0 && l.pl1 == pl1 && l.rawid0 == raw0 && l.rawid1 == raw1) {
			return l.chis_sum();
		}
	}
	return -1;
}