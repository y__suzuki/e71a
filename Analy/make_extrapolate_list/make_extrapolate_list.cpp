#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <sstream>
#include <ios>     // std::left, std::right
#include <iomanip> 

#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"
#pragma comment(lib, "VxxReader.lib")

std::map<int, mfile0::M_Chain> event_divide(std::vector<mfile0::M_Chain>&c);
void command_output(int eventid, mfile0::M_Chain&c, std::vector<std::string> &command);
int rawid_search(mfile0::M_Chain&c, int pl);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage: file-in-mfile output_batch\n\n");
		exit(1);
	}
	std::string filename = argv[1];
	std::string file_out = argv[2];
	mfile0::Mfile m;
	mfile1::read_mfile_extension(filename, m);
	std::map<int, mfile0::M_Chain>chain_ev = event_divide(m.chains);

	std::vector<std::string> command;
	for (auto itr = chain_ev.begin(); itr != chain_ev.end(); itr++) {
		command_output(itr->first, itr->second, command);
	}

	std::ofstream ofs(file_out);
	for (int i = 0; i < command.size(); i++) {
		ofs << command[i] << std::endl;
	}

}
std::map<int, mfile0::M_Chain> event_divide(std::vector<mfile0::M_Chain>&c) {
	std::map<int, mfile0::M_Chain> ret;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		int eventid = itr->basetracks.begin()->group_id / 100000;
		ret.insert(std::make_pair(eventid, *itr));
	}
	return ret;
}

void command_output(int eventid, mfile0::M_Chain&c, std::vector<std::string> &command) {
	std::set<std::pair<int, int>> check_pl;
	int nseg = c.basetracks.size();
	if (c.basetracks.size() > 1) {
		if (c.basetracks[nseg - 1].pos / 10 - c.basetracks[nseg - 2].pos / 10 > 1) {
			int pl0 = c.basetracks[nseg - 2].pos / 10;
			int pl_max = c.basetracks[nseg - 1].pos / 10;
			for (int pl = pl0 + 1; pl < pl_max; pl++) {
				check_pl.insert(std::make_pair(pl0, pl));
			}
		}
	}
	if (c.basetracks.size() > 2) {
		if (c.basetracks[nseg - 2].pos / 10 - c.basetracks[nseg - 3].pos / 10 > 1) {
			int pl0 = c.basetracks[nseg - 3].pos / 10;
			int pl_max = c.basetracks[nseg - 2].pos / 10;
			for (int pl = pl0 + 1; pl < pl_max; pl++) {
				check_pl.insert(std::make_pair(pl0, pl));
			}
		}
	}
	int up_pl = c.basetracks.rbegin()->pos / 10;
	check_pl.insert(std::make_pair(up_pl, up_pl ));
	check_pl.insert(std::make_pair(up_pl, up_pl + 1));

	for (auto itr = check_pl.begin(); itr != check_pl.end(); itr++) {
		std::stringstream com;
		int pl0 = itr->first;
		int pl1 = itr->second;
		int rawid = rawid_search(c, pl0);
		if (rawid < 0) {
			fprintf(stderr, "event %d PL%03d track not found\n", eventid, pl0);
		}
		com << "call extra.bat "
			<< std::setw(5) << std::setfill('0') << eventid << " "
			<< std::setw(3) << std::setfill('0') << pl0 << " "
			//			<< std::setw(10) << std::setfill(' ') << rawid << " "
			<< std::setfill(' ') << rawid << " "
			<< std::setw(3) << std::setfill('0') << pl1;
		command.push_back(com.str());

	}
	
}
int rawid_search(mfile0::M_Chain&c, int pl) {

	int rawid = -1;
	for (auto itr = c.basetracks.begin(); itr != c.basetracks.end(); itr++) {
		if (pl == itr->pos / 10) {
			rawid = itr->rawid;
			break;
		}
	}
	return rawid;
}