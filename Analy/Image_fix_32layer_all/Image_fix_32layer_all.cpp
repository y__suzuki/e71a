#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <cassert>  // std::assert
#include <sstream>
#include <iomanip>
#include <string>

#pragma comment(lib, "VxxReader.lib")
#pragma comment(lib, "opencv_world430d.lib")
#pragma comment(lib, "opencv_world430.lib")
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <VxxReader.h>
#include <picojson.h>
#include <omp.h>
#include <time.h>
#include <filesystem>
#include <ctime>

class EachImager_Param {
public:
	//CamareaID * 5 + SensorID = ImagerID
	//Width,Height 縦横のpixel数
	double Aff_coef[6], Aff_coef_offset[6], DZ;
	int CameraID, GridX, GridY, Height, ImagerID, SensorID, Width;
	std::string LastReportFilePath;
};
class EachShot_Param {
public:
	int View, Imager, StartAnalysisPicNo, GridX, GridY;
	double Z_begin, X_center, Y_center;
	std::string TrackFilePath;
	//GridX,Y intでダイジョブ?
};
class EachView_Param {
public:
	int  LayerID, NPicThickOfLayer;
	double Stage_x, Stage_y, ThickOfLayer, Z_begin, Z_end;
};

std::map<int, EachImager_Param> read_EachImager(std::string filename);
std::vector<EachShot_Param> read_EachShot(std::string filename);
std::map<int, EachView_Param> read_EachView(std::string filename);

std::string change_extension(std::string &filename);


std::vector<cv::Mat> read_spng(std::string filename);
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin);
void write_spng(std::string filename, std::vector<cv::Mat> &mat0);
template <class T> void write_vbin(std::ofstream& ofs, const std::vector<T>& vout);

void write_png(std::string filename, std::vector<cv::Mat> &mat0);
bool check_image(cv::Mat &mat);

void output_error_inf(std::string filename, std::string image_file, std::string scan_folder, EachImager_Param &imager, EachShot_Param &shot, EachView_Param &view, int num);

int use_thread(double ratio, bool output);

int main(int argc, char**argv) {
	clock_t start, fin;
	start = clock();
	if (argc != 3) {
		fprintf(stderr, "usage:prg scan-folder out_errorinf_file\n");
		exit(1);
	}

	std::string file_in_path = argv[1];
	std::string file_error_out_path = argv[2];
	std::string file_error_out = file_error_out_path + "\\error_inf.txt";
	std::string file_error_out_img = file_error_out_path + "\\image";
	//EachImager:1視野内での各sensorの値 センサー数(72)個
	//EachShot:全視野での各sensorの値 視野数*センサー数(72)個
	//EachView:各視野の値 視野数
	std::string file_in_Beta_EachImagerParam = file_in_path + "\\Beta_EachImagerParam.json";
	std::string file_in_Beta_EachShotParam = file_in_path + "\\Beta_EachShotParam.json";
	std::string file_in_Beta_EachViewParam = file_in_path + "\\Beta_EachViewParam.json";

	//printf("%s\n", file_in_Beta_EachImagerParam.c_str());
	//printf("%s\n", file_in_Beta_EachShotParam.c_str());
	//printf("%s\n", file_in_Beta_EachViewParam.c_str());

	std::map<int, EachImager_Param> imager_map = read_EachImager(file_in_Beta_EachImagerParam);
	std::vector<EachShot_Param> shot_vec = read_EachShot(file_in_Beta_EachShotParam);
	std::map<int, EachView_Param> view_map = read_EachView(file_in_Beta_EachViewParam);
	printf("Beta related files read fin\n");
	//error出力用のフォルダ作成
	std::filesystem::create_directories(file_error_out_path);
	double CPU_ratio = 0.4;
	int64_t atomic_num = 0;
#pragma omp parallel for num_threads(use_thread(CPU_ratio,true)) schedule(guided)
	for (int64_t i = 0; i < shot_vec.size(); i++) {
#pragma omp atomic
		atomic_num++;
		printf("\r now ...%d/%d", atomic_num, shot_vec.size());
		std::string file_spng = shot_vec[i].TrackFilePath;
		file_spng = change_extension(file_spng);
		file_spng = file_in_path + "\\" + file_spng;
		//printf("file %s\n", file_spng.c_str());
		if (!std::filesystem::exists(file_spng)) {
			printf("file[%s] not found\n",file_spng.c_str());
		}
		std::vector<cv::Mat> vmat = read_spng(file_spng);
		bool flg = false;
		for (int j = 0; j < vmat.size(); j++) {
			if (check_image(vmat[j])) continue;
			//errorだった場合
			//情報の出力
#pragma omp critical
			output_error_inf(file_error_out, file_spng, file_in_path,  imager_map.at(shot_vec[i].Imager), shot_vec[i], view_map.at(shot_vec[i].View), j);
			//0 imageで置き換え
			cv::Mat image = cv::Mat::zeros(1088, 2048, CV_8U);
			vmat[j] = image;

			flg = true;
		}
		if (flg) {
#pragma omp critical
			{
				//そのままの画像を保存
				std::string file_out_img_error = file_spng.substr(file_spng.length() - 37);
				file_out_img_error = file_error_out_img + "\\" + file_out_img_error;
				std::string file_out_img_path = file_out_img_error.substr(0, file_out_img_error.length() - 32);
				std::filesystem::create_directories(file_out_img_path);
				std::error_code ec;
				bool res = std::filesystem::copy_file(file_spng, file_out_img_error, std::filesystem::copy_options::none,ec);
				int id = 1;
				std::string  file_out_img_error2 = file_out_img_error.substr(0, file_out_img_error.length() - 5);
				while (!res&&id<100) {
					std::stringstream file_out_img_error3;
					file_out_img_error3 << file_out_img_error2 << "_" << std::setw(3) << std::setfill('0') << id << ".spng";
					res = std::filesystem::copy_file(file_spng, file_out_img_error3.str(), std::filesystem::copy_options::none, ec);
					id++;
				}
				//0-->false, 1-->true
				std::cout << "\rcopy result: " << res ;
				if (res == 0) std::cout << " Failure. The error image could not be copied." << std::endl;
				else {
					std::cout << " Success. The error image has been copied." << std::endl;
				}
				//修正画像の上書き
				write_spng(file_spng, vmat);
			}
		}
	}
	printf("\r now ...%d/%d\n", atomic_num, shot_vec.size());


	fin = clock();
	printf("all film check done %.0lf[s]\n", static_cast<double>(fin - start) / CLOCKS_PER_SEC);

}
std::map<int, EachImager_Param> read_EachImager(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}

	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	std::map<int, EachImager_Param> ret;
	//mapのkey=imagerID
	int ImagerID = 0;
	picojson::array &all = v.get<picojson::array>();

	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachImager_Param param;
		picojson::array &Aff_coef = obj["Aff_coef"].get<picojson::array>();
		//picojson::array &Aff_coef_offset = obj["Aff_coef_offset"].get<picojson::array>();
		int i = 0;
		for (int i = 0; i < 6; i++) {
			param.Aff_coef[i] = Aff_coef[i].get<double>();
			//param.Aff_coef_offset[i] = Aff_coef_offset[i].get<double>();
		}
		param.DZ = obj["DZ"].get<double>();
		param.CameraID = (int)obj["CameraID"].get<double>();
		param.GridX = (int)obj["GridX"].get<double>();
		param.GridY = (int)obj["GridY"].get<double>();
		param.Height = (int)obj["Height"].get<double>();
		param.ImagerID = (int)obj["ImagerID"].get<double>();
		param.SensorID = (int)obj["SensorID"].get<double>();
		param.Width = (int)obj["Width"].get<double>();
		param.LastReportFilePath = obj["LastReportFilePath"].get<std::string>();
		ret.insert(std::make_pair(ImagerID, param));
		ImagerID++;

		//printf("imagerID %d factor %.10lf\n", param.ImagerID, param.Aff_coef[0] * param.Aff_coef[3] - param.Aff_coef[1] * param.Aff_coef[2]);
	}
	printf("number of imager = %d\n", ImagerID);
	return ret;
}
std::vector<EachShot_Param> read_EachShot(std::string filename) {
	//JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}

	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}

	//位置-->shot paramに行けるようにしたい-->全beta.json読み込んだ後でやる
	std::vector<EachShot_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachShot_Param param;
		param.Z_begin = obj["Z_begin"].get<double>();
		param.View = (int)obj["View"].get<double>();
		param.Imager = (int)obj["Imager"].get<double>();
		param.StartAnalysisPicNo = (int)obj["StartAnalysisPicNo"].get<double>();
		param.GridX = (int)obj["GridX"].get<double>();
		param.GridY = (int)obj["GridY"].get<double>();
		param.TrackFilePath = obj["TrackFilePath"].get<std::string>();
		param.X_center = -1;
		param.Y_center = -1;
		ret.push_back(param);
	}
	printf("number of shot = %zd\n", ret.size());

	return ret;
}
std::map<int, EachView_Param> read_EachView(std::string filename) {
	// JSONデータの読み込み。
	std::ifstream ifs(filename, std::ios::in);
	if (ifs.fail()) {
		std::cerr << "failed to read test.json" << std::endl;
		std::cerr << "filename = " << filename << std::endl;
		exit(1);
	}
	const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
	ifs.close();

	// JSONデータを解析する。
	picojson::value v;
	const std::string err = picojson::parse(v, json);
	if (err.empty() == false) {
		std::cerr << err << std::endl;
		exit(1);
	}
	std::map<int, EachView_Param> ret;
	picojson::array &all = v.get<picojson::array>();
	int ViewID = 0;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		picojson::object& obj = itr->get<picojson::object>();
		EachView_Param param;
		param.Stage_x = obj["Stage_x"].get<double>();
		param.Stage_y = obj["Stage_y"].get<double>();
		param.ThickOfLayer = obj["ThickOfLayer"].get<double>();
		param.Z_begin = obj["Z_begin"].get<double>();
		param.Z_end = obj["Z_end"].get<double>();
		param.LayerID = (int)obj["LayerID"].get<double>();
		param.NPicThickOfLayer = (int)obj["NPicThickOfLayer"].get<double>();
		ret.insert(std::make_pair(ViewID, param));
		ViewID++;
	}
	printf("number of view = %d\n", ViewID);
	return ret;
}

std::vector<cv::Mat> read_spng(std::string filename) {
	std::vector<std::vector<uchar>> vvin;
	read_vbin(filename, vvin);
	std::vector<cv::Mat> vmat1;
	for (int i = 0; i < vvin.size(); i++) {
		cv::Mat mat1;
		mat1 = cv::imdecode(vvin[i], 0);
		vmat1.emplace_back(mat1);
	}
	return vmat1;
}
template <class T> void read_vbin(std::string filepath, std::vector<std::vector<T>>& vvin)
{
	std::ifstream ifs(filepath, std::ios::binary);
	if (!ifs.is_open()) { std::cout << "cannot open " << filepath << std::endl; throw std::exception(); }
	int i = 0;
	while (true)
	{
		uint64_t i64;
		ifs.read((char*)&i64, sizeof(uint64_t));
		if (ifs.eof()) { break; }

		//printf("%d i64 = %llu\n", i, i64);
		i++;

		std::vector<T> vin;
		if (i64 == -1)
		{
			vvin.push_back(vin);
			continue;
		}
		if (i64 > vin.max_size()) { throw std::exception(""); }
		vin.reserve(i64);
		for (uint64_t j = 0; j < i64; j++)
		{
			T p;
			ifs.read((char*)&p, sizeof(T));
			vin.push_back(p);
		}
		vvin.push_back(vin);
	}
	ifs.close();
}
std::string change_extension(std::string &filename) {
	//.dat --> .spngの変換
	for (int i = 0; i < 3;i++) {
		filename.pop_back();
	}
	filename += "spng";
	return filename;
}



void write_spng(std::string filename, std::vector<cv::Mat> &mat0) {
	std::vector<int> params = std::vector<int>(2);
	params[0] = cv::IMWRITE_PNG_STRATEGY;
	params[1] = cv::IMWRITE_PNG_STRATEGY_RLE;
	//出力ファイル名を作ってるだけ
	std::ofstream ofs;
	ofs.exceptions(std::ios::failbit | std::ios::badbit);
	ofs.open(filename, std::ios::binary);
	for (int i = 0; i < 32; i++)
	{
		std::vector<uchar> buf;
		cv::imencode(".png", mat0.at(i), buf, params);
		write_vbin(ofs, buf);
	}
	ofs.close();
}
template <class T> void write_vbin(std::ofstream& ofs, const std::vector<T>& vout)
{
	uint64_t j64 = vout.size();
	if (j64 == 0) { j64 = -1; }
	ofs.write((char*)&j64, sizeof(uint64_t));
	ofs.write((char*)vout.data(), sizeof(T) * vout.size());
}


void write_png(std::string filename, std::vector<cv::Mat> &mat0) {

	int width = mat0[1].cols;
	int height = mat0[1].rows;

	std::vector<cv::Mat> vmat_color;
	for (int i = 1; i < mat0.size(); i++) {
		cv::Mat image = cv::Mat::zeros(height, width, CV_8UC3);
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				if (mat0[i].at<uchar>(y, x) == 0x00) {
					image.at<cv::Vec3b>(y, x)[0] = 0x00; //青
					image.at<cv::Vec3b>(y, x)[1] = 0x00; //緑
					image.at<cv::Vec3b>(y, x)[2] = 0x00; //赤
				}
				else {
					image.at<cv::Vec3b>(y, x)[0] = 0xff; //青
					image.at<cv::Vec3b>(y, x)[1] = 0xff; //緑
					image.at<cv::Vec3b>(y, x)[2] = 0xff; //赤
				}
			}
		}
		vmat_color.push_back(image);
	}

	std::vector<int> params = std::vector<int>(2);
	params[0] = cv::IMWRITE_PNG_STRATEGY;
	params[1] = cv::IMWRITE_PNG_STRATEGY_RLE;
	std::string out_filename;
	for (int k = 0; k < vmat_color.size(); k++) {
		{
			std::stringstream ss;
			ss << filename << "_" << std::setfill('0') << std::setw(3) << k << ".png";
			out_filename = ss.str();
		}
		cv::imwrite(out_filename, vmat_color[k]);
	}
}

bool check_image(cv::Mat &mat) {
	if (mat.data == NULL) {
		return false;
	}
	return true;
}

void output_error_inf(std::string filename,std::string image_file,std::string scan_folder, EachImager_Param &imager, EachShot_Param &shot, EachView_Param &view,int num) {
	// ファイル・ディレクトリの最終更新日時を取得
	std::filesystem::file_time_type file_time = std::filesystem::last_write_time(image_file);
	auto sec = std::chrono::duration_cast<std::chrono::seconds>(file_time.time_since_epoch());
	std::time_t t = sec.count();
	const tm* lt = std::localtime(&t);
	
	std::ofstream ofs(filename, std::ios::app);
		ofs << "{" << std::endl
			<< "\t\"Name\": " << "\"" << scan_folder << "\"" << "," << std::endl
			<< "\t\"Time\": " << std::put_time(lt, "%Y/%m/%d %T")  << "," << std::endl
			<< "\t\"Folder\": " << "\"" << std::setw(2) << std::setfill('0') << imager.CameraID << "_" << std::setw(2) << std::setfill('0') << imager.SensorID << "\"" << "," << std::endl
			<< "\t\"View\": " << shot.View << "," << std::endl
			<< "\t\"Filename\": " <<"\""<< change_extension(shot.TrackFilePath) << "\""<< "," << std::endl
			<< "\t\"Layer\": " << view.LayerID << "," << std::endl
			<< "\t\"StartAnalysisPicNo\": " << shot.StartAnalysisPicNo << "," << std::endl
			<< "\t\"Number_of_image\": " << num << "," << std::endl
			<< "\t\"Gridx\": " << shot.GridX << "," << std::endl
			<< "\t\"Gridy\": " << shot.GridY << "," << std::endl
			<< "\t\"Z_begin(shot)\": " << shot.Z_begin << "," << std::endl
			<< "\t\"Z_begin(view)\": " << view.Z_begin << "," << std::endl
			<< "\t\"Z_end(view)\": " << view.Z_end <<std::endl
			<< "}" << std::endl;
			ofs.close();
}

int use_thread(double ratio, bool output) {
	if (ratio >= 1) {
		ratio = 1;
	}
	else if (ratio < 0.1) {
		ratio = 0.1;
	}
	int num_all_thread = omp_get_max_threads();
	if (output) {
		printf("max thread = %d\n", num_all_thread);
		printf("ratio      = %3.2lf\n", ratio);
		printf("using... %d thread\n", int(num_all_thread*ratio));
	}
	return (int)(num_all_thread * ratio);
}
