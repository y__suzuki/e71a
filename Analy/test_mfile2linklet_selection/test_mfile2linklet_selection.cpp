#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>

std::vector<netscan::linklet_t> select_linklet(std::vector<mfile0::M_Chain>&c, std::vector<netscan::linklet_t> link);

int main(int argc, char **argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg in-mfile in-link out-link\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_linklet = argv[2];
	std::string file_out_linklet = argv[3];

	mfile0::Mfile m;
	std::vector<netscan::linklet_t> link;
	mfile1::read_mfile_extension(file_in_mfile, m);
	netscan::read_linklet_txt(file_in_linklet, link);

	std::vector<netscan::linklet_t> link_sel = select_linklet(m.chains, link);
	netscan::write_linklet_txt(file_out_linklet, link);

}
std::vector<netscan::linklet_t> select_linklet(std::vector<mfile0::M_Chain>&c, std::vector<netscan::linklet_t> link) {
	int pl[2];
	pl[0] = link.begin()->b[0].pl;
	pl[1] = link.begin()->b[1].pl;

	std::vector<netscan::linklet_t> ret;
	std::map < std::pair<int, int>, netscan::linklet_t > linklet_map;
	for (auto itr = link.begin(); itr != link.end(); itr++) {
		linklet_map.insert(std::make_pair(std::make_pair(itr->b[0].rawid, itr->b[1].rawid), *itr));
	}

	std::pair<int, int> rawid_pair;
	int count = 0;
	int find = 0;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		count = 0;
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			if (itr2->pos / 10 == pl[0]) {
				count++;
				rawid_pair.first = itr2->rawid;
			}
			else if (itr2->pos / 10 == pl[1]) {
				count++;
				rawid_pair.second = itr2->rawid;
			}
		}
		if (count == 2) {
			auto res = linklet_map.find(rawid_pair);
			if (res == linklet_map.end())continue;
			ret.push_back(res->second);
			find++;

		}
	}
	printf("linklet selection %d --> %d (%4.1lf%%)\n", link.size(), find, find*100. / link.size());
	return ret;
}