#pragma comment(lib,"functions.lib")
#include <functions.hpp>
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>


#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <map>

#include <omp.h>
class Chain_inf {
public:
	uint64_t chainid;
	int pl0, pl1, nseg;
	double ax_up, ay_up, ax_down, ay_down, x_up, y_up, z_up, x_down, y_down, z_down;
	int flg;
	//ret.flg = pene_up.count(num) * 1
	//	+ edge_up.count(num) * 10
	//	+ pene_down.count(num) * 100
	//	+ edge_down.count(num) * 1000
	//	+ short_thin.count(num) * 10000;

};

std::vector<Chain_inf> read_chian_inf(std::string filename);
std::multimap<int, Chain_inf*> Select_up_stop_pl(std::vector<Chain_inf> &chain);
std::multimap<int, Chain_inf*>Select_down_stop_pl(std::vector<Chain_inf> &chain);

std::multimap<std::pair<int, int>, Chain_inf*> Select_up_stop_pos(std::vector<Chain_inf> &chain, std::pair<double, double> &pos_min, double hash);
std::multimap<std::pair<int, int>, Chain_inf*> Select_down_stop_pos(std::vector<Chain_inf> &chain, std::pair<double, double> &pos_min, double hash);

std::vector<Chain_inf> serach_connectable_chain_up_stop_lowmom(Chain_inf *chain, std::multimap<int, Chain_inf*>&target_chains);
std::vector<Chain_inf> serach_connectable_chain_up_stop_int(Chain_inf *chain, std::multimap<int, Chain_inf*>&target_chains);

mfile0::Mfile create_mfile(std::vector<std::vector<Chain_inf>>&chain_inf, mfile1::MFile_minimum &m, std::map<uint64_t, uint64_t> &chainid_map);


void serach_connectable_chain_up_stop_lowmom2(Chain_inf *chain, std::multimap<int, Chain_inf*>&target_chains, std::string file_out_path);
Chain_inf select_best_cand(Chain_inf *chain, std::vector<Chain_inf>&cand);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg file-in mfile-in file-out-path\n");
		exit(1);
	}

	std::string file_in_chain = argv[1];
	std::string file_in_mfile = argv[2];
	std::string file_out_path = argv[3];
	std::vector<Chain_inf> chain = read_chian_inf(file_in_chain);
	std::multimap<int, Chain_inf*> up_stop_pl = Select_up_stop_pl(chain);
	std::multimap<int, Chain_inf*> down_stop_pl = Select_down_stop_pl(chain);

	std::pair<double,double> pos_min_up, pos_min_down;
	double hash = 5000;
	std::multimap<std::pair<int, int>, Chain_inf*> up_stop_pos = Select_up_stop_pos(chain, pos_min_up, hash);
	std::multimap<std::pair<int, int>, Chain_inf*> down_stop_pos = Select_down_stop_pos(chain, pos_min_up, hash);



	mfile1::MFile_minimum m;
	mfile1::read_mfile(file_in_mfile, m);
	std::map<uint64_t, uint64_t> chainid_map;
	for (uint64_t i = 0; i < m.chains.size(); i++) {
		chainid_map.insert(std::make_pair(m.chains[i].chain_id, i));
	}

	int count = 0;
	int all = 0, cand = 0;
	for (auto itr = up_stop_pos.begin(); itr != up_stop_pos.end(); itr++) {
		count = up_stop_pos.count(itr->first);


		if (20 < itr->first.first&&itr->first.first < 25) {
			if (20 < itr->first.second&&itr->first.second < 25) {
				std::vector<std::vector<Chain_inf>> chain_v;
				std::vector<Chain_inf> chains;
				chains.reserve(count);
				auto range = up_stop_pos.equal_range(itr->first);
				for (auto itr2 = range.first; itr2 != range.second; itr2++) {
					//chain_v.push_back(serach_connectable_chain_up_stop_lowmom(itr2->second, down_stop_pl));
					serach_connectable_chain_up_stop_lowmom2(itr2->second, down_stop_pl, file_out_path);
				}
			}
		}
		printf("ix=%3d,iy=%3d fin\n", itr->first.first, itr->first.second);

		//all = 0;
		//cand = 0;
		//for (auto itr2 = chain_v.begin(); itr2 != chain_v.end(); itr2++) {
		//	all++;
		//	if ((*itr2).size() > 1)cand++;
		//}
		//printf("ix=%3d,iy=%3d all = %5d, cand = %5d (%4.1lf%%)\n", itr->first.first,itr->first.second,all, cand, cand*100. / all);

		//mfile0::Mfile m_txt = create_mfile(chain_v, m, chainid_map);
		//std::stringstream file_out;
		//file_out << file_out_path << "\\m_upstop_lowmom_"
		//	<< std::setw(3) << std::setfill('0') << itr->first.first << "_"
		//	<< std::setw(3) << std::setfill('0') << itr->first.second << ".all";
		//mfile0::write_mfile(file_out.str(), m_txt,0);
		itr = std::next(itr, count - 1);
	}

}

std::vector<Chain_inf> read_chian_inf(std::string filename) {
	std::vector<Chain_inf> ret;
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	Chain_inf t;
	while (ifs.read((char*)& t, sizeof(Chain_inf))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;

		ret.emplace_back(t);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no data!\n", filename.c_str());
		exit(1);
	}
	return ret;




}
std::multimap<int, Chain_inf*> Select_up_stop_pl(std::vector<Chain_inf> &chain) {
	std::multimap<int, Chain_inf*> ret;

	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr->flg % 10 == 0 && (itr->flg % 100) / 10 == 0) {
			ret.insert(std::make_pair(itr->pl1, &(*itr)));
		}

	}
	return ret;
}
std::multimap<int, Chain_inf*> Select_down_stop_pl(std::vector<Chain_inf> &chain) {
	std::multimap<int, Chain_inf*> ret;

	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if ((itr->flg % 1000)/100 == 0 && (itr->flg % 10000) / 1000 == 0) {
			ret.insert(std::make_pair(itr->pl0, &(*itr)));
		}

	}
	return ret;
}

std::multimap<std::pair<int, int>, Chain_inf*> Select_up_stop_pos(std::vector<Chain_inf> &chain, std::pair<double, double> &pos_min, double hash) {
	std::multimap<std::pair<int, int>, Chain_inf*> ret;

	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr == chain.begin()) {
			pos_min.first = itr->x_up;
			pos_min.second = itr->y_up;
		}
		pos_min.first = std::min(itr->x_up, pos_min.first);
		pos_min.second = std::min(itr->y_up, pos_min.second);
	}

	std::pair<int, int> id;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		//Fe ECCは無視
		if (itr->pl1 <= 17)continue;
		if (itr->flg % 10 == 0 && (itr->flg % 100) / 10 == 0) {
			id.first = (itr->x_up - pos_min.first) / hash;
			id.second = (itr->y_up - pos_min.second) / hash;
			ret.insert(std::make_pair(id, &(*itr)));
		}

	}
	return ret;
}
std::multimap<std::pair<int, int>, Chain_inf*> Select_down_stop_pos(std::vector<Chain_inf> &chain, std::pair<double, double> &pos_min, double hash) {
	std::multimap<std::pair<int, int>, Chain_inf*> ret;

	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if (itr == chain.begin()) {
			pos_min.first = itr->x_down;
			pos_min.second = itr->y_down;
		}
		pos_min.first = std::min(itr->x_down, pos_min.first);
		pos_min.second = std::min(itr->y_down, pos_min.second);
	}

	std::pair<int, int> id;
	for (auto itr = chain.begin(); itr != chain.end(); itr++) {
		if ((itr->flg % 1000) / 100 == 0 && (itr->flg % 10000) / 1000 == 0) {
			id.first = (itr->x_down - pos_min.first) / hash;
			id.second = (itr->y_down - pos_min.second) / hash;
			ret.insert(std::make_pair(id, &(*itr)));
		}

	}
	return ret;
}

void serach_connectable_chain_up_stop_lowmom2(Chain_inf *chain, std::multimap<int, Chain_inf*>&target_chains,std::string file_out_path) {
	std::ofstream ofs_1(file_out_path + "\\md_oa.txt",std::ios::app);
	std::ofstream ofs_2(file_out_path + "\\md_oa_best.txt", std::ios::app);


	matrix_3D::vector_3D dir, pos;
	dir.x = chain->ax_up;
	dir.y = chain->ay_up;
	dir.z = 1;
	pos.x = chain->x_up;
	pos.y = chain->y_up;
	pos.z = chain->z_up;
	int pl = chain->pl1;
#pragma omp parallel for num_threads(11) schedule(dynamic,1)
	for (int t_pl = pl - 4; t_pl <= pl + 6; t_pl++) {
		if (target_chains.count(t_pl) == 0)continue;
		auto range = target_chains.equal_range(t_pl);

		matrix_3D::vector_3D t_dir, t_pos;
		double oa, md, z_range[2], extra[2];
		z_range[0] = pos.z;

		std::vector<Chain_inf> cand;
		for (auto res = range.first; res != range.second; res++) {
			t_dir.x = res->second->ax_down;
			t_dir.y = res->second->ay_down;
			t_dir.z = 1;
			t_pos.x = res->second->x_down;
			t_pos.y = res->second->y_down;
			t_pos.z = res->second->z_down;
			z_range[1] = t_pos.z;

			oa = matrix_3D::opening_angle(dir, t_dir);
			if (chain->chainid == res->second->chainid)continue;
			if (oa > 0.1)continue;
			md = matrix_3D::minimum_distance(pos, t_pos, dir, t_dir, z_range, extra);
			//15度
			if (md > 500)continue;
#pragma omp critical
			{
				ofs_1 << chain->chainid << " " << res->second->chainid << " " << md << " " << oa << std::endl;
				cand.push_back(*(res->second));
			}
		}
		if (cand.size() == 0)continue;
		Chain_inf best = select_best_cand(chain, cand);
		t_dir.x = best.ax_down;
		t_dir.y = best.ay_down;
		t_dir.z = 1;
		t_pos.x = best.x_down;
		t_pos.y = best.y_down;
		t_pos.z = best.z_down;
		z_range[1] = t_pos.z;
		oa = matrix_3D::opening_angle(dir, t_dir);
		md = matrix_3D::minimum_distance(pos, t_pos, dir, t_dir, z_range, extra);
#pragma omp critical
		ofs_2 << chain->chainid << " " << best.chainid << " " << md << " " << oa << std::endl;


	}


}
Chain_inf select_best_cand(Chain_inf *chain, std::vector<Chain_inf>&cand) {

	matrix_3D::vector_3D dir, pos;
	dir.x = chain->ax_up;
	dir.y = chain->ay_up;
	dir.z = 1;
	pos.x = chain->x_up;
	pos.y = chain->y_up;
	pos.z = chain->z_up;
	int pl = chain->pl1;
	matrix_3D::vector_3D t_dir, t_pos;
	double oa, md, z_range[2], extra[2];
	z_range[0] = pos.z;

	Chain_inf ret;
	double md_min;
	for (auto itr = cand.begin(); itr != cand.end(); itr++) {
		std::vector<Chain_inf> cand;
		t_dir.x = itr->ax_down;
		t_dir.y = itr->ay_down;
		t_dir.z = 1;
		t_pos.x = itr->x_down;
		t_pos.y = itr->y_down;
		t_pos.z = itr->z_down;
		z_range[1] = t_pos.z;

		oa = matrix_3D::opening_angle(dir, t_dir);
		md = matrix_3D::minimum_distance(pos, t_pos, dir, t_dir, z_range, extra);
		if (itr == cand.begin()) {
			ret = *itr;
			md_min = md;
		}
		if (md < md_min) {
			ret = *itr;
			md_min = md;

		}
	}
	return ret;

}


std::vector<Chain_inf> serach_connectable_chain_up_stop_lowmom(Chain_inf *chain, std::multimap<int, Chain_inf*>&target_chains) {
	//上流方向を探索
	std::vector<Chain_inf> ret;
	ret.push_back(*chain);

	matrix_3D::vector_3D dir, pos;
	dir.x = chain->ax_up;
	dir.y = chain->ay_up;
	dir.z = 1;
	pos.x = chain->x_up;
	pos.y = chain->y_up;
	pos.z = chain->z_up;
	int pl = chain->pl1;
	matrix_3D::vector_3D t_dir, t_pos;
	double oa, md, z_range[2], extra[2];
	z_range[0] = pos.z;

	for (int t_pl = pl - 4; t_pl <= pl + 6; t_pl++) {
		if (target_chains.count(t_pl) == 0)continue;
		auto range = target_chains.equal_range(t_pl);
		for (auto res = range.first; res != range.second; res++) {

			t_dir.x = res->second->ax_down;
			t_dir.y = res->second->ay_down;
			t_dir.z = 1;
			t_pos.x = res->second->x_down;
			t_pos.y = res->second->y_down;
			t_pos.z = res->second->z_down;
			z_range[1] = t_pos.z;

			oa = matrix_3D::opening_angle(dir, t_dir);
			md = matrix_3D::minimum_distance(pos, t_pos, dir, t_dir, z_range, extra);
			//15度
			if (oa > 0.1)continue;
			if (md > 500)continue;
			ret.push_back(*(res->second));
		}
	}

	//printf("cid=%lld candidate %d\n", chain->chainid, ret.size() - 1);
	return ret;
}
std::vector<Chain_inf> serach_connectable_chain_up_stop_int(Chain_inf *chain, std::multimap<int, Chain_inf*>&target_chains) {
	//上流方向を探索
	std::vector<Chain_inf> ret;
	ret.push_back(*chain);

	matrix_3D::vector_3D dir, pos;
	dir.x = chain->ax_up;
	dir.y = chain->ay_up;
	dir.z = 1;
	pos.x = chain->x_up;
	pos.y = chain->y_up;
	pos.z = chain->z_up;
	int pl = chain->pl1;
	matrix_3D::vector_3D t_dir, t_pos;
	double oa, md, z_range[2], extra[2];
	z_range[0] = pos.z;

	for (int t_pl = pl - 4; t_pl <= pl + 6; t_pl++) {
		if (target_chains.count(t_pl) == 0)continue;
		auto range = target_chains.equal_range(t_pl);
		for (auto res = range.first; res != range.second; res++) {

			t_dir.x = res->second->ax_down;
			t_dir.y = res->second->ay_down;
			t_dir.z = 1;
			t_pos.x = res->second->x_down;
			t_pos.y = res->second->y_down;
			t_pos.z = res->second->z_down;
			z_range[1] = t_pos.z;

			oa = matrix_3D::opening_angle(dir, t_dir);
			md = matrix_3D::minimum_distance(pos, t_pos, dir, t_dir, z_range, extra);
			//if (oa > 3.141592 / 4)continue;
			if (md > 100)continue;
			ret.push_back(*(res->second));
		}
	}

	printf("cid=%lld candidate %d\n", chain->chainid, ret.size() - 1);
	return ret;
}

mfile0::Mfile create_mfile(std::vector<std::vector<Chain_inf>>&chain_inf, mfile1::MFile_minimum &m, std::map<uint64_t, uint64_t> &chainid_map) {
	mfile1::MFile_minimum m_sel;
	m_sel.header = m.header;
	m_sel.info_header = m.info_header;

	uint64_t num;
	
	for (int i = 0; i < chain_inf.size(); i++) {
		if (chain_inf[i].size() == 1)continue;
		for (auto itr = chain_inf[i].begin(); itr != chain_inf[i].end(); itr++) {
			if (chainid_map.count(itr->chainid) == 0) {
				fprintf(stderr, "chainid =%lld not found\n", itr->chainid);
				exit(1);
			}
			num = chainid_map.at(itr->chainid);
			auto basetracks = m.all_basetracks[num];
			for (auto itr2 = basetracks.begin(); itr2 != basetracks.end(); itr2++) {
				itr2->group_id = i;
			}
			m_sel.chains.push_back(m.chains[num]);
			m_sel.all_basetracks.push_back(basetracks);
		}
	}


	mfile0::Mfile ret;
	mfile1::converter(m_sel, ret);
	return ret;
}