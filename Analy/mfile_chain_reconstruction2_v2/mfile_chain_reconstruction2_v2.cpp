//muonのchainに対して、linkletから再度chainを再生成する
//1group 1chainが前提
#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib, "LibL2c-x.lib")
#include <LibL2c-x.h>

#include <filesystem>
#include <set>
#include <netscan_data_types_ui.h>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>


using namespace l2c;
// 自分で作った型
struct Segment {
	int16_t pos;
	int32_t rawid;
	bool operator==(const Segment& rhs) const
	{
		return pos == rhs.pos && rawid == rhs.rawid;
	}
};

// 自分で作った型をunorderdコンテナに入れたいときは、operator== の他に
// 型と同じ名前空間で hash_value 関数を定義

size_t hash_value(const Segment& d)
{
	// 複数の値のハッシュ値を組み合わせてハッシュ値を計算するには、
	// boost::hash_combine を使います。
	size_t h = 0;
	boost::hash_combine(h, d.pos);
	boost::hash_combine(h, d.rawid);
	return h;
}


class Sharing_file {
public:
	int32_t pl, ecc_id, fixedwall_id, trackerwall_id, spotid, zone[2], rawid[2], unix_time, tracker_track_id;
	//spotid:spotA * 100 + spotB
};

class Chain_baselist {
public:
	int64_t groupid;
	std::set<std::pair<int32_t, int64_t>> muon_base;
	std::set<std::pair<int32_t, int64_t>> btset;
	std::set<std::tuple<int, int, int, int>>ltlist;
	std::vector<int32_t> usepos;

	std::vector<Linklet> make_link_list();
	void set_usepos();
};

class linklet_header {
public:
	int pos0, raw0, pos1, raw1;
};

bool sort_M_Base(const mfile0::M_Base &left, const mfile0::M_Base &right) {

	if (left.pos == right.pos) {
		return left.rawid < right.rawid;
	}
	return left.pos < right.pos;
}
bool sort_Group(const mfile0::M_Chain &left, const mfile0::M_Chain &right) {
	if (left.basetracks.begin()->group_id == right.basetracks.begin()->group_id) {
		return left.chain_id < right.chain_id;
	}
	return left.basetracks.begin()->group_id < right.basetracks.begin()->group_id;
}
bool sort_search_list_to_up(const std::pair<int32_t, int64_t>&left, const std::pair<int32_t, int64_t>&right) {
	if (left.first == right.first) {
		return left.second < right.second;
	}
	return left.first < right.first;
}
bool sort_search_list_to_down(const std::pair<int32_t, int64_t>&left, const std::pair<int32_t, int64_t>&right) {
	if (left.first == right.first) {
		return left.second < right.second;
	}
	return left.first > right.first;
}

std::vector<Chain_baselist> read_mfile(std::string filename, mfile0::M_Header &header);
void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs);
void read_linklet_list(std::string filename, boost::unordered_multimap<Segment, Segment> &link_id);
//void read_linklet_list(std::string filename, std::multimap<std::pair<short, int>, std::pair<short, int>> &link_id);
int64_t Linklet_header_num(std::string filename);
void get_basetrack_information(std::map<int, std::map<int, vxx::base_track_t>> &basetracks, std::string file_in_ECC);

void make_group(Chain_baselist &b, boost::unordered_multimap<Segment, Segment> &link_id);
void make_group(Chain_baselist &b, boost::unordered_multimap<Segment, Segment> &link_id, int num);

l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos);
mfile0::M_Base convert_base_format(vxx::base_track_t &base);
std::vector<mfile0::M_Chain> chain_recon(l2c::Cdat&cdat, int group_id, Chain_baselist&b, std::map<std::pair<int32_t, int64_t>, vxx::base_track_t> &base_map);
std::vector<mfile0::M_Chain> chain_recon_signle(int group_id, Chain_baselist&b, std::map<std::pair<int32_t, int64_t>, vxx::base_track_t> &base_map);
void mfile_coordinate_trans(std::vector<mfile0::M_Chain>&c, std::vector<corrmap0::Corrmap> &corr, std::map<int, double> &z_map);
bool judeg_overflow(l2c::Cdat &cdat);
void output_linklet_list(std::string filename, std::vector<Chain_baselist>&chain_list);

int main(int argc, char**argv) {

	if (argc !=7) {
		fprintf(stderr, "usage:prg in-mfile ECC-Area-path mode output-mfile linklet-header-list(bin) output-group-list\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_ECC = argv[2];
	int mode = std::stoi(argv[3]);
	std::string file_out_mfile = argv[4];
	std::string file_in_link = argv[5];

	std::string file_in_corrabs = file_in_ECC + "\\0\\align\\corrmap-abs.lst";
	std::string file_out_group=argv[6];

	if (mode != -1 && mode != 1 && mode != 2 && mode != 3) {
		fprintf(stderr, "mode exception\n");
		fprintf(stderr, "mode=-1 PL133 --> PL001 follow down\n");
		fprintf(stderr, "mode= 1 PL001 --> PL133 scan back\n");
		fprintf(stderr, "mode= 2 PL001 --> PL133 -->PL001\n");
		fprintf(stderr, "mode= 3 PL001 --> PL133 -->PL001--->:endless\n");
		exit(1);
	}

	//corrmap absの読み込み
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrabs, corr_abs);

	//mfileの読み込み
	mfile0::Mfile m;
	std::vector<Chain_baselist> chain_list = read_mfile(file_in_mfile, m.header);
	printf("num of track %d\n", chain_list.size());
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\..\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);
	z_map_correction(z_map, corr_abs);

	//全linkletの読み込み
	boost::unordered_multimap<Segment, Segment> link_id;
	read_linklet_list(file_in_link, link_id);
	std::set<int> event_overflow;
	int count = 0;

	//使うbasetrack群
	std::map<int, std::map<int, vxx::base_track_t>> basetracks;

	for (int i = 0; i < chain_list.size(); i++) {
		if (count % 1 == 0) {
			printf("\r make group %d/%d", count, chain_list.size());
		}
		count++;
		make_group(chain_list[i], link_id,1);
		//使うbasertrackをplごとにfill
		for (auto itr = chain_list[i].btset.begin(); itr != chain_list[i].btset.end(); itr++) {
			vxx::base_track_t base_tmp;
			auto pl_find = basetracks.find(itr->first / 10);
			if (pl_find == basetracks.end()) {
				std::map<int, vxx::base_track_t> base_map_tmp;
				base_map_tmp.insert(std::make_pair(itr->second, base_tmp));
				basetracks.insert(std::make_pair(itr->first / 10, base_map_tmp));
			}
			else {
				pl_find->second.insert(std::make_pair(itr->second, base_tmp));
			}
		}
	}
	printf("\r make group %d/%d done\n", count, chain_list.size());

	////debug
	//for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
	//	printf("PL%03d basetrack num %d\n", itr->first, itr->second.size());
	//}

	//basetrackを拾う
	get_basetrack_information(basetracks, file_in_ECC);
	std::map<std::pair<int32_t, int64_t>, vxx::base_track_t> base_map;
	for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
		int32_t base_pl = itr->first;
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			int64_t rawid = itr2->first;
			base_map.insert(std::make_pair(std::make_pair(base_pl, rawid), itr2->second));
		}
	}

	//chainの生成
	count = 0;
	for (int i = 0; i < chain_list.size(); i++) {
		count++;
		printf("\n eventid = %d  %d/%d\n", chain_list[i].groupid, count, chain_list.size());

		chain_list[i].set_usepos();
		std::vector<Linklet> link = chain_list[i].make_link_list();

		//system("pause");
		//printf("pos %d :", muon.usepos.size());
		//for (auto itr2 = muon.usepos.begin(); itr2 != muon.usepos.end(); itr2++) {
		//	printf(" %4d", *itr2);
		//}
		//printf("\n");
		//for (auto itr2 = link.begin(); itr2 != link.end(); itr2++) {
		//	printf("%4d %4d %10d %10d\n", itr2->pos1, itr2->pos2, itr2->id1, itr2->id2);
		//}
		if (chain_list[i].ltlist.size() == 0) {
			std::vector<mfile0::M_Chain> group = chain_recon_signle(chain_list[i].groupid, chain_list[i], base_map);
			mfile_coordinate_trans(group, corr_abs, z_map);
			for (auto itr = group.begin(); itr != group.end(); itr++) {
				m.chains.push_back(*itr);
			}
		}
		else {
			l2c::Cdat cdat = l2c_x(chain_list[i].btset, link, chain_list[i].usepos);
			if (judeg_overflow(cdat)) {
				event_overflow.insert(chain_list[i].groupid);
			}

			std::vector<mfile0::M_Chain> group = chain_recon(cdat, chain_list[i].groupid, chain_list[i], base_map);
			mfile_coordinate_trans(group, corr_abs, z_map);
			for (auto itr = group.begin(); itr != group.end(); itr++) {
				m.chains.push_back(*itr);
			}
		}
	}

	std::ofstream ofs("overflow.txt");
	ofs << "overflow " << event_overflow.size() << std::endl;
	for (auto itr = event_overflow.begin(); itr != event_overflow.end(); itr++) {
		ofs << *itr << std::endl;
	}
	ofs.close();

	output_linklet_list(file_out_group, chain_list);

	sort(m.chains.begin(), m.chains.end(), sort_Group);
	mfile0::write_mfile(file_out_mfile, m);
	printf("prg fin\n");
	std::quick_exit(EXIT_SUCCESS);

}

std::vector<Chain_baselist> read_mfile(std::string filename, mfile0::M_Header &header) {
	mfile0::Mfile m;
	mfile1::read_mfile_extension(filename, m);
	header = m.header;

	std::multimap<int64_t, mfile0::M_Base> group;
	std::map<std::pair<int64_t, int>, mfile0::M_Base> group_map;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			group_map.insert(std::make_pair(std::make_pair(itr2->group_id, itr2->rawid), *itr2));
		}
	}
	for (auto itr = group_map.begin(); itr != group_map.end(); itr++) {
		group.insert(std::make_pair(itr->first.first, itr->second));
	}
	std::vector<Chain_baselist> ret;
	int count = 0;
	for (auto itr = group.begin(); itr != group.end(); itr++) {
		count = group.count(itr->first);

		Chain_baselist c;
		c.groupid = itr->second.group_id;
		auto range = group.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			c.btset.insert(std::make_pair(res->second.pos, res->second.rawid));
			if (res->second.flg_i[0] == 1) {
				c.muon_base.insert(std::make_pair(res->second.pos / 10, res->second.rawid));
			}
		}
		ret.push_back(c);

		itr = std::next(itr, count - 1);
	}

	//今あるchainをlinklet化しておく
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		if (itr->btset.size() == 1)continue;

		for (auto itr2 = itr->btset.begin(); itr2 != std::prev(itr->btset.end(), 1); itr2++) {
			auto itr3 = std::next(itr2, 1);
			//pl0 < pl1 になるようにltlistに入れる
			if (itr2->first < itr3->first) {
				itr->ltlist.insert(std::make_tuple(
					itr2->first, itr3->first,
					itr2->second, itr3->second));
			}
			else {
				itr->ltlist.insert(std::make_tuple(
					itr3->first, itr2->first,
					itr3->second, itr2->second));
			}
		}
	}


	return ret;
}

void z_map_correction(std::map<int, double> &z_map, std::vector<corrmap0::Corrmap> &corr_abs) {

	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		auto res = z_map.find(pl);
		if (res == z_map.end())continue;
		res->second += itr->dz;
	}

}

void read_linklet_list(std::string filename, boost::unordered_multimap<Segment, Segment> &link_id) {
	//std::unordered_multimap

	int64_t link_num = Linklet_header_num(filename);
	//link_id.reserve(link_num * 2);

	FILE*fp_in;
	if ((fp_in = fopen(filename.c_str(), "rb")) == NULL) {
		printf("%s file not open!\n", filename.c_str());
		exit(EXIT_FAILURE);
	}

	const int read_num_max = 10000000;
	//std::vector<linklet_header> link;
	//link.reserve(read_num_max);
	static linklet_header link[read_num_max];
	Segment seg0, seg1;
	std::vector<std::pair<Segment,Segment>> seg_pair_v;
	seg_pair_v.reserve(read_num_max*2);

	int64_t  now = 0;
	int read_num;
	bool flg = true;
	while (flg) {
		if (link_num - now == read_num_max) {
			read_num = read_num_max;
			flg = false;
		}
		else if (link_num - now < read_num_max) {
			read_num = link_num - now;
			flg = false;
		}
		else {
			read_num = read_num_max;
		}
		int num = fread(link, sizeof(linklet_header), read_num, fp_in);
		if (num != read_num) {
			fprintf(stderr, "read file error\n");
			exit(1);
		}
		seg_pair_v.clear();
		for (int i = 0; i < read_num; i++) {
			seg0.pos = link[i].pos0 + 1;
			seg0.rawid = link[i].raw0;
			seg1.pos = link[i].pos1 + 1;
			seg1.rawid = link[i].raw1;
			seg_pair_v.push_back(std::make_pair(seg0, seg1));
			seg_pair_v.push_back(std::make_pair(seg1, seg0));
		}
		link_id.insert(seg_pair_v.begin(), seg_pair_v.end());

		//for (int i = 0; i < read_num; i++) {
		//	seg0.pos = link[i].pos0 + 1;
		//	seg0.rawid = link[i].raw0;
		//	seg1.pos = link[i].pos1 + 1;
		//	seg1.rawid = link[i].raw1;
		//	//printf("%d %d %d %d\n", link[i].pos0, link[i].raw0, link[i].pos1, link[i].raw1);
		//	if (seg0.pos / 10 > 20)continue;
		//	link_id.insert(std::make_pair(seg0, seg1));
		//	link_id.insert(std::make_pair(seg1, seg0));
		//	//link_id.
		//}
		printf("\r read fin %12lld/%12lld (%4.1lf%%)", now, link_num, now*100. / link_num);

		now += read_num;
	}
	printf("\r read fin %12lld/%12lld (%4.1lf%%)\n", now, link_num, now*100. / link_num);
	fclose(fp_in);

}
int64_t Linklet_header_num(std::string filename) {
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	return size2 / sizeof(linklet_header);
}



void make_group(Chain_baselist &b, boost::unordered_multimap<Segment, Segment> &link_id) {
	//幅優先探索
	std::map<std::pair<int32_t, int64_t>, bool> search_list;

	std::set<std::pair<int32_t, int64_t>> btset_add;
	for (auto itr = b.btset.begin(); itr != b.btset.end(); itr++) {
		btset_add.insert(*itr);
	}
	Segment seg;

	bool flg = true;
	while (flg) {
		for (auto itr = btset_add.begin(); itr != btset_add.end(); itr++) {
			search_list.insert(std::make_pair(*itr, false));
		}
		flg = false;
		for (auto itr = search_list.begin(); itr != search_list.end(); itr++) {
			if (itr->second)continue;
			seg.pos = itr->first.first;
			seg.rawid = itr->first.second;
			if (link_id.count(seg) == 0) {
				itr->second = true;
				continue;
			}
			flg = true;
			auto range = link_id.equal_range(seg);
			for (auto res = range.first; res != range.second; res++) {
				b.btset.insert(std::make_pair(res->second.pos, res->second.rawid));
				btset_add.insert(std::make_pair(res->second.pos, res->second.rawid));
				//pl0 < pl1 になるようにltlistに入れる
				if (res->first.pos < res->second.pos) {
					b.ltlist.insert(std::make_tuple(
						res->first.pos, res->second.pos,
						res->first.rawid, res->second.rawid));
				}
				else {
					b.ltlist.insert(std::make_tuple(
						res->second.pos, res->first.pos,
						res->second.rawid, res->first.rawid));
				}
			}
			itr->second = true;
		}
	}
}

void make_group(Chain_baselist &b, boost::unordered_multimap<Segment, Segment> &link_id, int num) {
	//往復探索
	std::list<std::pair<int32_t, int64_t>> search_list;

	std::set<std::pair<int32_t, int64_t>> btset_add;
	Segment seg;

	bool flg = true;
	for (int i = 0; i < num; i++) {
		for (int j = 0; j <= 1; j++) {
			//j=0 下流-->上流
			//j=1 上流-->下流
			search_list.clear();
			btset_add.clear();
			for (auto itr = b.btset.begin(); itr != b.btset.end(); itr++) {
				search_list.push_back(*itr);
			}
			for (auto itr = b.btset.begin(); itr != b.btset.end(); itr++) {
				btset_add.insert(*itr);
			}

			if (j == 0)search_list.sort(sort_search_list_to_up);
			else if (j == 1)search_list.sort(sort_search_list_to_down);

			for (auto itr = search_list.begin(); itr != search_list.end(); itr++) {
				seg.pos = itr->first;
				seg.rawid = itr->second;
				if (link_id.count(seg) == 0) {
					continue;
				}
				auto range = link_id.equal_range(seg);
				for (auto res = range.first; res != range.second; res++) {
					//pl0 < pl1 のみltlistに入れる
					if (j == 0) {
						if (res->first.pos > res->second.pos)continue;
						b.btset.insert(std::make_pair(res->second.pos, res->second.rawid));
						b.ltlist.insert(std::make_tuple(
							res->first.pos, res->second.pos,
							res->first.rawid, res->second.rawid));

						if (btset_add.count(std::make_pair(res->second.pos, res->second.rawid)) == 1)continue;
						btset_add.insert(std::make_pair(res->second.pos, res->second.rawid));
						//追加したbasetrackをsearch_listに追加
						for (auto itr2 = itr; itr2 != search_list.end(); itr2++) {
							if (res->second.pos == itr2->first) {
								//挿入
								search_list.insert(itr2, std::make_pair(res->second.pos, res->second.rawid));
								break;
							}
							if (std::next(itr2, 1) == search_list.end()) {
								search_list.insert(search_list.end(), std::make_pair(res->second.pos, res->second.rawid));
								break;
							}
						}
					}
					else if (j == 1) {
						if (res->first.pos < res->second.pos)continue;
						b.btset.insert(std::make_pair(res->second.pos, res->second.rawid));
						b.ltlist.insert(std::make_tuple(
							res->second.pos, res->first.pos,
							res->second.rawid, res->first.rawid));

						if (btset_add.count(std::make_pair(res->second.pos, res->second.rawid)) == 1)continue;
						btset_add.insert(std::make_pair(res->second.pos, res->second.rawid));
						//追加したbasetrackをsearch_listに追加
						for (auto itr2 = itr; itr2 != search_list.end(); itr2++) {
							if (res->second.pos == itr2->first) {
								//挿入
								search_list.insert(itr2, std::make_pair(res->second.pos, res->second.rawid));
								break;
							}
							if (std::next(itr2, 1) == search_list.end()) {
								search_list.insert(search_list.end(), std::make_pair(res->second.pos, res->second.rawid));
								break;
							}
						}
					}
				}
			}
			//debug
			//system("pause");
			//printf("\n j=%d\n", j);
			//for (auto itr = search_list.begin(); itr != search_list.end(); itr++) {
			//	printf("%4d %d\n", itr->first, itr->second);
			//}
		}
	}
}


l2c::Cdat l2c_x(std::set<std::pair<int32_t, int64_t>> &btset, std::vector<Linklet> &ltlist, std::vector<int32_t> &usepos) {
	try
	{

		//std::vector<int32_t> usepos = { 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500 };
		size_t possize = usepos.size();

		//l2c関数本体を呼び出す。
		//出力されるCdatは基本的にl2c-xと同等のものになっているはずだが、若干の違いがある。
		//1. upperlimを超過したGroupは、Chainは持たず、全BaseTrackの情報だけを持った状態で出力される。
		//2. upperlim超過のGroupも含め、属す全BaseTrackの情報を持っている。
		//またplate枚数は最大64枚。変更するにはLibL2c-xのリビルドを要する。
		//デフォルトでoutput_isolated_linkletはfalse、upperlimは無効になっている。useposは与えないとエラーになる。
		l2c::Cdat cdat = l2c::MakeCdat(ltlist, opt::usepos = usepos, opt::upperlim = 10000000, opt::output_isolated_linklet = true);
		return cdat;


		//中身を出力する。
		size_t grsize = cdat.GetNumOfGroups();
		for (size_t grid = 0; grid < grsize; ++grid)
		{
			const Group& gr = cdat.GetGroup(grid);
			size_t chsize = gr.GetNumOfChains();
			if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
			int32_t spl = gr.GetStartPL();
			int32_t epl = gr.GetEndPL();

			//l2c-xのPLは一般に使われるPLとは意味が異なり、useposの中で"何番目のposであるか"を意味する。
			//LibL2c-xもl2c-xの仕様に倣うことにした（ただしl2c-xが1始まりなのに対してこちらは0始まり）。
			//例えば今回は、pos==310〜500の20枚でChain Groupを作成しているが、
			//GetStartPL()の戻り値が"3"であったとすると、これはこのGroupがusepos[3]すなわち380から始まっているという意味になる。

			fprintf(stdout, "Group ID:%-9llu nchain:%-7llu start:%-4d end:%-4d\n", gr.GetID(), chsize, spl, epl);
			if (gr.IsOverUpperLim())
			{
				//upperlimを超過している場合、chainの情報はない。
				//ただしchainの本数はGetNumOfChainsで正しく取得できる。
				//また属すBaseTrackの情報は保持しているので、GetBaseTracksで全BaseTrackを取得できる。
				fprintf(stdout, "over upperlim. nchain:%-9lld\n", chsize);
				continue;
			}
			for (size_t ich = 0; ich < chsize; ++ich)
			{
				Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
				int64_t chid = ch.GetID();
				int32_t nseg = ch.GetNSeg();
				int32_t spl = ch.GetStartPL();
				int32_t epl = ch.GetEndPL();
				fprintf(stdout, "    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d\n", chid, nseg, spl, epl);

				for (size_t pl = 0; pl < possize; ++pl)
				{
					//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
					//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
					BaseTrackID bt = ch.GetBaseTrack(pl);
					if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
					int32_t btpl = bt.GetPL();
					int64_t btid = bt.GetRawID();
					//btplとplは厳密に一致しなければおかしい。
					if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
					if (btset.find(std::make_pair(usepos[btpl], btid)) == btset.end())
					{
						//エラーチェック。
						//Chain内の全てのBaseTrackはbtset（ファイルから読み込まれた全BaseTrack）に必ず含まれているはず。
						//含まれていなければエラー。
						throw std::exception("BaseTrack is not found in btset.");
					}
					fprintf(stdout, "        pl:%-3d pos:%-4d rawid:%-9lld\n", btpl, usepos[pl], btid);
				}
			}

		}
	}
	catch (const std::exception& e)
	{
		fprintf(stderr, "%s\n", e.what());
	}

}
std::vector<Linklet> Chain_baselist::make_link_list() {
	std::vector<Linklet> ret;
	ret.reserve(ltlist.size());
	for (auto itr = ltlist.begin(); itr != ltlist.end(); itr++) {
		ret.emplace_back(std::get<0>(*itr), std::get<1>(*itr), std::get<2>(*itr), std::get<3>(*itr));
	}
	return ret;
}
void Chain_baselist::set_usepos() {
	std::set<int> pos_set;
	for (auto itr = btset.begin(); itr != btset.end(); itr++) {
		pos_set.insert(itr->first);
	}
	usepos.clear();
	for (auto itr = pos_set.begin(); itr != pos_set.end(); itr++) {
		usepos.push_back(*itr);
	}
}

void get_basetrack_information(std::map<int, std::map<int, vxx::base_track_t>> &basetracks, std::string file_in_ECC) {
	int count = 0;
	std::vector<int> pl_list;
	for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
		pl_list.push_back(itr->first);
	}
#pragma omp parallel for schedule(dynamic) num_threads(10)
	for (int i = 0; i < pl_list.size(); i++) {
		//for (auto itr = basetracks.begin(); itr != basetracks.end(); itr++) {
		auto itr = basetracks.find(pl_list[i]);
		int pl = itr->first;
#pragma omp atomic
		count++;
#pragma omp critical
		printf("now read %d/%d\n", count, pl_list.size());

		std::stringstream file_read_base;
		file_read_base << file_in_ECC << "\\PL" << std::setw(3) << std::setfill('0') << pl
			<< "\\b" << std::setw(3) << std::setfill('0') << pl << ".sel.cor.vxx";
		vxx::BvxxReader br;
		std::vector<vxx::base_track_t>base = br.ReadAll(file_read_base.str(), pl, 0);
		std::map<int, vxx::base_track_t> base_map;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_map.insert(std::make_pair(itr->rawid, *itr));
		}

		for (auto itr_b = itr->second.begin(); itr_b != itr->second.end(); itr_b++) {
			if (base_map.count(itr_b->first) == 0) {
				printf("PL%03d rawid = %d not found\n", pl, itr_b->first);
				exit(1);
			}
			itr_b->second = base_map.at(itr_b->first);
		}
	}
	printf("now read %d/%d done\n", count, pl_list.size());

}

bool judeg_overflow(l2c::Cdat &cdat) {
	size_t grsize = cdat.GetNumOfGroups();
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		if (gr.IsOverUpperLim())return true;
	}
	return false;
}
std::vector<mfile0::M_Chain> chain_recon_signle(int group_id, Chain_baselist&b, std::map<std::pair<int32_t, int64_t>, vxx::base_track_t> &base_map) {
	std::multimap<int32_t, int64_t> bt_map;

	int count = 0;
	std::vector<mfile0::M_Chain> ret;
	for (auto itr = b.btset.begin(); itr != b.btset.end(); itr++) {
		mfile0::M_Chain c;
		auto res = base_map.find(std::make_pair(itr->first / 10, itr->second));
		if (res == base_map.end()) {
			printf("PL%03d rawid=%lld not found\n", itr->first / 10, itr->second);
			continue;
		}
		mfile0::M_Base m_b = convert_base_format(res->second);
		m_b.group_id = group_id;
		if (b.muon_base.count(std::make_pair(itr->first / 10, itr->second)) == 1) {
			m_b.flg_i[0] = 1;
		}
		c.basetracks.push_back(m_b);
		sort(c.basetracks.begin(), c.basetracks.end(), sort_M_Base);
		c.nseg = c.basetracks.size();
		c.pos0 = c.basetracks.begin()->pos;
		c.pos1 = c.basetracks.rbegin()->pos;
		c.chain_id = group_id + count;
		ret.push_back(c);
		count++;
	}
	return ret;
}
std::vector<mfile0::M_Chain> chain_recon(l2c::Cdat&cdat, int group_id, Chain_baselist&b, std::map<std::pair<int32_t, int64_t>, vxx::base_track_t> &base_map) {
	std::multimap<int32_t, int64_t> bt_map;
	;

	for (auto itr = b.btset.begin(); itr != b.btset.end(); itr++) {
		bt_map.insert(std::make_pair(itr->first / 10, itr->second));
	}

	////basetrackの情報の取得
	int pl, count;

	////chainの組み立て

	//中身を出力する。
	std::vector<mfile0::M_Chain> ret;
	size_t grsize = cdat.GetNumOfGroups();
	size_t possize = b.usepos.size();
	for (size_t grid = 0; grid < grsize; ++grid)
	{
		const Group& gr = cdat.GetGroup(grid);
		size_t chsize = gr.GetNumOfChains();
		if (grid != gr.GetID()) throw std::exception("grid != gr.GetID().");//gridはgr.GetID()の戻り値と基本的に等しいはずである。
		int32_t spl = gr.GetStartPL();
		int32_t epl = gr.GetEndPL();

		//l2c-xのPLは一般に使われるPLとは意味が異なり、useposの中で"何番目のposであるか"を意味する。
		//LibL2c-xもl2c-xの仕様に倣うことにした（ただしl2c-xが1始まりなのに対してこちらは0始まり）。
		//例えば今回は、pos==310〜500の20枚でChain Groupを作成しているが、
		//GetStartPL()の戻り値が"3"であったとすると、これはこのGroupがusepos[3]すなわち380から始まっているという意味になる。

		fprintf(stdout, "Group ID:%-9llu nchain:%-7llu start:%-4d end:%-4d\n", gr.GetID(), chsize, spl, epl);
		if (gr.IsOverUpperLim())
		{
			//upperlimを超過している場合、chainの情報はない。
			//ただしchainの本数はGetNumOfChainsで正しく取得できる。
			//また属すBaseTrackの情報は保持しているので、GetBaseTracksで全BaseTrackを取得できる。
			fprintf(stdout, "over upperlim. nchain:%-9lld\n", chsize);
			auto all_base = gr.GetBaseTracks();
			mfile0::M_Chain c;
			for (auto itr = all_base.begin(); itr != all_base.end(); itr++) {
				int64_t raw;
				int32_t pl;
				pl = b.usepos[itr->GetPL()] / 10;
				raw = itr->GetRawID();
				auto res = base_map.find(std::make_pair(pl, raw));
				if (res == base_map.end()) {
					printf("PL%03d rawid=%lld not found\n", pl, raw);
					continue;
				}
				mfile0::M_Base m_b = convert_base_format(res->second);
				m_b.group_id = group_id;
				if (b.muon_base.count(std::make_pair(pl, raw)) == 1) {
					m_b.flg_i[0] = 1;
				}
				c.basetracks.push_back(m_b);
			}
			sort(c.basetracks.begin(), c.basetracks.end(), sort_M_Base);
			c.nseg = c.basetracks.size();
			c.pos0 = c.basetracks.begin()->pos;
			c.pos1 = c.basetracks.rbegin()->pos;
			c.chain_id = group_id;
			ret.push_back(c);
		}
		else {
			for (size_t ich = 0; ich < chsize; ++ich)
			{
				Chain ch = gr.GetChain(ich);//仕様上、Chainオブジェクトは一時変数として戻ってくる。
				int64_t chid = ch.GetID();
				int32_t nseg = ch.GetNSeg();
				int32_t spl = ch.GetStartPL();
				int32_t epl = ch.GetEndPL();
				if (ich % 1000 == 0) {
					fprintf(stdout, "\r    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d", chid, nseg, spl, epl);
				}
				if (ich + 1 == chsize) {
					fprintf(stdout, "\r    Chain ID:%-9lld nseg:%-3d start:%-4d end:%-4d\n", chid, nseg, spl, epl);
				}
				mfile0::M_Chain c;
				for (size_t pl = 0; pl < possize; ++pl)
				{
					//ここで用いるplも上と同様に、useposの中で何番目のposかを意味する。
					//pl == 5だとすると、usepos中で（0から数えて）5番目、つまりpos==400のBaseTrack情報が返ってくる。
					BaseTrackID bt = ch.GetBaseTrack(pl);
					if (bt.IsEmpty()) continue;//IsEmptyがtrueのときは、このBaseTrackは歯抜け。
					int32_t btpl = bt.GetPL();
					int64_t btid = bt.GetRawID();
					//btplとplは厳密に一致しなければおかしい。
					if (btpl != pl) throw std::exception("BaseTrackID::PL != pl.");
					if (b.btset.find(std::make_pair(b.usepos[btpl], btid)) == b.btset.end())
					{
						//エラーチェック。
						//Chain内の全てのBaseTrackはbtset（ファイルから読み込まれた全BaseTrack）に必ず含まれているはず。
						//含まれていなければエラー。
						throw std::exception("BaseTrack is not found in btset.");
					}
					auto res = base_map.find(std::make_pair(b.usepos[btpl] / 10, btid));
					if (res == base_map.end()) {
						printf("PL%03d rawid=%lld not found\n", b.usepos[btpl] / 10, btid);
						continue;
					}
					mfile0::M_Base m_b = convert_base_format(res->second);
					m_b.group_id = group_id;
					if (b.muon_base.count(std::make_pair(b.usepos[btpl] / 10, btid)) == 1) {
						m_b.flg_i[0] = 1;
					}
					c.basetracks.push_back(m_b);
				}
				sort(c.basetracks.begin(), c.basetracks.end(), sort_M_Base);
				c.nseg = c.basetracks.size();
				c.pos0 = c.basetracks.begin()->pos;
				c.pos1 = c.basetracks.rbegin()->pos;
				c.chain_id = group_id + ich;
				ret.push_back(c);
			}
		}
	}
	return ret;
}
mfile0::M_Base convert_base_format(vxx::base_track_t &base) {
	mfile0::M_Base ret;
	ret.ax = base.ax;
	ret.ay = base.ay;
	ret.group_id = 0;
	ret.ph = base.m[0].ph + base.m[1].ph;
	ret.pos = base.pl * 10 + 1;
	ret.rawid = base.rawid;
	ret.x = base.x;
	ret.y = base.y;
	ret.z = 0;




	ret.flg_d[0] = 0;
	ret.flg_d[1] = 0;
	ret.flg_i[0] = 0;
	ret.flg_i[1] = 0;
	ret.flg_i[2] = 0;
	ret.flg_i[3] = 0;

	return ret;

}

void mfile_coordinate_trans(std::vector<mfile0::M_Chain>&c, std::vector<corrmap0::Corrmap> &corr, std::map<int, double> &z_map) {
	std::map<int, corrmap0::Corrmap> corr_map;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		corr_map.insert(std::make_pair(itr->pos[0] / 10, *itr));
	}

	double tmp_x, tmp_y;
	for (auto itr_c = c.begin(); itr_c != c.end(); itr_c++) {
		for (auto itr_b = itr_c->basetracks.begin(); itr_b != itr_c->basetracks.end(); itr_b++) {
			int pl = itr_b->pos / 10;
			if (corr_map.count(pl) + z_map.count(pl) != 2) {
				printf("corrmap found flg=%d", corr_map.count(pl));
				printf("z map found flg=%d", z_map.count(pl));
			}
			itr_b->z = z_map.at(pl);
			auto param = corr_map.at(pl);
			tmp_x = itr_b->x;
			tmp_y = itr_b->y;
			itr_b->x = tmp_x * param.position[0] + tmp_y * param.position[1] + param.position[4];
			itr_b->y = tmp_x * param.position[2] + tmp_y * param.position[3] + param.position[5];

			tmp_x = itr_b->ax;
			tmp_y = itr_b->ay;
			itr_b->ax = tmp_x * param.angle[0] + tmp_y * param.angle[1] + param.angle[4];
			itr_b->ay = tmp_x * param.angle[2] + tmp_y * param.angle[3] + param.angle[5];
		}
	}
}

void output_linklet_list(std::string filename, std::vector<Chain_baselist>&chain_list) {

	std::ofstream ofs(filename);
	double weight = 1;
	for (auto itr = chain_list.begin(); itr != chain_list.end(); itr++) {
		ofs << std::fixed << std::right
			<< std::setw(3) << std::setprecision(0) << itr->groupid << " "
			<< std::setw(3) << std::setprecision(0) << itr->muon_base.size() << " "
			<< std::setw(3) << std::setprecision(0) << itr->ltlist.size() << std::endl;
		std::vector<Linklet> link = itr->make_link_list();
		if (itr->muon_base.size() > 0) {
			for (auto itr2 = itr->muon_base.begin(); itr2 != itr->muon_base.end(); itr2++) {
				ofs << std::fixed << std::right
					<< std::setw(4) << std::setprecision(0) << itr2->first << " "
					<< std::setw(12) << std::setprecision(0) << itr2->second << std::endl;
			}
		}
		for (auto itr2 = link.begin(); itr2 != link.end(); itr2++) {
			ofs << std::fixed << std::right
				<< std::setw(4) << std::setprecision(0) << itr2->pos1/10 << " "
				<< std::setw(12) << std::setprecision(0) << itr2->id1 << " "
				<< std::setw(4) << std::setprecision(0) << itr2->pos2/10 << " "
				<< std::setw(12) << std::setprecision(0) << itr2->id2 << " "
				<< std::setw(4) << std::setprecision(3) << weight << std::endl;
		}
	}
}