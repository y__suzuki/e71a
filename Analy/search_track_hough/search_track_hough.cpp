#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>



class GrainInformation {
public:
	int id, face, ix, iy, layer, pixelnum, brightness_sum9;
	double x, y, z;
};
class TrackInformation {
public:
	double ax, ay, x, y, z;
};

class SearchTrackInformation {
public:
	double ax, ay, x, y, z;
	int hit;
};

bool sort_track_hit(const SearchTrackInformation&left, const SearchTrackInformation&right) {
	return left.hit > right.hit;


}
std::vector<GrainInformation> read_grain_inf(std::string filename, int &face);
std::vector<GrainInformation> cut_grain(std::vector<GrainInformation>&gr, TrackInformation &tr, double radius);
void search_track(std::vector<GrainInformation>&gr, TrackInformation &tr);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:prg filename output-file\n");
		exit(1);
	}
	std::string file_in_grain = argv[1];
	std::string file_out_result = argv[2];

	int face;
	std::vector<GrainInformation> grain = read_grain_inf(file_in_grain, face);
	//721            478800000   2568423    250067   0.2764   0.2220    64828.5   162566.5 - 117908  0  0  0  0 0.0000 0.0000
	//721            478800010   3257133    320424  -0.3501  -0.1709    64517.9   162367.4    -117908  0  0  0  0 0.0000 0.0000 

	TrackInformation tr;
	tr.ax = -0.3501;
	tr.ay = -0.1709;
	tr.x = 64517.9;
	tr.y = 162367.4;
	tr.z = -117908;

	std::vector<GrainInformation> grain_sel = cut_grain(grain, tr, 30);
	//HoughResult res= search_track(grain_sel,tr);
	search_track(grain_sel, tr);
}
std::vector<GrainInformation> read_grain_inf(std::string filename, int &face) {
	std::ifstream ifs(filename);
	std::vector<GrainInformation> ret;
	GrainInformation gr;
	int count = 0;
	while (ifs >> gr.face >> gr.ix >> gr.iy >> gr.layer >> gr.x >> gr.y >> gr.z >> gr.pixelnum >> gr.brightness_sum9) {
		if (count % 100000 == 0) {
			printf("\r grain read %d", count);
		}
		face = gr.face;
		gr.id = count;
		count++;
		ret.push_back(gr);
	}
	printf("\r grain read fin %d\n", ret.size());
	return ret;

}
std::vector<GrainInformation> cut_grain(std::vector<GrainInformation>&gr,TrackInformation &tr, double radius) {
	std::vector<GrainInformation> ret;
	double tmp_x, tmp_y;
	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		tmp_x = tr.x + tr.ax*(itr->z - tr.z);
		tmp_y = tr.y + tr.ay*(itr->z - tr.z);
		if (fabs(itr->x - tmp_x) > radius)continue;
		if (fabs(itr->y - tmp_y) > radius)continue;
		//if (pow(itr->x - tmp_x, 2) + pow(itr->y - tmp_y, 2) > radius*radius)continue;
		ret.push_back(*itr);
	}
	std::ofstream ofs("out_cut_grain.txt");
	for (auto itr = ret.begin(); itr != ret.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->face << " "
			<< std::setw(3) << std::setprecision(0) << itr->ix << " "
			<< std::setw(3) << std::setprecision(0) << itr->iy << " "
			<< std::setw(10) << std::setprecision(0) << itr->layer << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(10) << std::setprecision(1) << itr->z << " "
			<< std::setw(10) << std::setprecision(0) << itr->pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->brightness_sum9 << std::endl;
	}
	ofs.close();
	return ret;
}
void search_track(std::vector<GrainInformation>&gr, TrackInformation &tr) {
	double radius = 30;
	double thick = 70;
	double xmin, xmax, ymin, ymax;
	double z_border;
	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		if (itr == gr.begin()) {
			z_border = itr->z;
		}
		z_border = std::max(itr->z, z_border);
	}
	xmin = tr.x + tr.ax*(z_border - tr.z) - radius;
	xmax = tr.x + tr.ax*(z_border - tr.z) + radius;
	ymin = tr.y + tr.ay*(z_border - tr.z) - radius;
	ymax = tr.y + tr.ay*(z_border - tr.z) + radius;

	double search_radius = 1;
	double step = search_radius / 2;

	int x_search_num = (xmax - xmin) / step;
	int y_search_num = (ymax - ymin) / step;

	double d_tan = radius / thick;
	double ax_min = tan(atan(tr.ax) - atan(d_tan/2));
	double ax_max = tan(atan(tr.ax) + atan(d_tan/2));
	double ay_min = tan(atan(tr.ay) - atan(d_tan/2));
	double ay_max = tan(atan(tr.ay) + atan(d_tan/2));
	

	//double ax_step = (ax_max - ax_min) / ax_search_num;
	//double ay_step = (ay_max - ay_min) / ay_search_num;
	double ax_step = search_radius / thick;
	double ay_step = search_radius / thick;
	int ax_search_num = (ax_max - ax_min) / ax_step;
	int ay_search_num = (ay_max - ay_min) / ay_step;

	printf("x  step=%g num=%d\n", step, x_search_num);
	printf("y  step=%g num=%d\n", step, y_search_num);
	printf("ax step=%g num=%d\n", ax_step, ax_search_num);
	printf("ay step=%g num=%d\n", ay_step, ay_search_num);

	double x, y, ax, ay;
	double tmp_x, tmp_y;
	int hit_num = 0;
	int64_t count = 0, all = x_search_num * y_search_num* ax_search_num*ay_search_num, hit_count = 0;
	std::vector< SearchTrackInformation> ret;
	for (int ix = 0; ix < x_search_num; ix++) {
		for (int iy = 0; iy < y_search_num; iy++) {
			for (int iax = 0; iax < ax_search_num; iax++) {
				for (int iay = 0; iay < ay_search_num; iay++) {
					if (count % 100000 == 0) {
						printf("\r track search %lld / %lld(%4.1lf%%)", count, all, count*100. / all);
					}
					count++;
					hit_num = 0;
					x = xmin + step * ix;
					y = ymin + step * iy;
					ax = ax_min + ax_step * iax;
					ay = ay_min + ay_step * iay;

					for (auto itr = gr.begin(); itr != gr.end(); itr++) {
						tmp_x = x + ax * (itr->z - z_border);
						tmp_y = y + ay * (itr->z - z_border);
						if (pow(itr->x - tmp_x, 2) + pow(itr->y - tmp_y, 2) > search_radius*search_radius)continue;
						hit_num++;
					}
					if (hit_num < 5)continue;
					hit_count++;
					SearchTrackInformation s_tr;
					s_tr.ax = ax;
					s_tr.ay = ay;
					s_tr.x = x;
					s_tr.y = y;
					s_tr.z = z_border;
					s_tr.hit = hit_num;
					ret.push_back(s_tr);
				}
			}
		}
	}
	printf("\r track search %lld / %lld(%4.1lf%%)\n", count, all, count*100. / all);

	printf("hit count %lld\n", hit_count);

	//peakをとるx-yの抽出
	sort(ret.begin(), ret.end(), sort_track_hit);
	SearchTrackInformation s_tr = *(ret.begin());
	std::vector<GrainInformation>hit_gr;
	for (auto itr = gr.begin(); itr != gr.end(); itr++) {
		tmp_x = s_tr.x + s_tr.ax * (itr->z - s_tr.z);
		tmp_y = s_tr.y + s_tr.ay * (itr->z - s_tr.z);
		if (pow(itr->x - tmp_x, 2) + pow(itr->y - tmp_y, 2) > search_radius*search_radius)continue;
		hit_gr.push_back(*itr);
	}

	std::ofstream ofs("out_cut_grain_hit.txt");
	for (auto itr = hit_gr.begin(); itr != hit_gr.end(); itr++) {
		ofs << std::right << std::fixed
			<< std::setw(4) << std::setprecision(0) << itr->face << " "
			<< std::setw(3) << std::setprecision(0) << itr->ix << " "
			<< std::setw(3) << std::setprecision(0) << itr->iy << " "
			<< std::setw(10) << std::setprecision(0) << itr->layer << " "
			<< std::setw(10) << std::setprecision(1) << itr->x << " "
			<< std::setw(10) << std::setprecision(1) << itr->y << " "
			<< std::setw(10) << std::setprecision(1) << itr->z << " "
			<< std::setw(10) << std::setprecision(0) << itr->pixelnum << " "
			<< std::setw(10) << std::setprecision(0) << itr->brightness_sum9 << std::endl;
	}
	ofs.close();

	//-->ax-ayを再度振る









	//std::ofstream ofs("out_result.txt");
	//for (auto itr = ret.begin(); itr != ret.end(); itr++) {

	//	ofs << std::right << std::fixed
	//		<< std::setw(8) << std::setprecision(4) << itr->ax << " "
	//		<< std::setw(8) << std::setprecision(4) << itr->ay << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->x << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->y << " "
	//		<< std::setw(10) << std::setprecision(1) << itr->z << " "
	//		<< std::setw(5) << std::setprecision(0) << itr->hit << std::endl;

	//}
}
