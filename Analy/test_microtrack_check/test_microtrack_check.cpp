#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#include <map>
#include <fstream>
#include <ios>     // std::left, std::right
#include <iomanip> 
std::vector<std::pair<vxx::micro_track_t, vxx::micro_track_t>> make_micro_pair(std::vector<vxx::micro_track_t> &micro1, std::vector<vxx::micro_track_t> &micro2);
void output(std::string filename, std::vector<std::pair<vxx::micro_track_t, vxx::micro_track_t>> p);


void main(int argc, char ** argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:prg in-fvxx in-fvxx pos out-txt\n");
		exit(1);
	}
	std::string file_in_fvxx1 = argv[1];
	std::string file_in_fvxx2 = argv[2];
	int pos = std::stoi(argv[3]);
	std::string file_out = argv[4];

	vxx::FvxxReader fr;
	std::vector<vxx::micro_track_t> micro1 = fr.ReadAll(file_in_fvxx1, pos, 0);
	std::vector<vxx::micro_track_t> micro2 = fr.ReadAll(file_in_fvxx2, pos, 0);

	std::vector<std::pair<vxx::micro_track_t, vxx::micro_track_t>> m_pair = make_micro_pair(micro1, micro2);
	output(file_out, m_pair);

}
std::vector<std::pair<vxx::micro_track_t, vxx::micro_track_t>> make_micro_pair(std::vector<vxx::micro_track_t> &micro1, std::vector<vxx::micro_track_t> &micro2) {
	std::map <std::tuple<int, int, int>, vxx::micro_track_t> micro_map;
	for (auto itr = micro2.begin(); itr != micro2.end(); itr++) {
		micro_map.insert(std::make_pair(std::make_tuple(itr->col, itr->row, itr->isg), *itr));
	}
	std::vector<std::pair<vxx::micro_track_t, vxx::micro_track_t>> ret;
	for (auto itr = micro1.begin(); itr != micro1.end(); itr++) {
		auto res = micro_map.find(std::make_tuple(itr->col, itr->row, itr->isg));
		if (res == micro_map.end()) {
			fprintf(stderr, "track not found %d %d %d\n", itr->col, itr->row, itr->isg);
		}
		else {
			ret.push_back(std::make_pair(*itr, res->second));
		}
	}
	return ret;



}
void output(std::string filename, std::vector<std::pair<vxx::micro_track_t, vxx::micro_track_t>> p) {
	std::ofstream ofs(filename);
	uint32_t ShotID;
	int ViewID = 0, ImagerID = 0;
	int NumberOfImager = 72;
	for (auto itr = p.begin(); itr != p.end(); itr++) {
		ShotID = ((uint32_t)(uint16_t)itr->first.row << 16) | ((uint32_t)(uint16_t)itr->first.col);
		ViewID = ShotID / NumberOfImager;
		ImagerID = ShotID % NumberOfImager;
		ofs << std::right << std::fixed
			<< std::setw(7) << std::setprecision(0) << ViewID << " "
			<< std::setw(3) << std::setprecision(0) << ImagerID << " "
			<< std::setw(7) << std::setprecision(0) << itr->first.ph << " "
			<< std::setw(7) << std::setprecision(4) << itr->first.ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->first.ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->first.x << " "
			<< std::setw(8) << std::setprecision(1) << itr->first.y << " "
			<< std::setw(8) << std::setprecision(1) << itr->first.z << " "
			<< std::setw(7) << std::setprecision(0) << itr->second.ph << " "
			<< std::setw(7) << std::setprecision(4) << itr->second.ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->second.ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.x << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.y << " "
			<< std::setw(8) << std::setprecision(1) << itr->second.z << std::endl;
	}

}