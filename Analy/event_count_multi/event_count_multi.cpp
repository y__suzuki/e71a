#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class particle_inf {
public:
	int groupid, chainid,pid;
	double angle, pb, vph, proton_likelihood, pion_likelihood, likelihood_ratio;
};

std::vector< particle_inf > Read_file(std::string filename);
std::map<std::pair<int, int>, int>count_multi(std::vector< particle_inf >&p_inf);
void output_multi(std::string filename, std::map<std::pair<int, int>, int>&count);

int main(int argc,char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}

	std::string file_in_event = argv[1];
	std::string file_out = argv[2];
	std::vector< particle_inf > p_inf=Read_file(file_in_event);

	std::map<std::pair<int, int>, int>count=count_multi(p_inf);
	output_multi(file_out, count);

}
std::vector< particle_inf > Read_file(std::string filename) {
	std::ifstream ifs(filename);
	std::vector< particle_inf > ret;
	particle_inf p_tmp;
	while (ifs >> p_tmp.groupid >> p_tmp.chainid >> p_tmp.angle >> p_tmp.pb >> p_tmp.vph
		>> p_tmp.proton_likelihood >> p_tmp.pion_likelihood >> p_tmp.likelihood_ratio >> p_tmp.pid) {
		ret.push_back(p_tmp);
	}
	return ret;
}

std::map<std::pair<int, int>, int>count_multi(std::vector< particle_inf >&p_inf) {
	std::multimap<int, particle_inf> group;
	for (auto &p : p_inf) {
		group.insert(std::make_pair(p.groupid, p));
	}
	std::map<std::pair<int, int>, int> ret;
	for (auto itr = group.begin(); itr != group.end(); itr++) {
		int num_p = 0;
		int num_pi = 0;
		auto range = group.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.pid == 2212)num_p++;
			else if (res->second.pid == 211)num_pi++;
		}
		auto res = ret.insert(std::make_pair(std::make_pair(num_p, num_pi), 1));
		if (!res.second) {
			res.first->second++;
		}

		itr = std::next(itr, group.count(itr->first) - 1);
	}
	return ret;

}


void output_multi(std::string filename, std::map<std::pair<int, int>, int>&count) {
	std::ofstream ofs(filename);
	ofs << "  p  pi num" << std::endl;

	for (auto itr = count.begin(); itr!= count.end(); itr++) {
		ofs << std::fixed << std::right
			<< std::setw(3) << std::setprecision(0) << itr->first.first << " "
			<< std::setw(3) << std::setprecision(0) << itr->first.second << " "
			<< std::setw(3) << std::setprecision(0) << itr->second << std::endl;
	}
}