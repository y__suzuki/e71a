#define _CRT_SECURE_NO_WARNINGS 

#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <set>
#include<vector>




class PID_track {
public:
	uint64_t chainid;
	short pos, trackingid, sensorid;
	int rawid, col, row, isg;
	double  pb, angle, vph, pixelnum, pid;

};

std::vector<PID_track> read_PID_track(std::string filename, double pb_cut);
std::map<std::tuple<int, int, int, int>, std::vector<double>> track_divide0(std::vector<PID_track>&t);
std::map<std::tuple<int, int, int, int>, std::vector<double>> track_divide1(std::vector<PID_track>&t);
std::map<std::tuple<int, int, int, int>, std::vector<double>> track_divide2(std::vector<PID_track>&t);
int sensor_id(int sensor);
int angle_id(double angle);
int tracking_id(int track);

void output0(std::tuple<int, int, int, int> id, std::vector<double>&vph, std::ofstream &ofs);
void output1(std::tuple<int, int, int, int> id, std::vector<double>&vph, std::ofstream &ofs);
void output2(std::tuple<int, int, int, int> id, std::vector<double>&vph, std::ofstream &ofs);
std::map<int, int>vph_divide(std::vector<double>&vph, int nbin, double min, double max);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage:prg filename outfile mode\n");
		fprintf(stderr, "mode=0:sort angleid\n");
		fprintf(stderr, "mode=1:sort sensorid\n");
		fprintf(stderr, "mode=2:sort angleid together sensorid\n");
		exit(1);
	}

	std::string file_in = argv[1];
	std::string file_out = argv[2];
	int mode = std::stoi(argv[3]);
	std::vector<PID_track> track = read_PID_track(file_in, 1000);
	std::map<std::tuple<int, int, int, int>, std::vector<double>> t_map;
	if (mode == 0)t_map = track_divide0(track);
	if (mode == 1)t_map = track_divide1(track);
	if (mode == 2)t_map = track_divide2(track);
	std::ofstream ofs(file_out);
	for (auto itr = t_map.begin(); itr != t_map.end(); itr++) {
		if (mode == 0) {
			printf("\r now pos=%5d trackingID=%2d sensorID=%2d", std::get<0>(itr->first), std::get<1>(itr->first), std::get<2>(itr->first));
			output0(itr->first, itr->second, ofs);
		}
		if (mode == 1) {
			printf("\r now pos=%5d trackingID=%2d angleID=%2d", std::get<0>(itr->first), std::get<1>(itr->first), std::get<2>(itr->first));
			output1(itr->first, itr->second, ofs);
		}
		if (mode == 2) {
			printf("\r now pos=%5d trackingID=%2d sensorID=%2d", std::get<0>(itr->first), std::get<1>(itr->first), std::get<2>(itr->first));
			output2(itr->first, itr->second, ofs);
		}

	}
	if (mode == 0)printf("\r now pos=%5d trackingID=%2d sensorID=%2d\n", std::get<0>(t_map.rbegin()->first), std::get<1>(t_map.rbegin()->first), std::get<2>(t_map.rbegin()->first));
	if (mode == 1)printf("\r now pos=%5d trackingID=%2d angleID=%2d\n", std::get<0>(t_map.rbegin()->first), std::get<1>(t_map.rbegin()->first), std::get<2>(t_map.rbegin()->first));
	if (mode == 2)printf("\r now pos=%5d trackingID=%2d sensorID=%2d\n", std::get<0>(t_map.rbegin()->first), std::get<1>(t_map.rbegin()->first), std::get<2>(t_map.rbegin()->first));

}



std::vector<PID_track> read_PID_track(std::string filename,double pb_cut) {
	std::ifstream ifs(filename.c_str(), std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t size1 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (GB > 0) {
		std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
	}
	else {
		std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
	}
	int64_t count = 0;
	PID_track t;
	std::vector<PID_track> ret;
	while (ifs.read((char*)& t, sizeof(PID_track))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			size1 = nowpos - begpos;
			printf("\r Fill ... %4.1lf%%", size1 * 100. / size2);
		}
		count++;
		if (pb_cut > t.pb)continue;
		ret.push_back(t);
	}
	size1 = eofpos - begpos;
	printf("\r Fill ... %4.1lf%%\n", size1 * 100. / size2);
	if (count == 0) {
		fprintf(stderr, "%s no track!\n", filename.c_str());
		exit(1);
	}
	return ret;
}
//pos,trackingid,sensorid,angleidで分ける
std::map<std::tuple<int, int, int, int>, std::vector<double>> track_divide0(std::vector<PID_track>&t) {
	std::multimap < std::tuple<int, int, int, int>, double> divide;
	std::set<int> pos_set;
	uint64_t count = 0, all = t.size();
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r now divide track %llu/%llu(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		divide.insert(std::make_pair(std::make_tuple(itr->pos, itr->trackingid, itr->sensorid, angle_id(itr->angle)), itr->vph));
		pos_set.insert(itr->pos);
	}
	fprintf(stderr, "\r now divide track %llu/%llu(%4.1lf%%)\n", count, all, count*100. / all);
	std::map<std::tuple<int, int, int, int>, std::vector<double>> ret;
	std::vector<double> vph_v;

	count = 0;
	for (auto itr = divide.begin(); itr != divide.end(); itr++) {
		auto count_v = divide.count(itr->first);
		auto range = divide.equal_range(itr->first);
		vph_v.clear();
		vph_v.reserve(count_v);
		fprintf(stderr, "\r now divide track2 %llu/%llu(%4.1lf%%)", count, all, count*100. / all);
		count += count_v;

		for (auto res = range.first; res != range.second; res++) {
			vph_v.push_back(res->second);
		}
		ret.insert(std::make_pair(itr->first, vph_v));
		itr = std::next(itr, count_v - 1);
	}
	fprintf(stderr, "\r now divide track2 %llu/%llu(%4.1lf%%)\n", count, all, count*100. / all);

	vph_v.clear();
	for (auto ipos = pos_set.begin(); ipos != pos_set.end(); ipos++) {
		for (int tid = 0; tid < 4; tid++) {
			for (int sid = 0; sid < 72; sid++) {
				for (int aid = 0; aid < 24; aid++) {
					ret.insert(std::make_pair(std::make_tuple(*ipos, tid, sid, aid), vph_v));
				}
			}
		}
	}
	return ret;

}
//pos,trackingid,angleid,sensoridで分ける
std::map<std::tuple<int, int, int, int>, std::vector<double>> track_divide1(std::vector<PID_track>&t) {
	std::multimap < std::tuple<int, int, int, int>, double> divide;
	std::set<int> pos_set;
	uint64_t count = 0, all = t.size();
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r now divide track %llu/%llu(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		divide.insert(std::make_pair(std::make_tuple(itr->pos, itr->trackingid, angle_id(itr->angle), itr->sensorid), itr->vph));
		pos_set.insert(itr->pos);
	}
	fprintf(stderr, "\r now divide track %llu/%llu(%4.1lf%%)\n", count, all, count*100. / all);
	std::map<std::tuple<int, int, int, int>, std::vector<double>> ret;
	std::vector<double> vph_v;

	count = 0;
	for (auto itr = divide.begin(); itr != divide.end(); itr++) {
		auto count_v = divide.count(itr->first);
		auto range = divide.equal_range(itr->first);
		vph_v.clear();
		vph_v.reserve(count_v);
		fprintf(stderr, "\r now divide track2 %llu/%llu(%4.1lf%%)", count, all, count*100. / all);
		count += count_v;

		for (auto res = range.first; res != range.second; res++) {
			vph_v.push_back(res->second);
		}
		ret.insert(std::make_pair(itr->first, vph_v));
		itr = std::next(itr, count_v - 1);
	}
	fprintf(stderr, "\r now divide track2 %llu/%llu(%4.1lf%%)\n", count, all, count*100. / all);

	vph_v.clear();
	for (auto ipos = pos_set.begin(); ipos != pos_set.end(); ipos++) {
		for (int tid = 0; tid < 4; tid++) {
			for (int sid = 0; sid < 72; sid++) {
				for (int aid = 0; aid < 24; aid++) {
					ret.insert(std::make_pair(std::make_tuple(*ipos, tid, aid, sid), vph_v));
				}
			}
		}
	}
	return ret;

}
//pos,trackingid,sensorid,angleidで分ける+sensorid まとめる+tracking 0,1まとめる
std::map<std::tuple<int, int, int, int>, std::vector<double>> track_divide2(std::vector<PID_track>&t) {
	std::multimap < std::tuple<int, int, int, int>, double> divide;
	std::set<int> pos_set;
	uint64_t count = 0, all = t.size();
	for (auto itr = t.begin(); itr != t.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r now divide track %llu/%llu(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		divide.insert(std::make_pair(std::make_tuple(itr->pos, tracking_id(itr->trackingid), sensor_id(itr->sensorid), angle_id(itr->angle)), itr->vph));
		pos_set.insert(itr->pos);
	}
	fprintf(stderr, "\r now divide track %llu/%llu(%4.1lf%%)\n", count, all, count*100. / all);
	std::map<std::tuple<int, int, int, int>, std::vector<double>> ret;
	std::vector<double> vph_v;

	count = 0;
	for (auto itr = divide.begin(); itr != divide.end(); itr++) {
		auto count_v = divide.count(itr->first);
		auto range = divide.equal_range(itr->first);
		vph_v.clear();
		vph_v.reserve(count_v);
		fprintf(stderr, "\r now divide track2 %llu/%llu(%4.1lf%%)", count, all, count*100. / all);
		count += count_v;

		for (auto res = range.first; res != range.second; res++) {
			vph_v.push_back(res->second);
		}
		ret.insert(std::make_pair(itr->first, vph_v));
		itr = std::next(itr, count_v - 1);
	}
	fprintf(stderr, "\r now divide track2 %llu/%llu(%4.1lf%%)\n", count, all, count*100. / all);

	vph_v.clear();
	for (auto ipos = pos_set.begin(); ipos != pos_set.end(); ipos++) {
		for (int tid = 0; tid < 3; tid++) {
			for (int sid = 0; sid < 2; sid++) {
				for (int aid = 0; aid < 24; aid++) {
					ret.insert(std::make_pair(std::make_tuple(*ipos, tid, sid, aid), vph_v));
				}
			}
		}
	}
	return ret;

}

int angle_id(double angle) {
	if (angle < 0.6) {
		return (int)(angle * 10);
	}
	else if (angle < 1.8) {
		return int((angle - 0.6) * 10) / 2 + 6;
	}
	else if (angle < 3.6) {
		return int((angle - 1.8) * 10) / 3 + 12;
	}
	else if (angle < 6.0) {
		return int((angle - 3.6) * 10) / 4 + 18;
	}
	else {
		return 24;
	}
}
int sensor_id(int sensor) {
	if ((24 <= sensor && sensor < 36) || sensor == 52)return 1;
	return 0;
}
int tracking_id(int track) {
	if (track == 0 || track == 1)return 0;
	else if (track == 2)return 1;
	return 2;
}
void output0(std::tuple<int, int, int, int> id, std::vector<double>&vph, std::ofstream &ofs) {

	std::map<int, int> bin;
	int nbin;
	double min, max;
	if (std::get<3>(id) < 1) {
		nbin = 10;
		min = 0;
		max = 200;
	}
	else if (std::get<3>(id) < 3) {
		nbin = 10;
		min = 0;
		max = 100;
	}
	else if (std::get<3>(id) < 6) {
		nbin = 25;
		min = 0;
		max = 100;
	}
	else if (std::get<3>(id) < 12) {
		nbin = 50;
		min = 0;
		max = 100;
	}
	else if (std::get<3>(id) < 18) {
		nbin = 25;
		min = 0;
		max = 100;
	}
	else if (std::get<3>(id) < 24) {
		nbin = 20;
		min = 0;
		max = 100;
	}
	else {
		nbin = 1;
		min = 0;
		max = 0;
	}
	bin = vph_divide(vph, nbin, min, max);
	ofs << std::right << std::fixed
		<< std::setw(3) << std::setprecision(0) << std::get<0>(id) << " "
		<< std::setw(3) << std::setprecision(0) << std::get<1>(id) << " "
		<< std::setw(3) << std::setprecision(0) << std::get<2>(id) << " "
		<< std::setw(3) << std::setprecision(0) << std::get<3>(id) << " "
		<< std::setw(4) << std::setprecision(0) << nbin << " "
		<< std::setw(3) << std::setprecision(0) << min << " "
		<< std::setw(4) << std::setprecision(0) << max << " "
		<< std::setw(4) << std::setprecision(0) << bin.size() << std::endl;
	if (bin.size() == 0)return;
	for (auto itr = bin.begin(); itr != bin.end(); itr++) {
		ofs << std::setw(3) << std::setprecision(0) << itr->first << " "
			<< std::setw(10) << std::setprecision(0) << itr->second << " "
			<< std::endl;
	}
}
void output1(std::tuple<int, int, int, int> id, std::vector<double>&vph, std::ofstream &ofs) {

	std::map<int, int> bin;
	int nbin;
	double min, max;
	if (std::get<2>(id) < 1) {
		nbin = 10;
		min = 0;
		max = 200;
	}
	else if (std::get<2>(id) < 3) {
		nbin = 10;
		min = 0;
		max = 100;
	}
	else if (std::get<2>(id) < 6) {
		nbin = 25;
		min = 0;
		max = 100;
	}
	else if (std::get<2>(id) < 12) {
		nbin = 50;
		min = 0;
		max = 100;
	}
	else if (std::get<2>(id) < 18) {
		nbin = 25;
		min = 0;
		max = 100;
	}
	else if (std::get<2>(id) < 24) {
		nbin = 20;
		min = 0;
		max = 100;
	}
	else {
		nbin = 1;
		min = 0;
		max = 0;
	}
	bin = vph_divide(vph, nbin, min, max);
	ofs << std::right << std::fixed
		<< std::setw(3) << std::setprecision(0) << std::get<0>(id) << " "
		<< std::setw(3) << std::setprecision(0) << std::get<1>(id) << " "
		<< std::setw(3) << std::setprecision(0) << std::get<3>(id) << " "
		<< std::setw(3) << std::setprecision(0) << std::get<2>(id) << " "
		<< std::setw(4) << std::setprecision(0) << nbin << " "
		<< std::setw(3) << std::setprecision(0) << min << " "
		<< std::setw(4) << std::setprecision(0) << max << " "
		<< std::setw(4) << std::setprecision(0) << bin.size() << std::endl;
	if (bin.size() == 0)return;
	for (auto itr = bin.begin(); itr != bin.end(); itr++) {
		ofs << std::setw(3) << std::setprecision(0) << itr->first << " "
			<< std::setw(10) << std::setprecision(0) << itr->second << " "
			<< std::endl;
	}
}
void output2(std::tuple<int, int, int, int> id, std::vector<double>&vph, std::ofstream &ofs) {

	std::map<int, int> bin;
	int nbin;
	double min, max;
	if (std::get<3>(id) < 1) {
		if (std::get<2>(id) == 0) {
			nbin = 100;
			min = 0;
			max = 200;
		}
		else {
			nbin = 50;
			min = 0;
			max = 200;
		}
	}
	else if (std::get<3>(id) < 3) {
		if (std::get<2>(id) == 0) {
			nbin = 50;
			min = 0;
			max = 100;
		}
		else {
			nbin = 25;
			min = 0;
			max = 100;
		}
	}
	else if (std::get<3>(id) < 6) {
		if (std::get<2>(id) == 0) {
			nbin = 100;
			min = 0;
			max = 100;
		}
		else {
			nbin = 50;
			min = 0;
			max = 100;
		}
	}
	else if (std::get<3>(id) < 12) {
		nbin = 100;
		min = 0;
		max = 100;
	}
	else if (std::get<3>(id) < 18) {
		nbin = 100;
		min = 0;
		max = 100;
	}
	else if (std::get<3>(id) < 24) {
		nbin = 100;
		min = 0;
		max = 100;
	}
	else {
		nbin = 1;
		min = 0;
		max = 0;
	}
	bin = vph_divide(vph, nbin, min, max);
	ofs << std::right << std::fixed
		<< std::setw(3) << std::setprecision(0) << std::get<0>(id) << " "
		<< std::setw(3) << std::setprecision(0) << std::get<1>(id) << " "
		<< std::setw(3) << std::setprecision(0) << std::get<2>(id) << " "
		<< std::setw(3) << std::setprecision(0) << std::get<3>(id) << " "
		<< std::setw(4) << std::setprecision(0) << nbin << " "
		<< std::setw(3) << std::setprecision(0) << min << " "
		<< std::setw(4) << std::setprecision(0) << max << " "
		<< std::setw(4) << std::setprecision(0) << bin.size() << std::endl;
	if (bin.size() == 0)return;
	for (auto itr = bin.begin(); itr != bin.end(); itr++) {
		ofs << std::setw(3) << std::setprecision(0) << itr->first << " "
			<< std::setw(10) << std::setprecision(0) << itr->second << " "
			<< std::endl;
	}
}

std::map<int, int>vph_divide(std::vector<double>&vph, int nbin, double min, double max) {
	std::map<int, int> ret;
	double width = (max - min) / nbin;
	int ibin = 0;
	for (auto itr = vph.begin(); itr != vph.end(); itr++) {
		if (*itr < min)continue;
		if (*itr >= max)continue;
		ibin = int((*itr - min) / width);
		auto res = ret.insert(std::make_pair(ibin, 1));
		if (!res.second)res.first->second++;
	}

	return ret;
}