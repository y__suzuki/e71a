#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>

mfile0::Mfile classify_muon_track(std::vector<Sharing_file::Sharing_file > &sf, mfile0::Mfile &ecc, mfile0::Mfile &shifter);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:filename ECC-mfile Shifter-mfile out-mfile\n");
		exit(1);
	}
	std::string file_in_sf = argv[1];
	std::string file_in_ecc = argv[2];
	std::string file_in_shifter= argv[3];
	std::string file_out_mfile = argv[4];

	std::vector<Sharing_file::Sharing_file > sf = Sharing_file::Read_sharing_file_bin(file_in_sf);
	mfile0::Mfile ecc, shifter;
	mfile1::read_mfile_extension(file_in_ecc, ecc);
	mfile1::read_mfile_extension(file_in_shifter,shifter);

	mfile0::Mfile m_all = classify_muon_track(sf, ecc, shifter);

	mfile0::write_mfile(file_out_mfile, m_all);
}
mfile0::Mfile classify_muon_track(std::vector<Sharing_file::Sharing_file > &sf, mfile0::Mfile &ecc, mfile0::Mfile &shifter) {

	std::multimap<std::pair<int, int>, Sharing_file::Sharing_file> sf_map;
	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		sf_map.insert(std::make_pair(std::make_pair(itr->unix_time, itr->tracker_track_id), *itr));
	}
	std::map<int, mfile0::M_Chain> ecc_chain, shifter_chain;
	for (auto itr = ecc.chains.begin(); itr != ecc.chains.end(); itr++) {
		ecc_chain.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}
	for (auto itr = shifter.chains.begin(); itr != shifter.chains.end(); itr++) {
		shifter_chain.insert(std::make_pair(itr->basetracks.begin()->group_id, *itr));
	}

	mfile0::Mfile ret;
	ret.header = ecc.header;

	for (auto itr = sf_map.begin(); itr != sf_map.end(); itr++) {
		auto range = sf_map.equal_range(itr->first);
		int unix_time = itr->first.first;
		int tracker_trackid = itr->first.second;

		std::vector<Sharing_file::Sharing_file > sf_v;
		for (auto res = range.first; res != range.second; res++) {
			sf_v.push_back(res->second);
		}
		int chain_ranking = 1;
		for (auto &s : sf_v) {
			if (ecc_chain.count(s.eventid) == 1) {
				mfile0::M_Chain c = ecc_chain.at(s.eventid);
				for (auto &b : c.basetracks) {
					b.flg_i[0] = s.eventid;
					//b.flg_i[1] = 
					b.flg_i[2] = s.track_type;
					b.flg_i[3] = s.ecc_track_type;
					b.group_id = s.unix_time;
				}
				c.chain_id = 0;
				ret.chains.push_back(c);
			}
			if (shifter_chain.count(s.eventid) == 1) {
				mfile0::M_Chain c = shifter_chain.at(s.eventid);
				for (auto &b : c.basetracks) {
					int pl = b.pos / 10;
					b.flg_i[0] = s.eventid;
					b.flg_i[1] = chain_ranking;
					b.flg_i[2] = s.charge;
					b.flg_i[3] = s.babymind_nplane;
					//ISS
					if (3 <= pl && pl <= 4) {
						b.flg_d[0] = 0;
						b.flg_d[1] = s.chi2_shifter[0];
					}
					//OSS
					else if (pl == 11) {
						b.flg_d[0] = s.chi2_shifter[0];
						b.flg_d[1] = s.chi2_shifter[1];
					}
					//FSS
					else if (pl == 41) {
						b.flg_d[0] = s.chi2_shifter[1];
						b.flg_d[1] = s.chi2_shifter[2];
					}
					//TSS
					else if (pl == 61) {
						b.flg_d[0] = s.chi2_shifter[2];
						b.flg_d[1] = s.chi2_shifter[3];
					}
					b.group_id = s.unix_time;
				}
				chain_ranking++;
				c.chain_id = 1;
				ret.chains.push_back(c);
			}
		}
		itr = std::next(itr, sf_map.count(itr->first) - 1);
	}

	return ret;
}