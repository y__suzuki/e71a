#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class PID_inf {
public:
	int groupid, chainid, pid;
	double angle,  pb, vph_ave;
};
class Trend_inf {
public:
	int groupid, chainid,direction;
	double sigma;

};
std::vector<PID_inf> Read_PID(std::string filename);
std::vector<Trend_inf> Read_trend(std::string filename);
std::vector < Trend_inf> Trend_add_direction(std::vector<Trend_inf>& t_inf, std::vector<PID_inf> &pid_inf, std::vector<Momentum_recon::Event_information> &momch);
void output_sigma(std::string filename, std::vector<Trend_inf>& t_inf);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:filename\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_in_pid= argv[2];
	std::string file_in_trend = argv[3];

	std::string file_out_trend = argv[4];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	std::vector<PID_inf> pid_inf= Read_PID(file_in_pid);
	std::vector<Trend_inf> t_inf = Read_trend(file_in_trend);


	t_inf = Trend_add_direction(t_inf, pid_inf, momch);

	output_sigma(file_out_trend, t_inf);
}
std::vector<PID_inf> Read_PID(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<PID_inf> ret;
	PID_inf p_inf;
	while (ifs >> p_inf.groupid >> p_inf.chainid >> p_inf.pid >> p_inf.angle >> p_inf.pb >> p_inf.vph_ave) {
		ret.push_back(p_inf);


	}
	return ret;

}

std::vector<Trend_inf> Read_trend(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<Trend_inf> ret;
	Trend_inf t_inf;
	while (ifs >> t_inf.groupid >> t_inf.chainid >> t_inf.direction >> t_inf.sigma) {
		ret.push_back(t_inf);
	}
	return ret;
	
}

std::vector < Trend_inf> Trend_add_direction(std::vector<Trend_inf>& t_inf, std::vector<PID_inf> &pid_inf, std::vector<Momentum_recon::Event_information> &momch) {
	std::vector < Trend_inf> ret;


	for (auto itr = t_inf.begin(); itr != t_inf.end(); itr++) {
		int groupid = itr->groupid;
		int chainid = itr->chainid;

		int pid = 0;
		double pb;
		for (auto &p : pid_inf) {
			if (p.groupid == groupid && p.chainid == chainid) {
				pid = p.pid;
				pb = p.pb;
			}
		}
		if (pid != 2212)continue;
		if (pb > 1000)continue;
		int direction = 0;
		int vertex_pl = 0;
		for (auto &ev : momch) {
			if (ev.groupid != groupid)continue;
			for (auto &c : ev.chains) {
				if (c.chainid == 0 || c.particle_flg == 13) {
					vertex_pl = c.base.rbegin()->pl;
				}
			}
			for (auto &c : ev.chains) {
				if (c.chainid != chainid)continue;
				if (c.base.size() < 6)continue;
				if (c.base.rbegin()->pl <= vertex_pl) {
					direction = 1;
				}
				else if (c.base.begin()->pl > vertex_pl) {
					direction = -1;
				}
			}
		}
		if (direction == 1) {
			if (itr->direction == 0)itr->direction = 1;
			else if (itr->direction == 1)itr->direction =-1;
			ret.push_back(*itr);
		}
		else if (direction == -1) {
			if (itr->direction == 0)itr->direction = -1;
			else if (itr->direction == 1)itr->direction = 1;
			ret.push_back(*itr);
		}

	}

	return ret;
}

void output_sigma(std::string filename, std::vector<Trend_inf>& t_inf) {
	std::ofstream ofs(filename);

	for (int i = 0; i < t_inf.size(); i++) {
		for (int j = i + 1; j < t_inf.size(); j++) {
			if (t_inf[i].chainid != t_inf[j].chainid)continue;
			if (t_inf[i].groupid != t_inf[j].groupid)continue;
			if (t_inf[i].direction == 1) {
				ofs << t_inf[i].groupid << " " << t_inf[i].chainid << " " << t_inf[i].sigma << " " << t_inf[j].sigma << std::endl;
			}
			else if (t_inf[j].direction == 1) {
				ofs << t_inf[i].groupid << " " << t_inf[i].chainid << " " << t_inf[j].sigma << " " << t_inf[i].sigma << std::endl;
			}
		}
	}
}