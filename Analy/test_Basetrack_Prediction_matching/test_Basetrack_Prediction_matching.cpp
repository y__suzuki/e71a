#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#include <set>

struct base_prediction {
	int pl, hit, hit_new;
	double x, y, ax, ay;
};


void base_matching(std::vector<base_prediction>&base_ori, std::vector<netscan::base_track_t>& base_all);
netscan::base_track_t select_best_base(netscan::base_track_t base, std::vector<netscan::base_track_t>base_all);
void read_prediction(std::string filename, std::vector<base_prediction> &base, int pl);
void count_prediction(std::vector<base_prediction> base);
void write_prediction(std::string filename, std::vector<base_prediction> base);
void base_trans(std::vector<netscan::base_track_t>&base, corrmap0::Corrmap param);

int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg in-bvxx-pred in-bvxx_all pl corrmap-abs out-file-list\n");
		exit(1);
	}
	std::string file_in_base_pred= argv[1];
	std::string file_in_base_all = argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_in_corr = argv[4];
	std::string file_out_list = argv[5];

	std::vector<base_prediction> base_pred;
	std::vector<netscan::base_track_t> base_all;
	read_prediction(file_in_base_pred, base_pred, pl);
	printf("predction %d\n", base_pred.size());
	netscan::read_basetrack_extension(file_in_base_all, base_all, pl, 0);

	std::vector<corrmap0::Corrmap> corr;
	corrmap0::read_cormap(file_in_corr, corr);
	corrmap0::Corrmap param;
	bool flg = false;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		if (itr->pos[0] / 10 == pl) {
			param = *itr;
			flg = true;
		}
	}
	if (!flg) {
		fprintf(stderr, "not found corrmap pl=%d\n", pl);
		exit(1);
	}
	base_trans(base_all, param);
	base_matching(base_pred, base_all);
	count_prediction(base_pred);
	write_prediction(file_out_list, base_pred);
}
void read_prediction(std::string filename, std::vector<base_prediction> &base, int pl) {
	std::ifstream ifs(filename);

	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		if (std::stoi(str_v[0]) != pl)continue;
		base_prediction tmp_b;
		tmp_b.pl = std::stoi(str_v[0]);
		tmp_b.ax = std::stod(str_v[1]);
		tmp_b.ay = std::stod(str_v[2]);
		tmp_b.x = std::stod(str_v[3]);
		tmp_b.y = std::stod(str_v[4]);
		tmp_b.hit = std::stoi(str_v[5]);
		tmp_b.hit_new = 0;
		base.push_back(tmp_b);
	}

}
void base_trans(std::vector<netscan::base_track_t>&base, corrmap0::Corrmap param) {
	double tmp_x, tmp_y;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		tmp_x = itr->x;
		tmp_y = itr->y;
		itr->x = param.position[0] * tmp_x + param.position[1] * tmp_y + param.position[4];
		itr->y = param.position[2] * tmp_x + param.position[3] * tmp_y + param.position[5];

		tmp_x = itr->ax;
		tmp_y = itr->ay;
		itr->ax = param.angle[0] * tmp_x + param.angle[1] * tmp_y + param.angle[4];
		itr->ay = param.angle[2] * tmp_x + param.angle[3] * tmp_y + param.angle[5];
	}
}
void base_matching(std::vector<base_prediction>&base_ori, std::vector<netscan::base_track_t>& base_all) {
	double x_min, y_min;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		if (itr == base_all.begin()) {
			x_min = itr->x;
			y_min = itr->y;
		}
		x_min = std::min(x_min, itr->x);
		y_min = std::min(y_min, itr->y);
	}
	double hash = 1000;
	std::multimap < std::pair<int, int>, netscan::base_track_t* > base_map;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		std::pair<int, int>id;
		id.first = (itr->x - x_min) / hash;
		id.second = (itr->y - y_min) / hash;
		base_map.insert(std::make_pair(id, &(*itr)));
	}

	for (int i = 0; i < base_ori.size(); i++) {
		std::vector<netscan::base_track_t> match_cand;
		double angle, all_pos[2], all_ang[2];
		angle = sqrt(base_ori[i].ax*base_ori[i].ax + base_ori[i].ay*base_ori[i].ay);
		all_pos[0] = 20 + angle * 20;
		all_pos[1] = 20;
		all_ang[0] = 0.05 + angle * 0.1;
		all_ang[1] = 0.05;
		int range[2][2];
		range[0][0] = (base_ori[i].x - x_min - all_pos[0]) / hash;
		range[0][1] = (base_ori[i].x - x_min + all_pos[0]) / hash;
		range[1][0] = (base_ori[i].y - y_min - all_pos[0]) / hash;
		range[1][1] = (base_ori[i].y - y_min + all_pos[0]) / hash;
		std::pair<int, int>id;
		double diff_pos[2], diff_ang[2];
		for (int ix = range[0][0]; ix <= range[0][1]; ix++) {
			for (int iy = range[1][0]; iy <= range[1][1]; iy++) {
				id.first = ix;
				id.second = iy;

				if (base_map.count(id) == 0)continue;
				else if (base_map.count(id) == 1) {
					auto res = base_map.find(id);
					if (angle > 0.1) {
						diff_pos[0] = ((res->second->x - base_ori[i].x)*base_ori[i].ax + (res->second->y - base_ori[i].y)*base_ori[i].ay) / angle;
						diff_pos[1] = ((res->second->x - base_ori[i].x)*base_ori[i].ay - (res->second->y - base_ori[i].y)*base_ori[i].ax) / angle;
						diff_ang[0] = ((res->second->ax - base_ori[i].ax)*base_ori[i].ax + (res->second->ay - base_ori[i].ay)*base_ori[i].ay) / angle;
						diff_ang[1] = ((res->second->ax - base_ori[i].ax)*base_ori[i].ay - (res->second->ay - base_ori[i].ay)*base_ori[i].ax) / angle;
						if (fabs(diff_ang[0]) > all_ang[0])continue;
						if (fabs(diff_ang[1]) > all_ang[1])continue;
						if (fabs(diff_pos[0]) > all_pos[0])continue;
						if (fabs(diff_pos[1]) > all_pos[1])continue;
						match_cand.push_back(*res->second);
					}
					else {
						diff_pos[0] = res->second->x - base_ori[i].x;
						diff_pos[1] = res->second->y - base_ori[i].y;
						diff_ang[0] = res->second->ax - base_ori[i].ax;
						diff_ang[1] = res->second->ay - base_ori[i].ay;
						if (fabs(diff_ang[0]) > all_ang[1])continue;
						if (fabs(diff_ang[1]) > all_ang[1])continue;
						if (fabs(diff_pos[0]) > all_pos[1])continue;
						if (fabs(diff_pos[1]) > all_pos[1])continue;
						match_cand.push_back(*res->second);
					}
				}
				else {
					auto range = base_map.equal_range(id);
					for (auto res = range.first; res != range.second; res++) {
						if (angle > 0.1) {
							diff_pos[0] = ((res->second->x - base_ori[i].x)*base_ori[i].ax + (res->second->y - base_ori[i].y)*base_ori[i].ay) / angle;
							diff_pos[1] = ((res->second->x - base_ori[i].x)*base_ori[i].ay - (res->second->y - base_ori[i].y)*base_ori[i].ax) / angle;
							diff_ang[0] = ((res->second->ax - base_ori[i].ax)*base_ori[i].ax + (res->second->ay - base_ori[i].ay)*base_ori[i].ay) / angle;
							diff_ang[1] = ((res->second->ax - base_ori[i].ax)*base_ori[i].ay - (res->second->ay - base_ori[i].ay)*base_ori[i].ax) / angle;
							if (fabs(diff_ang[0]) > all_ang[0])continue;
							if (fabs(diff_ang[1]) > all_ang[1])continue;
							if (fabs(diff_pos[0]) > all_pos[0])continue;
							if (fabs(diff_pos[1]) > all_pos[1])continue;
							match_cand.push_back(*res->second);
						}
						else {
							diff_pos[0] = res->second->x - base_ori[i].x;
							diff_pos[1] = res->second->y - base_ori[i].y;
							diff_ang[0] = res->second->ax - base_ori[i].ax;
							diff_ang[1] = res->second->ay - base_ori[i].ay;
							if (fabs(diff_ang[0]) > all_ang[1])continue;
							if (fabs(diff_ang[1]) > all_ang[1])continue;
							if (fabs(diff_pos[0]) > all_pos[1])continue;
							if (fabs(diff_pos[1]) > all_pos[1])continue;
							match_cand.push_back(*res->second);
						}
					}
				}
			}
		}
		if (match_cand.size() == 0)continue;
		else {
			base_ori[i].hit_new = 1;
		}
	}
}
netscan::base_track_t select_best_base(netscan::base_track_t base, std::vector<netscan::base_track_t>base_all) {
	netscan::base_track_t ret;
	double distance = -1, dist_tmp;
	for (auto itr = base_all.begin(); itr != base_all.end(); itr++) {
		dist_tmp = sqrt(pow(itr->x - base.x, 2) + pow(itr->y - base.y, 2));
		if (distance<0 || distance>dist_tmp) {
			distance = dist_tmp;
			ret = *itr;
		}
	}
	//�����o��
	/*
	std::ofstream ofs("base_diff.txt", std::ios::app);
	ofs << std::right << std::fixed << std::setfill(' ')
		<< std::setw(12) << std::setprecision(0) << base.rawid << " "
		<< std::setw(12) << std::setprecision(0) << ret.rawid << " "
		<< std::setw(7) << std::setprecision(4) << base.ax << " "
		<< std::setw(7) << std::setprecision(4) << base.ay << " "
		<< std::setw(10) << std::setprecision(1) << base.x << " "
		<< std::setw(10) << std::setprecision(1) << base.y << " "
		<< std::setw(7) << std::setprecision(4) << ret.ax << " "
		<< std::setw(7) << std::setprecision(4) << ret.ay << " "
		<< std::setw(10) << std::setprecision(1) << ret.x << " "
		<< std::setw(10) << std::setprecision(1) << ret.y << std::endl;
	*/
	return ret;
}
void count_prediction(std::vector<base_prediction> base) {
	int count00 = 0, count01 = 0, count10 = 0, count11 = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->hit == 0 && itr->hit_new == 0)count00++;
		else if (itr->hit == 0 && itr->hit_new == 1)count01++;
		else if (itr->hit == 1 && itr->hit_new == 0)count10++;
		else if (itr->hit == 1 && itr->hit_new == 1)count11++;
	}
	printf("count 0 0 = %d\n", count00);
	printf("count 1 0 = %d\n", count10);
	printf("count 0 1 = %d\n", count01);
	printf("count 1 1 = %d\n", count11);
}
void write_prediction(std::string filename, std::vector<base_prediction> base) {
	std::ofstream ofs(filename,std::ios::app);
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		ofs << std::right << std::fixed << std::setfill(' ')
			<< std::setw(3) << std::setprecision(0) << itr->pl << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(8) << std::setprecision(1) << itr->x << " "
			<< std::setw(8) << std::setprecision(1) << itr->y << " "
			<< std::setw(2) << std::setprecision(0) << itr->hit_new << std::endl;
	}
}
