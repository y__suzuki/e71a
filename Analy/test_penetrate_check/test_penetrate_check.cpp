#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>
#pragma comment(lib,"functions.lib")
#include <functions.hpp>

#include <filesystem>
#include <set>

class Muon_inf {
public:
	int eventid;
	int muon_chainid;
	std::vector<int> connect_chain_id;
	std::string classification;
	std::string memo;
	//0無し 1 connect 2 kink 3 bkwd
	int connect_flg;

	void Set_flg();
};

class Connect_inf {
public:
	int eventid,muon_chain,connect_chain;
	int d_pl,nseg;

	double opening_angle, minimum_distance,vph;
};


void output_muon_inf(Muon_inf &m);
void output_muon_inf(std::vector<Muon_inf> &mu);
std::vector<Muon_inf> read_muon_inf(std::string filename);
bool find_string(const std::string str, const std::string subStr);
std::map<int, std::map<int, mfile0::M_Chain>> Divide_event_chain(std::vector<mfile0::M_Chain> &chain);
std::vector<Connect_inf> Calc_connect_value(std::map<int, std::map<int, mfile0::M_Chain>> &event_chain, std::vector<Muon_inf> &event_id);
bool Calc_connect_inf(mfile0::M_Chain &mu, mfile0::M_Chain &c, Connect_inf &c_inf);
void otuput_connect_inf(std::string filename, std::vector<Connect_inf>&c_inf);
std::vector<Muon_inf> list_all(std::vector<Muon_inf>&mu_inf);
std::vector<Connect_inf> Calc_connect_value_connect(std::map<int, std::map<int, mfile0::M_Chain>> &event_chain, std::vector<Muon_inf> &event_id);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "prg file_in_mfile file_in_memo file_out_dist file_out_dist_connect\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];

	std::string file_in_result = argv[2];
	std::string file_out_dist = argv[3];
	std::string file_out_dist_connect = argv[4];
	std::vector<Muon_inf> mu = read_muon_inf(file_in_result);

	//output_muon_inf(mu);

	mfile0::Mfile m;
	mfile0::read_mfile(file_in_mfile, m);


	std::map<int, std::map<int, mfile0::M_Chain>> m_event = Divide_event_chain(m.chains);


	std::vector<Muon_inf> calc_list = list_all(mu);
	std::vector<Connect_inf> result = Calc_connect_value(m_event, calc_list);
	std::vector<Connect_inf> result2 = Calc_connect_value_connect(m_event, calc_list);

	otuput_connect_inf(file_out_dist, result);
	otuput_connect_inf(file_out_dist_connect, result2);


}
std::vector<Muon_inf> read_muon_inf(std::string filename) {
	std::ifstream ifs(filename);
	std::vector<Muon_inf> ret;
	std::string str;
	std::vector<std::string> str_v;
	std::string buffer;
	int cnt = 0;
	while (std::getline(ifs, str))
	{
		str_v.clear();
		str_v = StringSplit(str);
		std::stringstream ss(str);
		if (str_v.size() <= 2)continue;
		//commentout
		if (str_v[0][0] == '#')continue;

		Muon_inf mu;

		mu.eventid = std::stoi(str_v[0]);
		mu.classification = str_v[1];
		if (std::all_of(str_v[2].cbegin(), str_v[2].cend(), isdigit)) {
			mu.muon_chainid = std::stoi(str_v[2]);
			for (int i = 3; i < str_v.size(); i++) {

				if (std::all_of(str_v[i].cbegin(), str_v[i].cend(), isdigit)) {
					mu.connect_chain_id.push_back(std::stoi(str_v[i]));
				}
				else {
					for (int j = i; j < str_v.size(); j++) {
						mu.memo = mu.memo + str_v[j];

					}
					break;
				}
			}
		}
		else {
			mu.muon_chainid= -1;
			for (int i = 2; i < str_v.size(); i++) {
				mu.memo = mu.memo + str_v[i];
			}
		}
		mu.Set_flg();
		ret.push_back(mu);
	}
	return ret;

}
void output_muon_inf(std::vector<Muon_inf> &mu) {
	for (auto m : mu) {
		output_muon_inf(m);
	}
}
void output_muon_inf(Muon_inf &m) {
	std::cout << std::right << std::fixed << std::setfill(' ')
		<< std::setw(2) << std::setprecision(0) << m.connect_flg<< " "
		<< std::setw(5) << std::setprecision(0) << m.eventid << " "
		<< std::setw(8) << m.classification << " "
		<< std::setw(8) << m.muon_chainid << " ";
	for (int i = 0; i < m.connect_chain_id.size(); i++) {
		std::cout << std::setw(10) << std::setprecision(0) << m.connect_chain_id[i] << " ";
	}
	std::cout << m.memo << std::endl;
}

void Muon_inf::Set_flg() {
	std::string kink = "kink", bwd = "bwd";
	if (connect_chain_id.size() == 0) {
		if (find_string(memo, kink)) {
			connect_flg = 2;
		}
		else if (find_string(memo, bwd)) {
			connect_flg = 3;
		}
	}
	else {
		if (find_string(memo, kink)) {
			connect_flg = 2;
		}
		else if (find_string(memo, bwd)) {
			connect_flg = 3;
		}
		else {
			connect_flg = 1;
		}
	}
}


bool find_string(const std::string str, const std::string subStr) {
	std::vector<int> result;

	int subStrSize = subStr.size();
	int pos = str.find(subStr);

	while (pos != std::string::npos) {
		result.push_back(pos);
		pos = str.find(subStr, pos + subStrSize);
	}
	if (result.size() == 0)return false;
	return true;
}

std::map<int, std::map<int, mfile0::M_Chain>> Divide_event_chain(std::vector<mfile0::M_Chain> &chain) {


	std::map<int, std::map<int, mfile0::M_Chain>> ret;;

	std::multimap<int, mfile0::M_Chain> event_map;
	int eventid;
	for (auto c : chain) {
		eventid = c.basetracks.begin()->group_id / 100000;
		event_map.insert(std::make_pair(eventid, c));
	}
	int count = 0;
	for (auto itr = event_map.begin(); itr != event_map.end(); itr++) {
		count = event_map.count(itr->first);

		auto range = event_map.equal_range(itr->first);
		std::map<int, mfile0::M_Chain> chain_map;
		for (auto res = range.first; res != range.second; res++) {
			chain_map.insert(std::make_pair(res->second.chain_id, res->second));
		}
		ret.insert(std::make_pair(itr->first, chain_map));
		itr = std::next(itr, count - 1);
	}
	return ret;
}

std::vector<Connect_inf> Calc_connect_value(std::map<int, std::map<int, mfile0::M_Chain>> &event_chain, std::vector<Muon_inf> &event_id) {
	std::vector<Connect_inf> ret;
	printf("size=%d\n", event_id.size());
	for (auto eid : event_id) {

		auto chains = event_chain.find(eid.eventid);
		if (chains == event_chain.end())continue;
		//printf("eventid = %d chain size=%d\n", eid.eventid,chains->second.size());

		if (chains->second.size() == 1)continue;
		auto mu = chains->second.find(eid.muon_chainid);
		if (mu == chains->second.end())continue;
		for (auto c = chains->second.begin(); c != chains->second.end(); c++) {
			if (c->first == mu->first)continue;
			Connect_inf c_inf;
			c_inf.eventid = eid.eventid;
			if (Calc_connect_inf(mu->second, c->second, c_inf)) {
				ret.push_back(c_inf);
			}
		}

	}
	return ret;
}
std::vector<Connect_inf> Calc_connect_value_connect(std::map<int, std::map<int, mfile0::M_Chain>> &event_chain, std::vector<Muon_inf> &event_id) {
	std::vector<Connect_inf> ret;
	printf("size=%d\n", event_id.size());
	for (auto eid : event_id) {

		auto chains = event_chain.find(eid.eventid);
		if (chains == event_chain.end())continue;
		//printf("eventid = %d chain size=%d\n", eid.eventid,chains->second.size());

		if (chains->second.size() == 1)continue;
		auto mu = chains->second.find(eid.muon_chainid);
		if (mu == chains->second.end())continue;
		if (eid.connect_chain_id.size() == 0)continue;
		auto c = chains->second.find(*(eid.connect_chain_id.begin()));
		if (c == chains->second.end())continue;
		Connect_inf c_inf;
		c_inf.eventid = eid.eventid;
		if (Calc_connect_inf(mu->second, c->second, c_inf)) {
			ret.push_back(c_inf);
		}
	}
	return ret;
}
bool Calc_connect_inf(mfile0::M_Chain &mu, mfile0::M_Chain &c, Connect_inf &c_inf) {


	//計算すべきbasetrack対を探す
	int down_pl = c.pos0 / 10;
	mfile0::M_Base mu_base= *(mu.basetracks.begin());
	bool flg = false;
	for (auto b : mu.basetracks) {
		if (b.pos / 10 >= down_pl)continue;
		if (!flg) {
			mu_base = b;
			flg = true;
		}
		if (mu_base.pos < b.pos) {
			mu_base = b;
		}
	}
	if (!flg)return false;

	double vph = 0;
	int count = 0;
	for (auto b : c.basetracks) {
		vph += b.ph % 10000;
		count++;
	}
	c_inf.nseg = count;
	c_inf.vph = vph / count;
	c_inf.connect_chain = c.chain_id;
	c_inf.d_pl = fabs(down_pl - mu_base.pos / 10)+1;
	c_inf.muon_chain = mu.chain_id;
	
	matrix_3D::vector_3D pos0, pos1, dir0, dir1;
	pos0.x = mu_base.x;
	pos0.y = mu_base.y;
	pos0.z = mu_base.z;
	dir0.x = mu_base.ax;
	dir0.y = mu_base.ay;
	dir0.z = 1;

	pos1.x = c.basetracks.begin()->x;
	pos1.y = c.basetracks.begin()->y;
	pos1.z = c.basetracks.begin()->z;
	dir1.x = c.basetracks.begin()->ax;
	dir1.y = c.basetracks.begin()->ay;
	dir1.z = 1;

	c_inf.opening_angle = matrix_3D::opening_angle(dir0, dir1);

	double extra[2], z_range[2];
	z_range[1] = pos0.z;
	z_range[0] = pos1.z;
	c_inf.minimum_distance = matrix_3D::minimum_distance(pos0, pos1, dir0, dir1, z_range, extra);
	return true;
}
void otuput_connect_inf(std::string filename, std::vector<Connect_inf>&c_inf) {

	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (c_inf.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = c_inf.begin(); itr != c_inf.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)", count, int(c_inf.size()), count*100. / c_inf.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(8) << std::setprecision(0) << itr->eventid << " "
				<< std::setw(12) << std::setprecision(0) << itr->muon_chain << " "
				<< std::setw(12) << std::setprecision(0) << itr->connect_chain << " "
				<< std::setw(4) << std::setprecision(0) << itr->nseg << " "
				<< std::setw(8) << std::setprecision(0) << itr->d_pl << " "
				<< std::setw(4) << std::setprecision(1) << itr->vph << " "
				<< std::setw(9) << std::setprecision(5) << itr->opening_angle << " "
				<< std::setw(9) << std::setprecision(2) << itr->minimum_distance << std::endl;
		}

		fprintf(stderr, "\r Write corrmap ... %d/%d (%4.1lf%%)\n", count, int(c_inf.size()), count*100. / c_inf.size());
	}
}



std::vector<Muon_inf> list_all(std::vector<Muon_inf>&mu_inf) {
	std::vector<Muon_inf> ret;
	for (auto mu : mu_inf) {
		if (mu.muon_chainid > 0) {
			ret.push_back(mu);
		}
	}
	return ret;
}

std::vector<Muon_inf> list_connect(std::vector<Muon_inf>&mu_inf) {
	std::vector<Muon_inf> ret;
	for (auto mu : mu_inf) {
		if (mu.muon_chainid > 0) {
			if (mu.connect_flg > 0) {
				ret.push_back(mu);
			}
		}
	}
	return ret;
}