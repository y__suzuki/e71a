#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

class Prediction {
public:
	int pl, rawid, hit;
	double ax, ay, x, y, z;
};
std::vector<Prediction> read_predction(std::string filename, double &x_min, double &x_max, double &y_min, double &y_max);
void base_area(std::vector<vxx::base_track_t> &base, corrmap0::Corrmap param, double &x_min, double &x_max, double &y_min, double &y_max);


int main(int argc, char**argv) {
	if (argc != 6) {
		fprintf(stderr, "usage:prg file_base_path file_ali_path file-prediction pl output\n");
		exit(1);
	}
	std::string file_base_path = argv[1];
	std::string file_ali_path = argv[2];

	std::string file_pred = argv[3];
	int pl = std::stoi(argv[4]);
	std::string file_out = argv[5];
	double x_min, x_max, y_min, y_max;
	std::vector<Prediction> pred = read_predction(file_pred, x_min, x_max, y_min, y_max);
	vxx::BvxxReader br;
	for (int sensor = 0; sensor < 72; sensor++) {
		std::stringstream file_in_base, file_in_ali;
		file_in_base << file_base_path << "\\b"
			<< std::setw(3) << std::setfill('0') << pl << "_sensor"
			<< std::setw(2) << std::setfill('0') << sensor << ".sel.cor.vxx";
		if (sensor==0){
			file_in_ali << file_ali_path << "\\corrmap-g-"
				<< std::setw(3) << std::setfill('0') << pl << "-72.lst";
		}
		else {
			file_in_ali << file_ali_path << "\\corrmap-g-"
				<< std::setw(3) << std::setfill('0') << pl << "-"
				<< std::setw(2) << std::setfill('0') << sensor << ".lst";
		}
		std::vector<corrmap0::Corrmap> corr;
		corrmap0::read_cormap(file_in_ali.str(), corr);
		std::vector<vxx::base_track_t> base = br.ReadAll(file_in_base.str(), pl, 0);
		base_area(base, corr[0], x_min, x_max, y_min, y_max);
	}

	std::ofstream ofs(file_out);
	ofs << std::right << std::fixed << std::setfill(' ')
		<< std::setw(10) << std::setprecision(1) << x_min
		<< std::setw(10) << std::setprecision(1) << x_max
		<< std::setw(10) << std::setprecision(1) << y_min
		<< std::setw(10) << std::setprecision(1) << y_max << std::endl;

	printf("x %5.2lf mm\ny %5.2lf mm\n", (x_max - x_min) / 1000, (y_max - y_min) / 1000);
}

std::vector<Prediction> read_predction(std::string filename, double &x_min, double &x_max, double &y_min, double &y_max) {
	std::ifstream ifs(filename);
	std::vector<Prediction> ret;
	Prediction pred;
	int count = 0;
	while (ifs >> pred.pl >> pred.rawid >> pred.ax >> pred.ay >> pred.x >> pred.y >> pred.z >> pred.hit) {
		if (count == 0) {
			x_min = pred.x;
			x_max = pred.x;
			y_min = pred.y;
			y_max = pred.y;
		}
		x_min = std::min(x_min, pred.x);
		x_max = std::max(x_max, pred.x);
		y_min = std::min(y_min, pred.y);
		y_max = std::max(y_max, pred.y);
		
		if (count % 100000 == 0) {
			printf("\r read predcition %8d", count);
		}
		count++;
		ret.push_back(pred);
	}
	printf("\r read predcition %8d\n", count);

	return ret;
}		

void base_area(std::vector<vxx::base_track_t> &base, corrmap0::Corrmap param, double &x_min, double &x_max, double &y_min, double &y_max) {
	double area[4];
	double tmp_x, tmp_y ,tmp_ax, tmp_ay;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		tmp_x = itr->x*param.position[0] + itr->y * param.position[1] + param.position[4];
		tmp_y = itr->x*param.position[2] + itr->y * param.position[3] + param.position[5];
		tmp_ax = itr->ax*param.angle[0] + itr->ay * param.angle[1] + param.angle[4];
		tmp_ay = itr->ax*param.angle[2] + itr->ay * param.angle[3] + param.angle[5];

		tmp_x = tmp_x - param.dz*tmp_ax;
		tmp_y = tmp_y - param.dz*tmp_ay;

		if (itr == base.begin()) {
			area[0] = tmp_x;
			area[1] = tmp_x;
			area[2] = tmp_y;
			area[3] = tmp_y;
		}
		
		area[0] = std::min(area[0], tmp_x);
		area[1] = std::max(area[1], tmp_x);
		area[2] = std::min(area[2], tmp_y);
		area[3] = std::max(area[3], tmp_y);
	}

	x_min = std::max(x_min, area[0]);
	x_max = std::min(x_max, area[1]);
	y_min = std::max(y_min, area[2]);
	y_max = std::min(y_max, area[3]);
}
