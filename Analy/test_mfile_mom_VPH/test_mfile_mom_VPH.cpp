#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")

#include <FILE_structure.hpp>
#include "VxxReader.h"

class chain_inf {
public:
	int chainID, nseg, npl;
	double ax, ay, vph_ave, ph_ave,mom;
};
class microtrack_inf {
public:
	int chainID,pos, ph;
	double ax, ay ;
};
std::map<int, double>  read_mom(std::string filename);
std::vector<chain_inf> chain_match(std::vector<mfile0::M_Chain> &c, std::map<int, double> mom);
void write_chain_inf(std::string filename, std::vector<chain_inf> &c);

void chain_divide(std::vector<chain_inf> &c, std::vector<mfile0::M_Chain> &all, std::vector<mfile0::M_Chain> &thin, std::vector<mfile0::M_Chain> &proton);
std::vector<microtrack_inf> chain2micro(std::vector<mfile0::M_Chain> &c, std::string microtrack_path);
void out_micro(std::string filename, std::vector<microtrack_inf>&m);

int main(int argc, char**argv) {
	if (argc != 7) {
		fprintf(stderr, "usage:prg in-mfile in-mom-file in-area1-path outfile out-thin out-proton\n");
		exit(1);
	}
	std::string file_in_mfile = argv[1];
	std::string file_in_mom = argv[2];
	std::string file_in_path = argv[3];
	std::string file_out = argv[4];
	std::string file_out_thin = argv[5];
	std::string file_out_proton = argv[6];

	mfile0::Mfile m;
	mfile1::read_mfile_extension(file_in_mfile, m);

	std::map<int, double> mom = read_mom(file_in_mom);

	std::vector<chain_inf> c_inf = chain_match(m.chains, mom);

	write_chain_inf(file_out, c_inf);

	std::vector<mfile0::M_Chain> thin, proton;
	
	chain_divide(c_inf, m.chains, thin, proton);
	std::vector<microtrack_inf> m_pro = chain2micro(proton, file_in_path);
	std::vector<microtrack_inf> m_thin = chain2micro(thin, file_in_path);

	out_micro(file_out_proton, m_pro);
	out_micro(file_out_thin, m_thin);
}
std::map<int, double>  read_mom(std::string filename) {
	std::map<int, double> ret;
	std::ifstream ifs(filename);
	int id;
	double mom;
	while (ifs >> id >> mom) {
		if (mom > 0.1) {
			ret.insert(std::make_pair(id, mom));
		}
	}
	return ret;
}
std::vector<chain_inf> chain_match(std::vector<mfile0::M_Chain> &c, std::map<int, double> mom) {
	std::vector<chain_inf> ret;
	std::map<int, mfile0::M_Chain*> c_map;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		c_map.insert(std::make_pair(itr->chain_id, &(*itr)));
	}
	for (auto itr = mom.begin(); itr != mom.end(); itr++) {
		auto res = c_map.find(itr->first);
		if (res == c_map.end())continue;

		chain_inf c;
		c.chainID = itr->first;
		c.ax = mfile0::chain_ax(*res->second);
		c.ay = mfile0::chain_ay(*res->second);
		c.npl = (res->second->pos1 - res->second->pos0) / 10 + 1;
		c.nseg = res->second->nseg;
		c.mom = itr->second;
		double vph = 0, ph = 0;
		int num = 0;
		for (auto itr2 = res->second->basetracks.begin(); itr2 != res->second->basetracks.end(); itr2++) {
			num++;
			vph += itr2->ph % 10000;
			ph += itr2->ph / 10000;
		}
		c.ph_ave = ph / num;
		c.vph_ave = vph / num;
		ret.push_back(c);
	}
	return ret;
}
void write_chain_inf(std::string filename, std::vector<chain_inf> &c) {
	std::ofstream ofs(filename);
	for (auto itr = c.begin(); itr != c.end(); itr++) {

		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr->chainID << " "
			<< std::setw(5) << std::setprecision(0) << itr->nseg << " "
			<< std::setw(5) << std::setprecision(0) << itr->npl << " "
			<< std::setw(6) << std::setprecision(1) << itr->mom << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(7) << std::setprecision(4) << sqrt(itr->ax*itr->ax + itr->ay*itr->ay) << " "
			<< std::setw(7) << std::setprecision(2) << itr->ph_ave << " "
			<< std::setw(7) << std::setprecision(2) << itr->vph_ave << std::endl;

	}

}
void chain_divide(std::vector<chain_inf> &c, std::vector<mfile0::M_Chain> &all, std::vector<mfile0::M_Chain> &thin, std::vector<mfile0::M_Chain> &proton) {

	std::map<int, mfile0::M_Chain*> c_map;
	for (auto itr = all.begin(); itr != all.end(); itr++) {
		c_map.insert(std::make_pair(itr->chain_id, &(*itr)));
	}

	for (auto itr = c.begin(); itr != c.end(); itr++) {
		auto res = c_map.find(itr->chainID);
		if (res == c_map.end())continue;
		if (itr->vph_ave > 150 && itr->vph_ave>-0.5*itr->mom + 300) {
			proton.push_back(*res->second);
		}
		else {
			thin.push_back(*res->second);
		}
	}
}
std::vector<microtrack_inf> chain2micro(std::vector<mfile0::M_Chain> &c, std::string microtrack_path) {
	std::vector<microtrack_inf> ret;

	std::multimap<int, std::pair<int, int>> chain;
	for (auto itr = c.begin(); itr != c.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			chain.insert(std::make_pair(itr2->pos / 10, std::make_pair(itr->chain_id, itr2->rawid)));
		}
	}

	for (int pl = 0; pl < 133; pl++) {
		if (pl == 4 || pl == 60)continue;
		int num = chain.count(pl);
		//printf("PL%03d %d\n", pl, num);
		if (num == 0)continue;
		std::stringstream base_path;
		base_path << microtrack_path << "\\PL" << std::setw(3) << std::setfill('0') << pl<<"\\b" << std::setw(3) << std::setfill('0') <<pl<< ".sel.cor.vxx";
		//printf("%s\n", base_path.str().c_str());
		vxx::BvxxReader bw;
		std::vector<vxx::base_track_t> base = bw.ReadAll(base_path.str(), pl, 0);
		std::map<int, vxx::base_track_t*> base_map;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			base_map.insert(std::make_pair(itr->rawid, &(*itr)));
		}
		auto range = chain.equal_range(pl);
		for (auto res = range.first; res != range.second; res++) {
			auto b = base_map.find(res->second.second);
			if (b == base_map.end())continue;
			microtrack_inf m0, m1;
			m0.chainID = res->second.first;
			m0.pos = b->second->m[0].pos;
			m0.ax = b->second->m[0].ax;
			m0.ay = b->second->m[0].ay;
			m0.ph = b->second->m[0].ph;
			m1.chainID = res->second.first;
			m1.pos = b->second->m[1].pos;
			m1.ax = b->second->m[1].ax;
			m1.ay = b->second->m[1].ay;
			m1.ph = b->second->m[1].ph;
			ret.push_back(m0);
			ret.push_back(m1);
		}
	}
	return ret;
}

void out_micro(std::string filename, std::vector<microtrack_inf>&m) {
	std::ofstream ofs(filename);
	int count = 0 , all = m.size();
	for (auto itr = m.begin(); itr != m.end(); itr++) {
		if (count % 10000 == 0) {
			fprintf(stderr, "\r now writing... %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << itr->chainID << " "
			<< std::setw(5) << std::setprecision(0) << itr->pos << " "
			<< std::setw(7) << std::setprecision(4) << itr->ax << " "
			<< std::setw(7) << std::setprecision(4) << itr->ay << " "
			<< std::setw(7) << std::setprecision(4) << sqrt(itr->ax*itr->ax + itr->ay*itr->ay) << " "
			<< std::setw(5) << std::setprecision(0) << itr->ph / 10000 << " "
			<< std::setw(5) << std::setprecision(0) << itr->ph % 10000 << std::endl;
	}
	fprintf(stderr, "\r now writing... %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

}