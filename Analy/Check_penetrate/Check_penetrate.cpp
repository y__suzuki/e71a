#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch);
std::vector<Momentum_recon::Mom_chain > format_change(std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&mom_ev);
void Check_fill_factor_forward_water(std::string filename, std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&momch_ev);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-momch file-out\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch_name = argv[2];

	std::vector<Momentum_recon::Mom_chain> momch = Momentum_recon::Read_mom_chain_extension(file_in_momch);
	std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> momch_ev = divide_event(momch);

	Check_fill_factor_forward_water(file_out_momch_name, momch_ev);

}
std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> divide_event(std::vector<Momentum_recon::Mom_chain> &momch) {

	std::multimap<int, Momentum_recon::Mom_chain> divide_ev;
	for (auto itr = momch.begin(); itr != momch.end(); itr++) {
		divide_ev.insert(std::make_pair(itr->groupid, *itr));
	}
	std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> ret;
	for (auto itr = divide_ev.begin(); itr != divide_ev.end(); itr++) {
		Momentum_recon::Mom_chain muon;
		std::vector<Momentum_recon::Mom_chain>partner;
		muon.groupid = -1;
		auto range = divide_ev.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			if (res->second.chainid == 0) {
				muon = res->second;
			}
			else {
				partner.push_back(res->second);
			}
		}
		if (muon.groupid < 0) {
			fprintf(stderr, "exception event %d not found\n", itr->first);
			itr = std::next(itr, divide_ev.count(itr->first) - 1);
		}
		ret.push_back(std::make_pair(muon, partner));
		itr = std::next(itr, divide_ev.count(itr->first) - 1);
	}
	return ret;
}
void Check_fill_factor_forward_water(std::string filename, std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&momch_ev) {
	const int DPL_MAX = 5;
	std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>> out_fill[DPL_MAX];
	for (auto &ev : momch_ev) {

		std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>> chains[DPL_MAX];
		for (int i = 0; i < DPL_MAX; i++) {
			chains[i].first = ev.first;
		}
		for (auto &c : ev.second) {
			int vertex_pl = abs(c.stop_flg);
			//water
			if (vertex_pl <= 15 || vertex_pl % 2 == 0)continue;
			//forward
			if (c.stop_flg > 0) {
				int dpl = c.base.rbegin()->pl - vertex_pl - 1;
				chains[std::min(DPL_MAX - 1, dpl)].second.push_back(c);
			}
		}
		for (int i = 0; i < DPL_MAX; i++) {
			if (chains[i].second.size() == 0)continue;
			out_fill[i].push_back(chains[i]);
		}
	}


	for (int i = 0; i < DPL_MAX; i++) {
		int num = 0;
		for (auto itr = out_fill[i].begin(); itr != out_fill[i].end(); itr++) {
			num += itr->second.size();
		}
		printf("dPL %d num %d\n", i + 1, num);
		if (out_fill[i].size() == 0)continue;
		std::vector<Momentum_recon::Mom_chain > out_momch = format_change(out_fill[i]);
		std::stringstream file_out;
		file_out << filename << "_dpl" << i + 1 << ".txt";

		Momentum_recon::Write_mom_chain_extension(file_out.str(), out_momch);
	}

}


std::vector<Momentum_recon::Mom_chain > format_change(std::vector<std::pair<Momentum_recon::Mom_chain, std::vector<Momentum_recon::Mom_chain>>>&mom_ev) {
	std::vector<Momentum_recon::Mom_chain > ret;
	for (auto itr = mom_ev.begin(); itr != mom_ev.end(); itr++) {
		ret.push_back(itr->first);
		for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {
			if (itr2->base.size() > 0) {
				ret.push_back(*itr2);
			}
		}
	}
	return ret;

}