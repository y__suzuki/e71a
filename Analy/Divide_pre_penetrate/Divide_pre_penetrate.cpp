#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<Momentum_recon::Event_information>extract_stop(std::vector<Momentum_recon::Event_information>&momch_ev);
	std::vector<Momentum_recon::Event_information>extract_penetrate(std::vector<Momentum_recon::Event_information>&momch_ev);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-momch file-out\n");
		exit(1);
	}
	std::string file_in_momch = argv[1];
	std::string file_out_momch_name = argv[2];

	std::vector<Momentum_recon::Event_information> momch = Momentum_recon::Read_Event_information_extension(file_in_momch);
	std::vector<Momentum_recon::Event_information>momch_stop = extract_stop(momch);
	std::vector<Momentum_recon::Event_information>momch_penetrate = extract_penetrate(momch);

	Momentum_recon::Write_Event_information_extension(file_out_momch_name + "_stop.momch", momch_stop);
	Momentum_recon::Write_Event_information_extension(file_out_momch_name + "_penetrate.momch", momch_penetrate);

}
std::vector<Momentum_recon::Event_information>extract_stop(std::vector<Momentum_recon::Event_information>&momch_ev) {
	std::vector<Momentum_recon::Event_information> ret;
	int all = 0, sel = 0;
	for (auto &ev : momch_ev) {
		Momentum_recon::Event_information ev_inf = ev;
		ev_inf.chains.clear();
		ev_inf.true_chains.clear();
		for (auto &c : ev.chains) {
			if (c.particle_flg == 13) {
				ev_inf.chains.push_back(c);
				continue;
			}
			all++;
			//forard����
			if (c.direction == 1) {
				if (c.base.rbegin()->pl <= ev.vertex_pl) {
					sel++;
					ev_inf.chains.push_back(c);
				}
			}
			else if (c.direction == -1) {
				if (c.base.begin()->pl > ev.vertex_pl) {
					sel++;
					ev_inf.chains.push_back(c);
				}
			}
		}
		ret.push_back(ev_inf);
	}
	printf("stop attach %d --> %d\n", all, sel);
	return ret;
}
std::vector<Momentum_recon::Event_information>extract_penetrate(std::vector<Momentum_recon::Event_information>&momch_ev) {
	std::vector<Momentum_recon::Event_information> ret;
	int all = 0, sel = 0;
	for (auto &ev : momch_ev) {
		Momentum_recon::Event_information ev_inf = ev;
		ev_inf.chains.clear();
		ev_inf.true_chains.clear();
		for (auto &c : ev.chains) {
			if (c.particle_flg == 13) {
				ev_inf.chains.push_back(c);
				continue;
			}
			all++;
			//forard����
			if (c.direction == 1) {
				if (c.base.rbegin()->pl > ev.vertex_pl) {
					sel++;
					ev_inf.chains.push_back(c);
				}
			}
			else if (c.direction == -1) {
				if (c.base.begin()->pl <= ev.vertex_pl) {
					sel++;
					ev_inf.chains.push_back(c);
				}
			}
		}
		ret.push_back(ev_inf);
	}
	printf("penetrate attach %d --> %d\n", all, sel);
	return ret;
}
