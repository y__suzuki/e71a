#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>

void Print_same_event(std::vector<Sharing_file::Sharing_file> sf, std::vector<Sharing_file::Sharing_file> sf_old);

int main(int argc,char**argv) {
	if (argc <2) {
		fprintf(stderr, "usage:prg file-in-sharingfile file-in-sharingfile-old\n");
		exit(1);
	}
	std::string file_in_sf = argv[1];
	std::string file_in_sf_old = argv[2];

	std::vector<Sharing_file::Sharing_file> sf,sf_old;
	sf= Sharing_file::Read_sharing_file_bin(file_in_sf);
	if (argc == 3) {

		sf_old = Sharing_file::Read_sharing_file_bin(file_in_sf_old);
		Print_same_event(sf, sf_old);

	}
	else {


	}
}
void Print_same_event(std::vector<Sharing_file::Sharing_file> sf, std::vector<Sharing_file::Sharing_file> sf_old) {
	int eventid;
	while (true) {
		std::cout << "input eventid new (eventid=-1 break)" << std::endl;
		std::cin >> eventid;
		if (eventid < 0)return;
		Sharing_file::Sharing_file sel;
		bool flg = false;
		for (auto itr = sf.begin(); itr != sf.end(); itr++) {
			if (itr->eventid == eventid) {
				sel = *itr;
				flg = true;
				break;
			}
		}
		if (!flg) {
			fprintf(stderr, "eventid = %d not found\n", eventid);
			continue;
		}

		printf("new sharing file event=%d\n", eventid);
		printf("%s\n", sel.Print_content().c_str());
		printf("old : search matching basetrackid... \n");
		for (auto itr = sf_old.begin(); itr != sf_old.end(); itr++) {
			if (itr->pl == sel.pl&&
				itr->zone[0] == sel.zone[0] &&
				itr->zone[1] == sel.zone[1] &&
				itr->rawid[0] == sel.rawid[0] &&
				itr->rawid[1] == sel.rawid[1]) {
				printf("%s\n", itr->Print_content().c_str());
			}
		}
		printf("new : search matching tracker time... \n");
		for (auto itr = sf.begin(); itr != sf.end(); itr++) {
			if (itr->unix_time == sel.unix_time) {
				printf("%s\n", itr->Print_content().c_str());
			}
		}
		printf("new : search matching basetrackid... \n");
		for (auto itr = sf.begin(); itr != sf.end(); itr++) {
			if (itr->pl == sel.pl&&
				itr->zone[0] == sel.zone[0] &&
				itr->zone[1] == sel.zone[1] &&
				itr->rawid[0] == sel.rawid[0] &&
				itr->rawid[1] == sel.rawid[1]) {
				printf("%s\n", itr->Print_content().c_str());
			}
		}
	}
}