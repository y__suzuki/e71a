#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

std::vector<std::tuple<int, int, int, int>> Read_header(std::string filename);
void output_header(std::string filename, std::vector<std::tuple<int, int, int, int>>&header);

int main(int argc, char**argv) {
	if (argc != 3) {
		fprintf(stderr, "usage:file-in-linklet-header file-out-linklet-header\n");
		exit(1);
	}
	std::string file_in_linklet_header = argv[1];
	std::string file_out_linklet_header = argv[2];

	std::vector<std::tuple<int, int, int, int>> header = Read_header(file_in_linklet_header);
	sort(header.begin(), header.end());
	auto result = std::unique(header.begin(), header.end());
	printf("same linklet delte %d -->", header.size());
	header.erase(result, header.end());
	printf("%d\n", header.size());

	output_header(file_out_linklet_header, header);
}
std::vector<std::tuple<int, int, int, int>> Read_header(std::string filename) {
	std::vector<std::tuple<int, int, int, int>> ret;
	std::tuple<int, int, int, int> input;
	std::ifstream ifs(filename);
	int64_t count = 0;
	while (ifs >> std::get<0>(input) >> std::get<1>(input) >> std::get<2>(input) >> std::get<3>(input)) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r Read Linklet header %d", count);
		}
		count++;
		ret.push_back(input);
	}
	fprintf(stderr, "\r Read Linklet header %d fin\n", count);
	return ret;
}
void output_header(std::string filename, std::vector<std::tuple<int, int, int, int>>&header) {
	FILE  *fp_out;
	if ((fp_out = fopen(filename.c_str(), "w")) == NULL) {
		printf("file open error!!\n");
		exit(EXIT_FAILURE);	/* (3)エラーの場合は通常、異常終了する */
	}

	int64_t all = header.size(), count = 0;

	for (auto &h : header) {
		if (count % 100000 == 0) {
			fprintf(stderr, "\r write linklet header %d/%d(%4.1lf%%)", count, all, count*100. / all);
		}
		count++;
		fprintf(fp_out, "%4d %10d %4d %10d\n", std::get<0>(h), std::get<1>(h), std::get<2>(h), std::get<3>(h));
	}
	fprintf(stderr, "\r write linklet header %d/%d(%4.1lf%%)\n", count, all, count*100. / all);

	fclose(fp_out);
}