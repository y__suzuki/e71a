#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "VxxReader.lib")
#include "VxxReader.h"
#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>
#include <set>

class Fiducial_Area {
public:
	int pl;
	double x0, y0,z0, x1, y1,z1;
};

std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename);
void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map);
void output_Fiducial_Area_line(std::string fileanme, std::map<int, std::vector<Fiducial_Area>> &area);

int main(int argc, char**argv) {
	if (argc != 4) {
		fprintf(stderr, "usage: prg fa.txt(txt) file-in-ECC output-path\n");
		exit(1);
	}

	std::string file_in_fa = argv[1];
	std::string file_in_ECC = argv[2];
	std::string file_out_fa = argv[3];

	//corrmap absの読み込み
	std::string file_in_corrabs = file_in_ECC + "\\Area0\\0\\align\\corrmap-abs.lst";
	std::vector<corrmap0::Corrmap> corr_abs;
	corrmap0::read_cormap(file_in_corrabs, corr_abs);
	//fiducial areaの読み込み
	std::map<int, std::vector<Fiducial_Area>> area = read_fiducial_Area(file_in_fa);
	//gap nominal read
	std::stringstream structure_path;
	structure_path << file_in_ECC << "\\st\\st.dat";
	chamber1::Chamber chamber;
	chamber1::read_structure(structure_path.str(), chamber);
	std::map<int, double> z_map = chamber1::base_z_convert(chamber);

	//corrmap absの適用
	trans_mfile_cordinate(corr_abs, area, z_map);
	output_Fiducial_Area_line(file_out_fa, area);
}
std::map<int, std::vector<Fiducial_Area>> read_fiducial_Area(std::string filename) {

	std::ifstream ifs(filename);
	std::multimap<int, Fiducial_Area> fa_multi;
	std::map<int, std::vector<Fiducial_Area>> ret;
	Fiducial_Area fa;
	while (ifs >> fa.pl >> fa.x0 >> fa.y0 >>fa.z0>> fa.x1 >> fa.y1>>fa.z1) {
		fa_multi.insert(std::make_pair(fa.pl, fa));
	}

	int count = 0;
	for (auto itr = fa_multi.begin(); itr != fa_multi.end(); itr++) {
		count = fa_multi.count(itr->first);
		auto range = fa_multi.equal_range(itr->first);
		std::vector<Fiducial_Area> fa_vec;
		for (auto itr2 = range.first; itr2 != range.second; itr2++) {
			fa_vec.push_back(itr2->second);
		}
		ret.insert(std::make_pair(itr->first, fa_vec));
	}

	return ret;

}

void trans_mfile_cordinate(std::vector<corrmap0::Corrmap> &corr_abs, std::map<int, std::vector<Fiducial_Area>> &area, std::map<int, double> &z_map) {
	int pl;
	for (auto itr = corr_abs.begin(); itr != corr_abs.end(); itr++) {
		pl = itr->pos[0] / 10;
		//printf("PL%03d %d %d\n", pl,area.count(pl), z_map.count(pl));
		if (area.count(pl) + z_map.count(pl) == 2) {
			auto z = z_map.find(pl);

			auto vec = area.find(pl);
			double tmp_x, tmp_y;
			for (auto itr2 = vec->second.begin(); itr2 != vec->second.end(); itr2++) {
				tmp_x = itr2->x0;
				tmp_y = itr2->y0;
				itr2->x0 = tmp_x * itr->position[0] + tmp_y * itr->position[1] + itr->position[4];
				itr2->y0 = tmp_x * itr->position[2] + tmp_y * itr->position[3] + itr->position[5];
				tmp_x = itr2->x1;
				tmp_y = itr2->y1;
				itr2->x1 = tmp_x * itr->position[0] + tmp_y * itr->position[1] + itr->position[4];
				itr2->y1 = tmp_x * itr->position[2] + tmp_y * itr->position[3] + itr->position[5];

				itr2->z0 = z->second + itr->dz;
				itr2->z1 = z->second + itr->dz;
			}
		}
	}
}
void output_Fiducial_Area_line(std::string fileanme, std::map<int, std::vector<Fiducial_Area>> &area) {
	std::ofstream ofs(fileanme);
	if (area.size() == 0) {
		fprintf(stderr, "target corrmap ... null\n");
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = area.begin(); itr != area.end(); itr++) {
			for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++) {



				ofs << std::right << std::fixed
					<< std::setw(5) << std::setprecision(0) << itr2->pl << " "
					<< std::setw(8) << std::setprecision(1) << itr2->x0 << " "
					<< std::setw(8) << std::setprecision(1) << itr2->y0 << " "
					<< std::setw(8) << std::setprecision(1) << itr2->z0 << " "
					<< std::setw(8) << std::setprecision(1) << itr2->x1 << " "
					<< std::setw(8) << std::setprecision(1) << itr2->y1 << " "
					<< std::setw(8) << std::setprecision(1) << itr2->z1 << std::endl;
			}

		}
	}
}