#pragma comment(lib,"FILE_structure.lib")
#pragma comment(lib, "VxxReader.lib")
#include <FILE_structure.hpp>
#include "VxxReader.h"

#include <unordered_map>
#include <set>
#include <map>
#include <omp.h>

void ghost_filter(std::vector<vxx::base_track_t>&base, std::vector<vxx::base_track_t>&base_out);
void ghost_filter(std::vector<vxx::base_track_t>&base, std::vector<vxx::base_track_t>&base_out, std::string filename);

int main(int argc, char**argv) {
	if (argc != 5 && argc != 6) {
		fprintf(stderr, "usage:prg in_bvxx1 in_bvxx2 pl out_fvxx (out_diff.txt)\n");
		exit(1);
	}
	std::string file_in_bvxx1 = argv[1];
	std::string file_in_bvxx2 = argv[2];
	int pl = std::stoi(argv[3]);
	std::string file_out_bvxx = argv[4];
	std::string file_out_diff;
	int output_mode = argc - 5;
	if (output_mode == 1) {
		file_out_diff = argv[5];
	}
	printf("output mode=%d\n", output_mode);
	double x, y;
	int count = 0;
	std::vector<vxx::base_track_t> base1, base2;
	vxx::BvxxReader br;
	base1 = br.ReadAll(file_in_bvxx1, pl, 0);
	base2 = br.ReadAll(file_in_bvxx2, pl, 1);
	std::vector<vxx::base_track_t> base;
	base.reserve(base1.size() + base2.size());
	for (auto itr = base1.begin(); itr != base1.end(); itr++) {
		itr->zone = 0;
		base.push_back(*itr);
	}
	base1.clear();
	base1.shrink_to_fit();
	for (auto itr = base2.begin(); itr != base2.end(); itr++) {
		itr->zone = 1;
		base.push_back(*itr);
	}
	base2.clear();
	base2.shrink_to_fit();


	std::vector<vxx::base_track_t> base_out;
	if (output_mode == 0) {
		ghost_filter(base, base_out);
	}
	else {
		ghost_filter(base, base_out, file_out_diff);
	}
	fprintf(stderr, "ghost filter %lld --> %lld\n", base.size(), base_out.size());

	vxx::BvxxWriter w;
	w.Write(file_out_bvxx, pl, 0, base_out);

}
void ghost_filter(std::vector<vxx::base_track_t>&base, std::vector<vxx::base_track_t>&base_out) {
	double x_min, y_min, ax_min, ay_min;
	double  hash_area = 100, hash_angle = 0.10;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		itr->dmy = 0;
		if (itr == base.begin()) {
			x_min = itr->x;
			y_min = itr->y;
			ax_min = itr->ax;
			ay_min = itr->ay;
		}
		x_min = std::min(x_min, itr->x);
		y_min = std::min(y_min, itr->y);
		ax_min = std::min(ax_min, itr->ax);
		ay_min = std::min(ay_min, itr->ay);
	}
	//printf("%.1lf %.1lf %.4lf %.4lf\n", x_min, y_min, ax_min, ay_min);
	std::tuple<int, int, int, int> id;
	std::multimap < std::tuple<int, int, int, int>, vxx::base_track_t* > base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::get<0>(id) = (itr->x - x_min) / hash_area;
		std::get<1>(id) = (itr->y - y_min) / hash_area;
		std::get<2>(id) = (itr->ax - ax_min) / hash_angle;
		std::get<3>(id) = (itr->ay - ay_min) / hash_angle;
		base_map.insert(std::make_pair(id, &(*itr)));
	}

	//flg=0 未探索
	//flg=1 signal(high PH)
	//flg=2 ghost (low PH)
	int64_t count = 0;
//#pragma omp parallel for private(id)
	for (int i = 0; i < base.size(); i++) {
#pragma omp atomic
		count++;
		if (count % 100000 == 0) {
			fprintf(stderr, "\r ghost filter ...%d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
		}
		//if (count % 100000 == 0) {
		//	fprintf(stderr, "\r ghost filter ...%d/%d(%4.1lf%%) %d", count, base.size(), count*100. / base.size(),base[i].rawid);
		//}
		if (count >= 1650055 ) {
			fprintf(stderr, "\r ghost filter ...%d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
			fprintf(stderr, "\n %d %d %5.4lf %5.4lf %8.1lf %8.1lf\n", base[i].zone, base[i].rawid, base[i].ax, base[i].ay, base[i].x, base[i].y);
		}
		if (base[i].dmy > 0)continue;
		base[i].dmy = 1;
		int iix_range[2], iiy_range[2], iiax_range[2], iiay_range[2];
		double angle;
		angle = sqrt(base[i].ax*base[i].ax + base[i].ay*base[i].ay);
		double dr_pos = angle * 2 + 1;
		double dr_ang = angle * 0.03 + 0.01;
		double dl_pos = 1;
		double dl_ang = 0.005;

		iix_range[0] = ((base[i].x - x_min) - dr_pos) / hash_area;
		iix_range[1] = ((base[i].x - x_min) + dr_pos) / hash_area;
		iiy_range[0] = ((base[i].y - y_min) - dr_pos) / hash_area;
		iiy_range[1] = ((base[i].y - y_min) + dr_pos) / hash_area;
		iiax_range[0] = ((base[i].ax - ax_min) - dr_ang) / hash_angle;
		iiax_range[1] = ((base[i].ax - ax_min) + dr_ang) / hash_angle;
		iiay_range[0] = ((base[i].ay - ay_min) - dr_ang) / hash_angle;
		iiay_range[1] = ((base[i].ay - ay_min) + dr_ang) / hash_angle;
		//printf("%.1lf %.1lf %.4lf %.4lf\n", base[i].x, base[i].y, base[i].ax, base[i].ay);
		//printf("%d %d %d %d %d %d %d %d\n", iix_range[0], iix_range[1], iiy_range[0], iiy_range[1], iiax_range[0], iiax_range[1], iiay_range[0], iiay_range[1]);
		for (int iix = iix_range[0]; iix <= iix_range[1]; iix++) {
			for (int iiy = iiy_range[0]; iiy <= iiy_range[1]; iiy++) {
				for (int iiax = iiax_range[0]; iiax <= iiax_range[1]; iiax++) {
					for (int iiay = iiay_range[0]; iiay <= iiay_range[1]; iiay++) {
						std::get<0>(id) = iix;
						std::get<1>(id) = iiy;
						std::get<2>(id) = iiax;
						std::get<3>(id) = iiay;

						if (base_map.count(id) == 0)continue;
						else {
							auto range = base_map.equal_range(id);
							for (auto res = range.first; res != range.second; res++) {
								if (fabs((res->second->ax - base[i].ax)*base[i].ay - (res->second->ay - base[i].ay)*base[i].ax) > dl_ang*angle)continue;
								if (fabs((res->second->x - base[i].x)*base[i].ay - (res->second->y - base[i].y)*base[i].ax) > dl_pos*angle)continue;
								if (fabs((res->second->ax - base[i].ax)*base[i].ax + (res->second->ay - base[i].ay)*base[i].ay) > dr_ang*angle)continue;
								if (fabs((res->second->x - base[i].x)*base[i].ax + (res->second->y - base[i].y)*base[i].ay) > dr_pos*angle)continue;
								if (res->second->zone == base[i].zone&&res->second->rawid == base[i].rawid)continue;
								if (base[i].m[0].ph + base[i].m[1].ph > res->second->m[0].ph + res->second->m[1].ph) {
									res->second->dmy = 2;
								}
								else if (base[i].m[0].ph + base[i].m[1].ph < res->second->m[0].ph + res->second->m[1].ph) {
									base[i].dmy = 2;
								}
								else {
									if (res->second->dmy >= 0.9&&res->second->dmy <= 1.1) {
										base[i].dmy = 2;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	fprintf(stderr, "\r ghost filter ...%d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

	count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		//printf("%lf %lf\n", itr->px, itr->py);
		if (itr->dmy < 0.9) {
			fprintf(stderr, "exception not searched track\n");
			exit(1);
		}
		else if (itr->dmy >= 0.9&&itr->dmy <= 1.1) {
			base_out.push_back(*itr);
			count++;
		}
	}
	fprintf(stderr, "ghost filter %d --> %d(%4.1lf%%)\n", base.size(), count, count*100. / base.size());
}
void ghost_filter(std::vector<vxx::base_track_t>&base, std::vector<vxx::base_track_t>&base_out, std::string filename) {
	//ghost pair output
	double x_min, y_min, ax_min, ay_min;
	double  hash_area = 100, hash_angle = 0.10;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		itr->dmy = 0;
		if (itr == base.begin()) {
			x_min = itr->x;
			y_min = itr->y;
			ax_min = itr->ax;
			ay_min = itr->ay;
		}
		x_min = std::min(x_min, itr->x);
		y_min = std::min(y_min, itr->y);
		ax_min = std::min(ax_min, itr->ax);
		ay_min = std::min(ay_min, itr->ay);
	}
	std::tuple<int, int, int, int> id;
	std::multimap < std::tuple<int, int, int, int>, vxx::base_track_t* > base_map;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		std::get<0>(id) = (itr->x - x_min) / hash_area;
		std::get<1>(id) = (itr->y - y_min) / hash_area;
		std::get<2>(id) = (itr->ax - ax_min) / hash_angle;
		std::get<3>(id) = (itr->ay - ay_min) / hash_angle;
		base_map.insert(std::make_pair(id, &(*itr)));
	}

	//ghostのrawidをいったん入れる
	std::set<std::tuple<int, int, int, int>> ghost;
	//flg=0 未探索
	//flg=1 signal(high PH)
	//flg=2 ghost (low PH)
	int64_t count = 0;
#pragma omp parallel for private(id)
	for (int i = 0; i < base.size(); i++) {
#pragma omp atomic
		count++;
		if (count % 100000 == 0) {
			fprintf(stderr, "\r ghost filter ...%d/%d(%4.1lf%%)", count, base.size(), count*100. / base.size());
		}
		if (base[i].dmy > 0)continue;
		base[i].dmy = 1;
		int iix_range[2], iiy_range[2], iiax_range[2], iiay_range[2];
		double angle;
		angle = sqrt(base[i].ax*base[i].ax + base[i].ay*base[i].ay);
		double dr_pos = angle * 2 + 1;
		double dr_ang = angle * 0.03 + 0.01;
		double dl_pos = 1;
		double dl_ang = 0.005;

		iix_range[0] = ((base[i].x - x_min) - dr_pos) / hash_area;
		iix_range[1] = ((base[i].x - x_min) + dr_pos) / hash_area;
		iiy_range[0] = ((base[i].y - y_min) - dr_pos) / hash_area;
		iiy_range[1] = ((base[i].y - y_min) + dr_pos) / hash_area;
		iiax_range[0] = ((base[i].ax - ax_min) - dr_ang) / hash_angle;
		iiax_range[1] = ((base[i].ax - ax_min) + dr_ang) / hash_angle;
		iiay_range[0] = ((base[i].ay - ay_min) - dr_ang) / hash_angle;
		iiay_range[1] = ((base[i].ay - ay_min) + dr_ang) / hash_angle;
		//printf("%d %d %d %d %d %d %d %d\n", iix_range[0], iix_range[1], iiy_range[0], iiy_range[1], iiax_range[0], iiax_range[1], iiay_range[0], iiay_range[1]);
		for (int iix = iix_range[0]; iix <= iix_range[1]; iix++) {
			for (int iiy = iiy_range[0]; iiy <= iiy_range[1]; iiy++) {
				for (int iiax = iiax_range[0]; iiax <= iiax_range[1]; iiax++) {
					for (int iiay = iiay_range[0]; iiay <= iiay_range[1]; iiay++) {
						std::get<0>(id) = iix;
						std::get<1>(id) = iiy;
						std::get<2>(id) = iiax;
						std::get<3>(id) = iiay;

						if (base_map.count(id) == 0)continue;
						else {
							auto range = base_map.equal_range(id);
							for (auto res = range.first; res != range.second; res++) {
								if (fabs((res->second->ax - base[i].ax)*base[i].ay - (res->second->ay - base[i].ay)*base[i].ax) > dl_ang*angle)continue;
								if (fabs((res->second->x - base[i].x)*base[i].ay - (res->second->y - base[i].y)*base[i].ax) > dl_pos*angle)continue;
								if (fabs((res->second->ax - base[i].ax)*base[i].ax + (res->second->ay - base[i].ay)*base[i].ay) > dr_ang*angle)continue;
								if (fabs((res->second->x - base[i].x)*base[i].ax + (res->second->y - base[i].y)*base[i].ay) > dr_pos*angle)continue;
								if (res->second->zone == base[i].zone&&res->second->rawid == base[i].rawid)continue;

#pragma omp critical
								{
									if (base[i].zone < res->second->zone) {
										ghost.insert(std::make_tuple(base[i].zone, base[i].rawid, res->second->zone, res->second->rawid));
									}
									else if (base[i].zone > res->second->zone) {
										ghost.insert(std::make_tuple(res->second->zone, res->second->rawid, base[i].zone, base[i].rawid));
									}
									else if (base[i].rawid < res->second->rawid) {
										ghost.insert(std::make_tuple(base[i].zone, base[i].rawid, res->second->zone, res->second->rawid));
									}
									else {
										ghost.insert(std::make_tuple(res->second->zone, res->second->rawid, base[i].zone, base[i].rawid));
									}
								}

								if (base[i].m[0].ph + base[i].m[1].ph > res->second->m[0].ph + res->second->m[1].ph) {
									res->second->dmy = 2;
								}
								else if (base[i].m[0].ph + base[i].m[1].ph < res->second->m[0].ph + res->second->m[1].ph) {
									base[i].dmy = 2;
								}
								else {
									if (res->second->dmy >= 0.9&&res->second->dmy <= 1.1) {
										base[i].dmy = 2;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	fprintf(stderr, "\r ghost filter ...%d/%d(%4.1lf%%)\n", count, base.size(), count*100. / base.size());

	count = 0;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		//printf("%lf %lf\n", itr->px, itr->py);
		if (itr->dmy < 0.9) {
			fprintf(stderr, "exception not searched track\n");
			exit(1);
		}
		else if (itr->dmy >= 0.9&&itr->dmy <= 1.1) {
			base_out.push_back(*itr);
			count++;
		}
	}
	fprintf(stderr, "ghost filter %d --> %d(%4.1lf%%)\n", base.size(), count, count*100. / base.size());

	std::map<int, vxx::base_track_t *> base_rawid0;
	std::map<int, vxx::base_track_t *> base_rawid1;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		if (itr->zone == 0) {
			base_rawid0.insert(std::make_pair(itr->rawid, &(*itr)));
		}
		else {
			base_rawid1.insert(std::make_pair(itr->rawid, &(*itr)));
		}
	}
	std::ofstream ofs(filename);
	vxx::base_track_t t0, t1;
	for (auto itr = ghost.begin(); itr != ghost.end(); itr++) {
		if (std::get<0>(*itr) == 0) {
			t0 = *(base_rawid0.find(std::get<1>(*itr))->second);
		}
		else {
			t0 = *(base_rawid1.find(std::get<1>(*itr))->second);
		}
		if (std::get<2>(*itr) == 0) {
			t1 = *(base_rawid0.find(std::get<3>(*itr))->second);
		}
		else {
			t1 = *(base_rawid1.find(std::get<3>(*itr))->second);
		}
		ofs << std::right << std::fixed
			<< std::setw(12) << std::setprecision(0) << t0.rawid << " "
			<< std::setw(3) << std::setprecision(0) << t0.m[0].zone << " "
			<< std::setw(7) << std::setprecision(0) << t0.m[0].ph + t0.m[1].ph << " "
			<< std::setw(7) << std::setprecision(4) << t0.ax << " "
			<< std::setw(7) << std::setprecision(4) << t0.ay << " "
			<< std::setw(10) << std::setprecision(1) << t0.x << " "
			<< std::setw(10) << std::setprecision(1) << t0.y << " "
			<< std::setw(10) << std::setprecision(1) << t0.z << " "
			<< std::setw(12) << std::setprecision(0) << t1.rawid << " "
			<< std::setw(3) << std::setprecision(0) << t1.m[0].zone << " "
			<< std::setw(7) << std::setprecision(0) << t1.m[0].ph + t1.m[1].ph << " "
			<< std::setw(7) << std::setprecision(4) << t1.ax << " "
			<< std::setw(7) << std::setprecision(4) << t1.ay << " "
			<< std::setw(10) << std::setprecision(1) << t1.x << " "
			<< std::setw(10) << std::setprecision(1) << t1.y << " "
			<< std::setw(10) << std::setprecision(1) << t1.z << std::endl;
	}
}
