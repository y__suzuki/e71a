#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>

#include <filesystem>

std::map<int, mfile0::M_Base> read_base_OSS(std::string file_path);
std::map<int, mfile0::M_Base> read_base_FSS(std::string file_path);
std::map<int, mfile0::M_Base> read_base_TSS(std::string file_path);
std::map<int, mfile0::M_Base> read_base_PL3(std::string file_path);
std::map<int, mfile0::M_Base> read_base_PL4(std::string file_path);

void trans_local(std::multimap<int, mfile0::M_Base*>&base_map_single, std::map<int, std::vector<corrmap_3d::align_param2>> &corr);

int main(int argc, char**argv) {
	if (argc != 5) {
		fprintf(stderr, "usage:sharing-file ali-local file-path output\n");
		exit(1);
	}
	std::string file_in_sf = argv[1];
	std::string file_in_ali = argv[2];
	std::string file_in_basetrack_path = argv[3];
	std::string file_out_mfile=argv[4];

	std::vector<Sharing_file::Sharing_file> sf = Sharing_file::Read_sharing_file_bin(file_in_sf);

	std::string file_in_corrmap = file_in_ali;
	std::map<int, std::vector<corrmap_3d::align_param>>corrmap = corrmap_3d::read_ali_param_abs(file_in_corrmap, 1);
	std::map<int, std::vector<corrmap_3d::align_param2>>corrmap_dd = corrmap_3d::DelaunayDivide_map(corrmap);

	std::map<int, mfile0::M_Base> base_oss = read_base_OSS(file_in_basetrack_path);
	std::map<int, mfile0::M_Base> base_fss = read_base_FSS(file_in_basetrack_path);
	std::map<int, mfile0::M_Base> base_tss = read_base_TSS(file_in_basetrack_path);
	std::map<int, mfile0::M_Base> base_pl3 = read_base_PL3(file_in_basetrack_path);
	std::map<int, mfile0::M_Base> base_pl4 = read_base_PL4(file_in_basetrack_path);

	mfile0::Mfile m;
	mfile0::set_header(1, 133, m);
	std::vector<int> pos_all = { 31,41,111,411,611 };
	m.header.all_pos = pos_all;
	m.header.num_all_plate = pos_all.size();

	for (auto itr = sf.begin(); itr != sf.end(); itr++) {
		mfile0::M_Chain c;
		c.chain_id = itr->eventid;
		if (itr->pl == 3 && base_pl3.count(itr->ecc_id) == 1) c.basetracks.push_back(base_pl3.at(itr->ecc_id));
		if (itr->pl == 4 && base_pl4.count(itr->ecc_id) == 1) c.basetracks.push_back(base_pl4.at(itr->ecc_id));
		if (base_oss.count(itr->oss_id) == 1) c.basetracks.push_back(base_oss.at(itr->oss_id));
		if (base_fss.count(itr->fixedwall_id) == 1) c.basetracks.push_back(base_fss.at(itr->fixedwall_id));
		if (base_tss.count(itr->trackerwall_id) == 1) c.basetracks.push_back(base_tss.at(itr->trackerwall_id));

		for (int i = 0; i < c.basetracks.size(); i++) {
			c.basetracks[i].group_id = itr->eventid;
		}

		c.nseg = c.basetracks.size();
		c.pos0 = 0;
		c.pos1 = 0;
		if (c.nseg == 0)continue;
		m.chains.push_back(c);
	}

	std::multimap<int, mfile0::M_Base*>base_map_pl;
	for (auto itr = m.chains.begin(); itr != m.chains.end(); itr++) {
		for (auto itr2 = itr->basetracks.begin(); itr2 != itr->basetracks.end(); itr2++) {
			base_map_pl.insert(std::make_pair(itr2->pos / 10, &(*itr2)));
		}
	}

	trans_local(base_map_pl, corrmap_dd);
	mfile0::write_mfile(file_out_mfile, m);

}
std::map<int, mfile0::M_Base> read_base_OSS(std::string file_path) {
	std::string filename = file_path + "\\PL011\\b011.acryl.merge.vxx";
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(filename, 11, 0);

	std::map<int, mfile0::M_Base> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		mfile0::M_Base b;
		b.ax = itr->ax;
		b.ay = itr->ay;
		b.x = itr->x;
		b.y = itr->y;
		b.z = 0;
		b.group_id = 0;
		b.ph = itr->m[0].ph + itr->m[1].ph;
		b.pos = itr->pl * 10 + 1;
		b.rawid = itr->rawid;
		b.flg_i[0] = 0;
		b.flg_i[1] = 0;
		b.flg_i[2] = 0;
		b.flg_i[3] = 0;
		b.flg_d[0] = 0;
		b.flg_d[1] = 0;
		ret.insert(std::make_pair(itr->rawid, b));
	}
	return ret;
}
std::map<int, mfile0::M_Base> read_base_PL3(std::string file_path) {
	std::string filename = file_path + "\\PL003\\b003.acryl.merge.vxx";
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(filename, 3, 0);

	std::map<int, mfile0::M_Base> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		mfile0::M_Base b;
		b.ax = itr->ax;
		b.ay = itr->ay;
		b.x = itr->x;
		b.y = itr->y;
		b.z = 0;
		b.group_id = 0;
		b.ph = itr->m[0].ph + itr->m[1].ph;
		b.pos = itr->pl * 10 + 1;
		b.rawid = itr->rawid;
		b.flg_i[0] = 0;
		b.flg_i[1] = 0;
		b.flg_i[2] = 0;
		b.flg_i[3] = 0;
		b.flg_d[0] = 0;
		b.flg_d[1] = 0;
		ret.insert(std::make_pair(itr->rawid, b));
	}
	return ret;
}
std::map<int, mfile0::M_Base> read_base_PL4(std::string file_path) {
	std::string filename = file_path + "\\PL004\\b004.acryl.merge.vxx";
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(filename, 4, 0);

	std::map<int, mfile0::M_Base> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		mfile0::M_Base b;
		b.ax = itr->ax;
		b.ay = itr->ay;
		b.x = itr->x;
		b.y = itr->y;
		b.z = 0;
		b.group_id = 0;
		b.ph = itr->m[0].ph + itr->m[1].ph;
		b.pos = itr->pl * 10 + 1;
		b.rawid = itr->rawid;
		b.flg_i[0] = 0;
		b.flg_i[1] = 0;
		b.flg_i[2] = 0;
		b.flg_i[3] = 0;
		b.flg_d[0] = 0;
		b.flg_d[1] = 0;
		ret.insert(std::make_pair(itr->rawid, b));
	}
	return ret;
}
std::map<int, mfile0::M_Base> read_base_FSS(std::string file_path) {
	std::string filename = file_path + "\\PL041\\b041.acryl.merge.vxx";
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(filename, 41, 0);

	std::map<int, mfile0::M_Base> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		mfile0::M_Base b;
		b.ax = itr->ax;
		b.ay = itr->ay;
		b.x = itr->x;
		b.y = itr->y;
		b.z = 0;
		b.group_id = 0;
		b.ph = itr->m[0].ph + itr->m[1].ph;
		b.pos = itr->pl * 10 + 1;
		b.rawid = itr->rawid;
		b.flg_i[0] = 0;
		b.flg_i[1] = 0;
		b.flg_i[2] = 0;
		b.flg_i[3] = 0;
		b.flg_d[0] = 0;
		b.flg_d[1] = 0;
		ret.insert(std::make_pair(itr->rawid, b));
	}
	return ret;
}
std::map<int, mfile0::M_Base> read_base_TSS(std::string file_path) {
	std::string filename = file_path + "\\PL061\\b061.acryl.merge.vxx";
	vxx::BvxxReader br;
	std::vector<vxx::base_track_t> base = br.ReadAll(filename, 61, 0);

	std::map<int, mfile0::M_Base> ret;
	for (auto itr = base.begin(); itr != base.end(); itr++) {
		mfile0::M_Base b;
		b.ax = itr->ax;
		b.ay = itr->ay;
		b.x = itr->x;
		b.y = itr->y;
		b.z = 0;
		b.group_id = 0;
		b.ph = itr->m[0].ph + itr->m[1].ph;
		b.pos = itr->pl * 10 + 1;
		b.rawid = itr->rawid;
		b.flg_i[0] = 0;
		b.flg_i[1] = 0;
		b.flg_i[2] = 0;
		b.flg_i[3] = 0;
		b.flg_d[0] = 0;
		b.flg_d[1] = 0;
		ret.insert(std::make_pair(itr->rawid, b));
	}
	return ret;
}


void trans_local(std::multimap<int, mfile0::M_Base*>&base_map_single, std::map<int, std::vector<corrmap_3d::align_param2>> &corr) {
	int count = 0;
	for (auto itr = base_map_single.begin(); itr != base_map_single.end(); itr++) {
		count = base_map_single.count(itr->first);
		int pl = itr->first;
		printf("PL%03d basetrack tans\n", pl);
		if (corr.count(pl) == 0) {
			fprintf(stderr, "PL%03d corrmap not found\n", pl);
		}
		std::vector<corrmap_3d::align_param2> param = corr.at(pl);

		std::vector< mfile0::M_Base*> base_trans;
		auto range = base_map_single.equal_range(itr->first);
		for (auto res = range.first; res != range.second; res++) {
			base_trans.push_back(res->second);
		}
		std::vector <std::pair<mfile0::M_Base*, corrmap_3d::align_param2*>> base_trans_map = corrmap_3d::track_affineparam_correspondence(base_trans, param);
		trans_base_all(base_trans_map);


		itr = std::next(itr, count - 1);
	}

}