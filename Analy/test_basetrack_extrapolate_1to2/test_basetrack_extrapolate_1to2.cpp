//数字の小さいPLから大きいPLにbasetrackを外挿する。
//下流-->上流
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip> 
#include <set>
//basetrackに含まれるmicrotrack情報のクラス
class micro_track_subset_t {
public:
	double ax, ay;
	double z;
	int ph;
	int pos, col, row, zone, isg;
	int64_t rawid;
};
//basetrackのクラス
class base_track_t {
public:
	double ax, ay;
	double x, y, z;
	int pl;
	int isg, zone;
	int dmy;    // In ROOT, you will have to add this member because CINT does not handle 8byte alignment. 
	int64_t rawid;
	micro_track_subset_t m[2];
};
//corrmapのクラス
class Corrmap {
public:
	int id, pos[2];
	double areax[2], areay[2], position[6], angle[6], dz, signal, background, SN, rms_pos[2], rms_angle[2];
	double notuse_d[5];
	int notuse_i[9];
};

//関数プロトタイプ　宣言
std::vector<base_track_t> read_basetrack_bin(std::string filename, int output = 1);
std::vector<Corrmap> read_corrmap(std::string filename, int output = 1);
std::set<int> read_id_list(std::string filename);

base_track_t basetrack_extrapolate(base_track_t base, std::vector<Corrmap> &corr, double gap);
Corrmap search_corrmap(std::vector<Corrmap> &corr, double x, double y);
base_track_t basetrack_trans_inv(base_track_t base, Corrmap param);

void write_basetrack_txt(std::string filename, std::vector<base_track_t> &base);

//main関数
int main(int argc, char**argv) {
	//argc:引数の数
	//argv:引数の文字列

	//引数の数が間違っていた場合usage出力
	if (argc != 6) {
		fprintf(stderr, "usage:prg input-basetrack-bin corrmap-align nominal-gap rawid-list output-basetrack\n");
		//prg終了
		exit(1);
	}

	//argv[x]:x番目の引数の文字列
	//argv[0]:prg名
	std::string file_in_basetrack = argv[1];
	std::string file_in_corrmap = argv[2];
	double nominal_gap = std::stod(argv[3]);
	std::string file_in_id_list = argv[4];
	std::string file_out_basetrack = argv[5];

	//basetrackの読み込み
	std::vector<base_track_t> base = read_basetrack_bin(file_in_basetrack);
	
	//corrmapの読み込み
	std::vector<Corrmap> corr = read_corrmap(file_in_corrmap);

	//rawid listの読み込み
	std::set<int> rawid_list = read_id_list(file_in_id_list);

	std::vector<base_track_t> base_extra;
	for(auto itr=base.begin();itr!=base.end();itr++){
		//id listにないものは飛ばす
		if (rawid_list.count(itr->rawid) == 0)continue;
		//外挿
		//このprgのコアの部分
		base_extra.push_back(basetrack_extrapolate(*itr, corr, std::fabs(nominal_gap)));
	}

	//ファイル出力(txt出力)
	write_basetrack_txt(file_out_basetrack, base_extra);
}
std::vector<base_track_t> read_basetrack_bin(std::string filename,int output) {

	std::vector<base_track_t> ret;
	//バイナリモードでfile open
	std::ifstream ifs(filename, std::ios::binary);
	//filesize取得
	ifs.seekg(0, std::ios::end);
	int64_t eofpos = ifs.tellg();
	ifs.clear();
	ifs.seekg(0, std::ios::beg);
	int64_t begpos = ifs.tellg();
	int64_t nowpos = ifs.tellg();
	int64_t size2 = eofpos - begpos;
	int64_t GB = size2 / (1000 * 1000 * 1000);
	int64_t MB = (size2 - GB * 1000 * 1000 * 1000) / (1000 * 1000);
	int64_t KB = (size2 - GB * 1000 * 1000 * 1000 - MB * 1000 * 1000) / (1000);
	if (output == 1) {
		if (GB > 0) {
			std::cout << "FILE size :" << GB << "." << MB << " [GB]" << std::endl;
		}
		else {
			std::cout << "FILE size :" << MB << "." << KB << " [MB]" << std::endl;
		}
	}
	base_track_t b;
	int count = 0;
	//basetrackの情報1track分ずつ読み込み-->vectorに詰める
	while (ifs.read((char*)& b, sizeof(base_track_t))) {
		if (count % 10000 == 0) {
			nowpos = ifs.tellg();
			auto size1 = nowpos - begpos;
			std::cerr << std::right << std::fixed << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%";
		}
		count++;
		ret.emplace_back(b);
	}
	auto size1 = eofpos - begpos;
	std::cerr << "\r now reading ..." << std::setw(4) << std::setprecision(1) << size1 * 100. / size2 << "%" << std::endl;;
	if (count == 0) {
		fprintf(stderr, "%s no basetrack!\n", filename.c_str());
		exit(1);
	}
	return ret;
}
std::vector<Corrmap> read_corrmap(std::string filename, int output) {
	std::vector<Corrmap> ret;
	//ファイルをテキストモードでopen
	//バイナリに比べて速度は遅い
	std::ifstream ifs(filename);
	Corrmap buffer;

	while (ifs >> buffer.id >> buffer.pos[0] >> buffer.pos[1] >> buffer.areax[0] >> buffer.areax[1] >> buffer.areay[0] >> buffer.areay[1] >>
		buffer.position[0] >> buffer.position[1] >> buffer.position[2] >> buffer.position[3] >> buffer.position[4] >> buffer.position[5] >>
		buffer.angle[0] >> buffer.angle[1] >> buffer.angle[2] >> buffer.angle[3] >> buffer.angle[4] >> buffer.angle[5] >>
		buffer.dz >> buffer.signal >> buffer.background >> buffer.SN >> buffer.rms_pos[0] >> buffer.rms_pos[1] >>
		buffer.notuse_d[0] >> buffer.notuse_d[1] >> buffer.rms_angle[0] >> buffer.rms_angle[1] >>
		buffer.notuse_i[0] >> buffer.notuse_i[1] >> buffer.notuse_i[2] >> buffer.notuse_i[3] >> buffer.notuse_i[4] >> buffer.notuse_i[5] >>
		buffer.notuse_i[6] >> buffer.notuse_i[7] >> buffer.notuse_i[8] >>
		buffer.notuse_d[2] >> buffer.notuse_d[3] >> buffer.notuse_d[4]) {
		Corrmap *cormap = new Corrmap();
		cormap = &buffer;
		ret.push_back(*cormap);
	}
	if (output == 1) {
		fprintf(stderr, "%s input finish\n", filename.c_str());
	}
	if (ret.size() == 0) {
		fprintf(stderr, "%s alignment miss!\n", filename.c_str());
		exit(1);
	}
	return ret;
}
std::set<int> read_id_list(std::string filename) {
	std::set<int> ret;
	std::ifstream ifs(filename);
	int id;
	while (ifs >> id) {
		ret.insert(id);
	}
	return ret;
}

base_track_t basetrack_extrapolate(base_track_t base, std::vector<Corrmap> &corr, double gap) {
	//外挿先のbasetrackを用意
	base_track_t next_base = base;
	//PL番号の設定
	next_base.pl = corr.begin()->pos[1] / 10;
	next_base.m[0].pos = next_base.pl * 10 + 1;
	next_base.m[1].pos = next_base.pl * 10 + 2;
	//gap分外挿(gap>0を仮定)
	next_base.x = base.x - 1 * gap*base.ax;
	next_base.y = base.y - 1 * gap*base.ay;

	Corrmap param = search_corrmap(corr, next_base.x, next_base.y);
	//dzの補正を適用した外挿先を求める
	//このあたりzの符号がややこしいので注意
	next_base.x = base.x + (-1 * gap + param.dz)*base.ax;
	next_base.y = base.y + (-1 * gap + param.dz)*base.ay;
	
	param = search_corrmap(corr, next_base.x, next_base.y);
	//parameterで逆変換
	next_base = basetrack_trans_inv(next_base, param);

	return next_base;
}
Corrmap search_corrmap(std::vector<Corrmap> &corr, double x, double y) {
	Corrmap ret;
	double center_x, center_y, distance;
	for (auto itr = corr.begin(); itr != corr.end(); itr++) {
		center_x = (itr->areax[0] + itr->areax[1]) / 2;
		center_y = (itr->areay[0] + itr->areay[1]) / 2;
		if (itr == corr.begin()) {
			distance = pow(center_x - x, 2) + pow(center_y - y, 2);
			ret = *itr;
		}
		//補正mapの領域中心からの距離一番近いものを選ぶ
		if (distance > pow(center_x - x, 2) + pow(center_y - y, 2)) {
			distance = pow(center_x - x, 2) + pow(center_y - y, 2);
			ret = *itr;
		}
	}
	return ret;
}
base_track_t basetrack_trans_inv(base_track_t base, Corrmap param) {
	double tmp_x, tmp_y,factor;

	factor = 1 / (param.position[0] * param.position[3] - param.position[1] * param.position[2]);
	tmp_x = base.x - param.position[4];
	tmp_y = base.y - param.position[5];
	base.x = factor * (tmp_x*param.position[3] - tmp_y * param.position[1]);
	base.y = factor * (tmp_y*param.position[0] - tmp_x * param.position[2]);
	
	factor = 1 / (param.angle[0] * param.angle[3] - param.angle[1] * param.angle[2]);
	tmp_x = base.ax - param.angle[4];
	tmp_y = base.ay - param.angle[5];
	base.ax = factor * (tmp_x*param.angle[3] - tmp_y * param.angle[1]);
	base.ay = factor * (tmp_y*param.angle[0] - tmp_x * param.angle[2]);

	return base;
}

void write_basetrack_txt(std::string filename, std::vector<base_track_t> &base) {
	std::ofstream ofs(filename);
	if (!ofs) {
		//file open 失敗
		fprintf(stderr, "File[%s] is not exist!!\n", filename.c_str());
		exit(1);
	}
	if (base.size() == 0) {
		fprintf(stderr, "target linklet ... null\n");
		fprintf(stderr, "File[%s] has no text\n", filename.c_str());
	}
	else {
		int count = 0;
		std::cout << std::right << std::fixed;
		for (auto itr = base.begin(); itr != base.end(); itr++) {
			if (count % 10000 == 0) {
				fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)", count, int(base.size()), count*100. / base.size());
			}
			count++;
			ofs << std::right << std::fixed
				<< std::setw(10) << std::setprecision(0) << itr->rawid << " "
				<< std::setw(4) << std::setprecision(0) << itr->pl << " "
				<< std::setw(4) << std::setprecision(0) << itr->isg << " "
				<< std::setw(7) << std::setprecision(4) << itr->ax << " "
				<< std::setw(7) << std::setprecision(4) << itr->ay << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << " "

				<< std::setw(7) << std::setprecision(0) << itr->m[0].ph << " "
				<< std::setw(7) << std::setprecision(4) << itr->m[0].ax << " "
				<< std::setw(7) << std::setprecision(4) << itr->m[0].ay << " "
				<< std::setw(8) << std::setprecision(1) << itr->x << " "
				<< std::setw(8) << std::setprecision(1) << itr->y << " "
				<< std::setw(6) << std::setprecision(1) << itr->m[0].z << " "

				<< std::setw(4) << std::setprecision(0) << itr->m[0].pos << " "
				<< std::setw(3) << std::setprecision(0) << itr->m[0].col << " "
				<< std::setw(3) << std::setprecision(0) << itr->m[0].row << " "
				<< std::setw(3) << std::setprecision(0) << itr->m[0].zone << " "
				<< std::setw(4) << std::setprecision(0) << itr->m[0].isg << " "
				<< std::setw(12) << std::setprecision(0) << itr->m[0].rawid << " "

				<< std::setw(7) << std::setprecision(0) << itr->m[1].ph << " "
				<< std::setw(7) << std::setprecision(4) << itr->m[1].ax << " "
				<< std::setw(7) << std::setprecision(4) << itr->m[1].ay << " "
				<< std::setw(8) << std::setprecision(1) << itr->x + (itr->m[1].z - itr->m[0].z)*itr->ax << " "
				<< std::setw(8) << std::setprecision(1) << itr->y + (itr->m[1].z - itr->m[0].z)*itr->ay << " "
				<< std::setw(6) << std::setprecision(1) << itr->m[1].z << " "

				<< std::setw(4) << std::setprecision(0) << itr->m[1].pos << " "
				<< std::setw(3) << std::setprecision(0) << itr->m[1].col << " "
				<< std::setw(3) << std::setprecision(0) << itr->m[1].row << " "
				<< std::setw(3) << std::setprecision(0) << itr->m[1].zone << " "
				<< std::setw(4) << std::setprecision(0) << itr->m[1].isg << " "
				<< std::setw(12) << std::setprecision(0) << itr->m[1].rawid << std::endl;
		}
		fprintf(stderr, "\r Write Basetrack ... %d/%d (%4.1lf%%)\n", count, int(base.size()), count*100. / base.size());
	}
}
