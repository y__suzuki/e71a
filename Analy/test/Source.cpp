#define _CRT_NONSTDC_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <filesystem>
#include <iostream>
#include "windows.h"
#include <filesystem>
#include <time.h>
#include <ctime>
#include <math.h>

void product_vec(std::vector<double> v, int &exp, double &base);
double Calc_total_CL(std::vector<double> &calc_val);

void log_div_factorial_part(int&k, int max, double log_p, std::vector<std::pair<int, double>>&result, std::pair<int, double>&preb_res);

int main() {
	float f= 1;
	double d=1;
	long double ld = 1;
	printf("float %d\n", sizeof(float));
	printf("double %d\n", sizeof(double));
	printf("long double %d\n", sizeof(long double));

	double x = 0.1234567890123456789;
	double base_part;
	int exp_part;

	base_part = std::frexpl(x, &exp_part);
	printf("%.30lf\n", x);
	printf("%d\n", exp_part);
	printf("%.30lf\n", base_part);
	printf("%.30lf\n", base_part*pow(2, exp_part));

	printf("\n-----------------------------\n");
	std::vector<double>v{ 
		0.0002
,0.0002
,0.0008
,0.0009
,0.0015
,0.0009
,0.0009
,0.0009
,0.0007
,0.0011
,0.0009
,0.0011
,0.0007
,0.0013
,0.0006
,0.0011
,0.0087
,0.0069
,0.001
,0.0013
,0.0011
,0.001
,0.0035
,0.001
,0.0012
,0.0009
,0.0014
,0.0013
,0.0013
,0.0013
,0.0016
,0.0018
,0.0012
,0.0016
,0.0768
,0.0013
,0.0019
,0.0014
,0.0021
,0.0042
,0.0116
,0.0023
,0.0011
,0.0014
,0.0014
,0.0012
,0.0023
,0.012
,0.0013
,0.0012
,0.0021
,0.0019
,0.0019
,0.0015
,0.0011
,0.0024
,0.0015
,0.007
,0.0086
,0.0011
,0.0018
,0.0014
,0.0021
,0.0048
,0.0712
,0.0016
,0.0019
,0.0038
,0.0018
,0.0018
,0.0016
,0.0017
,0.0152
,0.0013
,0.0043
,0.0021
,0.0022
,0.0016
,0.0013
,0.0018

	
	};
	int exp;
		double base;
	product_vec(v, exp, base);
	printf("exp=%d\n", exp);
	printf("base=%.15lf\n", base);
	std::cout << std::scientific << std::setprecision(30) << base * pow(2, exp) << std::endl;
	
	double res = Calc_total_CL(v);
	std::cout << "total CL=" << std::scientific << std::setprecision(30) << res << std::endl;
	std::cout << "double min=" << std::scientific << std::setprecision(30) << DBL_MIN << std::endl;

	int exp_max;
	res = std::frexpl(DBL_MIN, &exp_max);
	printf("exp=%d\n", exp_max);
	printf("base=%.15lf\n", res);

	//7.8959E-134

	exit(0);


	//std::string filename = "I:\\NINJA\\E71a\\work\\suzuki\\image_bug\\Area2\\PL027\\DATA\\02_02\\TrackHit2_0_00000005_0_000.spng";
	std::string filename = "I:\\NINJA\\E71a\\work\\suzuki\\image_bug\\error_inf.txt";
	std::filesystem::file_time_type file_time = std::filesystem::last_write_time(filename);

	auto sec = std::chrono::duration_cast<std::chrono::seconds>(file_time.time_since_epoch());
	std::time_t t = sec.count();
	const tm* lt = std::localtime(&t);
	std::cout << std::endl << std::put_time(lt, "%Y/%m/%d %T") << std::endl;
	exit(0);

	// 情報収集
	MEMORYSTATUSEX msex = { sizeof(MEMORYSTATUSEX) };
	GlobalMemoryStatusEx(&msex);

	// 物理メモリの使用容量
	DWORDLONG ullUsed = (msex.ullTotalPhys - msex.ullAvailPhys);
	// 物理メモリの空き容量
	DWORDLONG ullFree = (msex.ullAvailPhys);
	// 物理メモリの搭載容量
	DWORDLONG ullSize = (msex.ullTotalPhys);

	printf("dwSize %d\n", ullSize);
	system("pause");
	return 0;

	std::string file_name = "P:\\NINJA\\E71a\\tracking\\ECC6\\Area2\\PL001\\DATA";

	size_t file_size = 0;
	for (const auto & entry : std::filesystem::directory_iterator(file_name)) {
		std::cout << entry.path()<<" file size"<<file_size << std::endl;

		for (const auto & entry2 : std::filesystem::directory_iterator(entry.path())) {
			file_size += std::filesystem::file_size(entry2.path());
		}
	}





	system("pause");
	return 0;
	std::vector<int> v0(100);
	std::vector<int> v1(100,1);
	std::vector<int> v2;
	v2.reserve(100);
	for (auto itr = v0.begin(); itr != v0.end(); itr++) {
		printf("v0 %d\n", *itr);
	}
	for (auto itr = v1.begin(); itr != v1.end(); itr++) {
		printf("v1 %d\n", *itr);
	}
	for (auto itr = v2.begin(); itr != v2.end(); itr++) {
		printf("v2 %d\n", *itr);
	}
	system("pause");
}
double Calc_total_CL(std::vector<double> &calc_val) {

	std::pair<int, double> p_product;
	//CLの積を計算
	product_vec(calc_val, p_product.first, p_product.second);
	double ln_p = log(p_product.second) + p_product.first*log(2.);

	std::vector<std::pair<int, double>> each_log_div_fact;
	std::pair<int, double> tmp_res;
	int k = 0;
	log_div_factorial_part(k, calc_val.size() - 1, ln_p, each_log_div_fact, tmp_res);

	std::vector<std::pair<int, double>> before_sum;
	for (auto itr = each_log_div_fact.begin(); itr != each_log_div_fact.end(); itr++) {
		std::cout << "-log/fact =" << std::scientific << std::setprecision(30) << itr->second*pow(2,itr->first) << std::endl;

		tmp_res.first = itr->first + p_product.first;
		tmp_res.second = itr->second*p_product.second;
		before_sum.push_back(tmp_res);
	}
	double res = 0;
	for (auto itr = before_sum.begin(); itr != before_sum.end(); itr++) {
		res += itr->second*pow(2, itr->first);
	}
	return res;


}
void product_vec(std::vector<double> v, int &exp, double &mant) {
	std::vector<int> exp_v;
	std::vector<double> mant_v;
	double mant_part;
	int exp_part;
	//変数をすべて指数と仮数に分離
	for (auto itr = v.begin(); itr != v.end(); itr++) {
		mant_part = std::frexpl(*itr, &exp_part);
		mant_v.push_back(mant_part);
		exp_v.push_back(exp_part);
	}

	//指数毎、仮数毎に計算
	exp = 0;
	mant = 1;
	for (int i = 0; i < exp_v.size(); i++) {
		mant = mant * mant_v[i];
		exp += exp_v[i];
	}
	//計算結果を指数と仮数に分離
	mant = std::frexpl(mant, &exp_part);
	exp += exp_part;
}
void log_div_factorial_part(int&k, int max, double log_p, std::vector<std::pair<int, double>>&result, std::pair<int, double>&preb_res) {
	double mant_part;
	int exp_part;

	if (k == 0) {
		mant_part = 1;
		result.clear();
		result.reserve(max);
		preb_res.second = std::frexpl(mant_part, &preb_res.first);
		result.push_back(preb_res);
	}
	if (k == max)return;
	k++;

	mant_part = -1 * log_p / k;
	std::pair<int, double> current_res;
	current_res.second = std::frexpl(mant_part, &current_res.first);

	preb_res.second = preb_res.second* current_res.second;
	preb_res.second = std::frexpl(preb_res.second, &exp_part);
	preb_res.first += exp_part + current_res.first;
	result.push_back(preb_res);
	log_div_factorial_part(k, max, log_p, result, preb_res);
}
