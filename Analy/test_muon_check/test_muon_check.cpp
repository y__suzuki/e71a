#pragma comment(lib,"FILE_structure.lib")
#include <FILE_structure.hpp>


std::multimap<int, mfile0::M_Chain> group_divide(std::vector<mfile0::M_Chain> &chains);
mfile0::M_Chain change_gid(mfile0::M_Chain &c, int add);

int main(int argc, char**argv) {
	if (argc != 11) {
		fprintf(stderr, "usage\n");
		exit(1);
	}
	std::string file_in_mfile_all = argv[1];
	std::string file_in_mfile_sel1 = argv[2];
	std::string file_in_mfile_sel2 = argv[3];

	std::string file_out_single_mfile_all = argv[4];
	std::string file_out_single_mfile_sel1 = argv[5];
	std::string file_out_single_mfile_sel2 = argv[6];

	std::string file_out_multi_mfile_all = argv[7];
	std::string file_out_multi_mfile_sel1 = argv[8];
	std::string file_out_multi_mfile_sel2 = argv[9];

	std::string file_out_multi_mfile_merge = argv[10];

	mfile0::Mfile m_all, m_sel1, m_sel2;
	mfile1::read_mfile_extension(file_in_mfile_all, m_all);
	mfile1::read_mfile_extension(file_in_mfile_sel1, m_sel1);
	mfile1::read_mfile_extension(file_in_mfile_sel2, m_sel2);

	std::multimap<int, mfile0::M_Chain> g_all, g_sel1, g_sel2;
	g_all = group_divide(m_all.chains);
	g_sel1 = group_divide(m_sel1.chains);
	g_sel2 = group_divide(m_sel2.chains);

	std::vector<mfile0::M_Chain> s_all, s_sel1, s_sel2;
	std::vector<mfile0::M_Chain> multi_all, multi_sel1, multi_sel2,multi_merge;
	int all = 0, single = 0, multi = 0;
	for (auto itr = g_all.begin(); itr != g_all.end(); itr++) {
		int gid = itr->first;
		int count = g_all.count(gid);
		all++;
		if (count == 1)single++;
		else {
			multi++;
		}

		if (g_sel1.count(gid) == 0) {
			fprintf(stderr, "sel1 gid=%d not found\n", gid);
			exit(1);
		}
		if (g_sel2.count(gid) == 0) {
			fprintf(stderr, "sel2 gid=%d not found\n", gid);
			exit(1);
		}

		auto range_all = g_all.equal_range(gid);
		auto range_sel1 = g_sel1.equal_range(gid);
		auto range_sel2 = g_sel2.equal_range(gid);
		for (auto res = range_all.first; res != range_all.second; res++) {
			if (count == 1) {
				s_all.push_back(res->second);
			}
			else { 
				multi_all.push_back(res->second); 
				multi_merge.push_back(change_gid(res->second, 0));
			}
		}
		for (auto res = range_sel1.first; res != range_sel1.second; res++) {
			if (count == 1)s_sel1.push_back(res->second);
			else { 
				multi_sel1.push_back(res->second); 
				multi_merge.push_back(change_gid(res->second, 1));

			}
		}
		for (auto res = range_sel2.first; res != range_sel2.second; res++) {
			if (count == 1)s_sel2.push_back(res->second);
			else { 
				multi_sel2.push_back(res->second); 
				multi_merge.push_back(change_gid(res->second, 2));
			}
		}


		itr = std::next(itr, g_all.count(itr->first) - 1);
	}
	printf("all    group : %d\n", all);
	printf("single group : %d\n", single);
	printf("multi  group : %d\n", multi);

	m_all.chains = s_all;
	mfile1::write_mfile_extension(file_out_single_mfile_all, m_all);
	m_all.chains = s_sel1;
	mfile1::write_mfile_extension(file_out_single_mfile_sel1, m_all);
	m_all.chains = s_sel2;
	mfile1::write_mfile_extension(file_out_single_mfile_sel2, m_all);

	m_all.chains = multi_all;
	mfile1::write_mfile_extension(file_out_multi_mfile_all, m_all);
	m_all.chains = multi_sel1;
	mfile1::write_mfile_extension(file_out_multi_mfile_sel1, m_all);
	m_all.chains = multi_sel2;
	mfile1::write_mfile_extension(file_out_multi_mfile_sel2, m_all);

	m_all.chains = multi_merge;
	mfile1::write_mfile_extension(file_out_multi_mfile_merge, m_all);

}
std::multimap<int, mfile0::M_Chain> group_divide(std::vector<mfile0::M_Chain> &chains) {
	std::multimap<int, mfile0::M_Chain> ret;
	int groupid;
	for (auto &c : chains) {
		groupid = c.basetracks.begin()->group_id;
		ret.insert(std::make_pair(groupid, c));
	}
	return ret;
}
mfile0::M_Chain change_gid(mfile0::M_Chain &c, int add) {
	mfile0::M_Chain ret = c;
	for (auto itr = ret.basetracks.begin(); itr != ret.basetracks.end(); itr++) {
		itr->group_id = itr->group_id * 10 + add;
	}
	return ret;
}